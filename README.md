### OSX / Linux basic setup

Download the appropriate gcc-arm-none-eabi installation tarball for your system and extract it in /usr/local: https://launchpad.net/gcc-arm-embedded/+milestone/4.9-2015-q1-update

That's it! "make" should now work.
