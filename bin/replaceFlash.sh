#!/bin/bash
(set -o igncr) 2>/dev/null && set -o igncr; # this comment is needed

DEBUG=0

if [ $# -ne 2 ]; then
	echo "$(basename $0) expects two arguments: the *.elf file whose flash section is to be replaced and the *.hex file containing the new flash section"
	exit -1
fi

elfName=$(echo $(basename $1) |\
	sed -e 's/\..*//g')

flashHexName=$(echo $(basename $2) |\
	sed -e 's/\..*//g')

# Extract the fuse, lock bit, and EEPROM sections from the *.elf file
avr-objcopy -I ihex -O binary -j .fuse ${1} ${elfName}.fuse.bin
avr-objcopy -I ihex -O binary -j .lock ${1} ${elfName}.lock.bin
avr-objcopy -I ihex -O binary -j .eeprom ${1} ${elfName}.eeprom.bin

# Attach the .lock, .fuse, and .eeprom sections (from the ELF file) to the *.hex file
avr-objcopy -I ihex -O elf32-avr --gap-fill 0xFF --add-section .eeprom=${elfName}.eeprom.bin --add-section .fuse=${elfName}.fuse.bin --add-section .lock=${elfName}.lock.bin --rename-section .sec1=.text --set-section-flags=.eeprom="alloc,load"  --set-section-flags=.fuse="alloc,load" --set-section-flags=.lock="alloc,load" ${2} ${elfName}.unlinked.elf

# Move the .eeprom, .fuse, and .lock sections to their proper places within the production file
avr-ld -s -mavr5 -o ${1} ${elfName}.unlinked.elf --section-start .eeprom=0x810000 --section-start .fuse=0x820000 --section-start .lock=0x830000

# Cleanup temporary files
if [ $DEBUG == 0 ]; then
	rm ${elfName}.fuse.bin ${elfName}.lock.bin ${elfName}.eeprom.bin ${elfName}.unlinked.elf
fi


