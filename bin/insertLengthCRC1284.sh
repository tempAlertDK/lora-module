#!/bin/bash
(set -o igncr) 2>/dev/null && set -o igncr; # this comment is needed

# This is the location of the length information in the file.  It
# should be located just after the interrupt vectors.
LENGTH_LOC=140
# This is the number of bytes dedicated to storing the file's length.
LENGTH_NBYTES=4

DEBUG=0

if [ $# -ne 1 ]; then
	echo "$(basename $0) expects one argument: the hex file to which the length and CRC should be inserted"
	exit -1
fi

# Verify that the file name passed as the first argument actually exists
if [ ! -f $1 ]; then
	echo "$1 does not exist"
	exit -1
fi

TARGET=${1//.hex/}

if [ $DEBUG == "1" ]; then
	# Create an ASCII file from the original hex file.
    srec_cat ${TARGET}.hex -Intel -o ${TARGET}.asc -Ascii-Hex
fi


# Compute the length of the hex file and place it at address 0x0068,
# immediately after the interrupt vector table.  The address range
# passed to the exclude option is inclusive at its lower limit and
# exlusive at its upper limit.  The Little_Endian_Maximum parameter
# instructs srec_cat to calculate the maximum address of the hex file.
# The "0x0068 4" places the computed size in the four bytes starting
# at 0x0068.

srec_cat ${TARGET}.hex -Intel -exclude $LENGTH_LOC $(expr $LENGTH_LOC + $LENGTH_NBYTES) \
    -Little-Endian-Maximum $LENGTH_LOC $LENGTH_NBYTES \
    -Output ${TARGET}.max.hex -Intel

# Fill the empty spaces in the file with 0xFF to ensure that CRC performed by the
# processor is deterministic and does not depend on random/old FLASH data
srec_cat ${TARGET}.max.hex -Intel \
    -fill 0xFF -over \( ${TARGET}.max.hex -Intel \) \
    -Output ${TARGET}.fill.hex -Intel

# Compute the CRC of the hex file and place it at the end of the file
srec_cat ${TARGET}.fill.hex -Intel --Little_Endian_CRC16 \
    -Maximum-Address \( ${TARGET}.fill.hex -Intel \) -Cyclic_Redundancy_Check_16_XMODEM \
    -Output ${TARGET}.crc.hex -Intel
	
if [ $DEBUG == "1" ]; then
	cp	${TARGET}.crc.hex ${TARGET}.prereformat.hex
fi

# Reformat the hex file to match original format
srec_cat ${TARGET}.crc.hex -Intel -o ${TARGET}.crc.hex -Intel --line_length=44

# Create a ASCII-Hex file for use with bootloader
srec_cat ${TARGET}.crc.hex -Intel -o ${TARGET}.crc.asc -Ascii-Hex

if [ $DEBUG == "0" ]; then
    rm ${TARGET}.max.hex ${TARGET}.fill.hex
fi

exit 0
