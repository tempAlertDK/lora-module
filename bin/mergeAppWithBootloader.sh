#!/bin/bash
(set -o igncr) 2>/dev/null && set -o igncr; # this comment is needed

DEBUG=0

if [ $# -ne 2 ]; then
	echo "$(basename $0) expects two arguments: the application and bootloader *.elf files"
	exit -1
fi

appName=$(echo $(basename $1) |\
	sed -e 's/\..*//g')

blName=$(echo $(basename $2) |\
	sed -e 's/\..*//g')
	
# Extract the fuse, lock bit, and text & data sections from the bootloader
avr-objcopy -I ihex -O binary -j .fuse ${2} ${blName}.fuse.bin
avr-objcopy -I ihex -O binary -j .lock ${2} ${blName}.lock.bin
avr-objcopy -I ihex -O ihex -j .text -j .data ${2} ${blName}.textdata.hex

#Combine the application image with the bootloader image in a single Intel hex file
srec_cat ${appName}.crc.hex -Intel ${blName}.textdata.hex -Intel -o ${appName}_${blName}.combinedtextdata.hex -Intel

# Extract the EEPROM contents from the application *.elf file into a binary file
avr-objcopy -I ihex -O binary -j .eeprom ${1} ${appName}.eeprom.bin

# Attach the .lock and .fuse sections (from the bootloader) and the .eeprom section (from the application) with the combined hex image produced above
# Note that we've made a change here when renaming sections.  When the application was less than 64kB, sec1 was the application and sec2 was the bootloader.
# As the application grew beyond 64kB, sec2 was also used to hold the application and sec3 was used to hold the bootloader.  I believe this is due to the fact
# that a Intel hex file uses 16-bit addressing, so to address more than 64kB, the format makes use of the 'extended linear address record' (0x04).  As such, 
# it appears that binary data beyond 64kB is treated as an additional section.  Unfortunately, the bootloader was pushed to sec3, and because we did not rename
# sec3 as text, it was then eliminated.  Now, we rename sec3 to text, and just to be safe, we also allow for a sec4, which we also rename to text.  Currently,
# nothing exists is sec4, but it was a test to see whether objcopy can be told to rename non-existent section.  Thankfully, it can, so that this script will
# still work if only sec1 and sec2 exist.
avr-objcopy -I ihex -O elf32-avr --gap-fill 0xFF --add-section .eeprom=${appName}.eeprom.bin --add-section .fuse=${blName}.fuse.bin --add-section .lock=${blName}.lock.bin --rename-section .sec1=.text --rename-section .sec2=.text --rename-section .sec3=.text --rename-section .sec4=.text --set-section-flags=.eeprom="alloc,load"  --set-section-flags=.fuse="alloc,load" --set-section-flags=.lock="alloc,load"  ${appName}_${blName}.combinedtextdata.hex ${appName}_${blName}.unlinked.elf

# Move the .eeprom, .fuse, and .lock sections to their proper places within the production file
avr-ld -s -mavr5 -o ${appName}_AppBL.elf ${appName}_${blName}.unlinked.elf --section-start .eeprom=0x810000 --section-start .fuse=0x820000 --section-start .lock=0x830000

# Cleanup temporary files
if [ $DEBUG == 0 ]; then
	rm ${blName}.fuse.bin ${blName}.lock.bin ${blName}.textdata.hex ${appName}.eeprom.bin ${appName}_${blName}.combinedtextdata.hex ${appName}_${blName}.unlinked.elf
fi


