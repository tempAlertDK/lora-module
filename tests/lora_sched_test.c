#include "unity.h"

#include "lora_sched.h"

void test_example(void) {
	for (int i = 0; i < LORA_SCHED_SIZE - 1; i++) {
		lora_sched_add_downlink(i, i*2);
	}
	for (int i = 0; i < (LORA_SCHED_SIZE - 1) * 2; i += 2) {
		lora_sched_entry_t *ent = lora_sched_pop(i);
		TEST_ASSERT_NOT_NULL(ent);
		TEST_ASSERT_EQUAL_INT(i, ent->slot_idx);

		// No entry for odd-numbered slots
		TEST_ASSERT_NULL(lora_sched_pop(i + 1));
	}

	// Just repeat the above again.
	for (int i = 0; i < LORA_SCHED_SIZE - 1; i++) {
		lora_sched_add_downlink(i, i*2);
	}
	for (int i = 0; i < (LORA_SCHED_SIZE - 1) * 2; i += 2) {
		lora_sched_entry_t *ent = lora_sched_pop(i);
		TEST_ASSERT_NOT_NULL(ent);
		TEST_ASSERT_EQUAL_INT(i, ent->slot_idx);

		// No entry for odd-numbered slots
		TEST_ASSERT_NULL(lora_sched_pop(i + 1));
	}
}

int main(void) {
	UNITY_BEGIN();
	RUN_TEST(test_example);
	UNITY_END();
}
