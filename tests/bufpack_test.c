#include <string.h>
#include "unity.h"

#include "bufpack.h"


// Tests normal operation of bufpack.
void test_bufpack_ok(void) {
	const char *string = "a_str";
	uint8_t buf[15];
	bufpack_t bp;
	bufpack_init(&bp, buf, 15);
	bufpack_write1(&bp, 0xe1);
	bufpack_write2(&bp, 0xb243);
	bufpack_write3(&bp, 0xc500a2);
	bufpack_write_buf(&bp, (uint8_t *)string, 5);
	bufpack_write4(&bp, 0xabcd1234);
	TEST_ASSERT_FALSE(bp.error);

	bufpack_init(&bp, buf, 15);
	uint8_t v1 = bufpack_read1(&bp);
	TEST_ASSERT_EQUAL_UINT8(0xe1, v1);
	uint16_t v2 = bufpack_read2(&bp);
	TEST_ASSERT_EQUAL_UINT16(0xb243, v2);
	uint32_t v3 = bufpack_read3(&bp);
	TEST_ASSERT_EQUAL_UINT32(0xc500a2, v3);
	uint8_t *str_actual = bufpack_read_buf(&bp, 5);
	TEST_ASSERT_EQUAL_MEMORY(string, str_actual, 5);
	uint32_t v4 = bufpack_read4(&bp);
	TEST_ASSERT_EQUAL_UINT32(0xabcd1234, v4);
	TEST_ASSERT_FALSE(bp.error);
}

// Tests bufpack is doesn't overrun, and produces errors.
void test_bufpack_bounds(void) {
	// Test writes
	uint8_t buf[6];
	memset(buf, 0, 6);
	bufpack_t bp;
	bufpack_init(&bp, buf, 5);
	bufpack_write4(&bp, 0xffffffff);
	TEST_ASSERT_FALSE(bp.error);
	bufpack_write1(&bp, 0xff);
	TEST_ASSERT_FALSE(bp.error);

	// At this point bufpack is full, all further writes should be no-ops
	// and bp.error should remain set.
	bufpack_write2(&bp, 0xff);
	TEST_ASSERT_TRUE(bp.error);
	bufpack_write1(&bp, 0xff);
	TEST_ASSERT_TRUE(bp.error);
	bufpack_write3(&bp, 0xff);
	TEST_ASSERT_TRUE(bp.error);
	bufpack_write4(&bp, 0xff);
	TEST_ASSERT_TRUE(bp.error);
	// Make sure we didn't overrun buf.
	TEST_ASSERT_EQUAL_UINT8(0, buf[5]);

	// Now test reads.
	bufpack_init(&bp, buf, 5);
	bufpack_read2(&bp);
	TEST_ASSERT_FALSE(bp.error);
	bufpack_read3(&bp);
	TEST_ASSERT_FALSE(bp.error);

	bufpack_read1(&bp);
	TEST_ASSERT_TRUE(bp.error);
	bufpack_read2(&bp);
	TEST_ASSERT_TRUE(bp.error);
	bufpack_read3(&bp);
	TEST_ASSERT_TRUE(bp.error);
	bufpack_read4(&bp);
	TEST_ASSERT_TRUE(bp.error);
}

int main(void) {
	UNITY_BEGIN();
	RUN_TEST(test_bufpack_ok);
	RUN_TEST(test_bufpack_bounds);
	return UNITY_END();
}
