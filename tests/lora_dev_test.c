#include <string.h>
#include "unity.h"

#include "lora_dev.h"

void test_lora_dev_ok(void) {
	// Need this to be true for these tests to work.
	TEST_ASSERT_TRUE(LORA_DEV_MAX_COUNT > LORA_DEV_MAX_PEND_DOWNLINKS + 2);

	TEST_ASSERT_NULL(lora_dev_lookup_addr(9));

	// Fill up devs
	for (int i = 0; i < LORA_DEV_MAX_COUNT; i++) {
		lora_dev_t *dev = lora_dev_get_empty();
		lora_dev_set_used(dev);
		memcpy(&dev->serial, &i, sizeof(int));
	}
	TEST_ASSERT_NULL(lora_dev_get_empty());

	lora_dev_t *dev = lora_dev_lookup_addr(5);
	TEST_ASSERT_NOT_NULL(dev);
	int addr = 5;
	TEST_ASSERT_EQUAL_MEMORY(&addr, &dev->serial, sizeof(int));

	uint8_t *downlink_data = (uint8_t *)"hello";
	for (int i = 0; i < LORA_DEV_MAX_PEND_DOWNLINKS; i++) {
		lora_dev_t *dev = lora_dev_lookup_addr(i);
		lora_dev_set_downlink(dev, downlink_data, 5);
	}
	// Shouldn't be able to add one more.
	dev = lora_dev_lookup_addr(LORA_DEV_MAX_PEND_DOWNLINKS + 1);
	TEST_ASSERT_FALSE(lora_dev_set_downlink(dev, downlink_data, 5));

	// Check one of the downlinks that we added.
	dev = lora_dev_lookup_addr(3);
	lora_dev_pending_downlink_t *downlink = lora_dev_get_downlink(dev);
	TEST_ASSERT_NOT_NULL(downlink);
	TEST_ASSERT_EQUAL_MEMORY(downlink_data, downlink->payload, 5);
	// Clear it
	lora_dev_downlink_clear(downlink);
	// Shouldn't exist anymore.
	downlink = lora_dev_get_downlink(dev);
	TEST_ASSERT_NULL(downlink);
	// Now we should be able to add another.
	dev = lora_dev_lookup_addr(LORA_DEV_MAX_PEND_DOWNLINKS + 1);
	TEST_ASSERT_TRUE(lora_dev_set_downlink(dev, downlink_data, 5));

	// Try to lookup an address that we didn't add a downlink for.
	addr = LORA_DEV_MAX_PEND_DOWNLINKS + 2;
	dev = lora_dev_lookup_addr(addr);
	TEST_ASSERT_NULL(lora_dev_get_downlink(dev));
}

int main(void) {
	UNITY_BEGIN();
	RUN_TEST(test_lora_dev_ok);
	return UNITY_END();
}
