/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_delay.h"
//#include "nrf_drv_spi.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "nrf_drv_clock.h"

#include "app_timer.h"
#include "app_button.h"
#include "app_util_platform.h"
#include "app_error.h"
#include <string.h>

#include "bsp.h"
#include "pstorage.h"

#include "config.h"

#include "core_cm4.h"
#include "boards.h"
#include "pa_lna.h"
#include "si705x.h"
#include "fdc221x.h"
#include "mcp23008.h"
#include "lis2dh12.h"
#include "els31.h"

#include "els31uart.h"
#include "modemPrinter.h"
#include "modem.h"

#include "pca8561.h"
#include "spi.h"

#include "irqn.h"

#include "twi_mtx.h"
#include "spi_smphr.h"

#include "epd.h"
#include "epdScreen.h"

#include "loraradio.h"
#include "loranode.h"
#include "memtrace.h"

#include "util.h"

#include "bluetooth.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "portmacro_cmsis.h"

#include "SEGGER_RTT.h"

#define PIEZO_BUZZER	18 //piezo buzzer

#define APP_TIMER_PRESCALER             0                                           /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE         4                                           /**< Size of timer operation queues. */

static TaskHandle_t m_startup_thread;
static TaskHandle_t m_irqn_thread;
static TaskHandle_t m_node_thread;
static TaskHandle_t m_app_thread;
static TaskHandle_t m_uart_rtt_thread;

static SemaphoreHandle_t temp_conversion_complete = NULL;


void hw_read_reset_reason(uint32_t *resetreas) {
	/* Read the reason why we reset into state. We like to store it into state so
	 * that we can test how our firmware reacts in different simulated reset
	 * scenarios. It also makes reading the reason from the debugger easier. */
	*resetreas = NRF_POWER->RESETREAS;

	/* Set the reset reason to 0, because it is a latched register and we have
	 * read it and want it to be valid the next time we read it (not contained
	 * old latched values from last time we read it). */
	NRF_POWER->RESETREAS = 0;

	/* Print some messages about why we reset. */

	printf("I reset because: 0x%08x", (unsigned int) *resetreas);

	if (*resetreas & POWER_RESETREAS_DIF_Msk) {
		printf("-Reset due to wake-up from OFF mode when wakeup is"
				" triggered from entering into debug interface mode");
	}
	if (*resetreas & POWER_RESETREAS_LPCOMP_Msk) {
		printf("-Reset due to wake-up from OFF mode when wakeup is"
				" triggered from ANADETECT signal from LPCOMP");
	}
	if (*resetreas & POWER_RESETREAS_OFF_Msk) {
		printf("-Reset due to wake-up from OFF mode when wakeup is"
				" triggered from the DETECT signal from GPIO");
	}
	if (*resetreas & POWER_RESETREAS_LOCKUP_Msk) {
		printf("-Reset from CPU lock-up detected");
	}
	if (*resetreas & POWER_RESETREAS_SREQ_Msk) {
		printf("-Reset from AIRCR.SYSRESETREQ detected");
	}
	if (*resetreas & POWER_RESETREAS_DOG_Msk) {
		printf("-Reset from watchdog detected");
	}
	if (*resetreas & POWER_RESETREAS_RESETPIN_Msk) {
		printf("-Reset from pin detected");
	}
}

static int16_t m_temp_tenthDegC;

void conversionComplete_cb(bool success, int16_t temp_tenthDegC) {
	static BaseType_t xHigherPriorityTaskWoken1 = pdFALSE, xHigherPriorityTaskWoken2 = pdFALSE;

	xSemaphoreGiveFromISR(g_twi_smphr, &xHigherPriorityTaskWoken1);
	MEMTRACE_EVENT0(MEMTRACE_EV_SENSOR_READ_DONE);

	if (success) {
		m_temp_tenthDegC = temp_tenthDegC;
	}

	xSemaphoreGiveFromISR(temp_conversion_complete, &xHigherPriorityTaskWoken2);

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken1 || xHigherPriorityTaskWoken2);
}

int seq = 0;
void app_thread(void *arg) {
	char data_buf[256];

	vTaskDelay(2000);

	// Acquire
	loranode_acquire();
	printf("acquiring\r\n");
	loranode_event_t event;
	xQueueReceive(loranode_eventQueue, &event, portMAX_DELAY);
	if (event.type != LORANODE_EVENT_ACQUIRED) {
		APP_ERROR_HANDLER(NRF_ERROR_INVALID_STATE);
	}
	printf("acquired beacon\r\n");

	// Send 2 uplinks
	//for (int i = 0; i < 2; i++) {
	while (1) {
		m_temp_tenthDegC = seq;
		seq++;
		int data_len = snprintf(
				data_buf, 256, "temp: %d", m_temp_tenthDegC);
		printf("sending uplink\r\n");
		loranode_uplink((uint8_t *)data_buf, data_len);
		xQueueReceive(loranode_eventQueue, &event, portMAX_DELAY);
		if (event.type != LORANODE_EVENT_DOWNLINK) {
			APP_ERROR_HANDLER(NRF_ERROR_INVALID_STATE);
		}
		if (event.result.data_len) {
			printf("    Hex:   ");
			util_printHex(event.result.data, event.result.data_len);
			printf("    Ascii: ");
			util_printAscii(event.result.data, event.result.data_len);
			printf("    Signal: ");
			loraradio_print_rx_signal_info(
					event.result.snr, event.result.rssi);
		}
		vTaskDelay(5000);
	}

	loranode_stop();
	printf("stopped loranode\r\n");

	vTaskDelay(2000);

	// Acquire
	printf("acquiring\r\n");
	loranode_acquire();
	xQueueReceive(loranode_eventQueue, &event, portMAX_DELAY);
	if (event.type != LORANODE_EVENT_ACQUIRED) {
		APP_ERROR_HANDLER(NRF_ERROR_INVALID_STATE);
	}
	printf("acquired beacon\r\n");

	// uplink forever
	while (1) {
		m_temp_tenthDegC = seq;
		seq++;
		int data_len = snprintf(
				data_buf, 256, "temp: %d", m_temp_tenthDegC);
		printf("sending uplink\r\n");
		loranode_uplink((uint8_t *)data_buf, data_len);
		xQueueReceive(loranode_eventQueue, &event, portMAX_DELAY);
		if (event.type != LORANODE_EVENT_DOWNLINK) {
			APP_ERROR_HANDLER(NRF_ERROR_INVALID_STATE);
		}
		if (event.result.data_len) {
			printf("    Hex:   ");
			util_printHex(event.result.data, event.result.data_len);
			printf("    Ascii: ");
			util_printAscii(event.result.data, event.result.data_len);
			printf("    Signal: ");
			loraradio_print_rx_signal_info(
					event.result.snr, event.result.rssi);
		}
		vTaskDelay(5000);
	}
}

static void startup_thread(void *arg) {
	irqn_init();
	loraradio_init();
	loranode_init();

#ifdef CFG_TxContinuousMode
	// Transmit forever, for power testing.
	loraradio_params_t params = {
		.freq = 918000000,
		.tx_pow = 20,
		.spreading_factor = SF10,
		.bandwidth = BW125,
		.coding_rate = CR_4_5,
		.implicit_header = true,
		.no_crc = false,
		.timeout_syms = 0,
		.data_len = 5
	};
	printf("TXing continuously\r\n");
	loraradio_prepare_tx(&params);
	loraradio_tx_sync();
	printf("Shouldn't be here\r\n");
	while (1) { }
#endif

	/* Start the Segger RTT command line thread */
	if (pdPASS != xTaskCreate(rtt_thread, "RTT", 128, NULL, 1, &m_uart_rtt_thread)) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	printf("TempAlert Node starting...\r\n");

	/* Start the IRQN thread */
	if(pdPASS != xTaskCreate(irqn_thread, "IRQN", 512, NULL, 1, &m_irqn_thread)) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	if(pdPASS != xTaskCreate(loranode_thread, "NODE", 512, NULL, 1, &m_node_thread)) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	if(pdPASS != xTaskCreate(app_thread, "APP", 512, NULL, 1, &m_app_thread)) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	/* Now that everything is running, suspend the start-up task */
	while(1) {
		vTaskSuspend(NULL);
	}
}

/**@brief Application main function.
 */
int main(void) {
	printf("\r\nUART Start!\r\n");
	uint32_t rs;
	hw_read_reset_reason(&rs);
	printf("\r\n");

	/*
	 * Do not start any interrupt that uses system functions before system
	 * initialization.  The best solution is to call vTaskStartScheduler()
	 * before any other initialization.
	 */
	nrf_gpio_pin_dir_set(PIEZO_BUZZER, 1); //it's an output
	nrf_gpio_pin_clear(PIEZO_BUZZER); //set to low (off)
	nrf_gpio_pin_dir_set(TEMPENn_PIN, 1); //it's an output
	nrf_gpio_pin_set(TEMPENn_PIN); //set to low (off)
	nrf_gpio_cfg_input(EXTENn_PIN, NRF_GPIO_PIN_PULLDOWN);

	/* Initialize the 3 select lines that drive the 3:8 demux */
	spics_init();

	/* Initialize the timer subsystem */
	APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);

	nrf_delay_ms(100);

	/*
	 * Create binary semaphores which protect the TWI and SPI busses
	 */
	g_spi_smphr = xSemaphoreCreateBinary();
	xSemaphoreGive(g_spi_smphr);

	temp_conversion_complete = xSemaphoreCreateBinary();
	if (temp_conversion_complete == NULL) {
		APP_ERROR_CHECK(NRF_ERROR_NO_MEM);
	}

	/* Initialize the SoftDevice and the BLE-related subsystems */
	ble_stack_init();
	gap_params_init();
	services_init();
	advertising_init();
	conn_params_init();

	/* Get the device ID from the SI705x temperature sensor. */
	uint8_t serial[8];
	memset(&serial, 0, sizeof(serial));

	printf("Device serial nubmer: ");
	si705x_getSerial(serial);
	for (uint8_t i = 0; i < 8; i++) {
		printf("%02x", serial[i]);
	}
	printf("\r\n");

	/* Attempt to communicate with the FDC221x capacitance sensor */
	uint16_t manID;
	if (fdc221x_getManufacturerID(&manID)) {
		printf("Manufacture ID: %04x\r\n", manID);
	} else {
		printf("Failed to communicate with FDC221x!\r\n");
	}

	/*
	 * Start execution. The start-up thread will call the initialization
	 * functions for other threads and then start the other threads.
	 */
	if(pdPASS != xTaskCreate(startup_thread, "Start-up", 512, NULL, 1, &m_startup_thread)) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	/* Activate deep sleep mode */
	SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;

	/* Start FreeRTOS scheduler.  This call should never return. */
	vTaskStartScheduler();

	while (true) {
		APP_ERROR_HANDLER(NRF_ERROR_FORBIDDEN);
	}
}

void vApplicationStackOverflowHook( TaskHandle_t xTask,
                                    signed char *pcTaskName ) {
	SEGGER_RTT_printf(0, "Stack overflow detected in task %s\r\n", pcTaskName);
	while(1);
}

/** 
 * @}
 */
