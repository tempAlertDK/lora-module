#pragma once

#include <stdint.h>

#include "lorashared.h"

// We will never have more than LORA_DOWNLINK_SLOT_DELTA entries, since
// we only use this for downlinks.
#define LORA_SCHED_SIZE (LORA_DOWNLINK_SLOT_DELTA + 1)

typedef struct {
	uint16_t dev_addr;
	int slot_idx;
} lora_sched_entry_t;

void lora_sched_add_downlink(uint16_t dev_addr, int slot_idx);
lora_sched_entry_t *lora_sched_pop(int slot_idx);
