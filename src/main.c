/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_delay.h"
//#include "nrf_drv_spi.h"
#include "nrf_gpio.h"
#include "nrf_drv_gpiote.h"
#include "nrf_log.h"
#include "nrf_drv_clock.h"

#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "softdevice_handler.h"
#include "ble_nus.h"
#include "ble_levelSensor.h"

#include "app_timer.h"
#include "app_button.h"
#include "app_util_platform.h"
#include "app_error.h"

#include "bsp.h"
#include "bsp_btn_ble.h"
#include "pstorage.h"

#include "config.h"

#include "core_cm4.h"
#include "boards.h"
#include "pa_lna.h"
#include "IRQn.h"
#include "mcp23008.h"
#include "uart.h"
#include "sensors.h"
#include "pca8561.h"
#include "deviceID.h"
#include "si705x.h"
#include "lis2dh12.h"
#include "els31.h"
#include "els31uart.h"
#include "modem.h"
#include "cmdline.h"
#include "stoFwd.h"
#include "scController.h"
#include "unixTime.h"
#include "gitversion.h"
#include "pca8561.h"
#include "sx1272.h"
#include "spi.h"
#include "gitversion.h"
#include "log.h"
#include "version.h"

#include "irqn.h"

#include "twi_mtx.h"
#include "spi_mtx.h"

#ifndef BOARD_LORA_MODULE_REV1P0
	#include "epd.h"
	#include "epdScreen.h"
#endif

#include "loraradio.h"
#include "loragw.h"
#include "loratest.h"
#include "memtrace.h"

#include "bluetooth.h"
#include "fcc_bluetooth.h"
#include "fcc_control.h"
#include "fcc_lora.h"
#include "util.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "portmacro_cmsis.h"

#include "SEGGER_RTT.h"

#include "tempalert/ui.h"
#include "globals.h"
#include "bootloader_util.h"
#include "atecc508a.h"
#include "errors.h"
#include "nrf_drv_clock.h"
#include "fcc_phy.h"

void hw_read_reset_reason(resetReason_t *reset_reason) {
	uint32_t rst;
	/* Read the reason why we reset into state. We like to store it into state so
	 * that we can test how our firmware reacts in different simulated reset
	 * scenarios. It also makes reading the reason from the debugger easier. */
	rst = NRF_POWER->RESETREAS;

	/* Set the reset reason to 0, because it is a latched register and we have
	 * read it and want it to be valid the next time we read it (not contained
	 * old latched values from last time we read it). */
	NRF_POWER->RESETREAS = 0;

	/* Print some messages about why we reset. */
	*reset_reason = RESET_REASON_POWERON;

	printf("Reset reason: 0x%08x", (unsigned int) rst);

	if (rst & POWER_RESETREAS_DIF_Msk) {
		printf("-Reset due to wake-up from OFF mode when wakeup is"
				" triggered from entering into debug interface mode");
		*reset_reason = RESET_REASON_JTAG;
	}
	if (rst & POWER_RESETREAS_LPCOMP_Msk) {
		printf("-Reset due to wake-up from OFF mode when wakeup is"
				" triggered from ANADETECT signal from LPCOMP");
		*reset_reason = RESET_REASON_ANADETECT_LPCOMP;
	}
	if (rst & POWER_RESETREAS_OFF_Msk) {
		printf("-Reset due to wake-up from OFF mode when wakeup is"
				" triggered from the DETECT signal from GPIO");
		*reset_reason = RESET_REASON_GPIO_DETECT;
	}
	if (rst & POWER_RESETREAS_LOCKUP_Msk) {
		printf("-Reset from CPU lock-up detected");
		*reset_reason = RESET_REASON_CPU_LOCKUP;
	}
	if (rst & POWER_RESETREAS_SREQ_Msk) {
		printf("-Reset from AIRCR.SYSRESETREQ detected");
		*reset_reason = RESET_REASON_SOFTWARE_RESET;
	}
	if (rst & POWER_RESETREAS_DOG_Msk) {
		printf("-Reset from watchdog detected");
		*reset_reason = RESET_REASON_WATCHDOG_UNEXPECTED;
	}
	if (rst & POWER_RESETREAS_RESETPIN_Msk) {
		printf("-Reset from pin detected");
		*reset_reason = RESET_REASON_EXTERNAL;
	}

}


static TaskHandle_t startup_thread_handle;
static StaticTask_t startup_thread_tcb;
#define STARTUP_THREAD_STACK_SIZE 256
static StackType_t startup_thread_stack[STARTUP_THREAD_STACK_SIZE];

#if defined (LORA_GATEWAY_FULL) || defined (LORA_GATEWAY_TEST)
static TaskHandle_t loragw_thread_handle;
static StaticTask_t loragw_thread_tcb;
#define LORAGW_THREAD_STACK_SIZE 512
static StackType_t loragw_thread_stack[LORAGW_THREAD_STACK_SIZE];

static TaskHandle_t gateway_thread_handle;
static StaticTask_t gateway_thread_tcb;
#define GATEWAY_THREAD_STACK_SIZE 512
static StackType_t gateway_thread_stack[GATEWAY_THREAD_STACK_SIZE];
#endif


void gateway_thread(void *arg) {
	while(1) {
		loragw_notify_t notify;
		xQueueReceive(loragw_notifyQueue, &notify, portMAX_DELAY);
		printf("Received uplink (%d):\r\n", notify.payload_len);
		if (notify.payload_len) {
			printf("    Hex:   ");
			util_printHex(notify.payload, notify.payload_len);
			printf("    Ascii: ");
			util_printAscii(notify.payload, notify.payload_len);
			printf("    Signal: ");
			loraradio_print_rx_signal_info(notify.snr, notify.rssi);

			loragw_send_downlink(notify.dev_serial, (uint8_t *)"downlink", 8);
		}
	}
}

#pragma GCC diagnostic push // ncr
#pragma GCC diagnostic warning "-Wunused-variable"
void startup_thread(void *arg) {

	bt_test_init();
#ifndef BOARD_LORA_MODULE_REV1P0
	irqn_init();
#else
	nrf_drv_gpiote_init();
#endif
	cmdline_init();

#if (0)
	loraradio_init();//dk_todo: need this?
#else
	sx1272_init();//dk:hardware only, no task
#endif

	unixTime_init();

	errors_init();
	spi_csmux_init();
	lora_test_init();
	control_init();
	//loratest_init();

	/* Now that everything is running, suspend the start-up task */
	while(1) {
		vTaskSuspend(NULL);
	}
}


#pragma GCC diagnostic pop

/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */

static void bsp_event_handler(bsp_event_t event) {
#if defined(MODEM_TESTING) && (MODEM_TESTING == 1)
	modem_state_t modem_state;
#endif

	switch (event) {

#if defined(MODEM_TESTING) && (MODEM_TESTING == 1)
#else
	case BSP_EVENT_SLEEP:			/* Long press (5s) on BSP_BUTTON_0 */
		if (!g_uploadInprogress) {
			scController_startSleep();
		} else {
			log_warn("Upload in progress!, Sleeping disabled!");
		}
		break;
#endif

	case BSP_EVENT_KEY_0:
#if defined(MODEM_TESTING) && (MODEM_TESTING == 1)
		if (modem_getCurrentState(&modem_state, pdMS_TO_TICKS(1000)) && (modem_state == MODEM_STATE_OFF)) {
			modem_queuePhyCommand(MODEM_PHY_TURN_ON);
		} else {
			modem_queuePhyCommand(MODEM_PHY_TURN_OFF);
		}
#elif defined(FCC_TESTING)
		//fcc_phy
		phy_radio_select_button_press();
#else
		sensors_requestRead();
		if (g_uploadInprogress) {
			log_warn("Upload in progress!");
		} else {
			scController_startUpload(UPLOAD_EVENT_USER_TRIGGERED_BUTTON_PRESS);
		}
		ui_buttonPress();
#if defined(ENABLE_BLE) && (ENABLE_BLE == 1)
		if (!g_bleIsConnected) {
			log_info("Advertising BLE service...");
			/* Restart BLE advertising if not currently active */
			ble_advertising_start(BLE_ADV_MODE_FAST);
		}
#endif // ENABLE_BLE
#endif // MODEM_TESTING
		break;
	case BSP_EVENT_KEY_1:
		phy_radio_test_button_press();
		break;
	case BSP_EVENT_KEY_2:
		phy_radio_band_button_press();
		break;
	default:
		log_errorf("Button press (%u) un-handled!",event);
		break;

	}
}




/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to wake the application up.
 */
static void buttons_leds_init(bool * p_erase_bonds) {
	//bsp_event_t startup_event;

	uint32_t err_code = bsp_init(BSP_INIT_LED | BSP_INIT_BUTTONS, APP_TIMER_TICKS(100, APP_TIMER_PRESCALER),
			bsp_event_handler);
	APP_ERROR_CHECK(err_code);

	//err_code = bsp_btn_ble_init(NULL, &startup_event);
	//APP_ERROR_CHECK(err_code);

	/* Configure the single button properly. */
	err_code = bsp_event_to_button_action_assign(BTN_ID_SLEEP, BSP_BUTTON_ACTION_LONG_PUSH, BSP_EVENT_SLEEP);
	APP_ERROR_CHECK(err_code);
	err_code = bsp_event_to_button_action_assign(BTN_ID_SLEEP, BSP_BUTTON_ACTION_PUSH, BSP_EVENT_KEY_0);
	APP_ERROR_CHECK(err_code);
	err_code = bsp_event_to_button_action_assign(BTN_ID_TEST1, BSP_BUTTON_ACTION_PUSH, BSP_EVENT_KEY_1);
	APP_ERROR_CHECK(err_code);
	err_code = bsp_event_to_button_action_assign(BTN_ID_TEST2, BSP_BUTTON_ACTION_PUSH, BSP_EVENT_KEY_2);
	APP_ERROR_CHECK(err_code);

	*p_erase_bonds = true; //(startup_event == BSP_EVENT_CLEAR_BONDING_DATA);
}

#define FPU_EXCEPTION_MASK 0x0000009F

void FPU_IRQHandler(void)
{
    uint32_t *fpscr = (uint32_t *)(FPU->FPCAR+0x40);
    (void)__get_FPSCR();

    *fpscr = *fpscr & ~(FPU_EXCEPTION_MASK);
}


/**@brief Application main function.
 */
int main(void) {

	bool erase_bonds = false;
	
	SEGGER_RTT_printf(0,"\r\n\r\n\r\n");


	ret_code_t err_code;
	err_code = nrf_drv_clock_init();
	APP_ERROR_CHECK(err_code);

	log_version();

	hw_read_reset_reason(&g_resetReason);

	printf("\r\n");

	NVIC_SetPriority(FPU_IRQn, APP_IRQ_PRIORITY_LOW);
	NVIC_EnableIRQ(FPU_IRQn);

	printf("\r\n");

#ifdef BOARD_CELL_NODE_REV1P0
	nrf_gpio_pin_dir_set(INTSENSORENN_PIN, 1); //it's an output
	nrf_gpio_pin_set(INTSENSORENN_PIN); //set to low (off)
#endif
#ifndef BOARD_LORA_MODULE_REV1P0
	nrf_gpio_cfg_input(EXTSENSOREN_PIN, NRF_GPIO_PIN_PULLDOWN);
#endif

	/*
	 * Create mutex which protect the SPI bus
	 */

	twi_mtx_init();
	spi_mtx_init();

	/* TODO: SPI must be initialized before calling flash_init() */
	//flash_init();

	/*dk_todo: do i need this?*/
	//loraradio_params_t params = {
	//	.freq = 918000000,
	//	.spreading_factor = SF8,
	//	.bandwidth = BW500,
	//	.coding_rate = CR_4_5,
	//	.implicit_header = true,
	//	.no_crc = false,
	//	.timeout_syms = 0,
	//	.data_len = 254
	//};
	//printf("TOA: %u\r\n", loraradio_time_on_air(&params));

	/*
	 * Do not start any interrupt that uses system functions before system
	 * initialization.  The best solution is to call vTaskStartScheduler()
	 * before any other initialization.
	 */

	/* Initialize the timer subsystem */
	APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);

	/* buttons_leds_init() uses app_timer */
	/*dk_todo: rewrite buttons_leds_init */
	buttons_leds_init(&erase_bonds);


	LEDS_ON(LEDS_MASK);
	nrf_delay_ms(100);
	nrf_delay_ms(100);
	nrf_delay_ms(100);
	LEDS_OFF(LEDS_MASK);
	nrf_delay_ms(100);
	nrf_delay_ms(100);
	nrf_delay_ms(100);

	/*
	 * Start execution. The start-up thread will call the initialization
	 * functions for other threads and then start the other threads.
	 */
	if((startup_thread_handle = xTaskCreateStatic(startup_thread, "STARTUP", STARTUP_THREAD_STACK_SIZE, NULL, 1, startup_thread_stack, &startup_thread_tcb)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_INVALID_PARAM);
	}

	/* Activate deep sleep mode */
	SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;

	/* Start FreeRTOS scheduler.  This call should never return. */
	vTaskStartScheduler();

	while (true) {
		APP_ERROR_HANDLER(NRF_ERROR_FORBIDDEN);
	}
}

void externalFault_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
	LEDS_INVERT(BSP_LED_1_MASK);
    unsigned int n_pin = pin;
    printf("Pin %u Detected External Fault!\r\n", n_pin);
#ifndef BOARD_LORA_MODULE_REV1P0
    nrf_gpio_cfg_input(EXTSENSOREN_PIN,GPIO_PIN_CNF_PULL_Pulldown);
#endif
}

void ethernetIRQ_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
	LEDS_INVERT(BSP_LED_1_MASK);
    unsigned int n_pin = pin;
    printf("Pin %u Detected Ethernet Interrupt Request!\r\n", n_pin);
}

void expansionIRQ_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
	LEDS_INVERT(BSP_LED_1_MASK);
    unsigned int n_pin = pin;
    printf("Pin %u Detected Expansion Interrupt Request!\r\n", n_pin);
}

bool init_externalIRQs(void) {
	uint32_t err_code;

#ifndef BOARD_LORA_MODULE_REV1P0
	nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(true);
	in_config.pull = NRF_GPIO_PIN_PULLUP;


    /* Configure the EXTSENSOREN_PIN line as a input with HI-LO configured input (for now) */
    err_code = nrf_drv_gpiote_in_init(EXTSENSOREN_PIN, &in_config, externalFault_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(EXTSENSOREN_PIN, true);
#endif

    /* Configure the Ethernet IRQn line as a input with HI-LO configured input (for now) */
#if defined(BOARD_CELL_NODE_REV1P0)
    err_code = nrf_drv_gpiote_in_init(ETHERNET_IRQN_PIN, &in_config, ethernetIRQ_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(ETHERNET_IRQN_PIN, true);
#elif defined(BOARD_CELL_NODE_REV1P1)
    err_code = nrf_drv_gpiote_in_init(ETHERNET_MISO_IRQN_PIN, &in_config, ethernetIRQ_handler);
	APP_ERROR_CHECK(err_code);
	nrf_drv_gpiote_in_event_enable(ETHERNET_MISO_IRQN_PIN, true);
#endif

#if defined(BOARD_CELL_NODE_REV1P0)
    /* Configure the EXPANSION-IRQn line as a input with HI-LO configured input (for now) */
    err_code = nrf_drv_gpiote_in_init(EXPANSION_IRQN_PIN, &in_config, expansionIRQ_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(EXPANSION_IRQN_PIN, true);
#elif defined(BOARD_CELL_NODE_REV1P1)
    /* Rev 1.1 PCBs do not have a dedicated interrupt line for the expansion board. */
#endif

    return true;
}



void vApplicationStackOverflowHook( TaskHandle_t xTask,
                                    signed char *pcTaskName ) {
	log_errorf("Stack overflow detected in task %s", pcTaskName);										
	SEGGER_RTT_printf(0, "Stack overflow detected in task %s\r\n", pcTaskName);
	while(1);
}


/* configSUPPORT_STATIC_ALLOCATION is set to 1, so the application must provide an
implementation of vApplicationGetIdleTaskMemory() to provide the memory that is
used by the Idle task. */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer,
                                    StackType_t **ppxIdleTaskStackBuffer,
                                    uint32_t *pulIdleTaskStackSize )
{
	/* If the buffers to be provided to the Idle task are declared inside this
	function then they must be declared static - otherwise they will be allocated on
	the stack and so not exists after this function exits. */
	static StaticTask_t xIdleTaskTCB;
	static StackType_t uxIdleTaskStack[ configMINIMAL_STACK_SIZE ];

    /* Pass out a pointer to the StaticTask_t structure in which the Idle task's
    state will be stored. */
    *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

    /* Pass out the array that will be used as the Idle task's stack. */
    *ppxIdleTaskStackBuffer = uxIdleTaskStack;

    /* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
    Note that, as the array is necessarily of type StackType_t,
    configMINIMAL_STACK_SIZE is specified in words, not bytes. */
    *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}
/*-----------------------------------------------------------*/

/* configSUPPORT_STATIC_ALLOCATION and configUSE_TIMERS are both set to 1, so the
application must provide an implementation of vApplicationGetTimerTaskMemory()
to provide the memory that is used by the Timer service task. */
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer,
                                     StackType_t **ppxTimerTaskStackBuffer,
                                     uint32_t *pulTimerTaskStackSize )
{
	/* If the buffers to be provided to the Timer task are declared inside this
	function then they must be declared static - otherwise they will be allocated on
	the stack and so not exists after this function exits. */
	static StaticTask_t xTimerTaskTCB;
	static StackType_t uxTimerTaskStack[ configTIMER_TASK_STACK_DEPTH ];

    /* Pass out a pointer to the StaticTask_t structure in which the Timer
    task's state will be stored. */
    *ppxTimerTaskTCBBuffer = &xTimerTaskTCB;

    /* Pass out the array that will be used as the Timer task's stack. */
    *ppxTimerTaskStackBuffer = uxTimerTaskStack;

    /* Pass out the size of the array pointed to by *ppxTimerTaskStackBuffer.
    Note that, as the array is necessarily of type StackType_t,
    configTIMER_TASK_STACK_DEPTH is specified in words, not bytes. */
    *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}

/** 
 * @}
 */
