/*
 * loratest.h
 *
 *  Created on: Aug 1, 2017
 *      Author: kwgilpin
 */

#ifndef SRC_LORATEST_H_
#define SRC_LORATEST_H_

#include <stdint.h>
#include <stdbool.h>

bool loratest_start_tone(void);
bool loratest_stop_tone(void);
bool loratest_set_freq(uint32_t freq);
bool loratest_set_tx_pow(int8_t tx_pow);
bool loratest_enable_pa(void);
bool loratest_disable_pa(void);

void loratest_init(void);

#endif /* SRC_LORATEST_H_ */
