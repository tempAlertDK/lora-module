/* Copyright (c) 2015 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */
#include <stdint.h>

void HardFault_Handler(void) __attribute__(( naked ));

#if (1)
void HardFault_Handler(void)
{
    __asm volatile(
    "   ldr r3, =HardFault_c_handler            \n"
    "   tst lr, #4                              \n"

    /* PSP is quite simple and does not require additional handler */
    "   itt ne                                  \n"
    "   mrsne r0, psp                           \n"
    /* Jump to the handler, do not store LR - returning from handler just exits exception */
    "   bxne  r3                                \n"

    /* Processing MSP requires stack checking */
    "   mrs r0, msp                             \n"

    "   ldr   r1, =__StackTop                   \n"
    "   ldr   r2, =__StackLimit                 \n"

    /* MSP is in the range of <__StackTop, __StackLimit) */
    "   cmp   r0, r1                            \n"
    "   bhi   HardFault_MoveSP                  \n"
    "   cmp   r0, r2                            \n"
    "   bhi   HardFault_Handler_Continue        \n"

    "HardFault_MoveSP:                          \n"
    "   mov   sp, r1                            \n"
    "   mov   r0, #0                            \n"

    "HardFault_Handler_Continue:                \n"
    "   bx r3                                   \n"

    "   .align                                  \n"
    );
}
#else
void HardFault_Handler(void)
{
    __asm volatile
    (
        " tst lr, #4                                                \n"
        " ite eq                                                    \n"
        " mrseq r0, msp                                             \n"
        " mrsne r0, psp                                             \n"
        " ldr r1, [r0, #24]                                         \n"
        " ldr r2, handler2_address_const                            \n"
        " bx r2                                                     \n"
        " handler2_address_const: .word prvGetRegistersFromStack    \n"
    );
}

void prvGetRegistersFromStack( uint32_t *pulFaultStackAddress )
{
	/* These are volatile to try and prevent the compiler/linker optimising them
	away as the variables never actually get used.  If the debugger won't show the
	values of the variables, make them global my moving their declaration outside
	of this function. */
	volatile uint32_t r0;
	volatile uint32_t r1;
	volatile uint32_t r2;
	volatile uint32_t r3;
	volatile uint32_t r12;
	volatile uint32_t lr; /* Link register. */
	volatile uint32_t pc; /* Program counter. */
	volatile uint32_t psr;/* Program status register. */

    r0 = pulFaultStackAddress[ 0 ];
    r1 = pulFaultStackAddress[ 1 ];
    r2 = pulFaultStackAddress[ 2 ];
    r3 = pulFaultStackAddress[ 3 ];

    r12 = pulFaultStackAddress[ 4 ];
    lr = pulFaultStackAddress[ 5 ];
    pc = pulFaultStackAddress[ 6 ];
    psr = pulFaultStackAddress[ 7 ];

    /* When the following line is hit, the variables contain the register values. */
    for( ;; );
}

#endif
