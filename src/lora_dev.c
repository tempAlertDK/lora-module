#include "lora_dev.h"

#include <assert.h>
#include <string.h>

static bool lora_dev_used(lora_dev_t *dev) {
	return dev->flags & LORA_DEV_FLAGS_USED;
}

void lora_dev_set_used(lora_dev_t *dev) {
	dev->flags |= LORA_DEV_FLAGS_USED;
}

lora_dev_t *lora_dev_lookup_serial(uint8_t serial[8]) {
	for (int i = 0; i < LORA_DEV_MAX_COUNT; i++) {
		lora_dev_t *dev = &lora_devset[i];
		if (lora_dev_used(dev) && memcmp(dev->serial, serial, 8) == 0) {
			return dev;
		}
	}
	return NULL;
}

lora_dev_t *lora_dev_lookup_addr(uint16_t addr) {
	assert(addr < LORA_DEV_MAX_COUNT);
	lora_dev_t *dev = &lora_devset[addr];
	if (lora_dev_used(dev)) {
		return dev;
	}
	return NULL;
}

lora_dev_t *lora_dev_get_empty(void) {
	for (int i = 0; i < LORA_DEV_MAX_COUNT; i++) {
		lora_dev_t *dev = &lora_devset[i];
		if (!lora_dev_used(dev)) {
			memset(dev, 0, sizeof(lora_dev_t));
			dev->addr = i;
			return dev;
		}
	}
	return NULL;
}

// If we already have a downlink for this dev, return it. Otherwise return
// an empty slot, or NULL if none.
static lora_dev_pending_downlink_t *lora_dev_get_avail_downlink(lora_dev_t *dev) {
	lora_dev_pending_downlink_t *empty_slot = NULL;
	for (int i = 0; i < LORA_DEV_MAX_PEND_DOWNLINKS; i++) {
		lora_dev_pending_downlink_t *pend = &lora_dev_downlinks[i];
		if (pend->len == 0) {
			empty_slot = pend;
		} else if (pend->dev_addr == dev->addr) {
			return pend;
		}
	}
	return empty_slot;
}

bool lora_dev_set_downlink(lora_dev_t *dev, uint8_t *data, uint8_t len) {
	assert(len > 0);
	assert(len < LORA_MAX_DOWNLINK_LEN);
	lora_dev_pending_downlink_t *downlink = lora_dev_get_avail_downlink(dev);
	if (!downlink) {
		return false;
	}
	memcpy(downlink->payload, data, len);
	downlink->len = len;
	downlink->dev_addr = dev->addr;
	return true;
}

lora_dev_pending_downlink_t *lora_dev_get_downlink(lora_dev_t *dev) {
	lora_dev_pending_downlink_t *downlink = lora_dev_get_avail_downlink(dev);
	if (!downlink) {
		return NULL;
	} else if (downlink->len == 0) {
		return NULL;
	}
	return downlink;
}

void lora_dev_downlink_clear(lora_dev_pending_downlink_t *downlink) {
	if (downlink) {
		downlink->len = 0;
	}
}
