/*
 * Created on: May 6, 2017
 *     Author: shawnlewis
 *
 */

#include <stdint.h>
#include <string.h>
#include "SEGGER_RTT.h"
#include "FreeRTOS.h"
#include "nrf_gpio.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "portmacro_cmsis.h"
#include "crc16.h"

#include "loraradio.h"
#include "bufpack.h"
#include "util.h"
#include "memtrace.h"

#include "loragw.h"
#include "lora_sched.h"
#include "lora_dev.h"

typedef enum {
	LORAGW_COMMAND_DOWNLINK
} loragw_command_type_t;

typedef struct {
	loragw_command_type_t type;
	uint8_t dev_serial[8];
	uint8_t payload[LORA_MAX_DOWNLINK_LEN];
	int payload_len;
} loragw_command_t;

static QueueHandle_t commandQueue = NULL;
#define COMMANDQUEUE_LENGTH 1
static uint8_t commandQueue_buffer[COMMANDQUEUE_LENGTH * sizeof(loragw_command_t)];
static StaticQueue_t commandQueue_struct;

QueueHandle_t loragw_notifyQueue = NULL;
#define LORAGW_NOTIFYQUEUE_LENGTH		3
#define LORAGW_NOTIFYQUEUE_ITEM_SIZE	sizeof(loragw_notify_t)
static uint8_t loragw_notifyQueue_buffer[LORAGW_NOTIFYQUEUE_LENGTH * LORAGW_NOTIFYQUEUE_ITEM_SIZE];
static StaticQueue_t loragw_notifyQueue_struct;


static void make_beacon_packet(uint8_t *buf, uint32_t gateway_id, int beacon_seq) {
	bufpack_t bp;
	bufpack_init(&bp, buf, LORA_BEACON_LEN);
	bufpack_write3(&bp, gateway_id);
	bufpack_write2(&bp, beacon_seq);
	bufpack_write2(&bp, crc16_compute(buf, 5, NULL));
}

static void loragw_send_beacon(loragw_params_t *gw_params, uint8_t *frame_buf, TickType_t *beacon_ticks, int *beacon_seq) {
	const loraradio_params_t beacon_params = lora_beacon_params(*beacon_seq);
	make_beacon_packet(frame_buf, gw_params->id, *beacon_seq);

	loraradio_prepare_tx(&beacon_params);
	util_delayUntilAndUpdateBase(beacon_ticks, LORA_BEACON_PERIOD_MS);

	MEMTRACE_EVENT0(MEMTRACE_EV_BEACON_TX_START);
#if defined(LORA_DEBUG_USING_GPIO) && (LORA_DEBUG_USING_GPIO == 1)
	nrf_gpio_pin_set(30);
#endif
	loraradio_tx_sync();
#if defined(LORA_DEBUG_USING_GPIO) && (LORA_DEBUG_USING_GPIO == 1)
	nrf_gpio_pin_clear(30);
#endif
	MEMTRACE_EVENT0(MEMTRACE_EV_BEACON_TX_END);

	printf("Beacon sent\r\n");
	// TODO: is this correct?
	(*beacon_seq)++;
}

static void loragw_accept_commands(void) {
	// TODO: process all incoming commands at once?
	loragw_command_t command;
	if (!xQueueReceive(commandQueue, &command, 0)) {
		return;
	}
	switch (command.type) {
	case LORAGW_COMMAND_DOWNLINK: ;
		// NOTE: payload_len set_downlink asserts on payload_len too long.
		lora_dev_t *dev = lora_dev_lookup_serial(command.dev_serial);
		if (dev) {
			lora_dev_set_downlink(dev, (uint8_t *)&command.payload, command.payload_len);
		} else {
			printf("Don't have requested downlink dev\r\n");
		}
		break;
	}
}

static void loragw_do_downlink(uint8_t *frame_buf, lora_sched_entry_t *downlink_entry, lora_slot_t slot) {
	lora_dev_t *dev = lora_dev_lookup_addr(downlink_entry->dev_addr);
	if (dev) {
		lora_dev_pending_downlink_t *downlink = lora_dev_get_downlink(dev);
		uint8_t *payload = (uint8_t *)"";
		int payload_len = 0;
		if (downlink) {
			payload = downlink->payload;
			payload_len = downlink->len;
			lora_dev_downlink_clear(downlink);
		}
		int packet_len = make_data_packet(
			frame_buf, downlink_entry->dev_addr, payload, payload_len);
		const loraradio_params_t downlink_params = lora_downlink_params(
			slot.index, packet_len);
		//printf("  TX offset: %u freq: %u sf: %u\r\n", (unsigned int)xTaskGetTickCount() - beacon_ticks, downlink_params.freq, downlink_params.spreading_factor);
		loraradio_prepare_tx(&downlink_params);
		util_delayUntil(slot.beacon_time, slot.offset_time + 10);
#if defined(LORA_DEBUG_USING_GPIO) && (LORA_DEBUG_USING_GPIO == 1)
		nrf_gpio_pin_set(30);
#endif
		loraradio_tx_sync();
#if defined(LORA_DEBUG_USING_GPIO) && (LORA_DEBUG_USING_GPIO == 1)
		nrf_gpio_pin_clear(30);
#endif
	} else {
		printf("Downlink scheduled, but no dev in table: %u\r\n", downlink_entry->dev_addr);
	}
}

static void loragw_do_uplink(lora_slot_t slot) {
	const loraradio_params_t uplink_params = lora_uplink_params(slot.index, 0);
	util_delayUntil(slot.beacon_time, slot.offset_time - 2);

	printf("  RX offset: %u freq: %u sf: %u\r\n",
		(unsigned int)(xTaskGetTickCount() - slot.beacon_time),
		(unsigned int)uplink_params.freq,
		(unsigned int)uplink_params.spreading_factor);

	//if (next_slot_idx == 3 || next_slot_idx == 45) {
	//	nrf_gpio_pin_set(30);
	//}
	loraradio_result_t uplink = loraradio_rx_sync(&uplink_params);
	//if (next_slot_idx == 3 || next_slot_idx == 45) {
	//	nrf_gpio_pin_clear(30);
	//}

	if (uplink.data_len) {
		// parse uplink
		lora_packet_data_t packet = parse_data_packet(uplink.data, uplink.data_len);
		if (packet.result) {
			printf("Uplink packet error: %d\r\n", packet.result);
		} else {
			lora_dev_t *dev = lora_dev_lookup_addr(packet.dev_addr);
			if (dev) {
				// schedule downlink slot
				lora_sched_add_downlink(
					packet.dev_addr,
					slot.index + LORA_DOWNLINK_SLOT_DELTA);

				// notify
				loragw_notify_t notify = {
					.dev_addr = dev->addr,
					.rssi = uplink.rssi,
					.snr = uplink.snr,
					.payload_len = packet.payload_len,
				};
				memcpy(&notify.dev_serial, &dev->serial, 8);
				int payload_len = packet.payload_len;
				if (payload_len > LORA_MAX_UPLINK_LEN) {
					payload_len = LORA_MAX_UPLINK_LEN;
					printf("Warning: truncating long uplink payload\r\n");
				}
				memcpy(&notify.payload, packet.payload, payload_len);
				int ret = xQueueSend(loragw_notifyQueue, &notify, 0);
				if (!ret) {
					APP_ERROR_HANDLER(NRF_ERROR_INVALID_STATE);
				}
			} else {
				printf("Received packet for unjoined dev: %u\r\n", packet.dev_addr);
			}
		}
	}
}

bool loragw_send_downlink(uint8_t dev_serial[8], uint8_t *payload, int payload_len) {
	if (payload_len > LORA_MAX_DOWNLINK_LEN) {
		return false;
	}
	loragw_command_t command = {
		.payload_len = payload_len
	};
	memcpy(&command.dev_serial, dev_serial, 8);
	memcpy(&command.payload, payload, payload_len);
	xQueueSend(commandQueue, &command, portMAX_DELAY);
	return true;
}

void loragw_full_thread(void *arg) {
	loragw_params_t *gw_params = arg;

	uint8_t *frame_buf = sxradio_get_frame_buf();
	int beacon_seq = 0;

	TickType_t start_ticks = xTaskGetTickCount();
	// We have to start beacon_time at what the previous beacon time would
	// have been.
	TickType_t beacon_ticks = start_ticks - pdMS_TO_TICKS(LORA_BEACON_PERIOD_MS) + pdMS_TO_TICKS(10);

	while(1) {
		loragw_send_beacon(gw_params, frame_buf, &beacon_ticks, &beacon_seq);

		while (1) {
			loragw_accept_commands();
			lora_slot_t next_slot = lora_slot(
				beacon_ticks, xTaskGetTickCount(), 1);
			if (!lora_slot_ok(next_slot)) {
				break;
			}
			//printf("Beacon_ticks: %u Ticks: %d Slot: %d Offset: %d Delta: %d\r\n",
			//	beacon_ticks, cur_ticks, next_slot_idx, next_slot_tick_offset,
			//	(beacon_ticks + next_slot_tick_offset) - cur_ticks);
			lora_sched_entry_t *downlink_entry = lora_sched_pop(next_slot.index);
			if (downlink_entry) {
				loragw_do_downlink(frame_buf, downlink_entry, next_slot);
			} else {
				loragw_do_uplink(next_slot);
			}
		}
	}
}


void loragw_full_init(void) {

	commandQueue = xQueueCreateStatic(
			COMMANDQUEUE_LENGTH,
			sizeof(loragw_command_t),
			commandQueue_buffer,
			&commandQueue_struct);

	if (commandQueue == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}


	// Auto-join a device with address 0.
	lora_dev_t *dev = lora_dev_get_empty();
	lora_dev_set_used(dev);
	uint8_t serial[8] = {0xa, 0xb, 0xc, 0xd, 1, 2, 3, 4};
	memcpy(dev->serial, &serial, 8);

	// TODO: ensure this is sized to ensure we never block on pushing into
	//     it.
	loragw_notifyQueue = xQueueCreateStatic(
			LORAGW_NOTIFYQUEUE_LENGTH,
			LORAGW_NOTIFYQUEUE_ITEM_SIZE,
			loragw_notifyQueue_buffer,
			&loragw_notifyQueue_struct);

	if (loragw_notifyQueue == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	// Eth IRQn pin for debugging
	nrf_gpio_pin_clear(30);
	nrf_gpio_cfg_output(30);
}
