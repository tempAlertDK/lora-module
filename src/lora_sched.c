#include "FreeRTOS.h"

#include "lora_sched.h"

static lora_sched_entry_t lora_sched[LORA_SCHED_SIZE];
static int write_idx = 0;
static int read_idx = 0;

void lora_sched_add_downlink(uint16_t dev_addr, int slot_idx) {
	lora_sched[write_idx].dev_addr = dev_addr;
	lora_sched[write_idx].slot_idx = slot_idx;
	write_idx++;
	if (write_idx == LORA_SCHED_SIZE) {
		write_idx = 0;
	}
	if (write_idx == read_idx) {
		APP_ERROR_HANDLER(NRF_ERROR_INVALID_STATE);
	}
}

lora_sched_entry_t *lora_sched_pop(int slot_idx) {
	int size = write_idx - read_idx;
	if (size < 0) {
		size += LORA_SCHED_SIZE;
	}
	if (size > 0 && lora_sched[read_idx].slot_idx == slot_idx) {
		lora_sched_entry_t *ret = &lora_sched[read_idx];
		read_idx++;
		if (read_idx == LORA_SCHED_SIZE) {
			read_idx = 0;
		}
		return ret;
	} else {
		return NULL;
	}
}
