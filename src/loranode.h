/*
 * Created on: May 6, 2017
 *     Author: shawnlewis
 *
 */

#pragma once

typedef struct {
	uint32_t id;
} loragw_params_t;

typedef enum {
	LORANODE_EVENT_ACQUIRED,
	LORANODE_EVENT_DOWNLINK
} loranode_event_type_t;

typedef struct {
	loranode_event_type_t type;
	loraradio_result_t result;
} loranode_event_t;

QueueHandle_t loranode_eventQueue;

void loranode_init(void);
void loranode_thread(void *arg);
void loranode_uplink(uint8_t *data, uint8_t data_len);
void loranode_acquire(void);
void loranode_stop(void);
