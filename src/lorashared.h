#pragma once

#include "loraradio.h"

#define LORA_BEACON_LEN 7
#define LORA_SLOT_LEN_MS 128
#define LORA_NUM_SLOTS 64
#define LORA_BEACON_PERIOD_MS (LORA_SLOT_LEN_MS * LORA_NUM_SLOTS)
#define LORA_MAX_BEACON_MISSES 5

// Downlink window begins this many slots after uplink begin slot.
#define LORA_DOWNLINK_SLOT_DELTA 8

// These are max payload lengths.
#define LORA_MAX_DOWNLINK_LEN 32
#define LORA_MAX_UPLINK_LEN 32

// Amount of time before beacon window that we don't allow transmits.
// Currently set so that any uplink has time for a downlink slot before
// the beacon.
#define LORA_BEACON_KEEPOUT_MS \
	((LORA_DOWNLINK_SLOT_DELTA + 3) * LORA_SLOT_LEN_MS)
// Number of slots always kept free for beacon transmissions and broadcasts.
#define LORA_BEACON_RESERVED_SLOTS 4

typedef struct {
	int index;
	TickType_t beacon_time;
	TickType_t offset_time;
} lora_slot_t;

lora_slot_t lora_slot(TickType_t beacon_time, TickType_t time, int delta);
bool lora_slot_ok(lora_slot_t slot);

loraradio_params_t lora_beacon_params(int beacon_seq);
loraradio_params_t lora_uplink_params(int slot_idx, uint8_t data_len);
loraradio_params_t lora_downlink_params(int slot_idx, uint8_t data_len);

