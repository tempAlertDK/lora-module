/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */
#ifndef CELL_NODE_REV1P0_H
#define CELL_NODE_REV1P0_H


#define CELL_TXD_PIN  			2
#define CELL_RXD_PIN  			3

#define CSN_SEL2_PIN			4
#define CSN_SEL1_PIN			5

#define SCLK_PIN				6
#define MOSI_PIN				7
#define MISO_PIN				8

#define NFC1_PIN				9
#define NFC2_PIN				10

#define IRQN_PIN 				11

#define SCL_PIN		    		12
#define SDA_PIN     			13

#if !defined(TRACE)
#define LED1_PIN				14
#define RED_LED_PIN 			LED1_PIN
#define LED2_PIN				15
#define GREEN_LED_PIN			LED2_PIN
#define ACPG_PIN				16
#endif

#define CSN_SEL0_PIN			17

#if !defined(TRACE)
#define PIEZO_PIN				18
#endif

#define EXPANSION_IRQN_PIN		19

#if !defined(TRACE)
#define CELL_FAST_SHDN_PIN		20
#endif

#define RESETN_PIN				21

#define EXTSENSOREN_PIN			22
#define INTSENSORENN_PIN		23

#define BUTTON_PIN				24

#define FEM_TX_EN_PIN			25
#define FEM_RX_EN_PIN			26

#if defined(MODEM_POPULATED) && (MODEM_POPULATED == 1)
#define CELL_RTS_PIN 			27
#define CELL_CTS_PIN			28
#else
#define FEM_BYPASS_PIN			27
#define FEM_ANT_SEL_PIN			28
#endif

#define CSN_ENN_PIN				29

#define ETHERNET_IRQN_PIN		30

#define VBAT_SENSE_PIN			31


/* LED definitions to support Nordic BSP */
#define LEDS_NUMBER    2

#define LED_START      RED_LED_PIN
#define LED_STOP       GREEN_LED_PIN

#define LEDS_LIST { RED_LED_PIN, GREEN_LED_PIN }

#define BSP_LED_0      RED_LED_PIN
#define BSP_LED_1      GREEN_LED_PIN

#define BSP_LED_0_MASK (1<<BSP_LED_0)
#define BSP_LED_1_MASK (1<<BSP_LED_1)

#define LEDS_MASK      (BSP_LED_0_MASK | BSP_LED_1_MASK )
#define LEDS_INV_MASK  ~LEDS_MASK


/* Button definitions to support Nordic BSP */
#define BUTTONS_NUMBER 1

#define BTN_ID_SLEEP 0

#define BUTTON_START   BUTTON_PIN
#define BUTTON_STOP    BUTTON_PIN
#define BUTTON_PULL    NRF_GPIO_PIN_NOPULL

#define BUTTONS_LIST { BUTTON_PIN }

#define BSP_BUTTON_0   BUTTON_PIN

#define BSP_BUTTON_0_MASK (1<<BSP_BUTTON_0)

#define BUTTONS_MASK   (BSP_BUTTON_0_MASK)



/* I2C addresses for the MCP23008 port expanders*/
#define  MCP23008_CELL_I2C_ADDR		0x22
#define  MCP23008_EPD_I2C_ADDR		0x21
#define  MCP23008_LORA_I2C_ADDR		0x20

/* Pin definitions for the cellular MCP23008 port expander */
#define CELL_IGNITION_EXPANDER_PIN	(1<<0)
#define CELL_STATUSN_EXPANDER_PIN	(1<<1)
#define LDOEN_EXPANDER_PIN			(1<<2)
#define FEM_ANT_SEL_EXPANDER_PIN	(1<<3)
#define FEM_BYPASS_EXPANDER_PIN		(1<<4)
// GP5 is unused
#define CELL_POWER_EXPANDER_PIN		(1<<6)
#define CELL_EMRG_RST_EXPANDER_PIN	(1<<7)

/* Pin definitions for the ePaper MCP23008 port expander */
#define EPD_DISCHARGE_EXPANDER_PIN		(1<<0)
#define EPD_BUSY_EXPANDER_PIN			(1<<1)
#define EPD_RESETN_EXPANDER_PIN			(1<<2)
#define EPD_ONN_EXPANDER_PIN			(1<<3)
#define EPD_BORDER_CTRLN_EXPANDER_PIN	(1<<4)

/* Pin definitions for the LoRa MCP23008 port expander */
#define LORA_RESETN_EXPANDER_PIN	(1<<0)
#define LORA_DIO0_EXPANDER_PIN		(1<<1)
#define LORA_DIO1_EXPANDER_PIN		(1<<2)
#define LORA_DIO2_EXPANDER_PIN		(1<<3)
#define LORA_DIO3_EXPANDER_PIN		(1<<4)
#define LORA_DIO4_EXPANDER_PIN		(1<<5)
#define LORA_DIO5_EXPANDER_PIN		(1<<6)
#define LORA_RFENN_EXPANDER_PIN		(1<<7)


// Low frequency clock source to be used by the SoftDevice
#define NRF_CLOCK_LFCLKSRC      {.source        = NRF_CLOCK_LF_SRC_XTAL,            \
                                 .rc_ctiv       = 0,                                \
                                 .rc_temp_ctiv  = 0,                                \
                                 .xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_20_PPM}

#endif // CELL_NODE_REV1P0_H
