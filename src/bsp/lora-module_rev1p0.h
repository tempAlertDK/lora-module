/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */
#ifndef LORA_MODULE_REV1P0_H
#define LORA_MODULE_REV1P0_H


#define LORA_DIO0				27
#define LORA_DIO3				28
#define LORA_DIO1_DIO2			30

#define LORA_FEM_CPS			2
#define LORA_FEM_CSD			3
#define LORA_RESETN				4

#define LORA_CSN_PIN			5

#define SCLK_PIN				6
#define MOSI_PIN				7
#define MISO_PIN				8

#define NFC2_PIN				9
#define NFC1_PIN				10

#define CONFIG_NFCT_PINS_AS_GPIOS

/* todo: gone I believe*/
#define IRQN_PIN 				11

#define SCL_PIN		    		12
#define SDA_PIN     			13

#if !defined(TRACE)
#define LED1_PIN				14
#define RED_LED_PIN 			LED1_PIN
#define LED2_PIN				15
#define GREEN_LED_PIN			LED2_PIN
#define ACPG_VBATEN_PIN			16
#endif

#define FLASH_RESETN_PIN			17

#define FEM_BYPASS_PIN			19


#define RESETN_PIN				21

#define FEM_RX_EN_PIN			22
#define FEM_TX_EN_PIN			23

#define BUTTON_PIN				24

/* todo: change these names, decide how to treat GPIO1:GPIO4*/
#define GPIO1					25
#define GPIO2 					26
#define GPIO3					20
#define GPIO4					18

#define EXT_IRQN_PIN 			27

/*todo: add individual LoRa pins*/
#define ETHERNET_MISO_IRQN_PIN	30

#define VBAT_SENSE_PIN			31

/* LED definitions to support Nordic BSP */
#define LEDS_NUMBER    2

#define LED_START      RED_LED_PIN
#define LED_STOP       GREEN_LED_PIN

#define LEDS_LIST { RED_LED_PIN, GREEN_LED_PIN }

#define BSP_LED_0      RED_LED_PIN
#define BSP_LED_1      GREEN_LED_PIN


#define BSP_LED_0_MASK (1<<BSP_LED_0)
#define BSP_LED_1_MASK (1<<BSP_LED_1)

#define LEDS_MASK      (BSP_LED_0_MASK | BSP_LED_1_MASK )

#define LEDS_INV_MASK  ~LEDS_MASK


/* Button definitions to support Nordic BSP */
#define BUTTONS_NUMBER 		3
#define BTN_ID_SLEEP 		0
#define BTN_ID_TEST1		1
#define BTN_ID_TEST2		2

//#define BUTTON_START   NFC1_PIN
//#define BUTTON_STOP    BUTTON_PIN
#define BUTTON_PULL    NRF_GPIO_PIN_NOPULL

#define BUTTONS_LIST { BUTTON_PIN, NFC1_PIN, NFC2_PIN }

#define BSP_BUTTON_0   BUTTON_PIN
#define BSP_BUTTON_1   NFC1_PIN
#define BSP_BUTTON_2   NFC2_PIN

#define BSP_BUTTON_0_MASK (1<<BSP_BUTTON_0)
#define BSP_BUTTON_1_MASK (1<<BSP_BUTTON_1)
#define BSP_BUTTON_2_MASK (1<<BSP_BUTTON_2)

#define BUTTONS_MASK   (BSP_BUTTON_0_MASK | BSP_BUTTON_1_MASK | BSP_BUTTON_2_MASK)



/* I2C address for the MCP23008 port expanders*/
//#define  MCP23008_CELL_I2C_ADDR		0x22
//#define  MCP23008_EPD_I2C_ADDR		0x21
//#define  MCP23008_LORA_I2C_ADDR		0x20

/* Pin definitions for the cellular MCP23008 port expander */
//#define CELL_IGNITION_EXPANDER_PIN	(1<<0)
//#define CELL_STATUSN_EXPANDER_PIN	(1<<1)
//#define ETHERNETEN_EXPANDER_PIN		(1<<2)
//#define FEM_BYPASS_EXPANDER_PIN		(1<<3)
//#define LDOEN_EXPANDER_PIN			(1<<4)
//#define BUCKMODE_EXPANDER_PIN		(1<<5)
//#define CELL_POWER_EXPANDER_PIN		(1<<6)
//#define CELL_EMRG_RST_EXPANDER_PIN	(1<<7)

/* Pin definitions for the ePaper MCP23008 port expander */
//#define EPD_DISCHARGE_EXPANDER_PIN		(1<<0)
//#define EPD_BUSY_EXPANDER_PIN			(1<<1)
//#define EPD_RESETN_EXPANDER_PIN			(1<<2)
//#define EPD_ONN_EXPANDER_PIN			(1<<3)
//#define EPD_BORDER_CTRLN_EXPANDER_PIN	(1<<4)

/* Pin definitions for the LoRa MCP23008 port expander */
//#define LORA_RESETN_EXPANDER_PIN	(1<<0)
//#define LORA_DIO0_EXPANDER_PIN		(1<<1)
//#define LORA_DIO1_EXPANDER_PIN		(1<<2)
//#define LORA_DIO2_EXPANDER_PIN		(1<<3)
//#define LORA_DIO3_EXPANDER_PIN		(1<<4)
//#define LORA_DIO4_EXPANDER_PIN		(1<<5)
//#define LORA_FEM_CPS_EXPANDER_PIN	(1<<6)
//#define LORA_FEM_CSD_EXPANDER_PIN	(1<<7)

// Low frequency clock source to be used by the SoftDevice
#define NRF_CLOCK_LFCLKSRC      {.source        = NRF_CLOCK_LF_SRC_XTAL,            \
                                 .rc_ctiv       = 0,                                \
                                 .rc_temp_ctiv  = 0,                                \
                                 .xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_20_PPM}

#endif // CELL_NODE_REV1P1_H

