#pragma once

#include "FreeRTOS.h"

typedef unsigned char      bit_t;
typedef unsigned char      u1_t;
typedef   signed char      s1_t;
typedef unsigned short     u2_t;
typedef          short     s2_t;
typedef unsigned int       u4_t;
typedef          int       s4_t;
typedef unsigned long long u8_t;
typedef          long long s8_t;
typedef unsigned int       uint;
typedef const char* str_t;
typedef              u1_t* xref2u1_t;
typedef const        u1_t* xref2cu1_t;

enum _cr_t { CR_4_5=0, CR_4_6, CR_4_7, CR_4_8 };
enum _sf_t { FSK=0, SF7, SF8, SF9, SF10, SF11, SF12, SFrfu };
enum _bw_t { BW125=0, BW250, BW500, BWrfu };
typedef u1_t cr_t;
typedef u1_t sf_t;
typedef u1_t bw_t;

enum { RXMODE_SINGLE, RXMODE_SCAN, RXMODE_RSSI };

#define AES_ENC       0x00 
// Defined in lmic/aes.c
u4_t os_aes (u1_t mode, xref2u1_t buf, u2_t len);

typedef void (*sxradio_interrupt_cb_t)(int is_tx, TickType_t done_ticks, const uint8_t *data, uint8_t dataLen, uint8_t rssi, uint8_t snr, uint32_t freqerr);

extern sxradio_interrupt_cb_t sxradio_interrupt_cb;

void sxradio_init ();
void sxradio_preparetx (u4_t freq, s1_t txpow, sf_t sf, bw_t bw, cr_t cr, int ih, int nocrc, u1_t dataLen);
void sxradio_starttx (void);
void sxradio_startrx (u1_t rxmode, u4_t freq, sf_t sf, bw_t bw, cr_t cr, int ih, int nocrc, u1_t noRXIQinversion, u1_t rxsyms, u1_t dataLen);
void sxradio_sleep(void);
void sxradio_standby(void);
uint8_t *sxradio_get_frame_buf(void);

void sxradio_setContinuousMode(bool continuousMode);
