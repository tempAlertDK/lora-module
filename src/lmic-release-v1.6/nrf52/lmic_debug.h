/*
 * lmic_debug.h
 *
 *  Created on: Feb 23, 2017
 *      Author: kwgilpin
 */

#ifndef SRC_LMIC_RELEASE_V1_6_NRF52_LMIC_DEBUG_H_
#define SRC_LMIC_RELEASE_V1_6_NRF52_LMIC_DEBUG_H_

#include <stdint.h>
#include <stdbool.h>

#include "oslmic.h"

void debug_init();
void debug_led(int val);
void debug_char(char c);
void debug_hex (u1_t b);
void debug_buf (const u1_t* buf, int len);
void debug_uint (u4_t v);
void debug_int (s4_t v);
void debug_str (const char* str);
void debug_val (const char* label, u4_t val);
void debug_valdec (const char* label, s4_t val);
int debug_fmt (char* buf, int max, s4_t val, int base, int width, char pad);

void debug_event (int ev);

#endif /* SRC_LMIC_RELEASE_V1_6_NRF52_LMIC_DEBUG_H_ */
