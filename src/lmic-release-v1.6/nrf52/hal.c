/*
 * hal.c
 *
 *  Created on: Feb 20, 2017
 *      Author: kwgilpin
 */


#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "nrf52.h"
#include "cmsis_gcc.h"

#include "nrf_rtc.h"

#include "nrf_drv_spi.h"
#include "nrf_drv_rtc.h"
#include "nrf_drv_clock.h"

#if (1) // kwg
#include "nrf_gpio.h"
#endif

#include "app_util_platform.h"

#include "FreeRTOS.h"
#include "semphr.h"
#include "portmacro_cmsis.h"

#include "boards.h"
#include "twi_mtx.h"
#include "irqn.h"
#include "mcp23008.h"
#include "spi.h"
#include "oslmic.h"
#include "hal.h"

static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(1);
static volatile bool m_spi_xfer_done = false;

static SemaphoreHandle_t sleep_semaphore;
static StaticSemaphore_t sleep_semaphore_buffer;

QueueHandle_t dioIrqQueue = NULL;

static int irqlevel = 0;
static u4_t overflows = 0;

/* Declare an instance of nrf_drv_rtc for RTC2. */
const nrf_drv_rtc_t rtc = NRF_DRV_RTC_INSTANCE(2);

nrf_drv_spi_config_t spi_config = {
	.sck_pin = SCLK_PIN,
	.mosi_pin = MOSI_PIN,
	.miso_pin = MISO_PIN,
	.ss_pin = NRF_DRV_SPI_PIN_NOT_USED,
	.irq_priority = APP_IRQ_PRIORITY_HIGH,
	.orc = 0x00,
	.frequency = NRF_DRV_SPI_FREQ_1M,
	.mode = NRF_DRV_SPI_MODE_0,
	.bit_order = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST
};

static void rtc_handler(nrf_drv_rtc_int_type_t int_type);

static void dio_irq_handler(const irq_params_t *p_params);

static void spi_event_handler(nrf_drv_spi_evt_t const * p_event);

void hal_init() {
	uint32_t err_code;

#if (0) // kwg
	// Use ETHERNET-IRQn to signal that we're actively receiving
	nrf_gpio_pin_clear(30);
	nrf_gpio_cfg_output(30);

	// Use EXPANSION-IRQn to indicate when nRF saves LMIC.txend
	nrf_gpio_pin_clear(19);
	nrf_gpio_cfg_output(19);

#endif
#if defined(BOARD_LORA_MODULE_REV1P0)
	nrf_gpio_cfg_output(LORA_FEM_CSD);
	nrf_gpio_cfg_output(LORA_FEM_CPS);
	nrf_gpio_cfg_output(LORA_RESETN);
	nrf_gpio_cfg_output(LORA_CSN_PIN);
#endif


#if defined(BOARD_CELL_NODE_REV1P0)
	// Enable the RX/TX antenna mux
	WITH_TWI_MTX() {
		mcp23008_clearPins(MCP23008_LORA_I2C_ADDR, LORA_RFENn_PIN);
	}

#elif defined(BOARD_CELL_NODE_REV1P1)
	WITH_TWI_MTX() {
		/* Initialize the pins that drive the LoRa FEM--for now keep it in shutdown */
		mcp23008_clearPins(MCP23008_LORA_I2C_ADDR, LORA_FEM_CSD_EXPANDER_PIN | LORA_FEM_CPS_EXPANDER_PIN);
	}
#elif defined(BOARD_LORA_MODULE_REV1P0)
	nrf_gpio_pin_clear(LORA_FEM_CSD);
#endif

    /*
     * Create a semaphore which is used to wake the LMIC OS from sleep.
     */
    sleep_semaphore = xSemaphoreCreateBinaryStatic(&sleep_semaphore_buffer);

    /*
     * RTC Config
     */

    // Ensure that the 32.768kHz clock is running
    //err_code =  nrf_drv_clock_init();
	//APP_ERROR_CHECK(err_code);
    nrf_drv_clock_lfclk_request(NULL);

    // Initialize RTC instance
    err_code = nrf_drv_rtc_init(&rtc, NULL, rtc_handler);
    APP_ERROR_CHECK(err_code);

    // Enable overflow event & interrupt
    nrf_drv_rtc_overflow_enable(&rtc, true);

    //Power on RTC instance
    nrf_drv_rtc_enable(&rtc);

    /*
     * Interrupt config
     */

#ifndef BOARD_LORA_MODULE_REV1P0 //todo:
	irq_params_t irq_params;

	irq_params.source = IRQ_SRC_LORA;
	irq_params.subtype = IRQ_SUBTYPE_RISING_EDGE;

	irq_params.type = IRQ_TYPE_MCP23008_PIN1_CHANGE;
	irqn_registerCallback(&irq_params, dio_irq_handler);

	irq_params.type = IRQ_TYPE_MCP23008_PIN2_CHANGE;
	irqn_registerCallback(&irq_params, dio_irq_handler);

	irq_params.type = IRQ_TYPE_MCP23008_PIN3_CHANGE;
	irqn_registerCallback(&irq_params, dio_irq_handler);
#endif
}

void rtc_handler(nrf_drv_rtc_int_type_t int_type) {
	static BaseType_t xHigherPriorityTaskWoken;

	xHigherPriorityTaskWoken = pdFALSE;

    if (int_type == NRF_DRV_RTC_INT_OVERFLOW) {
        overflows++;
    } else if (int_type == NRF_DRV_RTC_INT_COMPARE0) {
    	xSemaphoreGiveFromISR(sleep_semaphore, &xHigherPriorityTaskWoken);
    }

    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

extern void radio_irq_handler(u1_t dio);

void dio_irq_handler(const irq_params_t *p_params) {
	// TODO: Check interrupt priority of the GPIOTE system

	if (p_params->source != IRQ_SRC_LORA) {
		return;
	}

	if (p_params->subtype != IRQ_SUBTYPE_RISING_EDGE) {
		return;
	}

	hal_disableIRQs();

	switch (p_params->type) {
	case IRQ_TYPE_MCP23008_PIN1_CHANGE:
		printf("LORA: DIO0 interrupt\r\n");
		radio_irq_handler(0);
		break;
	case IRQ_TYPE_MCP23008_PIN2_CHANGE:
		printf("LORA: DIO1 interrupt\r\n");
		radio_irq_handler(1);
		break;
	case IRQ_TYPE_MCP23008_PIN3_CHANGE:
		printf("LORA: DIO2 interrupt\r\n");
		radio_irq_handler(2);
		break;
	default:
		printf("LORA: unexpected interrupt\r\n");
	}

	hal_enableIRQs();

	/*
	 * The LMIC OS may be sleeping (i.e. waiting for the semaphore in
	 * hal_sleep), so we need to give the semaphore to wake the OS.
	 */
	xSemaphoreGive(sleep_semaphore);
}

void hal_failed() {
	printf("ERROR: LMIC hal_failed\r\n");
	while(1);
}

void hal_pin_rxtx(u1_t val) {
	/*
	 * The radio controls the state of the antenna switch.  We only have to
	 * enable the switch (which we do in hal_init above).
	 */
}


void hal_pin_nss(u1_t val) {
	if (val == 0) {
		spi_init(&spi_config, NULL);
#ifndef BOARD_LORA_MODULE_REV1P0
		spi_csmux_select(LORA_CSN);//dk_todo: remove mux, straightforward pin set
		nrf_gpio_pin_clear(CSN_ENN_PIN);
#else
		nrf_gpio_pin_clear(LORA_CSN_PIN);
#endif
	} else {
#ifndef BOARD_LORA_MODULE_REV1P0
		nrf_gpio_pin_set(CSN_ENN_PIN);
#endif
		nrf_gpio_pin_set(LORA_CSN_PIN);
		spi_deinit();
	}

}

void hal_pin_rst(u1_t val) {
#ifndef BOARD_LORA_MODULE_REV1P0
	WITH_TWI_MTX() {
		switch (val) {
		case 0:
			mcp23008_clearPins(MCP23008_LORA_I2C_ADDR, LORA_RESETN_EXPANDER_PIN);
			break;
		case 1:
			mcp23008_setPins(MCP23008_LORA_I2C_ADDR, LORA_RESETN_EXPANDER_PIN);
			break;
		case 2:
			mcp23008_tristatePins(MCP23008_LORA_I2C_ADDR, LORA_RESETN_EXPANDER_PIN);
			break;
		}
	}
#else
		switch (val) {
		case 0:
			nrf_gpio_pin_clear(LORA_RESETN);
			break;
		case 1:
			nrf_gpio_pin_set(LORA_RESETN);
			break;
		case 2:
			nrf_gpio_cfg_input(LORA_RESETN, NRF_GPIO_PIN_PULLUP);
			break;
		}
	
#endif
}

u1_t hal_spi(u1_t outval) {
	u1_t inbuffer[1];

	m_spi_xfer_done = false;
	nrf_drv_spi_transfer(&spi, &outval, 1, inbuffer, 1);

	while(!m_spi_xfer_done);

	return inbuffer[0];
}

int total_spi_aux = 0;
void hal_spi_aux(const u1_t *tx, u2_t txCount, u1_t *rx, u2_t rxCount) {
	total_spi_aux++;
	m_spi_xfer_done = false;
	nrf_drv_spi_transfer(&spi, tx, txCount, rx, rxCount);

	while(!m_spi_xfer_done);
}

int total_spi_event = 0;
void spi_event_handler(nrf_drv_spi_evt_t const * p_event)
{
	total_spi_event++;
    m_spi_xfer_done = true;
}

void hal_disableIRQs() {

	/*
	 * Disable interrupts with logical priority APP_IRQ_PRIORITY_MID and
	 * lower (i.e. higher priority numbers).  This will disable the RTC2
	 * interrupt but leave the SPI1 and TWI0 interrupts enabled.
	 */
	__set_BASEPRI(APP_IRQ_PRIORITY_MID << (8 - __NVIC_PRIO_BITS));
	irqlevel++;
}

void hal_enableIRQs() {
    ASSERT(irqlevel);

    if (--irqlevel == 0) {
    	__set_BASEPRI(0);
    }
}

void hal_sleep() {
	xSemaphoreTake(sleep_semaphore, portMAX_DELAY);
}
