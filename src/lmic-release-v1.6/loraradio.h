/*
 * Created on: May 4, 2017
 *     Author: shawnlewis
 *
 * This provides an interface to transmit and receive with the LoRa radio (no
 * LoRaWAN). It abstracts away all of the lmic internals.
 *
 */

#pragma once

#include <stdint.h>
#include <stdbool.h>

#include "FreeRTOS.h"
#include "queue.h"

#include "radio.h"

typedef struct {
	uint32_t freq;
	sf_t spreading_factor;
	bw_t bandwidth;
	cr_t coding_rate;
	bool implicit_header;
	bool no_crc;
	uint8_t data_len;

	// tx only
	int8_t tx_pow;
	// rx only
	uint16_t timeout_syms;
} loraradio_params_t;

typedef struct {
	int is_tx;
	TickType_t  done_ticks;

	// rx only
	int16_t rssi;
	uint8_t snr;
	uint32_t freqerr;
	const uint8_t* data;
	uint8_t data_len;
} loraradio_result_t;

extern QueueHandle_t loraradio_commandDone_queue;

void loraradio_init(void);

// Calculate how long a given packet will be on air.
uint32_t loraradio_time_on_air(const loraradio_params_t *params);

// Send data to the radio, but don't transmit it.
void loraradio_prepare_tx(const loraradio_params_t *params);

// Transmit data with the lora radio asynchronously.
void loraradio_tx_async(void);

// Transmit data with the lora radio synchronously.
loraradio_result_t loraradio_tx_sync(void);

// Receive data from the lora radio asynchronously.
void loraradio_rx_async(const loraradio_params_t *params);

// Receive data from the lora radio synchronously.
loraradio_result_t loraradio_rx_sync(const loraradio_params_t *params);

void loraradio_print_rx_signal_info(uint8_t sig_snr, int16_t sig_rssi);
