/*
 * Created on: May 4, 2017
 *     Author: shawnlewis
 *
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "radio.h"

#include "loraradio.h"

#define LORARADIO_TX_TIMEOUT 4000

typedef enum {
	LORARADIO_COMMAND_TX,
	LORARADIO_COMMAND_RX,
	LORARADIO_COMMAND_PREPARE
} loraradio_command_type_t;

static StaticQueue_t loraradio_commandDone_queue_struct;
#define LORARADIO_COMMANDDONE_QUEUE_LENGTH 1
static uint8_t loraradio_commandDone_queue_storage[LORARADIO_COMMANDDONE_QUEUE_LENGTH * sizeof(loraradio_result_t)];

// For now we define our own typed queue item, and share parameters among
// different item types. We may want to use genericQueue here instead.
typedef struct {
	loraradio_command_type_t type;
	const loraradio_params_t *params;
} loraradio_command_t;

QueueHandle_t loraradio_commandDone_queue = NULL;

// based on lmic implementation and not really tested yet.
// returns packet time in ms.
uint32_t loraradio_time_on_air(const loraradio_params_t *params) {
    u1_t plen = params->data_len;
    u1_t bw = params->bandwidth;  // 0,1,2 = 125,250,500kHz
    u1_t sf = params->spreading_factor;  // 0=FSK, 1..6 = SF7..12
    u1_t nocrc = params->no_crc;
    u1_t ih = params->implicit_header;
    u1_t cr = params->coding_rate;
    if( sf == FSK ) {
        return ((uint32_t)plen+/*preamble*/5+/*syncword*/3+/*len*/1+/*crc*/2) * /*bits/byte*/8
            * 1000UL / /*kbit/s*/50000;
    }
    u1_t sfx = 4*(sf+(7-SF7));
    u1_t q = sfx - (sf >= SF11 ? 8 : 0);
    int tmp = 8*plen - sfx + 28 + (nocrc?0:16) - (ih?20:0);
    if( tmp > 0 ) {
        tmp = (tmp + q - 1) / q;
        tmp *= cr+5;
        tmp += 8;
    } else {
        tmp = 8;
    }
    tmp = (tmp<<2) + /*preamble*/49 /* 4 * (8 + 4.25) */;
    // bw = 125000 = 15625 * 2^3
    //      250000 = 15625 * 2^4
    //      500000 = 15625 * 2^5
    // sf = 7..12
    //
    // osticks =  tmp * OSTICKS_PER_SEC * 1<<sf / bw
    //
    // 3 => counter reduced divisor 125000/8 => 15625
    // 2 => counter 2 shift on tmp
    sfx = sf+(7-SF7) - (3+2) - bw;
    int div = 15625;
    if( sfx > 4 ) {
        // prevent 32bit signed int overflow in last step
        div >>= sfx-4;
        sfx = 4;
    }

    // Need 32bit arithmetic for this last step
    return (((uint32_t)tmp << sfx) * 1000UL + div/2) / div;
}

void loraradio_prepare_tx(const loraradio_params_t *params) {
	sxradio_preparetx(
		params->freq,
		params->tx_pow,
		params->spreading_factor,
		params->bandwidth,
		params->coding_rate,
		params->implicit_header,
		params->no_crc,
		params->data_len);
}

void loraradio_tx_async(void) {
	sxradio_starttx();
}

loraradio_result_t loraradio_tx_sync(void) {
	loraradio_tx_async();

	loraradio_result_t result;
	int ret = xQueueReceive(loraradio_commandDone_queue, &result, pdMS_TO_TICKS(LORARADIO_TX_TIMEOUT));
	if (!ret) {
		APP_ERROR_HANDLER(NRF_ERROR_TIMEOUT);
	}

	return result;
}

void loraradio_rx_async(const loraradio_params_t *params) {
	uint8_t rx_mode = RXMODE_SCAN;
	if (params->timeout_syms) {
		rx_mode = RXMODE_SINGLE;
	}
	sxradio_startrx(
		rx_mode,
		params->freq,
		params->spreading_factor,
		params->bandwidth,
		params->coding_rate,
		params->implicit_header,
		params->no_crc,
		1,
		params->timeout_syms,
		params->data_len);
}
loraradio_result_t loraradio_rx_sync(const loraradio_params_t *params) {
	loraradio_rx_async(params);

	loraradio_result_t result;
	xQueueReceive(loraradio_commandDone_queue, &result, portMAX_DELAY);

	return result;
}

void loraradio_print_rx_signal_info(uint8_t sig_snr, int16_t sig_rssi) {
	int16_t snr = (int16_t) sig_snr;
	int16_t rssi = 4 * (-139 + (int16_t) sig_rssi);
	if (snr < 0) {
		rssi -= snr;
	}
	printf("RSSI: %d.%ddBm SNR: %d.%ddB\r\n", rssi / 4, 25 * (rssi % 4), snr / 4, 25 * (snr % 4));
}

static void loraradio_radio_done_cb(int is_tx, TickType_t done_ticks, const uint8_t *data, uint8_t data_len, uint8_t rssi, uint8_t snr, uint32_t freqerr) {
	loraradio_result_t result = {
		.is_tx = is_tx,
		.done_ticks = done_ticks,
		.data = data,
		.data_len = data_len,
		.rssi = rssi,
		.snr = snr,
		.freqerr = freqerr
	};
	int ret = xQueueSend(loraradio_commandDone_queue, &result, 0);
	if (!ret) {
		APP_ERROR_HANDLER(NRF_ERROR_INVALID_STATE);
	}
}

void loraradio_init(void) {
	sxradio_init();

	sxradio_interrupt_cb = loraradio_radio_done_cb;

	loraradio_commandDone_queue = xQueueCreateStatic(
			LORARADIO_COMMANDDONE_QUEUE_LENGTH,
			sizeof(loraradio_result_t),
			loraradio_commandDone_queue_storage,
			&loraradio_commandDone_queue_struct);

	if (loraradio_commandDone_queue == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
}
