/*
 * Created on: May 6, 2017
 *     Author: shawnlewis
 *
 */

#pragma once

#include "loraradio.h"
#include "lorashared.h"
#include "lora_packet.h"

typedef struct {
	uint32_t id;
} loragw_params_t;

typedef struct {
	uint8_t dev_serial[8];
	uint16_t dev_addr;
	int16_t rssi;
	uint8_t snr;
	uint8_t payload[LORA_MAX_DOWNLINK_LEN];
	int payload_len;
} loragw_notify_t;

extern QueueHandle_t loragw_notifyQueue;

bool loragw_send_downlink(uint8_t dev_serial[8], uint8_t *payload, int payload_len);

void loragw_full_init(void);

void loragw_full_thread(void *arg);
