/*
 * loratest.c
 *
 *  Created on: Aug 1, 2017
 *      Author: kwgilpin
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "nrf_error.h"

#include "app_error.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "portmacro_cmsis.h"

#include "boards.h"
#include "mcp23008.h"
#include "twi_mtx.h"

#include "radio.h"
#include "loraradio.h"


typedef enum {
	LORATEST_COMMAND_START_TONE,
	LORATEST_COMMAND_STOP_TONE,
	LORATEST_COMMAND_ENABLE_PA,
	LORATEST_COMMAND_DISABLE_PA,
	LORATEST_COMMAND_SET_FREQUENCY,
	LORATEST_COMMAND_SET_POWER
} loratest_command_type_t;

typedef struct {
	loratest_command_type_t type;
	union {
		uint32_t freq;
		int8_t tx_pow;
	} data;
} loratest_command_t;

static QueueHandle_t commandQueue = NULL;
#define COMMANDQUEUE_LENGTH 1
#define COMMANDQUEUE_ITEM_SIZE sizeof(loratest_command_t)
static uint8_t commandQueue_buffer[COMMANDQUEUE_LENGTH * COMMANDQUEUE_ITEM_SIZE];
static StaticQueue_t commandQueue_struct;

static TaskHandle_t loratest_thread_handle;
static StaticTask_t loratest_thread_tcb;
#define LORATEST_THREAD_STACK_SIZE 512
static StackType_t loratest_thread_stack[LORATEST_THREAD_STACK_SIZE];

static bool tone_on = false;

static loraradio_params_t params = {
	.freq = 915000000,
	.tx_pow = 14,
	.spreading_factor = SF10,
	.bandwidth = BW125,
	.coding_rate = CR_4_5,
	.implicit_header = true,
	.no_crc = false,
	.timeout_syms = 0,
	.data_len = 5
};

void loratest_thread(void *arg);

static void start_tone();
static void stop_tone();

bool loratest_start_tone() {
	loratest_command_t cmd;

	if (commandQueue == NULL) {
		return false;
	}

	cmd.type = LORATEST_COMMAND_START_TONE;

	if (xQueueSend(commandQueue, &cmd, 0) == pdTRUE) {
		return true;
	} else {
		return false;
	}
}

bool loratest_stop_tone() {
	loratest_command_t cmd;

	if (commandQueue == NULL) {
		return false;
	}

	cmd.type = LORATEST_COMMAND_STOP_TONE;

	if (xQueueSend(commandQueue, &cmd, 0) == pdTRUE) {
		return true;
	} else {
		return false;
	}
}

bool loratest_set_freq(uint32_t freq) {
	loratest_command_t cmd;

	if (commandQueue == NULL) {
		return false;
	}

	if (!((900000000 <= freq) && (freq <= 930000000))) {
		return false;
	}

	cmd.type = LORATEST_COMMAND_SET_FREQUENCY;
	cmd.data.freq = freq;

	if (xQueueSend(commandQueue, &cmd, 0) == pdTRUE) {
		return true;
	} else {
		return false;
	}
}

bool loratest_set_tx_pow(int8_t tx_pow) {
	loratest_command_t cmd;

	if (commandQueue == NULL) {
		return false;
	}

	if (!((-1 <= tx_pow) && (tx_pow <= 14))) {
		return false;
	}

	cmd.type = LORATEST_COMMAND_SET_POWER;
	cmd.data.tx_pow = tx_pow;

	if (xQueueSend(commandQueue, &cmd, 0) == pdTRUE) {
		return true;
	} else {
		return false;
	}
}

bool loratest_enable_pa() {
	loratest_command_t cmd;

	if (commandQueue == NULL) {
		return false;
	}

	cmd.type = LORATEST_COMMAND_ENABLE_PA;

	if (xQueueSend(commandQueue, &cmd, 0) == pdTRUE) {
		return true;
	} else {
		return false;
	}
}

bool loratest_disable_pa() {
	loratest_command_t cmd;

	if (commandQueue == NULL) {
		return false;
	}

	cmd.type = LORATEST_COMMAND_DISABLE_PA;

	if (xQueueSend(commandQueue, &cmd, 0) == pdTRUE) {
		return true;
	} else {
		return false;
	}
}

void loratest_init() {
	/* Create the queue which is used to receive commands from other tasks
	 * with the assistance of the helper functions above */
	commandQueue = xQueueCreateStatic(
			COMMANDQUEUE_LENGTH,
			COMMANDQUEUE_ITEM_SIZE,
			commandQueue_buffer,
			&commandQueue_struct);

	if (commandQueue == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	/* Start the new task running */
	loratest_thread_handle = xTaskCreateStatic(
			loratest_thread,
			"LORATEST",
			LORATEST_THREAD_STACK_SIZE,
			NULL,
			1,
			loratest_thread_stack,
			&loratest_thread_tcb
			);

	if (loratest_thread_handle == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_INVALID_PARAM);
	}
}

void loratest_thread(void *arg) {
	loratest_command_t cmd;

	while (1) {
		while (xQueueReceive(commandQueue, &cmd, portMAX_DELAY) == pdFALSE);

		switch(cmd.type) {
		case LORATEST_COMMAND_START_TONE:
			start_tone();
			break;
		case LORATEST_COMMAND_STOP_TONE:
			stop_tone();
			break;
		case LORATEST_COMMAND_ENABLE_PA:
				// Set CPS to 1 to enable PA
#if defined (BOARD_CELL_NODE_REV1P1) | defined (BOARD_CELL_NODE_REV1P2)
		    WITH_TWI_MTX() {

				mcp23008_setPins(MCP23008_LORA_I2C_ADDR, LORA_FEM_CPS_EXPANDER_PIN);

		    }
#elif defined (BOARD_LORA_MODULE_REV1P0)
				nrf_gpio_pin_set(LORA_FEM_CPS);
#endif
			break;
		case LORATEST_COMMAND_DISABLE_PA:
				// Set CPS to 0 to disable PA
#if defined (BOARD_CELL_NODE_REV1P1) | defined (BOARD_CELL_NODE_REV1P2)
		    WITH_TWI_MTX() {

				mcp23008_clearPins(MCP23008_LORA_I2C_ADDR, LORA_FEM_CPS_EXPANDER_PIN);

		    }
#elif defined (BOARD_LORA_MODULE_REV1P0)
				nrf_gpio_pin_clear(LORA_FEM_CPS);
#endif

			break;
		case LORATEST_COMMAND_SET_FREQUENCY:
			params.freq = cmd.data.freq;

			/* Restart if transmitter is already on */
			if (tone_on) {
				start_tone();
			}

			break;
		case LORATEST_COMMAND_SET_POWER:
			params.tx_pow = cmd.data.tx_pow;

			/* Restart if transmitter is already on */
			if (tone_on) {
				start_tone();
			}

			break;

		default:
			break;
		}
	}
}

void start_tone() {
	sxradio_setContinuousMode(true);

	/* When starting a transmission, we expect the LoRa chip to be in its sleep
	 * state. */
	stop_tone();

	loraradio_prepare_tx(&params);
	loraradio_tx_async();

	tone_on = true;
}

void stop_tone() {
	sxradio_standby();
	sxradio_sleep();

	tone_on = false;
}
