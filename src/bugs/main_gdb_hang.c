#include <stdint.h>

#include "bufpack.h"

uint8_t buf[100];

int main(void) {
	bufpack_t bp;
	bufpack_init(&bp, buf, 7);
	// Can't print bp.buf in gdb after the following line executes!
	// gdb will hang. If you change the value though, gdb prints bp.buf
	// just fine (try 0x1010).
	bufpack_write3(&bp, 0xbeef);

	bufpack_write2(&bp, 0x1234);
	bufpack_write2(&bp, 0x5678);
	while (1) {}
}
