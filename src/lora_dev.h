#pragma once

#include <stdbool.h>
#include <stdint.h>
#include "FreeRTOS.h"

#include "lorashared.h"

#define LORA_DEV_MAX_COUNT 16
#define LORA_DEV_MAX_PEND_DOWNLINKS 4

#define LORA_DEV_FLAGS_USED 0x1

typedef struct {
	uint8_t serial[8];

	// These two bytes could be shaved, since addr == index in array
	uint16_t addr;

	uint16_t seq_up;
	uint16_t seq_down;
	TickType_t last_uplink_ts;
	uint8_t flags;
} lora_dev_t;

lora_dev_t lora_devset[LORA_DEV_MAX_COUNT];

// We use a fixed sized pool of fix sized payloads. We could be more memory
// efficient by making this dynamic.
//
// Invariant: every dev_addr is present at most once.
typedef struct {
	uint8_t payload[LORA_MAX_DOWNLINK_LEN];
	uint8_t len;
	uint16_t dev_addr;
} lora_dev_pending_downlink_t;

lora_dev_pending_downlink_t lora_dev_downlinks[LORA_DEV_MAX_PEND_DOWNLINKS];

void lora_dev_set_used(lora_dev_t *dev);

// O(n)
lora_dev_t *lora_dev_lookup_serial(uint8_t serial[8]);

// O(1)
lora_dev_t *lora_dev_lookup_addr(uint16_t addr);

// O(n)
lora_dev_t *lora_dev_get_empty(void);

bool lora_dev_set_downlink(lora_dev_t *dev, uint8_t *data, uint8_t len);

lora_dev_pending_downlink_t *lora_dev_get_downlink(lora_dev_t *dev);

void lora_dev_downlink_clear(lora_dev_pending_downlink_t *downlink);
