#include "lorashared.h"

lora_slot_t lora_slot(TickType_t beacon_time, TickType_t time, int delta) {
	lora_slot_t ret;
	ret.beacon_time = beacon_time;
	ret.index = (time - beacon_time) / LORA_SLOT_LEN_MS + delta;
	ret.offset_time = ret.index * LORA_SLOT_LEN_MS;
	return ret;
}

bool lora_slot_ok(lora_slot_t slot) {
	return slot.index < LORA_NUM_SLOTS;
}

loraradio_params_t lora_beacon_params(int beacon_seq) {
	loraradio_params_t downlink_params = {
		.freq = 918000000,
		.spreading_factor = SF10,
		.bandwidth = BW125,
		.coding_rate = CR_4_5,
		.implicit_header = true,
		.no_crc = false,
		.timeout_syms = 0,
		.data_len = LORA_BEACON_LEN
	};
	return downlink_params;
}

// data_len set if tx
loraradio_params_t lora_uplink_params(int slot_idx, uint8_t data_len) {
	if (slot_idx < 0 || slot_idx > LORA_NUM_SLOTS) {
		APP_ERROR_HANDLER(NRF_ERROR_INVALID_PARAM);
	}
	uint32_t freq = 903000000 + 200000 * slot_idx;
	sf_t spreading_factor;
	int symbol_time_us;
	switch(slot_idx % 4) {
		case 0:
			spreading_factor = SF10;
			symbol_time_us = 8190;
			break;
		case 1:
			spreading_factor = SF9;
			symbol_time_us = 4100;
			break;
		case 2:
			spreading_factor = SF8;
			symbol_time_us = 2050;
			break;
		case 3:
			spreading_factor = SF7;
			symbol_time_us = 1020;
			break;
	}
	// Assume ticks = millis for now.
	int timeout_syms = (LORA_SLOT_LEN_MS - 8) * 1000 / symbol_time_us;
	loraradio_params_t uplink_params = {
		.freq = freq,
		.spreading_factor = spreading_factor,
		.bandwidth = BW125,
		.coding_rate = CR_4_5,
		.implicit_header = false,
		.no_crc = false,
		.timeout_syms = timeout_syms,
		.data_len = data_len
	};
	return uplink_params;
}

loraradio_params_t lora_downlink_params(int slot_idx, uint8_t data_len) {
	loraradio_params_t params = lora_uplink_params(slot_idx, data_len);
	params.freq += 5000000;
	return params;
}
