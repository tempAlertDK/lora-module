#include <string.h>
#include "crc32.h"

#include "lora_packet.h"

int make_data_packet(uint8_t *buf, uint16_t dev_addr, uint8_t *payload, uint8_t payload_len) {
	bufpack_t bp;
	int len = payload_len + 5;
	bufpack_init(&bp, buf, len);
	bufpack_write1(&bp, dev_addr);
	bufpack_write_buf(&bp, payload, payload_len);
	bufpack_write4(&bp, crc32_compute(buf, payload_len + 1, NULL));
	bufpack_assert_ok(&bp);
	return len;
}

lora_packet_data_t parse_data_packet(const uint8_t *buf, uint8_t buf_len) {
	bufpack_t bp;
	bufpack_init(&bp, (uint8_t *) buf, buf_len);
	lora_packet_data_t ret;
	ret.dev_addr = bufpack_read1(&bp);
	ret.payload = bufpack_read_buf(&bp, buf_len - 5);
	ret.payload_len = buf_len - 5;
	uint32_t expected_crc = bufpack_read4(&bp);
	if (bp.error) {
		ret.result = LORA_PACKET_RESULT_SHORT;
	} else if (crc32_compute(buf, ret.payload_len + 1, NULL) != expected_crc) {
		ret.result = LORA_PACKET_RESULT_CRC_MISMATCH;
	} else {
		ret.result = LORA_PACKET_RESULT_OK;
	}
	return ret;
}
