#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "bufpack.h"

typedef enum {
	LORA_PACKET_RESULT_OK = 0,
	LORA_PACKET_RESULT_SHORT,
	LORA_PACKET_RESULT_CRC_MISMATCH
} lora_packet_result_t;

typedef struct {
	uint16_t dev_addr;
	uint8_t *payload;
	uint8_t payload_len;
	int result;
} lora_packet_data_t;

int make_data_packet(uint8_t *buf, uint16_t dev_addr, uint8_t *payload, uint8_t payload_len);
lora_packet_data_t parse_data_packet(const uint8_t *buf, uint8_t buf_len);
