/*
 * Created on: May 6, 2017
 *     Author: shawnlewis
 *
 */

// NOTE: Throughout this file we assume configTICK_RATE_HZ == 1000

#include <stdint.h>
#include <string.h>
#include "SEGGER_RTT.h"
#include "FreeRTOS.h"
#include "nrf_gpio.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "portmacro_cmsis.h"
#include "crc16.h"
#include "simplehsm.h"

#include "loraradio.h"
#include "bufpack.h"
#include "util.h"
#include "memtrace.h"

#include "lorashared.h"
#include "loranode.h"
#include "lora_packet.h"

#define DEBUG_LORANODE_HSM

// The amount of time prior to beacon or downlink slots that we should
// wakeup.
#define LORA_NODE_WAKEUP_MS 50

typedef enum {
	LORANODE_COMMAND_STOP,
	LORANODE_COMMAND_ACQUIRE,
	LORANODE_COMMAND_UPLINK
} loranode_command_type_t;

typedef struct {
	loranode_command_type_t type;
} loranode_command_t;

typedef struct {
	loraradio_result_t result;
	TickType_t ticks;
	uint32_t gateway_id;
	uint16_t beacon_seq;
	uint16_t checksum;
	int ok;
} loranode_beacon_t;

typedef enum {
	SIG_CMD_STOP = SIG_USER,
	SIG_CMD_ACQUIRE,
	SIG_CMD_UPLINK,
	SIG_RECEIVE_BEACON,
	SIG_RECEIVE_DATA,
	SIG_TX_DONE,
	SIG_RX_DONE,
} hsm_signal_t;

static const char *signal_names[44] = {
	[0] = "SIG_NULL",
	[1] = "SIG_INIT",
	[2] = "SIG_ENTRY",
	[3] = "SIG_DEEPHIST",
	[4] = "SIG_EXIT",
	[5] = "SIG_CMD_STOP",
	[6] = "SIG_CMD_ACQUIRE",
	[7] = "SIG_CMD_UPLINK",
	[8] = "SIG_RECEIVE_BEACON",
	[9] = "SIG_RECEIVE_DATA",
	[10] = "SIG_TX_DONE",
	[11] = "SIG_RX_DONE"
};

static QueueHandle_t commandQueue = NULL;
QueueHandle_t loranode_eventQueue = NULL;

static QueueSetHandle_t queueSet = NULL;

static simplehsm_t hsm = {NULL};

static stnext state_top(int signal, void *param);
  static stnext state_idle(int signal, void *param);
  static stnext state_acquiring(int signal, void *param);
  static stnext state_tracking(int signal, void *param);
    static stnext state_rxing_beacon(int signal, void *param);
    static stnext state_ready(int signal, void *param);
    static stnext state_txing(int signal, void *param);
    static stnext state_response_delay(int signal, void *param);
    static stnext state_rxing_response(int signal, void *param);
    static stnext state_rxing_beacon_then_response(int signal, void *param);

static loranode_beacon_t loranode_parse_beacon(loraradio_params_t params, loraradio_result_t result);

// Globals
static uint8_t *frame_buf;
static int beacon_missed_count = 0;
static loranode_beacon_t beacon;
static uint16_t dev_addr = 0;

static bool beacon_event;
static TickType_t next_beacon_ticks;
static bool downlink_slot_event;
static lora_slot_t downlink_slot;
static uint8_t uplink_data[256];
static uint8_t uplink_data_len;

static stnext state_top(int signal, void *param) {
#if defined(DEBUG_LORANODE_HSM)
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, signal_names[signal]);
	}
#endif
	switch (signal) {
	case SIG_ENTRY:
		return stnone;
	case SIG_INIT:
		frame_buf = sxradio_get_frame_buf();
		simplehsm_init_transition_state(&hsm, state_idle);
		return stnone;
	case SIG_EXIT:
		return stnone;
	case SIG_CMD_STOP:
		simplehsm_transition_state(&hsm, state_idle);
		return stnone;
	case SIG_NULL:
		break;
	default:
		printf("Invalid signal. current_state: %p signal: %s\r\n",
			hsm.current_state, signal_names[signal]);
		break;
	}

	return stnone;
}
static stnext state_idle(int signal, void *param) {
#if defined(DEBUG_LORANODE_HSM)
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, signal_names[signal]);
	}
#endif
	switch (signal) {
	case SIG_ENTRY:
		return stnone;
	case SIG_INIT:
		return stnone;
	case SIG_EXIT:
		return stnone;
	case SIG_CMD_ACQUIRE:
		simplehsm_transition_state(&hsm, state_acquiring);
		return stnone;
	}
	return state_top;
}

static stnext state_acquiring(int signal, void *param) {
#if defined(DEBUG_LORANODE_HSM)
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, signal_names[signal]);
	}
#endif
	static loraradio_params_t beacon_params;
	switch (signal) {
	case SIG_ENTRY:
		return stnone;
	case SIG_INIT: ;
		beacon_params = lora_beacon_params(0);
		printf("listening for beacon\r\n");
		loraradio_rx_async(&beacon_params);
		return stnone;
	case SIG_EXIT:
		return stnone;
	case SIG_RX_DONE: ;
		loraradio_result_t *result = param;
		beacon = loranode_parse_beacon(beacon_params, *result);
		if (beacon.ok) {
			printf("beacon ok\r\n");
			loranode_event_t event = {
				.type=LORANODE_EVENT_ACQUIRED
			};
			// Warning: delaying forever.
			xQueueSend(loranode_eventQueue, &event, portMAX_DELAY);
			simplehsm_transition_state(&hsm, state_tracking);
		} else {
			printf("received invalid beacon, acquiring\r\n");
			simplehsm_transition_state(&hsm, state_acquiring);
		}
		return stnone;
	}
	return state_top;
}

static stnext state_tracking(int signal, void *param) {
#if defined(DEBUG_LORANODE_HSM)
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, signal_names[signal]);
	}
#endif
	switch (signal) {
	case SIG_ENTRY:
		beacon_missed_count = 0;
		return stnone;
	case SIG_INIT:
		simplehsm_init_transition_state(&hsm, state_ready);
		return stnone;
	case SIG_EXIT:
		return stnone;
	case SIG_RECEIVE_BEACON:
		printf("Receive beacon signal receive in invalid state\r\n");
		APP_ERROR_HANDLER(NRF_ERROR_INVALID_STATE);
		return stnone;
	}
	return state_top;
}

static stnext state_ready(int signal, void *param) {
#if defined(DEBUG_LORANODE_HSM)
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, signal_names[signal]);
	}
#endif
	switch (signal) {
	case SIG_ENTRY:
		return stnone;
	case SIG_INIT:
		return stnone;
	case SIG_EXIT:
		return stnone;
	case SIG_CMD_UPLINK: ;
		lora_slot_t next_slot = lora_slot(
			beacon.ticks, xTaskGetTickCount(), 1);
		if (!lora_slot_ok(next_slot)) {
			// Shouldn't happen
			printf("Error: Next slot_idx out of range\r\n");
			break;
		}
		printf("Uplink: using slot: %d %u\r\n",
			next_slot.index,
			(unsigned int)next_slot.offset_time);
		int packet_len = make_data_packet(
			frame_buf, dev_addr, uplink_data, uplink_data_len);
		const loraradio_params_t uplink_params = lora_uplink_params(
				next_slot.index, packet_len);
		printf("  packet_len: %d\r\n", packet_len);
		loraradio_prepare_tx(&uplink_params);
		// TODO: using 10ms offset into window, this should
		//    be refined.
		util_delayUntil(beacon.ticks, next_slot.offset_time + 10);
		printf("tx offset: %u %u %u %u\r\n",
			(unsigned int)xTaskGetTickCount(),
			(unsigned int)(xTaskGetTickCount() - beacon.ticks),
			(unsigned int)uplink_params.freq,
			(unsigned int)uplink_params.spreading_factor);
#if defined(LORA_DEBUG_USING_GPIO) && (LORA_DEBUG_USING_GPIO == 1)
		nrf_gpio_pin_set(30);
#endif
		loraradio_tx_async();
#if defined(LORA_DEBUG_USING_GPIO) && (LORA_DEBUG_USING_GPIO == 1)
		nrf_gpio_pin_clear(30);
#endif

		// Setup downlink slot
		downlink_slot_event = true;
		downlink_slot.beacon_time = beacon.ticks;
		downlink_slot.index = next_slot.index + LORA_DOWNLINK_SLOT_DELTA;
		// Wrap if in next beacon period
		if (downlink_slot.index >= LORA_NUM_SLOTS) {
			downlink_slot.beacon_time +=  LORA_BEACON_PERIOD_MS;
			downlink_slot.index -= LORA_NUM_SLOTS;
		}
		// Keep out of beacon reserved slots.
		if (downlink_slot.index < LORA_BEACON_RESERVED_SLOTS) {
			downlink_slot.index += LORA_BEACON_RESERVED_SLOTS;
		}
		downlink_slot.offset_time = downlink_slot.index * LORA_SLOT_LEN_MS;

		simplehsm_transition_state(&hsm, state_txing);
		return stnone;
	case SIG_RECEIVE_BEACON:
		simplehsm_transition_state(&hsm, state_rxing_beacon);
		return stnone;
	}
	return state_tracking;
}

static stnext state_txing(int signal, void *param) {
#if defined(DEBUG_LORANODE_HSM)
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, signal_names[signal]);
	}
#endif
	switch (signal) {
	case SIG_ENTRY:
		return stnone;
	case SIG_INIT:
		return stnone;
	case SIG_EXIT:
		return stnone;
	case SIG_TX_DONE:
		simplehsm_transition_state(&hsm, state_response_delay);
		return stnone;
	}
	return state_tracking;
}

static stnext state_response_delay(int signal, void *param) {
#if defined(DEBUG_LORANODE_HSM)
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, signal_names[signal]);
	}
#endif
	switch (signal) {
	case SIG_ENTRY:
		return stnone;
	case SIG_INIT:
		return stnone;
	case SIG_EXIT:
		return stnone;
	case SIG_RECEIVE_BEACON:
		simplehsm_transition_state(&hsm, state_rxing_beacon_then_response);
		return stnone;
	case SIG_RECEIVE_DATA:
		simplehsm_transition_state(&hsm, state_rxing_response);
		return stnone;
	}
	return state_tracking;
}

static stnext state_rxing_response(int signal, void *param) {
#if defined(DEBUG_LORANODE_HSM)
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, signal_names[signal]);
	}
#endif
	switch (signal) {
	case SIG_ENTRY:
		return stnone;
	case SIG_INIT: ;
		const loraradio_params_t downlink_params = lora_downlink_params(downlink_slot.index, 0);
		util_delayUntil(downlink_slot.beacon_time, downlink_slot.offset_time - 2);
		printf("rx offset: %u %u %u %u\r\n",
			(unsigned int)xTaskGetTickCount(),
			(unsigned int)(xTaskGetTickCount() - beacon.ticks),
			(unsigned int)downlink_params.freq,
			(unsigned int)downlink_params.spreading_factor);
#if defined(LORA_DEBUG_USING_GPIO) && (LORA_DEBUG_USING_GPIO == 1)
		nrf_gpio_pin_set(30);
#endif
		loraradio_rx_async(&downlink_params);
#if defined(LORA_DEBUG_USING_GPIO) && (LORA_DEBUG_USING_GPIO == 1)
		nrf_gpio_pin_clear(30);
#endif
		return stnone;
	case SIG_EXIT:
		return stnone;
	case SIG_RX_DONE: ;
		loraradio_result_t *downlink = param;
		printf("RX_DONE: data_len: %d\r\n", downlink->data_len);
		loranode_event_t event = {
			.type=LORANODE_EVENT_DOWNLINK,
			.result=*downlink
		};
		// Warning: delaying forever.
		xQueueSend(loranode_eventQueue, &event, portMAX_DELAY);
		simplehsm_transition_state(&hsm, state_ready);
		return stnone;
	}
	return state_tracking;
}

// Code shared between the two state_rxing_beacon* states
static stnext _state_rxing_beacon(
		int signal, void *param, stnext return_state) {
	static loraradio_params_t beacon_params;
	switch (signal) {
	case SIG_ENTRY:
		return stnone;
	case SIG_INIT:
		beacon_params = lora_beacon_params(0);
		beacon_params.timeout_syms = 30;
		printf("listening for beacon\r\n");
		loraradio_rx_async(&beacon_params);
		return stnone;
	case SIG_EXIT:
		return stnone;
	case SIG_RX_DONE: ;
		loraradio_result_t *result = param;
		beacon = loranode_parse_beacon(beacon_params, *result);
		if (beacon.ok) {
			printf("beacon ok\r\n");
			beacon_missed_count = 0;
			simplehsm_transition_state(&hsm, return_state);
		} else {
			beacon_missed_count++;
			printf("missed beacon. count: %u\r\n", beacon_missed_count);
			if (beacon_missed_count >= LORA_MAX_BEACON_MISSES) {
				// TODO: send event to user.
				simplehsm_transition_state(&hsm, state_acquiring);
			} else {
				simplehsm_transition_state(&hsm, return_state);
			}
		}
		return stnone;
	}
	return state_tracking;
}

static stnext state_rxing_beacon(int signal, void *param) {
#if defined(DEBUG_LORANODE_HSM)
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, signal_names[signal]);
	}
#endif
	return _state_rxing_beacon(signal, param, state_ready);
}

static stnext state_rxing_beacon_then_response(int signal, void *param) {
#if defined(DEBUG_LORANODE_HSM)
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, signal_names[signal]);
	}
#endif
	return _state_rxing_beacon(signal, param, state_response_delay);
}

static void loranode_send_command(loranode_command_t *command) {
	int ret = xQueueSend(commandQueue, command, portMAX_DELAY);
	if (!ret) {
		APP_ERROR_HANDLER(NRF_ERROR_INVALID_STATE);
	}
}

void loranode_uplink(uint8_t *data, uint8_t data_len) {
	loranode_command_t command = {
		.type = LORANODE_COMMAND_UPLINK,
	};
	memcpy(uplink_data, data, data_len);
	uplink_data_len = data_len;
	loranode_send_command(&command);
}

void loranode_acquire(void) {
	loranode_command_t command = {
		.type = LORANODE_COMMAND_ACQUIRE,
	};
	loranode_send_command(&command);
}

void loranode_stop(void) {
	loranode_command_t command = {
		.type = LORANODE_COMMAND_STOP,
	};
	loranode_send_command(&command);
}

static loranode_beacon_t loranode_parse_beacon(loraradio_params_t params, loraradio_result_t result) {
	loranode_beacon_t ret;
	ret.result = result;
	ret.ticks = ret.result.done_ticks - loraradio_time_on_air(&params);
	beacon_event = true;
	next_beacon_ticks = ret.ticks + LORA_BEACON_PERIOD_MS;

	if (ret.result.data_len != LORA_BEACON_LEN) {
		printf("Received short beacon, len: %u\r\n", ret.result.data_len);
		ret.ok = 0;
		return ret;
	}

	bufpack_t bp;
	bufpack_init(&bp, (uint8_t *)ret.result.data, ret.result.data_len);
	ret.gateway_id = bufpack_read3(&bp);
	ret.beacon_seq = bufpack_read2(&bp);
	ret.checksum = bufpack_read2(&bp);
	printf("Received packet. gateway_id: %03x seq: %d checksum: %d\r\n",
		(unsigned int)ret.gateway_id, ret.beacon_seq, ret.checksum);
	if (ret.checksum != crc16_compute(ret.result.data, 5, NULL)) {
		printf("  CRC mismatch\r\n");
		ret.ok = 0;
	} else {
		printf("  CRC ok\r\n");
		ret.ok = 1;
	}
	return ret;
}

void loranode_thread(void *arg) {
	while(1) {
		// Compute time delta to next event (downlink slot or beacon).
		TickType_t cur_ticks = xTaskGetTickCount();
		TickType_t timeout = portMAX_DELAY;
		hsm_signal_t sig = 0;
		if (beacon_event) {
			timeout = next_beacon_ticks - cur_ticks;
			printf("beacon timeout: %u\r\n",
				(unsigned int)timeout);
			sig = SIG_RECEIVE_BEACON;
		}
		if (downlink_slot_event) {
			TickType_t downlink_slot_delta = downlink_slot.beacon_time
					+ downlink_slot.offset_time - cur_ticks;
			printf("downlink slot timeout: %u\r\n",
				(unsigned int)downlink_slot_delta);
			if (downlink_slot_delta < timeout) {
				timeout = downlink_slot_delta;
				sig = SIG_RECEIVE_DATA;
			}
		}

		if (timeout < LORA_NODE_WAKEUP_MS) {
			if (sig == SIG_RECEIVE_BEACON) {
				beacon_event = false;
			} else if (sig == SIG_RECEIVE_DATA) {
				downlink_slot_event = false;
			}
			simplehsm_signal_current_state(&hsm, sig, NULL);
		} else {
			printf("using timeout: %u\r\n",
				(unsigned int)timeout - LORA_NODE_WAKEUP_MS + 1);
			QueueSetMemberHandle_t activated = xQueueSelectFromSet(
					queueSet, timeout - LORA_NODE_WAKEUP_MS + 1);
			if (activated == commandQueue) {
				loranode_command_t command;
				xQueueReceive(commandQueue, &command, 0);
				switch (command.type) {
				case LORANODE_COMMAND_STOP:
					simplehsm_signal_current_state(&hsm, SIG_CMD_STOP, NULL);
					break;
				case LORANODE_COMMAND_ACQUIRE:
					simplehsm_signal_current_state(&hsm, SIG_CMD_ACQUIRE, &command);
					break;
				case LORANODE_COMMAND_UPLINK:
					simplehsm_signal_current_state(&hsm, SIG_CMD_UPLINK, NULL);
					break;
				}
			} else if (activated == loraradio_commandDone_queue) {
				loraradio_result_t result;
				xQueueReceive(loraradio_commandDone_queue, &result, 0);
				printf("loraradio_commandDone. is_tx: %d\r\n", result.is_tx);
				if (result.is_tx) {
					simplehsm_signal_current_state(&hsm, SIG_TX_DONE, &result);
				} else {
					simplehsm_signal_current_state(&hsm, SIG_RX_DONE, &result);
				}
			} else {
				printf("hit timeout\r\n");
				// timeout
			}
		}
	}
}

void loranode_init(void) {
	if ((commandQueue = xQueueCreate(1, sizeof(loranode_command_t))) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	if ((loranode_eventQueue = xQueueCreate(1, sizeof(loranode_event_t))) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	if ((queueSet = xQueueCreateSet(2)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	xQueueAddToSet(commandQueue, queueSet);
	xQueueAddToSet(loraradio_commandDone_queue, queueSet);

	simplehsm_initialize(&hsm, state_top);

	// Eth IRQn pin for debugging
	nrf_gpio_pin_clear(30);
	nrf_gpio_cfg_output(30);
}
