/*
 * util.h
 *
 *  Created on: Nov 13, 2013
 *      Author: kwgilpin
 */

#ifndef UTIL_H_
#define UTIL_H_

#include <stdint.h>
#include <stdbool.h>

enum {
	DEBUG_LEVEL_NONE = 0,
	DEBUG_LEVEL_ERROR = 1,
	DEBUG_LEVEL_BASIC = 2,
	DEBUG_LEVEL_DETAILED = 3,
	DEBUG_LEVEL_ALL = 4
};

void util_setDebugLevel(uint8_t level);
uint8_t util_getDebugLevel(void);
uint32_t util_printDebugString(const char *str, uint8_t debugLevelThreshold);

bool delay_ms(uint32_t ms);

void util_printHex(const uint8_t * p_data, const uint32_t len);
void util_printAscii(const uint8_t * p_data, const uint32_t len);

// Delay until base + offset
void util_delayUntil(TickType_t base, const TickType_t offset);
// Delay until base + offset, and update base to base + offset
void util_delayUntilAndUpdateBase(TickType_t *base, const TickType_t offset);

#endif /* UTIL_H_ */
