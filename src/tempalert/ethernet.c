/*
 * ethernet.c
 *
 *  Created on: Jul 2, 2017
 *      Author: kwgilpin
 */


#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"

#include "app_util_platform.h"
#include "app_error.h"

#include "boards.h"

#include "mcp23008.h"

#include "ethernet.h"

void ethernet_irq_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);

void ethernet_init() {
    uint32_t err_code;

    /* Disable power by default */
#if defined(MODEM_POPULATED) && (MODEM_POPULATED == 1)
	mcp23008_clearPins(MCP23008_CELL_ADDR_OFFSET, ETHERNETEN_PIN);
#else
	nrf_gpio_pins_clear(ETHERNETEN_PIN);
#endif

	/* Configure the ETHERNET_MISO_IRQN_PIN line as a input with HI-LO sensing */
    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(false);
    /* Enable the pull-up because the tri-state buffer driving the line goes
     * high impedance when un-powered; we don't want the line floating. */
    in_config.pull = NRF_GPIO_PIN_PULLUP;
    err_code = nrf_drv_gpiote_in_init(ETHERNET_MISO_IRQN_PIN, &in_config, ethernet_irq_pin_handler);
    APP_ERROR_CHECK(err_code);
}

void ethernet_off() {
	nrf_drv_gpiote_in_event_disable(ETHERNET_MISO_IRQN_PIN);

#if defined(MODEM_POPULATED) && (MODEM_POPULATED == 1)
	mcp23008_clearPins(MCP23008_CELL_ADDR_OFFSET, ETHERNETEN_PIN);
#else
	nrf_gpio_pins_clear(ETHERNETEN_PIN);
#endif
}

void ethernet_on() {
#if defined(MODEM_POPULATED) && (MODEM_POPULATED == 1)
	mcp23008_setPins(MCP23008_CELL_ADDR_OFFSET, ETHERNETEN_PIN);
#else
	nrf_gpio_pins_set(ETHERNETEN_PIN);
#endif

	nrf_delay_ms(10);

	nrf_drv_gpiote_in_event_enable(ETHERNET_MISO_IRQN_PIN, true);
}

bool ethernet_getState() {
	bool on;

#if defined(MODEM_POPULATED) && (MODEM_POPULATED == 1)
	uint8_t pins;
	mcp23008_readPins(MCP23008_CELL_ADDR_OFFSET, &pins);
	on = pins & ETHERNETEN_PIN;
#else
	on = nrf_gpio_pin_read(ETHERNETEN_PIN);
#endif

	return on;
}

void ethernet_irq_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action) {
	printf("Ethernet IRQ detected\r\n");
}
