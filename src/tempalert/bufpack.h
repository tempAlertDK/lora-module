/*
 * Created on: May 6, 2017
 *     Author: shawnlewis
 *
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

// TODO: Is there already something like this in the source tree?

typedef struct {
	unsigned char *buf;
	uint32_t idx;
	uint32_t max_size;
	bool error;
} bufpack_t;

void bufpack_init(bufpack_t *bp, uint8_t *buf, uint32_t max_size);
void bufpack_assert_ok(bufpack_t *bp);
void bufpack_write1(bufpack_t *bp, uint8_t v);
void bufpack_write2(bufpack_t *bp, uint16_t v);
void bufpack_write3(bufpack_t *bp, uint32_t v);
void bufpack_write4(bufpack_t *bp, uint32_t v);
void bufpack_write_buf(bufpack_t *bp, uint8_t *buf, int buf_len);
uint8_t bufpack_read1(bufpack_t *bp);
uint16_t bufpack_read2(bufpack_t *bp);
uint32_t bufpack_read3(bufpack_t *bp);
uint32_t bufpack_read4(bufpack_t *bp);
uint8_t *bufpack_read_buf(bufpack_t *bp, int buf_len);
