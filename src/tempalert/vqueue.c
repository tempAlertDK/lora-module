/*
 * vqueue.c
 *
 *  Created on: Feb 2, 2017
 *      Author: kwgilpin
 */

#include <stdint.h>
#include <stdbool.h>

#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"

typedef struct {
	uint8_t *data;
	uint16_t length;
	uint16_t inPtr;
	uint16_t outPtr;
	bool full;
	bool empty;
	QueueHandle_t item_md;
	SemaphoreHandle_t mutex;
} varqueue_t;

typedef struct {
	uint8_t item_type;
	uint16_t item_start;
	uint16_t item_end;
} varqueue_item_desc_t;

bool varqueue_push(varqueue_t *vq, uint8_t *data, uint16_t length, TickType_t xTicksToWait) {
	if (xSemaphoreTake(vq->mutex, xTicksToWait) != pdTRUE) {
		return false;
	}

	if ((vq->full) || (uxQueueSpacesAvailable(vq->item_md) < 1)) {
		xSemaphoreGive(vq->mutex);
		return false;
	}

	xSemaphoreGive(vq->mutex);
}
