/*
 * modemPrinter.c
 *
 *  Created on: Feb 3, 2017
 *      Author: kwgilpin
 */

#include <stdint.h>
#include <stdbool.h>

#include "nordic_common.h"
#include "nrf.h"

#include "SEGGER_RTT.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "portmacro_cmsis.h"

#include "els31uart.h"
#include "genericQueue.h"

#include "modemPrinter.h"

typedef union {
	modem_uart_response_t els31uart_response;
} genericQueueData_t;

#define MODEMPRINTERQUEUE_LENGTH	6
#define MODEMPRINTERQUEUE_ITEMSIZE sizeof(genericQueueElement_t)
uint8_t modemPrinterQueueBuffer[MODEMPRINTERQUEUE_LENGTH*MODEMPRINTERQUEUE_ITEMSIZE];
static StaticQueue_t modemPrinter_genericQueueStaic;

QueueHandle_t modemPrinter_genericQueue;

static TaskHandle_t m_modemPrinter_thread;
static StaticTask_t m_modemPrinter_thread_static;
#define MDPRT_STACK_SIZE		512
static StackType_t m_modemPrinter_thread_buffer[ MDPRT_STACK_SIZE ];


static void modemPrinter_thread(void *arg);

bool modemPrinter_init(void) {

	modemPrinter_genericQueue = xQueueCreateStatic(MODEMPRINTERQUEUE_LENGTH, MODEMPRINTERQUEUE_ITEMSIZE
			, modemPrinterQueueBuffer, &modemPrinter_genericQueueStaic);
	if (NULL == modemPrinter_genericQueue) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	// Start execution.
	if((m_modemPrinter_thread = xTaskCreateStatic(modemPrinter_thread, "MODEM PRINTER", MDPRT_STACK_SIZE,
			NULL, 1, m_modemPrinter_thread_buffer, &m_modemPrinter_thread_static)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	return true;
}

void modemPrinter_thread(void *arg) {
	genericQueueElementType_t type;
	genericQueueData_t queueData;
	size_t queueDataSize= sizeof(queueData);

	SEGGER_RTT_printf(0, "Modem printer thread started\r\n");
	while (1) {

		while(!genericQueueReceive(modemPrinter_genericQueue, &type, &queueData, &queueDataSize, portMAX_DELAY));

		switch(type) {
		case GENERIC_QUEUE_MODEM_RESPONSE_TYPE:
			if (queueData.els31uart_response.type == 'C') {
				els31uart_prettyPrint((char *)queueData.els31uart_response.data, queueData.els31uart_response.length, "MODEM COMMAND");
			} else if (queueData.els31uart_response.type == 'R') {
				els31uart_prettyPrint((char *)queueData.els31uart_response.data, queueData.els31uart_response.length, "MODEM RESPONSE");
			} else {
				els31uart_prettyPrint((char *)queueData.els31uart_response.data, queueData.els31uart_response.length, "MODEM ?");
			}
			break;

		default:
			printf("ERROR: Unknown event type (%d) in generic modemPrinter queue\r\n", type);
			break;
		}
	}
}
