/*
 * modemPrinter.h
 *
 *  Created on: Feb 3, 2017
 *      Author: kwgilpin
 */

#ifndef SRC_TEMPALERT_MODEMPRINTER_H_
#define SRC_TEMPALERT_MODEMPRINTER_H_

#include <stdint.h>
#include <stdbool.h>

#include "queue.h"

extern QueueHandle_t modemPrinter_genericQueue;

bool modemPrinter_init(void);

#endif /* SRC_TEMPALERT_MODEMPRINTER_H_ */
