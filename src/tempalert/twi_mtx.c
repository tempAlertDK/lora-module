/*
 * twi.c
 *
 *  Created on: Jan 12, 2017
 *      Author: kwgilpin
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "twi_mtx.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "portmacro_cmsis.h"
#include "log.h"

static SemaphoreHandle_t twi_mtx = NULL;
static StaticSemaphore_t twi_mtx_buffer;

void twi_mtx_init() {
	twi_mtx = xSemaphoreCreateMutexStatic(&twi_mtx_buffer);
	xSemaphoreGive(twi_mtx);
}


uint8_t twi_mtx_take(TickType_t ticksToWait) {
	if (twi_mtx == NULL) {
#if defined(TWI_MTX_DEBUG) && (TWI_MTX_DEBUG >= 1)
		log_error("TWI: mutex not initialized!");
#endif
		return 0;
	}

#if defined(TWI_MTX_DEBUG) && (TWI_MTX_DEBUG >= 1)
	/* Poll to see whether the mutex is immediately available */
	if (xSemaphoreTake(twi_mtx, 0) == pdFALSE) {
		log_errorf("TWI: %s task blocking because mutex held by %s task", pcTaskGetName(NULL), pcTaskGetName(xSemaphoreGetMutexHolder(twi_mtx)));
	} else {
#if (TWI_MTX_DEBUG == 2)
		log_errorf("TWI: mutex taken by %s task", pcTaskGetName(NULL));
#endif
		return 1;
	}
#endif

	/* Wait for the mutex */
	if (xSemaphoreTake(twi_mtx, ticksToWait) == pdTRUE) {
#if defined(TWI_MTX_DEBUG) && (TWI_MTX_DEBUG >= 1)
		log_errorf("TWI: mutex taken by %s task", pcTaskGetName(NULL));
#endif
		return 1;
	} else {
#if defined(TWI_MTX_DEBUG) && (TWI_MTX_DEBUG >= 1)
		log_errorf("TWI: %s task failed to take mutex", pcTaskGetName(NULL));
#endif
		return 0;
	}
}

void twi_mtx_give() {
	if (twi_mtx == NULL) {
#if defined(TWI_MTX_DEBUG) && (TWI_MTX_DEBUG >= 1)
		log_error("TWI: mutex not initialized");
#endif
		return;
	}

#if defined(TWI_MTX_DEBUG) && (TWI_MTX_DEBUG == 2)
	log_errorf("TWI: mutex given by %s task", pcTaskGetName(NULL));
#endif
	xSemaphoreGive(twi_mtx);
}

void twi_mtx_restore(const uint8_t *taken) {
	if (*taken) {
		twi_mtx_give();
	}
}
