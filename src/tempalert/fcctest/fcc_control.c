#include <stdint.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include "fcc_control.h"
#include "fcc_bluetooth.h"
#include "fcc_lora.h"
#include "fcc_phy.h"


static TaskHandle_t control_thread_handle; /**< Definition of control thread. */
static StaticTask_t control_thread_tcb;
#define CONTROL_STACK_SIZE		512
static StackType_t control_thread_stack[ CONTROL_STACK_SIZE ];


#define RADIOCTL_QUEUE_LENGTH	3
#define RADIOCTL_QUEUE_ITEMSIZE sizeof(radio_control_message_t)


QueueHandle_t radio_controlQueue;
static StaticQueue_t radio_controlQueueStatic;
uint8_t radio_controlQueueBuffer[RADIOCTL_QUEUE_LENGTH*RADIOCTL_QUEUE_ITEMSIZE];
volatile uint8_t test;
static void control_thread(void *arg);

void ctl_run_radio_test(fcc_radio_settings_t * settings)
{
	settings->radio_fxn(settings->params);
	phy_led_update(settings->params);
}

void ctl_update_radio_settings(fcc_radio_settings_t * settings, radio_control_message_t * message)
{
	settings->params[message->updated_field] = message->updated_value;
}

void control_init(void)
{
		/* Create radio_controlQueue */
	if ((radio_controlQueue = xQueueCreateStatic(RADIOCTL_QUEUE_LENGTH,RADIOCTL_QUEUE_ITEMSIZE,
			radio_controlQueueBuffer, &radio_controlQueueStatic)) == pdFAIL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	if ((control_thread_handle = xTaskCreateStatic(control_thread, "CONTROL", CONTROL_STACK_SIZE, NULL, 2, control_thread_stack, &control_thread_tcb)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

}

static void control_thread(void *arg){
	radio_control_message_t radioControlRead;
	radio_selector_t			radioSelector;

	fcc_radio_settings_t	bt_settings;
	fcc_radio_settings_t	lora_settings;

	bt_settings.radio_fxn = bt_run_radio_test;
	bt_settings.params[RADIO_SELECT] = SELECT_BT;
	bt_settings.params[RADIO_BAND] = LOW_BAND;
	bt_settings.params[RADIO_POWER] = RADIO_TXPOWER_TXPOWER_4dBm;
	bt_settings.params[RADIO_MODE] = RADIO_MODE_MODE_Nrf_2Mbit;
	bt_settings.params[RADIO_TEST] = TX_CARRIER;

	//dk_todo: set lora defaults
	lora_settings.radio_fxn = lora_run_radio_test;
	lora_settings.params[RADIO_SELECT] = SELECT_LORA;
	lora_settings.params[RADIO_BAND] = MED_BAND;
	lora_settings.params[RADIO_TEST] = TX_CARRIER;

	radioSelector = SELECT_BT; //default to
	ctl_run_radio_test(&bt_settings);

	while(1)
	{
		if (xQueueReceive(radio_controlQueue, &radioControlRead, pdMS_TO_TICKS(1000)) == pdTRUE) {
			/*if the radio selector changes, choose the radio to turn on and which off.
			if both or neither are selected, no settings can change until a single radio is selected*/
			if (RADIO_SELECT == radioControlRead.updated_field)
			{
				radioSelector = radioControlRead.updated_value;

				switch(radioSelector){
					case (SELECT_BT) :
						lora_radio_disable();
						ctl_run_radio_test(&bt_settings);
						break;
					case (SELECT_LORA) :
						bt_radio_disable();
						ctl_run_radio_test(&lora_settings);
						break;
					case (SELECT_NONE) :
						bt_radio_disable();
						lora_radio_disable();
						break;
					case (SELECT_BOTH) :
						ctl_run_radio_test(&bt_settings);
						ctl_run_radio_test(&lora_settings);
						break;
					default:
						break;
				}
			}
			else if (SELECT_BT == radioSelector)
			{
				ctl_update_radio_settings(&bt_settings, &radioControlRead);
				ctl_run_radio_test(&bt_settings);
			}
			else if (SELECT_LORA == radioSelector)
			{
				ctl_update_radio_settings(&lora_settings, &radioControlRead);
				ctl_run_radio_test(&lora_settings);
			}
		}
	}

}
