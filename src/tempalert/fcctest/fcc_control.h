
#ifndef _FCC_CONTROL_H
#define	_FCC_CONTROL_H

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "queue.h"

typedef uint32_t radio_param_t;

typedef enum radio_selector {
	SELECT_BT = 0,
	SELECT_LORA,
	SELECT_BOTH,
	SELECT_NONE,

	NUM_RAD_SELECT,
} radio_selector_t;

typedef enum radio_field {
	RADIO_SELECT = 0,
	RADIO_POWER,
	RADIO_MODE,
	//dk_todo: sort out ambiguity between "channel" and "band"
	RADIO_CHANNEL,
	RADIO_BAND,
	RADIO_TEST,

	NUM_PARAMS,
} radio_field_t;

enum radio_test {
	TX_CARRIER = 0,
	TX_MODULATED,
	RX_CARRIER,

	NUM_TESTS,
} radio_test_t;

enum radio_band {
	LOW_BAND,
	MED_BAND,
	HIGH_BAND,
	
	NUM_BANDS,
};

typedef struct radio_control_message {
	radio_field_t updated_field;
	uint8_t updated_value;
} radio_control_message_t;

typedef struct fcc_radio_settings {
	radio_param_t params[NUM_PARAMS];
	void (*radio_fxn)(radio_param_t *); //dk_todo: does this copy or pass address?
} fcc_radio_settings_t;

extern QueueHandle_t radio_controlQueue;

void control_init(void);
#endif
