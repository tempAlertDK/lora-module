#pragma once

#include <stdbool.h>
#include "fcc_control.h"
#define RADIO_MODE_MODE_Nrf_2Mbit (1UL) /*!< 2 Mbit/s Nordic proprietary radio mode */
#define RADIO_TXPOWER_TXPOWER_0dBm (0x00UL) /*!< 0 dBm */
#define RADIO_TXPOWER_TXPOWER_4dBm (0x04UL) /*!< 0 dBm */
#define RADIO_MIN_CHANNEL					(0x00)
#define RADIO_MAX_CHANNEL					(0x80)

typedef struct fcc_bt_params {
	uint32_t channel;
	uint32_t txpower;
	uint32_t radio_mode;
} fcc_bt_params_t;
/*
typedef struct fcc_bt_settings {
	fcc_bt_params_t params;
	void (*test_fxn)(fcc_bt_params_t * test_params);
} fcc_bt_settings_t;
*/

//fcc_bt_settings_t bt_test_settings;
typedef enum fcc_bt_error {
	BT_ERR_OK = 0,
	BT_ERR_PARAM_FAIL,//generic fail for now
} fcc_bt_error_t;

extern QueueHandle_t bt_controlQueue;

void bt_test_init(void);
void bt_radio_disable(void);
void bt_run_radio_test(radio_param_t * params);
