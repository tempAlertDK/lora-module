#include <stdint.h>
#include <stdbool.h>

#include "nordic_common.h"
#include "nrf.h"
#include "nrf_delay.h"

#include "boards.h"
//dk_todo: probably don't need most of these includes:

#include "bsp.h"
#include "bsp_btn_ble.h"
#include "pstorage.h"

#include "SEGGER_RTT.h"


#include "config.h"
#include "fcc_control.h"
#include "loraradio.h"
#include "fcc_lora.h"
#include "radio.h"

#include "sx1272.h"
#include "mcp23008.h"
#include "twi_mtx.h"

static void start_tone( void );
static void stop_tone( void );
static bool tone_on = false;
//dk_todo: what values for these:
uint32_t lora_band_freq[NUM_BANDS] = {912000000, 915000000, 918000000};

void lora_test_init(void);

static loraradio_params_t lora_params = {
	.freq = 915000000,
	.tx_pow = 14,
	.spreading_factor = SF10,
	.bandwidth = BW125,
	.coding_rate = CR_4_5,
	.implicit_header = true,
	.no_crc = false,
	.timeout_syms = 0,
	.data_len = 5
};

void start_tone() {
	sxradio_setContinuousMode(true);
	/* When starting a transmission, we expect the LoRa chip to be in its sleep
	 * state. */
	stop_tone();

	loraradio_prepare_tx(&lora_params);
	loraradio_tx_async();

	tone_on = true;
}

void stop_tone() {
	sxradio_standby();
	sxradio_sleep();

	tone_on = false;
}

void lora_radio_disable(void){
	stop_tone();
	sx1272_disableRF();

#ifndef LORA_FEM_CPS
    WITH_TWI_MTX() {
	// Set CPS to 1 to enable PA
//#ifndef BOARD_CELL_NODE_REV1P0
	mcp23008_clearPins(MCP23008_LORA_I2C_ADDR, LORA_FEM_CPS_EXPANDER_PIN);
	}
#else
	nrf_gpio_pin_clear(LORA_FEM_CPS);
#endif
//#endif

}
/**
 * @brief Function for turning on the TX carrier test mode.
*/
void lora_tx_carrier(radio_param_t * params)
{
	lora_params.freq = lora_band_freq[params[RADIO_BAND]];
	lora_params.tx_pow = 14;//params[RADIO_POWER];
	//lora_params.bandwidth; //what should this be for narrowest
	loraradio_prepare_tx(&lora_params);
	start_tone();

}
/*
void loraradio_rx_async(const loraradio_params_t *params) {
	uint8_t rx_mode = RXMODE_SCAN;
	if (params->timeout_syms) {
		rx_mode = RXMODE_SINGLE;
	}
	sxradio_startrx(
		rx_mode,
		params->freq,
		params->spreading_factor,
		params->bandwidth,
		params->coding_rate,
		params->implicit_header,
		params->no_crc,
		1,
		params->timeout_syms,
		params->data_len);
}

loraradio_result_t loraradio_rx_sync(const loraradio_params_t *params) {
	loraradio_rx_async(params);

	loraradio_result_t result;
	xQueueReceive(loraradio_commandDone_queue, &result, portMAX_DELAY);

	return result;
}*/

fcc_lora_error_t lora_param_fault_test(radio_param_t * params)
{
	//dk_todo:
	return LORA_ERR_OK;
}

void lora_run_radio_test(radio_param_t * params)
{	
	fcc_lora_error_t param_error;
	param_error = lora_param_fault_test(params);

	lora_radio_disable();
	if (LORA_ERR_OK == param_error) 
	{
		switch(params[RADIO_TEST])
		{
			case TX_CARRIER:
				lora_tx_carrier(params);
				break;
			case TX_MODULATED:
				//blora_modulated_tx_carrier(params);
				break;
			case RX_CARRIER:
				//lora_rx_carrier(params);
				break;
			default:
				break;
		}
	}
}

void lora_test_init(void){
	loraradio_init();
	sx1272_enableRF();
    WITH_TWI_MTX() {
	// Set CPS to 1 to enable PA
//#ifndef BOARD_CELL_NODE_REV1P0
#ifdef LORA_FEM_CPS
	nrf_gpio_pin_clear(LORA_FEM_CPS);
#else
	mcp23008_setPins(MCP23008_LORA_I2C_ADDR, LORA_FEM_CPS_EXPANDER_PIN);
#endif
	}
}









