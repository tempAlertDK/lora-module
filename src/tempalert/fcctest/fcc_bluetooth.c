#include <stdint.h>
#include <stdbool.h>

#include "nordic_common.h"
#include "nrf.h"
#include "nrf_delay.h"

#include "boards.h"
//dk_todo: probably don't need most of these includes:

#include "bsp.h"
#include "bsp_btn_ble.h"
#include "pstorage.h"

#include "SEGGER_RTT.h"

#include "pa_lna.h"

#include "config.h"
#include "fcc_bluetooth.h"
#include "fcc_control.h"


static uint8_t packet[256];
uint8_t bt_band_freq[NUM_BANDS] = {0, 40, 80};

void bt_test_init(void);

/**
 * @brief Function for generating an 8 bit random number using the internal random generator.
*/
static uint32_t rnd8(void)
{
    NRF_RNG->EVENTS_VALRDY = 0;
    while (NRF_RNG->EVENTS_VALRDY == 0)
    {
        // Do nothing.
    }
    return NRF_RNG->VALUE;
}


/**
 * @brief Function for generating a 32 bit random number using the internal random generator.
*/
static uint32_t rnd32(void)
{
    uint8_t  i;
    uint32_t val = 0;

    for (i=0; i<4; i++)
    {
        val <<= 8;
        val  |= rnd8();
    }
    return val;
}

/**
 * @brief Function for configuring the radio to use a random address and a 254 byte random payload.
 * The S0 and S1 fields are not used.
*/
static void generate_modulated_rf_packet(void)
{
    uint8_t i;

    NRF_RADIO->PREFIX0 = rnd8();
    NRF_RADIO->BASE0   = rnd32();

    // Packet configuration:
    // S1 size = 0 bits, S0 size = 0 bytes, payload length size = 8 bits
    NRF_RADIO->PCNF0  = (0UL << RADIO_PCNF0_S1LEN_Pos) |
                        (0UL << RADIO_PCNF0_S0LEN_Pos) |
                        (8UL << RADIO_PCNF0_LFLEN_Pos);
    // Packet configuration:
    // Bit 25: 1 Whitening enabled
    // Bit 24: 1 Big endian,
    // 4 byte base address length (5 byte full address length),
    // 0 byte static length, max 255 byte payload .
    NRF_RADIO->PCNF1  = (RADIO_PCNF1_WHITEEN_Enabled << RADIO_PCNF1_WHITEEN_Pos) |
                        (RADIO_PCNF1_ENDIAN_Big << RADIO_PCNF1_ENDIAN_Pos) |
                        (4UL << RADIO_PCNF1_BALEN_Pos) |
                        (0UL << RADIO_PCNF1_STATLEN_Pos) |
                        (255UL << RADIO_PCNF1_MAXLEN_Pos);
    NRF_RADIO->CRCCNF = (RADIO_CRCCNF_LEN_Disabled << RADIO_CRCCNF_LEN_Pos);
    packet[0]         = 254;    // 254 byte payload.

    // Fill payload with random data.
    for (i = 0; i < 254; i++)
    {
        packet[i + 1] = rnd8();
    }
    NRF_RADIO->PACKETPTR = (uint32_t)packet;
}

void bt_radio_disable(void)
{
    NRF_RADIO->SHORTS          = 0;
    NRF_RADIO->EVENTS_DISABLED = 0;
#ifdef NRF51
    NRF_RADIO->TEST            = 0;
#endif
    NRF_RADIO->TASKS_DISABLE   = 1;
    while (NRF_RADIO->EVENTS_DISABLED == 0)
    {
        // Do nothing.
    }
    NRF_RADIO->EVENTS_DISABLED = 0;
}


/**
 * @brief Function for turning on the TX carrier test mode.
*/
void bt_tx_carrier(radio_param_t * params)
{
	uint8_t txpower, mode, channel;

	txpower = params[RADIO_POWER];
	mode = params[RADIO_MODE];
	channel = bt_band_freq[params[RADIO_BAND]];

    bt_radio_disable();
    NRF_RADIO->SHORTS     = RADIO_SHORTS_READY_START_Msk;
    NRF_RADIO->TXPOWER    = (txpower << RADIO_TXPOWER_TXPOWER_Pos);
    NRF_RADIO->MODE       = (mode << RADIO_MODE_MODE_Pos);
    NRF_RADIO->FREQUENCY  = channel;
#ifdef NRF51
 /*   NRF_RADIO->TEST       = (RADIO_TEST_CONST_CARRIER_Enabled << RADIO_TEST_CONST_CARRIER_Pos) \
                            | (RADIO_TEST_PLL_LOCK_Enabled << RADIO_TEST_PLL_LOCK_Pos);*/
#endif
    NRF_RADIO->TASKS_TXEN = 1;
}

/**
 * @brief Function for starting modulated TX carrier by repeatedly sending a packet with random address and
 * random payload.
*/
void bt_modulated_tx_carrier(radio_param_t * params)
{
	uint8_t txpower, mode, channel;

	txpower = params[RADIO_POWER];
	mode = params[RADIO_MODE];
	channel = bt_band_freq[params[RADIO_BAND]];

    bt_radio_disable();
    generate_modulated_rf_packet();
    NRF_RADIO->SHORTS     = RADIO_SHORTS_END_DISABLE_Msk | RADIO_SHORTS_READY_START_Msk | \
                            RADIO_SHORTS_DISABLED_TXEN_Msk;
    NRF_RADIO->TXPOWER    = (txpower << RADIO_TXPOWER_TXPOWER_Pos);
    NRF_RADIO->MODE       = (mode << RADIO_MODE_MODE_Pos);
    NRF_RADIO->FREQUENCY  = channel;
    NRF_RADIO->TASKS_TXEN = 1;
}


/**
 * @brief Function for turning on RX carrier.
*/
void bt_rx_carrier(radio_param_t *test_params)
{
	uint8_t channel;

	channel = bt_band_freq[test_params[RADIO_BAND]];
	
    bt_radio_disable();
    NRF_RADIO->SHORTS     = RADIO_SHORTS_READY_START_Msk;
    NRF_RADIO->FREQUENCY  = channel;
    NRF_RADIO->TASKS_RXEN = 1;
}

fcc_bt_error_t bt_param_fault_test(radio_param_t * params)
{
	return BT_ERR_OK;
}

void bt_run_radio_test(radio_param_t * params)
{	
	fcc_bt_error_t param_error;
	param_error = bt_param_fault_test(params);
	if (BT_ERR_OK == param_error) 
	{
		switch(params[RADIO_TEST])
		{
			case TX_CARRIER:
				bt_tx_carrier(params);
				break;
			case TX_MODULATED:
				bt_modulated_tx_carrier(params);
				break;
			case RX_CARRIER:
				bt_rx_carrier(params);
				break;
			default:
				break;
		}
	}
}

void bt_test_init(void){
	/*from "init()" of nrf test example*/
    NRF_RNG->TASKS_START = 1;

    // Start 16 MHz crystal oscillator
    //dk_todo: does this belong in startup thread?
    NRF_CLOCK->EVENTS_HFCLKSTARTED  = 0;
    NRF_CLOCK->TASKS_HFCLKSTART     = 1;

    // Wait for the external oscillator to start up
    while (NRF_CLOCK->EVENTS_HFCLKSTARTED == 0)
    {
        // Do nothing.
    }
    //todo: make sure pa_lna_init does does this, then reset
    nrf_gpio_pin_set(FEM_TX_EN_PIN);
    nrf_gpio_pin_clear(FEM_RX_EN_PIN);
	nrf_gpio_cfg_output(FEM_TX_EN_PIN);
	nrf_gpio_cfg_output(FEM_RX_EN_PIN);
	nrf_gpio_pin_clear(FEM_BYPASS_PIN);
	nrf_gpio_cfg_output(FEM_BYPASS_PIN);

	/*if (pa_lna_init(FEM_TX_EN_PIN, FEM_RX_EN_PIN)) {
		printf("\r\nPA/LNA enabled.\r\n");
	}*/
}







