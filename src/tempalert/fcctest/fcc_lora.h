//#ifndef _FCC_LORA
//#define _FCC_LORA
#pragma once

typedef enum fcc_lora_error {
	LORA_ERR_OK = 0,
	LORA_ERR_PARAM_FAIL,//generic fail for now
} fcc_lora_error_t;


void lora_test_init(void);
void lora_run_radio_test(radio_param_t * params);
void lora_radio_disable(void);

//#endif
