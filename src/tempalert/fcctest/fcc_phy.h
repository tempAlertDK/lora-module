#ifndef _FCC_PHY_H
#define	_FCC_PHY_H

typedef struct g_phy_led_indications {
	uint8_t radio_select;
	uint8_t radio_test;
	uint8_t radio_band;
}g_phy_led_indications_t;

void phy_led_update(radio_param_t * params);
void phy_radio_band_button_press(void);
void phy_radio_test_button_press(void);
void phy_radio_select_button_press(void);
#endif
