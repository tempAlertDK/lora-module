#include <stdint.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include "fcc_control.h"
#include "fcc_phy.h"
#include "boards.h"

static radio_param_t g_phy_led_indications[NUM_PARAMS];

void phy_led_update(radio_param_t * params) {
	volatile uint8_t temp_u8;
//todo: move this
	nrf_gpio_cfg_output(GPIO1);
	nrf_gpio_cfg_output(GPIO2);
	nrf_gpio_cfg_output(GPIO3);
	nrf_gpio_cfg_output(GPIO4);
	nrf_gpio_cfg_output(LED1_PIN);
	nrf_gpio_cfg_output(LED2_PIN);

	memcpy(g_phy_led_indications, params, sizeof(params[0])*NUM_PARAMS);

	temp_u8 = g_phy_led_indications[RADIO_TEST];

	switch(temp_u8)
	{
		case TX_CARRIER:
			nrf_gpio_pin_set(LED1_PIN);
			nrf_gpio_pin_set(LED2_PIN);
			break;
		case TX_MODULATED:
			nrf_gpio_pin_clear(LED1_PIN);
			nrf_gpio_pin_set(LED2_PIN);
			break;
		case RX_CARRIER:
			nrf_gpio_pin_clear(LED1_PIN);
			nrf_gpio_pin_clear(LED2_PIN);
			break;
		default:
			break;
	}

	temp_u8 = g_phy_led_indications[RADIO_SELECT];

	switch(temp_u8)
	{
		case SELECT_BT:
			nrf_gpio_pin_set(GPIO3);
			nrf_gpio_pin_clear(GPIO4);
			break;
		case SELECT_LORA:
			nrf_gpio_pin_clear(GPIO3);
			nrf_gpio_pin_set(GPIO4);
			break;
		default:
			break;
	}

	temp_u8 = g_phy_led_indications[RADIO_BAND];

	switch(temp_u8)
	{
		case LOW_BAND:
			nrf_gpio_pin_set(GPIO1);
			nrf_gpio_pin_set(GPIO2);
			break;
		case MED_BAND:
			nrf_gpio_pin_clear(GPIO1);
			nrf_gpio_pin_set(GPIO2);
			break;
		case HIGH_BAND:
			nrf_gpio_pin_clear(GPIO1);
			nrf_gpio_pin_clear(GPIO2);
			break;
		default:
			break;
	}

}

void phy_radio_band_button_press(void) {
	radio_control_message_t band_update;
	uint8_t new_radio_band;

	new_radio_band = g_phy_led_indications[RADIO_BAND];
	new_radio_band++;

	if (new_radio_band >= NUM_BANDS) 
	{
		new_radio_band = 0;
	}

	band_update.updated_field = RADIO_BAND;
	band_update.updated_value = new_radio_band;

	xQueueSend(radio_controlQueue, &band_update, pdMS_TO_TICKS(10));

}

void phy_radio_test_button_press(void) {
	radio_control_message_t test_update;
	uint8_t new_radio_test;


	new_radio_test = g_phy_led_indications[RADIO_TEST];
	new_radio_test++;

	if (new_radio_test >= NUM_TESTS) 
	{
		new_radio_test = 0;
	}

	test_update.updated_field = RADIO_TEST;
	test_update.updated_value = new_radio_test;

	xQueueSend(radio_controlQueue, &test_update, pdMS_TO_TICKS(10));

}

void phy_radio_select_button_press(void) {
	radio_control_message_t select_update;
	uint8_t new_radio_select;

	new_radio_select = g_phy_led_indications[RADIO_SELECT];
	new_radio_select++;

	if (new_radio_select >= 2)
	{
		new_radio_select = 0;
	}

	select_update.updated_field = RADIO_SELECT;
	select_update.updated_value = new_radio_select;

	xQueueSend(radio_controlQueue, &select_update, pdMS_TO_TICKS(10));
}
