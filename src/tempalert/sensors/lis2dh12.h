/*
 * lis2dh12.h
 *
 *  Created on: Jan 15, 2017
 *      Author: kwgilpin
 */

#ifndef SRC_TEMPALERT_SENSORS_LIS2DH12_H_
#define SRC_TEMPALERT_SENSORS_LIS2DH12_H_

bool lis2dh12_init(void);
bool lis2dh12_activeLowInterruptPins(void);

#endif /* SRC_TEMPALERT_SENSORS_LIS2DH12_H_ */
