/*
 * ads1013.c
 *
 *  Created on: Feb 9, 2017
 *      Author: Nate-Tempalert
 */

#include "SEGGER_RTT.h"
#include "app_util_platform.h"
#include "nordic_common.h"
#include "nrf.h"
#include "boards.h"
#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "app_error.h"
#include "FreeRTOS.h"
#include "task.h"
#include "app_twi.h"
#include "twi_register.h"
#include "twi_mtx.h"
#include "ads1013.h"

#define VREF_MV				1250		// mV
#define RESISTOR_HIGH	 	100			// kOhm
#define RESISTOR_LOW		20			// kOhm
#define ADS1013_BITS		((1<<12)-1)	// Bits

bool ads1013_readBatteryVoltage(uint16_t * p_Vbatt) {
    /* To read the battery voltage:
     * 1.) Enable VBATEN_PIN, make it an output then set HIGH
     * 2.) Write to the config register of the ADS1013 to enable a conversion
     * 3.) Read the conversion register 
     * 4.) Disable VBATEN_PIN and make it a input again 
     * */
	bool success = true;
	uint8_t raw_data[2];
	int16_t vbatt_raw = 0;
	int32_t vbatt_tmp = 0;

	WITH_TWI_MTX() {
    
    /* Set ACPG_VBATEN_PIN high to enable the resistor-divider that feeds the
     * ADC.  After the conversion is complete, we need to make this pin an
     * input again so that we can sense the state of ACPG. */
	nrf_gpio_pin_dir_set(ACPG_VBATEN_PIN, 1); //it's an output
	nrf_gpio_pin_set(ACPG_VBATEN_PIN); // set to enable

	vTaskDelay(pdMS_TO_TICKS(100));
	/* Assume that the TWI bus has been claimed using twi_smphr,
     * write to the config register. */

	uint8_t twi_write[3];
	twi_write[0] = ADS1013_CONFIG_REG;
	twi_write[1] = ONE_SHOT_CONV_BIT | ONE_SHOT_MODE_BIT;
	twi_write[2] = DR_1600SPS;

	/* TWI Write 16Bit config reg */
	if (success) {
		success = twiRegister_writeData(&config, ADS1013_TWI_ADDR, twi_write,sizeof(twi_write));
	}
	
	vTaskDelay(pdMS_TO_TICKS(10));

    /* Read the conversion register */
	if (success) {
		success = twiRegister_readData(&config, ADS1013_TWI_ADDR, ADS1013_CONV_REG, raw_data,ADS1013_REG_SIZE);
	} else {
		SEGGER_RTT_printf(0, "I2C communication failed write ADS1013_CONFIG_REG!\r\n");
	}

	if (success) {

		/* combine 8 bit conversion registers */
		vbatt_raw = (( ( ( uint16_t ) raw_data[0] ) << 8) | (( uint16_t ) raw_data[1] )) >> 4;
		/* convert to measured voltage */
		vbatt_tmp = (((int32_t) vbatt_raw) * (ADS1013_FSR)) / ((1 << (ADS1013_RESOLUTION-1))-1);
		/* apply resistor divider scaling factor */
		vbatt_tmp *= (RESISTOR_HIGH + RESISTOR_LOW) / RESISTOR_LOW;
		/* return converted value via provided pointer */
		*p_Vbatt = (uint16_t)vbatt_tmp;

	} else {
		SEGGER_RTT_printf(0, "I2C communication failed read ADS1013_CONV_REG!\r\n");
	}

	vTaskDelay(pdMS_TO_TICKS(10));

	nrf_gpio_cfg_input(ACPG_VBATEN_PIN, NRF_GPIO_PIN_NOPULL);

	}

	return success;

}
