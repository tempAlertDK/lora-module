/*
 * ads7866.h
 *
 *  Created on: Feb 9, 2017
 *      Author: Nate-Tempalert
 */

#ifndef SRC_TEMPALERT_SENSORS_ADS7866_H_
#define SRC_TEMPALERT_SENSORS_ADS7866_H_

#include <stdint.h>
#include <string.h>

bool ads7866_readBatteryVoltage(uint16_t * p_Vbatt);

#endif /* SRC_TEMPALERT_SENSORS_ADS7866_H_ */
