/*
 * capwand.c
 *
 *  Created on: Jan 4, 2017
 *      Author: Nate-Tempalert
 */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "app_twi.h"
#include "app_util_platform.h"

#include "SEGGER_RTT.h"

#include "boards.h"
#include "nrf_delay.h"
#include "fdc221x.h"
#include "capwand.h"
#include "sensors.h"
#include "extsensor.h"
#include "ee24aaxx.h"
#include "globals.h"
#include "log.h"

#define CAL_INIT_CAP_EMPTY		160.0
#define CAL_INIT_CAP_FULL		235.0
#define MAX_LEVEL				100
#define MIN_LEVEL				0
#define MIN_INDEX 				0
#define CALIBRATION_POINTS 		2U
#define MAX_INDEX				CALIBRATION_POINTS-1
#define MAX_CAPACITANCE 			16777215UL

const char ERROR_STRINGS[][48] = {
	"CAPWAND_CALIBRATION_INIT_REQUIRED",
	"CAPWAND_SUCCESS",	// 0
	"CAPWAND_FAILURE",
	"CAPWAND_ERROR_BAD_PARAM",
	"CAPWAND_ERROR_NO_SENSOR",
	"CAPWAND_ERROR_SENSOR_TWI",
	"CAPWAND_ERROR_CALIBRATION_INVALID",
	"CAPWAND_ERROR_CALIBRATION_ERROR"
};

#define ERROR_STR(err) ERROR_STRINGS[1+err*-1]

typedef struct {
	int8_t level_pct;
	double capacitance_pF;
} CalPoint_t;

typedef struct {
	CalPoint_t calPoints[CALIBRATION_POINTS];
	uint32_t tableValid;
	uint32_t cDummy32; // included to support pre 1.30 sensors
} CapwandCalibrationTable_t;

#define VALID_CALIBRATION_TABLE_KEY			0x12345678

CapwandCalibrationTable_t m_CalibrationTable;

/**
 * Helper function to print current state of calibration table.
 *
 * param[in] index	argument to indicate update calibration point
 *
*/
void capwand_printTable(uint8_t index) {
	uint8_t i;
	log_write("\r\nCurrent Calibration Table for FDC221x\r\n");
	log_write("Level (%%)   Capcitance (pF)   Status \r\n");

	uint32_t integer,decimal;
	int8_t lvl;

	for (i = 0; i < CALIBRATION_POINTS; i++) {
		lvl = m_CalibrationTable.calPoints[i].level_pct;
		integer = (uint32_t)m_CalibrationTable.calPoints[i].capacitance_pF;
		decimal = (uint32_t)(m_CalibrationTable.calPoints[i].capacitance_pF*100) - (integer*100);

		log_write(" %3d        %3u.%02upF ",lvl,integer,decimal);
		if (index == i) {
			log_write("          U  \r\n"); // updated point
		} else {
			log_write("\r\n");
		}
	}
	log_write("\r\n");
}



/**
 * Initialize a calibration table to default values
 * 
 * param[out] calTable	Calibration table to init
*/
static void capwand_initCalibrationTable(CapwandCalibrationTable_t * calTable) {
	/* Initialize the calibration table to default values */
	calTable->tableValid = 0; // invalidate table (i.e. not calibrated)
	calTable->calPoints[0].level_pct = 0; // DEFAULT LOW
	calTable->calPoints[0].capacitance_pF = CAL_INIT_CAP_EMPTY;
	calTable->calPoints[1].level_pct = 100; // DEFAULT HGIH
	calTable->calPoints[1].capacitance_pF = CAL_INIT_CAP_FULL;
}

/**
 * Detect the presence of the FDC2122 chip
 * return true on detection, false on failure
*/
static bool capwand_isConnected(void) {
	uint16_t id;
	/* Allow 10ms for device to power-up */
	vTaskDelay(pdMS_TO_TICKS(10)); 
	/* Attempt to locate device, call to HAL to request and verify manufacturer ID */
	if (fdc221x_getManufacturerID(&id) && (id == FDC221X_MAN_ID)) {
		return true;	
	} else {
		return false;
	}
}

/**
 * Read calibration table from external EEPROM
 * 
 * param[out] calTable	variable to return calibration table
 * 
 * return CAPWAND_SUCCESS, CAPWAND_NO_SENSOR, CAPWAND_CALIBRATION_ERROR, CAPWAND_SENSOR_TWI_ERROR, CAPWAND_ERROR_BAD_PARAM
*/
static int capwand_readCalibrationTable(CapwandCalibrationTable_t *calTable) {
	EXT_SENSOR() {
		// detect sensor
		if (!capwand_isConnected()) {
			return CAPWAND_NO_SENSOR;
		}
		// read calibration table
		if (!ee24aaxx_read(0,(uint8_t *)calTable,sizeof(CapwandCalibrationTable_t))) {
			return CAPWAND_ERROR_SENSOR_TWI;
		}
		// validate stored table
		if (calTable->tableValid != VALID_CALIBRATION_TABLE_KEY) {
			return CAPWAND_ERROR_CALIBRATION_INVALID;
		}
	}
	return CAPWAND_SUCCESS;
}

static int capwand_writeCalibrationTable(CapwandCalibrationTable_t *calTable) {
	EXT_SENSOR() {
		// detect sensor
		if (!capwand_isConnected()) {
			return CAPWAND_NO_SENSOR;
		}
		// write calibration table
		if (!ee24aaxx_write(0,(uint8_t *)calTable,sizeof(CapwandCalibrationTable_t))) {
			return CAPWAND_ERROR_SENSOR_TWI;
		}
	}
	return CAPWAND_SUCCESS;
}


/**
 * Read the capacitance sensor
 * 
 * return CAPWAND_SUCCESS, CAPWAND_NO_SENSOR, CAPWAND_SENSOR_TWI_ERROR
*/
static int capwand_readCapacitance(double *capacitance_pF) {
	uint32_t raw_data;
	EXT_SENSOR() {
		// detect sensor
		if (!capwand_isConnected()) {
			return CAPWAND_NO_SENSOR;
		}

		// configure device and enable conversion
		if (!fdc221x_configureDevice() || !fdc221x_enableConvertion()) {
			return CAPWAND_ERROR_SENSOR_TWI;
		}

		// let device warm up and then take a reading
		vTaskDelay(pdMS_TO_TICKS(50));
		if (!fdc221x_readDataChannel(0,&raw_data)) {
			return CAPWAND_ERROR_SENSOR_TWI;
		}

		fdc221x_disableConvertion(); // no need to check for error, it will be powered down anyway
		vTaskDelay(pdMS_TO_TICKS(1));
	}
	// convert to fF
	*capacitance_pF = fdc221x_convert_raw2pF(raw_data);
	return CAPWAND_SUCCESS;
}


/**
 * Convert capacitance to level using associated calibration table
 * 
 * param[in] capacitance	Capacitance value to convert
 * param[in] calTable		Pointer to calibration data table
 * 
 * return level as a percentage [0-100%]
*/
static int8_t capwand_calculateLevel(double capacitance, CapwandCalibrationTable_t *calTable) {

	double C_x = capacitance;

	//CURRENT LEVEL = Lv_min + (C_x - C_min) *  (Lv_max - Lv_min) /  (C_max - C_min)
	//TODO: Implement as either LUT and/or integer math
	double C_min = calTable->calPoints[0].capacitance_pF;
	double C_max = calTable->calPoints[1].capacitance_pF;
	int8_t Lv_min = calTable->calPoints[0].level_pct;
	int8_t Lv_max = calTable->calPoints[1].level_pct;

	//Calculate the current level from the read capacitance value.
	int16_t Lv_x = Lv_min + (int16_t)((C_x - C_min) *  ((double)(Lv_max - Lv_min)) /  (C_max - C_min));

	//Clamp to real percentage. TODO: otherwise flag warning?
	if (Lv_x > 100) {
		Lv_x = 100;
	} else if (Lv_x < 0) {
		Lv_x = 0;
	}

	if (g_simLevel) {
		log_debug("Level simulation active");
		return g_simLevelPercent;
	}
	return Lv_x;
}





/**
 * see .h
*/
int capwand_readSensor(int8_t *level, uint32_t *capacitance_fF) {
	double cap_pF;
	int8_t lvl;
	int err;

	// validate params 
	if (level == NULL) {
		log_error("Invalid function call!");
		return CAPWAND_ERROR_BAD_PARAM;
	}

	// read sensor capacitance
	err = capwand_readCapacitance(&cap_pF);
	if (err != CAPWAND_SUCCESS) {
		log_debugf("Capwand read returned error! [%s]",ERROR_STR(err));
		return err;
	}

	// read calibration data table
	err = capwand_readCalibrationTable(&m_CalibrationTable);
	if (err != CAPWAND_SUCCESS) {
		// on error, we report the error but continue to calculate the level
		log_warnf("Capwand calibration error: [%s], Using default table",ERROR_STR(err));
		capwand_initCalibrationTable(&m_CalibrationTable);
	}

	// calculate the level from capacitance
	lvl = capwand_calculateLevel(cap_pF,&m_CalibrationTable);
	
	// Update return values. If function was called with a valid 
	// pointer for capacitance, return the collected value
	*level = lvl;
	if (capacitance_fF != NULL)  {
		if (g_simLevel) {
			*capacitance_fF = g_simCapacitance;
		} else {
			*capacitance_fF = (uint32_t) (cap_pF*1000);			
		}
	}

	return CAPWAND_SUCCESS;
}


/**
 * see .h
*/
int capwand_readCalibration(uint8_t point, int8_t *level_pct, uint32_t *capacitance_fF) {
	int err;
	if ((level_pct == NULL) || (capacitance_fF == NULL) || (point >= CALIBRATION_POINTS)) {
		return CAPWAND_ERROR_BAD_PARAM;
	}

	//read calibration table
	err = capwand_readCalibrationTable(&m_CalibrationTable);
	if (err != CAPWAND_SUCCESS) {
		log_errorf("Capwand calibration read error: [%s], Using default table", ERROR_STR(err));
		capwand_initCalibrationTable(&m_CalibrationTable);
	}
	
	// return calibration stuff
	*level_pct = m_CalibrationTable.calPoints[point].level_pct;
	*capacitance_fF = (uint32_t)(m_CalibrationTable.calPoints[point].capacitance_pF * 1000);

	return err;
}


/**
 * see .h
*/
int capwand_writeCalibration(uint8_t point, int8_t level_pct, uint32_t capacitance_fF) {
	int err;
	bool initCalibration = false;
	if (((level_pct < 0) || (level_pct > 100)) || (capacitance_fF > (MAX_CAPACITANCE)) || (point >= CALIBRATION_POINTS)) {
		return CAPWAND_ERROR_BAD_PARAM;
	}

	// read current calibration data table
	err = capwand_readCalibrationTable(&m_CalibrationTable);
	if (err != CAPWAND_SUCCESS) {
		if (err != CAPWAND_ERROR_CALIBRATION_INVALID) {
			log_errorf("Capwand calibration error! [%s]",ERROR_STR(err));
			return err;
		}
		log_warn("Capwand calibration invalid. Using default table");
		capwand_initCalibrationTable(&m_CalibrationTable);
		initCalibration = true;
	}

	// update calibration table and validate for future use
	m_CalibrationTable.calPoints[point].level_pct = level_pct;
	m_CalibrationTable.calPoints[point].capacitance_pF = ((double)(capacitance_fF)/1000);
	m_CalibrationTable.tableValid = VALID_CALIBRATION_TABLE_KEY;

	// write new calibration table to sensor
	err = capwand_writeCalibrationTable(&m_CalibrationTable);
	if (err != CAPWAND_SUCCESS) {
		log_errorf("Capwand calibration write error! [%s]",ERROR_STR(err));
		return err;
	}

	// verify write
	CapwandCalibrationTable_t tmpTable;
	err = capwand_readCalibrationTable(&tmpTable);
	if (err != CAPWAND_SUCCESS) {
		log_errorf("Capwand calibration verify error! [%s]",ERROR_STR(err));
		return err;
	}
	if (memcmp(&tmpTable,&m_CalibrationTable,sizeof(CapwandCalibrationTable_t))!=0) {
		log_error("Capwand written data does not match!");
	}

	// print calibration table 
	capwand_printTable(point);

	// if initialization was nessisary 
	if (initCalibration) {
		err = CAPWAND_CALIBRATION_INIT_REQUIRED;
	}
	return err;
}
