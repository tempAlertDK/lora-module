/*
 * ads1013.h
 *
 *  Created on: Feb 9, 2017
 *      Author: Nate-Tempalert
 */

#ifndef SRC_TEMPALERT_SENSORS_ADS1013_H_
#define SRC_TEMPALERT_SENSORS_ADS1013_H_

#include <stdint.h>
#include <string.h>

#define     ADS1013_TWI_ADDR    0x49
#define     ADS1013_CONV_REG    0x00
#define     ADS1013_CONFIG_REG  0x01
#define     ONE_SHOT_CONV_BIT   (0b1<<7)
#define     ONE_SHOT_MODE_BIT   (0b1<<0)
#define     DR_1600SPS          (0b100 << 4)
#define     ADS1013_FSR         2048
#define     ADS1013_RESOLUTION  12
#define     ADS1013_REG_SIZE    2

bool ads1013_readBatteryVoltage(uint16_t * p_Vbatt);

#endif /* SRC_TEMPALERT_SENSORS_ADS1013_H_ */
