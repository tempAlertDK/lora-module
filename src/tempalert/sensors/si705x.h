/*
 * si705x.h
 *
 *  Created on: Jul 11, 2016
 *      Author: kwgilpin
 */

#ifndef SRC_TEMPALERT_INC_SI705X_H_
#define SRC_TEMPALERT_INC_SI705X_H_

#include "stdint.h"
#include "stdbool.h"

bool si705x_getSerial(uint8_t *serial);
uint8_t si705x_getAccuracy_tenthDegC(void);
bool si705x_getTemp_tenthDegC(int16_t * p_temp);

#endif /* SRC_TEMPALERT_INC_SI705X_H_ */
