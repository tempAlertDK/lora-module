/*
 * ee4aaXX.h
 *
 *  Created on: July 25, 2017
 *      Author: Nate-Tempalert
 *
 *      Driver for TI capacitive sensor eeprom device
 *
 */

#ifndef SRC_TEMPALERT_SENSORS_EE24AAXX_H_
#define SRC_TEMPALERT_SENSORS_EE24AAXX_H_

//REG ADDRESSES

#define EE24AAXX_TWI_ADDR 			0x50
#define EE24AAXX_PAGE_SIZE           8
#define EE24AAXX_WRITE_DELAY_MS      5
#define EE24AAXX_MEMORY_SIZE_BYTES   128

bool ee24aaxx_write(uint16_t offset, uint8_t * data, uint16_t dataLength);
bool ee24aaxx_read(uint16_t offset, uint8_t * data, uint16_t dataLength);

#endif /* SRC_TEMPALERT_SENSORS_EE24AAXX_H_ */
