/*
 * fdc221x.c
 *
 *  Created on: Dec 29, 2016
 *      Author: Nate-Tempalert
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "app_twi.h"
#include "app_util_platform.h"

#include "SEGGER_RTT.h"
#include "boards.h"
#include "twi_register.h"
#include "twi_mtx.h"
#include "semphr.h"
#include "portmacro_cmsis.h"
#include "fdc221x.h"
#include "log.h"
/* Helper functions to write FDC 16Bit registers */
bool fdc221x_readReg(uint8_t reg, uint16_t * data);
bool fdc221x_writeReg(uint8_t reg, uint16_t data);

double fdc221x_convert_raw2pF(uint32_t raw_value)
{
	double data_raw = (double)raw_value;
#ifdef DOUBLE_ENDED
	double f_sensor = 1 * F_XTL * data_raw / (1 << 28);
	f_sensor = (4*PI*PI)*f_sensor*f_sensor;
	double cap_pf = (1 / (L_t * f_sensor)) * 1e12;
#else
	double f_sensor = 2 * F_XTL * data_raw / (1 << 28);
	double cap_pf = pow(( 1 / (PI * sqrt(L_t) * f_sensor) - sqrt( C_t ) ),2) - C_t;
	cap_pf *= 1e12;
#endif
	return cap_pf;
}


bool fdc221x_getManufacturerID(uint16_t *id) {
	bool success = false;

	WITH_TWI_MTX() {
		success = fdc221x_readReg(MANUFACTURER_ID, id);
	}

	return success;
}


bool fdc221x_configureDevice(void) {

	WITH_TWI_MTX() {
		/* Configure device */

		if (!fdc221x_writeReg(SETTLECOUNT_CH0,0x0F00)) {}
		else if (!fdc221x_writeReg(RCOUNT_CH0,0xFFFF)) {}
#ifdef DOUBLE_ENDED
		else if (!fdc221x_writeReg(CLOCK_DIVIDERS_CH0,0x1001)) {}
#else
		else if (!fdc221x_writeReg(CLOCK_DIVIDERS_CH0,0x2001)) {}
#endif
		else if (!fdc221x_writeReg(DRIVE_CURRENT_CH0,0x6500)) {}
		else if (!fdc221x_writeReg(MUX_CONFIG,0x020D)) {}
		else if (!fdc221x_writeReg(CONFIG,0x3601)) {}
		else {
			return true;
		}
	}

	#if defined (EXT_SENSOR_DEBUG) && EXT_SENSOR_DEBUG > 0
				SEGGER_RTT_printf(0, "I2C communication failed during config of FDC221x!\r\n");
#endif
	return false;

}



bool fdc221x_enableConvertion(void) {
	uint16_t config_reg_value = 0;
	uint16_t new_config_reg_value = 0;

	WITH_TWI_MTX() {
		/* Read current register value */
		if (!fdc221x_readReg(CONFIG,&config_reg_value)) {
#if defined (EXT_SENSOR_DEBUG) && EXT_SENSOR_DEBUG > 0
			SEGGER_RTT_printf(0, "FDC221X I2C READ FAILURE! (enableConvertion)\r\n");
#endif
			return false;
		}

		/* Clear SLEEP_ENABLE bit and update register value */
		config_reg_value &= ~(SLEEP_MODE_ENABLE_BIT);
		if(!fdc221x_writeReg(CONFIG,config_reg_value)) {
#if defined (EXT_SENSOR_DEBUG) && EXT_SENSOR_DEBUG > 0
			SEGGER_RTT_printf(0, "WRITE I2C READ FAILURE! (enableConvertion)\r\n");
#endif
			return false;
		}

		/* Confirm conversion was enabled */
		if (!fdc221x_readReg(CONFIG,&new_config_reg_value) || (config_reg_value != new_config_reg_value)) {
#if defined (EXT_SENSOR_DEBUG) && EXT_SENSOR_DEBUG > 0
			SEGGER_RTT_printf(0, "FDC221X I2C UPDATE FAILURE! [%04x!=%04x] (enableConvertion)\r\n",config_reg_value,new_config_reg_value);
#endif
			return false;
		}
	}

	return true;
}



bool fdc221x_disableConvertion(void) {

	uint16_t config_reg_value = 0;
	uint16_t new_config_reg_value = 0;

	WITH_TWI_MTX() {
		/* Read current register value */
		if (!fdc221x_readReg(CONFIG,&config_reg_value)) {
#if defined (EXT_SENSOR_DEBUG) && EXT_SENSOR_DEBUG > 0
			SEGGER_RTT_printf(0, "FDC221X I2C READ FAILURE! (fdc221x_disableConvertion)\r\n");
#endif
			return false;
		}

		/* Set SLEEP_ENABLE bit and update register value */
		config_reg_value |= SLEEP_MODE_ENABLE_BIT;
		if(!fdc221x_writeReg(CONFIG,config_reg_value)) {
#if defined (EXT_SENSOR_DEBUG) && EXT_SENSOR_DEBUG > 0
			SEGGER_RTT_printf(0, "FDC221X I2C WRITE FAILURE! (fdc221x_disableConvertion)\r\n");
#endif
			return false;
		}

		/* Confirm conversion was disabled */
		if (!fdc221x_readReg(CONFIG,&new_config_reg_value) || (config_reg_value != new_config_reg_value)) {
#if defined (EXT_SENSOR_DEBUG) && EXT_SENSOR_DEBUG > 0
			SEGGER_RTT_printf(0, "FDC221X I2C UPDATE FAILURE! [%04x!=%04x] (enableConvertion)\r\n",config_reg_value,new_config_reg_value);
#endif
			return false;
		}
	}

	return true;
}



bool fdc221x_readDataChannel(uint8_t channel, uint32_t * raw) {
	uint32_t temp_data = 0;
	uint16_t temp_data_msb = 0;
	uint16_t temp_data_lsb = 0;

	WITH_TWI_MTX() {
		/* Read the raw count value converted for channel 0 (28bits)
		 * TODO: Allow for multi-channel support. */
		if (!fdc221x_readReg(DATA_CH0, &temp_data_msb) || !fdc221x_readReg(DATA_LSB_CH0, &temp_data_lsb)) {
			return false;
		}
	}

	/* Mask out warning bits for now
	 * TODO: report warning/error bits. */
	temp_data = ((temp_data_msb & (0x0FFF)) << 16) | (temp_data_lsb & 0x0000FFFF);

	*raw = temp_data;

	return true;
}






/* TWI comm functions */
bool fdc221x_readReg(uint8_t reg, uint16_t * data) {
	bool success = true;

	uint8_t twi_data[2];
	/* TWI Read 16Bit Reg */
	if (success) {
		success = twiRegister_readData(&TWI_CONFIG, FDC_TWI_ADDR, reg, twi_data, sizeof(twi_data));
	}

	if (!success) {
#if defined (EXT_SENSOR_DEBUG) && EXT_SENSOR_DEBUG > 1
		SEGGER_RTT_printf(0, "I2C communication failed write communicate with FDC221x! at Address 0x%02x\r\n", reg);
#endif
		return false;
	}

	*data = (twi_data[0] << 8) | twi_data[1];

	return true;
}




bool fdc221x_writeReg(uint8_t reg, uint16_t data) {
	bool success = true;

	uint8_t twi_write[3];
	twi_write[0] = reg;
	twi_write[1] = (data >> 8) & 0x00FF;
	twi_write[2] = (data) & 0x00FF;

	/* TWI Write 16Bit Reg */
	if (success) {
		success = twiRegister_writeData(&TWI_CONFIG, FDC_TWI_ADDR, twi_write,sizeof(twi_write));
	}

	if (!success) {
#if defined (EXT_SENSOR_DEBUG) && EXT_SENSOR_DEBUG > 1
		SEGGER_RTT_printf(0, "I2C communication failed write communicate with FDC221x! at Address 0x%02x\r\n", reg);
#endif
		return false;
	}

	return true;
}
