/*
 * fdc221x.h
 *
 *  Created on: Dec 29, 2016
 *      Author: Nate-Tempalert
 *
 *      Driver for TI capacitive sensor series FDC221x
 *
 */

#ifndef SRC_TEMPALERT_SENSORS_FDC221X_H_
#define SRC_TEMPALERT_SENSORS_FDC221X_H_

//REG ADDRESSES
#define  DATA_CH0				0x00
#define  DATA_LSB_CH0			0x01
#define  DATA_CH1				0x02
#define  DATA_LSB_CH1			0x03
#define  RCOUNT_CH0				0x08
#define  RCOUNT_CH1				0x09
#define  SETTLECOUNT_CH0		0x10
#define  SETTLECOUNT_CH1		0x11
#define  CLOCK_DIVIDERS_CH0		0x14
#define  CLOCK_DIVIDERS_CH1		0x15
#define  STATUS					0x18
#define  ERROR_CONFIG			0x19
#define  CONFIG					0x1A
#define  MUX_CONFIG				0x1B
#define  RESET_DEV				0x1C
#define  DRIVE_CURRENT_CH0		0x1E
#define  DRIVE_CURRENT_CH1		0x1F
#define  MANUFACTURER_ID		0x7E
#define  DEVICE_ID				0x7F

#define  FDC_TWI_ADDR 			0x2B

// CONFIG REG BITS
#define  SLEEP_MODE_ENABLE_BIT		(1<<13)


// Global Calibration Variables
#define  FDC221X_MAN_ID			0x5449
#define  F_XTL 					25000000.0F // External Oscilator Freq
#define  L_t			 		22e-6F // Tank Inductance
#define  C_t			 		48e-12F // Tank Capacitance
#define  PI						3.141592F
#define  SINGLE_ENDED           1            
//#define  DOUBLE_ENDED           1            

bool fdc221x_getManufacturerID(uint16_t *id);
double fdc221x_convert_raw2pF(uint32_t raw);
bool fdc221x_configureDevice(void);
bool fdc221x_readDataChannel(uint8_t channel, uint32_t * raw);
bool fdc221x_disableConvertion(void);
bool fdc221x_enableConvertion(void);

#endif /* SRC_TEMPALERT_SENSORS_FDC221X_H_ */
