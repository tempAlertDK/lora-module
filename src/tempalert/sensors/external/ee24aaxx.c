/*
 * fdc221x.c
 *
 *  Created on: July 25, 2017
 *      Author: Nate-Tempalert
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "app_twi.h"
#include "app_util_platform.h"

#include "SEGGER_RTT.h"
#include "boards.h"
#include "FreeRTOS.h"
#include "task.h"
#include "twi_register.h"
#include "twi_mtx.h"

#include "semphr.h"
#include "portmacro_cmsis.h"
#include "ee24aaxx.h"

bool ee24aaxx_write(uint16_t offset, uint8_t * data, uint16_t dataLength) {
    bool success = true;
    if ((offset + dataLength) > EE24AAXX_MEMORY_SIZE_BYTES) {
        return false;
    }

    uint16_t dataPtr;
    uint8_t numBytes, twiWrite[1 + EE24AAXX_PAGE_SIZE]; // write address + EE24AAXX_MEMORY_SIZE_BYTES of data;

    for (dataPtr = 0; dataPtr < dataLength; dataPtr=dataPtr+8) {
        numBytes = (dataLength - dataPtr);
        if (numBytes > EE24AAXX_PAGE_SIZE) {
            numBytes = EE24AAXX_PAGE_SIZE;
        }
        // set the pointer to the desired memory address
        twiWrite[0] = offset + dataPtr;

        // copy data block to twi data container
        memcpy(&twiWrite[1],&data[dataPtr],numBytes);

        if (success) {
            success = twiRegister_writeData(&TWI_CONFIG, EE24AAXX_TWI_ADDR, twiWrite, sizeof(twiWrite));
        }

        if (!success) {
            SEGGER_RTT_printf(0, "I2C communication failed write communicate with EE24AAXX! at Address 0x%02x\r\n", dataPtr);
            return false;
        }
        vTaskDelay(pdMS_TO_TICKS(5));
    }
    return true;
}
bool ee24aaxx_read(uint16_t offset, uint8_t * data, uint16_t dataLength) {

    if ((offset + dataLength) > EE24AAXX_MEMORY_SIZE_BYTES) {
        return false;
    }

    bool success = true;

	/* TWI Read 16Bit Reg */
	if (success) {
		success = twiRegister_readData(&TWI_CONFIG, EE24AAXX_TWI_ADDR, offset, data, dataLength);
	}

	if (!success) {
		SEGGER_RTT_printf(0, "I2C communication failed write communicate with EE24AAXX! at Address 0x%02x\r\n", offset);
		return false;
	}
	return true;
}
