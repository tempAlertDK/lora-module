/*
 * lis2dh12.c
 *
 *  Created on: Jan 15, 2017
 *      Author: kwgilpin
 */

#include <stdint.h>
#include <stdbool.h>

#include "app_twi.h"
#include "app_util_platform.h"

#include "SEGGER_RTT.h"

#include "boards.h"
#include "twi_register.h"
#include "lis2dh12.h"

#define LIS2DH12_ADDRESS	0x19

static const nrf_drv_twi_config_t const lis2dh12_twi_config = {
		.scl = SCL_PIN,
		.sda = SDA_PIN,
		.frequency = NRF_TWI_FREQ_100K,
		.interrupt_priority = APP_IRQ_PRIORITY_LOW
};



bool lis2dh12_init() {
	bool success = true;
	uint8_t data;

	if (!twiRegister_read(&lis2dh12_twi_config, LIS2DH12_ADDRESS, 0x0F, &data) || (data != 0x33)) {
		SEGGER_RTT_printf(0, "ERROR: Failed to detect LIS2DH12 accelerometer\r\n");
		return false;
	}

	if (!success) {
		SEGGER_RTT_printf(0, "ERROR: Failed to initialize the LIS2DH12 accelerometer\r\n");
	}

	return success;
}


bool lis2dh12_activeLowInterruptPins() {
	bool success = true;

	/*
	 * Set active-low interrupt polarity, so that INT pins are high by default.
	 */
	if (success) {
		success = twiRegister_setBits(&lis2dh12_twi_config, LIS2DH12_ADDRESS, 0x25, 1<<1);
	}

	if (!success) {
		SEGGER_RTT_printf(0, "ERROR: Failed to configure INT pins of LIS2DH12 as active-low\r\n");
	}

	return success;
}
