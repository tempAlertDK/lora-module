/*
 * capwand.h
 *
 *  Created on: Jan 4, 2017
 *      Author: Nate-Tempalert
 */
#ifndef CAPWAND_H_
#define CAPWAND_H_

// #include <stdint.h>
// #include <stdbool.h>

// #define CAPWAND_INVALID_LEVEL						-1
// #define CAPWAND_INVALID_CALIBRATION_SINGLE_POINT		-1
// #define CAPWAND_INVALID_CALIBRATION_NO_POINTS		-2
// #define CAPWAND_INVALID_SENSOR_READING				-3

// #define CAPWAND_MAX_LEVEL					100
// #define CAPWAND_MIN_LEVEL					0

// #define CAPWAND_SENSOR_NOT_DETECTED				-3
// #define MIN_INDEX 							0
// #define MAX_INDEX							CALIBRATION_POINTS-1
// #define CALIBRATION_POINTS     				2U

// typedef struct {
// 	int8_t level;
// 	double capValue;
// } CalPoint_t;

// typedef struct {
// 	/* 0%, 1%-99%, 100%*/
// 	CalPoint_t calPoints[CALIBRATION_POINTS];
// 	uint32_t tableValid;
// 	uint32_t validPoints;
// } CapwandCalibrationTable_t;

// #define VALID_CALIBRATION_TABLE_KEY			0x12345678

// bool capwand_init(void);
// bool capwand_isPresent(void);
// bool capwand_isCalibrated(void);
// bool capwand_calibrate(uint8_t index, int8_t level);
// bool capwand_readLevel(int8_t * level, bool * isvalid);
// uint32_t capwand_getLastCapacitance(void);

// bool capwand_getCalibrationDataPoint(uint8_t point, uint8_t *data);
// bool capwand_setCalibrationDataPoint(uint8_t point, uint8_t *data);

/**
 * Returnable Error Codes for Capwand Sensor Module
 */
#define	CAPWAND_SUCCESS						0
#define	CAPWAND_FAILURE						-1
#define	CAPWAND_ERROR_BAD_PARAM				-2
#define CAPWAND_NO_SENSOR		    		-3
#define CAPWAND_ERROR_SENSOR_TWI			-4
#define CAPWAND_ERROR_CALIBRATION_INVALID	-5
#define CAPWAND_ERROR_CALIBRATION_ERROR		-6

/**
 * Returnable non failure codes
*/
#define CAPWAND_CALIBRATION_INIT_REQUIRED	1

/**
 * Attempt to read an capacitance sensor.
 * 
 * param[out] level			current level read by sensor
 * param[out] capacitance	current capacitance read by sensor
 * 
 * return 	CAPWAND_SUCCESS, CAPWANT_NO_SENSOR, CAPWANT_CALIBRATION_INVALID, CAPWAND_SENSOR_TWI_ERROR, CAPWAND_ERROR_BAD_PARAM
 */
extern int capwand_readSensor(int8_t *level, uint32_t *capacitance);

/**
 * Attempt to calibrate the sensor at a single point. This process requires 
 * a current level and capacitance value to apply to the calibration table
 * at the index provided. This data will then be saved to the external EEPROM
 * on the sensor.
 * 
 * param[in] point			desired calibration point index
 * param[in] level			level to calibrate with
 * param[in] capacitance	capacitance to calibrate with, in femptofarads
 * 
 * return 	CAPWAND_SUCCESS, CAPWAND_NO_SENSOR, CAPWAND_CALIBRATION_ERROR, CAPWAND_SENSOR_TWI_ERROR
 */
extern int capwand_writeCalibration(uint8_t point, int8_t level_pct, uint32_t capacitance_fF);


/**
 * Read a device calibration point
 * Allows the reading of a sensor's calibration data point, returning 
 * the level and capacitance saved on the sensor EEPROM at the requested point
 * 
 * param[in] point			point to collect calibration data from
 * param[out] level			level at calibration point
 * param[out] capacitance	capacitance at calibration point, in femptofarads
 * 
 * return 	CAPWAND_SUCCESS, CAPWAND_NO_SENSOR, CAPWAND_CALIBRATION_ERROR, CAPWAND_SENSOR_TWI_ERROR, CAPWAND_ERROR_BAD_PARAM
 */
extern int capwand_readCalibration(uint8_t point, int8_t *level_pct, uint32_t *capacitance_fF);

#endif //#define CAPWAND_H_
