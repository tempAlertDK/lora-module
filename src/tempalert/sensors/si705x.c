/*
 * si705x.c
 *
 *  Created on: Jul 11, 2016
 *      Author: kwgilpin
 */

#include <stdint.h>
#include <stdbool.h>

#include "app_twi.h"
#include "app_util_platform.h"

#include "SEGGER_RTT.h"

#include "boards.h"
#include "nrf_delay.h"
#include "FreeRTOS.h"
#include "task.h"
#include "twi_mtx.h"
#include "twi_register.h"
#include "semphr.h"
#include "portmacro_cmsis.h"
#include "si705x_crc.h"
#include "si705x.h"

#define MAX_PENDING_TWI_TRANSACTIONS	6

static uint8_t twiSerialACommands[2] = { 0xFA, 0x0F };
static uint8_t twiSerialBCommands[2] = { 0xFC, 0xC9 };
static uint8_t twiSerialA[8];
static uint8_t twiSerialB[6];

app_twi_t m_app_twi = APP_TWI_INSTANCE(0);

static uint8_t twiTempCommand = 0xE3;
static uint8_t twiTempData[3];

static int16_t m_localTempReading_tenthDegC = 0;
static void (*m_conversionCompleteCallback)( bool, int16_t);

bool si705x_getSerial(uint8_t *serial) {
	uint32_t err_code;
	uint8_t calculatedCrcA;
	uint8_t calculatedCrcB;

	WITH_TWI_MTX() {
		/* Configure the TWI driver */
		nrf_drv_twi_config_t const config = { .scl = SCL_PIN, .sda = SDA_PIN, .frequency = NRF_TWI_FREQ_100K,
				.interrupt_priority = APP_IRQ_PRIORITY_LOW };
		APP_TWI_INIT(&m_app_twi, &config, MAX_PENDING_TWI_TRANSACTIONS, err_code);
		APP_ERROR_CHECK(err_code);

		static app_twi_transfer_t const transfers[] = {
		APP_TWI_WRITE(0x40, &twiSerialACommands, sizeof(twiSerialACommands), APP_TWI_NO_STOP),
		APP_TWI_READ(0x40, twiSerialA, sizeof(twiSerialA), 0),
		APP_TWI_WRITE(0x40, &twiSerialBCommands, sizeof(twiSerialBCommands), APP_TWI_NO_STOP),
		APP_TWI_READ(0x40, twiSerialB, sizeof(twiSerialB), 0) };

	#ifdef BOARD_CELL_NODE_REV1P0
		/* Init and clear enable power pin ... delay? */
		nrf_gpio_cfg_output(INTSENSORENN_PIN);
		nrf_gpio_pin_clear(INTSENSORENN_PIN);
		vTaskDelay(pdMS_TO_TICKS(100));
	#endif

		err_code = app_twi_perform(&m_app_twi, transfers, sizeof(transfers) / sizeof(transfers[0]), NULL);

		app_twi_uninit(&m_app_twi);
	}

#ifdef BOARD_CELL_NODE_REV1P0
	nrf_gpio_pin_set(INTSENSORENN_PIN);
#endif

	if (err_code != NRF_SUCCESS) {
		SEGGER_RTT_printf(0, "I2C communication failed when serial number from Si705x\r\n");
		return false;
	}

	calculatedCrcA = si705x_crc_init();
	calculatedCrcA = si705x_crc_update(calculatedCrcA, &twiSerialA[0], 1);
	calculatedCrcA = si705x_crc_update(calculatedCrcA, &twiSerialA[2], 1);
	calculatedCrcA = si705x_crc_update(calculatedCrcA, &twiSerialA[4], 1);
	calculatedCrcA = si705x_crc_update(calculatedCrcA, &twiSerialA[6], 1);
	calculatedCrcA = si705x_crc_finalize(calculatedCrcA);

	calculatedCrcB = si705x_crc_init();
	calculatedCrcB = si705x_crc_update(calculatedCrcB, &twiSerialB[0], 1);
	calculatedCrcB = si705x_crc_update(calculatedCrcB, &twiSerialB[1], 1);
	calculatedCrcB = si705x_crc_update(calculatedCrcB, &twiSerialB[3], 1);
	calculatedCrcB = si705x_crc_update(calculatedCrcB, &twiSerialB[4], 1);
	calculatedCrcB = si705x_crc_finalize(calculatedCrcB);

	if ((calculatedCrcA != twiSerialA[7]) || (calculatedCrcB != twiSerialB[5])) {
		SEGGER_RTT_printf(0, "CRC check failed when reading serial number from Si705x\r\n");
		return false;
	}

	serial[0] = twiSerialA[0];
	serial[1] = twiSerialA[2];
	serial[2] = twiSerialA[4];
	serial[3] = twiSerialA[6];
	serial[4] = twiSerialB[0];
	serial[5] = twiSerialB[1];
	serial[6] = twiSerialB[3];
	serial[7] = twiSerialB[4];

	return true;
}

uint8_t si705x_getAccuracy_tenthDegC() {
	uint8_t serial[8];

	if (!si705x_getSerial(serial)) {
		return UINT8_MAX;
	}

	switch (serial[4]) {
	case 50:
		return 10;
		break;
	case 51:
		return 1;
		break;
	case 53:
		return 3;
		break;
	case 54:
		return 4;
		break;
	case 55:
		return 5;
		break;
	default:
		return UINT8_MAX;
		break;
	}

	return UINT8_MAX;
}

bool si705x_getTemp_tenthDegC(int16_t * p_temp) {
	uint32_t err_code;
	uint8_t calculatedCrc;
	int32_t tempCode;
	int32_t temp_hundredthsDegC;

	WITH_TWI_MTX() {

		APP_TWI_INIT(&m_app_twi, &config, MAX_PENDING_TWI_TRANSACTIONS, err_code);
		APP_ERROR_CHECK(err_code);

		/* Issue the command to start a hold conversion.  That is, while the
		 * conversion is in progress, the Si705x will stretch the I2C
		 * clock.  */
		static app_twi_transfer_t const transfers[] = {
		APP_TWI_WRITE(0x40, &twiTempCommand, 1, APP_TWI_NO_STOP),
		APP_TWI_READ(0x40, twiTempData, sizeof(twiTempData), 0) };

	#ifdef BOARD_CELL_NODE_REV1P0
		/* Init and clear enable power pin ... delay? */
		nrf_gpio_cfg_output(INTSENSORENN_PIN);
		nrf_gpio_pin_clear(INTSENSORENN_PIN);
		vTaskDelay(pdMS_TO_TICKS(100));

	#endif

		err_code = app_twi_perform(&m_app_twi, transfers, sizeof(transfers) / sizeof(transfers[0]), NULL);

		app_twi_uninit(&m_app_twi);

	#ifdef BOARD_CELL_NODE_REV1P0
		nrf_gpio_pin_set(INTSENSORENN_PIN);
	#endif

		if (err_code != NRF_SUCCESS) {
			SEGGER_RTT_printf(0, "I2C communication failed to communicate with Si705x!\r\n");
			return false;
		}

		calculatedCrc = si705x_crc_init();
		calculatedCrc = si705x_crc_update(calculatedCrc, &twiTempData[0], 1);
		calculatedCrc = si705x_crc_update(calculatedCrc, &twiTempData[1], 1);
		calculatedCrc = si705x_crc_finalize(calculatedCrc);

		if (calculatedCrc != twiTempData[2]) {
			SEGGER_RTT_printf(0, "CRC check failed when reading temperature from Si705x\r\n");
			m_conversionCompleteCallback(false, 0);
			return false;
		}

		tempCode = ((uint16_t) twiTempData[0] << 8) | (uint16_t) twiTempData[1];

		temp_hundredthsDegC = (17572 * tempCode / 65535) - 4685;
		m_localTempReading_tenthDegC = (int16_t)(temp_hundredthsDegC / 10);

		*p_temp = m_localTempReading_tenthDegC;
	}

	return true;
}
