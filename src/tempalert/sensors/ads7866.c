/*
 * ads7866.c
 *
 *  Created on: Feb 9, 2017
 *      Author: Nate-Tempalert
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "nordic_common.h"

#include "nrf.h"
#include "nrf_delay.h"
#include "nrf_drv_spi.h"
#include "nrf_gpio.h"
#include "nrf_log.h"

#include "app_error.h"
#include "app_util_platform.h"

#include "FreeRTOS.h"
#include "task.h"

#include "SEGGER_RTT.h"

#include "boards.h"

#include "spi.h"
#include "spi_mtx.h"
#include "ads7866.h"

#define VREF_MV				1250		// mV
#define RESISTOR_HIGH	 	100			// kOhm
#define RESISTOR_LOW		20			// kOhm
#define ADS7866_BITS		((1<<12)-1)	// Bits

static const nrf_drv_spi_config_t ads7866_spi_config = {
		.sck_pin = SCLK_PIN,
		.mosi_pin = MOSI_PIN,
		.miso_pin = MISO_PIN,
		.ss_pin = NRF_DRV_SPI_PIN_NOT_USED,
		.irq_priority = APP_IRQ_PRIORITY_HIGH,
		.orc = 0xFF,
		.frequency = NRF_DRV_SPI_FREQ_250K,
		.mode = NRF_DRV_SPI_MODE_0,
		.bit_order = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST
};

static inline void ads7866_assert_cs(void);
static inline void ads7866_deassert_cs(void);

bool ads7866_readBatteryVoltage(uint16_t * p_Vbatt) {
	bool success = true;
	uint8_t raw_data[2];
	uint32_t vbatt_tmp = 0;

	nrf_gpio_pin_dir_set(INTSENSORENN_PIN, 1); //it's an output
	nrf_gpio_pin_clear(INTSENSORENN_PIN); // clear to enable

	vTaskDelay(pdMS_TO_TICKS(10));
	/* Assume that the SPI bus has been claimed using spi_smphr */

	WITH_SPI_MTX() {
		ads7866_assert_cs();

		vTaskDelay(pdMS_TO_TICKS(10));

		if (success) {
			success = spi_transfer(NULL, 0, raw_data, 2);
		}

		ads7866_deassert_cs();
	}

	if (success) {
		/* Convert raw data to voltage and return */
		vbatt_tmp = (( ( ( uint32_t ) raw_data[0] ) << 8) | (( uint32_t ) raw_data[1] ))
				* VREF_MV * (RESISTOR_HIGH + RESISTOR_LOW) / RESISTOR_LOW / ADS7866_BITS;

		/* Factor in voltage divider */
		*p_Vbatt = (uint16_t)vbatt_tmp;

		SEGGER_RTT_printf(0,"Data Received, %02x%02x >> Vbatt: %umV\r\n",raw_data[0],raw_data[1], (uint16_t) *p_Vbatt);

	} else {
		SEGGER_RTT_printf(0,"Failed to execute spi xfr with ADS7866!\r\n");
	}

	vTaskDelay(pdMS_TO_TICKS(10));

	nrf_gpio_pin_set(INTSENSORENN_PIN); // set to disable

	return success;
}


void ads7866_assert_cs() {
	spi_init(&ads7866_spi_config, NULL);

	spi_csmux_select(ADC_CSN);
	nrf_gpio_pin_clear(CSN_ENN_PIN);
}


void ads7866_deassert_cs(void) {
	nrf_gpio_pin_set(CSN_ENN_PIN);
	spi_deinit();
}
