/*
 * unixTime.h
 *
 *  Created on: Mar 3, 2017
 *      Author: Nate-Tempalert
 */

#ifndef SRC_TEMPALERT_UNIXTIME_H_
#define SRC_TEMPALERT_UNIXTIME_H_

#include "FreeRTOS.h"
#include "semphr.h"
#include <stdint.h>
#include <stdbool.h>

extern xSemaphoreHandle unixTimeMutex;

bool unixTime_init(void);
bool unixTime_setSeconds(uint32_t seconds);
bool unixTime_isValid(void);
bool unixTime_getSeconds(int32_t *seconds);
bool unixTime_getSecondsUntillUpdate(int32_t *seconds);
bool unixTime_triggerTimeUpdate(void);
bool unixTime_invalidate();
bool unixTime_getTimeAtLastReset_100ms(uint32_t *timeAtLastReset_100ms);
uint32_t get_time_100ms(void);

#endif /* SRC_TEMPALERT_UNIXTIME_H_ */
