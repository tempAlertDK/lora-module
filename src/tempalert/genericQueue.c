/*
 * genericQueue.c
 *
 *  Created on: Feb 2, 2017
 *      Author: kwgilpin
 */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "portmacro_cmsis.h"

#include "SEGGER_RTT.h"

#include "genericQueue.h"

bool genericQueueSend(QueueHandle_t genQ, genericQueueElementType_t type, const genericQueuePool_t *pool, const void *data, TickType_t ticksToWait) {
	bool success = false;
	bool freeBufferFound = false;
	genericQueueElement_t element;

	if ((pool == NULL) && (data == NULL)) {
		element.type = type;
		element.mutex = NULL;
		element.p_record = NULL;

		if (xQueueSend(genQ, &element, 0) == pdTRUE) {
			success = true;
		} else {
			SEGGER_RTT_printf(0, "ERROR: xQueueSend failed in %s\r\n", __FUNCTION__);
		}
	} else if ((pool != NULL) && (data != NULL)) {
		if (xSemaphoreTake(pool->mutex, ticksToWait) == pdTRUE) {
			/*
			 * Iterate over all records in the pool, looking for one that is
			 * marked free.
			 */
			for (int i = 0; i < pool->poolBufferCount; i++) {
				if (pool->p_poolHeaders[i].free) {
					/*
					 * When we find a free record in the pool, we copy the provided
					 * data into the record and indicate that it is now in use.
					 */
					freeBufferFound = true;
					memcpy(pool->p_poolHeaders[i].p_data, data, pool->poolBufferSize);
					pool->p_poolHeaders[i].dataSize = pool->poolBufferSize;
					pool->p_poolHeaders[i].free = false;

					/*
					 * The element metadata holds the record type, a handle to
					 * mutex protecting the data, and a pointer to the record in
					 * the pool holding the actual data.  It is this element
					 * metadata that will be placed into a FreeRTOS queue.
					 */
					element.type = type;
					element.mutex = pool->mutex;
					element.p_record = &pool->p_poolHeaders[i];

					if (xQueueSend(genQ, &element, 0) == pdTRUE) {
						success = true;
					} else {
						/*
						 * If the queue which holds the type of-, mutex
						 * protecting-, and pointer to the data, is full, we mark
						 * the record in pool free so that it can be reused.
						 */
						pool->p_poolHeaders[i].free = true;

						SEGGER_RTT_printf(0, "ERROR: xQueueSend failed in %s\r\n", __FUNCTION__);
					}

					/*
					 * As soon as we find a free element in the pool, we stop
					 * searching.
					 */
					break;
				}
			}

			if (!freeBufferFound) {
				SEGGER_RTT_printf(0, "ERROR: %s failed to find free buffer in pool\r\n", __FUNCTION__);
			}

			/* If we successfully took the mutex, give it back */
			xSemaphoreGive(pool->mutex);
		} else {
			SEGGER_RTT_printf(0, "ERROR: Failed to add take mutex protecting pool in %s\r\n", __FUNCTION__);
		}
	} else {
		SEGGER_RTT_printf(0, "ERROR: In %s, the *pool and *data arguments must both be NULL or must both be valid pointers\r\n", __FUNCTION__);
	}

	if (!success) {
		SEGGER_RTT_printf(0, "ERROR: %s failed\r\n", __FUNCTION__);
	}

	return success;
}


bool genericQueueReceive(QueueHandle_t genQ,  genericQueueElementType_t *type, void *data, size_t *dataSize, TickType_t ticksToWait) {
	bool success = false;
	genericQueueElement_t element;

	if (type == NULL) {
		SEGGER_RTT_printf(0, "ERROR: In %s, *type argument cannot be NULL\r\n", __FUNCTION__);
	} else if (data == NULL) {
		SEGGER_RTT_printf(0, "ERROR: In %s, *data argument cannot be NULL\r\n", __FUNCTION__);
	} else if (dataSize == NULL) {
		SEGGER_RTT_printf(0, "ERROR: In %s, *dataSize argument cannot be NULL\r\n", __FUNCTION__);
	} else if (xQueueReceive(genQ, &element, ticksToWait) == pdTRUE) {
		if ((element.mutex == NULL) && (element.p_record == NULL)) {
			*type = element.type;
			success = true;
		} else if ((element.mutex != NULL) && (element.p_record != NULL)) {

			if (xSemaphoreTake(element.mutex, ticksToWait) == pdTRUE) {

				if (*dataSize >= element.p_record->dataSize) {
					*type = element.type;
					memcpy(data, element.p_record->p_data, element.p_record->dataSize);
					*dataSize = element.p_record->dataSize;
					success = true;
				} else {
					printf("ERROR: Data buffer provided to %s too small\r\n", __FUNCTION__);
					success = false;
				}

				element.p_record->dataSize = 0;
				element.p_record->free = true;

				xSemaphoreGive(element.mutex);
			} else {
				SEGGER_RTT_printf(0, "ERROR: Failed to add take mutex protecting pool in %s\r\n", __FUNCTION__);
			}
		} else {
			SEGGER_RTT_printf(0, "ERROR: In %s, the element received from genQ must have both its *pool and *data elements be NULL or both must be valid pointers\r\n", __FUNCTION__);
		}
	} else {
		SEGGER_RTT_printf(0, "ERROR: xQueueRecieve failed in %s\r\n", __FUNCTION__);
	}

	return success;
}
