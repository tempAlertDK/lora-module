#include <string.h>

#include "FreeRTOS.h"
#include "task.h"

#include "memtrace.h"

// 7 * 585 ~== 4096 bytes
#define MEMTRACE_SIZE 585

const char *memtrace_events[] = {
	[MEMTRACE_EV_BEACON_TX_START] = "BCN_TX_START",
	[MEMTRACE_EV_BEACON_TX_END] = "BCN_TX_END"
};

typedef struct {
	TickType_t ticks;
	memtrace_event_t ev;
	uint8_t arg1;
	uint8_t arg2;
} memtrace_elem_t;

static memtrace_elem_t memtrace_buf[MEMTRACE_SIZE];

struct {
	int write_index;
	int len;
	memtrace_elem_t *buf;
} MEMTRACE = {
	.write_index = 0,
	.len = MEMTRACE_SIZE,
	.buf = memtrace_buf
};

void memtrace_event2(memtrace_event_t ev, uint8_t arg1, uint8_t arg2) {
	MEMTRACE.buf[MEMTRACE.write_index].ticks = xTaskGetTickCount();
	MEMTRACE.buf[MEMTRACE.write_index].ev = ev;
	MEMTRACE.buf[MEMTRACE.write_index].arg1 = arg1;
	MEMTRACE.buf[MEMTRACE.write_index].arg2 = arg2;
	MEMTRACE.write_index++;
	if (MEMTRACE.write_index >= MEMTRACE_SIZE) {
		MEMTRACE.write_index = 0;
	}
}
