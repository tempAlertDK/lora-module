/*
 * unixTime.c
 *
 *  Created on: Mar 3, 2017
 *      Author: Nate-Tempalert
 */

#include "FreeRTOS.h"
#include "semphr.h"
#include "unixTime.h"
#include "timers.h"
#include "globals.h"
#include "SEGGER_RTT.h"
#include "log.h"

xSemaphoreHandle unixTimeMutex;
StaticSemaphore_t unixTimeMutex_buffer;

TimerHandle_t unixTime_secondTimer;
StaticTimer_t unixTime_secondTimerStatic;
TimerHandle_t unixTime_updateTimer;
StaticTimer_t unixTime_updateTimerStatic;

static int32_t m_unixTimeSeconds = 0;
static uint32_t m_time100ms = 0;
static bool m_unixTimeIsValid = false;

static TickType_t secondTimerInterval = pdMS_TO_TICKS(1000);
static TickType_t updateTimerInterval = pdMS_TO_TICKS(1000)*24*60*60; //24 hours

void unixTime_timerCallback(TimerHandle_t pvTimerID) {
	if (pvTimerID == unixTime_secondTimer) {
		// Increment the 100ms tick count
		m_time100ms+=10;
		if (xSemaphoreTake(unixTimeMutex,pdMS_TO_TICKS(100)) == pdTRUE) {
			m_unixTimeSeconds++;
			xSemaphoreGive(unixTimeMutex);
		} else {
			SEGGER_RTT_printf(0,"Missed 1s tick!\r\n");
		}
	} else if (pvTimerID == unixTime_updateTimer) {
		log_info("Unix time update timer expired, sending update request on next upload");
		g_updateUnixTime = true;
	}
}

bool unixTime_init(void) {

	m_time100ms = 0;
	m_unixTimeSeconds = 0;
	m_unixTimeIsValid = false;
	// Create timers for handling unix time updates
	if ((unixTime_secondTimer = xTimerCreateStatic("UXST",secondTimerInterval,
			pdTRUE, (void *)0,unixTime_timerCallback, &unixTime_secondTimerStatic)) == pdFALSE){
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	if ((unixTime_updateTimer = xTimerCreateStatic("UXUT",updateTimerInterval,
			pdTRUE, (void *)0,unixTime_timerCallback, &unixTime_updateTimerStatic)) == pdFALSE){
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	if ((unixTimeMutex = xSemaphoreCreateMutexStatic(&unixTimeMutex_buffer)) == pdFALSE ) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	return true;
}


bool unixTime_setSeconds(uint32_t seconds) {
	if (xTaskGetCurrentTaskHandle() == xQueueGetMutexHolder(unixTimeMutex)) {
		m_unixTimeSeconds = seconds;
	} else {
		return false;
	}
	SEGGER_RTT_printf(0,"Unix time set successfully to %lds.\r\n",m_unixTimeSeconds);
	m_unixTimeIsValid = true;
	/* Start timer execution */
	xTimerStart(unixTime_secondTimer,portMAX_DELAY);
	xTimerStart(unixTime_updateTimer,portMAX_DELAY);
	return true;
}

bool unixTime_isValid(void) {
	return m_unixTimeIsValid;
}

bool unixTime_getSeconds(int32_t *seconds) {
	if (unixTime_isValid()) {
		*seconds = m_unixTimeSeconds;
		return true;
	}
	return false;
}

bool unixTime_getSecondsUntillUpdate(int32_t *seconds) {
	if (xTimerIsTimerActive(unixTime_updateTimer) != pdTRUE) {
		return false;
	}

	TickType_t nextUploadSeconds = ((xTimerGetExpiryTime( unixTime_updateTimer ) - xTaskGetTickCount())) / pdMS_TO_TICKS(1000);
	*seconds = nextUploadSeconds;

	return true;
}

bool unixTime_triggerTimeUpdate(void) {
	if (xTimerIsTimerActive(unixTime_updateTimer) != pdTRUE) {
		return false;
	}

	if (xTimerReset(unixTime_updateTimer,pdMS_TO_TICKS(1000)) == pdTRUE) {
		g_updateUnixTime = true;
		return true;
	} else {
		return false;
	}

}

bool unixTime_invalidate() {
	if (xTaskGetCurrentTaskHandle() != xQueueGetMutexHolder(unixTimeMutex)) {
		return false;
	}
	m_unixTimeIsValid = false;
	/* Stop timer execution */
	xTimerStop(unixTime_secondTimer,portMAX_DELAY);
	xTimerStop(unixTime_updateTimer,portMAX_DELAY);
	return true;

}

bool unixTime_getTimeAtLastReset_100ms(uint32_t *timeAtLastReset_100ms) {
//	if ((!m_timeAtLastResetValid) || (timeAtLastReset_100ms == NULL)) {
//		return false;
//	}
//
//	*timeAtLastReset_100ms = m_timeAtLastReset_100ms;
//	return true;
	return false;
}

uint32_t get_time_100ms(void) {

	/* Changed from using xTaskGetTickCount() to prevent
	 * roll over math issues. */
	return (uint32_t) m_time100ms;

}

