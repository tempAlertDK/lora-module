/*
 * util.c
 *
 *  Created on: Nov 13, 2013
 *      Author: kwgilpin
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"
#include "nrf_error.h"
#include "util.h"

static uint8_t m_debugLevel = 8;

void util_setDebugLevel(uint8_t level) {
	m_debugLevel = level;
}

uint8_t util_getDebugLevel() {
	return m_debugLevel;
}

uint32_t util_printDebugString(const char *str, uint8_t debugLevelThreshold) {
	if (m_debugLevel >= debugLevelThreshold) {
		return printf("%s",str);
	} else {
		return NRF_SUCCESS;
	}
}

void util_printHex(const uint8_t * p_data, const uint32_t len) {
    for (int i = 0; i < len; i++) {
        printf("%02X", p_data[i]);
		if (i % 2 == 1) {
			printf(" ");
		}
    }
	printf("\r\n");
}

void util_printAscii(const uint8_t * p_data, const uint32_t len) {
    for (int i = 0; i < len; i++) {
		if (p_data[i] >= 32 && p_data[i] <= 126) {
			printf("%c ", p_data[i]);
		} else {
			printf(". ");
		}
		if (i % 2 == 1) {
			printf(" ");
		}
    }
	printf("\r\n");
}

void util_delayUntil(TickType_t base, const TickType_t offset) {
	vTaskDelayUntil(&base, offset);
}

void util_delayUntilAndUpdateBase(TickType_t *base, const TickType_t offset) {
	vTaskDelayUntil(base, offset);
}
