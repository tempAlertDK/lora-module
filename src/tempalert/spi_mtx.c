/*
 * spi_mtx.c
 *
 *  Created on: Jan 12, 2017
 *      Author: kwgilpin
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "nrf_drv_spi.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "portmacro_cmsis.h"

#include "log.h"
#include "spi_mtx.h"

static SemaphoreHandle_t spi_mtx = NULL;
static StaticSemaphore_t spi_mtx_buffer;

static nrf_drv_spi_t m_spi_instance = NRF_DRV_SPI_INSTANCE(1);

void spi_mtx_init() {
	spi_mtx = xSemaphoreCreateMutexStatic(&spi_mtx_buffer);
	xSemaphoreGive(spi_mtx);
}


uint8_t spi_mtx_take(TickType_t ticksToWait) {
	if (nrf_drv_spi_getinit(&m_spi_instance) == true) {
#if defined(SPI_MTX_DEBUG) && (SPI_MTX_DEBUG >= 1)
		log_warnf("SPI: peripheral already initialized before calling spi_mtx_take() from %s task\r\n", pcTaskGetName(NULL));
#endif
	}

	if (spi_mtx == NULL) {
#if defined(SPI_MTX_DEBUG) && (SPI_MTX_DEBUG >= 1)
		printf("SPI: mutex not initialized\r\n");
#endif
		return 0;
	}

#if defined(SPI_MTX_DEBUG) && (SPI_MTX_DEBUG >= 1)
	/* Poll to see whether the mutex is immediately available */
	if (xSemaphoreTake(spi_mtx, 0) == pdFALSE) {
		printf("SPI: %s task blocking because mutex held by %s task\r\n", pcTaskGetName(NULL), pcTaskGetName(xSemaphoreGetMutexHolder(spi_mtx)));
	} else {
#if (SPI_MTX_DEBUG == 2)
		printf("SPI: mutex taken by %s task\r\n", pcTaskGetName(NULL));
#endif
		return 1;
	}
#endif

	/* Wait for the mutex */
	if (xSemaphoreTake(spi_mtx, ticksToWait) == pdTRUE) {
#if defined(SPI_MTX_DEBUG) && (SPI_MTX_DEBUG >= 1)
		printf("SPI: mutex taken by %s task\r\n", pcTaskGetName(NULL));
#endif
		return 1;
	} else {
#if defined(SPI_MTX_DEBUG) && (SPI_MTX_DEBUG >= 1)
		printf("SPI: %s task failed to take mutex\r\n", pcTaskGetName(NULL));
#endif
		return 0;
	}
}

void spi_mtx_give() {
	if (nrf_drv_spi_getinit(&m_spi_instance) == true) {
#if defined(SPI_MTX_DEBUG) && (SPI_MTX_DEBUG >= 1)
		log_warnf("SPI: peripheral not de-initialized before calling spi_mtx_give() from %s task\r\n", pcTaskGetName(NULL));
#endif
	}

	if (spi_mtx == NULL) {
#if defined(SPI_MTX_DEBUG) && (SPI_MTX_DEBUG >= 1)
		printf("SPI: mutex not initialized\r\n");
#endif
		return;
	}

#if defined(SPI_MTX_DEBUG) && (SPI_MTX_DEBUG == 2)
	printf("SPI: mutex given by %s task\r\n", pcTaskGetName(NULL));
#endif
	xSemaphoreGive(spi_mtx);
}

void spi_mtx_restore(const uint8_t *taken) {
	if (*taken) {
		spi_mtx_give();
	}
}
