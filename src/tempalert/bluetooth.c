#include <stdint.h>
#include <stdbool.h>

#include "nordic_common.h"
#include "nrf.h"
#include "nrf_delay.h"

#include "boards.h"

#include "app_timer.h"
#include "app_button.h"
#include "app_util_platform.h"
#include "app_error.h"

#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "softdevice_handler.h"
#include "ble_nus.h"

#include "bsp.h"
#include "bsp_btn_ble.h"
#include "pstorage.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#include "SEGGER_RTT.h"

#include "pa_lna.h"

#include "config.h"
#include "bluetooth.h"
#include "ble_levelSensor.h"
#include "log.h"
#include "globals.h"
#include "deviceID.h"

static TaskHandle_t m_ble_stack_thread_handle; /**< Definition of BLE stack thread. */
static StaticTask_t m_ble_stack_thread_tcb;
#define BLE_STACK_SIZE		512
static StackType_t m_ble_stack_thread_stack[ BLE_STACK_SIZE ];

ble_levelSensorService_t m_levelSensorService;

static uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID; /**< Handle of the current connection. */

#pragma GCC diagnostic push // ncr
#pragma GCC diagnostic warning "-Wunused-variable"
 static ble_uuid_t m_adv_uuids[] = { { LEVELSENSOR_UUID_SERVICE,
 NUS_SERVICE_UUID_TYPE } }; /**< Universally unique service identifier. */
#pragma GCC diagnostic pop

static SemaphoreHandle_t m_ble_event_ready; /**< Semaphore raised if there is a new event to be processed in the BLE thread. */
static StaticSemaphore_t m_ble_event_ready_buffer;

//static TimerHandle_t m_simlevel_timer;        /**< Definition of battery timer. */

static void ble_stack_init(void);
static void gap_params_init(void);
static void advertising_init(void);
static void services_init(void);
static void conn_params_init(void);

static void ble_stack_thread(void *arg);

#if (0)
/**@brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyse 
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name) {
	app_error_handler(DEAD_BEEF, line_num, p_file_name);
}
#endif

/**@brief Function for the GAP initialization.
 *
 * @details This function will set up all the necessary GAP (Generic Access Profile) parameters of 
 *          the device. It also sets the permissions and appearance.
 */
void gap_params_init(void) {
	uint32_t err_code;
	ble_gap_conn_params_t gap_conn_params;
	ble_gap_conn_sec_mode_t sec_mode;

	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

	err_code = sd_ble_gap_device_name_set(&sec_mode, (const uint8_t *) DEVICE_NAME, strlen(DEVICE_NAME));
	APP_ERROR_CHECK(err_code);

	memset(&gap_conn_params, 0, sizeof(gap_conn_params));

	gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
	gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
	gap_conn_params.slave_latency = SLAVE_LATENCY;
	gap_conn_params.conn_sup_timeout = CONN_SUP_TIMEOUT;

	err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
	APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void) {
	uint32_t err_code;

	ble_levelSensorService_init_t levelSensorService_init;

	memset(&levelSensorService_init, 0, sizeof(levelSensorService_init));

	/* Initialize levelSensor service */
	levelSensorService_init.measurementInterval_min = 10;
	levelSensorService_init.calibration = -1;

	deviceID_getSerialNumberArray(levelSensorService_init.sensorSerial);

	err_code = ble_levelSensorService_init(&m_levelSensorService, &levelSensorService_init);
	APP_ERROR_CHECK(err_code);


}

/**@brief Function for handling an event from the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module
 *          which are passed to the application.
 *
 * @note All this function does is to disconnect. This could have been done by simply setting
 *       the disconnect_on_fail config parameter, but instead we use the event handler
 *       mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt) {
	uint32_t err_code;

	if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED) {
		err_code = sd_ble_gap_disconnect(m_conn_handle,
		BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
		APP_ERROR_CHECK(err_code);
	}
}

/**@brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error) {
	APP_ERROR_HANDLER(nrf_error);
}

/**@brief Function for initializing the Connection Parameters module.
 */
void conn_params_init(void) {
	uint32_t err_code;
	ble_conn_params_init_t cp_init;

	memset(&cp_init, 0, sizeof(cp_init));

	cp_init.p_conn_params = NULL;
	cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
	cp_init.next_conn_params_update_delay = NEXT_CONN_PARAMS_UPDATE_DELAY;
	cp_init.max_conn_params_update_count = MAX_CONN_PARAMS_UPDATE_COUNT;
	cp_init.start_on_notify_cccd_handle = BLE_GATT_HANDLE_INVALID;
	cp_init.disconnect_on_fail = false;
	cp_init.evt_handler = on_conn_params_evt;
	cp_init.error_handler = conn_params_error_handler;

	err_code = ble_conn_params_init(&cp_init);
	APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt) {

	switch (ble_adv_evt) {
	case BLE_ADV_EVT_FAST:
		g_bleIsAdvertising = true;
		break;
	case BLE_ADV_EVT_IDLE:
		g_bleIsAdvertising = false;
		break;
	default:
		break;
	}
}

/**@brief Function for the application's SoftDevice event handler.
 *
 * @param[in] p_ble_evt SoftDevice event.
 */
static void on_ble_evt(ble_evt_t * p_ble_evt) {
	uint32_t err_code;

	switch (p_ble_evt->header.evt_id) {
	case BLE_GAP_EVT_CONNECTED:
		g_bleIsConnected = true;
		m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
		break;

	case BLE_GAP_EVT_DISCONNECTED:
		g_bleIsConnected = false;
		m_conn_handle = BLE_CONN_HANDLE_INVALID;
		break;

	case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
		// Pairing not supported
		err_code = sd_ble_gap_sec_params_reply(m_conn_handle,
		BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
		APP_ERROR_CHECK(err_code);
		break;

	case BLE_GATTS_EVT_SYS_ATTR_MISSING:
		// No system attributes have been stored.
		err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
		APP_ERROR_CHECK(err_code);
		break;

	default:
		// No implementation needed.
		break;
	}
}

/**@brief Function for dispatching a SoftDevice event to all modules with a SoftDevice 
 *        event handler.
 *
 * @details This function is called from the SoftDevice event interrupt handler after a 
 *          SoftDevice event has been received.
 *
 * @param[in] p_ble_evt  SoftDevice event.
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt) {
	ble_conn_params_on_ble_evt(p_ble_evt);
	//ble_nus_on_ble_evt(&m_nus, p_ble_evt);
    ble_levelSensorService_on_ble_evt(&m_levelSensorService, p_ble_evt);
	on_ble_evt(p_ble_evt);
	ble_advertising_on_ble_evt(p_ble_evt);
	//bsp_btn_ble_on_ble_evt(p_ble_evt);

}

/**@brief Function for dispatching a system event to interested modules.
 *
 * @details This function is called from the System event interrupt handler after a system
 *          event has been received.
 *
 * @param[in]   sys_evt   System stack event.
 */
static void sys_evt_dispatch(uint32_t sys_evt) {
	pstorage_sys_event_handler(sys_evt);
	ble_advertising_on_sys_evt(sys_evt);
}

/* This function is called from the SoftDevice handler from the interrupt
 * level.  It indicates that there is a new event which needs to be handled.
 * To actually handle the event, we give the m_ble_event_ready semaphore which
 * is then taken by ble_stack_thread.  It, in turn, calls
 * intern_softdevice_events_execute(), which calls either the BLE or SYS event
 * callback function previously registered with the SoftDevice during stack
 * initialization.  In this code, these callbacks are named ble_evt_dispatch
 * and sys_evt_dispatch, respectively.
 *
 * @return The returned value is checked in the softdevice_handler module,
 *         using the APP_ERROR_CHECK macro.
 */
static uint32_t ble_new_event_handler(void) {
	BaseType_t yield_req = pdFALSE;
	// The returned value may be safely ignored, if error is returned it only means that
	// the semaphore is already given (raised).
	UNUSED_VARIABLE(xSemaphoreGiveFromISR(m_ble_event_ready, &yield_req));
	portYIELD_FROM_ISR(yield_req);
	return NRF_SUCCESS;
}

/**@brief Function for the SoftDevice initialization.
 *
 * @details This function initializes the SoftDevice and the BLE event interrupt.
 */
void ble_stack_init(void) {
	uint32_t err_code;

	// Initialize SoftDevice.
	nrf_clock_lf_cfg_t clock_lf_cfg = NRF_CLOCK_LFCLKSRC;
	SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, ble_new_event_handler);

	ble_enable_params_t ble_enable_params;
	err_code = softdevice_enable_get_default_config(
			CENTRAL_LINK_COUNT,
			PERIPHERAL_LINK_COUNT,
			&ble_enable_params);
	APP_ERROR_CHECK(err_code);

	//Check the ram settings against the used number of links
	CHECK_RAM_START_ADDR(CENTRAL_LINK_COUNT, PERIPHERAL_LINK_COUNT);

	// Enable BLE stack.
	err_code = softdevice_enable(&ble_enable_params);
	APP_ERROR_CHECK(err_code);

	// Subscribe for BLE events.
	err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
	APP_ERROR_CHECK(err_code);

	// Subscribe for system events.
	err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
	APP_ERROR_CHECK(err_code);
}



/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void) {
    uint32_t      err_code;
    ble_advdata_t advdata;
    ble_advdata_manuf_data_t manuf_data_adv;
    ble_advdata_t scanrsp;
    ble_advdata_manuf_data_t manuf_data_scanrsp;

	/******************************/
    /* Build the advertising data */
	/******************************/
    memset(&advdata, 0, sizeof(advdata));

    advdata.name_type               = BLE_ADVDATA_NO_NAME;
    advdata.include_appearance      = false;
    advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;

    /* Pick which service UUIDs we advertise */
    ble_uuid_t adv_uuids[] = {
    		{LEVELSENSOR_UUID_SERVICE, m_levelSensorService.service_uuid_type}
    };
    advdata.uuids_complete.uuid_cnt = sizeof(adv_uuids) / sizeof(adv_uuids[0]);
    advdata.uuids_complete.p_uuids = adv_uuids;

//    /* Build the manufacturing data field to include in the advertising packet */
//    memset(&picoStatus, 0, sizeof(picoStatus));
	memset(&manuf_data_adv, 0, sizeof(manuf_data_adv));

	/* Include the manufacturing data that we just built in the advertising
	 * packet. */
    advdata.p_manuf_specific_data = &manuf_data_adv;


    /********************************/
    /* Build the scan response data */
    /********************************/
    memset(&scanrsp, 0, sizeof(scanrsp));
	scanrsp.name_type = BLE_ADVDATA_NO_NAME;
	scanrsp.include_appearance = false;

    /* Pick which service UUIDs we advertise */
    ble_uuid_t scn_uuids[] = {
    		{LEVELSENSOR_UUID_SERVICE, m_levelSensorService.service_uuid_type},
    };
    scanrsp.uuids_complete.uuid_cnt = sizeof(scn_uuids) / sizeof(scn_uuids[0]);
    scanrsp.uuids_complete.p_uuids = scn_uuids;

    /* Build the manufacturer data field */
    memset(&manuf_data_scanrsp, 0, sizeof(manuf_data_scanrsp));

    /* Truncate the serial number to 6 bytes */
    uint64_t serialNumber = 0;
    deviceID_getSerialNumber(&serialNumber);
    manuf_data_scanrsp.company_identifier = 0xFFFF;
    manuf_data_scanrsp.data.size = 8;
    manuf_data_scanrsp.data.p_data = (uint8_t *)&serialNumber;

	int i,c;
	for (i =0; i < 8; i++) {
		c = (int)(((uint8_t *)&serialNumber)[i]);
		printf(" %02x ",c);
	}

    /* Include the manufacturing data that we just built in the scan response
     * packet. */
    scanrsp.p_manuf_specific_data = &manuf_data_scanrsp;

	ble_adv_modes_config_t options = { 0 };
	options.ble_adv_fast_enabled = BLE_ADV_FAST_ENABLED;
	options.ble_adv_fast_interval = APP_ADV_INTERVAL;
	options.ble_adv_fast_timeout = APP_ADV_TIMEOUT_IN_SECONDS;

	err_code = ble_advertising_init(&advdata, &scanrsp, &options, on_adv_evt, NULL);
	APP_ERROR_CHECK(err_code);

}

void ble_init(bool erase_bonds) {
	UNUSED_PARAMETER(erase_bonds);

	/* Initialize the semaphore for the BLE thread. */
	m_ble_event_ready = xSemaphoreCreateBinaryStatic(&m_ble_event_ready_buffer);
	if (NULL == m_ble_event_ready) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	ble_stack_init();

	/* Initialize the GPIO's to control PA and/or LNA; must be done while radio
	 * is inactive (i.e. not advertising and not connected). */
	if (pa_lna_init(FEM_TX_EN_PIN, FEM_RX_EN_PIN)) {
		printf("\r\nPA/LNA enabled.\r\n");
	}

	/*if ((m_ble_stack_thread_handle = xTaskCreateStatic(ble_stack_thread, "BLE", BLE_STACK_SIZE, NULL, 2, m_ble_stack_thread_stack, &m_ble_stack_thread_tcb)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}*/
}

/**@brief Thread for handling the Application's BLE Stack events.
 *
 * @details This thread is responsible for handling BLE Stack events sent from on_ble_evt().
 *
 * @param[in]   arg   Pointer used for passing some arbitrary information (context) from the
 *                    osThreadCreate() call to the thread.
 */
void ble_stack_thread(void *arg) {
	UNUSED_PARAMETER(arg);
	uint32_t err_code;

	gap_params_init();

	/* delay until */
	uint64_t tempSerial;
	bool idIsReady = deviceID_getSerialNumber(&tempSerial);
	while (!idIsReady) {
		vTaskDelay(pdMS_TO_TICKS(50));
		idIsReady = deviceID_getSerialNumber(&tempSerial);
	}
	printf("Device ID ready.\r\n");

	services_init();
	advertising_init();
	conn_params_init();

#if defined(ENABLE_BLE) && (ENABLE_BLE == 1)
	err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
	APP_ERROR_CHECK(err_code);
#endif

	while (1) {
		/* Wait for event from SoftDevice */
		while (pdFALSE == xSemaphoreTake(m_ble_event_ready, portMAX_DELAY)) {
			// Just wait again in the case when INCLUDE_vTaskSuspend is not enabled
		}

		/* This function gets events from the SoftDevice and processes them by
		 * calling the functions registered by with the SoftDevice using the
		 * softdevice_ble_evt_handler_set(ble_evt_dispatch) and
		 * softdevice_sys_evt_handler_set(sys_evt_dispatch) calls. */
		intern_softdevice_events_execute();
	}
}







