/*
 * twi_register.c
 *
 *  Created on: Jan 15, 2017
 *      Author: kwgilpin
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "app_twi.h"
#include "app_util_platform.h"

#include "FreeRTOS.h"
#include "task.h"

#include "SEGGER_RTT.h"

#include "boards.h"
#include "twi_register.h"
#include "log.h"

#define MAX_PENDING_TWI_TRANSACTIONS	6

app_twi_t m_app_twi_mpc = APP_TWI_INSTANCE(0);

nrf_drv_twi_config_t const config = { .scl = SCL_PIN, .sda = SDA_PIN, .frequency = NRF_TWI_FREQ_100K,
		.interrupt_priority = APP_IRQ_PRIORITY_LOW };
#ifdef BOARD_CELL_NODE_REV1P1
nrf_drv_twi_config_t const config_ext = { .scl = EXT_SCL_PIN, .sda = EXT_SDA_PIN, .frequency = NRF_TWI_FREQ_100K,
		.interrupt_priority = APP_IRQ_PRIORITY_LOW };
#endif


const nrf_drv_twi_config_t * m_currentTwiConfig;

bool twiRegister_app_perform (app_twi_transfer_t const * p_transfers, uint8_t num_transfers);

bool twiRegister_configureI2C(const nrf_drv_twi_config_t *config) {
	uint32_t err_code;

	APP_TWI_INIT(&m_app_twi_mpc, config, MAX_PENDING_TWI_TRANSACTIONS, err_code);
	APP_ERROR_CHECK(err_code);

	if (err_code != NRF_SUCCESS) {
		log_debugf("TWI ERROR: %s task failed to configure I2C interface", pcTaskGetName(NULL));
		return false;
	} else {
		m_currentTwiConfig = config;
		return true;
	}
}

bool twiRegister_read(const nrf_drv_twi_config_t *config, uint8_t addr, uint8_t reg, uint8_t *data) {
	bool success = true;

	if (success) {
		success = twiRegister_configureI2C(config);
	}

	// Read:
	if (success) {
		app_twi_transfer_t transfers[] = {
				APP_TWI_WRITE(addr, &reg, 1, APP_TWI_NO_STOP),
				APP_TWI_READ(addr, data, 1, 0),
		};

		if (!twiRegister_app_perform(transfers, sizeof(transfers) / sizeof(transfers[0]))) {
			success = false;
		}
	}

	app_twi_uninit(&m_app_twi_mpc);

	if (!success) {
		log_debugf("TWI ERROR: %s task failed to read I2C register (I2C addr: 0x%02X, reg addr: 0x%02X)", pcTaskGetName(NULL), addr, reg);
	}

	return success;
}


bool twiRegister_readData(const nrf_drv_twi_config_t *config, uint8_t addr, uint8_t reg, uint8_t *data, uint8_t dataLen) {
	bool success = true;

	if (success) {
		success = twiRegister_configureI2C(config);
	}

	// Read:
	if (success) {
		app_twi_transfer_t transfers[] = {
				APP_TWI_WRITE(addr, &reg, 1, APP_TWI_NO_STOP),
				APP_TWI_READ(addr, data, dataLen, 0),
		};

		if (!twiRegister_app_perform(transfers, sizeof(transfers) / sizeof(transfers[0]))) {
			success = false;
		}
	}

	app_twi_uninit(&m_app_twi_mpc);

	if (!success) {
		log_debugf("TWI ERROR: %s task failed to read multiple (%d) I2C registers (I2C addr: 0x%02X, reg addr: 0x%02X)", pcTaskGetName(NULL), dataLen, addr, reg);
	}

	return success;
}


bool twiRegister_write(const nrf_drv_twi_config_t *config, uint8_t addr, uint8_t reg, uint8_t data) {
	uint8_t twiData[2];
	bool success = true;

	if (success) {
		success = twiRegister_configureI2C(config);
	}

	twiData[0] = reg;
	twiData[1] = data;

	// Write:
	if (success) {
		app_twi_transfer_t transfers[] = {
				APP_TWI_WRITE(addr, twiData, 2, 0)
		};

		if (!twiRegister_app_perform(transfers, sizeof(transfers) / sizeof(transfers[0]))) {
			success = false;
		}
	}

	// Verify:
	if (success) {
		app_twi_transfer_t transfers[] = {
				APP_TWI_WRITE(addr, &reg, 1, APP_TWI_NO_STOP),
				APP_TWI_READ(addr, &twiData[1], 1, 0),
		};

		if (!twiRegister_app_perform(transfers, sizeof(transfers) / sizeof(transfers[0]))) {
			success = false;
		}

		if (twiData[1] != data) {
			success = false;
		} else {
			success = true;
		}
	}

	app_twi_uninit(&m_app_twi_mpc);

	if (!success) {
		log_debugf("TWI ERROR: %s task failed to write I2C register (I2C addr: 0x%02X, reg addr: 0x%02X, data: 0x%02X)", pcTaskGetName(NULL), addr, reg, data);
	}

	return success;
}

bool twiRegister_writeData(const nrf_drv_twi_config_t *config, uint8_t addr, uint8_t *data, uint8_t dataLen) {
	bool success = true;

	if (success) {
		success = twiRegister_configureI2C(config);
	}

	// Write:
	if (success) {
		app_twi_transfer_t transfers[] = {
				APP_TWI_WRITE(addr, data, dataLen, 0)
		};

		if (!twiRegister_app_perform(transfers, sizeof(transfers) / sizeof(transfers[0]))) {
			success = false;
		}
	}

	// Verify: This may be complex.

	app_twi_uninit(&m_app_twi_mpc);

	if (!success) {
		log_debugf("TWI ERROR: %s task failed to write multiple (%d) I2C registers (I2C addr: 0x%02X)", pcTaskGetName(NULL), dataLen, addr);
	}

	return success;
}

bool twiRegister_setBits(const nrf_drv_twi_config_t *config, uint8_t addr, uint8_t reg, uint8_t bits) {
	uint8_t twiData[2];
	bool success = true;

	if (success) {
		success = twiRegister_configureI2C(config);
	}

	// Read:
	if (success) {
		app_twi_transfer_t transfers[] = {
				APP_TWI_WRITE(addr, &reg, 1, APP_TWI_NO_STOP),
				APP_TWI_READ(addr, &twiData[1], 1, 0),
		};

		if (!twiRegister_app_perform(transfers, sizeof(transfers) / sizeof(transfers[0]))) {
			success = false;
		}
	}

	// Modify:
	twiData[1] |= bits;

	// Write:
	if (success) {
		twiData[0]  = reg;

		app_twi_transfer_t transfers[] = {
				APP_TWI_WRITE(addr, twiData, 2, 0)
		};

		if (!twiRegister_app_perform(transfers, sizeof(transfers) / sizeof(transfers[0]))) {
			success = false;
		}
	}

	// Verify:
	if (success) {
		app_twi_transfer_t transfers[] = {
				APP_TWI_WRITE(addr, &reg, 1, APP_TWI_NO_STOP),
				APP_TWI_READ(addr, &twiData[1], 1, 0),
		};

		if (!twiRegister_app_perform(transfers, sizeof(transfers) / sizeof(transfers[0]))) {
			success = false;
		}

		if ((twiData[1] & bits) != bits) {
			success = false;
		} else {
			success = true;
		}
	}

	// Cleanup:
	app_twi_uninit(&m_app_twi_mpc);

	if (!success) {
		log_debugf("TWI ERROR: %s task failed to set bits in I2C register (I2C addr: 0x%02X, reg addr: 0x%02X, bits: 0x%02X)", pcTaskGetName(NULL), addr, reg, bits);
	}

	return success;
}

bool twiRegister_clearBits(const nrf_drv_twi_config_t *config, uint8_t addr, uint8_t reg, uint8_t bits) {
	uint8_t twiData[2];
	bool success = true;

	if (success) {
		success = twiRegister_configureI2C(config);
	}

	// Read:
	if (success) {
		app_twi_transfer_t transfers[] = {
				APP_TWI_WRITE(addr, &reg, 1, APP_TWI_NO_STOP),
				APP_TWI_READ(addr, &twiData[1], 1, 0),
		};

		if (!twiRegister_app_perform(transfers, sizeof(transfers) / sizeof(transfers[0]))) {
			success = false;
		}
	}

	// Modify:
	twiData[1] &= ~bits;

	// Write:
	if (success) {
		twiData[0] = reg;

		app_twi_transfer_t transfers[] = {
				APP_TWI_WRITE(addr, twiData, 2, 0)
		};

		if (!twiRegister_app_perform(transfers, sizeof(transfers) / sizeof(transfers[0]))) {
			success = false;
		}
	}

	// Verify:
	if (success) {
		app_twi_transfer_t transfers[] = {
				APP_TWI_WRITE(addr, &reg, 1, APP_TWI_NO_STOP),
				APP_TWI_READ(addr, &twiData[1], 1, 0),
		};

		if (!twiRegister_app_perform(transfers, sizeof(transfers) / sizeof(transfers[0]))) {
			success = false;
		}

		if ((twiData[1] | ~bits) != ~bits) {
			success = false;
		} else {
			success = true;
		}
	}

	// Cleanup:
	app_twi_uninit(&m_app_twi_mpc);

	if (!success) {
		log_debugf("TWI ERROR: %s task to clear bits in I2C register (I2C addr: 0x%02X, reg addr: 0x%02X, bits: 0x%02X)", pcTaskGetName(NULL), addr, reg, bits);
	}

	return success;

}

static TickType_t m_appPerformStartTime;
#define TWI_APP_PERFORM_TIMEOUT_MS		250
static bool m_appPerformDidTimeout = false;

void twiRegister_app_wait(void) {

	if ((xTaskGetTickCount() - m_appPerformStartTime) > TWI_APP_PERFORM_TIMEOUT_MS ) {
		/* if the timeout has expired, uninit the twi interface to release  */
		app_twi_uninit(&m_app_twi_mpc);
		m_appPerformDidTimeout = true;
		log_error("TWI interface timed out!");
		/* reinit the interface to clear the pending transfer flag */
		if (!twiRegister_configureI2C(m_currentTwiConfig)) {
			log_error("Failed to re-initialize TWI interface!");
		}
	}
}


bool twiRegister_app_perform (app_twi_transfer_t const * p_transfers, uint8_t num_transfers) {
	bool success = true;

	/* init timeout */
	m_appPerformDidTimeout = false;
	m_appPerformStartTime = xTaskGetTickCount();

	/* perform transaction */
	if ((app_twi_perform(&m_app_twi_mpc, p_transfers, num_transfers, twiRegister_app_wait) != NRF_SUCCESS)
			|| (m_appPerformDidTimeout)) {
		success = false;
	}

	return success;
}



