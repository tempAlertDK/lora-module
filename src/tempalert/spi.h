/*
 * spi.h
 *
 *  Created on: Feb 9, 2017
 *      Author: Nate-Tempalert
 */

#ifndef SRC_TEMPALERT_SPI_H_
#define SRC_TEMPALERT_SPI_H_

#include <stdint.h>
#include <stdbool.h>

#include "nrf_drv_spi.h"

typedef enum {
#if defined(BOARD_CELL_NODE_REV1P0)
	EXPANSION_CSN = 0,
#elif defined(BOARD_CELL_NODE_REV1P1)
	FLASH_RESETN_CSN = 0,
#endif
	EPD_CSN = 1,
#ifndef BOARD_LORA_MODULE_REV1P0
	LORA_CSN = 2,
#endif
#if defined(BOARD_CELL_NODE_REV1P1)
	EXPANSION_CSN = 3,
#endif
#if defined(BOARD_CELL_NODE_REV1P0)
	ADC_CSN = 4,
#elif defined(BOARD_CELL_NODE_REV1P1)
	GPIO_EXPANDER_RESETN_CSN = 4,
#endif
	ETHERNET_CSN = 5,
	FLASH_CSN = 6,
	EXPANSION_RESETN_CSN = 7
} spi_cs_t;


bool spi_csmux_init(void);
bool spi_csmux_select(spi_cs_t);

bool spi_init(const nrf_drv_spi_config_t *spi_config, nrf_drv_spi_handler_t handler);
bool spi_deinit(void);

bool spi_transfer(const uint8_t * p_tx_data, uint8_t tx_buffer_len, uint8_t * p_rx_data, uint8_t rx_buffer_len);

#endif /* SRC_TEMPALERT_SPI_H_ */
