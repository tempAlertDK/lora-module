/*
 * scController.h
 *
 *  Created on: Feb 15, 2017
 *      Author: Nate-Tempalert
 */

#ifndef SRC_TEMPALERT_SCCONTROLLER_H_
#define SRC_TEMPALERT_SCCONTROLLER_H_

#include <stdint.h>
#include <stdbool.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "genericQueue.h"
#include "stoFwd.h"
#include "packet.h"
#include "notify.h"


#define SERVER_ERROR_DELAY_100MS 100 // 10 seconds

typedef enum {
	UPLOAD_RESULT_SUCCESS = 0,
	UPLOAD_RESULT_NO_MEASUREMENT_AVAIL,
	UPLOAD_RESULT_TOO_MANY_FAILURES,
	UPLOAD_RESULT_INVALID_SF_DATA,
	UPLOAD_RESULT_TOO_OLD,
	UPLOAD_RESULT_MODEM_ERROR,
	UPLOAD_RESULT_SERVER_UNREACHABLE,
	UPLOAD_RESULT_SERVER_GENERAL_NACK,
	UPLOAD_RESULT_SERVER_UNKNOWN_ERROR,
	UPLOAD_RESULT_SERVER_REJECTED_CRC,
	UPLOAD_RESULT_SERVER_REJECTED_ID_UNKNOWN,
	UPLOAD_RESULT_SERVER_REJECTED_ID_BAD_ACCOUNT,
	UPLOAD_RESULT_SERVER_REJECTED_GATEWAY_ID_UNKNOWN,
	UPLOAD_RESULT_SERVER_REJECTED_GATEWAY_ID_BAD_ACCOUNT,
	UPLOAD_RESULT_SERVER_REJECTED_NODE_ID_UNKNOWN,
	UPLOAD_RESULT_SERVER_REJECTED_NODE_ID_BAD_ACCOUNT,
	UPLOAD_RESULT_SERVER_BUSY,
	UPLOAD_RESULT_INVALID_CRC_FROM_SERVER,
	UPLOAD_RESULT_INVALID_SEQNUM_FROM_SERVER,
	UPLOAD_RESULT_INVALID_MSG_FROM_SERVER,
	UPLOAD_RESULT_CANNOT_FORWARD_TO_EMBER
} uploadResult_t;

typedef enum {
	UPLOAD_EVENT_INITIAL_TRANSMITTION = 0,
	UPLOAD_EVENT_UPLOAD_INTEVAL_EXPIRED,
	UPLOAD_EVENT_RECOVERY_INTERVAL_EXPIRED,
	UPLOAD_EVENT_USER_TRIGGERED_BUTTON_PRESS,
	UPLOAD_EVENT_DYNAMIC_INTERVAL_THREASHOLD,
	UPLOAD_EVENT_TR_COMMAND
} scUploadEventType_t;

typedef uint8_t ipAddr_t[16];

/* Task related variables */
extern TaskHandle_t m_scController_thread;

extern EventGroupHandle_t sc_eventGroup;

extern NotifyHandle_t uploadFinishedNotifyHandle;

#define SC_EVENTGROUP_UPLOAD_EVENT_BIT 		1
#define SC_EVENTGROUP_SLEEP_EVENT_BIT		2
#define SC_EVENTGROUP_OTA_EVENT_BIT			4

#define SC_MAX_UPLOAD_ATTEMPTS				5
bool scController_init(void);
void scController_start(void);

uint32_t scController_getSecondsToNextUpload(void);
bool scController_getRecoveryIntervalSecounds(uint32_t * interval);
bool scController_scheduleRecoveryIntervalMins(uint32_t mins);
bool scController_getUploadIntervalSecounds(uint32_t * interval);
void scController_startOTA(uint8_t fw_maj, uint8_t fw_min);
void scController_startSleep(void);
void scController_startUpload(scUploadEventType_t eventType);

#endif /* SRC_TEMPALERT_SCCONTROLLER_H_ */
