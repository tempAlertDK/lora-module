/*
 * buck.h
 *
 *  Created on: Jun 29, 2017
 *      Author: kwgilpin
 */

#ifndef SRC_TEMPALERT_BUCK_H_
#define SRC_TEMPALERT_BUCK_H_

#include <stdbool.h>

typedef enum {
	BUCKMODE_BURST = 0,
	BUCKMODE_PULSE_SKIPPING,
	BUCKMODE_PULSE_SKIPPING_SPREAD_SPECTRUM
} buckmode_t;

bool buck_setBuckmode(buckmode_t mode);

#endif /* SRC_TEMPALERT_BUCK_H_ */
