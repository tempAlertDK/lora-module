/*
 * modem.h
 *
 *  Created on: Feb 7, 2017
 *      Author: kwgilpin
 */

#ifndef SRC_TEMPALERT_MODEM_H_
#define SRC_TEMPALERT_MODEM_H_

#include <stdint.h>
#include <stdbool.h>
#include "queue.h"
#include "packet.h"
#include "event_groups.h"

#define SIG_STRS(X) #X
#define SIG_ENUM(X) X
#define ENUM(name,...) typedef enum {__VA_ARGS__, END_##name} name;
#define STRS(name,...) static const char * name = {__VA_ARGS__};

extern QueueHandle_t modem_genericQueue;

#define RSP_LENGTH_MAX			256

extern EventGroupHandle_t modem_stateEventGroup;
extern EventGroupHandle_t queueEventGroup;

/* Accessible Event Bits */
#define MODEM_OFF_COMMAND_EVENT_GROUP_BIT		0x80

typedef struct {
	uint8_t type;
	uint16_t length;
	char data[RSP_LENGTH_MAX];
} modem_uart_response_t;

typedef enum {
	MODEM_PHY_TURN_ON,
	MODEM_PHY_TURN_OFF,
	MODEM_PHY_QUERY_RSSI,
	MODEM_PHY_GET_STATE,
	MODEM_PHY_DISABLE_FSM,
	MODEM_PHY_ENABLE_FSM
} modem_phy_command_type_t;

typedef struct {
	modem_phy_command_type_t type;
	QueueHandle_t responseQueue;
} modem_phy_command_t;

typedef enum {
	SOCKET_FAILURE,
	SOCKET_CLOSE,
	SOCKET_OPEN,
	SOCKET_CLOSED,
	SOCKET_OPENED,
	SOCKET_CLOSE_ERROR
} modem_socket_command_type_t;

typedef enum {
	UNKNOWN_COMMAND,
	PACKET_SEND,
	PACKET_SEND_SUCCESS,
	PACKET_SEND_FAILURE,
	PACKET_RECEIVE,
	PACKET_RECEIVE_SUCCESS,
	PACKET_RECEIVE_FAILURE
} modem_packet_command_type_t;

typedef struct {
	modem_packet_command_type_t type;
	packet_t packet;
	QueueHandle_t responseQueue;
} modem_packet_command_t;

typedef struct {
	uint8_t rsrp;
	uint8_t rsrq;
} modem_signal_data_t;

typedef struct {
	int sd;				// integer 0-9 socket identifier
	bool open; 			// is socket open?
	char addr[16]; 		// connection address
	uint16_t port; 		// conneciton port
	modem_socket_command_type_t status;
} socket_t;

typedef struct {
	modem_socket_command_type_t type;
	socket_t socket;
	QueueHandle_t responseQueue;
} modem_socket_command_t;

typedef enum {
	MODEM_STATE_UNKOWN = 0,
	MODEM_STATE_OFF,
	MODEM_STATE_BOOTING,
	MODEM_STATE_ONLINE_IDLE,
	MODEM_STATE_BUSY
} modem_state_t;

#define MODEM_STATE_UNKOWN_BIT		(1<<0)
#define	MODEM_STATE_OFF_BIT 			(1<<1)
#define	MODEM_STATE_BOOTING_BIT 		(1<<2)
#define	MODEM_STATE_ONLINE_IDLE_BIT 	(1<<3)
#define	MODEM_STATE_BUSY_BIT			(1<<4)
#define	MODEM_STATE_ADMIN_ACTIVE_BIT	(1<<5)
#define	MODEM_STATE_LOW_BATTERY_BIT	(1<<6)
#define	MODEM_STATE_ACTIVATING_PDP	(1<<7)
#define	MODEM_STATE_REGISTERING		(1<<8)
#define	MODEM_STATE_GENERAL_CONFIG	(1<<8)

modem_state_t g_modem_state;

#if defined (MODEM_TESTING) && MODEM_TESTING == 1
typedef enum {
	MODEM_TESTMODE_OFF,
	MODEM_TESTMODE_BOOTING,
	MODEM_TESTMODE_ON
} modem_test_state_t;
extern modem_test_state_t g_modemTestState;
#endif

#define MODEM_MIN_BATTERY_PERCENTAGE			0


typedef struct {
	uint8_t data[256];
	uint16_t dataLength;
	char addr[16];
	uint16_t port;
} packetStruct_t;

bool modem_init();

uint8_t modem_rssiAdjusted(uint8_t rssi);

bool modem_queueUartResponse(const modem_uart_response_t *p_response);
bool modem_queryModemState();
bool modem_queuePhyCommand(modem_phy_command_type_t command);
bool modem_getSignalQuality(uint8_t * rsrp, uint8_t * rsrq);
bool modem_acknowledgeLowBattery(void);
bool modem_openSocket(socket_t * socket, TickType_t xTicksToWait);
bool modem_closeSocket(socket_t * socket, TickType_t xTicksToWait);
bool modem_sendPacket(int sd, packet_t * pkt, TickType_t xTicksToWait);
bool modem_receivePacket(int sd, packet_t * pkt, TickType_t xTicksToWait);
bool modem_getCurrentState (modem_state_t * state, TickType_t xTicksToWait);

#endif /* SRC_TEMPALERT_MODEM_H_ */
