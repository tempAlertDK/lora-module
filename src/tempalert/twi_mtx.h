/*
 * twi.h
 *
 *  Created on: Jan 12, 2017
 *      Author: kwgilpin
 */

#ifndef SRC_TEMPALERT_TWI_SMPHR_H_
#define SRC_TEMPALERT_TWI_SMPHR_H_

#include "FreeRTOS.h"
#include "portmacro_cmsis.h"

#define TWI_MTX_DEFAULT_DELAY		pdMS_TO_TICKS(1000)

void twi_mtx_init(void);
uint8_t twi_mtx_take(TickType_t ticksToWait);
void twi_mtx_give(void);
void twi_mtx_restore(const uint8_t *taken);

#define WITH_TWI_MTX_CUSTOM_DELAY(ticks) for (uint8_t taken __attribute__((__cleanup__(twi_mtx_restore))) = twi_mtx_take(ticks), __ToDo = 1; __ToDo; __ToDo = 0)
#define WITH_TWI_MTX_DEFAULT_DELAY() for (uint8_t taken __attribute__((__cleanup__(twi_mtx_restore))) = twi_mtx_take(TWI_MTX_DEFAULT_DELAY), __ToDo = 1; __ToDo; __ToDo = 0)

#define GET_MACRO(_0, _1, NAME, ...) NAME
#define WITH_TWI_MTX(...) GET_MACRO(_0, ##__VA_ARGS__, WITH_TWI_MTX_CUSTOM_DELAY, WITH_TWI_MTX_DEFAULT_DELAY)(__VA_ARGS__)


#endif /* SRC_TEMPALERT_TWI_SMPHR_H_ */
