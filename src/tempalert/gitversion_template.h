/*
 * version.h
 *
 * Created: 11/23/2012 12:20:39 PM
 *  Author: kwgilpin
 */ 


#ifndef VERSION_H_
#define VERSION_H_

#define MAJOR_VERSION $MAJOR_VERSION_STR$
#define MINOR_VERSION $MINOR_VERSION_STR$

#if defined (QA) && (QA == 1)
#define TARGET_SERVER 		"QA"
#elif defined (DEV) && (DEV == 1)
#define TARGET_SERVER		"DEV"
#elif (PR) //Production Devices
#define TARGET_SERVER		"PR"
#else
	#error Server enviroment not defined!
#endif

extern const char gitVersionStr[];

extern void log_version(void);

#endif /* VERSION_H_ */
