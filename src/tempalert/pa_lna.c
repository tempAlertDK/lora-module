#include <stdint.h>
#include <string.h>

#include "ble.h"
#include "app_error.h"
#include "nrf_drv_gpiote.h"
#include "nrf_drv_ppi.h"

#include "SEGGER_RTT.h"

#include "FreeRTOS.h"
#include "semphr.h"

#include "boards.h"
#include "twi_mtx.h"
#include "mcp23008.h"
#include "pa_lna.h"

static bool m_initialized = false;

bool pa_lna_init(uint32_t gpio_pa_pin, uint32_t gpio_lna_pin)
{
	bool success = true;
    ble_opt_t opt;
    uint32_t gpiote_ch = 0;
    ret_code_t err_code;        

    memset(&opt, 0, sizeof(ble_opt_t));
    
    err_code = nrf_drv_gpiote_init();
    if(err_code != NRF_ERROR_INVALID_STATE)
        APP_ERROR_CHECK(err_code);
    
    err_code = nrf_drv_ppi_init();
    if(err_code != MODULE_ALREADY_INITIALIZED)
        APP_ERROR_CHECK(err_code);
    
    nrf_ppi_channel_t ppi_set_ch;
    nrf_ppi_channel_t ppi_clr_ch;
    
    err_code = nrf_drv_ppi_channel_alloc(&ppi_set_ch);
    APP_ERROR_CHECK(err_code);
    
    err_code = nrf_drv_ppi_channel_alloc(&ppi_clr_ch);
    APP_ERROR_CHECK(err_code);

    nrf_drv_gpiote_out_config_t config = GPIOTE_CONFIG_OUT_TASK_TOGGLE(false);
    
    if(!(gpio_pa_pin) && !(gpio_lna_pin))
    {
        err_code = NRF_ERROR_INVALID_PARAM;
        APP_ERROR_CHECK(err_code);
    }    

    if(gpio_pa_pin)
    {
        if(gpiote_ch == 0)
        {
            err_code = nrf_drv_gpiote_out_init(gpio_pa_pin, &config);
            APP_ERROR_CHECK(err_code);
            
            gpiote_ch = nrf_drv_gpiote_out_task_addr_get(gpio_pa_pin); 
        }
        
        // PA config
        opt.common_opt.pa_lna.pa_cfg.active_high = 1;   // Set the pin to be active high
        opt.common_opt.pa_lna.pa_cfg.enable      = 1;   // Enable toggling
        opt.common_opt.pa_lna.pa_cfg.gpio_pin    = gpio_pa_pin; // The GPIO pin to toggle tx  
    }
    
    if(gpio_lna_pin)
    {
        if(gpiote_ch == 0)
        {
            err_code = nrf_drv_gpiote_out_init(gpio_lna_pin, &config);
            APP_ERROR_CHECK(err_code);        
            
            gpiote_ch = nrf_drv_gpiote_out_task_addr_get(gpio_lna_pin); 
        }
        
        // LNA config
        opt.common_opt.pa_lna.lna_cfg.active_high  = 1; // Set the pin to be active high
        opt.common_opt.pa_lna.lna_cfg.enable       = 1; // Enable toggling
        opt.common_opt.pa_lna.lna_cfg.gpio_pin     = gpio_lna_pin;  // The GPIO pin to toggle rx
    }

    // Common PA/LNA config
    opt.common_opt.pa_lna.gpiote_ch_id  = (gpiote_ch - NRF_GPIOTE_BASE) >> 2;   // GPIOTE channel used for radio pin toggling
    opt.common_opt.pa_lna.ppi_ch_id_clr = ppi_clr_ch;   // PPI channel used for radio pin clearing
    opt.common_opt.pa_lna.ppi_ch_id_set = ppi_set_ch;   // PPI channel used for radio pin setting
#ifdef DSOFTDEVICE_PRESENT
    err_code = sd_ble_opt_set(BLE_COMMON_OPT_PA_LNA, &opt);
    APP_ERROR_CHECK(err_code);
#endif

    /* In addition to configuring the TX and RX enable lines, we need to ensure
     * that the front-end-module is not in bypass mode.  We assume that since
     * the caller is initializing the PA/LNA, they likely want to use it. */
#if defined(MODEM_POPULATED) && (MODEM_POPULATED == 1) && defined (BOARD_LORA_MODULE_REV1P0)
    WITH_TWI_MTX() {
    	success = mcp23008_setPins(MCP23008_CELL_I2C_ADDR, FEM_BYPASS_EXPANDER_PIN);
	}
#else
	nrf_gpio_cfg_output(FEM_BYPASS_PIN);
	nrf_gpio_pin_set(FEM_BYPASS_PIN);
	success = true;
#endif

	m_initialized = true;

	return success;
}


bool pa_lna_enable() {
	bool success = false;

	if (!m_initialized) {
		return false;
	}

#if defined(MODEM_POPULATED) && (MODEM_POPULATED == 1) && defined (BOARD_LORA_MODULE_REV1P0)
	WITH_TWI_MTX() {
		success = mcp23008_setPins(MCP23008_CELL_I2C_ADDR, FEM_BYPASS_EXPANDER_PIN);
	}
#else
	nrf_gpio_pin_set(FEM_BYPASS_PIN);
	success = true;
#endif

	if (!success) {
		SEGGER_RTT_printf(0, "ERROR: Failed to enable Bluetooth PA/LNA\r\n");
	}

	return success;
}


bool pa_lna_disable() {
	bool success = false;

	if (!m_initialized) {
		return false;
	}

#if defined(MODEM_POPULATED) && (MODEM_POPULATED == 1)
	WITH_TWI_MTX() {
		success = mcp23008_clearPins(MCP23008_CELL_I2C_ADDR, FEM_BYPASS_EXPANDER_PIN);
	}
#else
	nrf_gpio_pin_clear(FEM_BYPASS_PIN);
	success = true;
#endif

	if (!success) {
		SEGGER_RTT_printf(0, "ERROR: Failed to disable Bluetooth PA/LNA\r\n");
	}

	return success;
}
