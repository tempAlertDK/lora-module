/*
 * battTemp.h
 *
 *  Created on: Sep 20, 2017
 *      Author: nreimens
 */

#ifndef SRC_TEMPALERT_BATTTEMP_H_
#define SRC_TEMPALERT_BATTTEMP_H_

#include <stdint.h>
#include "FreeRTOS.h"

void battTemp_init(void); // Module initializer

/* Module iterator: Pass in current conditions to generate a new battery percentage approximation */
uint8_t battTemp_getBatteryPercentage(TickType_t readingTime_ms,  uint16_t batteryVoltage_mV, int16_t temperature_dC);

/* Module internal accessor: Returns low threshold used to calculate recent battery percentage */
uint16_t battTemp_getLastLowThreshold(void);

/* Module internal accessor: Returns low threshold used to calculate recent battery percentage */
uint16_t battTemp_getLastHighThreshold(void);

/* Module external: Returns last battery percentage used to calculate recent battery percentage */
uint16_t battTemp_getLastBatteryPercentage(void);

#endif /* SRC_TEMPALERT_BATTTEMP_H_ */
