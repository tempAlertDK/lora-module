/*
 * uart.h
 *
 *  Created on: Jan 3, 2017
 *      Author: Nate-Tempalert
 */

#ifndef SRC_TEMPALERT_UART_H_
#define SRC_TEMPALERT_UART_H_

bool uart_init(void);
bool uart_sendStr(char * txStr);

#endif /* SRC_TEMPALERT_UART_H_ */
