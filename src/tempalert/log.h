#ifndef SRC_TEMPALERT_LOG_H_
#define SRC_TEMPALERT_LOG_H_

#include "SEGGER_RTT.h"
#include "stdint.h"
#include "stdbool.h"

#define log_segger_rtt(severity, format, ...)  SEGGER_RTT_printf(0, "%s: " format "\r\n", severity, __VA_ARGS__)

#define log_debugf(format, ...) if (log_getLevel() >= LOG_LEVEL_ALL) {log_segger_rtt("DEBUG", format, __VA_ARGS__);}
#define log_debug(x)  log_debugf("%s", x)

#define log_warnf(format, ...) if (log_getLevel() >= LOG_LEVEL_DETAILED) {log_segger_rtt("WARN", format, __VA_ARGS__);}
#define log_warn(x) log_warnf("%s",x)

#define log_infof(format, ...) if (log_getLevel() >= LOG_LEVEL_BASIC) {log_segger_rtt("INFO", format, __VA_ARGS__);}
#define log_info(x) log_infof("%s",x)

#define log_errorf(format, ...) if (log_getLevel() >= LOG_LEVEL_ERROR) {log_segger_rtt("ERROR", format, __VA_ARGS__);}
#define log_error(x) log_errorf("%s",x)

#define log_printf(format, ...) log_segger_rtt("LOG", format, __VA_ARGS__)
#define log_print(x) log_printf("%s",x)
#define log_write(...) SEGGER_RTT_printf(0, __VA_ARGS__)

//
// if these ever need to become functions in the future...
//

// extern void log_debug(char * message);
// extern void log_debugf(const char * format, ...);

// extern void log_info(char * message);
// extern void log_infof(const char * format, ...);

// extern void log_warn(char * message);
// extern void log_warnf(const char * format, ...);

// extern void log_error(char * message);
// extern void log_errorf(const char * format, ...);

enum {
	LOG_LEVEL_NONE = 0,	// Nothing displayed
	LOG_LEVEL_ERROR = 1, // Only Errors displayed
	LOG_LEVEL_BASIC = 2, // Errors and Info
	LOG_LEVEL_DETAILED = 3, // Errors, Info and Warnings
	LOG_LEVEL_ALL = 4 // Errors, Info, Warnings and Debug
};

extern char * log_levelStrs[];

bool log_setLevel (uint8_t level);
uint8_t log_getLevel (void);

#endif /* SRC_TEMPALERT_LOG_H_ */
