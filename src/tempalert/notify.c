/*
 * notify.c
 *
 *  Created on: Aug 30, 2017
 *      Author: nreimens
 */

#include <stdint.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "SEGGER_RTT.h"
#include "event_groups.h"

#include "notify.h"

#define PRINT_FROM_NOTIFY 1
#if (PRINT_FROM_NOTIFY)
#define NOTIFY_PRINT(...) SEGGER_RTT_printf(__VA_ARGS__)
#else
#define NOTIFY_PRINT(...)
#endif

#define CHECK_HANDLE(HANDLE) 	\
	if (HANDLE == NULL) { \
		NOTIFY_PRINT(0, "ERROR: NotifyElement is NULL![%s:%u]\r\n", __FUNCTION__,__LINE__); \
		return NOTIFY_ERROR_NULL_HANDLE; \
	}

int8_t notify_findIndex(Notify_t * nf, NotifyReceipt_t receipt);
NotifyReceipt_t notify_generateRecipt(Notify_t * nf);


bool notifyInit(Notify_t * notifyHandle) {
	uint8_t i;

	CHECK_HANDLE(notifyHandle);

	for (i=0; i<MAX_SUBSCRIPTIONS; i++) {
		notifyHandle->subscriptions[i].eventGroup = NULL;
		notifyHandle->receipts[i] = 0;
	};

	notifyHandle->numSubscribers = 0;
	return true;
}


bool notifySubscribe(NotifyHandle_t notifyHandle, NotifyReceipt_t * p_subscriberReceipt, NotifyInfo_t subscriberInfo) {
	int8_t subIndex;

	CHECK_HANDLE(notifyHandle);

	if (notifyHandle->numSubscribers >= MAX_SUBSCRIPTIONS) {
		NOTIFY_PRINT(0, "ERROR: No subscriptions available![%s:%u]\r\n", __FUNCTION__,__LINE__);
		return false;
	}

	if (subscriberInfo.eventGroup == NULL) {
		NOTIFY_PRINT(0, "ERROR: reported eventGroup is NULL![%s:%u]\r\n", __FUNCTION__,__LINE__);
		return false;
	}

	/* find an empty index */
	subIndex = notify_findIndex(notifyHandle, 0);
	if (subIndex < 0) {
		NOTIFY_PRINT(0, "ERROR: failed to get empty index![%s:%u]\r\n", __FUNCTION__,__LINE__);
		return false;
	}

	/* copy subscriber info & increment subscription count */
	notifyHandle->receipts[subIndex] = notify_generateRecipt(notifyHandle);
	memcpy(&notifyHandle->subscriptions[subIndex],&subscriberInfo,sizeof(NotifyInfo_t));
	notifyHandle->numSubscribers++;

	*p_subscriberReceipt = notifyHandle->receipts[subIndex];

	return true;
}


bool notifyUnsubscribe(NotifyHandle_t notifyHandle, NotifyReceipt_t subscriberReceipt) {

	CHECK_HANDLE(notifyHandle);
	if (subscriberReceipt <= 0) {
		NOTIFY_PRINT(0, "ERROR: Invalid Subscriber Receipt![%s:%u]\r\n", __FUNCTION__,__LINE__);
		return false;
	}

	int8_t subIndex = notify_findIndex(notifyHandle, subscriberReceipt);

	if (subIndex < 0) {
		NOTIFY_PRINT(0, "ERROR: failed to find subscriber Receipt![%s:%u]\r\n", __FUNCTION__,__LINE__);
		return true;
	}

	/* otherwise remove the subscription */
	notifyHandle->numSubscribers--;
	notifyHandle->receipts[subIndex] = 0;
	notifyHandle->subscriptions[subIndex].eventGroup = NULL;

	return true;

}


bool notifySubscribers(NotifyHandle_t notifyHandle, void * item) {
	int8_t i= 0;

	CHECK_HANDLE(notifyHandle);

	for (i = 0; i < MAX_SUBSCRIPTIONS; i++) {
		if (notifyHandle->subscriptions[i].eventGroup != NULL) {
			xEventGroupSetBits(notifyHandle->subscriptions[i].eventGroup,
					notifyHandle->subscriptions[i].eventBit);
			if (notifyHandle->subscriptions[i].respQueue) {
				if (xQueueSend(notifyHandle->subscriptions[i].respQueue,
						item, pdMS_TO_TICKS(100)) == pdFALSE) {
					NOTIFY_PRINT(0, "ERROR: failed queue item to subscriber![%s:%u]\r\n", __FUNCTION__,__LINE__);
				}
			}
		}
	}
	return true;
}


int8_t notify_findIndex(Notify_t * nf, NotifyReceipt_t receipt) {
	int32_t i;
	int32_t id;

	id = -1;

	for (i = 0; i < MAX_SUBSCRIPTIONS; i++) {
		if (nf->receipts[i] == receipt) {
			id = i;
			break;
		}
	}
	return id;
}


NotifyReceipt_t notify_generateRecipt(Notify_t * nf) {
	NotifyReceipt_t recp = nf->lastReceipt;
	bool done = false;
	uint8_t i;

	if (nf == NULL) {
		return NOTIFY_ERROR_NULL_HANDLE;
	}
	
	/* create a new receipt making sure it's > 0
	 * and not already assigned to a subscriber */
	while(!done) {
		if (++recp <= 0) recp = 1;
		done = true;
		for (i = 0; i < MAX_SUBSCRIPTIONS; i++) {
			if (nf->receipts[i] == recp) {
				done = false;
			}
		}
	}
	nf->lastReceipt = recp;
	/* return new receipt */
	return recp;
}
