/*
 * els31uart.h
 *
 *  Created on: Jan 16, 2017
 *      Author: kwgilpin
 */

#ifndef SRC_TEMPALERT_ELS31UART_H_
#define SRC_TEMPALERT_ELS31UART_H_

#include <stdint.h>
#include <stdbool.h>

#include "FreeRTOS.h"

bool els31uart_init(void);
void els31uart_thread(void *arg);

bool els31uart_sendATCmd(const uint8_t *cmd, uint16_t cmdLength, TickType_t xTicksToWait);
bool els31uart_sleepUart(TickType_t xTicksToWait);
bool els31uart_wakeUart(TickType_t xTicksToWait);
void els31uart_prettyPrint(const char *str, uint16_t strLength, const char *header);

#endif /* SRC_TEMPALERT_ELS31UART_H_ */
