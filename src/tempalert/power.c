/*
f * power.c
 *
 *  Created on: Feb 10, 2017
 *      Author: Nate-Tempalert
 */

#include <stdint.h>
#include <stdbool.h>
#include "nrf_gpio.h"
#include "boards.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "power.h"
#include "globals.h"

#ifdef BOARD_CELL_NODE_REV1P0
	#include "ads7866.h"
#elif defined (BOARD_CELL_NODE_REV1P1)
	#include "ads1013.h"
#elif defined (BOARD_LORA_MODULE_REV1P0)
#else
	#error board undefined!
#endif

#if defined(HARDWARE_VERSION) && (HARDWARE_VERSION == 1 || HARDWARE_VERSION == 3)
#define MIN_BATTERY_VOLTAGE_MV			3800UL
#define MAX_BATTERY_VOLTAGE_MV			6000UL
#define BATTERY_WARN_MV					3800UL
#elif defined (HARDWARE_VERSION) && (HARDWARE_VERSION == 2)
#define MIN_BATTERY_VOLTAGE_MV			3400UL
#define MAX_BATTERY_VOLTAGE_MV			4200UL
#define BATTERY_WARN_MV					3600UL
#else
#error undefined board type!!
#endif

#define BATTERY_STATE_FACTOR			(((MAX_BATTERY_VOLTAGE_MV - MIN_BATTERY_VOLTAGE_MV) / 10UL))

bool power_init() {
#ifdef BOARD_CELL_NODE_REV1P0
	nrf_gpio_cfg_input(ACPG_PIN, NRF_GPIO_PIN_NOPULL);
#elif defined (BOARD_CELL_NODE_REV1P1)
	nrf_gpio_cfg_input(ACPG_VBATEN_PIN, NRF_GPIO_PIN_NOPULL);
#endif
	return true;
}

bool power_readBattery(uint16_t * batteryVoltage_mV) {
	if (g_simBatt) {
		*batteryVoltage_mV = g_simBattVoltage;
		return true;
	}
#ifdef BOARD_CELL_NODE_REV1P0
	return ads7866_readBatteryVoltage(batteryVoltage_mV);
#elif defined (BOARD_CELL_NODE_REV1P1)
	return ads1013_readBatteryVoltage(batteryVoltage_mV);
#elif defined (BOARD_LORA_MODULE_REV1P0)
	//todo
	return false;
#endif

}

bool power_acPowerGood(void) {
#if defined(BOARD_CELL_NODE_REV1P0)
	return nrf_gpio_pin_read(ACPG_PIN);
#elif defined(BOARD_CELL_NODE_REV1P1)
	return nrf_gpio_pin_read(ACPG_VBATEN_PIN);
#endif
	//LoRa Todo:
	return false;

}

bool power_getBatteryLow(bool *batteryLow, bool verbose) {
	return false;
}

bool power_checkBatteryVoltageLow(uint16_t vbat_mV) {
	if (vbat_mV <= BATTERY_WARN_MV) {
		return true;
	} else {
		return false;
	}
}

bool power_batteryState(int8_t * batteryState, uint16_t batteryVoltage_mv) {

	if (batteryVoltage_mv > MAX_BATTERY_VOLTAGE_MV) {
		/* Clamp output voltage TODO: Is it worth determining if an overvoltage condition has occurred? */
		batteryVoltage_mv = MAX_BATTERY_VOLTAGE_MV;
	}
	if (batteryVoltage_mv > MIN_BATTERY_VOLTAGE_MV) {
		/* Convert to 0-10 value to send to core */
		*batteryState = (batteryVoltage_mv - MIN_BATTERY_VOLTAGE_MV) / BATTERY_STATE_FACTOR;
	} else {
		*batteryState = 0;
	}
	if (power_acPowerGood()) {
		*batteryState |= 0x10;
	}

	return true;
}

