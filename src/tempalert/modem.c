/*
 * modem.c
 *
 *  Created on: Feb 2, 2017
 *      Author: kwgilpin
 */

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include "log.h"
#include "stoFwd.h"

#include "nordic_common.h"
#include "nrf.h"
#include "nrf_error.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include "semphr.h"
#include "event_groups.h"
#include "portmacro_cmsis.h"

#include "SEGGER_RTT.h"
#include "utils.h"

#include "simplehsm.h"

#include "ui.h"

#include "els31uart.h"
#include "els31.h"

#include "modem.h"
#include "power.h"

#include "notify.h"
#include "sensors.h"
#include "errorCount.h"

#include "buck.h"

#define DEBUG_MODEM_HSM 1

typedef enum {
	TimerExpiredEvent
} modem_eventType_t;

typedef struct {
	modem_eventType_t eventType;
	uint8_t *data;
} modem_event_t;

#if defined (MODEM_TESTING) && MODEM_TESTING == 1
modem_test_state_t g_modemTestState;
#endif


typedef enum {
	SIG_TIMEOUT = SIG_USER,
	SIG_MODEM_COMM,
	SIG_TURN_MODEM_ON,
	SIG_TURN_MODEM_OFF,
	SIG_AT_CMD_AT,
	SIG_AT_CMD_SMSO,
	SIG_AT_CMD_CFUN_EQUALS_0,
	SIG_AT_CMD_CFUN_EQUALS_1,
	SIG_AT_CMD_SCFG_FSO,
	SIG_AT_CMD_SCFG_FSR,
	SIG_AT_CMD_CEREG_EQUALS_1,
	SIG_AT_CMD_CEREG_QUERY,
	SIG_AT_CMD_SICA_ACTIVATE,
	SIG_AT_CMD_SICA_QUERY,
	SIG_AT_CMD_SICA_DEACTIVATE,
	SIG_AT_CMD_SICS_SET_DNS1,
	SIG_AT_CMD_SICS_SET_DNS2,
	SIG_AT_CMD_CGPADDR,
	SIG_AT_CMD_SCFG_WITHURCS,
	SIG_AT_CMD_SISS_CONID,
	SIG_AT_CMD_SISS_SRVTYPE,
	SIG_AT_CMD_SISS_ADDRESS,
	SIG_AT_CMD_SISO,
	SIG_AT_CMD_SISC,
	SIG_AT_CMD_SISW_ECHOED,
	SIG_AT_CMD_SISW_COMPLETE,
	SIG_AT_CMD_SISR,
	SIG_TX_PACKET,
	SIG_URC_SYSSTART,
	SIG_URC_SBC,
	SIG_URC_SHUTDOWN,
	SIG_URC_SISR,
	SIG_URC_SISW,
	SIG_URC_SIS,
	SIG_URC_CEREG,
	SIG_URC_SCTM_B,
	SIG_URC_SMSO_MS_OFF,
	SIG_MEASURE_RSSI,
	SIG_AT_CMD_CESQ,
	SIG_OPEN_SOCKET,
	SIG_CLOSE_SOCKET,
	SIG_SOCKET_ERROR,
	SIG_RX_PACKET,
	SIG_ENTER_TESTING,
	SIG_EXIT_TESTING,
	SIG_AT_CMD_SISS_SECOPT,
	SIG_URC_TCP_REMOTE_CLOSE,
} hsm_signal_t;

static const char *m_signalNames[50];

static bool modemHasBeenOn = false;

typedef union {
	modem_uart_response_t els31uart_response;
} genericQueueData_t;

/* Define Static Queue Variables and Buffers here. */

#define PACKETQUEUE_LENGTH	2
#define PACKETQUEUE_ITEMSIZE sizeof(modem_packet_command_t)
uint8_t packetQueueBuffer[PACKETQUEUE_LENGTH*PACKETQUEUE_ITEMSIZE];
static StaticQueue_t packetQueueStatic;

#define RESPONSEQUEUE_LENGTH	6
#define RESPONSEQUEUE_ITEMSIZE sizeof(modem_uart_response_t)
uint8_t responseQueueBuffer[RESPONSEQUEUE_LENGTH*RESPONSEQUEUE_ITEMSIZE];
static StaticQueue_t responseQueueStatic;

#define PHYQUEUE_LENGTH	1
#define PHYQUEUE_ITEMSIZE sizeof(modem_phy_command_t)
uint8_t phyQueueBuffer[PHYQUEUE_LENGTH*PHYQUEUE_ITEMSIZE];
static StaticQueue_t phyQueueStatic;

#define APPPHYQUEUE_LENGTH	1
#define APPPHYQUEUE_ITEMSIZE sizeof(modem_phy_command_t)
uint8_t appPhyQueueBuffer[APPPHYQUEUE_LENGTH*APPPHYQUEUE_ITEMSIZE];
static StaticQueue_t appPhyQueueStatic;

#define SOCKETQUEUE_LENGTH	2
#define SOCKETQUEUE_ITEMSIZE sizeof(modem_socket_command_t)
uint8_t socketQueueBuffer[SOCKETQUEUE_LENGTH*SOCKETQUEUE_ITEMSIZE];
static StaticQueue_t socketQueueStatic;

/*****************************************************************/




static simplehsm_t hsm = {NULL};

QueueHandle_t modem_genericQueue;
EventGroupHandle_t modem_stateEventGroup = NULL;
StaticEventGroup_t modem_stateEventGroupStatic;

static QueueHandle_t responseQueue = NULL;
static QueueHandle_t phyQueue = NULL;
static QueueHandle_t socketQueue = NULL;
static QueueHandle_t appPhyQueue = NULL;

EventGroupHandle_t queueEventGroup = NULL;
static StaticEventGroup_t queueEventGroupStatic;

static EventBits_t queueEventGroupMask = 0;
static EventBits_t activeBit = 0;
static SemaphoreHandle_t queueEventGroupMutex = NULL;
static StaticSemaphore_t queueEventGroupMutexBuffer;

#define RESPONSE_QUEUE_EVENT_GROUP_BIT			0x01
#define PHY_QUEUE_EVENT_GROUP_BIT				0x02
#define SOCKET_QUEUE_EVENT_GROUP_BIT				0x04
#define GENERAL_PURPOSE_TIMER_EVENT_GROUP_BIT	0x08
#define AT_COMMAND_TIMER_EVENT_GROUP_BIT			0x10
#define PACKET_COMMAND_EVENT_GROUP_BIT			0x20
#define APP_PHY_QUEUE_EVENT_GROUP_BIT			0x40
//#define MODEM_OFF_COMMAND_EVENT_GROUP_BIT		0x80
#define LOWBATT_NOTIFY_EVENT_GROUP_BIT			0x100

/* Default bits indicating the normally active event triggers */
#define DEFAULT_MODEM_EVENT_GROUP_BITS  RESPONSE_QUEUE_EVENT_GROUP_BIT | \
										GENERAL_PURPOSE_TIMER_EVENT_GROUP_BIT | \
										AT_COMMAND_TIMER_EVENT_GROUP_BIT | \
										MODEM_OFF_COMMAND_EVENT_GROUP_BIT | LOWBATT_NOTIFY_EVENT_GROUP_BIT
/* Define the application bits that will mask events if not in idle states */
#define APPLICATION_EVENT_GROUP_BITS			(APP_PHY_QUEUE_EVENT_GROUP_BIT | SOCKET_QUEUE_EVENT_GROUP_BIT | PACKET_COMMAND_EVENT_GROUP_BIT)

/* notifications */
NotifyReceipt_t modemlowBatteryNotification;
NotifyInfo_t modemlowBattNotifyInfo = {.eventBit = LOWBATT_NOTIFY_EVENT_GROUP_BIT,
		.respQueue = NULL};



static TimerHandle_t gpTimer;
static StaticTimer_t gpTimerStatic;
static TaskHandle_t m_modem_thread;
StaticTask_t m_modem_thread_static;
#define MODEM_STACK_SIZE		1024
StackType_t m_modem_thread_buffer[ MODEM_STACK_SIZE ];

static hsm_signal_t sendATCmd_complete_signal = SIG_NULL;
static void *sendATCmd_extraParam = NULL;

static char sendATCmd_cmd[256];
static uint16_t sendATCmd_cmdLen = 0;

static uint8_t sendATCmd_auxData[512];
static size_t sendATCmd_auxDataLength;

static bool sendATCmd_respExpected = false;

typedef enum {
	SENDATCMD_RESULT_OK,
	SENDATCMD_RESULT_ERROR,
	SENDATCMD_RESULT_TIMEOUT
} sendATCmd_result_t;

/* Enum describing the four possible reasons for ^SBC URC */
typedef enum {
	UNDERVOLTAGE_WARNING,
	UNDERVOLTAGE_SHUTDOWN,
	OVERVOLTAGE_WARNING,
	OVERVOLTAGE_SHUTDOWN
} urc_sbc_states_t;

/* Struct holding parameters of the ^SBC URC */
typedef struct {
	urc_sbc_states_t urcCause;
} urc_sbc_params_t;

/* Struct holding parameters of the +CEREG URC */
typedef struct {
	unsigned int stat;
	unsigned int tac;
	unsigned int ci;
	unsigned int AcT;
} urc_cereg_params_t;

typedef struct {
	unsigned int srvProfileId;
	unsigned int urcCauseId;
} urc_sisr_params_t;

typedef struct {
	unsigned int srvProfileId;
	unsigned int urcCauseId;
} urc_sisw_params_t;

typedef struct {
	unsigned int srvProfileId;
	unsigned int urcCause;
	unsigned int urcInfoId;
	char urcInfoText[256];
} urc_sis_params_t;

typedef struct {
	int UrcCause;
} urc_sctm_b_params_t;

typedef struct {
	unsigned int srvProfileId;
	unsigned int cnfReadLength;
	unsigned int remainUdpPacketLength; // Optional, number of bytes not read, still waiting in buffer
	char Udp_RemClient[22]; // Optional, holds remote IP:port if modem is acting as end-point (server)
} cmd_sisr_params_t;

typedef struct {
	sendATCmd_result_t result;
	union {
		urc_sbc_params_t urc_sbc;
		urc_sisr_params_t urc_sisr;
		urc_sisw_params_t urc_sisw;
		urc_sis_params_t urc_sis;
		urc_cereg_params_t urc_cereg;
		urc_sctm_b_params_t urc_sctm_b;
		cmd_sisr_params_t cmd_sisr;
	} parsed;
	char raw[512];
	size_t rawLength;
	void *extraParam;
} sendATCmd_signal_param_t;

typedef enum {
	SENDATCMD_STATE_IDLE,
	SENDATCMD_STATE_WAITING_FOR_COMMAND_ECHO,
	SENDATCMD_STATE_WAITING_FOR_SISW_BYTE_COUNT,
	SENDATCMD_STATE_WAITING_FOR_DATA_ECHO,
	SENDATCMD_STATE_WAITING_FOR_SISR_BYTE_COUNT,
	SENDATCMD_STATE_WAITING_FOR_RECEIVED_DATA,
	SENDATCMD_STATE_WAITING_FOR_RESPONSE
} sendATCmd_state_t;

static sendATCmd_state_t sendATCmd_state = SENDATCMD_STATE_IDLE;
static TimerHandle_t sendATCmd_timer;
static StaticTimer_t sendATCmd_timerStatic;

static SemaphoreHandle_t packetSemphr;
static StaticSemaphore_t packetSemphrBuffer;

static bool packetReady = false;
static packetStruct_t txpacket;
static QueueHandle_t packetQueue;

/*
 * Socket handles:
 *
 * Socket handles will be used to protect sockets from access once
 * requested by a task. A task attempt to open a socket and pass its
 * task handle as an identifier. This identifier will be used to "lock"
 * the socket number to is owner task to prevent other tasks from utilizing
 * the same socket. If the task handle is NULL, it is assumed that the socket
 * is not in use. When the task attempts to utilize a socket connection, it's
 * task handle is checked against socket number to determine if the socket is
 * valid.
 * */
#define MAX_AVAILIBLE_SOCKETS 10

typedef struct {
	TaskHandle_t task;
} socketHandle_t;

socketHandle_t m_socketHandleArray[MAX_AVAILIBLE_SOCKETS];
SemaphoreHandle_t socketHandlesMutex;
StaticSemaphore_t socketHandlesMutexBuffer;

void modem_thread(void *arg);

static stnext state_top(int signal, void *param);
  static stnext state_off(int signal, void *param);
  static stnext state_on(int signal, void *param);
    static stnext state_checkResponsive(int signal, void *param);
    static stnext state_emergencyReset(int signal, void *param);
    static stnext state_booting(int signal, void *param);
    static stnext state_generalConfig(int signal, void *param);
    static stnext state_testing(int signal, void *param);
    static stnext state_registering(int signal, void *param);
    static stnext state_activating_pdp(int signal, void *param);
    static stnext state_online(int signal, void *param);
	  static stnext state_online_idle(int signal, void *param);
	  static stnext state_opensocket(int signal, void *param);
	  static stnext state_txing(int signal, void *param);
	  static stnext state_rxing(int signal, void *param);
	  static stnext state_closesocket(int signal, void *param);
	  //static stnext state_dnsing(int signal, void *param);
    static stnext state_turnoff(int signal, void *param);
    static stnext state_forceShutdown(int signal, void *param);


static void modem_printRegistrationStatus(unsigned int regStatus);

static bool modem_sendATCmdString(const char *cmdString, TickType_t xTicksToWait, hsm_signal_t onCompleteSignal);
static bool modem_sendATCmdStringAux(const char *cmdString, const uint8_t *auxData, size_t auxDataLength, TickType_t xTicksToWait, hsm_signal_t onCompleteSignal, void *extraParam);
static bool modem_sendData(const uint8_t *cmd, uint16_t cmdLength, const uint8_t *auxData, size_t auxDataLength, bool responseExpected, TickType_t xTicksToWait, hsm_signal_t onCompleteSignal, void *extraParam);

static void modem_cancelATCmd(void);
static bool processModemResponseLine(modem_uart_response_t *p_response, hsm_signal_t *p_signal, sendATCmd_signal_param_t *p_param);
static bool processModemURC(modem_uart_response_t *p_response, hsm_signal_t *p_signal, sendATCmd_signal_param_t *p_param);
static bool modem_atCommandTimerExpired(hsm_signal_t *p_signal, sendATCmd_signal_param_t *p_param);
static void modem_atCommandTimerCallback(TimerHandle_t timer);

static void modem_generalPurposeTimerCallback(TimerHandle_t timer);

static const char *substr(const char *haystack, const char *needle);

bool modem_init(void) {
	m_signalNames[0] = "SIG_NULL";
	m_signalNames[1] = "SIG_INIT";
	m_signalNames[2] = "SIG_ENTRY";
	m_signalNames[3] = "SIG_DEEPHIST";
	m_signalNames[4] = "SIG_EXIT";
	m_signalNames[5] = "SIG_TIMEOUT";
	m_signalNames[6] = "SIG_MODEM_COMM";
	m_signalNames[7] = "SIG_TURN_MODEM_ON";
	m_signalNames[8] = "SIG_TURN_MODEM_OFF";
	m_signalNames[9] = "SIG_AT_CMD_AT";
	m_signalNames[10] = "SIG_AT_CMD_SMSO";
	m_signalNames[11] = "SIG_AT_CMD_CFUN_EQUALS_0";
	m_signalNames[12] = "SIG_AT_CMD_CFUN_EQUALS_1";
	m_signalNames[13] = "SIG_AT_CMD_SCFG_FSO";
	m_signalNames[14] = "SIG_AT_CMD_SCFG_FSR";
	m_signalNames[15] = "SIG_AT_CMD_CEREG_EQUALS_1";
	m_signalNames[16] = "SIG_AT_CMD_CEREG_QUERY";
	m_signalNames[17] = "SIG_AT_CMD_SICA_ACTIVATE";
	m_signalNames[18] = "SIG_AT_CMD_SICA_QUERY";
	m_signalNames[19] = "SIG_AT_CMD_SICA_DEACTIVATE";
	m_signalNames[20] = "SIG_AT_CMD_SICS_SET_DNS1";
	m_signalNames[21] = "SIG_AT_CMD_SICS_SET_DNS2";
	m_signalNames[22] = "SIG_AT_CMD_CGPADDR";

	m_signalNames[23] = "SIG_AT_CMD_SCFG_WITHURCS";
	m_signalNames[24] = "SIG_AT_CMD_SISS_CONID";
	m_signalNames[25] = "SIG_AT-CMD_SISS_SRVTYPE";
	m_signalNames[26] = "SIG_AT_CMD_SISS_ADDRESS";
	m_signalNames[27] = "SIG_AT_CMD_SISO";
	m_signalNames[28] = "SIG_AT_CMD_SISC";

	m_signalNames[29] = "SIG_AT_CMD_SISW_ECHOED";
	m_signalNames[30] = "SIG_AT_CMD_SISW_COMPLETE";
	m_signalNames[31] = "SIG_AT_CMD_SISR";

	m_signalNames[32] = "SIG_TX_PACKET";

	m_signalNames[33] = "SIG_URC_SYSSTART";
	m_signalNames[34] = "SIG_URC_SBC";
	m_signalNames[35] = "SIG_URC_SHUTDOWN";
	m_signalNames[36] = "SIG_URC_SISR";
	m_signalNames[37] = "SIG_URC_SISW";
	m_signalNames[38] = "SIG_URC_SIS";
	m_signalNames[39] = "SIG_URC_CEREG";
	m_signalNames[40] = "SIG_URC_SCTM_B";
	m_signalNames[41] = "SIG_URC_SMSO_MS_OFF";
	m_signalNames[42] = "SIG_MEASURE_RSSI";
	m_signalNames[43] = "SIG_AT_CMD_CESQ";
	m_signalNames[44] = "SIG_OPEN_SOCKET";
	m_signalNames[45] = "SIG_CLOSE_SOCKET";
	m_signalNames[46] = "SIG_SOCKET_ERROR";
	m_signalNames[47] = "SIG_RX_PACKET";
	m_signalNames[48] = "SIG_ENTER_TESTING";
	m_signalNames[49] = "SIG_EXIT_TESTING";
	m_signalNames[50] = "SIG_AT_CMD_SISS_SECOPT";
	m_signalNames[51] = "SIG_URC_TCP_REMOTE_CLOSE";
	
	// For testing only
	packetReady = true;
	strcpy((char *)txpacket.data, "DEAD\r\nBEEF\r");
	txpacket.dataLength = strlen((char *)txpacket.data);
	strcpy(txpacket.addr, "72.42.164.102");
	txpacket.port = 8022;

	// Initialize socket handles
	for (int i = 0; i < MAX_AVAILIBLE_SOCKETS; i++) {
		m_socketHandleArray[i].task = NULL;
	}
	if ((socketHandlesMutex = xSemaphoreCreateMutexStatic(&socketHandlesMutexBuffer)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	// Queue to hold packet requests
	if ((packetQueue = xQueueCreateStatic(PACKETQUEUE_LENGTH,PACKETQUEUE_ITEMSIZE,packetQueueBuffer,&packetQueueStatic)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	// Queue to hold lines from the modem UART driver
	if ((responseQueue = xQueueCreateStatic(RESPONSEQUEUE_LENGTH,RESPONSEQUEUE_ITEMSIZE,responseQueueBuffer,&responseQueueStatic)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	// Queue to hold physical layer commands (e.g. turn modem on / off)
	if ((phyQueue = xQueueCreateStatic(PHYQUEUE_LENGTH,PHYQUEUE_ITEMSIZE,phyQueueBuffer,&phyQueueStatic)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	// Queue to hold application calls to the physical layer
	if ((appPhyQueue = xQueueCreateStatic(APPPHYQUEUE_LENGTH,APPPHYQUEUE_ITEMSIZE,appPhyQueueBuffer,&appPhyQueueStatic)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	// Create queue to hold socket requests
	if ((socketQueue = xQueueCreateStatic(SOCKETQUEUE_LENGTH,SOCKETQUEUE_ITEMSIZE,socketQueueBuffer,&socketQueueStatic)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	// Event group holds bits indicating which queue have data available for receiving
	if ((queueEventGroup = xEventGroupCreateStatic(&queueEventGroupStatic)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	// Create mutex to protect mask bits
	if ((queueEventGroupMutex = xSemaphoreCreateMutexStatic(&queueEventGroupMutexBuffer)) == NULL) {
			APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	gpTimer = xTimerCreateStatic("Modem General Purpose Timer", pdMS_TO_TICKS(100), pdFALSE, (void *)0, modem_generalPurposeTimerCallback, &gpTimerStatic);
	sendATCmd_timer = xTimerCreateStatic("Modem AT Command Timer", pdMS_TO_TICKS(100), pdFALSE, (void *)0, modem_atCommandTimerCallback, &sendATCmd_timerStatic);

	/* Create event group bits for exposing modem state. */
	modem_stateEventGroup = xEventGroupCreateStatic(&modem_stateEventGroupStatic);

	packetSemphr = xSemaphoreCreateBinaryStatic(&packetSemphrBuffer);
	xSemaphoreGive(packetSemphr);

	/* initialize notifications */
	modemlowBattNotifyInfo.eventGroup = queueEventGroup;

	// Start execution.
	if ((m_modem_thread = xTaskCreateStatic(modem_thread, "MODEM", MODEM_STACK_SIZE, NULL, 1, m_modem_thread_buffer, &m_modem_thread_static)) == NULL ) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	return true;
}

bool modem_queueUartResponse(const modem_uart_response_t *p_response) {
	bool success = false;

	if ((responseQueue != NULL) && (queueEventGroup != NULL)) {
		if (xQueueSend(responseQueue, p_response, pdMS_TO_TICKS(100)) == pdTRUE) {
			xEventGroupSetBits(queueEventGroup, RESPONSE_QUEUE_EVENT_GROUP_BIT);
			success = true;
		}
	}

	return success;
}

enum {
	SOCKET_SUCCESS,
	INVALID_TASK_HANDLE,
	INVALID_SOCKET_IDENTIFIER,
	NO_SOCKETS_AVAILIBLE,
	SOCKET_HANDLE_ACCESS_FAILURE,
	SOCKETS_NOT_INITAILIZED
};

void socket_init(void) {
	// Initialize socket handles
	for (int i = 0; i < MAX_AVAILIBLE_SOCKETS; i++) {
		m_socketHandleArray[i].task = NULL;
	}
}

int socket_getHandle(int * socket) {
	int i;

	if (socketHandlesMutex == NULL) {
		return SOCKETS_NOT_INITAILIZED;
	}

	if (xSemaphoreGetMutexHolder(socketHandlesMutex) != xTaskGetCurrentTaskHandle()) {
		SEGGER_RTT_printf(0, "ERROR: Socket mutex already in use. [%s]\r\n", __FUNCTION__);
		return SOCKET_HANDLE_ACCESS_FAILURE;
	}
	/* Attempt to obtain a valid socket handle */
	for (i = 0; i < MAX_AVAILIBLE_SOCKETS; i++) {
		if (m_socketHandleArray[i].task == NULL)
			break;
	}
	if (i == MAX_AVAILIBLE_SOCKETS) {
		SEGGER_RTT_printf(0, "ERROR: All socket handles currently in use. [%s]\r\n", __FUNCTION__);
		return NO_SOCKETS_AVAILIBLE;
	}

	/* Mark socket handle as obtained by current task */
	m_socketHandleArray[i].task = xTaskGetCurrentTaskHandle();
	*socket = i;

	return SOCKET_SUCCESS;
}


bool socket_checkHandle(int socket) {

	if (socketHandlesMutex == NULL) {
		return SOCKETS_NOT_INITAILIZED;
	}

	if (m_socketHandleArray[socket].task == NULL) {
		/* We allow a NULL socket to be closed in case the socket is opened from
		 * a previous function and was not closed properly. Also if the device experianced
		 * a reset and the modem was not initialized properly. TODO: initialize sockets on RESET */
		SEGGER_RTT_printf(0, "WARN: Socket handle not valid [%s]\r\n", __FUNCTION__);
		return true;
	} else if (m_socketHandleArray[socket].task != xTaskGetCurrentTaskHandle()) {
		SEGGER_RTT_printf(0, "ERROR: Socket is locked by another task! [%s]\r\n", __FUNCTION__);
		return false;
	}
	return true;
}

int socket_giveHandle(int socket) {

	if (socketHandlesMutex == NULL) {
		return SOCKETS_NOT_INITAILIZED;
	}

	if (m_socketHandleArray[socket].task == NULL) {
		SEGGER_RTT_printf(0, "WARN: Socket handle already NULL. [%s]\r\n", __FUNCTION__);
		return SOCKET_SUCCESS;
	} else if (m_socketHandleArray[socket].task != xTaskGetCurrentTaskHandle()) {
		SEGGER_RTT_printf(0, "ERROR: Socket is locked by another task! [%s]\r\n", __FUNCTION__);
		return INVALID_SOCKET_IDENTIFIER;
	} else if (xSemaphoreGetMutexHolder(socketHandlesMutex) != xTaskGetCurrentTaskHandle()) {
		SEGGER_RTT_printf(0, "ERROR: Socket mutex already in use! [%s]\r\n", __FUNCTION__);
		return SOCKET_HANDLE_ACCESS_FAILURE;
	}
	/* Release socket handle obtained by current task */
	m_socketHandleArray[socket].task = NULL;
	return SOCKET_SUCCESS;
}

//static QueueHandle_t rspQueue = NULL;
static StaticQueue_t socketResponceStaticQueue;

bool modem_openSocket(socket_t * socket, TickType_t xTicksToWait) {
	int err;
	bool success = true;
	modem_socket_command_t sd_cmd;

	QueueHandle_t rspQueue = NULL;
	uint8_t rspQueueBuffer[ 1 * sizeof(modem_socket_command_t) ];


	if (socketQueue && socketHandlesMutex && queueEventGroup) {
		rspQueue = xQueueCreateStatic(1, sizeof(modem_socket_command_t), rspQueueBuffer, &socketResponceStaticQueue);
		if (rspQueue == NULL) {
			SEGGER_RTT_printf(0, "ERROR: Failed to create queue![%s:%u]\r\n", __FUNCTION__,__LINE__);
			success = false;
		} else {

			/* Take socket handle mutex */
			if (xSemaphoreTake(socketHandlesMutex, pdMS_TO_TICKS(1000)) == pdFALSE) {
				SEGGER_RTT_printf(0, "ERROR: Failed to obtain socket handle mutex![%s:%u]\r\n", __FUNCTION__,__LINE__);
				success = false;
			} else {
				err = socket_getHandle(&socket->sd);

				if (err == SOCKET_SUCCESS) {
					memcpy(&sd_cmd.socket,socket,sizeof(socket_t));
					sd_cmd.type = SOCKET_OPEN;
					sd_cmd.responseQueue = rspQueue;

					if (xQueueSend(socketQueue, &sd_cmd, pdMS_TO_TICKS(100)) == pdTRUE) {
						xEventGroupSetBits(queueEventGroup, SOCKET_QUEUE_EVENT_GROUP_BIT);
						if (xQueueReceive(rspQueue, &sd_cmd, xTicksToWait)) {
							if (sd_cmd.type == SOCKET_OPENED) {
								success = true;
								/* overwrite passed socket with received socket,
								 * we do this in case the modem has modified original
								 * socket parameters. */
								memcpy(socket,&sd_cmd.socket,sizeof(socket_t));
							} else {
								SEGGER_RTT_printf(0, "ERROR: Socket failed to open![%s:%u]\r\n", __FUNCTION__,__LINE__);
								success = false;
							}
							/* report received cmd in socket status */
							socket->status = sd_cmd.type;
						} else {
							SEGGER_RTT_printf(0, "ERROR: No response from modem state machine![%s:%u]\r\n", __FUNCTION__,__LINE__);
							success = false;
						}
					} else {
						SEGGER_RTT_printf(0, "ERROR: Failed to send socket open command to state machine![%s:%u]\r\n", __FUNCTION__,__LINE__);
						success = false;
					}

					/* if the modem state machine failed to successfully open the socket, make sure we unlock it. */
					if (!success) {
						socket_giveHandle(socket->sd);
					}

				} else {
					SEGGER_RTT_printf(0, "ERROR: Failed to obtain socket handle [E%d] in %s\r\n", err, __FUNCTION__);
					success = false;
				}

				/* Regardless of the outcome return the socket mutex */
				xSemaphoreGive(socketHandlesMutex);
			}
		}
	} else {
		SEGGER_RTT_printf(0, "ERROR: Called before modem task initialization![%s:%u]\r\n", __FUNCTION__,__LINE__);
		success = false;
	}
	if (!success) {
		errorCount_incrementCount(ERROR_FAILED_TO_OPEN_UDP_SOCKET);
	}
	return success;
}

bool modem_closeSocket(socket_t * socket, TickType_t xTicksToWait){
	bool success = true;
	modem_socket_command_t sd_cmd;

	QueueHandle_t rspQueue = NULL;
	uint8_t rspQueueBuffer[ 1 * sizeof(modem_socket_command_t) ];

	if (socketQueue && socketHandlesMutex && queueEventGroup) {
		rspQueue = xQueueCreateStatic(1, sizeof(modem_socket_command_t), rspQueueBuffer, &socketResponceStaticQueue);
		if (rspQueue == NULL) {
			SEGGER_RTT_printf(0, "ERROR: Failed to create queue![%s:%u]\r\n", __FUNCTION__,__LINE__);
			success = false;
		} else {

			/* Take socket handle mutex */
			if (xSemaphoreTake(socketHandlesMutex, pdMS_TO_TICKS(1000)) == pdFALSE) {
				SEGGER_RTT_printf(0, "ERROR: Failed to obtain socket handle mutex![%s:%u]\r\n", __FUNCTION__,__LINE__);
				success = false;
			} else {
				if (socket_checkHandle(socket->sd)) {
					memcpy(&sd_cmd.socket,socket,sizeof(socket_t));
					sd_cmd.type = SOCKET_CLOSE;
					sd_cmd.responseQueue = rspQueue;

					if (xQueueSend(socketQueue, &sd_cmd, pdMS_TO_TICKS(100)) == pdTRUE) {
						xEventGroupSetBits(queueEventGroup, SOCKET_QUEUE_EVENT_GROUP_BIT);
						if (xQueueReceive(rspQueue, &sd_cmd, xTicksToWait)) {
							if ((sd_cmd.type == SOCKET_CLOSED) || (sd_cmd.type == SOCKET_CLOSE_ERROR)) {
								success = true;
								/* because we know that socket handle is valid, we can safely return it */
								socket_giveHandle(socket->sd);
								/* overwrite passed socket with received socket,
								 * we do this in case the modem has modified original
								 * socket parameters. */
								memcpy(socket,&sd_cmd.socket,sizeof(socket_t));
							} else {
								SEGGER_RTT_printf(0, "ERROR: Socket failed to close![%s:%u]\r\n", __FUNCTION__,__LINE__);
								success = false;
							}
							/* report received cmd in socket status */
							socket->status = sd_cmd.type;
						} else {
							SEGGER_RTT_printf(0, "ERROR: No response from modem state machine![%s:%u]\r\n", __FUNCTION__,__LINE__);
							success = false;
						}
					} else {
						SEGGER_RTT_printf(0, "ERROR: Failed to send socket close command to state machine![%s:%u]\r\n", __FUNCTION__,__LINE__);
						success = false;
					}
				} else {
					SEGGER_RTT_printf(0, "ERROR: Socket handle is invalid! [%s]\r\n", __FUNCTION__);
					success = false;
				}

				/* Return socket handle mutex */
				xSemaphoreGive(socketHandlesMutex);
			}
		}
	}
	return success;
}

//static QueueHandle_t rspQueue = NULL;
static StaticQueue_t packetResponceStaticQueue;

bool modem_sendPacket(int sd, packet_t * pkt, TickType_t xTicksToWait) {
	bool success = false;
	modem_packet_command_t packet_command;

	UNUSED_VARIABLE(sd);

	QueueHandle_t rspQueue = NULL;
	uint8_t rspQueueBuffer[ 1 * sizeof(modem_packet_command_t) ];

	if ((packetQueue != NULL) && (queueEventGroup != NULL)) {
		/* Create the static queue once */
		rspQueue = xQueueCreateStatic(1, sizeof(modem_packet_command_t), rspQueueBuffer, &packetResponceStaticQueue);

		if (rspQueue == NULL) {
			SEGGER_RTT_printf(0, "ERROR: Failed to create queue![%s:%u]\r\n", __FUNCTION__,__LINE__);
			success = false;
		} else {

			/* Validate the socket handle */
			if (socket_checkHandle(sd)) {
				memcpy(&packet_command.packet,pkt,sizeof(packet_t));
				packet_command.type = PACKET_SEND;
				packet_command.responseQueue = rspQueue;
				if (xQueueSend(packetQueue, &packet_command, pdMS_TO_TICKS(100)) == pdTRUE) {
					xEventGroupSetBits(queueEventGroup, PACKET_COMMAND_EVENT_GROUP_BIT);
					if (xQueueReceive(rspQueue, &packet_command, xTicksToWait) == pdTRUE) {
						if (packet_command.type == PACKET_SEND_SUCCESS) {
							success = true;
							/* Packet sent successfully. */
						} else {
							SEGGER_RTT_printf(0, "ERROR: Packet failed to send![%s:%u]\r\n", __FUNCTION__,__LINE__);
							success = false;
						}
					} else {
						SEGGER_RTT_printf(0, "ERROR: No response from modem state machine![%s:%u]\r\n", __FUNCTION__,__LINE__);
						success = false;
					}
				} else {
					SEGGER_RTT_printf(0, "ERROR: Failed to send packet send command to state machine![%s:%u]\r\n", __FUNCTION__,__LINE__);
					success = false;
				}
			} else {
				SEGGER_RTT_printf(0, "ERROR: Requested socket handle [%d] is invalid! [%s]\r\n", sd, __FUNCTION__);
				success = false;
			}
		}
	}
	return success;
}


bool modem_receivePacket(int sd, packet_t * pkt, TickType_t xTicksToWait) {
	bool success = false;
	static modem_packet_command_t packet_command;

	UNUSED_VARIABLE(sd);

	QueueHandle_t rspQueue = NULL;
	uint8_t rspQueueBuffer[ 1 * sizeof(modem_packet_command_t) ];

	if ((packetQueue != NULL) && (queueEventGroup != NULL)) {
		/* Create the static queue once */
		rspQueue = xQueueCreateStatic(1, sizeof(modem_packet_command_t), rspQueueBuffer, &packetResponceStaticQueue);

		if (rspQueue == NULL) {
			SEGGER_RTT_printf(0, "ERROR: Failed to create queue![%s:%u]\r\n", __FUNCTION__,__LINE__);
			success = false;
		} else {

			/* Validate the socket handle */
			if (socket_checkHandle(sd)) {
				packet_command.type = PACKET_RECEIVE;
				packet_command.responseQueue = rspQueue;

				if (xQueueSend(packetQueue, &packet_command, pdMS_TO_TICKS(100)) == pdTRUE) {
					xEventGroupSetBits(queueEventGroup, PACKET_COMMAND_EVENT_GROUP_BIT);
					if (xQueueReceive(rspQueue, &packet_command, xTicksToWait) == pdTRUE) {
						if (packet_command.type == PACKET_RECEIVE_SUCCESS) {
							success = true;
							/* fill receive packet */
							memcpy(pkt,&packet_command.packet,sizeof(packet_t));
						} else {
							errorCount_incrementCount(ERROR_NO_RESPONSE_FROM_SERVER);
							SEGGER_RTT_printf(0, "ERROR: Packet failed to be received![%s:%u]\r\n", __FUNCTION__,__LINE__);
							success = false;
						}
					} else {
						SEGGER_RTT_printf(0, "ERROR: No response from modem state machine![%s:%u]\r\n", __FUNCTION__,__LINE__);
						success = false;
					}
				} else {
					SEGGER_RTT_printf(0, "ERROR: Failed to enqueue packet receive command to state machine![%s:%u]\r\n", __FUNCTION__,__LINE__);
					success = false;
				}
			} else {
				SEGGER_RTT_printf(0, "ERROR: Requested socket handle [%d] is invalid! [%s]\r\n", sd, __FUNCTION__);
				success = false;
			}
		}
	}
	return success;
}

bool modem_queuePhyCommand(modem_phy_command_type_t command) {
	bool success = false;

	if ((phyQueue != NULL) && (queueEventGroup != NULL)) {
		if (xQueueSend(phyQueue, &command, pdMS_TO_TICKS(100)) == pdTRUE) {
			xEventGroupSetBits(queueEventGroup, PHY_QUEUE_EVENT_GROUP_BIT);
			success = true;
		}
	}

	return success;
}

//static QueueHandle_t rspQueue = NULL;
static StaticQueue_t getStateRspStaticQueue;

bool modem_getCurrentState (modem_state_t * state, TickType_t xTicksToWait) {
	bool success = false;
	modem_phy_command_t command;

	QueueHandle_t rspQueue = NULL;
	uint8_t rspQueueBuffer[ 1 * sizeof(modem_state_t) ];


	if ((phyQueue != NULL) && (queueEventGroup != NULL)) {

		/* Create the static queue once */
		rspQueue = xQueueCreateStatic(1, sizeof(modem_state_t), rspQueueBuffer, &getStateRspStaticQueue);

		if (rspQueue == NULL) {
			SEGGER_RTT_printf(0, "ERROR: Failed to create response queue![%s:%u]\r\n", __FUNCTION__,__LINE__);
			success = false;
		} else {
			command.type = MODEM_PHY_GET_STATE;
			command.responseQueue = rspQueue;
			if (xQueueSend(phyQueue, &command, pdMS_TO_TICKS(100)) == pdTRUE) {
				xEventGroupSetBits(queueEventGroup, PHY_QUEUE_EVENT_GROUP_BIT);
				if (xQueueReceive(rspQueue, state, xTicksToWait)) {
					success = true;
				} else {
					SEGGER_RTT_printf(0, "ERROR: No response from modem state machine![%s:%u]\r\n", __FUNCTION__,__LINE__);
					success = false;
				}
			} else {
				SEGGER_RTT_printf(0, "ERROR: Failed to send phy query command to state machine![%s:%u]\r\n", __FUNCTION__,__LINE__);
			}
		}
	}

	return success;
}

bool modem_acknowledgeLowBattery(void) {

	xEventGroupClearBits(modem_stateEventGroup,MODEM_STATE_LOW_BATTERY_BIT);
	return true;

}

//static QueueHandle_t rspQueue = NULL;
static StaticQueue_t rspStaticQueue;

bool modem_getSignalQuality(uint8_t * rsrp, uint8_t * rsrq) {
	bool success = false;
	modem_signal_data_t q_sig;
	modem_phy_command_t command;

	QueueHandle_t rspQueue = NULL;
	uint8_t rspQueueBuffer[ 1 * sizeof(modem_signal_data_t) ];


	if ((appPhyQueue != NULL) && (queueEventGroup != NULL)) {

		/* Create the static queue once */
		rspQueue = xQueueCreateStatic(1, sizeof(modem_signal_data_t), rspQueueBuffer, &rspStaticQueue);

		if (rspQueue == NULL) {
			SEGGER_RTT_printf(0, "ERROR: Failed to create queue![%s:%u]\r\n", __FUNCTION__,__LINE__);
			success = false;
		} else {
			command.type = MODEM_PHY_QUERY_RSSI;
			command.responseQueue = rspQueue;

			if (xQueueSend(appPhyQueue, &command, pdMS_TO_TICKS(100)) == pdTRUE) {
				xEventGroupSetBits(queueEventGroup, APP_PHY_QUEUE_EVENT_GROUP_BIT);

				if (xQueueReceive(rspQueue, &q_sig, pdMS_TO_TICKS(1000))) {
					*rsrp = q_sig.rsrp;
					*rsrq = q_sig.rsrq;
					success = true;
				} else {
					SEGGER_RTT_printf(0, "ERROR: No response from modem state machine![%s:%u]\r\n", __FUNCTION__,__LINE__);
					success = false;
				}
			} else {
				SEGGER_RTT_printf(0, "ERROR: Failed to send RSSI query command to state machine![%s:%u]\r\n", __FUNCTION__,__LINE__);
			}
		}
	}

	return success;
}

void modem_thread(void *arg) {
	EventBits_t bits = 0;
	/*
	 *  Initially, only receive messages from the modem UART and physical layer
	 *  queues.
	 */
	queueEventGroupMask = DEFAULT_MODEM_EVENT_GROUP_BITS; // comment out to initally disable the modem state machine 

	/*
	 * Local variables to hold the items received from the queues this task
	 * monitors
	 */
	modem_uart_response_t modem_uart_response;
	modem_phy_command_t modem_phy_command;
	modem_phy_command_t modem_app_phy_command;
	modem_socket_command_t modem_socket_command;
	modem_packet_command_t modem_packet_command;
	/*
	 * Local variables to hold the signal and signal parameter that are passed
	 * to the state machine.
	 */
	hsm_signal_t signal;
	sendATCmd_signal_param_t param;

	QueueHandle_t responseQueueHandle;
	modem_state_t state;

	/*
	 * Initialize the lines on the port expander which control the modem
	 * as well as the LDO and buck output voltage level.
	 */
	els31_init();

	/*
	 * Initialize the state machine
	 */
	simplehsm_initialize(&hsm, state_top);

	queueEventGroupMask |= PHY_QUEUE_EVENT_GROUP_BIT;

	SEGGER_RTT_printf(0, "Modem thread started\r\n");
	while (1) {

		if (!bits) {
			bits = xEventGroupWaitBits(queueEventGroup, queueEventGroupMask, pdFALSE, pdFALSE, portMAX_DELAY);
		}

		activeBit = 0;

		if (bits & RESPONSE_QUEUE_EVENT_GROUP_BIT) {
			if (xQueueReceive(responseQueue, &modem_uart_response, 0) == pdTRUE) {
				activeBit = RESPONSE_QUEUE_EVENT_GROUP_BIT;
			} else {
				/*
				 * If the queue is empty, clear the corresponding bit in our
				 * local copy of the event flags.  Then, clear the bit in the
				 * event group itself.  The xEventGroupClearBits() function
				 * will return the state of the event group before the clear
				 * operation is executed.
				 */
				bits &= ~RESPONSE_QUEUE_EVENT_GROUP_BIT;
				bits |= xEventGroupClearBits(queueEventGroup, RESPONSE_QUEUE_EVENT_GROUP_BIT);
			}
		} else if (bits & PHY_QUEUE_EVENT_GROUP_BIT) {
			if (xQueueReceive(phyQueue, &modem_phy_command, 0) == pdTRUE) {
				activeBit = PHY_QUEUE_EVENT_GROUP_BIT;
			} else {
				bits &= ~PHY_QUEUE_EVENT_GROUP_BIT;
				bits |= xEventGroupClearBits(queueEventGroup, PHY_QUEUE_EVENT_GROUP_BIT);
			}
		} else if (bits & APP_PHY_QUEUE_EVENT_GROUP_BIT) {
			if (xQueueReceive(appPhyQueue, &modem_app_phy_command, 0) == pdTRUE) {
				activeBit = APP_PHY_QUEUE_EVENT_GROUP_BIT;
			} else {
				bits &= ~APP_PHY_QUEUE_EVENT_GROUP_BIT;
				bits |= xEventGroupClearBits(queueEventGroup, APP_PHY_QUEUE_EVENT_GROUP_BIT);
			}
		} else if (bits & SOCKET_QUEUE_EVENT_GROUP_BIT) {
			if (xQueueReceive(socketQueue, &modem_socket_command, 0) == pdTRUE) {
				activeBit = SOCKET_QUEUE_EVENT_GROUP_BIT;
			} else {
				bits &= ~SOCKET_QUEUE_EVENT_GROUP_BIT;
				bits |= xEventGroupClearBits(queueEventGroup, SOCKET_QUEUE_EVENT_GROUP_BIT);
			}
		} else if (bits & GENERAL_PURPOSE_TIMER_EVENT_GROUP_BIT) {
			/*
			 * The bit in the event group corresponding to the general purpose
			 * timer has been set.  This indicates that the general purpose
			 * timer has expired.  Because it is a timer, there is no queue to
			 * check.  After setting activeBit appropriate, (so that we know
			 * what signal to send to the HSM), we simply clear the bit in our
			 * local copy of the active bits and also clear the bit in the
			 * event group.
			 */
			activeBit = GENERAL_PURPOSE_TIMER_EVENT_GROUP_BIT;
			bits &= ~GENERAL_PURPOSE_TIMER_EVENT_GROUP_BIT;
			xEventGroupClearBits(queueEventGroup, GENERAL_PURPOSE_TIMER_EVENT_GROUP_BIT);
		} else if (bits & AT_COMMAND_TIMER_EVENT_GROUP_BIT) {
			activeBit = AT_COMMAND_TIMER_EVENT_GROUP_BIT;
			bits &= ~AT_COMMAND_TIMER_EVENT_GROUP_BIT;
			xEventGroupClearBits(queueEventGroup, AT_COMMAND_TIMER_EVENT_GROUP_BIT);
		} else if (bits & MODEM_OFF_COMMAND_EVENT_GROUP_BIT) {
			activeBit = MODEM_OFF_COMMAND_EVENT_GROUP_BIT;
			bits &= ~MODEM_OFF_COMMAND_EVENT_GROUP_BIT;
			xEventGroupClearBits(queueEventGroup, MODEM_OFF_COMMAND_EVENT_GROUP_BIT);
		} else if (bits & PACKET_COMMAND_EVENT_GROUP_BIT) {
			/*
			 * A packet command was placed into the queue.
			 */
			if (xQueueReceive(packetQueue, &modem_packet_command, 0) == pdTRUE) {
				activeBit = PACKET_COMMAND_EVENT_GROUP_BIT;
			} else {
				bits &= ~PACKET_COMMAND_EVENT_GROUP_BIT;
				bits |= xEventGroupClearBits(queueEventGroup, PACKET_COMMAND_EVENT_GROUP_BIT);
			}
		} else if (bits & LOWBATT_NOTIFY_EVENT_GROUP_BIT) {
			activeBit = LOWBATT_NOTIFY_EVENT_GROUP_BIT;
			bits &= ~LOWBATT_NOTIFY_EVENT_GROUP_BIT;
			xEventGroupClearBits(queueEventGroup, LOWBATT_NOTIFY_EVENT_GROUP_BIT);
		}

		/*
		 * Use the activeBit, as determined by the queueEventGroup above, to send
		 * the appropriate signal to the HSM.
		 */
		switch (activeBit) {
		case RESPONSE_QUEUE_EVENT_GROUP_BIT:
			if (processModemResponseLine(&modem_uart_response, &signal, &param)) {
				simplehsm_signal_current_state(&hsm, signal, &param);
			}
			break;

		case MODEM_OFF_COMMAND_EVENT_GROUP_BIT:
			simplehsm_signal_current_state(&hsm, SIG_TURN_MODEM_OFF, NULL);
			break;

		case PHY_QUEUE_EVENT_GROUP_BIT:
			if (modem_phy_command.type== MODEM_PHY_TURN_OFF) {
				simplehsm_signal_current_state(&hsm, SIG_TURN_MODEM_OFF, NULL);
			} else if (modem_phy_command.type == MODEM_PHY_TURN_ON) {
				simplehsm_signal_current_state(&hsm, SIG_TURN_MODEM_ON, NULL);
			} else if (modem_phy_command.type == MODEM_PHY_GET_STATE) {
				if (modem_phy_command.responseQueue) {
					if (simplehsm_is_in_state(&hsm, state_off)) {
						state = MODEM_STATE_OFF;
					} else if (simplehsm_is_in_state(&hsm, state_booting)) {
						state = MODEM_STATE_BOOTING;
					} else if (simplehsm_is_in_state(&hsm, state_online_idle)) {
						state = MODEM_STATE_ONLINE_IDLE;
					} else {
						state = MODEM_STATE_UNKOWN;
					}
					/* Send the caller the current modem state */
					if (xQueueSend(modem_phy_command.responseQueue, &state, pdMS_TO_TICKS(100)) == pdTRUE) {
						SEGGER_RTT_printf(0, "MODEM_PHY_GET_STATE response sent to caller.\r\n");
					} else {
						SEGGER_RTT_printf(0, "ERROR: Failed send MODEM_PHY_GET_STATE response to caller!\r\n");
					}
				} else {
					SEGGER_RTT_printf(0, "ERROR: MODEM_PHY_GET_STATE Response queue invalid. %s\r\n", __FUNCTION__);
				}
			} else if (modem_phy_command.type == MODEM_PHY_DISABLE_FSM) {
				queueEventGroupMask = PHY_QUEUE_EVENT_GROUP_BIT;
			} else if (modem_phy_command.type == MODEM_PHY_ENABLE_FSM) {
				queueEventGroupMask |= DEFAULT_MODEM_EVENT_GROUP_BITS;
			}
			break;

		case APP_PHY_QUEUE_EVENT_GROUP_BIT:
			if (modem_app_phy_command.type == MODEM_PHY_QUERY_RSSI) {
				responseQueueHandle = modem_app_phy_command.responseQueue;
				simplehsm_signal_current_state(&hsm, SIG_MEASURE_RSSI, &responseQueueHandle);
			}
			break;

		case SOCKET_QUEUE_EVENT_GROUP_BIT:
			if (modem_socket_command.type == SOCKET_OPEN) {
				simplehsm_signal_current_state(&hsm, SIG_OPEN_SOCKET, &modem_socket_command);
			} else if (modem_socket_command.type == SOCKET_CLOSE) {
				simplehsm_signal_current_state(&hsm, SIG_CLOSE_SOCKET, &modem_socket_command);
			}
			break;

		case GENERAL_PURPOSE_TIMER_EVENT_GROUP_BIT:
			simplehsm_signal_current_state(&hsm, SIG_TIMEOUT, NULL);
			break;

		case AT_COMMAND_TIMER_EVENT_GROUP_BIT:
			if (modem_atCommandTimerExpired(&signal, &param)) {
				simplehsm_signal_current_state(&hsm, signal, &param);
			}
			break;

		case PACKET_COMMAND_EVENT_GROUP_BIT:
			if (modem_packet_command.type == PACKET_SEND) {
				simplehsm_signal_current_state(&hsm, SIG_TX_PACKET, &modem_packet_command);
			} else if (modem_packet_command.type == PACKET_RECEIVE) {
				simplehsm_signal_current_state(&hsm, SIG_RX_PACKET, &modem_packet_command);
			}
			break;

		case LOWBATT_NOTIFY_EVENT_GROUP_BIT:
			SEGGER_RTT_printf(0, "WARN: Low battery reported![%s]\r\n", __FUNCTION__);
			/* turn off modem, either immediately or soon? */
			simplehsm_signal_current_state(&hsm, SIG_TURN_MODEM_OFF, NULL);
			break;
		case 0:
			/*
			 * We don't want to print the error below in the case that there is
			 * no active bit in the event group.
			 */
			break;

		default:
			SEGGER_RTT_printf(0, "ERROR: activeBit (%d) not recognized in %s\r\n", activeBit, __FUNCTION__);
			break;
		}
	}
}

stnext state_top(int signal, void *param) {
	bool powerIsOn = false;
	bool modemIsOn = false;

#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_INIT:
    	if (!(els31_queryPower(&powerIsOn) && els31_queryOn(&modemIsOn))) {
    		SEGGER_RTT_printf(0, "ERROR: Failed to get modem status\r\n");
    	}

    	if (powerIsOn && modemIsOn) {
    		modemHasBeenOn = true;
    		simplehsm_init_transition_state(&hsm, state_on);
    	} else {
    		if (powerIsOn) {
    	    	els31_disablePower();
    	    	SEGGER_RTT_printf(0, "Removed power from modem\r\n");
    		}
    		simplehsm_init_transition_state(&hsm, state_off);
    	}

    	return stnone;

    case SIG_EXIT:
    	return stnone;
    case SIG_TX_PACKET:
    	packetReady = true;
    	return stnone;
    case SIG_NULL:
    	return stnone;

    default:
    	SEGGER_RTT_printf(0, "Unhandled signal in modem state machine: %s\r\n", m_signalNames[signal]);

	}

	return stnone;
}


 stnext state_off(int signal, void *param) {
	 bool modemIsOn = false;
	 uint8_t batteryPercentage;
#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
		case SIG_ENTRY:
			return stnone;
		case SIG_INIT:
#if defined(MODEM_TESTING) && (MODEM_TESTING == 1)
			g_modemTestState = MODEM_TESTMODE_OFF;
#endif

			if (!els31uart_sleepUart(pdMS_TO_TICKS(1000))) {
				SEGGER_RTT_printf(0, "ERROR: Failed to sleep UART module![%s:%u]\r\n", __FUNCTION__, __LINE__);
				errorCount_incrementCount(ERROR_MODEM_HARDWARE_FAILURE);
			} else {
				/* unsubscribe to lowBattery Notifications */
				notifyUnsubscribe(lowBatteryNotifyHandle,modemlowBatteryNotification);

				queueEventGroupMask = PHY_QUEUE_EVENT_GROUP_BIT;
				xEventGroupSetBits(modem_stateEventGroup, MODEM_STATE_OFF_BIT);
			}
			socket_init();

#ifdef BOARD_CELL_NODE_REV1P1
			/* set the buck converter to power save */
			buck_setBuckmode(BUCKMODE_BURST);
#endif

#if defined(MODEM_ON_BY_DEFAULT) && (MODEM_ON_BY_DEFAULT == 1)
			if (!modemHasBeenOn) {
				simplehsm_signal_current_state(&hsm, SIG_TURN_MODEM_ON, NULL);
			}
#endif

			return stnone;
		case SIG_EXIT:
			return stnone;
		case SIG_TURN_MODEM_ON:

		if (!sensors_getRecentBatteryPercentage(&batteryPercentage) || (batteryPercentage <= MODEM_MIN_BATTERY_PERCENTAGE)) {
			LOGGER(0,"WARN: Battery percentage too low (%u) to boot modem. Setting Bit. %s\r\n",batteryPercentage, __FUNCTION__);
			errorCount_incrementCount(ERROR_VBAT_TOO_LOW_FOR_MODEM);
			xEventGroupSetBits(modem_stateEventGroup,MODEM_STATE_LOW_BATTERY_BIT);
			return stnone;
		} else {
			xEventGroupClearBits(modem_stateEventGroup,MODEM_STATE_LOW_BATTERY_BIT);

#ifdef BOARD_CELL_NODE_REV1P1
				/* set the buck converter to power mode */
				buck_setBuckmode(BUCKMODE_PULSE_SKIPPING);
#endif

				els31_enablePower();
				vTaskDelay(pdMS_TO_TICKS(100));

				if (!els31uart_wakeUart(pdMS_TO_TICKS(1000))) {
					SEGGER_RTT_printf(0, "ERROR: Failed to wake UART module![%s:%u]\r\n", __FUNCTION__,__LINE__);
				}

				els31_modemIgnition();
				vTaskDelay(pdMS_TO_TICKS(500));

				els31_queryOn(&modemIsOn);

				modemHasBeenOn = true;

				if (modemIsOn) {
					xEventGroupClearBits(modem_stateEventGroup, MODEM_STATE_OFF_BIT);
					/* enable default event bits */
					queueEventGroupMask |= DEFAULT_MODEM_EVENT_GROUP_BITS;

					/* Attempt to subscribe to lowBattery Notifications */
					if (!notifySubscribe(lowBatteryNotifyHandle,
							&modemlowBatteryNotification, modemlowBattNotifyInfo)) {
						SEGGER_RTT_printf(0, "ERROR: Failed to subscribe to Low Battery Notifications![%s]\r\n", __FUNCTION__);
					}

					simplehsm_transition_state(&hsm, state_booting);
				} else {
					SEGGER_RTT_printf(0, "ERROR: Failed to turn modem on, removing power\r\n");
					els31_disablePower();
					els31uart_sleepUart(pdMS_TO_TICKS(1000));
					errorCount_incrementCount(ERROR_MODEM_HARDWARE_FAILURE);
					/* No transition here so we remain in state_off */

#ifdef BOARD_CELL_NODE_REV1P1
					/* set the buck converter to power save */
					buck_setBuckmode(BUCKMODE_BURST);
#endif
				}

			}
			return stnone;
		}

	return state_top;
 }


 stnext state_on(int signal, void *param) {
	 sendATCmd_signal_param_t *p_atResp;
	 unsigned int regStatus;
	 urc_sbc_states_t sbcURC;

#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;

	case SIG_INIT:
#if defined(MODEM_TESTING) && (MODEM_TESTING == 1)
		simplehsm_init_transition_state(&hsm, state_testing);
#else
		simplehsm_init_transition_state(&hsm, state_checkResponsive);
#endif
    	return stnone;

    case SIG_EXIT:
    	return stnone;

    case SIG_URC_SHUTDOWN:
    	simplehsm_transition_state(&hsm, state_off);
    	return stnone;

    case SIG_URC_SBC:
    	sbcURC = ( (sendATCmd_signal_param_t *)param )->parsed.urc_sbc.urcCause;
    	errorCount_incrementCount(ERROR_VBAT_TOO_LOW_FOR_MODEM);
    	if ((sbcURC == OVERVOLTAGE_SHUTDOWN) || (sbcURC == UNDERVOLTAGE_SHUTDOWN)) {
    		simplehsm_transition_state(&hsm, state_off);
    	} else if ((sbcURC == OVERVOLTAGE_WARNING) || (sbcURC == UNDERVOLTAGE_WARNING)) {
    		simplehsm_transition_state(&hsm, state_turnoff);
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Unhandled ^SBC URC! (%u)\r\n",(unsigned int)sbcURC);
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
    	return stnone;

    case SIG_URC_CEREG:
    	regStatus = ( (sendATCmd_signal_param_t *)param )->parsed.urc_cereg.stat;
    	modem_printRegistrationStatus(regStatus);

    	/*
    	 * If the modem reports that it is not registered, and we are not
    	 * attempting to turn the modem off, jump back to the registering
    	 * state until it does register.  Otherwise, remain in whatever
    	 * substrate we are already in.
    	 */
    	if (!((regStatus == 1) || (regStatus == 5)) && !simplehsm_is_in_state(&hsm, state_turnoff)) {
    			simplehsm_transition_state(&hsm, state_registering);
    	}

    	return stnone;

    case SIG_ENTER_TESTING:
    	simplehsm_transition_state(&hsm, state_testing);
    	return stnone;

    case SIG_MODEM_COMM:
    	p_atResp = (sendATCmd_signal_param_t *)param;
    	if (strncmp(p_atResp->raw, "\r\n", 2) != 0) {
    		els31uart_prettyPrint(p_atResp->raw, p_atResp->rawLength, "UNHANDLED MODEM RESPONSE");
    		// Modem response went unhandled, increment counter
    		errorCount_incrementCount(ERROR_MODEM_UNHANDLED_RESPONSE);
    	}
    	return stnone;

    case SIG_TURN_MODEM_OFF:
    	simplehsm_transition_state(&hsm, state_turnoff);
    	return stnone;

	case SIG_AT_CMD_SMSO:
		p_atResp = (sendATCmd_signal_param_t *)param;
		if (p_atResp->result != SENDATCMD_RESULT_OK) {
			SEGGER_RTT_printf(0, "ERROR: Failed to send command to switch off modem\r\n");
		}
		errorCount_incrementCount(ERROR_MODEM_REQUIRING_OFF_CMD_FAILED_AT_ATSHDN);
		/*
		 * Remain in the current substrate waiting for the modem to send a
		 * ^SHUTDOWN URC, which will be handled above.
		 */
    	return stnone;
	}

	return state_top;
 }

stnext state_checkResponsive(int signal, void *param) {
#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_INIT:
    	SEGGER_RTT_printf(0, "Checking whether modem is responsive\r\n");
    	modem_sendATCmdString("AT\r", pdMS_TO_TICKS(1000), SIG_AT_CMD_AT);
    	return stnone;
    case SIG_EXIT:
    	return stnone;
    case SIG_AT_CMD_AT:
    	if (((sendATCmd_signal_param_t *)param)->result == SENDATCMD_RESULT_OK) {
    		simplehsm_transition_state(&hsm, state_generalConfig);
    	} else if (((sendATCmd_signal_param_t *)param)->result == SENDATCMD_RESULT_ERROR) {
    		/*
    		 * Even if the modem responded with "ERROR", it is still doing
    		 * something, so we do not yet attempt an emergency reset.
    		 */
    		simplehsm_transition_state(&hsm, state_generalConfig);
    	} else if (((sendATCmd_signal_param_t *)param)->result == SENDATCMD_RESULT_TIMEOUT)  {
    		/*
    		 * Modem did not respond, so we perform an emergency reset
    		 */
    		SEGGER_RTT_printf(0, "ERROR: Modem did not respond to simple AT\\r command\r\n");
    		errorCount_incrementCount(ERROR_MODEM_UNRESPONSIVE);
    		simplehsm_transition_state(&hsm, state_emergencyReset);
    	}
    	return stnone;
	}

	return state_on;
 }


stnext state_emergencyReset(int signal, void *param) {
#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_INIT:
    	SEGGER_RTT_printf(0, "Engaging modem's emergency reset procedure\r\n");
    	els31_emergencyReset();
    	errorCount_incrementCount(ERROR_MODEM_REQUIRING_RESET_CMD);
    	simplehsm_init_transition_state(&hsm, state_booting);
    	return stnone;
    case SIG_EXIT:
    	return stnone;
	}

	return state_on;
}

stnext state_booting(int signal, void *param) {
#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;

    case SIG_INIT:
#if defined(MODEM_TESTING) && (MODEM_TESTING == 1)
    	g_modemTestState = MODEM_TESTMODE_BOOTING;
#endif

		xEventGroupSetBits(modem_stateEventGroup,MODEM_STATE_BOOTING_BIT);
    	SEGGER_RTT_printf(0, "Waiting for modem to boot\r\n");
    	/*
    	 *  Ensure that we're not waiting for the modem to respond to a
    	 *  specific AT command.
    	 */
    	modem_cancelATCmd();
    	// Start 60 second timer
		xTimerChangePeriod(gpTimer, pdMS_TO_TICKS(60000), portMAX_DELAY);
		xTimerStart(gpTimer, portMAX_DELAY);
    	return stnone;

    case SIG_EXIT:
		xEventGroupClearBits(modem_stateEventGroup,MODEM_STATE_BOOTING_BIT);
    	return stnone;

    case SIG_URC_SYSSTART:
    	xTimerStop(gpTimer, portMAX_DELAY);
    	simplehsm_transition_state(&hsm, state_generalConfig);
    	return stnone;

    case SIG_TIMEOUT:
    	SEGGER_RTT_printf(0, "ERROR: Modem failed to boot\r\n");
    	// We have not seen the modem print "^SYSSTART" after 60 seconds
    	errorCount_incrementCount(ERROR_MODEM_FAILED_TO_BOOT);
    	simplehsm_transition_state(&hsm, state_checkResponsive);
    	return stnone;
	}

	return state_on;
}

stnext state_testing(int signal, void *param) {
#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;

    case SIG_INIT:
    	SEGGER_RTT_printf(0, "Modem has entered testing mode\r\n");
#if defined(MODEM_TESTING) && (MODEM_TESTING == 1)
    	g_modemTestState = MODEM_TESTMODE_ON;
#endif
    	return stnone;

    case SIG_EXIT_TESTING:
    	simplehsm_transition_state(&hsm, state_on);
    	return stnone;

    case SIG_URC_SHUTDOWN:
    case SIG_TURN_MODEM_OFF:
    	/* If the modem indicates that it is shutting down or if the modem
    	 * driver receives a request to turn the modem off, let our parent,
    	 * the "on state" handle it. */
    	return state_on;

    case SIG_EXIT:
    	SEGGER_RTT_printf(0, "Modem leaving testing mode\r\n");
    	return stnone;

    default:
    	/* Trap all other signals here */
    	return stnone;
	}

	return state_on;
}

stnext state_generalConfig(int signal, void *param) {
	 sendATCmd_signal_param_t *p_atResp;

#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_INIT:
    	xEventGroupSetBits(modem_stateEventGroup,MODEM_STATE_GENERAL_CONFIG);
    	//TODO: Enable AT command echo
    	SEGGER_RTT_printf(0, "Performing general modem configuration\r\n");
    	// AT+CFUN=1 ensures that the radio is on and that the SIM card is accessible
    	modem_sendATCmdString("AT+CFUN=1\r", pdMS_TO_TICKS(5000), SIG_AT_CMD_CFUN_EQUALS_1);
    	return stnone;
    case SIG_EXIT:
    	xEventGroupClearBits(modem_stateEventGroup,MODEM_STATE_GENERAL_CONFIG);
    	return stnone;

    case SIG_AT_CMD_CFUN_EQUALS_1:
    	p_atResp = (sendATCmd_signal_param_t *)param;
    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
    		// Enable fast shutdown pin
    		modem_sendATCmdString("AT^SCFG=\"GPIO/mode/FSR\",\"std\"\r", pdMS_TO_TICKS(2000), SIG_AT_CMD_SCFG_FSO);
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Failed to enable modem's fast switch off function\r\n");
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
    	return stnone;

    case SIG_AT_CMD_SCFG_FSO:
    	p_atResp = (sendATCmd_signal_param_t *)param;
    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
        	/*
        	 * Enable fast switch off functionality.  With fast switch off
        	 * enabled, the modem will not print OK or ERROR after the
        	 * AT^SMSO command.  Nor will the modem send an ^SHUTDOWN URC.
        	 */
        	modem_sendATCmdString("AT^SCFG=\"MEShutdown/Fso\",\"1\"\r", pdMS_TO_TICKS(2000), SIG_AT_CMD_SCFG_FSR);
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Failed to put modem into full functionality mode\r\n");
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
    	return stnone;

    case SIG_AT_CMD_SCFG_FSR:
    	p_atResp = (sendATCmd_signal_param_t *)param;
    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
        	modem_sendATCmdString("AT^SCFG=\"Tcp/WithURCs\",\"on\"\r", pdMS_TO_TICKS(2000), SIG_AT_CMD_SCFG_WITHURCS);
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Failed to enable modem's fast shutdown pin\r\n");
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
    	return stnone;

    case SIG_AT_CMD_SCFG_WITHURCS:
    	p_atResp = (sendATCmd_signal_param_t *)param;
		if (p_atResp->result == SENDATCMD_RESULT_OK) {
#if defined(MODEM_TESTING) && (MODEM_TESTING == 1)
			simplehsm_transition_state(&hsm, state_testing);
#else
			simplehsm_transition_state(&hsm, state_registering);
#endif
		} else {
			SEGGER_RTT_printf(0, "ERROR: Failed enable TCP URCs mode\r\n");
			simplehsm_transition_state(&hsm, state_checkResponsive);
		}
		return stnone;
	}

	return state_on;
 }

 stnext state_registering(int signal, void *param) {
	 sendATCmd_signal_param_t *p_atResp;
	 unsigned int regStatus;

#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;

    case SIG_INIT:
    	xEventGroupSetBits(modem_stateEventGroup,MODEM_STATE_REGISTERING);
    	// Upon entering this state, enable +CEREG URC's
    	modem_sendATCmdString("AT+CEREG=1\r", pdMS_TO_TICKS(5000), SIG_AT_CMD_CEREG_EQUALS_1);
    	return stnone;

    case SIG_EXIT:
    	xEventGroupClearBits(modem_stateEventGroup,MODEM_STATE_REGISTERING);
    	return stnone;

    case SIG_AT_CMD_CEREG_EQUALS_1:
    	p_atResp = (sendATCmd_signal_param_t *)param;
    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
    		modem_sendATCmdString("AT+CEREG?\r", pdMS_TO_TICKS(3000), SIG_AT_CMD_CEREG_QUERY);
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Failed to enable +CEREG URC\r\n");
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
    	return stnone;

    case SIG_AT_CMD_CEREG_QUERY:
    	p_atResp = (sendATCmd_signal_param_t *)param;
    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
    		if (sscanf(substr(p_atResp->raw, "+CEREG:"), "%*[^,],%u", &regStatus) == 1) {
    			modem_printRegistrationStatus(regStatus);
    			if ((regStatus == 1) || (regStatus == 5)) {
    				simplehsm_transition_state(&hsm, state_activating_pdp);
    			}
    		} else {
    			SEGGER_RTT_printf(0, "ERROR: Failed to parse result of AT+CEREG? command\r\n");
    		}
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Failed to execute AT+CEREG? command\r\n");
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
    	return stnone;

    case SIG_URC_CEREG:
    	regStatus = ( (sendATCmd_signal_param_t *)param )->parsed.urc_cereg.stat;

    	modem_printRegistrationStatus(regStatus);

		if ((regStatus == 1) || (regStatus == 5)) {
			/* incase we are waiting for at+cereg? command */
			modem_cancelATCmd();
			simplehsm_transition_state(&hsm, state_activating_pdp);
		}
    	return stnone;
	}

	return state_on;
 }


 stnext state_activating_pdp(int signal, void *param) {
	 sendATCmd_signal_param_t *p_atResp;
	 unsigned int pdpContexStatus;
	 unsigned int ipAddrOctets[4];

#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_INIT:
    	xEventGroupSetBits(modem_stateEventGroup,MODEM_STATE_ACTIVATING_PDP);
    	// Attempt to activate PDP context 3 (VZWINTERNET)
    	modem_sendATCmdString("AT^SICA=1,3\r", pdMS_TO_TICKS(1000), SIG_AT_CMD_SICA_ACTIVATE);
    	return stnone;
    case SIG_EXIT:
    	xEventGroupClearBits(modem_stateEventGroup,MODEM_STATE_ACTIVATING_PDP);
    	return stnone;

    case SIG_AT_CMD_SICA_ACTIVATE:
    	p_atResp = (sendATCmd_signal_param_t *)param;
    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
    		// Query whether the PDP context is actually active
    		modem_sendATCmdString("AT^SICA?\r", pdMS_TO_TICKS(1000), SIG_AT_CMD_SICA_QUERY);
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Failed to execute AT+SICA=1,3 command\r\n");
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
    	return stnone;

    case SIG_AT_CMD_SICA_QUERY:
    	p_atResp = (sendATCmd_signal_param_t *)param;
    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
    		if (sscanf(substr(p_atResp->raw, "^SICA: 3,"), "%*[^,],%u", &pdpContexStatus) == 1) {
    			if (pdpContexStatus == 1) {
    				// Set address of primary DNS server
    				modem_sendATCmdString("AT^SICS=3,\"DNS1\",\"8.8.8.8\"\r", pdMS_TO_TICKS(1000), SIG_AT_CMD_SICS_SET_DNS1);
    			} else {
					/* Verify that the admin pdp context is not currently active, indicating 
					 * that the modem is provisioning on the network */
					 if (sscanf(substr(p_atResp->raw, "^SICA: 2,"), "%*[^,],%u", &pdpContexStatus) == 1) {
						if (pdpContexStatus == 1) {
							/* The admin context is active, set an event bit to alert the 
							 * the application code to indicate that the modem should be left on. */
							SEGGER_RTT_printf(0, "WARN: Admin PDP context active.\r\n");
							xEventGroupSetBits(modem_stateEventGroup,MODEM_STATE_ADMIN_ACTIVE_BIT);
						} else {
							// Clear the admin flag.
							xEventGroupClearBits(modem_stateEventGroup,MODEM_STATE_ADMIN_ACTIVE_BIT);
						}
					} else {
						SEGGER_RTT_printf(0, "ERROR: Failed to parse result of AT^SICA? command\r\n");
					}

					SEGGER_RTT_printf(0, "ERROR: Failed to activate PDP context\r\n");
					// Attempt to activate the PDP context again
					modem_sendATCmdString("AT^SICA=1,3\r", pdMS_TO_TICKS(1000), SIG_AT_CMD_SICA_ACTIVATE);
    			}
    		} else {
    			SEGGER_RTT_printf(0, "ERROR: Failed to parse result of AT^SICA? command\r\n");
    		}
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Failed to execute AT^SICA? command\r\n");
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
    	return stnone;

    case SIG_AT_CMD_SICS_SET_DNS1:
    	p_atResp = (sendATCmd_signal_param_t *)param;
    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
    		// Set address of secondary DNS server
    		modem_sendATCmdString("AT^SICS=3,\"DNS2\",\"18.70.0.160\"\r", pdMS_TO_TICKS(1000), SIG_AT_CMD_SICS_SET_DNS2);
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Failed to set modem's primary DNS server\r\n");
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
    	return stnone;

    case SIG_AT_CMD_SICS_SET_DNS2:
    	p_atResp = (sendATCmd_signal_param_t *)param;
    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
    		// Query modem's local IP address
    		modem_sendATCmdString("AT+CGPADDR\r", pdMS_TO_TICKS(1000), SIG_AT_CMD_CGPADDR);
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Failed to set modem's secondary DNS server\r\n");
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
    	return stnone;

    case SIG_AT_CMD_CGPADDR:
    	p_atResp = (sendATCmd_signal_param_t *)param;
    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
    		if (sscanf(substr(p_atResp->raw, "+CGPADDR: 3,\""), "%*[^\"]\"%u.%u.%u.%u",
    				&ipAddrOctets[0], &ipAddrOctets[1], &ipAddrOctets[2], &ipAddrOctets[3]) == 4) {
    			SEGGER_RTT_printf(0, "Modem's local IP address: %u.%u.%u.%u\r\n",
    					ipAddrOctets[0], ipAddrOctets[1], ipAddrOctets[2], ipAddrOctets[3]);
    			simplehsm_transition_state(&hsm, state_online);
    		} else {
    			SEGGER_RTT_printf(0, "ERROR: Failed to parse result of AT+CGPADDR command\r\n");
    		}
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Failed to query modem's local IP address\r\n");
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
    	return stnone;

	}

	return state_on;
 }


 stnext state_online(int signal, void *param) {
	 sendATCmd_signal_param_t *p_atResp;
	 modem_signal_data_t q_sig;
	 unsigned int rsrp, rsrq;
#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;

    case SIG_INIT:
    	simplehsm_init_transition_state(&hsm, state_online_idle);
    	return stnone;

    case SIG_EXIT:
    	return stnone;

    case SIG_TX_PACKET:
    	simplehsm_transition_state(&hsm, state_txing);
    	simplehsm_signal_current_state(&hsm,signal,param);
    	return stnone;
    case SIG_RX_PACKET:
    	simplehsm_transition_state(&hsm, state_rxing);
    	simplehsm_signal_current_state(&hsm,signal,param);
    	return stnone;
    case SIG_OPEN_SOCKET:
    	simplehsm_transition_state(&hsm, state_opensocket);
    	simplehsm_signal_current_state(&hsm,signal,param);
    	return stnone;
	case SIG_CLOSE_SOCKET:
    	simplehsm_transition_state(&hsm, state_closesocket);
    	simplehsm_signal_current_state(&hsm,SIG_CLOSE_SOCKET,param);
    	return stnone;

	case SIG_URC_TCP_REMOTE_CLOSE:
		log_warn("Remote end closed socket");
		errorCount_incrementCount(ERROR_SERVER_CLOSED_TCP_CONNECTION);
		// FIXME - geech - do we want to transition to a different state here?
		// i think no matter which way we go we're gonna end up in a checkResponsive call
		// and a modem reconfig, etc. etc.
		return stnone;
		
    case SIG_MEASURE_RSSI:
     	modem_sendATCmdStringAux("AT+CESQ\r", NULL, 0, pdMS_TO_TICKS(1000), SIG_AT_CMD_CESQ, param);
     	return stnone;
    case SIG_AT_CMD_CESQ:
    	p_atResp = (sendATCmd_signal_param_t *)param;
    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
    	    if (sscanf(substr(p_atResp->raw, "+CESQ:"), "%*[^:]: %*u,%*u,%*u,%*u,%u,%u", &rsrq, &rsrp) == 2) {
				q_sig.rsrq = (uint8_t)rsrq;
				q_sig.rsrp = (uint8_t)rsrp;
    	    	if ((p_atResp->extraParam != NULL) && xQueueSend(*(QueueHandle_t *)p_atResp->extraParam, &q_sig, 0)) {
    	    		;
    	    	} else {
    	    		SEGGER_RTT_printf(0, "ERROR: Failed to send Signal Quality message to resposne queue![%s:%u]\r\n", __FUNCTION__,__LINE__);
    	    	}
			} else {
				SEGGER_RTT_printf(0, "ERROR: Failed to parse result of AT+CESQ command\r\n");
			}
		} else {
			SEGGER_RTT_printf(0, "ERROR: Failed to execute AT+CESQ command\r\n");
			simplehsm_transition_state(&hsm, state_checkResponsive);
		}
    	return stnone;

	}

	return state_on;
 }


stnext state_online_idle(int signal, void *param) {
#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_INIT:
		xEventGroupSetBits(modem_stateEventGroup,MODEM_STATE_ONLINE_IDLE_BIT);
    	xSemaphoreTake(queueEventGroupMutex, pdMS_TO_TICKS(100));
		/* enable application mask bits */
		queueEventGroupMask |= APPLICATION_EVENT_GROUP_BITS;
		xSemaphoreGive(queueEventGroupMutex);
    	return stnone;
    case SIG_EXIT:
		xEventGroupClearBits(modem_stateEventGroup,MODEM_STATE_ONLINE_IDLE_BIT);
    	xSemaphoreTake(queueEventGroupMutex, pdMS_TO_TICKS(100));
		/* disable the active event bit in the mask */
    	queueEventGroupMask &= ~(APPLICATION_EVENT_GROUP_BITS);
    	xSemaphoreGive(queueEventGroupMutex);
    	return stnone;
	}

	return state_online;
}


stnext state_opensocket(int signal, void *param) {
	sendATCmd_signal_param_t *p_atResp;
	static modem_socket_command_t sd_command;
	unsigned int serviceProfileId, urcCauseId;

#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_INIT:
    	return stnone;
    case SIG_EXIT:
    	return stnone;
    case SIG_OPEN_SOCKET:
    	memcpy(&sd_command,(modem_socket_command_t *)param, sizeof(modem_socket_command_t));

    	//TODO: Check if the service profile is open, error out if fails

    	if (sd_command.type == SOCKET_OPEN) {
    		// Set service profile 0's type to socket
    		modem_sendATCmdString("AT^SISS=0,\"srvtype\",\"socket\"\r", pdMS_TO_TICKS(1000), SIG_AT_CMD_SISS_SRVTYPE);
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Failed enable TCP URCs mode\r\n");
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
    	return stnone;

    case SIG_AT_CMD_SISS_SRVTYPE:
    	p_atResp = (sendATCmd_signal_param_t *)param;
    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
    		// Associate service profile 0 with PDP context 3
    		modem_sendATCmdString("AT^SISS=0,\"conid\",3\r", pdMS_TO_TICKS(1000), SIG_AT_CMD_SISS_CONID);
    	} else if (p_atResp->result == SENDATCMD_RESULT_ERROR) {
    		simplehsm_signal_current_state(&hsm, SIG_SOCKET_ERROR, param);
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Failed to set service's type to socket\r\n");
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
    	return stnone;

    case SIG_AT_CMD_SISS_CONID:
    	p_atResp = (sendATCmd_signal_param_t *)param;
    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
    		// Assign an address to the service profile
    		char addressString[60];
    		snprintf(addressString, sizeof(addressString) - 1, "AT^SISS=0,\"address\",\"socktcps://%s:%u\"\r", sd_command.socket.addr, sd_command.socket.port);

    		modem_sendATCmdString(addressString, pdMS_TO_TICKS(1000), SIG_AT_CMD_SISS_ADDRESS);
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Failed to set service's connection ID\r\n");
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
    	return stnone;

    case SIG_AT_CMD_SISS_ADDRESS:
    	p_atResp = (sendATCmd_signal_param_t *)param;
    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
    		// set security option: tell the modem not to check certificates.
    		modem_sendATCmdString("AT^SISS=0,\"secopt\",0\r", pdMS_TO_TICKS(1000), SIG_AT_CMD_SISS_SECOPT);
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Failed to set service address\r\n");
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
    	return stnone;

	case SIG_AT_CMD_SISS_SECOPT:
		p_atResp = (sendATCmd_signal_param_t *)param;
		if (p_atResp->result == SENDATCMD_RESULT_OK) {
    		// Open the service profile
    		modem_sendATCmdString("AT^SISO=0\r", pdMS_TO_TICKS(1000), SIG_AT_CMD_SISO);
		} else {
			log_error("Failed to set security option");
			simplehsm_transition_state(&hsm, state_checkResponsive);
		}
		return stnone;

	case SIG_AT_CMD_SISO:
    	p_atResp = (sendATCmd_signal_param_t *)param;
    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
    		/*
    		 * After issuing the AT^SISO to open the connection, the modem
    		 * should reply with a ^SISW URC once it is ready to accept
    		 * data.  We start a timer and then wait for the URC.
    		 */
    		xTimerChangePeriod(gpTimer, pdMS_TO_TICKS(5000), portMAX_DELAY);
    		xTimerStart(gpTimer, portMAX_DELAY);
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Failed to open service profile\r\n");
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
    	return stnone;

	case SIG_URC_SISW:
		urcCauseId = ( (sendATCmd_signal_param_t *)param )->parsed.urc_sisw.urcCauseId;

		if (urcCauseId == 1) {
			xTimerStop(gpTimer, portMAX_DELAY);
			/* Respond to the caller that the socket was successfully opened using the
			 * passed response queue */
			if (sd_command.responseQueue) {
				/* Change to OPENED type to alert caller */
				sd_command.type = SOCKET_OPENED;
				sd_command.socket.open = true;
				sd_command.socket.sd = 0;		//TODO: save the opened id not the hard coded value.
				/* Send the caller the update socket command */
				if (xQueueSend(sd_command.responseQueue, &sd_command, pdMS_TO_TICKS(100)) == pdTRUE) {
					SEGGER_RTT_printf(0, "Socket opened response sent to caller.\r\n");
				} else {
					SEGGER_RTT_printf(0, "ERROR: Failed to queue socket open success response to caller![%s]\r\n",__FUNCTION__);
				}
			} else {
				SEGGER_RTT_printf(0, "ERROR: Socket open response queue is invalid![%s]\r\n",__FUNCTION__);
			}
			simplehsm_transition_state(&hsm, state_online);
		} else {
			SEGGER_RTT_printf(0, "ERROR: Unexpected urcCauseId (%u) in ^SISW: %u,%u URC\r\n", &serviceProfileId, &urcCauseId);
		}

		return stnone;

	case SIG_SOCKET_ERROR:
		SEGGER_RTT_printf(0, "ERROR: Modem returned ERROR\\r\\n while attempting to opening service profile!\r\n");
		if (sd_command.responseQueue) {
			/* Change to OPENED type to alert caller */
			sd_command.type = SOCKET_FAILURE;
			sd_command.socket.open = false;
			/* Send the caller the update socket command */
			if (xQueueSend(sd_command.responseQueue, &sd_command, pdMS_TO_TICKS(100)) == pdTRUE) {
				SEGGER_RTT_printf(0, "Socket failure response sent to caller.[%s]\r\n",__FUNCTION__);
			} else {
				SEGGER_RTT_printf(0, "ERROR: Failed to queue socket failure response to caller![%s]\r\n",__FUNCTION__);
			}
		} else {
			SEGGER_RTT_printf(0, "ERROR: Socket open failure response queue is invalid![%s]\r\n",__FUNCTION__);
		}
		simplehsm_transition_state(&hsm, state_online);
		break;
	case SIG_TIMEOUT:
		SEGGER_RTT_printf(0, "ERROR: Timeout while opening service profile[%s]\r\n",__FUNCTION__);
		simplehsm_transition_state(&hsm, state_closesocket);
		return stnone;

	}

	return state_online;
 }


 /**
  * Populate a buffer of data to transmit to the server via TCP from the specified modem command.
  * Packets are transmitted via TCP by first sending two bytes to indicate the length of the packet data,
  * then the packet data itself.
  * 
  * @return the length of data written to the buffer.
  */
uint16_t  modem_populateTCPSendBufferFromModemCommand(uint8_t * sendBuffer, 
			uint16_t bufferLength, 
			modem_packet_command_t * modemCommand) {

	if (modemCommand == NULL || sendBuffer == NULL) {
		log_error("Invalid buffer argument");
		return -1;
	}
	if (modemCommand->packet.dataLength+2 > bufferLength) {
		log_errorf("TCP send buffer too small: %u bytes needed, %u provided", modemCommand->packet.dataLength+2, bufferLength);
		return -1;
	}

	uint16_t sendLength = 0;

	// first two bytes indicate the amount of data (MSB first)
	sendBuffer[sendLength] = (modemCommand->packet.dataLength & 0xFF00) >> 8;
	sendLength++;
	sendBuffer[sendLength] = (modemCommand->packet.dataLength & 0x00FF);
	sendLength++;

	// then the data itself
	memcpy(sendBuffer+sendLength, modemCommand->packet.data, modemCommand->packet.dataLength);
	sendLength += modemCommand->packet.dataLength;

	return sendLength;
}


stnext state_txing(int signal, void *param) {
	sendATCmd_signal_param_t *p_atResp;
	static modem_packet_command_t tx_packet_command;
	#define sendBufferLength PACKET_MAX_LENGTH + CRC_LENGTH + 2
	static uint8_t sendBuffer[sendBufferLength];
	static char siswCmdStr[16];

#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;

    case SIG_INIT:
    	return stnone;

	case SIG_TX_PACKET:
	    /* Copy packet to local variable */
		memcpy(&tx_packet_command,((modem_packet_command_t *) param), sizeof(modem_packet_command_t));		

		// TCP packets have 2 bytes to specify the length of data being sent, then the data itself.
		// Prepare the buffer of data we'll send that includes both...
		uint16_t sendLength = modem_populateTCPSendBufferFromModemCommand(sendBuffer, sendBufferLength, &tx_packet_command);
		if (sendLength <= 0) {
			simplehsm_transition_state(&hsm, state_checkResponsive);
		} else {
			/* Send AT^SISW command indicating how many bytes we wish to send */
			snprintf(siswCmdStr, sizeof(siswCmdStr), "AT^SISW=0,%u\r", sendLength);
			if (!modem_sendATCmdStringAux(siswCmdStr, sendBuffer, sendLength, pdMS_TO_TICKS(5000), SIG_AT_CMD_SISW_COMPLETE, NULL)) {
				log_errorf("Failed to send %u bytes to modem with socket write command (^SISW)", sendLength);
				simplehsm_transition_state(&hsm, state_checkResponsive);
			}
		}
    	return stnone;

	case SIG_EXIT:
    	return stnone;

    case SIG_AT_CMD_SISW_COMPLETE:
    	p_atResp = (sendATCmd_signal_param_t *)param;

    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
    		SEGGER_RTT_printf(0, "Packet sent\r\n");

    		/* Respond to packet caller to inform them of success. */
    		if (tx_packet_command.responseQueue) {
				/* Change to OPENED type to alert caller */
    			tx_packet_command.type = PACKET_SEND_SUCCESS;
				/* Send the caller the update socket command */
				if (xQueueSend(tx_packet_command.responseQueue, &tx_packet_command, pdMS_TO_TICKS(100)) == pdTRUE) {
					SEGGER_RTT_printf(0, "PACKET_SEND_SUCCESS response sent to caller.\r\n");
				} else {
					SEGGER_RTT_printf(0, "ERROR: Failed send PACKET_SEND_SUCCESS response to caller!\r\n");
				}
			} else {
				SEGGER_RTT_printf(0, "ERROR: Packet send response queue is invalid!\r\n");
			}

    		simplehsm_transition_state(&hsm, state_online);
    	} else if (p_atResp->result == SENDATCMD_RESULT_ERROR) {
    		if (tx_packet_command.responseQueue) {
				/* Change to OPENED type to alert caller */
				tx_packet_command.type = PACKET_SEND_FAILURE;
				/* Send the caller the update socket command */
				if (xQueueSend(tx_packet_command.responseQueue, &tx_packet_command, pdMS_TO_TICKS(100)) == pdTRUE) {
					SEGGER_RTT_printf(0, "PACKET_SEND_FAILURE response sent to caller.\r\n");
				} else {
					SEGGER_RTT_printf(0, "ERROR: Failed send PACKET_SEND_FAILURE response to caller!\r\n");
				}
			} else {
				SEGGER_RTT_printf(0, "ERROR: Packet send response queue is invalid!\r\n");
			}
    		simplehsm_transition_state(&hsm, state_online);
		} else {
			/* SENDATCMD_RESULT_TIMEOUT */
    		SEGGER_RTT_printf(0, "ERROR: Failed to send packet to modem for transmission\r\n");
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
		return stnone;

	}

	return state_online;
 }


 /**
  * We've received a complete packet from Core.  Use the queue in rx_packet_command to
  * notify somebody.
  */
static void handlePacketReception(uint16_t packetLength, uint8_t * packetBuffer, modem_packet_command_t * rx_packet_command) {
	/* Return packet to caller via response queue */
	if (rx_packet_command->responseQueue != NULL) {
		/* Change type to alert caller and copy packet details */
		rx_packet_command->type = PACKET_RECEIVE_SUCCESS;
		if (packetLength > sizeof(((packet_t *)(0))->data)) {
			log_errorf("Packet to large for packet_t: %u bytes", packetLength);
			/* Only copy the acceptable amount of data. */
			packetLength = sizeof(((packet_t *)(0))->data);
			rx_packet_command->type = PACKET_RECEIVE_FAILURE;
		}
		memcpy(rx_packet_command->packet.data, packetBuffer, packetLength);
		rx_packet_command->packet.dataLength = packetLength;
		rx_packet_command->packet.created = xTaskGetTickCount();
		/* Send the caller the update socket command */
		if (xQueueSend(rx_packet_command->responseQueue, rx_packet_command, pdMS_TO_TICKS(100)) == pdTRUE) {
			log_debug("Packet receive success response sent to caller.");
		} else {
			log_error("Failed to queue packet send success response to caller!");
		}
	} else {
		log_error("Packet send response queue is invalid!");
	}
}

/**
 * See if we've received an entire packet from Core.  First two bytes of the receive buffer indicate
 * the length of the Core packet.  If we've received an complete packet, the packet's length 
 * is copied to *outPacketLength.
 * 
 * @return pdTRUE is a complete packet is received, pdFALSE if more data is required.
 */
static bool checkForCompletePacket(uint8_t * receiveBuffer, uint16_t receivedLength, uint16_t * outPacketLength) {
	if (receivedLength < 2) {
		// we need the first two bytes to determine the length of the packet.
		return pdFALSE;
	}

	// first two bytes are packet length, MSB first.
	uint16_t packetLength = receiveBuffer[0] << 8;
	packetLength |= receiveBuffer[1];

	uint16_t packetBytesReceived = receivedLength - 2;
	if (packetBytesReceived < packetLength) {
		log_debugf("Still waiting for %u bytes from Core", packetLength - packetBytesReceived);
		return pdFALSE;
	} 
	// if we've received too many bytes, that's bad.
	// FIXME - geech - 9/12/17 - this works right now because we never send data to Core w/o
	// first completely reading any previous response, but ultimately this should change so 
	// the next packet's data won't confuse it.
	if (packetBytesReceived > packetLength) {
		log_errorf("%u unexpected bytes from Core", packetBytesReceived - packetLength);
	}

	if (outPacketLength != NULL) {
		*outPacketLength = packetLength;
	}
	return pdTRUE;
}


stnext state_rxing(int signal, void *param) {
	sendATCmd_signal_param_t *p_atCmd;
	static modem_packet_command_t rx_packet_command;
	// a buffer to assembly data as it comes in from Core via TCP.
	// this is inefficient in terms of RAM, but we're under time duress right now.
	#define RECV_BUFFER_SIZE 1024
	static uint8_t receiveBuffer[RECV_BUFFER_SIZE];
	static uint16_t receivedLength;

#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;

	case SIG_INIT:
    	return stnone;

	case SIG_RX_PACKET:

		/* store packet command locally. */
		memcpy(&rx_packet_command,(modem_packet_command_t *) param, sizeof(modem_packet_command_t));

		// Attempt to read pending data.
		receivedLength = 0;
		modem_sendATCmdString("AT^SISR=0,256\r", pdMS_TO_TICKS(1000), SIG_AT_CMD_SISR);
    	return stnone;

    case SIG_EXIT:
    	return stnone;

    case SIG_AT_CMD_SISR:
    	p_atCmd = (sendATCmd_signal_param_t *)param;
    	if (p_atCmd->result == SENDATCMD_RESULT_OK) {
    		if (p_atCmd->parsed.cmd_sisr.cnfReadLength > 0) {
				int bytesRead = p_atCmd->rawLength;
				log_debugf("Received %u bytes of data from Core", bytesRead);
				if (bytesRead + receivedLength > RECV_BUFFER_SIZE) {
					log_errorf("Total received data of %u exceeds buffer size of %u", bytesRead + receivedLength, RECV_BUFFER_SIZE);
				} else {
					memcpy(receiveBuffer+receivedLength, p_atCmd->raw, bytesRead);
					receivedLength += bytesRead;
				}

				uint16_t packetLength = 0;
				bool packetComplete = checkForCompletePacket(receiveBuffer, receivedLength, &packetLength);
				// -1 indicates we don't have a complete packet, and still need more data from Core.
				if (packetComplete) {
					log_debugf("Packet received: %u bytes", packetLength);
					// notify whoever cares that we've received a complete packet
					handlePacketReception(packetLength, receiveBuffer+2, &rx_packet_command);
					// transition out of this state
					simplehsm_transition_state(&hsm, state_online);
				} else {
					// we're still expecting more data...try to read more
					modem_sendATCmdString("AT^SISR=0,256\r", pdMS_TO_TICKS(1000), SIG_AT_CMD_SISR);
				}
    		} else {
    			SEGGER_RTT_printf(0, "No packet pending\r\n");
    			// Start timer
    			xTimerChangePeriod(gpTimer, pdMS_TO_TICKS(5000), portMAX_DELAY);
    			xTimerStart(gpTimer, portMAX_DELAY);
    		}
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Failed to receive packet\r\n");
    		simplehsm_transition_state(&hsm, state_online);
    	}
    	return stnone;

    case SIG_URC_SISR:
    	xTimerStop(gpTimer, portMAX_DELAY);
    	modem_sendATCmdString("AT^SISR=0,256\r", pdMS_TO_TICKS(1000), SIG_AT_CMD_SISR);
    	return stnone;

    case SIG_TIMEOUT:
    	SEGGER_RTT_printf(0, "Timeout while waiting to recieve packet\r\n");
    	simplehsm_transition_state(&hsm, state_online);
    	return stnone;

	}

	return state_online;
 }

static stnext state_closesocket(int signal, void *param) {
	static modem_socket_command_t sd_command;
	sendATCmd_signal_param_t *p_atResp;

#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_INIT:
    	return stnone;
    case SIG_CLOSE_SOCKET:
    	memcpy(&sd_command,(modem_socket_command_t *)param, sizeof(modem_socket_command_t));
		if (sd_command.type == SOCKET_CLOSE) {
			// Set service profile 0's type to socket
			modem_sendATCmdString("AT^SISC=0\r", pdMS_TO_TICKS(1000), SIG_AT_CMD_SISC);
		} else {
			SEGGER_RTT_printf(0, "ERROR: Wrong command sent to state machine. [%u]\r\n",sd_command.type);
		}
    	return stnone;

    case SIG_EXIT:
    	return stnone;

    case SIG_AT_CMD_SISC:
    	p_atResp = (sendATCmd_signal_param_t *)param;
    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
        	SEGGER_RTT_printf(0, "Closed service profile\r\n");
        	/* Respond to the caller that the socket was successfully opened using the
			 * passed response queue */
			if (sd_command.responseQueue) {
				/* Change to OPENED type to alert caller */
				sd_command.type = SOCKET_CLOSED;
				sd_command.socket.open = false;
				/* Send the caller the update socket command */
				if (xQueueSend(sd_command.responseQueue, &sd_command, pdMS_TO_TICKS(100)) == pdTRUE) {
					SEGGER_RTT_printf(0, "Socket close response sent to caller.\r\n");
				} else {
					SEGGER_RTT_printf(0, "ERROR: Failed to queue socket close success response to caller!\r\n");
				}
			} else {
				SEGGER_RTT_printf(0, "ERROR: Socket close response queue is invalid!\r\n");
			}
    		simplehsm_transition_state(&hsm, state_online);
    	} else if (p_atResp->result == SENDATCMD_RESULT_ERROR) {
    	/* If an error is reported, its very likely that the service is invalid
    	 * and can be initialized freely, we report this to the caller. */
        	SEGGER_RTT_printf(0, "WARN: Service profile failed to close, likely invalid.\r\n");
        	/* Respond to the caller that the socket was successfully opened using the
			 * passed response queue */
			if (sd_command.responseQueue) {
				/* Change to OPENED type to alert caller */
				sd_command.type = SOCKET_CLOSE_ERROR;
				sd_command.socket.open = false;
				/* Send the caller the update socket command */
				if (xQueueSend(sd_command.responseQueue, &sd_command, pdMS_TO_TICKS(100)) == pdTRUE) {
					SEGGER_RTT_printf(0, "Socket close response sent to caller.\r\n");
				} else {
					SEGGER_RTT_printf(0, "ERROR: Failed to queue socket close warning response to caller!\r\n");
				}
			} else {
				SEGGER_RTT_printf(0, "ERROR: Socket close response queue is invalid!\r\n");
			}
    		simplehsm_transition_state(&hsm, state_online);
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: Failed to close service profile\r\n");
    		simplehsm_transition_state(&hsm, state_checkResponsive);
    	}
    	return stnone;
	}

	return state_online;
}

/* For now not used. */
//stnext state_dnsing(int signal, void *param) {
//#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
//	if (signal != SIG_NULL) {
//		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
//	}
//#endif
//
//	switch (signal) {
//	case SIG_ENTRY:
//		return stnone;
//    case SIG_INIT:
//    	return stnone;
//    case SIG_EXIT:
//    	return stnone;
//	}
//
//	return state_online;
//}

stnext state_forceShutdown(int signal, void *param) {
	bool modemIsOn;
	static unsigned int statusCheckCount = 0;
#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_INIT:
#ifdef BOARD_CELL_NODE_REV1P0
		SEGGER_RTT_printf(0, "Engaging modem's fast shutdown procedure\r\n");
    	els31_fastShutdown();
#endif
		SEGGER_RTT_printf(0, "Waiting for modem to completely power-down\r\n");
		statusCheckCount = 0;
		xTimerChangePeriod(gpTimer, pdMS_TO_TICKS(100), portMAX_DELAY);
		xTimerStart(gpTimer, portMAX_DELAY);
		return stnone;

    case SIG_EXIT:
    	return stnone;

    case SIG_TIMEOUT:
    	if (els31_queryOn(&modemIsOn)) {
    		if (!modemIsOn) {
    			if (els31_disablePower()) {
    				SEGGER_RTT_printf(0, "Removed power from modem\r\n");
    				simplehsm_transition_state(&hsm, state_off);
    			}
    		} else if (++statusCheckCount < 20) {
    			/*
    			 * If the modem is still powering-down wait another 100ms and
    			 * check its status again.
    			 */
    			SEGGER_RTT_printf(0, "Waiting for modem to completely power-down\r\n");
    			xTimerStart(gpTimer, portMAX_DELAY);
    		} else {
    			SEGGER_RTT_printf(0, "ERROR: Modem has not powered-down after 5 seconds; removing power\r\n");
    			if (els31_disablePower()) {
    				SEGGER_RTT_printf(0, "Removed power from modem\r\n");
    				simplehsm_transition_state(&hsm, state_off);
    			}
    		}
    	} else if (++statusCheckCount < 50) {
    		/*
    		 *  Failed to query whether modem is still on, but we have made
    		 *  less than 50 attempts doing so--try again.
    		 */
    		xTimerStart(gpTimer, portMAX_DELAY);
    	} else {
    		/* Repeated queries to check whether the modem is still on have
    		 * failed--remove power anyway.
    		 */
    		SEGGER_RTT_printf(0, "ERROR: Too many failures querying whether modem is still on; removing power\r\n");
			if (els31_disablePower()) {
				SEGGER_RTT_printf(0, "Removed power from modem\r\n");
				simplehsm_transition_state(&hsm, state_off);
			}
    	}
    	return stnone;
	}

	return state_on;
}

stnext state_turnoff(int signal, void *param) {
	sendATCmd_signal_param_t *p_atResp;
	bool modemIsOn;
	static unsigned int statusCheckCount = 0;

#if (defined(DEBUG_MODEM_HSM) && (DEBUG_MODEM_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s - %s\r\n", __FILE__,  __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_INIT:
    	if (els31_queryOn(&modemIsOn) && !modemIsOn) {
    		SEGGER_RTT_printf(0, "Modem is already off\r\n");
    		if (els31_disablePower()) {
    			SEGGER_RTT_printf(0, "Removed power from modem\r\n");
    			simplehsm_transition_state(&hsm, state_off);
    			return stnone;
    		}
    	}

    	SEGGER_RTT_printf(0, "De-registering modem from network\r\n");
    	/*
    	 * Because we enable the modem's fast shutdown capability, the
    	 * AT^SMSO shutdown command will not de-register the modem from the
    	 * network.  To be kind and proper, we manually de-register before
    	 * turning the modem off.
    	 */
    	if (!modem_sendATCmdString("AT+CFUN=0\r", pdMS_TO_TICKS(10000), SIG_AT_CMD_CFUN_EQUALS_0)) {
    		/* The AT command failed to be sent, likely cause is that the modems
    		 * AT command parser is corrupted. Transisision to state check responsive */
    		simplehsm_transition_state(&hsm, state_forceShutdown);
    	}
    	return stnone;
    case SIG_EXIT:
    	return stnone;

    case SIG_AT_CMD_CFUN_EQUALS_0:
    	p_atResp = (sendATCmd_signal_param_t *)param;

    	if (p_atResp->result == SENDATCMD_RESULT_OK) {
    		SEGGER_RTT_printf(0, "Shutting down modem\r\n");
    		if (!modem_sendData((const uint8_t *)"AT^SMSO\r", 8, NULL, 0, false, pdMS_TO_TICKS(1000), SIG_AT_CMD_SMSO, NULL)) {
    			simplehsm_transition_state(&hsm, state_forceShutdown);
    		}
		} else {
			SEGGER_RTT_printf(0, "ERROR: Failed place modem in minimum functionality mode\r\n");
			simplehsm_transition_state(&hsm, state_forceShutdown);
		}
    	return stnone;


    case SIG_AT_CMD_SMSO:
    	p_atResp = (sendATCmd_signal_param_t *)param;

    	if (p_atResp->result != SENDATCMD_RESULT_OK) {
    		errorCount_incrementCount(ERROR_MODEM_REQUIRING_OFF_CMD_FAILED_AT_ATSHDN);
    		SEGGER_RTT_printf(0, "ERROR: Failed to send shutdown command to modem\r\n");
    		simplehsm_transition_state(&hsm, state_forceShutdown);
    	}
    	return stnone;

    case SIG_URC_SMSO_MS_OFF:
		/*
		 * After receiving ^SMSO: MS OFF, we wait for the modem to deassert
		 * its status line before removing power.
		 */
		SEGGER_RTT_printf(0, "Waiting for modem to completely power-down\r\n");
		statusCheckCount = 0;
		xTimerChangePeriod(gpTimer, pdMS_TO_TICKS(100), portMAX_DELAY);
		xTimerStart(gpTimer, portMAX_DELAY);
		return stnone;

    case SIG_TIMEOUT:
    	if (els31_queryOn(&modemIsOn)) {
    		if (!modemIsOn) {
    			if (els31_disablePower()) {
    				SEGGER_RTT_printf(0, "Removed power from modem\r\n");
    				simplehsm_transition_state(&hsm, state_off);
    			}
    		} else if (++statusCheckCount < 20) {
    			/*
    			 * If the modem is still powering-down wait another 100ms and
    			 * check its status again.
    			 */
    			SEGGER_RTT_printf(0, "Waiting for modem to completely power-down\r\n");
    			xTimerStart(gpTimer, portMAX_DELAY);
    		} else {
    			SEGGER_RTT_printf(0, "ERROR: Modem has not powered-down after 2 seconds; removing power\r\n");
    			if (els31_disablePower()) {
    				SEGGER_RTT_printf(0, "Removed power from modem\r\n");
    				simplehsm_transition_state(&hsm, state_off);
    			}
    		}
    	} else if (++statusCheckCount < 20) {
    		/*
    		 *  Failed to query whether modem is still on, but we have made
    		 *  less than 20 attempts doing so--try again.
    		 */
    		xTimerStart(gpTimer, portMAX_DELAY);
    	} else {
    		/* Repeated queries to check whether the modem is still on have
    		 * failed--remove power anyway.
    		 */
    		errorCount_incrementCount(ERROR_MODEM_REQUIRING_OFF_CMD_FAILED_AT_UNCONDITIONAL_SHUTDOWN);
    		SEGGER_RTT_printf(0, "ERROR: Too many failures querying whether modem is still on; removing power\r\n");
			if (els31_disablePower()) {
				SEGGER_RTT_printf(0, "Removed power from modem\r\n");
				simplehsm_transition_state(&hsm, state_off);
			}
    	}
    	return stnone;
	}

	return state_on;
}

void modem_printRegistrationStatus(unsigned int regStatus) {
	if (regStatus == 0) {
		SEGGER_RTT_printf(0, "Modem is not registered and is not searching\r\n");
	} else if (regStatus == 1) {
		SEGGER_RTT_printf(0, "Modem is registered on home network\r\n");
	} else if (regStatus == 2) {
		SEGGER_RTT_printf(0, "Modem is not registered but searching for network\r\n");
	} else if (regStatus == 3) {
		SEGGER_RTT_printf(0, "Modem registration denied\r\n");
	} else if (regStatus == 4) {
		SEGGER_RTT_printf(0, "Modem registration status unknown, likely out of range of network\r\n");
	} else if (regStatus == 5) {
		SEGGER_RTT_printf(0, "Modem is registered but roaming\r\n");
	} else {
		SEGGER_RTT_printf(0, "Modem registration status unknown (%u)\r\n", regStatus);
	}
}

//

bool modem_sendATCmdString(const char *cmdString, TickType_t xTicksToWait, hsm_signal_t onCompleteSignal) {
	/*
	 * No auxiliary data for the basic sendATCmdString call.
	 */
	return modem_sendATCmdStringAux(cmdString, NULL, 0, xTicksToWait, onCompleteSignal, NULL);
}

bool modem_sendATCmdStringAux(const char *cmdString, const uint8_t *auxData, size_t auxDataLength, TickType_t xTicksToWait, hsm_signal_t onCompleteSignal, void *extraParam) {
	/*
	 * Just compute the length of the cmdString before passing all arguments
	 * along.
	 */
	return modem_sendData((const uint8_t *)cmdString, strlen(cmdString), auxData, auxDataLength, true, xTicksToWait, onCompleteSignal, extraParam);
}

bool modem_sendData(const uint8_t *cmd, uint16_t cmdLength, const uint8_t *auxData, size_t auxDataLength, bool responseExpected, TickType_t xTicksToWait, hsm_signal_t onCompleteSignal, void *extraParam) {
	bool success = false;

	if (sendATCmd_state != SENDATCMD_STATE_IDLE) {
		// There's already an AT command pending
		log_errorf("%s failed because other data is already pending", __FUNCTION__);
		els31uart_prettyPrint((const char *) cmd, cmdLength, "UNSENT COMMAND");		
		success = false;
	} else if (cmdLength > sizeof(sendATCmd_cmd)) {
		// AT command is too big for buffer
		SEGGER_RTT_printf(0, "ERROR: %s failed because the command is too long\r\n", __FUNCTION__);
		success = false;
	} else if (auxDataLength >= sizeof(sendATCmd_auxData)) {
		SEGGER_RTT_printf(0, "ERROR: %s failed because the auxiliary data is too long\r\n", __FUNCTION__);
		success = false;
	} else if ((auxDataLength > 0) && (auxData == NULL)) {
		SEGGER_RTT_printf(0, "ERROR %s failed because auxData pointer is null\r\n", __FUNCTION__);
		success = false;

	} else if (els31uart_sendATCmd(cmd, cmdLength, pdMS_TO_TICKS(250))) {
		// Safe because we already verified that cmdLength <= sizeof(sendATCmd_cmd)
		memcpy(sendATCmd_cmd, cmd, cmdLength);
		sendATCmd_cmdLen = cmdLength;

		if (auxData != NULL) {
			// Safe because we already verified that auxDataLength <= sizeof(sendATCmd_auxData)
			memcpy(sendATCmd_auxData, auxData, auxDataLength);
			sendATCmd_auxDataLength = auxDataLength;
		} else {
			sendATCmd_auxDataLength = 0;
		}

		sendATCmd_respExpected = responseExpected;
		sendATCmd_complete_signal = onCompleteSignal;
		sendATCmd_extraParam = extraParam;

		if (xTimerChangePeriod(sendATCmd_timer, pdMS_TO_TICKS(xTicksToWait), portMAX_DELAY) != pdTRUE) {
			// Failed to change timer period; we'll not bother to start it
			SEGGER_RTT_printf(0, "ERROR: %s failed because the timer period could not be changed\r\n", __FUNCTION__);
			success = false;
		} else if (xTimerStart(sendATCmd_timer, portMAX_DELAY) != pdTRUE) {
			// Failed to start timer
			SEGGER_RTT_printf(0, "ERROR: %s failed because the timer could not be started\r\n", __FUNCTION__);
			success = false;
		} else {
			sendATCmd_state = SENDATCMD_STATE_WAITING_FOR_COMMAND_ECHO;
			success = true;
		}

	} else {
		// Failed to queue command
		SEGGER_RTT_printf(0, "ERROR: %s failed because the AT command could not be en-queued\r\n", __FUNCTION__);
		errorCount_incrementCount(ERROR_MODEM_AT_COMMAND_FAILED_TO_QUEUE);
		success = false;
	}

	return success;
}

void modem_cancelATCmd() {
	xTimerStop(sendATCmd_timer, portMAX_DELAY);
	sendATCmd_extraParam = NULL;
	sendATCmd_state = SENDATCMD_STATE_IDLE;
}

bool processModemResponseLine(modem_uart_response_t *p_response, hsm_signal_t *p_signal, sendATCmd_signal_param_t *p_param) {
	bool sendSignal = false;
	unsigned int reqReadLength;

	static unsigned int srvProfileId = 0;
	static unsigned int unackData = 0;
	static unsigned int cnfReadLength = 0;
	static unsigned int remainUdpPacketLength = 0;
	static unsigned int cnfWriteLength = 0;
	static char Udp_RemClient[24]; // e.g. \"255.255.255.255:65535\"\0
	static bool readPeekOnly = false;


	switch (sendATCmd_state) {
	case SENDATCMD_STATE_IDLE:
		if (p_response->type == 'C') {
			/*
			 * If the modem echoes a command when we have not previously sent
			 * a command, it likely indicates that another task has sent a
			 * command to the modem.  There's no need to pass the command to
			 * modem FSM.
			 */
			sendSignal = false;

			// TODO: Consider updating our state to WAITING_FOR_RESPONSE
		} else {
			/*
			 * We have received a line of data form the modem that is
			 * something other than an echoed command.  We treat the line
			 * as a URC, but it may in fact be the modem's response to a
			 * command executed from another task.
			 */
			sendSignal = processModemURC(p_response, p_signal, p_param);
		}
		break;

	case SENDATCMD_STATE_WAITING_FOR_COMMAND_ECHO:
		if ((p_response->type == 'C') && (strncmp(p_response->data, sendATCmd_cmd, sendATCmd_cmdLen) == 0)) {
			// Modem echoed the command, now we wait for either OK, ERROR, or timeout

			p_param->raw[0] = '\0';
			p_param->rawLength = 0;

			if (sendATCmd_respExpected) {
				if (strncmp(p_response->data, "AT^SISW=", 8) == 0) {
					/*
					 * If we're sending a AT^SISW command, we need to handle it
					 * specially because the number of bytes we can actually
					 * write to the socket is specified in the first line of
					 * the modem's response.  That is, the byte count sent as
					 * part of initial AT command is only a request to write
					 * a certain number of bytes.  The modem then either
					 * accepts this or reduces it.
					 */
					sendATCmd_state = SENDATCMD_STATE_WAITING_FOR_SISW_BYTE_COUNT;
				} else if (sscanf(p_response->data, "AT^SISR=%*u,%u", &reqReadLength) == 1) {
					sendATCmd_state = SENDATCMD_STATE_WAITING_FOR_SISR_BYTE_COUNT;
					if (reqReadLength == 0) {
						readPeekOnly = true;
					} else {
						readPeekOnly = false;
					}
				} else {
					sendATCmd_state = SENDATCMD_STATE_WAITING_FOR_RESPONSE;
				}
				sendSignal = false;
			} else {
				xTimerStop(sendATCmd_timer, portMAX_DELAY);

				p_param->result = SENDATCMD_RESULT_OK;
				*p_signal = sendATCmd_complete_signal;

				sendATCmd_state = SENDATCMD_STATE_IDLE;
				sendSignal = true;
			}
		} else {
			/*
			 * URC was received before the command echoed.
			 */
			sendSignal = processModemURC(p_response, p_signal, p_param);
		}
		break;

	case SENDATCMD_STATE_WAITING_FOR_SISW_BYTE_COUNT:
		if ((p_response->type == 'R') &&
				(sscanf(p_response->data, "^SISW: %u,%u,%u", &srvProfileId, &cnfWriteLength, &unackData) == 3)) {

			if (cnfWriteLength < sendATCmd_auxDataLength) {
				SEGGER_RTT_printf(0, "ERROR: Modem will only accept %d of the %d bytes requested by the ^SISW command\r\n", cnfWriteLength, sendATCmd_auxDataLength);
			} else if (cnfWriteLength > sendATCmd_auxDataLength) {
				SEGGER_RTT_printf(0, "ERROR: Modem is requesting more data bytes (%d) than was provided with the ^SISW command (%d)\r\n", cnfWriteLength, sendATCmd_auxDataLength);
			}

			if (els31uart_sendATCmd(sendATCmd_auxData, cnfWriteLength, pdMS_TO_TICKS(250))) {
				sendATCmd_state = SENDATCMD_STATE_WAITING_FOR_DATA_ECHO;
			} else {
				SEGGER_RTT_printf(0, "ERROR: Failed to en-queue data for ^SISW command\r\n");
			}
		} else if ((p_response->type == 'R') && (strncmp(p_response->data, "ERROR\r\n", 7) == 0)) {
			/* Modem rejected the request to send data, likely the
			 * service id (such as a UDP socket) is not available */
			xTimerStop(sendATCmd_timer, portMAX_DELAY);

			*p_signal = sendATCmd_complete_signal;
			p_param->result = SENDATCMD_RESULT_ERROR;

			sendATCmd_state = SENDATCMD_STATE_IDLE;

			sendSignal = true;
		}
		break;

	case SENDATCMD_STATE_WAITING_FOR_DATA_ECHO:
		if ((p_response->type == 'C') && (memcmp(p_response->data, sendATCmd_auxData, cnfWriteLength) == 0)) {
			sendATCmd_state = SENDATCMD_STATE_WAITING_FOR_RESPONSE;
		} else {
			SEGGER_RTT_printf(0, "ERROR: Modem did not echo ^SISW data as expected\r\n");
		}
		break;

	case SENDATCMD_STATE_WAITING_FOR_SISR_BYTE_COUNT:
		if (p_response->type == 'R') {
			/*
			 * The remainUdpPacketLength parameter is only present if we have
			 * requested to read a number of bytes smaller than the full UDP
			 * packet.  In case sscanf does not find it, we set to to 0 now.
			 */
			remainUdpPacketLength = 0;
			int nArgs = sscanf(p_response->data, "^SISR: %u,%u,%u", &srvProfileId, &cnfReadLength, &remainUdpPacketLength);

			/*
			 * If the modem is serving as a UDP end-point, the last (either 3rd or
			 * 4th) parameter will contain the IP and port of the remote client
			 * in a string starting with a double quotation.
			 */
			Udp_RemClient[0] = '\0';
			if (sscanf(substr(p_response->data, "\""), "%23s", Udp_RemClient) == 1) {
				// Shift bytes 1-22 down by 1 index to remove the leading double-quote
				memmove(&Udp_RemClient[0], &Udp_RemClient[1], 22);
				// Over-write the trailing double-quote with a null terminator
				Udp_RemClient[strlen(Udp_RemClient) - 1] = '\0';
			}

			/*
			 * If sscanf didn't convert at least two arguments, (the service
			 * ID and the byte count), we'll wait in this state until timeout
			 */
			if (nArgs >= 2) {
				if ((remainUdpPacketLength > 0) && (!readPeekOnly)) {
					SEGGER_RTT_printf(0, "ERROR: ^SISR command is not reading entire packet--%u bytes remain\r\n", remainUdpPacketLength);
				}

				if (readPeekOnly) {
					/*
					 * If we're only peeking (by setting reqReadLength to 0,
					 * no bytes will follow, so we start looking for the OK.
					 */
					sendATCmd_state = SENDATCMD_STATE_WAITING_FOR_RESPONSE;
				} else {
					/*
					 * We'll use the auxDataLength as an index into the auxData
					 * array. The received data from the modem could come in
					 * multiple chunks because the low-level state machine interprets
					 * \r\n as a line break.  So, if there a \r\n in the received
					 * data, the low level state machine will send a message
					 * containing everything up to and include the \r\n and then
					 * send another message with everything after.
					 */
					sendATCmd_auxDataLength = 0;
					sendATCmd_state = SENDATCMD_STATE_WAITING_FOR_RECEIVED_DATA;
				}
			}
		}
		break;

	case SENDATCMD_STATE_WAITING_FOR_RECEIVED_DATA:
		if (p_response->type == 'R') {
			if (sendATCmd_auxDataLength + p_response->length <= cnfReadLength) {
				// Room for all received data in the buffer
				memcpy(&sendATCmd_auxData[sendATCmd_auxDataLength], p_response->data, p_response->length);
				sendATCmd_auxDataLength += p_response->length;
			} else {
				// Only room for some of the received data in the buffer
				memcpy(&sendATCmd_auxData[sendATCmd_auxDataLength], p_response->data, cnfReadLength - sendATCmd_auxDataLength);
				sendATCmd_auxDataLength = cnfReadLength;
				/* Now that the buffer is full, wait for the final OK */
				sendATCmd_state = SENDATCMD_STATE_WAITING_FOR_RESPONSE;
			}
		}
		break;

	case SENDATCMD_STATE_WAITING_FOR_RESPONSE:
		if ((p_response->type == 'R') && (strncmp(p_response->data, "OK\r\n", 4) == 0)) {
			xTimerStop(sendATCmd_timer, portMAX_DELAY);

			*p_signal = sendATCmd_complete_signal;

			if (*p_signal == SIG_AT_CMD_SISR) {
				p_param->parsed.cmd_sisr.srvProfileId = srvProfileId;
				p_param->parsed.cmd_sisr.cnfReadLength = cnfReadLength;
				p_param->parsed.cmd_sisr.remainUdpPacketLength = remainUdpPacketLength;
				strncpy(p_param->parsed.cmd_sisr.Udp_RemClient, Udp_RemClient, sizeof(p_param->parsed.cmd_sisr.Udp_RemClient) - 1);
				p_param->parsed.cmd_sisr.Udp_RemClient[sizeof(p_param->parsed.cmd_sisr.Udp_RemClient) - 1] = '\0';

				if (sendATCmd_auxDataLength <= sizeof(p_param->raw)) {
					memcpy(p_param->raw, sendATCmd_auxData, sendATCmd_auxDataLength);
					p_param->rawLength = sendATCmd_auxDataLength;
				} else {
					SEGGER_RTT_printf(0, "ERROR: Received data too large for sendATCmd_signal_param_t.raw\r\n");
					memcpy(p_param->raw, sendATCmd_auxData, sizeof(p_param->raw));
					p_param->rawLength = sizeof(p_param->raw);
				}
			}

			p_param->result = SENDATCMD_RESULT_OK;
			p_param->extraParam = sendATCmd_extraParam;

			sendATCmd_state = SENDATCMD_STATE_IDLE;

			sendSignal = true;
		} else if ((p_response->type == 'R') && (strncmp(p_response->data, "ERROR\r\n", 7) == 0)) {
			xTimerStop(sendATCmd_timer, portMAX_DELAY);

			*p_signal = sendATCmd_complete_signal;
			p_param->result = SENDATCMD_RESULT_ERROR;

			sendATCmd_state = SENDATCMD_STATE_IDLE;

			sendSignal = true;
		} else if (p_response->type == 'R') {
			if (p_param->rawLength + p_response->length < sizeof(p_param->raw)) {
				memcpy(&p_param->raw[p_param->rawLength], p_response->data, p_response->length);
				p_param->rawLength += p_response->length;
				p_param->raw[p_param->rawLength] = '\0';
			} else {
				SEGGER_RTT_printf(0, "ERROR: In %s, modem response truncated because is sendATCmd_signal_param.raw is full\r\n", __FUNCTION__);
				memcpy(&p_param->raw[p_param->rawLength], p_response->data,
						sizeof(p_param->raw) - p_param->rawLength - 1);
				p_param->rawLength = sizeof(p_param->raw) - 1;
				p_param->raw[p_param->rawLength] = '\0';
			}
			/* Wait to send signal until we receive OK or ERROR */
			sendSignal = false;
		} else {
			// Modem appears to have echoed a command, which is unexpected, we still just pass it along
			SEGGER_RTT_printf(0, "ERROR: In %s, unexpected command echo received from modem\r\n", __FUNCTION__);
			sendSignal = false;
		}
		break;
	default:
		SEGGER_RTT_printf(0, "ERROR: In %s, unexpected state\r\n", __FUNCTION__);
		sendATCmd_state = SENDATCMD_STATE_IDLE;
		sendSignal = false;
	}

	return sendSignal;
}


bool processModemURC(modem_uart_response_t *p_response, hsm_signal_t *p_signal, sendATCmd_signal_param_t *p_param) {
	bool sendSignal = true;
	bool matchFound = false;
	int nArgs;

	p_param->result = SENDATCMD_RESULT_OK;

	if (strncmp(p_response->data, "^SYSSTART", 9) == 0) {
		*p_signal = SIG_URC_SYSSTART;
		matchFound = true;
	} else if (strncmp(p_response->data, "^SIS: 0,0,48", 12) == 0) {
		// remote peer has closed the TCP connection
		*p_signal = SIG_URC_TCP_REMOTE_CLOSE;
		matchFound = true;
	} else if (strncmp(p_response->data, "^SBC", 4) == 0) {
		char sbcCause[32];
		if (sscanf(p_response->data, "%*[^:]: %s", sbcCause) == 1) {
			matchFound = true;
			if (strncmp(sbcCause, "Undervoltage Warning", 4) == 0) {
				p_param->parsed.urc_sbc.urcCause = UNDERVOLTAGE_WARNING;
			} else if (strncmp(sbcCause, "Undervoltage Shutdown", 4) == 0) {
				p_param->parsed.urc_sbc.urcCause = UNDERVOLTAGE_SHUTDOWN;
			} else if (strncmp(sbcCause, "Overvoltage Warning", 4) == 0) {
				p_param->parsed.urc_sbc.urcCause = OVERVOLTAGE_WARNING;
			} else if (strncmp(sbcCause, "Overvoltage Shutdown", 4) == 0) {
				p_param->parsed.urc_sbc.urcCause = OVERVOLTAGE_SHUTDOWN;
			} else {
				matchFound = false;
			}
			if (matchFound) {
				*p_signal = SIG_URC_SBC;
			}
		}
	} else if (strncmp(p_response->data, "^SHUTDOWN", 9) == 0) {
		*p_signal = SIG_URC_SHUTDOWN;
		matchFound = true;
	} else if (strncmp(p_response->data, "^SISW", 5) == 0) {
		unsigned int dummy;

		nArgs = sscanf(p_response->data, "%*[^:]: %u,%u,%u",
				&p_param->parsed.urc_sisw.srvProfileId,
				&p_param->parsed.urc_sisw.urcCauseId,
				&dummy);

		/*
		 * The AT^SISW command generates a response that looks very
		 * similar to ^SISW URC.  The only difference is that the URC
		 * always has two parameters (srvProfileId and urcCauseId), but
		 * the command response always has three parameters (srvProfileId,
		 * cnfWriteLength, and unackData).
		 */

		if (nArgs == 2) {
			*p_signal = SIG_URC_SISW;
			p_param->result = SENDATCMD_RESULT_OK;
			matchFound = true;
		} else {
			/*
			 * Setting matchFound to false will result in sending a
			 * SIG_MODEM_COMM signal below.
			 */
			sendSignal = false;
			matchFound = false;
		}
	} else if (strncmp(p_response->data, "^SISR", 5) == 0) {
		/*
		 * The AT^SISR command generates a response that looks very
		 * similar or even identical to ^SISW URC.  The only difference is that
		 * the URC always has two parameters (srvProfileId and urcCauseId), but
		 * the command response has two, three, or four parameters.  The only
		 * guaranteed way to differentiate is by context, i.e. whether the ^SISR
		 * line is in response to an AT command or not.
		 */
		unsigned int dummy;
		if (sscanf(p_response->data, "%*[^:]: %u,%u,%u",
				&p_param->parsed.urc_sisr.srvProfileId,
				&p_param->parsed.urc_sisr.urcCauseId,
				&dummy) == 2) {
			*p_signal = SIG_URC_SISR;
			p_param->result = SENDATCMD_RESULT_OK;
			matchFound = true;
		}
	} else if (strncmp(p_response->data, "+CEREG", 6) == 0) {
		*p_signal = SIG_URC_CEREG;

		nArgs = sscanf(p_response->data, "%*[^:]: %u,%u,%u,%u",
				&p_param->parsed.urc_cereg.stat,
				&p_param->parsed.urc_cereg.tac,
				&p_param->parsed.urc_cereg.ci,
				&p_param->parsed.urc_cereg.AcT);

		/*
		 * The +CEREG URC should have either 1 argument or 4 arguments.  Two
		 * arguments indicates that the line is a response to a AT+CEREG=
		 * command, not a URC.
		 */
		if (nArgs == 1) {
			matchFound = true;
			/*
			 * The tac, ci, and Act parameter will only be present if a
			 * AT+CEREG=2 command has been issued.
			 */
			p_param->parsed.urc_cereg.tac = p_param->parsed.urc_cereg.ci = p_param->parsed.urc_cereg.AcT = 0;
		} else if (nArgs == 4) {
			matchFound = true;
		} else {
			matchFound = false;
		}
	} else if (strncmp(p_response->data, "^SMSO: MS OFF", 13) == 0) {
		*p_signal = SIG_URC_SMSO_MS_OFF;
		matchFound = true;
	}


	if (!matchFound) {
		*p_signal = SIG_MODEM_COMM;
		sendSignal = true;
		p_param->result = SENDATCMD_RESULT_OK;
		if (p_response->length < sizeof(p_param->raw)) {
			memcpy(p_param->raw, p_response->data, p_response->length);
			p_param->rawLength = p_response->length;
			p_param->raw[p_param->rawLength] = '\0';
		} else {
			memcpy(p_param->raw, p_response->data, sizeof(p_param->raw) - 1);
			p_param->rawLength = sizeof(p_param->raw) - 1;
			p_param->raw[p_param->rawLength] = '\0';
		}
	}

	return sendSignal;
}


bool modem_atCommandTimerExpired(hsm_signal_t *p_signal, sendATCmd_signal_param_t *p_param) {
	bool sendSignal = false;

	if ((sendATCmd_state == SENDATCMD_STATE_WAITING_FOR_COMMAND_ECHO)
			|| (sendATCmd_state == SENDATCMD_STATE_WAITING_FOR_RESPONSE)) {
		/*
		 * If we were waiting for the modem to either echo back an AT command that
		 * we're trying to send or waiting for the modem to respond (with OK or
		 * ERROR) to an AT command that it has already echoed and the timer expires,
		 * we send a signal (as set by the call to modem_sendATCmd) to the HSM with
		 * a parameter indicating that the timer expired before the modem finished
		 * responding.
		 */
		*p_signal = sendATCmd_complete_signal;
		p_param->result = SENDATCMD_RESULT_TIMEOUT;

		sendATCmd_state = SENDATCMD_STATE_IDLE;

		sendSignal = true;
	} else {
		SEGGER_RTT_printf(0, "ERROR: In %s, AT command timer expired while not waiting on a response from the modem\r\n", __FILE__);

		sendSignal = false;
	}

	return sendSignal;
}

void modem_atCommandTimerCallback(TimerHandle_t timer) {
	/*
	 * This callback will be called by FreeRTOS' timer task, so we cannot call
	 * any of the other functions in the file safely.  Instead, we set a bit in
	 * the event group.  When the modem tasks sees that this bit has been set,
	 * it will call the modem_atCommandTimerExpired() function above.
	 */
	if (queueEventGroup != NULL) {
		xEventGroupSetBits(queueEventGroup, AT_COMMAND_TIMER_EVENT_GROUP_BIT);
	}
}


void modem_generalPurposeTimerCallback(TimerHandle_t timer) {
	if (queueEventGroup != NULL) {
		xEventGroupSetBits(queueEventGroup, GENERAL_PURPOSE_TIMER_EVENT_GROUP_BIT);
	}
}


/* The substr() function finds the first occurrence of the substring needle in
 * the string haystack. The terminating null bytes (\0) are not compared. The
 * functions returns a pointer to the beginning of the substring, or if the
 * substring is not found, a pointer to the NULL at the end of the haystack. */
const char *substr(const char *haystack, const char *needle) {
	char *cp;

	cp = strstr(haystack, needle);

	if (cp == NULL) {
		return &haystack[strlen(haystack)-1];
	}

	return cp;
}
