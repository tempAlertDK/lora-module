#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include "defines.h"
#include "crc.h"
#include "sfFlashBuf.h"
#include "spiflash_utils.h"
#include "log.h"

#define FLASH_ADDRESS_OFFSET APP_DATA_START_ADDR
#define RECORD_SIZE 64

enum {
	SECTOR_EMPTY = 0xFF,
	SECTOR_OCCUPIED = 0x22,
	SECTOR_SPOILED = 0x00
};

enum {
	RECORD_EMPTY = 0xFF,
	RECORD_FILLER = 0xCC,
	RECORD_VALID = 0xAA,
	RECORD_EXTENDED = 0x55,
	RECORD_CONTINUATION = 0x33,
	RECORD_INVALID = 0x00
};

typedef struct __attribute__ ((__packed__)) {
	uint8_t status;
	uint8_t dataLen;
	uint8_t data[RECORD_SIZE - 3 - 1];
	uint8_t crc;
	/* The sectorStatus field is not used in context of each record.  It is 
	 * used to reserve one byte at the end of each sector to hold the sector's
	 * status byte. */
	uint8_t sectorStatus;
} flashRecord_t;

static bool basicInitialized = false;
static bool fullyInitialized = false;

/* Even a 2Mbit flash contains 262144 bytes, so we need a uint32 */
static uint32_t flashSize;
/* Sectors are generally small, e.g. 4096 bytes, so a uint16 is sufficient*/
static uint16_t sectorSize;
/* With 4096-byte sectors, a uint16 will cover flash sizes up to 2Gbits */
static uint16_t sectorCount;
/* With 64 byte records, a uint16 will cover flash sizes up to 32Mbits */
static uint16_t recordCount;
/* We can only fit 64 64-byte records into a 4096 byte buffer */
static uint16_t recordsPerSector;

static uint16_t inRecPtr;
static uint16_t outRecPtr;
static bool empty;

static uint8_t sfFlashBuf_computeChecksum(flashRecord_t *record);
static bool sfFlashBuf_verifyChecksum(flashRecord_t *record);
static bool sfFlashBuf_getSectorStatus(uint16_t sectorNum, uint8_t *status);
static bool sfFlashBuf_setSectorStatus(uint16_t sectorNum, uint8_t status);
static bool sfFlashBuf_eraseSector(uint16_t sectorNum);
static bool sfFlashBuf_eraseAll(void);
static bool sfFlashBuf_getRecordStatus(uint16_t recordNum, uint8_t *status);
static bool sfFlashBuf_setRecordStatus(uint16_t recordNum, uint8_t status);
static bool sfFlashBuf_writeRecord(uint16_t recordNum, flashRecord_t *record);
static bool sfFlashBuf_readRecord(uint16_t recordNum, flashRecord_t *record);

bool sfFlashBuf_init() {
	uint16_t sectorNum = 0, nextSectorNum = 0;
	uint16_t emptySectorCount = 0;
	uint16_t recordNum = 0;
	uint8_t sectorStatus, nextSectorStatus;
	uint8_t recordStatus;
	bool inSectorFound = false;
	bool inRecordFound = false;
	bool outSectorFound = false;
	bool outRecordFound = false;
	uint16_t outSecPtr = 0, inSecPtr = 0;
	
	log_info("Initializing flash-based sto/fwd FIFO");
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
	
		spiflash_disableBlockProtect(0x0F);
		
		flashSize = spiflash_getSize() - FLASH_ADDRESS_OFFSET;
		/* Limit the useable size of the flash buffer to keep from accumulating
		 * too many measurements that would only overwhelm the server. */
		if (flashSize > DATA_FIFO_BUFFER_SIZE) {
			flashSize = DATA_FIFO_BUFFER_SIZE;
		}
		
		sectorSize = spiflash_getSectorSize();
		
		if ((flashSize == 0) || (sectorSize == 0)) {
			basicInitialized = false;
			fullyInitialized = false;
			log_error("Unable to initialize serial flash; chip missing or non-responsive!");
			return false; 
		}
		
		sectorCount = flashSize / sectorSize - 1;
		recordCount = flashSize / RECORD_SIZE; 
		recordsPerSector = sectorSize / RECORD_SIZE;
		
		basicInitialized = true;

		/* Scan over all sectors to identify the 'in' sector (that is, the sector
		 * containing the empty record where the first measurement to be "pushed"
		 * onto the FIFO will be placed) and the 'out' sector (that is, the sector
		 * containing the oldest non-invalid record that will be first to be
		 * "popped" from the FIFO. */
		for (sectorNum = 0; sectorNum < sectorCount; sectorNum++) {
			nextSectorNum = (sectorNum + 1) % sectorCount;

			sfFlashBuf_getSectorStatus(sectorNum, &sectorStatus);
			sfFlashBuf_getSectorStatus(nextSectorNum, &nextSectorStatus);

			if (sectorStatus == SECTOR_EMPTY) {
				emptySectorCount++;
			}
			
			/* The 'in' sector is identified by the fact that it is occupied with
			 * some number of measurements but the next sector is empty. */
			if (!inSectorFound && (sectorStatus == SECTOR_OCCUPIED) &&
					(nextSectorStatus == SECTOR_EMPTY)) {
				inSectorFound = true;
				inSecPtr = sectorNum;
			}

			/* The 'out' sector is identified by the fact that it is the first
			 * occupied sector after an empty sector. */
			if (!outSectorFound && (sectorStatus == SECTOR_EMPTY) && 
					(nextSectorStatus == SECTOR_OCCUPIED)) {
				outSectorFound = true;
				outSecPtr = nextSectorNum;
			}

			/* There's no need to waste time continuing the search once we have
			 * found both the in and out sectors. */
			if (inSectorFound && outSectorFound) {
				break;
			}
		}

		if (emptySectorCount == sectorCount) {
			log_warn("Flash sto/fwd buffer is empty.");

			sfFlashBuf_eraseAll();
			inRecPtr = 0;
			outRecPtr = 0;
			empty = true;
			fullyInitialized = true;
			return true;
		} else if (!(inSectorFound && outSectorFound)) {
			/* If we did not find both the 'in' and 'out' sectors, something is 
			 * wrong. We should always be able to identify them because we 
			 * always keep a completely empty sector between the 'in' sector and
			 * the 'out' sector so that the 'in' sector cannot "lap" the 'out' 
			 * sector in the case where lots of data is being written to the 
			 * FIFO but none is being removed. */
			log_error("Error recovering flash sto/fwd buffer ('in'/'out' sectors not found); buffer is being re-initialized and all data will be lost!");

			sfFlashBuf_eraseAll();
			inRecPtr = 0;
			outRecPtr = 0;
			empty = true;
			fullyInitialized = true;
			return true;
		}

		/* Scan the sector which we believe contains the most recently added record
		 * for the first empty record.  This empty record becomes the 'in' record
		 * pointer.  It is identified by the fact that it is the first empty 
		 * record. */
		for (recordNum = inSecPtr * recordsPerSector; 
				recordNum < (inSecPtr + 1) * recordsPerSector; recordNum++) {
			sfFlashBuf_getRecordStatus(recordNum, &recordStatus);

			if (recordStatus == RECORD_EMPTY) {
				inRecPtr = recordNum;
				inRecordFound = true;
				break;
			}
		}

		/* It could be that the sector is completely full and that the first empty
		 * record resides in the next sector.  */
		if (!inRecordFound) {
			inSecPtr = (inSecPtr + 1) % sectorCount;
			recordNum = inSecPtr * recordsPerSector;

			sfFlashBuf_getRecordStatus(recordNum, &recordStatus);
			/* If the first record of the next sector is not empty, the original
			 * sector never should have been flagged as the sector containing the
			 * input pointer; something has gone wrong. */
			if (recordStatus != RECORD_EMPTY) {
				log_error("Error recovering flash sto/fwd buffer ('in' record not found); buffer is being re-initialized and all data will be lost!");

				sfFlashBuf_eraseAll();
				inRecPtr = 0;
				outRecPtr = 0;
				empty = true;
				fullyInitialized = true;
				return true;			
			} else {
				inRecPtr = recordNum;
				inRecordFound = true;
			}
		}

		log_warnf("Flash-based sto/fwd FIFO 'in' record number: %u", inRecPtr);

		/* Scan the sector which we believe contains the oldest record for the first
		 * non-invalid record.  This first valid record becomes in output record
		 * pointer.  That is, it will be the first record to be "popped" from the
		 * FIFO.  It is identified by the fact that it is the first non-invalid
		 * record in the sector. */
		for (recordNum = outSecPtr * recordsPerSector;
				recordNum < (outSecPtr + 1) * recordsPerSector; recordNum++) {

			sfFlashBuf_getRecordStatus(recordNum, &recordStatus);

			if (recordStatus != RECORD_INVALID) {
				outRecPtr = recordNum;
				outRecordFound = true;
				break;
			}
		}

		/* There is a chance that the sector is filled with invalid records and that
		 * the first non-invalid record is in the next sector.  This could happen if
		 * the device lost power after marking the last record of the sector invalid
		 * but before it had time to erase the sector.  */
		if (!outRecordFound) {
			outSecPtr = (outSecPtr + 1) % sectorCount;
			recordNum = outSecPtr * recordsPerSector;

			sfFlashBuf_getRecordStatus(recordNum, &recordStatus);

			/* If the first record of the next sector is also invalid, the original
			 * sector never should have been flagged as the sector containing the
			 * out record pointer; something has gone wrong. */
			if (recordStatus == RECORD_INVALID) {
				log_error("Error recovering flash sto/fwd buffer ('out' record not found); buffer is being re-initialized and all data will be lost!");

				sfFlashBuf_eraseAll();
				inRecPtr = 0;
				outRecPtr = 0;
				empty = true;
				fullyInitialized = true;
				return true;				
			} else {
				outRecPtr = recordNum;
				outRecordFound = true;
			}
		}

		log_warnf("Flash-based sto/fwd FIFO 'out' record number: %u", outRecPtr);
		

		if (inRecPtr == outRecPtr) {
			empty = true;
		}

		fullyInitialized = true;
		
		log_warnf("Number of records in flash-based sto/fwd FIFO: %u", sfFlashBuf_countItems());
		log_warnf("Flash-based sto/fwd FIFO space consumed: %lu of %lu (bytes)", sfFlashBuf_getConsumedSpace(), sfFlashBuf_getTotalSpace());
		log_warn("");
	}
	
	return true;
}

uint16_t sfFlashBuf_getInRecPtr(){
	return inRecPtr;
}

uint16_t sfFlashBuf_getOutRecPtr() {
	return outRecPtr;
}

bool sfFlashBuf_isEmpty() {
	if (!fullyInitialized)
		return true;
	
	return empty;
}

uint16_t sfFlashBuf_countItems() {
	uint16_t recordNumber;
	uint8_t recordStatus;
	uint16_t count = 0;
	
	if (!fullyInitialized) {
		return 0;
	}
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		recordNumber = outRecPtr;

		/* Iterate over all records between the 'out' pointer and the 'in' pointer,
		 * incrementing the record count each time we find a valid (i.e. basic) or
		 * extended record. */
		while (recordNumber != inRecPtr) {
			sfFlashBuf_getRecordStatus(recordNumber, &recordStatus);

			if ((recordStatus == RECORD_VALID) || 
					(recordStatus == RECORD_EXTENDED)) {
				count++;
			}

			recordNumber = (recordNumber + 1) % recordCount;
		}
	}
	
	return count;
}

uint32_t sfFlashBuf_getTotalSpace() {
	if (!basicInitialized) {
		return 0;
	}
	
	/* We always keep at least one sector erased */
	return flashSize - sectorSize;
}

uint32_t sfFlashBuf_getAvailSpace() {
	uint16_t occupiedRecordCount, freeRecordCount;
	uint32_t occupiedSpace;
	uint32_t availSpace;
	
	if (!fullyInitialized) {
		return 0;
	}
		
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		/* The 'in' pointer always "ahead of" the 'out' pointer, but because
		 * we're using a circular buffer, the 'in' pointer is not always 
		 * numerically larger than the 'out' pointer. */
	
		if (inRecPtr >= outRecPtr) {
			/* When the 'in' pointer has not looped around "behind" the 'out' 
			 * pointer, the amount of used space is the difference between the 
			 * two pointers.  Therefore, the amount of free space is the 
			 * total space minus the used space. */
			occupiedRecordCount = inRecPtr - outRecPtr;
			
			/* All records in the same sector as the 'out' pointer are also 
			 * unavailable because they cannot be repurposed for new data 
			 * without erasing the entire sector. */
			occupiedRecordCount += outRecPtr % recordsPerSector;
			
			/* Each record is a fixed size, so it is easy to compute the total
			 * occupied and available space. */
			occupiedSpace = RECORD_SIZE * (uint32_t)occupiedRecordCount;
			availSpace = flashSize - occupiedSpace;
			
			/* We always keep one sector empty so that the 'in' pointer cannot
			 * overtake the 'out' pointer. This empty sector is not available
			 * for storage, so we subtract its size. */
			availSpace -= sectorSize;
		} else {
			/* When the top/front index has looped around the end of the 
			 * circular buffer and is now numerically less than the bottom/back
			 * index, the remaining free space is the number of bytes between 
			 * the 'out' pointer and the 'in' pointer */
			freeRecordCount = outRecPtr - inRecPtr;
			
			/* All records in the same sector as the 'out' pointer are also 
			 * unavailable because they cannot be repurposed for new data 
			 * without erasing the entire sector. */
			freeRecordCount -= outRecPtr % recordsPerSector;
			
			/* We always keep one sector empty so that the 'in' pointer cannot
			 * overtake the 'out' pointer. This empty sector is not available
			 * for storage, so we subtract the number of records it contains. */
			freeRecordCount -= recordsPerSector;
			
			/* Each record is a fixed size, so it is easy to compute the total
			 * available space. */
			availSpace = RECORD_SIZE * (uint32_t)freeRecordCount;
		}
	}
	
	return availSpace;
}

uint32_t sfFlashBuf_getConsumedSpace() {
	if (!fullyInitialized) {
		return 0;
	}
	
	return sfFlashBuf_getTotalSpace() - sfFlashBuf_getAvailSpace();
}

void sfFlashBuf_flush() {
	if (!fullyInitialized) {
		return;
	}
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		sfFlashBuf_eraseAll();
		inRecPtr = 0;
		outRecPtr = 0;
		empty = true;
	}
}

bool sfFlashBuf_push(const uint8_t *data, uint8_t dataLen) {
	flashRecord_t fillerRecord;
	flashRecord_t record[2];
	bool split;
	bool beginningOfSector, endOfSector;
	uint8_t sectorStatus;
	uint16_t inSecPtr, outSecPtr;
	bool success = true;
	
	if (!fullyInitialized) {
		return false;
	}
	
	/* Erased flash bytes contains 0xFF, so we prefill the records to match */
	memset(&fillerRecord, 0xFF, sizeof(flashRecord_t));
	memset(&record[0], 0xFF, sizeof(flashRecord_t));
	memset(&record[1], 0xFF, sizeof(flashRecord_t));
	
	/* Populate the filler record in case we need to use it. */
	fillerRecord.status = RECORD_FILLER;
	fillerRecord.dataLen = 0;
	fillerRecord.crc = sfFlashBuf_computeChecksum(&fillerRecord);
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
	
		/* Derive the sector number in which the in and out record pointers reside*/
		inSecPtr = (uint16_t)((RECORD_SIZE * (uint32_t)inRecPtr) / (uint32_t)sectorSize);
		outSecPtr = (uint16_t)((RECORD_SIZE * (uint32_t)outRecPtr) / (uint32_t)sectorSize);

		/* Determine whether the data is large enough that we need to split it over
		 * two records. */
		if (dataLen <= sizeof(record[0].data)) {
			split = false;
		} else if (dataLen <= 2*sizeof(record[0].data)) {
			split = true;
		} else {
			log_error("Data passed to sfFlashBuf_push() is too large!");
			return false;
		}

		/* We do not want extended records to be split over two sectors.  Therefore,
		 * if this is the last record in the sector, and we have to split the 
		 * measurement across two flash records, will will place a dummy record in
		 * the last record of the current sector and place the new measurement in 
		 * the first two flash records of the next sector. */
		if (inRecPtr % recordsPerSector == recordsPerSector - 1) {
			endOfSector = true; 
		} else {
			endOfSector = false;
		}

		if (inRecPtr % recordsPerSector == 0) {
			beginningOfSector = true;
		} else {
			beginningOfSector = false;
		}

		if (split) {
			record[0].status = RECORD_EXTENDED;
			record[0].dataLen = (uint8_t)sizeof(record[0].data);
			memcpy(record[0].data, data, record[0].dataLen);
			record[0].crc = sfFlashBuf_computeChecksum(&record[0]);

			record[1].status = RECORD_CONTINUATION;
			record[1].dataLen = (uint8_t)(dataLen - record[0].dataLen);
			/* Copy what remaining data would not first into the extended record 
			 * into the data field of the continuation record. */
			memcpy(record[1].data, &data[record[0].dataLen], record[1].dataLen);
			/* The CRC of the continuation record is independent of the CRC of the
			 * extended record. */
			record[1].crc = sfFlashBuf_computeChecksum(&record[1]);
		} else {
			record[0].status = RECORD_VALID;
			record[0].dataLen = (uint8_t)dataLen;
			memcpy(record[0].data, data, record[0].dataLen);
			record[0].crc = sfFlashBuf_computeChecksum(&record[0]);
		}

		if (endOfSector && split) {
			/* If the current "in pointer" points to the last spot in the current 
			 * sector and the measurement we are trying to add to the buffer is 
			 * split over two flash records, we place a filler record into the last
			 * spot of the current sector and then place the new measurement in the 
			 * next sector. */

			/* We should never have to worry about the next sector containing the
			 * out pointer because we are about to write the _last_ record of the 
			 * current sector, and we should have erased the next sector when we
			 * wrote the first record of the current sector.
			 */

			if (!sfFlashBuf_writeRecord(inRecPtr, &fillerRecord)) {
				log_error("Unable to write filler record to flash!");
				success = false;
			}
			inRecPtr = (inRecPtr + 1) % recordCount;

			/* Since we have incremented the in record pointer, we must update the 
			 * in sector pointer as well. */
			inSecPtr = (uint16_t)((RECORD_SIZE * (uint32_t)inRecPtr) / (uint32_t)sectorSize);

			beginningOfSector = true;
			endOfSector = false;
		}

		if ((inSecPtr + 1) % sectorCount == outSecPtr) { 
			/* If the sector following the sector into which the new record(s) will
			 * be written is the sector which currently contains the out pointer,
			 * we need to bump the out pointer into the subsequent sector and erase
			 * the sector which used to contain the out pointer so that we always 
			 * maintain one erased sector between the in and out pointers. */
			sfFlashBuf_eraseSector(outSecPtr);

			outSecPtr = (outSecPtr + 1) % sectorCount;
			/* The new out pointer is the first record of the new out sector. */
			outRecPtr = outSecPtr * recordsPerSector;
		}

		if (beginningOfSector) {
			/* If we are about to write a flash record into the first space of the
			 * current sector, we verify that the sector status indicates that the
			 * sector is empty.  If it does not, we erase the sector. */
			if (!sfFlashBuf_getSectorStatus(inSecPtr, &sectorStatus)) {
				success = false;
			}

			if (sectorStatus != SECTOR_EMPTY) {
				sfFlashBuf_eraseSector(inSecPtr);
			}
		}

		/* Before writing any data to the sector, mark the sector as occupied so 
		 * that if a power failure occurs while writing data we will have some 
		 * indication that the sector may contain some (possibly corrupt) data. */
		if (!sfFlashBuf_setSectorStatus(inSecPtr, SECTOR_OCCUPIED)) {
			success = false;
		}

		if (!sfFlashBuf_writeRecord(inRecPtr, &record[0])) {
			success = false;
		}

		inRecPtr = (inRecPtr + 1) % recordCount;

		if (split) {
			if (!sfFlashBuf_writeRecord(inRecPtr, &record[1])) {
				success = false;
			}
			inRecPtr = (inRecPtr + 1) % recordCount;
		}

		empty = false;
	}
		
	return success;
}

bool sfFlashBuf_pop(uint8_t *data, uint8_t *dataLen) {
	flashRecord_t record[2]; 
	uint16_t outSecPtr;
	bool validRecordFound = false;
	uint8_t i;

	if (!fullyInitialized) {
		return false;
	}
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
	
		i = 0;
		while (!(validRecordFound || empty)) {	
			sfFlashBuf_readRecord(outRecPtr, &record[i]);

			if (record[i].status == RECORD_VALID) {
				if (sfFlashBuf_verifyChecksum(&record[i])) {
					validRecordFound = true;
					if (*dataLen >= record[i].dataLen) {
						memcpy(data, record[i].data, record[i].dataLen);
						*dataLen = record[i].dataLen;
					} else {
						*dataLen = 0;
						log_error("Space for returned record provided by caller to sfFlashBuf_pop() by caller is too small!");
					}
				} else {
					log_error("sfFlashBuf_pop() failed to verify checksum of record!");
				}
			} else if (record[i].status == RECORD_FILLER) {
				;
			} else if (record[i].status == RECORD_EXTENDED) {
				if (i != 0) {
					/* If i was 1 (the only valid value other than 0) and we found
					 * an extended record instead of a continuation record, we
					 * restart our search for the next valid record by resetting i 
					 * to 0. */
					log_error("sfFlashBuf_pop() found unexpected extended record!");
					i = 0;
				} else {
					/* If the extended record was found as the first part of a two-
					 * part record pair, increment i so that what we expect to be 
					 * the continuation record in the next spot does not overwrite 
					 * record[0]. */
					i=1;
				}
			} else if (record[i].status == RECORD_CONTINUATION) {
				if (i != 1) {
					/* If i was 0 (the only valid value other than 1) and we found
					 * a continuation record, we ignore it because we should have 
					 * first found an extended record.*/
					log_error("sfFlashBuf_pop() found unexpected continuation record!");
					i = 0;
				} else if (!sfFlashBuf_verifyChecksum(&record[0])) {
					log_error("sfFlashBuf_pop() failed to verify checksum of extended record!");
				} else if (!sfFlashBuf_verifyChecksum(&record[1])) {
					log_error("sfFlashBuf_pop() failed to verify checksum of continuation record!");
				} else {
					validRecordFound = true;
					if (*dataLen >= record[0].dataLen + record[1].dataLen) {
						memcpy(&data[0], record[0].data, record[0].dataLen);
						memcpy(&data[record[0].dataLen], record[1].data, record[1].dataLen);
						*dataLen = record[0].dataLen + record[1].dataLen;
					} else if (*dataLen >= record[0].dataLen) {
						*dataLen = 0;
						log_error("Space for returned record provided by caller to sfFlashBuf_pop() by caller is too small!");
					}
				}		
			} else if (record[i].status == RECORD_INVALID) {
				log_error("sfFlashBuf_pop() found unexpected invalid record!");
			} else if (record[i].status == RECORD_EMPTY) {
				log_error("sfFlashBuf_pop() found unexpected empty record!");
			} else {
				log_error("sfFlashBuf_pop() found unknown record type!");
			}

			/* After reading each record, we mark it invalid */
			sfFlashBuf_setRecordStatus(outRecPtr, RECORD_INVALID);

			/* If we just read the last record from a sector, we erase the sector.*/
			if (outRecPtr % recordsPerSector == recordsPerSector - 1) {
				outSecPtr = (uint16_t)((RECORD_SIZE * (uint32_t)outRecPtr) / (uint32_t)sectorSize);
				sfFlashBuf_eraseSector(outSecPtr);
			}

			outRecPtr = (outRecPtr + 1) % recordCount;

			if (outRecPtr == inRecPtr) {
				empty = true;
			}
		}
	
	}
	
	if (validRecordFound) {
		return true;
	} else {
		*dataLen = 0;
		return false;
	}
}

uint8_t sfFlashBuf_computeChecksum(flashRecord_t *record) {
	return computeCRC((uint8_t *)record, sizeof(*record) - 
			sizeof(record->crc) - sizeof(record->sectorStatus));
}

bool sfFlashBuf_verifyChecksum(flashRecord_t *record) {
	if (record->crc == sfFlashBuf_computeChecksum(record)) {
		return true;
	}
	return false;
}

bool sfFlashBuf_getSectorStatus(uint16_t sectorNum, uint8_t *status) {
	uint32_t addr;
	
	if (!basicInitialized) {
		return false;
	}
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if (sectorNum >= sectorCount) {
			return false;
		}

		/* The sector status byte is stored as the last byte in each sector */
		addr = ((uint32_t)sectorSize * ((uint32_t)sectorNum + 1)) - 1;
		addr += FLASH_ADDRESS_OFFSET;

		if (!spiflash_read(addr, status, 1)) {
			log_error("sfFlashBuf_getSectorStatus() failed!");
			return false;
		}
	}
	
	return true;
}

bool sfFlashBuf_setSectorStatus(uint16_t sectorNum, uint8_t status) {
	uint32_t addr;
	uint8_t byte;
	
	if (!basicInitialized) {
		return false;
	}
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if (sectorNum >= sectorCount) {
			return false;
		}

		/* The sector status byte is stored as the last byte in each sector */
		addr = ((uint32_t)sectorSize * ((uint32_t)sectorNum + 1)) - 1;
		addr += FLASH_ADDRESS_OFFSET;

		spiflash_programByte(addr, status);


		if (!spiflash_read(addr, &byte, 1) || (byte != status)) {
			log_error("sfFlashBuf_setSectorStatus() failed!");
			return false;
		}
	}
	
	return true;
}

bool sfFlashBuf_eraseSector(uint16_t sectorNum) {
	uint32_t addr;
	bool success;
	
	if (!basicInitialized) {
		return false;
	}
	log_warnf("Erasing sector %u...",sectorNum);
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if (sectorNum >= sectorCount) {
			return false;
		}

		addr = (uint32_t)sectorSize * (uint32_t)sectorNum;
		addr += FLASH_ADDRESS_OFFSET;

		success = spiflash_eraseSector(addr);
	}
	
	return success;
}

bool sfFlashBuf_eraseAll() {
	uint16_t sectorNum;
	bool success = true;
	
	if (!basicInitialized) {
		return false;
	}
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		for (sectorNum = 0; sectorNum < sectorCount; sectorNum++) {
			success &= sfFlashBuf_eraseSector(sectorNum);
		}
	}
	
	return success;
}

bool sfFlashBuf_getRecordStatus(uint16_t recordNum, uint8_t *status) {
	uint32_t addr;
	
	if (!basicInitialized) {
		return false;
	}
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if (recordNum >= recordCount) {
			return false;
		}

		addr = RECORD_SIZE * (uint32_t)recordNum;
		addr += FLASH_ADDRESS_OFFSET;

		if (!spiflash_read(addr, status, 1)) {
			log_error("sfFlashBuf_getRecordStatus() failed!");
			return false;
		}
	}
	
	return true;
}

bool sfFlashBuf_setRecordStatus(uint16_t recordNum, uint8_t status) {
	uint32_t addr;
	uint8_t byte;
	
	if (!basicInitialized) {
		return false;
	}	
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if (recordNum >= recordCount) {
			return false;
		}

		addr = RECORD_SIZE * (uint32_t)recordNum;
		addr += FLASH_ADDRESS_OFFSET;

		spiflash_programByte(addr, status);

		if (!spiflash_read(addr, &byte, 1) || (byte != status)) {
			log_error("sfFlashBuf_setRecordStatus() failed!");
			return false;
		}
	}
	
	return true;
}

bool sfFlashBuf_writeRecord(uint16_t recordNum, flashRecord_t *record) {
	uint32_t addr;
	uint8_t readBackBuffer[sizeof(flashRecord_t)];
	uint8_t *recordPtr;
	uint8_t i;
	
	if (!basicInitialized) {
		return false;
	}
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if (recordNum >= recordCount) {
			return false;
		}

		addr = RECORD_SIZE * (uint32_t)recordNum;
		addr += FLASH_ADDRESS_OFFSET;

		/* Make sure that we do not overwrite the sector status byte.  This is only
		 * relevant if we're writing the last record in the sector. */
		if (!spiflash_program(addr, (uint8_t *)record, 
				sizeof(flashRecord_t) - sizeof(record->sectorStatus))) {
			log_error("sfFlashBuf_writeRecord() failed while writing record to flash!");
			return false;
		}

		if (!spiflash_read(addr, readBackBuffer, 
				sizeof(readBackBuffer) - sizeof(record->sectorStatus))) {
			log_error("sfFlashBuf_writeRecord() failed while reading record back from flash!");
			return false;
		}

		recordPtr = (uint8_t *)record;
		for (i=0; i<sizeof(flashRecord_t) - sizeof(record->sectorStatus); i++) {
			if (readBackBuffer[i] != recordPtr[i]) {
				log_error("sfFlashBuf_writeRecord() failed to verify record written to flash!");
				return false;
			}
		}
	}
	
	return true;
}

bool sfFlashBuf_readRecord(uint16_t recordNum, flashRecord_t *record) {
	uint32_t addr;
	
	if (!basicInitialized) {
		return false;
	}	
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if (recordNum >= recordCount) {
			return false;
		}

		addr = RECORD_SIZE * (uint32_t)recordNum;
		addr += FLASH_ADDRESS_OFFSET;

		if (!spiflash_read(addr, (uint8_t *)record, 
				sizeof(*record) - sizeof(record->sectorStatus))) {
			log_error("sfFlashBuf_readRecord() failed!");
			return false;
		}
	}
	
	return true;
}


