/*
 * stoFwd.c
 *
 *  Created on: Aug 3, 2014
 *      Author: kwgilpin
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


#include "nrf52.h"

#include "app_scheduler.h"
#include "app_error.h"

#include "pstorage_platform.h"
#include "pstorage.h"

#include "stoFwdFifo.h"
#include "../utils.h"

#define STOFWD_PAGE_COUNT		(12)

#define STOFWD_BLOCK_SIZE		(8)
#define STOFWD_BLOCK_COUNT		((STOFWD_PAGE_COUNT * PSTORAGE_FLASH_PAGE_SIZE) / STOFWD_BLOCK_SIZE)
#define STOFWD_BLOCKS_PER_PAGE	(PSTORAGE_FLASH_PAGE_SIZE / STOFWD_BLOCK_SIZE)

#define STOFWD_RECORD_SIZE		(8)
#define STOFWD_RECORD_COUNT		((STOFWD_PAGE_COUNT * PSTORAGE_FLASH_PAGE_SIZE) / STOFWD_RECORD_SIZE)
#define STOFWD_RECORDS_PER_PAGE	(PSTORAGE_FLASH_PAGE_SIZE / STOFWD_RECORD_SIZE)

#define DEFAULT_RETRY_COUNT		(5)

typedef enum {
	STOFWD_RECORD_STATUS_EMPTY,
	STOFWD_RECORD_STATUS_ACTIVE,
	STOFWD_RECORD_STATUS_INVALID,
	STOFWD_RECORD_STATUS_SPOILED
} stoFwdFifo_recordStatus_t;

typedef enum {
	STOFWD_OPERATION_NONE,
	STOFWD_OPERATION_INITIALIZE_ERASE,
	STOFWD_OPERATION_PUSH_ERASE,
	STOFWD_OPERATION_PUSH_WRITE,
	STOFWD_OPERATION_POP_OVERWRITE,
	STOFWD_OPERATION_POP_ERASE,
	STOFWD_OPERATION_FLUSH_ERASE
} stoFwdFifo_operation_t;

typedef struct {
	uint8_t op_code;
	uint32_t result;
} pstorage_result_t;

static bool m_initialized = false;
static bool m_busy = false;

static pstorage_handle_t m_pstorage_handle;

static uint32_t m_inRecNum = 0, m_outRecNum = 0;
static bool m_empty = true;

static stoFwdFifo_record_t m_pushRecord;
static stoFwdFifo_record_t m_spoiledRecord;
static stoFwdFifo_operation_t m_operation = STOFWD_OPERATION_NONE;
static int8_t m_retriesRemaining;

static uint32_t m_lostRecordCount = 0;

static void (*m_initCompleteCallback)(bool success) = NULL;
static void (*m_pushCompleteCallback)(bool success) = NULL;
static void (*m_popCompleteCallback)(bool success) = NULL;
static void (*m_flushCompleteCallback)(bool success) = NULL;

static stoFwdFifo_recordStatus_t stoFwd_getRecordStatus(const stoFwdFifo_record_t *record);

static uint32_t stoFwdFifo_eraseBlocks(uint32_t blockStartNum, uint32_t blockCount, bool firstAttempt);
static uint32_t stoFwdFifo_writeBlock(uint32_t blockNum, uint8_t *data, bool firstAttempt);
static uint32_t stoFwdFifo_readBlock(uint32_t blockNum, uint8_t *data);
static bool stoFwdFifo_verifyBlock(uint32_t blockNum, const uint8_t *desiredData);

static void stoFwdFifo_executeInitCompleteCallback(void *p_event_data, uint16_t event_size);
static void stoFwdFifo_pstorageCallback(pstorage_handle_t * p_handle,
		uint8_t op_code, uint32_t result,
		uint8_t *p_data, uint32_t data_len);
void stoFwdFifo_eventCallback(void *p_event_data, uint16_t event_size);


bool stoFwdFifo_init(void (*stoFwdFifo_initCompleteCallback)(bool success)) {
	uint32_t err_code;
	uint32_t recNum, nextRecNum;
	stoFwdFifo_record_t record, nextRecord;
	bool inRecFound = false, outRecFound = false;
	bool notEmptyRecFound = false;
	char str[128];

	if (m_initialized) {
		util_printDebugString("stoFwdFifo_init() failed because store and forward is already initialized\r\n", DEBUG_LEVEL_ERROR);
		return false;
	}

	if (m_busy) {
		util_printDebugString("stoFwdFifo_init() failed because m_busy flag is set\r\n", DEBUG_LEVEL_DETAILED);
		return false;
	}

	/* The busy flag will be cleared once the FIFO is fully initialized. */
	m_busy = true;

	pstorage_module_param_t pstorage_module_param = {	.block_size = STOFWD_RECORD_SIZE,
														.block_count = STOFWD_RECORD_COUNT,
														.cb = stoFwdFifo_pstorageCallback};

	err_code = pstorage_register(&pstorage_module_param, &m_pstorage_handle);
	if (err_code != NRF_SUCCESS) {
		util_printDebugString("stoFwdFifo_init() failed because pstorage_register() failed\r\n", DEBUG_LEVEL_ERROR);
	}
	APP_ERROR_CHECK(err_code);

	/* Create a completely empty record that we'll use as a basis for
	 * comparison with other records that we pull out of the FIFO when
	 * looking for the in and out records.  */
	stoFwdFifo_record_t emptyRecord;
	memset(&emptyRecord, 0xFF, sizeof(emptyRecord));

	for (recNum = 0; recNum < STOFWD_RECORD_COUNT; recNum++) {
		nextRecNum = (recNum + 1) % STOFWD_RECORD_COUNT;

		err_code = stoFwdFifo_readBlock(recNum, (uint8_t *)&record);
		APP_ERROR_CHECK(err_code);

		err_code = stoFwdFifo_readBlock(nextRecNum, (uint8_t *)&nextRecord);
		APP_ERROR_CHECK(err_code);

		/* Test whether the record we just retreived is empty. */
		if (memcmp(&record, &emptyRecord, sizeof(record)) != 0) {
			notEmptyRecFound = true;
		}

		/* If the current record that we're inspecting is not empty but the
		 * next record is empty, we have found the 'in' record, that is the
		 * location where the next element should be placed into the FIFO. */
		if (!inRecFound &&
				(stoFwd_getRecordStatus(&record) != STOFWD_RECORD_STATUS_EMPTY) &&
				(stoFwd_getRecordStatus(&nextRecord) == STOFWD_RECORD_STATUS_EMPTY)) {
			inRecFound = true;
			m_inRecNum = nextRecNum;

			snprintf(str, sizeof(str), "stoFwdFifo_init() found 'in' record: %lu\r\n", m_inRecNum);
			util_printDebugString(str, DEBUG_LEVEL_DETAILED);
		}

		/* The 'out' record is the first active record after a not active
		 * record.  This active record will be the first removed from the FIFO.
		 */
		if (!outRecFound &&
				(stoFwd_getRecordStatus(&record) != STOFWD_RECORD_STATUS_ACTIVE) &&
				(stoFwd_getRecordStatus(&nextRecord) == STOFWD_RECORD_STATUS_ACTIVE)) {
			outRecFound = true;
			m_outRecNum = nextRecNum;

			snprintf(str, sizeof(str), "stoFwdFifo_init() found 'out' record: %lu\r\n", m_outRecNum);
			util_printDebugString(str, DEBUG_LEVEL_DETAILED);
		}

		if (inRecFound && outRecFound) {
			/* If we found both 'in' and 'out' records, the FIFO is not empty,
			 * but nothing further is required to start using it.  To avoid
			 * executing the initCompleteCallback before this function even
			 * returns, we place function that will execute the callback at a
			 * later time onto the scheduler's queue. */
			m_empty = false;

			bool success = true;
			err_code = app_sched_event_put((void *)&success, sizeof(success), stoFwdFifo_executeInitCompleteCallback);
			APP_ERROR_CHECK(err_code);

			return true;
		}
	}

	/* If the code above could not identify both the 'in' record and the
	 * 'out' record... */
	if (notEmptyRecFound) {
		/* and if a non-empty record was found during the search, the FIFO
		 * must be corrupt, so we need to erase it. */
		util_printDebugString("stoFwdFifo_init() found non-empty record, but could not identify both 'in' and 'out' records\r\n", DEBUG_LEVEL_ERROR);
		util_printDebugString("stoFwdFifo_init() queueing erase of entire store and forward FIFO\r\n", DEBUG_LEVEL_DETAILED);

		m_operation = STOFWD_OPERATION_INITIALIZE_ERASE;
		err_code = stoFwdFifo_eraseBlocks(0, STOFWD_BLOCK_COUNT, true);
		APP_ERROR_CHECK(err_code);
	} else {
		/* but if all that was found were empty records, the FIFO is
		 * already empty.  We arbitrarily locate the 'in' and 'out' records
		 * at the beginning of the flash memory reserved for the FIFO. */
		util_printDebugString("stoFwdFifo_init() found FIFO to be empty\r\n", DEBUG_LEVEL_DETAILED);

		m_empty = true;
		m_inRecNum = m_outRecNum = 0;

		bool success = true;
		err_code = app_sched_event_put((void *)&success, sizeof(success), stoFwdFifo_executeInitCompleteCallback);
		APP_ERROR_CHECK(err_code);
	}

	return true;
}

uint32_t stoFwdFifo_getFreeRecordCount() {
	uint32_t occupiedRecordCount;
	uint32_t availableRecordCount;

	if (!m_initialized) {
		util_printDebugString("stoFwdFifo_getFreeRecordCount() failed because store and forward is not initialized\r\n", DEBUG_LEVEL_ERROR);
		return false;
	}

	if (m_busy) {
		util_printDebugString("stoFwdFifo_getFreeRecordCount() failed because m_busy flag is set\r\n", DEBUG_LEVEL_DETAILED);
		return false;
	}

	m_busy = true;
	m_operation = STOFWD_OPERATION_NONE;

	/* The 'in' record always "ahead of" the 'out' record, but because we're
	 * using a circular buffer, the 'in' record's block number is not always
	 * numerically larger than the 'out' record's block number. */

	if (m_inRecNum >= m_outRecNum) {
		/* When the 'in' record has not looped around "behind" the 'out'
		 * record, the amount of occupied space is the difference between the
		 * two pointers.  Therefore, the amount of free space is the total
		 * space minus the occupied space. */
		occupiedRecordCount = m_inRecNum - m_outRecNum;

		/* All records "behind" the 'out' record and also in the same page as
		 * the 'out' record are also unavailable because they cannot be
		 * re-purposed for new data without erasing the entire sector. */
		occupiedRecordCount += m_outRecNum % STOFWD_RECORDS_PER_PAGE;

		availableRecordCount = STOFWD_RECORD_COUNT - occupiedRecordCount;

		/* We always keep one page empty so that the 'in' record cannot
		 * overtake the 'out' record. This empty page is not available
		 * for storage, so we subtract its size. */
		availableRecordCount -= STOFWD_RECORDS_PER_PAGE;
	} else {
		/* When the 'in' record block has looped around the end of the
		 * circular buffer and is now numerically less than the 'out'
		 * index, the remaining free space is the number of bytes between
		 * the 'out' record and the 'in' record. */
		availableRecordCount = m_outRecNum - m_inRecNum;

		/* All records in the same page as the 'out' pointer are also
		 * unavailable because they cannot be re-purposed for new data
		 * without erasing the entire page. */
		availableRecordCount -= m_outRecNum % STOFWD_RECORDS_PER_PAGE;

		/* We always keep one page empty so that the 'in' record cannot
		 * overtake the 'out' record. This empty page is not available
		 * for storage, so we subtract the number of records it contains. */
		availableRecordCount -= STOFWD_RECORDS_PER_PAGE;
	}

	m_operation = STOFWD_OPERATION_NONE;
	m_busy = false;

	return availableRecordCount;
}

uint32_t stoFwdFifo_getActiveRecordCount() {
	uint32_t activeRecordCount;
	uint32_t availableRecordCount;

	if (!m_initialized) {
		util_printDebugString("stoFwdFifo_getActiveRecordCount() failed because store and forward is not initialized\r\n", DEBUG_LEVEL_ERROR);
		return false;
	}

	if (m_busy) {
		util_printDebugString("stoFwdFifo_getActiveRecordCount() failed because m_busy flag is set\r\n", DEBUG_LEVEL_DETAILED);
		return false;
	}

	m_busy = true;
	m_operation = STOFWD_OPERATION_NONE;

	/* The 'in' record always "ahead of" the 'out' record, but because we're
	 * using a circular buffer, the 'in' record's block number is not always
	 * numerically larger than the 'out' record's block number. */

	if (m_inRecNum >= m_outRecNum) {
		/* When the 'in' record has not looped around "behind" the 'out'
		 * record, the number of active records is the difference between the
		 * two record numbers.  */
		activeRecordCount = m_inRecNum - m_outRecNum;
	} else {
		/* When the 'in' record block has looped around the end of the
		 * circular buffer and is now numerically less than the 'out'
		 * index, the remaining free space is the number of bytes between
		 * the 'out' record and the 'in' record. */
		availableRecordCount = m_outRecNum - m_inRecNum;

		/* The number of active records is the total record count minus the
		 * number of still yet available records. */
		activeRecordCount = STOFWD_RECORD_COUNT - availableRecordCount;
	}

	m_operation = STOFWD_OPERATION_NONE;
	m_busy = false;

	return activeRecordCount;
}

uint32_t stoFwdFifo_getLostRecordCount() {
	return m_lostRecordCount;
}

void stoFwdFifo_resetLostRecordCount() {
	m_lostRecordCount = 0;
}

uint32_t stoFwdFifo_getOccupiedSize() {
	return stoFwdFifo_getActiveRecordCount() * STOFWD_RECORD_SIZE;
}

uint32_t stoFwdFifo_getTotalSize() {
	return STOFWD_PAGE_COUNT * PSTORAGE_FLASH_PAGE_SIZE;
}

uint32_t stoFwdFifo_getInRecNumber() {
	return m_inRecNum;
}

uint32_t stoFwdFifo_getOutRecNumber() {
	return m_outRecNum;
}

bool stoFwdFifo_push(const stoFwdFifo_record_t *record, void (*stoFwdFifo_pushCompleteCallback)(bool success)) {
	uint32_t err_code;

	uint32_t inPageNum, outPageNum;

	if (!m_initialized) {
		util_printDebugString("stoFwdFifo_push() failed because store and forward is not initialized\r\n", DEBUG_LEVEL_ERROR);
		return false;
	}

	if (m_busy) {
		util_printDebugString("stoFwdFifo_push() failed because m_busy flag is set\r\n", DEBUG_LEVEL_DETAILED);
		return false;
	}

	if ((record->status & 0x7F) == 0x00) {
		/* If none of the bits which signal what type of data the record
		 * contains are set, we assume that the record does not actually
		 * contain any data, so we return false with pushing the record to the
		 * FIFO. */
		util_printDebugString("stdFwdFifo_push() failed because record's status field indicates that it contains no data\r\n", DEBUG_LEVEL_DETAILED);
		return false;
	}

	m_busy = true;

	m_pushCompleteCallback = stoFwdFifo_pushCompleteCallback;

	/* Make a copy of the record that the caller wishes to push.  The data we
	 * pass to the flash write function must persist after the write call,
	 * until the write complete callback is executed. */
	m_pushRecord = *record;

	/* Clear the MSb in the status field to mark the record as active */
	m_pushRecord.status &= ~(0x80);

	/* Compute the flash pages (minimum eraseable unit) in which the 'in' and
	 * 'out' records are located. */
	inPageNum = (STOFWD_RECORD_SIZE * m_inRecNum) / PSTORAGE_FLASH_PAGE_SIZE;
	outPageNum = (STOFWD_RECORD_SIZE * m_outRecNum) / PSTORAGE_FLASH_PAGE_SIZE;

	if ((inPageNum + 1) % STOFWD_PAGE_COUNT == outPageNum) {
		/* If the page following the page into which the new record(s) will
		 * be written is the page which currently contains the out pointer,
		 * we need to bump the out pointer into the subsequent page and erase
		 * the page which used to contain the out pointer so that we always
		 * maintain one erased page between the in and out pointers. */

		m_operation = STOFWD_OPERATION_PUSH_ERASE;

		util_printDebugString("stoFwdFifo_push() queueing erase of page that contained the 'out' pointer in order to keep a blank page separating the 'in' and 'out' pointers\r\n", DEBUG_LEVEL_DETAILED);

		/* Issue the erase command.  It will return immediately, but the actual
		 * erase operation will be queued.  When it is complete, the
		 * stoFwd_pstorageCallback() function will be executed by the
		 * scheduler.  Look there for the code which actually writes the new
		 * record to flash.  */
		err_code = stoFwdFifo_eraseBlocks(outPageNum * STOFWD_BLOCKS_PER_PAGE, STOFWD_BLOCKS_PER_PAGE, true);
		APP_ERROR_CHECK(err_code);

		return true;
	}

	/* At this point, we know that writing a new record to flash will not
	 * compromise the empty page between the page containing the 'in' pointer
	 * and the 'out' pointer, so we can go ahead and write the data. */

	m_operation = STOFWD_OPERATION_PUSH_WRITE;

	util_printDebugString("stoFwdFifo_push() queueing write of new record to flash\r\n", DEBUG_LEVEL_DETAILED);

	/* Queue the write operation.  This will return immediately, but the
	 * write will not be complete until the stoFwd_pstorageCallback is
	 * executed.  Look there for the remainder of the code which pushes a new
	 * record to the FIFO. */
	err_code = stoFwdFifo_writeBlock(m_inRecNum, (uint8_t *)&m_pushRecord, true);
	APP_ERROR_CHECK(err_code);

	return true;
}

bool stoFwdFifo_peek(stoFwdFifo_record_t *record) {
	uint32_t err_code;

	if (!m_initialized) {
		util_printDebugString("stoFwdFifo_peek() failed because store and forward is not initialized\r\n", DEBUG_LEVEL_ERROR);
		return false;
	}

	if (m_busy) {
		util_printDebugString("stoFwdFifo_peek() failed because m_busy flag is set\r\n", DEBUG_LEVEL_DETAILED);
		return false;
	}

	if (m_empty) {
		util_printDebugString("stoFwdFifo_peek() failed because store and forward FIFO is empty\r\n", DEBUG_LEVEL_DETAILED);
		return false;
	}

	err_code = stoFwdFifo_readBlock(m_outRecNum, (uint8_t *)record);
	APP_ERROR_CHECK(err_code);

	util_printDebugString("stoFwdFifo_peek() successful\r\n", DEBUG_LEVEL_DETAILED);

	return true;
}

bool stoFwdFifo_pop(stoFwdFifo_record_t *record, void (*stoFwdFifo_popCompleteCallback)(bool success)) {
	uint32_t err_code;

	if (!m_initialized) {
		util_printDebugString("stoFwdFifo_pop() failed because store and forward is not initialized\r\n", DEBUG_LEVEL_ERROR);
		return false;
	}

	if (m_busy) {
		util_printDebugString("stoFwdFifo_pop() failed because m_busy flag is set\r\n", DEBUG_LEVEL_DETAILED);
		return false;
	}

	if (m_empty) {
		util_printDebugString("stoFwdFifo_pop() failed because store and forward FIFO is empty\r\n", DEBUG_LEVEL_DETAILED);
		return false;
	}

	m_busy = true;

	m_popCompleteCallback = stoFwdFifo_popCompleteCallback;

	/* Read the 'out' record from flash */
	err_code = stoFwdFifo_readBlock(m_outRecNum, (uint8_t *)record);
	APP_ERROR_CHECK(err_code);

	/* Make a copy of the record that we just read from flash but set its
	 * status field to 0x00 to indicate that the record is no longer valid,
	 * i.e. that it is spoiled and that the block which contains it must be
	 * erased before it can be used again. */
	m_spoiledRecord = *record;
	m_spoiledRecord.status = 0x00;

	/* Queue the write operation which will over-write the 'out' record with
	 * the spoiled record.  This should return immediately, but the write will
	 * not be complete until the stoFwd_pstorageCallback is executed.  Look
	 * there for the remainder of the code. */
	util_printDebugString("stoFwdFifo_pop() queueing over-write of popped record to flash\r\n", DEBUG_LEVEL_DETAILED);

	m_operation = STOFWD_OPERATION_POP_OVERWRITE;
	err_code = stoFwdFifo_writeBlock(m_outRecNum, (uint8_t *)&m_spoiledRecord, true);
	APP_ERROR_CHECK(err_code);

	return true;
}

bool stoFwdFifo_flush(void (*stoFwdFifo_flushCompleteCallback)(bool success)) {
	uint32_t err_code;

	if (!m_initialized) {
		util_printDebugString("stoFwdFifo_flush() failed because store and forward is not initialized\r\n", DEBUG_LEVEL_ERROR);
		return false;
	}

	if (m_busy) {
		util_printDebugString("stoFwdFifo_flush() failed because m_busy flag is set\r\n", DEBUG_LEVEL_DETAILED);
		return false;
	}

	m_busy = true;

	m_flushCompleteCallback = stoFwdFifo_flushCompleteCallback;

	util_printDebugString("stoFwdFifo_flush() queueing erase of entire store and forward FIFO\r\n", DEBUG_LEVEL_DETAILED);

	m_operation = STOFWD_OPERATION_FLUSH_ERASE;
	err_code = stoFwdFifo_eraseBlocks(0, STOFWD_BLOCK_COUNT, true);
	APP_ERROR_CHECK(err_code);

	return true;
}

stoFwdFifo_recordStatus_t stoFwd_getRecordStatus(const stoFwdFifo_record_t *record) {
	if (record->status == 0xFF) {
		/* The flash is erased to 0xFF, so we assume that if the status byte
		 * is 0xFF, the rest of the record is empty. */
		return STOFWD_RECORD_STATUS_EMPTY;
	} else if (record->status == 0x00) {
		/* Bits in flash memory cannot be set to 1 except by erasing the flash.
		 * Bits can only be cleared to 0.  So to mark a record as spoiled, to
		 * indicate that the data it contains is no longer needed, we clear all
		 * bits in the status field. */
		return STOFWD_RECORD_STATUS_SPOILED;
	} else if (record->status & 0x80) {
		/* The MSb should only remain set while the record is empty.  If the
		 * status is not 0xFF, indicating that the record is not empty, but the
		 * MSb is still set, something is wrong with the record. */
		return STOFWD_RECORD_STATUS_INVALID;
	} else {
		return STOFWD_RECORD_STATUS_ACTIVE;
	}
}

uint32_t stoFwdFifo_eraseBlocks(uint32_t blockStartNum, uint32_t blockCount, bool firstAttempt) {
	uint32_t err_code;
	pstorage_handle_t blockID;

	err_code = pstorage_block_identifier_get(&m_pstorage_handle, blockStartNum, &blockID);
	if (err_code != NRF_SUCCESS) {
		return err_code;
	}

	if (firstAttempt) {
		m_retriesRemaining = DEFAULT_RETRY_COUNT;
	}

	return pstorage_clear(&blockID, blockCount * STOFWD_BLOCK_SIZE);
}

uint32_t stoFwdFifo_writeBlock(uint32_t blockNum, uint8_t *data, bool firstAttempt) {
	uint32_t err_code;
	pstorage_handle_t blockID;

	err_code = pstorage_block_identifier_get(&m_pstorage_handle, blockNum, &blockID);
	if (err_code != NRF_SUCCESS) {
		return err_code;
	}

	if (firstAttempt) {
		m_retriesRemaining = DEFAULT_RETRY_COUNT;
	}

	return pstorage_store(&blockID, data, STOFWD_BLOCK_SIZE, 0);
}

uint32_t stoFwdFifo_readBlock(uint32_t blockNum, uint8_t *data) {
	uint32_t err_code;
	pstorage_handle_t blockID;

	err_code = pstorage_block_identifier_get(&m_pstorage_handle, blockNum, &blockID);
	if (err_code != NRF_SUCCESS) {
		return err_code;
	}

	return pstorage_load(data, &blockID, STOFWD_BLOCK_SIZE, 0);
}

bool stoFwdFifo_verifyBlock(uint32_t blockNum, const uint8_t *desiredData) {
	uint32_t err_code;
	pstorage_handle_t blockID;
	uint8_t actualData[STOFWD_BLOCK_SIZE];

	err_code = pstorage_block_identifier_get(&m_pstorage_handle, blockNum, &blockID);
	APP_ERROR_CHECK(err_code);

	err_code = pstorage_load(actualData, &blockID, sizeof(actualData), 0);
	APP_ERROR_CHECK(err_code);

	if (memcmp(actualData, desiredData, sizeof(actualData)) == 0) {
		return true;
	}

	return false;
}

void stoFwdFifo_executeInitCompleteCallback(void *p_event_data, uint16_t event_size) {
	/* This function exists in order to delay the execution of the
	 * initCompleteCallback until after the stoFwd_init() function has
	 * returned.  In case that the stoFwd FIFO is ready to use as soon as the
	 * stoFwd_init function is called, this function is placed into the
	 * scheduler's event queue to be called later.  When it is called, all it
	 * does is execute the callback. */
	m_initialized = true;

	m_operation = STOFWD_OPERATION_NONE;
	m_busy = false;

	util_printDebugString("stoFwdFifo_init() complete\r\n", DEBUG_LEVEL_DETAILED);

	if (m_initCompleteCallback != NULL) {
		m_initCompleteCallback(*(bool *)p_event_data);
	}
}

void stoFwdFifo_pstorageCallback(pstorage_handle_t * p_handle,
		uint8_t op_code, uint32_t result,
		uint8_t *p_data, uint32_t data_len) {
	uint32_t err_code;
	pstorage_result_t pstorage_result;

	pstorage_result.op_code = op_code;
	pstorage_result.result = result;

	err_code = app_sched_event_put(&pstorage_result, sizeof(pstorage_result), stoFwdFifo_eventCallback);
	APP_ERROR_CHECK(err_code);
}

void stoFwdFifo_eventCallback(void *p_event_data, uint16_t event_size) {
	uint32_t err_code;
	bool success;
	char str[256];
	pstorage_result_t *p_pstorage_result;
	uint32_t result;
	uint8_t op_code;

	p_pstorage_result = (pstorage_result_t *)p_event_data;

	op_code = p_pstorage_result->op_code;
	result = p_pstorage_result->result;

	if (m_operation == STOFWD_OPERATION_INITIALIZE_ERASE) {
		if (op_code == PSTORAGE_CLEAR_OP_CODE) {
			if (result == NRF_SUCCESS) {
				/* If we were initializing the FIFO and the flash controller
				 * successfully cleared memory reserved for the FIFO, the FIFO
				 * is now empty and ready to use. */
				util_printDebugString("stoFwdFifo_init() erase operation completed successfully\r\n", DEBUG_LEVEL_DETAILED);

				m_empty = true;
				m_inRecNum = m_outRecNum = 0;

				m_initialized = true;
				success = true;
			} else {
				/* If the flash controller failed to complete a clear operation,
				 * something has gone wrong. */
				snprintf(str, sizeof(str), "stoFwdFifo_init() erase operation failed with result 0x%lx\r\n", result);
				util_printDebugString(str, DEBUG_LEVEL_ERROR);

				if (--m_retriesRemaining > 0) {
					snprintf(str, sizeof(str), "stoFwdFifo_init() re-queueing erase of entire store and forward FIFO (remaining retries: %d)\r\n", m_retriesRemaining);
					util_printDebugString(str, DEBUG_LEVEL_DETAILED);
					err_code = stoFwdFifo_eraseBlocks(0, STOFWD_BLOCK_COUNT, false);
					APP_ERROR_CHECK(err_code);
					return;
				} else {
					/* We indicate that the FIFO is not
					 * initialized before executing the callback with the success
					 * argument set to false. */
					m_initialized = false;
					success = false;
				}
			}
		} else {
			/* If we've been notified that some operation other than clear has
			 * just succeeded/failed, something has gone wrong as no other
			 * operation should have been pending. */
			snprintf(str, sizeof(str), "stoFwdFifo_init() received unexpected op_code/result (0x%02x / 0x%lx) while waiting for erase operation to complete\r\n", op_code, result);
			util_printDebugString(str, DEBUG_LEVEL_ERROR);

			m_initialized = false;
			success = false;
		}

		m_operation = STOFWD_OPERATION_NONE;
		m_busy = false;

		if (success) {
			util_printDebugString("stoFwdFifo_init() complete\r\n", DEBUG_LEVEL_DETAILED);
		} else {
			util_printDebugString("stoFwdFifo_init() failed\r\n", DEBUG_LEVEL_ERROR);
		}

		if (m_initCompleteCallback != NULL) {
			m_initCompleteCallback(success);
		}

		return;
	} else if (m_operation == STOFWD_OPERATION_PUSH_ERASE) {
		if (op_code == PSTORAGE_CLEAR_OP_CODE) {
			if (result == NRF_SUCCESS) {
				/* If we are attempting to push a new record to the FIFO and the
				 * pstorage controller indicates that it just completed a clear
				 * operation successfully, we must have been erasing the page that
				 * contained the 'out' pointer in order to maintain a complete
				 * empty page between the page containing the 'in' pointer and the
				 * page containing the 'out' pointer. */
				util_printDebugString("stoFwdFifo_push() erase operation completed successfully\r\n", DEBUG_LEVEL_DETAILED);

				/* Since we just erased the page containing the 'out' pointer, we
				 * have to adjust the 'out' pointer to point to the first record of
				 * the subsequent page. */
				uint32_t outPageNum = (STOFWD_RECORD_SIZE * m_outRecNum) / PSTORAGE_FLASH_PAGE_SIZE;
				outPageNum = (outPageNum + 1) % STOFWD_PAGE_COUNT;
				m_outRecNum = outPageNum * STOFWD_RECORDS_PER_PAGE;

				/* Having just erased a page full of valid measurements, we
				 * increment the lost record count (assuming its not already
				 * maximized. */
				if (m_lostRecordCount <= (UINT32_MAX - (PSTORAGE_FLASH_PAGE_SIZE / STOFWD_RECORD_SIZE))) {
					m_lostRecordCount += PSTORAGE_FLASH_PAGE_SIZE / STOFWD_RECORD_SIZE;
				} else {
					m_lostRecordCount = UINT32_MAX;
				}

				/* Now we can finally write the new record to the freshly erased
				 * page.  To do so, we first compute the ID of the block that we
				 * wish to write with the new record. */

				/* Queue the write operation.  This should return immediately, but
				 * the write will not be complete until the stoFwd_pstorageCallback
				 * is executed again with the op_code parameter set to *_STORE_*.
				 * Look below the remainder of the code which updates the 'in'
				 * pointer and informs the caller that the push operation is
				 * complete.*/
				util_printDebugString("stoFwdFifo_push() queueing write of new record to flash\r\n", DEBUG_LEVEL_DETAILED);

				m_operation = STOFWD_OPERATION_PUSH_WRITE;
				err_code = stoFwdFifo_writeBlock(m_inRecNum, (uint8_t *)&m_pushRecord, true);
				APP_ERROR_CHECK(err_code);

				/* Return now and wait for the next callback indicating that
				 * the write operation is complete.  */
				return;
			} else {
				snprintf(str, sizeof(str), "stoFwdFifo_push() erase operation failed with result 0x%lx\r\n", result);
				util_printDebugString(str, DEBUG_LEVEL_ERROR);

				if (--m_retriesRemaining > 0) {
					snprintf(str, sizeof(str), "stoFwdFifo_push() re-queueing erase of page that contained the 'out' pointer in order to keep a blank page separating the 'in' and 'out' pointers (remaining retries: %d)\r\n", m_retriesRemaining);
					util_printDebugString(str, DEBUG_LEVEL_DETAILED);

					uint32_t outPageNum = (STOFWD_RECORD_SIZE * m_outRecNum) / PSTORAGE_FLASH_PAGE_SIZE;
					err_code = stoFwdFifo_eraseBlocks(outPageNum * STOFWD_BLOCKS_PER_PAGE, STOFWD_BLOCKS_PER_PAGE, false);
					APP_ERROR_CHECK(err_code);
					return;
				} else {
					success = false;
				}
			}
		} else {
			/* If we've been notified that some operation other than clear has
			 * just completed, something has gone wrong as no other type of
			 * operation should have been pending. */
			snprintf(str, sizeof(str), "stoFwdFifo_push() received unexpected op_code/result (0x%02x / 0x%lx) while waiting for erase operation to complete\r\n", op_code, result);
			util_printDebugString(str, DEBUG_LEVEL_ERROR);

			success = false;
		}

		m_operation = STOFWD_OPERATION_NONE;
		m_busy = false;

		if (success) {
			util_printDebugString("stoFwdFifo_push() complete\r\n", DEBUG_LEVEL_DETAILED);
		} else {
			util_printDebugString("stoFwdFifo_push() failed\r\n", DEBUG_LEVEL_ERROR);
		}

		if (m_pushCompleteCallback != NULL) {
			m_pushCompleteCallback(success);
		}
	} else if (m_operation == STOFWD_OPERATION_PUSH_WRITE) {
		if (op_code == PSTORAGE_STORE_OP_CODE) {
			if (result == NRF_SUCCESS) {
				/* If we are attempting to push a new record to the FIFO and the
				 * pstorage controller indicates that it just completed a write
				 * operation successfully, the newest record must have been
				 * written.  We'll update the 'in' pointer, verify the data, and
				 * then inform the caller that the push operation was successful
				 * (or not). */
				util_printDebugString("stoFwdFifo_push() write operation completed successfully\r\n", DEBUG_LEVEL_DETAILED);

				bool verifyOK = stoFwdFifo_verifyBlock(m_inRecNum, (uint8_t *)&m_pushRecord);
				if (verifyOK) {
					util_printDebugString("stoFwdFifo_push() successfully verified the data written to the FIFO\r\n", DEBUG_LEVEL_DETAILED);
					success = true;
				} else {
					util_printDebugString("stoFwdFifo_push() failed to verify the data written to the FIFO\r\n", DEBUG_LEVEL_ERROR);
					success = false;
				}

				m_inRecNum = (m_inRecNum + 1) % STOFWD_RECORD_COUNT;

				/* Since we just wrote a record, the FIFO cannot be empty. */
				m_empty = false;
			} else {
				snprintf(str, sizeof(str), "stoFwdFifo_push() write operation failed with result 0x%lx\r\n", result);
				util_printDebugString(str, DEBUG_LEVEL_ERROR);

				if (--m_retriesRemaining > 0) {
					snprintf(str, sizeof(str), "stoFwdFifo_push() re-queueing write of new record to flash (remaining retries: %d)\r\n", m_retriesRemaining);
					util_printDebugString(str, DEBUG_LEVEL_DETAILED);
					err_code = stoFwdFifo_writeBlock(m_inRecNum, (uint8_t *)&m_pushRecord, false);
					APP_ERROR_CHECK(err_code);
					return;
				} else {
					success = false;					;
				}
			}
		} else {
			/* If we've been notified that some operation other than store has
			 * just complete, something has gone wrong as no other type of
			 * operation should have been pending. */
			snprintf(str, sizeof(str), "stoFwdFifo_push() received unexpected op_code/result (0x%02x / 0x%lx) while waiting for write operation to complete\r\n", op_code, result);
			util_printDebugString(str, DEBUG_LEVEL_ERROR);

			success = false;
		}

		m_operation = STOFWD_OPERATION_NONE;
		m_busy = false;

		if (success) {
			util_printDebugString("stoFwdFifo_push() complete\r\n", DEBUG_LEVEL_DETAILED);
		} else {
			util_printDebugString("stoFwdFifo_push() failed\r\n", DEBUG_LEVEL_ERROR);
		}

		if (m_pushCompleteCallback != NULL) {
			m_pushCompleteCallback(success);
		}
	} else if (m_operation == STOFWD_OPERATION_POP_OVERWRITE) {
		if (op_code == PSTORAGE_STORE_OP_CODE) {
			if (result == NRF_SUCCESS) {
				/* If we are attempting to pop the oldest record to the FIFO
				 * and the pstorage controller indicates that it just completed
				 * a write operation successfully, the oldest record must have
				 * been updated so that its status field now indicates that the
				 * record is spoiled.  We'll verify the status field, update the
				 * 'out' pointer, and either erase the page that contained the
				 * popped record if it is now completely spoiled or
				 * alternatively inform the caller that the pop operation was
				 * successful. */
				util_printDebugString("stoFwdFifo_pop() over-write operation completed\r\n", DEBUG_LEVEL_DETAILED);
			} else {
				snprintf(str, sizeof(str), "stoFwdFifo_pop() over-write operation failed with result 0x%lx\r\n", result);
				util_printDebugString(str, DEBUG_LEVEL_ERROR);

				if (--m_retriesRemaining > 0) {
					snprintf(str, sizeof(str), "stoFwdFifo_pop() re-queueing over-write of popped record to flash (remaining retries: %d)\r\n", m_retriesRemaining);
					util_printDebugString(str, DEBUG_LEVEL_DETAILED);
					err_code = stoFwdFifo_writeBlock(m_outRecNum, (uint8_t *)&m_spoiledRecord, false);
					APP_ERROR_CHECK(err_code);
					return;
				} else {
					;
				}
			}

			/* Read-back the data that we just finished writing to flash
			 * and compare it to the spoiled record that was initially
			 * passed to the store function. */
			bool verifyOK = stoFwdFifo_verifyBlock(m_outRecNum, (uint8_t *)&m_spoiledRecord);

			if (verifyOK) {
				util_printDebugString("stoFwdFifo_pop() verified that the popped record was marked as spoiled\r\n", DEBUG_LEVEL_DETAILED);
			} else {
				util_printDebugString("stoFwdFifo_pop() failed to mark the popped record as spoiled\r\n", DEBUG_LEVEL_ERROR);
			}

			if (m_outRecNum % STOFWD_RECORDS_PER_PAGE == STOFWD_RECORDS_PER_PAGE - 1) {
				/* If the 'out' record was the last record of a page, we
				 * need to erase that page in order to free it for re-use.
				 * While this function will return immediately, the erase
				 * will not be complete until the stoFwd_pstorageCallback
				 * is executed a second time. */
				util_printDebugString("stoFwdFifo_pop() queueing erase of spoiled page\r\n", DEBUG_LEVEL_DETAILED);

				m_operation = STOFWD_OPERATION_POP_ERASE;
				err_code = stoFwdFifo_eraseBlocks(m_outRecNum - (STOFWD_RECORDS_PER_PAGE - 1), STOFWD_RECORDS_PER_PAGE, true);
				APP_ERROR_CHECK(err_code);

				/* Return now before updating the 'out' block. We'll do
				 * that after the page has been erased. */
				return;
			}


			/* At this point, we know that we do not need to erase a
			 * spoiled page as the page that contained the 'out' block
			 * still contains valid records. */

			if (verifyOK) {
				/* If we verified that the old 'out' record was marked as
				 * spoiled, we go ahead and update the out record block number.
				 * If the old out record was not marked as spoiled, we do not
				 * update the out record as the pop operation failed.  */
				m_outRecNum = (m_outRecNum + 1) % STOFWD_RECORD_COUNT;

				/* If the out record has caught up to the in record, the FIFO is now empty. */
				if (m_outRecNum == m_inRecNum) {
					m_empty = true;
				}

				success = true;
			} else {
				success = false;
			}
		} else {
			/* If we've been notified that some operation other than store has
			 * just completed, something has gone wrong as we had only queued
			 * an (over-)write operation.  We do not update the out block
			 * number as presumably we failed to mark it as spoiled. */
			snprintf(str, sizeof(str), "stoFwdFifo_pop() received unexpected op_code/result (0x%02x / 0x%lx) while waiting for over-write operation to complete\r\n", op_code, result);
			util_printDebugString(str, DEBUG_LEVEL_ERROR);

			success = false;
		}


		m_operation = STOFWD_OPERATION_NONE;
		m_busy = false;

		if (success) {
			util_printDebugString("stoFwdFifo_pop() complete\r\n", DEBUG_LEVEL_DETAILED);
		} else {
			util_printDebugString("stoFwdFifo_pop() failed\r\n", DEBUG_LEVEL_ERROR);
		}

		if (m_popCompleteCallback != NULL) {
			m_popCompleteCallback(success);
		}
	} else if (m_operation == STOFWD_OPERATION_POP_ERASE) {
		if (op_code == PSTORAGE_CLEAR_OP_CODE) {
			if (result == NRF_SUCCESS) {
				/* Erase of the spoiled page was successful */
				util_printDebugString("stoFwdFifo_pop() erase spoiled page operation completed successfully\r\n", DEBUG_LEVEL_DETAILED);

				/* Update the 'out' record number */
				m_outRecNum = (m_outRecNum + 1) % STOFWD_RECORD_COUNT;

				/* If the out record has caught up to the in record, the FIFO is now empty. */
				if (m_outRecNum == m_inRecNum) {
					m_empty = true;
				}

				success = true;
			} else {
				/* Erase of spoiled page failed.  We will not update the out
				 * record pointer. */
				snprintf(str, sizeof(str), "stoFwdFifo_pop() erase spoiled page operation failed with result 0x%lx\r\n", result);
				util_printDebugString(str, DEBUG_LEVEL_ERROR);

				if (--m_retriesRemaining > 0) {
					snprintf(str, sizeof(str), "stoFwdFifo_pop() re-queueing erase of spoiled page\r\n (remaining retries: %d)\r\n", m_retriesRemaining);
					util_printDebugString(str, DEBUG_LEVEL_DETAILED);
					err_code = stoFwdFifo_eraseBlocks(m_outRecNum - (STOFWD_RECORDS_PER_PAGE - 1), STOFWD_RECORDS_PER_PAGE, false);
					APP_ERROR_CHECK(err_code);
					return;
				} else {
					success = false;
				}
			}
		} else {
			/* If we've been notified that some operation other than clear has
			 * just completed, something has gone wrong as we had only queued
			 * a clear operation.  We do not update the out block number as
			 * presumably we failed to mark it as spoiled. */
			snprintf(str, sizeof(str), "stoFwdFifo_pop() received unexpected op_code/result (0x%02x / 0x%lx) while waiting for spoiled page erase operation to complete\r\n", op_code, result);
			util_printDebugString(str, DEBUG_LEVEL_ERROR);

			success = false;
		}

		m_operation = STOFWD_OPERATION_NONE;
		m_busy = false;

		if (success) {
			util_printDebugString("stoFwdFifo_pop() complete\r\n", DEBUG_LEVEL_DETAILED);
		} else {
			util_printDebugString("stoFwdFifo_pop() failed\r\n", DEBUG_LEVEL_ERROR);
		}

		if (m_popCompleteCallback != NULL) {
			m_popCompleteCallback(success);
		}
	} else if (m_operation == STOFWD_OPERATION_FLUSH_ERASE) {
		if (op_code == PSTORAGE_CLEAR_OP_CODE) {
			if (result == NRF_SUCCESS) {
				/* If we successfully erased the entire FIFO... */
				util_printDebugString("stoFwdFifo_flush() operation completed successfully\r\n", DEBUG_LEVEL_DETAILED);

				m_empty = true;
				m_inRecNum = m_outRecNum = 0;

				success = true;
			} else {
				/* Erase of the FIFO failed.  We'll leave the in and out block
				 * numbers unchanged. */
				snprintf(str, sizeof(str), "stoFwdFifo_flush() erase operation failed with result 0x%lx\r\n", result);
				util_printDebugString(str, DEBUG_LEVEL_ERROR);

				if (--m_retriesRemaining > 0) {
					snprintf(str, sizeof(str), "stoFwdFifo_flush() re-queueing erase of entire store and forward FIFO (remaining retries: %d)\r\n", m_retriesRemaining);
					util_printDebugString(str, DEBUG_LEVEL_DETAILED);
					err_code = stoFwdFifo_eraseBlocks(0, STOFWD_BLOCK_COUNT, false);
					APP_ERROR_CHECK(err_code);
					return;
				} else {
					success = false;
				}
			}
		} else {
			/* If we've been notified that some operation other than clear has
			 * just completed, something has gone wrong as we had only queued
			 * a clear operation.  We do not update the out block number as
			 * presumably we failed to mark it as spoiled. */
			snprintf(str, sizeof(str), "stoFwdFifo_flush() received unexpected op_code/result (0x%02x / 0x%lx) while waiting for erase operation to complete\r\n", op_code, result);
			util_printDebugString(str, DEBUG_LEVEL_ERROR);

			success = false;
		}

		m_operation = STOFWD_OPERATION_NONE;
		m_busy = false;

		if (success) {
			util_printDebugString("stoFwdFifo_flush() complete\r\n", DEBUG_LEVEL_DETAILED);
		} else {
			util_printDebugString("stoFwdFifo_flush() failed\r\n", DEBUG_LEVEL_ERROR);
		}

		if (m_flushCompleteCallback != NULL) {
			m_flushCompleteCallback(success);
		}
	} else {
		snprintf(str, sizeof(str), "stoFwdFifo_pstorageCallback() executed with unexpected value of m_operation: 0x%x\r\n", m_operation);
		util_printDebugString(str, DEBUG_LEVEL_ERROR);
	}
}

