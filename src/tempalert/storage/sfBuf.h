/* 
 * File:   sfBuf.h
 * Author: kwgilpin
 *
 * Created on January 7, 2013, 11:11 PM
 */

#ifndef SFBUF_H
#define	SFBUF_H

#include <stdint.h>
#include <stdbool.h> 

#include "globals.h"

typedef uint16_t sfBufSize_t;
typedef uint8_t sfBufItemSize_t;

#define SFBUF_ITEM_SIZE_MAX 127

typedef struct {
	volatile bool full;
	volatile bool empty;
	volatile sfBufSize_t bbIndex; // Bottom/back
	volatile sfBufSize_t tfIndex; // Top/front
	sfBufSize_t dataSize;
	volatile uint8_t data[];
} sfBuf_t;


bool sfBuf_recoverPostReset(const sfBuf_t *buf, resetReason_t resetReason);

bool sfBuf_init(void *memory, sfBufSize_t memSize);

uint32_t sfBuf_getMaxItemSize(void);
bool sfBuf_isEmpty(sfBuf_t *buf);
uint32_t sfBuf_countItems(sfBuf_t *buf);
sfBufSize_t sfBuf_getAvailSpace(sfBuf_t *buf);

bool sfBuf_validate(const sfBuf_t *buf);
void sfBuf_flush(sfBuf_t *buf);

bool sfBuf_discardBB(sfBuf_t *buf);
bool sfBuf_discardTF(sfBuf_t *buf);

bool sfBuf_forcePushBB(sfBuf_t *buf, const void *item, sfBufItemSize_t size);
bool sfBuf_forcePushTF(sfBuf_t *buf, const void *item, sfBufItemSize_t size);

bool sfBuf_tryPushBB(sfBuf_t *buf, const void *item, sfBufItemSize_t size);
bool sfBuf_tryPushTF(sfBuf_t *buf, const void *item, sfBufItemSize_t size);

bool sfBuf_popBB(sfBuf_t *buf, void *item, sfBufItemSize_t *sizePtr);
bool sfBuf_popTF(sfBuf_t *buf, void *item, sfBufItemSize_t *sizePtr);

bool sfBuf_peekBB(sfBuf_t *buf, void *item, sfBufItemSize_t *sizePtr);
bool sfBuf_peekTF(sfBuf_t *buf, void *item, sfBufItemSize_t *sizePtr);

#endif	/* SFBUF_H */

