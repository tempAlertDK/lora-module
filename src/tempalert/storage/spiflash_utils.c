/*
 * flash.c
 *
 *  Created on: Mar 10, 2017
 *      Author: Nate-Tempalert
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "nordic_common.h"
#include "nrf.h"
#include "nrf_delay.h"

#include "app_util_platform.h"

#include "FreeRTOS.h"
#include "task.h"

#include "boards.h"

#include "spi_mtx.h"
#include "spi.h"
#include "log.h"
#include "utils.h"

#include "spiflash_utils.h"

#define MFG_ID 		0xC2

#define MX25R6435F_DEVICEID 				0x17
#define MX25R6435F_SIZE_BYTES 			8388608
#define MX25R6435F_SECTOR_SIZE_BYTES 	4096
#define MX25R6435F_TBP_USEC 				21

#define VALIDATE_ADDRESS(ADDR)  	if ((ADDR) > MAX_ADDRESS) { \
		LOGGER(0, "ERROR: Invlid write address (%0x%08x) exceeds MAX_ADDRESS (%0x%08x)!  [%s:%u]\r\n",\
			(ADDR), MAX_ADDRESS,__FUNCTION__,__LINE__); \
		return false; \
		}

// DATA COMMANDS
#define PROGRAM_PAGE_COMMAND		0x02
#define ERASE_SECTOR_COMMAND		0x20
#define ERASE_BLOCK_COMMAND		0x52
#define READ_DATA_COMMAND		0x03

// CONTROL COMMANDS
#define DEEP_SLEEP_COMMAND		0xB9
#define READ_STATUS_COMMAND		0x05
#define ENABLE_WRITE_COMMAND		0x06
#define DISABLE_WRITE_COMMAND	0x04


#define 		WIP_BIT 					(1 << 0)
#define 		WEL_BIT 					(1 << 1)
#define		READWRITE_CMD_LENGTH		4
#define 		MAX_READWRITE_LENGTH 	256 - READWRITE_CMD_LENGTH

static bool spiflash_enableWrite(void);
//static bool spiflash_disableWrite(void);

static void spiflash_reset(void);
static inline void spiflash_assert_cs(void);
static inline void spiflash_deassert_cs(void);

static bool initialized = false;
static uint8_t deviceID;

static uint32_t sizeBytes; // Size of the flash chip (in bytes)
static uint32_t sectorSizeBytes; // Size of the minimum erasable unit
//static uint8_t tbp_usec; // Byte programming time (in usec)

static const nrf_drv_spi_config_t spiflash_spi_config = {
		.sck_pin = SCLK_PIN,
		.mosi_pin = MOSI_PIN,
		.miso_pin = MISO_PIN,
		.ss_pin = NRF_DRV_SPI_PIN_NOT_USED,
		.irq_priority = APP_IRQ_PRIORITY_HIGH,
		.orc = 0xFF,
		.frequency = NRF_DRV_SPI_FREQ_1M,
		.mode = NRF_DRV_SPI_MODE_0,
		.bit_order = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST
};

bool spiflash_init(void) {
	uint16_t id;

#if defined(BOARD_CELL_NODE_REV1P1)
	spiflash_reset();
#endif

	spiflash_standby();
	vTaskDelay(20);

	id = spiflash_readID();
	deviceID = (id & 0xFF);

	if (deviceID == MX25R6435F_DEVICEID) {
		sizeBytes = MX25R6435F_SIZE_BYTES;
		sectorSizeBytes = MX25R6435F_SECTOR_SIZE_BYTES;
	} else {
		log_errorf("Device ID of external flash (0x%02x) unrecognized!\r\n", deviceID);
		initialized = false;
		return false;
	}

	initialized = true;
	return true;
}


bool spiflash_getInit() {
	return initialized;
}

uint32_t spiflash_getSize() {
	if (!initialized) {
		return 0;
	}

	return sizeBytes;
}

uint32_t spiflash_getSectorSize() {
	if (!initialized) {
		return 0;
	}

	return sectorSizeBytes;
}


bool spiflash_standby(void) {
	WITH_SPI_MTX() {
		spiflash_assert_cs();
		nrf_delay_us(10);
		spiflash_deassert_cs();
	}

	nrf_delay_us(50);

	return true;
}

bool spiflash_deepSleep(void) {
	bool status = false;

	WITH_SPI_MTX() {
		spiflash_assert_cs();

		uint8_t write[] = {0xB9};

		if (spi_transfer(write, 1, NULL, 0)) {
			status = true;
		}

		spiflash_deassert_cs();
	}

	return status;
}

bool spiflash_enableWrite(void) {
	bool status = false;

	WITH_SPI_MTX() {
		spiflash_assert_cs();

		uint8_t command[] = {ENABLE_WRITE_COMMAND};

		if (spi_transfer(command, sizeof(command), NULL, 0)){
			status = true;
		}

		spiflash_deassert_cs();
	}

	return status;
}

bool spiflash_disableBlockProtect(uint8_t blocks) {
	return true;
}

//bool spiflash_disableWrite(void) {
//	bool status = false;
//	spi_select(FLASH_CSN);
//
//	uint8_t command[] = {DISABLE_WRITE_COMMAND};
//
//	if (spi_transfer(command,sizeof(command),NULL,0)){
//		status = true;
//	}
//
//	spi_select(ETHERNET_CSN);
//
//	return status;
//}

uint8_t spiflash_readStatus() {
	uint8_t readBuffer[2];

	WITH_SPI_MTX() {
		spiflash_assert_cs();

		uint8_t command[] = {READ_STATUS_COMMAND};

		spi_transfer(command, sizeof(command), readBuffer, sizeof(readBuffer));

		spiflash_deassert_cs();
	}

	return readBuffer[1];
}


uint16_t spiflash_readID(void) {
	uint8_t readBuffer[] = {0,0,0,0,0,0};
	uint8_t mfgID = 0;
	uint8_t deviceID = 0;

	WITH_SPI_MTX() {
		spiflash_assert_cs();

		vTaskDelay(1);
		uint8_t command[] = {0x90,0,0,0,0};

		spi_transfer(command, sizeof(command), readBuffer, sizeof(readBuffer));

		log_debugf("READ: [%02x %02x %02x %02x %02x %02x ]", readBuffer[0], readBuffer[1], readBuffer[2], readBuffer[3], readBuffer[4], readBuffer[5]);

		spiflash_deassert_cs();
	}

	mfgID = readBuffer[4];
	deviceID = readBuffer[5];

	return ((uint16_t)mfgID << 8) | (uint16_t)deviceID;

}


//bool spiflash_eraseBlock(uint32_t addr) {
//	VALIDATE_ADDRESS(addr);
//	bool status = false;
//	uint8_t busy = 0;
//
//	flash_standby();
//	vTaskDelay(10);
//
//	status = flash_enable_write();
//	vTaskDelay(1);
//
//	spi_select(FLASH_CSN);
//
//	addr &= ~(0x00007FFF);
//	uint8_t cmd[4];
//	cmd[0] = ERASE_BLOCK_COMMAND;
//	cmd[0] = (uint8_t) ((addr >> 16) & 0x000000FF);
//	cmd[1] = (uint8_t) ((addr >> 8) & 0x000000FF);
//	cmd[2] = (uint8_t) ((addr) & 0x000000FF);
//
//	if (spi_transfer(cmd,4,NULL,0)){
//			status = true;
//	}
//	spi_select(ETHERNET_CSN);
//
//	do {
//		vTaskDelay(10);
//		busy = spiflash_readStatus() & WIP_BIT;
//	} while (busy);
//
//	return status;
//}


bool spiflash_eraseSector(uint32_t addr) {
	VALIDATE_ADDRESS(addr);
	bool status = false;
	uint8_t busy = 0;

	spiflash_standby();
	vTaskDelay(10);

	spiflash_enableWrite();

	vTaskDelay(1);

	WITH_SPI_MTX() {
		spiflash_assert_cs();

		uint8_t cmd[4];
		cmd[0] = ERASE_SECTOR_COMMAND;
		cmd[1] = (uint8_t) ((addr >> 16) & 0x000000FF);
		cmd[2] = (uint8_t) ((addr >> 8) & 0x000000FF);
		cmd[3] = (uint8_t) ((addr) & 0x000000FF);

		if (spi_transfer(cmd, 4, NULL, 0)) {
				status = true;
		}

		spiflash_deassert_cs();
	}

	do {
		vTaskDelay(3);
		busy = spiflash_readStatus() & WIP_BIT;
	} while (busy);

	return status;
}

bool spiflash_programByte(uint32_t addr, uint8_t data) {
	bool success = false;
	uint8_t writeBuffer[5];
	VALIDATE_ADDRESS(addr);

	spiflash_enableWrite();

	WITH_SPI_MTX() {
		spiflash_assert_cs();

		writeBuffer[0] = PROGRAM_PAGE_COMMAND;
		writeBuffer[1] = (uint8_t) ((addr >> 16) & 0x000000FF);
		writeBuffer[2] = (uint8_t) ((addr >> 8) & 0x000000FF);
		writeBuffer[3] = (uint8_t) ((addr) & 0x000000FF);
		writeBuffer[4] = data;

		if (spi_transfer(writeBuffer, sizeof(writeBuffer), NULL, 0)) {
			success = true;
		}

		spiflash_deassert_cs();
	}

	return success;
}


bool spiflash_program(uint32_t addr, uint8_t *data, uint16_t length) {
	uint16_t writeLength = 0;
	uint8_t writeBuffer[4];

	VALIDATE_ADDRESS(addr);

	/* verify write length */
	if (length == 0) {
		log_errorf("ERROR: Invalid buffer length of %u!  [%s:%u]\r\n",length,__FUNCTION__,__LINE__);
		return false;
	}
	if ((addr+length) > (MAX_ADDRESS + 1)) {
		log_errorf("ERROR: Invalid write address (%0x%08x) exceeds MAX_ADDRESS (%0x%08x)!  [%s:%u]\r\n",
				(addr+(uint32_t)length), MAX_ADDRESS,__FUNCTION__,__LINE__);
		return false;
	}

	while (length >= 1) {
		spiflash_enableWrite();

		/* determine write length */
		writeLength = PAGE_SIZE - (addr % PAGE_SIZE);

		if (writeLength > length) {
			writeLength = length;
		}

		if (writeLength > MAX_READWRITE_LENGTH) {
			writeLength = MAX_READWRITE_LENGTH;
		}

		WITH_SPI_MTX() {
			spiflash_assert_cs();

			/* Send the address command to initiate the page write */
			writeBuffer[0] = PROGRAM_PAGE_COMMAND;
			writeBuffer[1] = (uint8_t) ((addr >> 16) & 0x000000FF);
			writeBuffer[2] = (uint8_t) ((addr >> 8) & 0x000000FF);
			writeBuffer[3] = (uint8_t) ((addr) & 0x000000FF);
			spi_transfer(writeBuffer,4,NULL,0);

			/* Now send the data */
			spi_transfer(data, writeLength, NULL, 0);

			spiflash_deassert_cs();
		}

		while (spiflash_readStatus() & WIP_BIT) {
			vTaskDelay(3);
		}

		/* Increment data and decrement length */
		length -= writeLength;
		data += writeLength;
		addr += writeLength;
	}

	return true;
}

bool spiflash_read(uint32_t addr, uint8_t *data, uint16_t length) {
	uint16_t readLength = 0;
	uint8_t command[4];
	uint8_t readData[256];

	VALIDATE_ADDRESS(addr);

	/* verify write length */
	if (length == 0) {
		log_errorf("ERROR: Invalid buffer length of %u!  [%s:%u]\r\n",length,__FUNCTION__,__LINE__);
		return false;
	}
	if ((addr+length) > (MAX_ADDRESS + 1)) {
		log_errorf("ERROR: Invalid read address (%0x%08x) exceeds MAX_ADDRESS (%0x%08x)!  [%s:%u]\r\n",
				(addr+(uint32_t)length), MAX_ADDRESS,__FUNCTION__,__LINE__);
		return false;
	}

	while (length >= 1) {
		/* determine read length */
		if (length > MAX_READWRITE_LENGTH) {
			readLength = MAX_READWRITE_LENGTH;
		} else {
			readLength = length;
		}

		WITH_SPI_MTX() {
			spiflash_assert_cs();

			/* Send the address command to initiate the page write */
			command[0] = READ_DATA_COMMAND;
			command[1] = (uint8_t) ((addr >> 16) & 0x000000FF);
			command[2] = (uint8_t) ((addr >> 8) & 0x000000FF);
			command[3] = (uint8_t) ((addr) & 0x000000FF);
			spi_transfer(command, 4, readData, readLength+4);

			memcpy(data, (readData+4), readLength);

			spiflash_deassert_cs();
		}

		/* Increment data and decrement length */
		length -= readLength;
		data += readLength;
		addr += readLength;

	}

	return true;
}

bool spiflash_recovery(void) {
	bool status = true;

	WITH_SPI_MTX() {
		spiflash_assert_cs();

		uint8_t cmd[] = {0x66};

		if (!spi_transfer(cmd, 1, NULL, 0)) {
			status &= false;
		}

		spiflash_deassert_cs();

		vTaskDelay(1);

		spiflash_assert_cs();

		cmd[0] = 0x99;

		if (!spi_transfer(cmd,1,NULL,0)){
			status &= false;
		}

		spiflash_deassert_cs();

	}

	return status;
}

bool spiflash_test(void) {
	uint8_t writeData[16] = {0x34, 0x01, 0xab, 0x4c, 0xd8, 0x10, 0xFF, 0x03,
								0x9b, 0xe2, 0xa0, 0x29, 0xc5, 0xdd, 0x60, 0x38};
	uint8_t readData[16];
	uint8_t i;

	memset(readData,0,sizeof(readData));

	/* Disable the write protection on all four blocks */
	spiflash_disableBlockProtect(0x0F);

	spiflash_eraseSector(0x00000);
	spiflash_read(0x00, readData, sizeof(readData));
	for (i=0; i<sizeof(readData); i++) {
		if (readData[i] != 0xFF) {
			log_error("spiflash initial erase test failed\r\n");
			return false;
		}
	}
	log_info("spiflash initial erase test passed\r\n");

	spiflash_eraseSector(0x000000);
	spiflash_programByte(0x000000, writeData[0]);
	spiflash_read(0x000000, readData, 1);
	if (readData[0] != writeData[0]) {
		LOGGER(0,"spiflash byte program test failed\r\n");
		return false;
	}
	log_info("spiflash byte program test passed\r\n");

	memset(readData,2,sizeof(readData));

	spiflash_eraseSector(0x000000);
	spiflash_program(0x000001, writeData, sizeof(writeData));
	spiflash_read(0x000001, readData, sizeof(readData));
	for (i=0; i<sizeof(writeData); i++) {
		if (readData[i] != writeData[i]) {
			log_error("spiflash bulk program test failed\r\n");
			return false;
		}
	}
	log_info("spiflash bulk program test passed\r\n");

	memset(readData,3,sizeof(readData));

	spiflash_eraseSector(0x000000);
	spiflash_eraseSector(spiflash_getSectorSize());
	uint32_t writeAddr = spiflash_getSectorSize() - (sizeof(writeData)/2);
	spiflash_program(writeAddr,writeData, sizeof(writeData));
	spiflash_read(writeAddr, readData, sizeof(readData));
	for (i=0; i<sizeof(readData); i++) {
		if (readData[i] != writeData[i]) {
			log_error("spiflash write though sector test failed\r\n");
			return false;
		}
	}
	log_info("spiflash write though sector test passed\r\n");


	spiflash_eraseSector(0x000000);
	spiflash_eraseSector(spiflash_getSectorSize());
	spiflash_read(writeAddr, readData, sizeof(readData));
	for (i=0; i<sizeof(readData); i++) {
		if (readData[i] != 0xFF) {
			log_error("spiflash erase test failed\r\n");
			return false;
		}
	}
	log_info("spiflash erase test passed\r\n");

	return true;
}


static void spiflash_reset() {
	WITH_SPI_MTX() {
		spi_csmux_select(FLASH_RESETN_CSN);
		nrf_gpio_pin_clear(CSN_ENN_PIN);

		vTaskDelay(10);

		nrf_gpio_pin_set(CSN_ENN_PIN);

		vTaskDelay(200);
	}
}


void spiflash_assert_cs() {
	spi_init(&spiflash_spi_config, NULL);

	spi_csmux_select(FLASH_CSN);
	nrf_gpio_pin_clear(CSN_ENN_PIN);
}


void spiflash_deassert_cs() {
	nrf_gpio_pin_set(CSN_ENN_PIN);
	spi_deinit();
}

