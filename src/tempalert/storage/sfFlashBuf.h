/* 
 * File:   sfFlashBuf.h
 * Author: kwgilpin
 *
 * Created on March 30, 2013, 11:36 PM
 */

#ifndef SFFLASHBUF_H
#define	SFFLASHBUF_H

bool sfFlashBuf_init(void);

uint16_t sfFlashBuf_getInRecPtr(void);
uint16_t sfFlashBuf_getOutRecPtr(void);
bool sfFlashBuf_isEmpty(void);
uint16_t sfFlashBuf_countItems(void);
uint32_t sfFlashBuf_getTotalSpace(void);
uint32_t sfFlashBuf_getAvailSpace(void);
uint32_t sfFlashBuf_getConsumedSpace(void);

void sfFlashBuf_flush(void);

bool sfFlashBuf_push(const uint8_t *data, uint8_t dataLen);
bool sfFlashBuf_pop(uint8_t *data, uint8_t *dataLen);


#endif	/* SFFLASHBUF_H */

