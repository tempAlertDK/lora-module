/* 
 * File:   storeFwd.h
 * Author: kwgilpin
 *
 * Created on January 10, 2013, 1:58 AM
 */

#ifndef STOREFWD_H
#define	STOREFWD_H

#include <stdint.h>
#include <stdbool.h>

#include "defines.h"
#include "sensors.h"
#include "globals.h"

enum {
	SF_LOCAL_MEAS,
	SF_LOCAL_PACKET
};

//typedef struct __attribute__ ((__packed__)) {
//	uint8_t sensorTypeCount;
//	uint8_t sensorValueDataLen;
//	uint8_t sensorSNDataLen;
//	uint8_t sensorCombinedData[96];
//} sfLocalMeas_t;

#include <stdint.h>
#include <stdbool.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "genericQueue.h"
#include "sensors.h"
#include "event_groups.h"


/* Task related variables */
extern TaskHandle_t m_storeFowrard_thread;
extern QueueHandle_t sf_recordInQueue;
extern QueueHandle_t sf_recordOutQueue;

#define SF_EVENT_GROUP_INCOMING_BIT		1
#define SF_EVENT_GROUP_OUTGOING_BIT		2

#define RECORD_DATA_LENGTH				30

extern EventGroupHandle_t sf_eventGroup;

typedef uint32_t UTC_Timestamp_t; //  UTC in seconds

typedef enum {
	SF_NULL_RECORD,
	SF_SENSORS_RECORD,
	SF_STATUS_RECORD,
	SF_PACKET_RECORD
} sf_record_type_t;

typedef struct {
	sf_record_type_t type;		// Type of record pushed to S&F
	int32_t timeStamp;			// Timestamp (100ms) of record creation
	UTC_Timestamp_t utc_s;		// TODO: Add UTC timestamp
	uint16_t p_pkt;				// Index to record in current packet
	uint16_t messages;			// Number message in record
	uint16_t dataLength;		// Length of record data
} sf_record_header_t;

typedef struct {
	sf_record_header_t header;
	uint8_t data[RECORD_DATA_LENGTH];
} sf_record_t;

typedef struct __attribute__ ((__packed__)){
	uint8_t packetLen;
	uint8_t packet[PACKET_MAX_LENGTH];
} sf_local_packet_t;

typedef struct __attribute__ ((__packed__)) {
	/* timeStamp should hold the value returned by get_time_100ms() when the 
	 * measurement was inserted into the FIFO. */
	int32_t timeStamp;
	/* uploadFailures represents the number of times we've attempted to upload
	 * the measurement to server and have failed for reasons other than a 
	 * connection problem.  For example, a CRC mismatch results in 
	 * uploadFailures being incremented. */
	uint8_t uploadFailures; 
	 /* type indicates whether whether this measurement is remote or local data,
	  * and it determine how we process the sourceData union. */
	uint8_t type;
	union {
		sf_record_t localMeas;
	} sourceData;
} sf_data_t;

bool stoFwd_init(void);

/**
 * Blocking function to halt task flow until S&F has been initialized
 */
void stoFwd_waitUntilInit();

bool stoFwd_initStack(bool attemptRecovery, resetReason_t resetReason);
bool stoFwd_initFIFO(void);

uint16_t stoFwd_getStackDiscardedItemCount(void);
void stoFwd_resetStackDiscardedItemCount(void);
bool stoFwd_isEmpty(void);
uint32_t stoFwd_countItems(void);
uint32_t stoFwd_countItemsStack(void);
uint32_t stoFwd_countItemsFIFO(void);
uint32_t stoFwd_getOccupiedSize(void);
uint32_t stoFwd_getOccupiedSizeStack(void);
uint32_t stoFwd_getOccupiedSizeFIFO(void);
uint32_t stoFwd_getTotalSize(void);
uint32_t stoFwd_getTotalSizeStack(void);
uint32_t stoFwd_getTotalSizeFIFO(void);
bool stoFwd_push(const sf_data_t *data);
bool stoFwd_pushFIFO(const sf_data_t *data);
bool stoFwd_pop(sf_data_t *data);
bool stoFwd_moveStackToFIFO(void);
bool stoFwd_flush(void);

bool stoFwd_sendRecord(sf_record_t * inRecord, TickType_t waitTicks);
bool stoFwd_receiveRecord(sf_record_t * outRecord, TickType_t waitTicks);

#endif	/* STOREFWD_H */
