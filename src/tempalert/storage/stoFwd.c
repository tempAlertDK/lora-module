#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"

#include "log.h"

#include "defines.h"
//#include "global.h"
#include "power.h"
#include "sfBuf.h"
#include "stoFwd.h"
#include "unixTime.h"
#include "sfFlashBuf.h" 
#include "errorCount.h"
#include "spiflash_utils.h"


#ifndef STOFWD_STACK_FIFO_AGE_THOLD_SEC
/* This threshold is used to determine whether items being added to the store
 * and forward buffers are placed on the stack portion of the buffer or are
 * placed in the FIFO portion of the buffer. Yonger items will be placed on the
 * top of the stack while older items will be placed in the FIFO. */
#define STOFWD_STACK_FIFO_AGE_THOLD_SEC 300
#endif

/* The age field of the data sent to sensorcloud is only 3 bytes long.
 * Therefore, if a packet is older than 6.4 months, we can overflow when we
 * try to transmit. The max age cutoff of 180 days will be used to throw away data in the
 * Stack or FIFO when we try to pop. */
#define MAX_DATA_AGE_SEC	(15552000UL)
#define MAX_DATA_AGE_100MS	(155520000UL)

static bool stoFwdStackInitialized = false;
static bool stoFwdFIFOInitialized = false;
static bool stoFwdInitialized = false;

#define NOINIT_STACK_DATA_KEY	0x83DE8F02

/* Despite the fact that the buffer will have elements potentially added and
 * removed within an ISR, there is no need to mark the buffer as volatile so
 * long as we always use the sfBuf_* functions to access it.  This is because
 * these function are interrupt safe, i.e. they are atomic.  Note: the ISR must
 * use the sfBuf_* functions as well. */
static sfBuf_t *stoFwdStack = NULL;

/* We place all of the un-initialized variables in a struct together so that if
 * they are relocated from one version to the next, they will all be relocated
 * together.  This avoid the scenario in which the key is not relocated, and
 * appears valid, but the variables it protects are relocated, thereby causing
 * corruption after an OTA upgrade. */
static struct {
	uint32_t key;
	uint16_t stackCount;
	uint16_t discardedStackCount;
	uint8_t stoFwdStackMemory[DATA_STACK_BUFFER_SIZE];
} m_noinitStackData __attribute__ ((section(".noinit")));

static bool stoFwd_adjustStackTimestampsPostReset(sfBuf_t *buf);

TaskHandle_t m_storeFowrard_thread;
StaticTask_t m_storeFowrard_thread_static;
#define SF_STACK_SIZE		512
StackType_t m_storeFowrard_thread_buffer[ SF_STACK_SIZE ];

// Event Group Variables
EventBits_t sf_eventBitsMask;
EventBits_t sf_activeEventBit;
EventBits_t sf_localEventBits;
EventGroupHandle_t sf_eventGroup;
StaticEventGroup_t sf_eventGroupStatic;
static SemaphoreHandle_t sf_initSmphr = NULL;
static StaticSemaphore_t sf_initSmphrBuffer;


// Queue Handles
#define RECORDINQUEUE_LENGTH	4
#define RECORDINQUEUE_ITEMSIZE sizeof(sf_record_t)
uint8_t recordInQueueBuffer[RECORDINQUEUE_LENGTH*RECORDINQUEUE_ITEMSIZE];
static StaticQueue_t recordInQueueStatic;

#define RECORDOUTQUEUE_LENGTH	1
#define RECORDOUTQUEUE_ITEMSIZE sizeof(sf_record_t)
uint8_t recordOutQueueBuffer[RECORDOUTQUEUE_LENGTH*RECORDOUTQUEUE_ITEMSIZE];
static StaticQueue_t recordOutQueueStatic;

QueueHandle_t sf_recordInQueue;
QueueHandle_t sf_recordOutQueue;

void stoFwd_thread(void *arg);

bool stoFwd_init(void) {

	// Create S&F record queue to collect incoming data
	if ((sf_recordInQueue = xQueueCreateStatic(RECORDINQUEUE_LENGTH,RECORDINQUEUE_ITEMSIZE,
			recordInQueueBuffer, &recordInQueueStatic)) == pdFALSE){
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	// Create S&F record queue to provide access to outgoing data
	if ((sf_recordOutQueue = xQueueCreateStatic(RECORDOUTQUEUE_LENGTH,RECORDOUTQUEUE_ITEMSIZE,
			recordOutQueueBuffer, &recordOutQueueStatic)) == pdFALSE){
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	// Create S&F event group
	if ((sf_eventGroup = xEventGroupCreateStatic(&sf_eventGroupStatic)) == pdFALSE){
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	if ((sf_initSmphr = xSemaphoreCreateBinaryStatic(&sf_initSmphrBuffer)) == pdFALSE) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	// Create task.
	if ((m_storeFowrard_thread = xTaskCreateStatic(stoFwd_thread, "S&F", SF_STACK_SIZE,
			NULL, 1, m_storeFowrard_thread_buffer, &m_storeFowrard_thread_static)) == NULL ) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	return true;

}


// see .h
void stoFwd_waitUntilInit() {
	if (xSemaphoreTake(sf_initSmphr,portMAX_DELAY) == pdTRUE) {
		xSemaphoreGive(sf_initSmphr);
		log_info("S&F Stack ready, continuing.");
	}
}


void stoFwd_thread(void *arg) {
	/* Sensors thread local gQ containers */
	sf_data_t sdData;
	log_print("S&F THREAD STARTED");

	stoFwd_initStack(true, RESET_REASON_UNKNOWN);
	spiflash_init();
	stoFwd_initFIFO();

	// indicate that the stack is ready to go
	xSemaphoreGive(sf_initSmphr);

	/* Initialize mask */
	sf_eventBitsMask = SF_EVENT_GROUP_INCOMING_BIT | 	SF_EVENT_GROUP_OUTGOING_BIT;

	/* Main S&F Task Loop */
	for (;;) {

		sf_activeEventBit = 0;
		/* Wait for event to be placed into the tasks event generic queue */
		if (!sf_localEventBits) {
			sf_localEventBits = xEventGroupWaitBits(sf_eventGroup, sf_eventBitsMask, pdFALSE, pdFALSE, portMAX_DELAY);
		}

		/* Set type to NULL to handle empty Queue*/
		sdData.sourceData.localMeas.header.type = SF_NULL_RECORD;
		sdData.type = SF_LOCAL_MEAS;

		/* Handle incoming sources (Queues) */
		if (sf_localEventBits & SF_EVENT_GROUP_INCOMING_BIT) {
			if (xQueueReceive(sf_recordInQueue,&( sdData.sourceData.localMeas ),pdMS_TO_TICKS(10)) == true) {
				sf_activeEventBit = SF_EVENT_GROUP_INCOMING_BIT;
			} else {
				sf_localEventBits &= ~SF_EVENT_GROUP_INCOMING_BIT;
				sf_localEventBits |= xEventGroupClearBits(sf_eventGroup, SF_EVENT_GROUP_INCOMING_BIT);
			}
		} else if (sf_localEventBits & SF_EVENT_GROUP_OUTGOING_BIT) {
			sf_activeEventBit = SF_EVENT_GROUP_OUTGOING_BIT;
			sf_localEventBits &= ~SF_EVENT_GROUP_OUTGOING_BIT;
			xEventGroupClearBits(sf_eventGroup, SF_EVENT_GROUP_OUTGOING_BIT);
		} else {
			/* Unknown event bit triggered */
			log_errorf("Unknown event bits [%08x] in sf_localEventBits [%s]", sf_localEventBits, __FUNCTION__);
			/* bits should be cleared to avoid re-triggering this event */
			sf_localEventBits &= SF_EVENT_GROUP_INCOMING_BIT | SF_EVENT_GROUP_OUTGOING_BIT;
			continue;
		}

		spiflash_standby();

		/* S&F task simply shuttles data from its incoming data queue into
		 * its outgoing message queue to be delivered to core. */
		switch (sf_activeEventBit) {
			case SF_EVENT_GROUP_INCOMING_BIT:
				if (sdData.sourceData.localMeas.header.type != SF_NULL_RECORD) {
					log_info("Pushing incoming record onto local SF Stack...");
					/* Push the received sf record onto the local ram based stack. We push it
					 * at the current stack pointer, overwriting older data if the stack is full. */
					sdData.timeStamp = sdData.sourceData.localMeas.header.timeStamp;
					stoFwd_push(&sdData);
				} else {
					log_error("NULL Record passed to S&F, Discarding.");
				}
				break;
			case SF_EVENT_GROUP_OUTGOING_BIT:
				/* A task has indicatd it would like to receive a S&F item.
				 * We now load the item into the queue. */
				if (!stoFwd_isEmpty() && stoFwd_pop(&sdData)) {
					if (sdData.sourceData.localMeas.header.type == SF_NULL_RECORD) {
						log_warn("NULL Record popped from S&F!");
					}
					sdData.sourceData.localMeas.header.timeStamp = sdData.timeStamp;
					if (pdFALSE == xQueueSend(sf_recordOutQueue,&sdData.sourceData.localMeas,0)) {
						stoFwd_push(&sdData);
						log_errorf("Failed to send pop SF record! [%s]",__FUNCTION__);
					}
				} else {
					sdData.sourceData.localMeas.header.type = SF_NULL_RECORD;
					if (!unixTime_isValid()) {
						log_info("Unixtime invalid, S&F inaccessible, Sending NULL recored.");
					} else if (stoFwd_isEmpty()) {
						log_debug("S&F Stack empty, Sending NULL recored.");
					} else {
						log_error("S&F ERROR! Sending NULL recored!");
					}
					if (pdFALSE == xQueueSend(sf_recordOutQueue,&sdData.sourceData.localMeas,0)) {
						log_errorf("Failed to send null record! [%s]",__FUNCTION__);
					}
				}
				break;
		}

		spiflash_deepSleep();

	}
}

/**
 * see .h
 */
bool stoFwd_sendRecord(sf_record_t * inRecord, TickType_t waitTicks) {
	/* If everything is successful, push sensor sf record into S&F queue */
	if (pdTRUE == (xQueueSend(sf_recordInQueue, inRecord, waitTicks))) {
		/* Set the bit to indicate that there is a element waiting */
		xEventGroupSetBits(sf_eventGroup,SF_EVENT_GROUP_INCOMING_BIT);
		return true;
	} else {
		log_errorf("%s task failed to push data into S&F Stack!",
				pcTaskGetName(xTaskGetCurrentTaskHandle()));
		errorCount_incrementCount(ERROR_STOFWD_PUSH_ERROR_RESULTING_IN_LOST_ITEM);
		return false;
	}
}

/**
 * see .h
 */
bool stoFwd_receiveRecord(sf_record_t * outRecord, TickType_t waitTicks) {
	// indicate that we would like to retrieve some data
	xEventGroupSetBits(sf_eventGroup,SF_EVENT_GROUP_OUTGOING_BIT);

	// block waitTicks for something to arrive
	if ((pdTRUE != xQueueReceive(sf_recordOutQueue, outRecord, pdMS_TO_TICKS(100)))) {
		log_errorf("%s task failed to pop data from S&F Stack!",
						pcTaskGetName(xTaskGetCurrentTaskHandle()));
		/* void local the record */
		outRecord->header.type = SF_NULL_RECORD;
		return false;
	}
	return true;
}





bool stoFwd_initStack(bool attemptRecovery, resetReason_t resetReason) {
	bool success = true;
	bool stackRecovered = false;

	if (stoFwdStackInitialized) {
		return true;
	}

	/* If the key which protects the discarded stack count variable is corrupt,
	 * we assume that the discarded stack count is also corrupt and reset it to
	 * 0.  */
	if (m_noinitStackData.key != NOINIT_STACK_DATA_KEY) {
		m_noinitStackData.discardedStackCount = 0;
	}

	if (attemptRecovery) {
		stoFwdStack = (sfBuf_t *)(m_noinitStackData.stoFwdStackMemory);

		if (m_noinitStackData.key == NOINIT_STACK_DATA_KEY) {
			if (sfBuf_recoverPostReset(stoFwdStack, resetReason)) {
				uint16_t stackCountPreTimeAdjustment = (uint16_t)sfBuf_countItems(stoFwdStack);

				log_warn("S&F stack appears intact post-reset");

				if (stoFwd_adjustStackTimestampsPostReset(stoFwdStack)) {
					stackRecovered = true;
					m_noinitStackData.stackCount = (uint16_t)sfBuf_countItems(stoFwdStack);
					/* We use the stack count key to mark the stack count as valid */
					m_noinitStackData.key = NOINIT_STACK_DATA_KEY;

					if (m_noinitStackData.stackCount == stackCountPreTimeAdjustment) {
						log_infof("Successfully adjusted all %u S&F stack timestamps post-reset", m_noinitStackData.stackCount);
					} else {
						m_noinitStackData.discardedStackCount += (uint16_t)(stackCountPreTimeAdjustment - m_noinitStackData.stackCount);
						log_errorf("Unable to adjust all S&F stack timestamps post-reset; %u of %u measurements lost", stackCountPreTimeAdjustment - m_noinitStackData.stackCount, stackCountPreTimeAdjustment);
					}
				} else {
					stackRecovered = false;
					m_noinitStackData.discardedStackCount += stackCountPreTimeAdjustment;
					log_errorf("Unable to adjust any S&F stack timestamps post-reset; %u measurements lost", stackCountPreTimeAdjustment);
				}
			} else {
				/* This block is executed if stack recovery fails, but the key
				 * was valid. */
				stackRecovered = false;
				m_noinitStackData.discardedStackCount += m_noinitStackData.stackCount;
				log_errorf("S&F stack corrupt post-reset; %u measurements lost", m_noinitStackData.stackCount);
			}
		} else {
			/* This block is executed if the key does not match */
			stackRecovered = false;
			log_error("S&F stack corrupt post-reset (key mismatch); unknown number of measurements lost");
		}
	} else {
		/* This block is executed if the caller did not want to attempt stack
		 * recovery. */
		stackRecovered = false;
	}

	/* If we failed to recover the stack, re-initialize it */
	if (!stackRecovered) {
		m_noinitStackData.stackCount = 0;
		m_noinitStackData.key = NOINIT_STACK_DATA_KEY;

		if (!sfBuf_init(m_noinitStackData.stoFwdStackMemory, sizeof(m_noinitStackData.stoFwdStackMemory))) {
			stoFwdStack = NULL;
			success = false;
			log_error("Failed to initialize the S&F stack!");
		} else {
			log_warn("Successfully re-initialized the S&F stack");
		}
	}

	if (success) {
		stoFwdStackInitialized = true;
	}

	if (stoFwdStackInitialized && stoFwdFIFOInitialized) {
		stoFwdInitialized = true;
	}

	return success;
}

bool stoFwd_initFIFO() {
	bool success = true;

	if (stoFwdFIFOInitialized) {
		return true;
	}

	if (!sfFlashBuf_init()) {
		success = false;
		log_error("Failed to initialize the S&F FIFO!");
	} else {
		log_info("Successfully initialized the S&F FIFO");
	}

	if (success) {
		stoFwdFIFOInitialized = true;
	}

	if (stoFwdStackInitialized && stoFwdFIFOInitialized) {
		stoFwdInitialized = true;
	}

	return success;
}

uint16_t stoFwd_getStackDiscardedItemCount() {
	if (!stoFwdInitialized) {
		return 0;
	}

	return m_noinitStackData.discardedStackCount;
}

void stoFwd_resetStackDiscardedItemCount() {
	m_noinitStackData.discardedStackCount = 0;
}

bool stoFwd_isEmpty() {
	if (!stoFwdInitialized) {
		return true;
	}

	if (!sfBuf_isEmpty(stoFwdStack)) {
		return false;
	}

	if (!sfFlashBuf_isEmpty() && unixTime_isValid()) {
		/* We must know the current Unix time before we can access the records
		 * in the flash-based FIFO.  So, if the Unix time is not valid, we
		 * pretend that the buffer is empty.  This test of the Unix time fixed
		 * a bug which caused the gateway to attempt and then abort an upload
		 * every 5 seconds between the first and second sensor measurement
		 * (i.e. until the Unix time was known) if the S&F buffer contained
		 * items after a reboot. */
		return false;
	}

	return true;
}

uint32_t stoFwd_countItems() {
	if (!stoFwdInitialized)
		return 0;

	return stoFwd_countItemsStack() + stoFwd_countItemsFIFO();
}

uint32_t stoFwd_countItemsStack() {
	if (!stoFwdStackInitialized)
		return 0;

	return sfBuf_countItems(stoFwdStack);
}

uint32_t stoFwd_countItemsFIFO() {
	if (!stoFwdFIFOInitialized)
		return 0;

	return (uint32_t) sfFlashBuf_countItems();
}

uint32_t stoFwd_getOccupiedSize() {
	uint32_t occupiedSize;

	if (!stoFwdInitialized)
		return 0;

	occupiedSize =  stoFwd_getTotalSize();
	occupiedSize -= (uint32_t)sfBuf_getAvailSpace(stoFwdStack);
	occupiedSize -= (uint32_t)sfFlashBuf_getAvailSpace();

	return occupiedSize;
}

uint32_t stoFwd_getOccupiedSizeStack() {
	if (!stoFwdInitialized)
		return 0;

	return (uint32_t)stoFwd_getTotalSizeStack() -
			(uint32_t)sfBuf_getAvailSpace(stoFwdStack);
}

uint32_t stoFwd_getOccupiedSizeFIFO() {
	uint32_t totalSize;

	if (!stoFwdInitialized)
		return 0;

	totalSize = stoFwd_getTotalSizeFIFO();

	return totalSize - (uint32_t)sfFlashBuf_getAvailSpace();
}

uint32_t stoFwd_getTotalSize() {
	if (!stoFwdInitialized)
		return 0;

	return stoFwd_getTotalSizeStack() + stoFwd_getTotalSizeFIFO();
}

uint32_t stoFwd_getTotalSizeStack() {
	if (!stoFwdInitialized)
		return 0;

	return stoFwdStack->dataSize;
}

uint32_t stoFwd_getTotalSizeFIFO() {
	if (!stoFwdInitialized)
		return 0;

	return sfFlashBuf_getTotalSpace();
}

bool stoFwd_adjustStackTimestampsPostReset(sfBuf_t *buf) {
	uint32_t stackCount, i;
	int32_t timeAtLastReset_100ms;
	sf_data_t sfData;
	sfBufItemSize_t sfDataSize;

	stackCount = sfBuf_countItems(stoFwdStack);

	if (stackCount == 0) {
		return true;
	}

	if (!unixTime_getTimeAtLastReset_100ms((uint32_t *)(&timeAtLastReset_100ms))) {
		return false;
	}

	/* Iterate over all items in the stack */
	for (i = 0; i < stackCount; i++) {
		/* Pop each item from the bottom/back of the stack */
		sfDataSize = (uint8_t)sizeof(sfData);
		if (!sfBuf_popBB(buf, &sfData, &sfDataSize)) {
			return false;
		}

		/* Modify the item's timestamp. First, compute the item's age by
		 * subtracting the item's timestamp (referenced to the local 100ms pre-
		 * reset time) from the sum of the local clock pre-reset and the
		 * approximate time required by the bootloader to verify the
		 * application image.  Then, subtract the age from the current time to
		 * calculate its timestamp as referenced to the new clock time. */
		sfData.timeStamp = (int32_t)get_time_100ms() - (timeAtLastReset_100ms + BOOTLOADER_VERIFICATION_TIME_100MS - sfData.timeStamp);

		/* Attempt to push the modified item to the top/front of the stack.
		 */
		if (!sfBuf_tryPushTF(buf, &sfData, sfDataSize)) {
			log_error("Failed to push updated item to bottom of S&F stack when updating timestamps post-reset!");
		}
	}

	return true;
}


bool stoFwd_push(const sf_data_t *dataOriginal) {
	sf_data_t data;
	uint16_t dataLen;
	int32_t age_sec;
	int32_t unixTime;
	sf_data_t stackToFIFOData;
	sfBufItemSize_t stackToFIFODataLen;
	bool pushToFIFO;

	uint16_t initialStackCount;
	uint16_t popAttempts = 0;
	bool batteryLow = false;

	if (!stoFwdInitialized) {
		return false;
	}

	initialStackCount = stoFwd_countItemsStack();

	/* Make a copy of the passed structure because we'll be modifying its
	 * time stamp if we insert it into the flash-based store and forward FIFO.
	 * All time stamps in the FIFO are expected to use absolute Unix times, not
	 * local times. */
	memcpy(&data, dataOriginal, sizeof(data));

	dataLen = sizeof(data.timeStamp) + sizeof(data.uploadFailures) + sizeof(data.type);

	if (data.type == SF_LOCAL_MEAS) {
		dataLen += sizeof(data.sourceData.localMeas);
//		dataLen += sizeof(data.sourceData.localMeas.sensorTypeCount);
//		dataLen += sizeof(data.sourceData.localMeas.sensorValueDataLen);
//		dataLen += sizeof(data.sourceData.localMeas.sensorSNDataLen);
//		dataLen += data.sourceData.localMeas.sensorValueDataLen;
//		dataLen += data.sourceData.localMeas.sensorSNDataLen;
		age_sec = 0;
	} else if (data.type == SF_LOCAL_PACKET) {
#if defined(HARDWARE_FAMILY) && (HARDWARE_FAMILY==3)
		dataLen += sizeof(data.sourceData.nodePacket.packetLen);
		dataLen += data.sourceData.nodePacket.packetLen;

		/* Extract the packet's age when it arrived at the gateway */
		if (!packet_getAge(data.sourceData.nodePacket.packet,
				&(data.sourceData.nodePacket.packetLen), PACKET_DIR_INBOUND, &age_sec)) {
			/* Node packets only contain an age if store and forward was enabled
			* on the node. */
			age_sec = 0;
		}
#else
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
		log_error("S&F data type of SF_NODE_PACKET detected in stoFwd_push()--packet will not be pushed!");
#endif
		return false;
#endif
	} else {
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
		log_errorf("Unknown S&F data type (%u) detected in stoFwd_push()--packet will not be pushed!", data.type);
#endif
		return false;
	}

	/* Compute the packet's total age in seconds */
	age_sec += (get_time_100ms() - data.timeStamp) / 10;


	if (!unixTime_getSeconds(&unixTime)) {
		/* If the Unix time is unknown, we cannot push to the FIFO. */
		pushToFIFO = false;
	} else if (age_sec > STOFWD_STACK_FIFO_AGE_THOLD_SEC) {
		/* If the measurement being pushed is "old" we push it to the the FIFO.
		 */
		pushToFIFO = true;
	} else if (dataLen >= stoFwd_getTotalSizeStack()) {
		/* If the measurement is too big for the stack (highly unlikely) we
		 * push it to the FIFO. */
		pushToFIFO = true;
	} else if (!power_acPowerGood()) {
		batteryLow = false;
		if (power_getBatteryLow(&batteryLow, true) && !batteryLow) {
			/* We can successfully determine that the battery is not low */
			pushToFIFO = false;
		} else {
			/* We cannot determine whether the battery is low or we know that
			 * it is low. */
			pushToFIFO = true;
		}
	} else {
		/* If external power is present */
		pushToFIFO = true;//false;
	}

	/* If we know the current Unix time and the measurement is "old", or too big
	 * for the stack (unlikely), we place it in the FIFO instead of the stack.
	 * When transmitting measurements to the server, we first empty the stack
	 * and then we empty the FIFO.  This ensures that we transmit the newest
	 * measurements first and then transmit all of the "old" data.  Also, if
	 * the stack is very small, we may not be able to fit larger measurements,
	 * so we have to place those in the FIFO even if they are not old. */
	if (pushToFIFO) {
		/* Update the packet's time stamp to use Unix (i.e. the number of
		 * seconds since 1970) instead of local time (the number of tenths of a
		 * second since reset).  Note, in the case of node packets in the
		 * gateway's buffer, the time stamp does not reflect the time at which
		 * the measurement was taken by the node.  Instead, it reflects the time
		 * at which the gateway received the packet from the node.  */
		data.timeStamp = unixTime - ((get_time_100ms() - data.timeStamp) / 10);

		if (!sfFlashBuf_push((uint8_t *)&data, dataLen)) {
			errorCount_incrementCount(ERROR_STOFWD_PUSH_ERROR_RESULTING_IN_LOST_ITEM);
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
			log_error("Unable to push new item onto front of the S&F FIFO!");
#endif
			return false;
		}
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
		log_warnf("Pushed %ld-sec-old item onto the front of the S&F FIFO", age_sec);
#endif
		return true;
	} else if (dataLen < stoFwd_getTotalSizeStack()) {
		while(!sfBuf_tryPushTF(stoFwdStack, &data, dataLen) &&
				(++popAttempts < initialStackCount + 10)) {
			taskYIELD();
			/* Attempt to peek at the item at the bottom/back of the stack. */
			stackToFIFODataLen = (uint8_t)sizeof(stackToFIFOData);
			if (!sfBuf_peekBB(stoFwdStack, &stackToFIFOData, &stackToFIFODataLen)) {
				/* If we fail to peek at the item at the bottom/back of the
				 * stack, we set the data length variable to 0 so that we do
				 * not later attempt to push a non-existent item onto the FIFO.
				 */
				stackToFIFODataLen = 0;
			}

			/* Attempt to remove the item at the bottom/back of the stack */
			if (!sfBuf_discardBB(stoFwdStack)) {
				/* If we can't remove an item from the bottom of the stack, it
				 * means we're not making progress towards freeing up space, so
				 * we're in danger of entering an infinite loop.  To avoid that,
				 * we give up and return failure. */
				errorCount_incrementCount(ERROR_STOFWD_PUSH_ERROR_RESULTING_IN_LOST_ITEM);
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
				log_error("Unable to remove item from the bottom of the S&F stack to make room for new measurement--new measurement will be discarded!");
#endif
				return false;
			}

			/* Having successfully popped an item onto the stack, decrement the
			 * counter used to track the number of items on the stack. */
			if (m_noinitStackData.stackCount > 0) {
				m_noinitStackData.stackCount--;
			}

			if (stackToFIFODataLen == 0) {
				/* If we were able to discard the item at the bottom/back of
				 * the stack but not peek at it, then we cannot add the item
				 * to the FIFO. */
				errorCount_incrementCount(ERROR_STOFWD_PUSH_ERROR_RESULTING_IN_LOST_ITEM);
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
				log_error("Unable to read item at the bottom of the S&F stack, but it was removed and discarded to make room for the new measurement!");
#endif
				continue;
			}

			/* Now we want to push the item we removed from the stack onto the
			 * FIFO.  To do this, we need to update the packet's time stamp so
			 * that it represents the absolute Unix time at which the
			 * measurement was taken or received from the node. */
			if (unixTime_getSeconds(&unixTime)) {
				/* Because we are placing the packet in the flash-based sto/fwd
				 * buffer, we update the time stamp to reflect the Unix time at
				 * which the local measurement was taken or the node packet was
				 * received.  We do this by subtracting the packet's age from the
				 * current Unix time. */
				stackToFIFOData.timeStamp = unixTime -
						((get_time_100ms() - stackToFIFOData.timeStamp) / 10);

				if (!sfFlashBuf_push((uint8_t *)&stackToFIFOData,
						(uint8_t)stackToFIFODataLen)) {
					/* If we can't push the item that we removed from the bottom of
					 * the stack into the FIFO, something has gone wrong so we
					 * return failure. */
					errorCount_incrementCount(ERROR_STOFWD_PUSH_ERROR_RESULTING_IN_LOST_ITEM);
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
					log_error("Unable to push item removed from bottom of the S&F stack onto the front of the S&F FIFO!");
#endif
					/* Jump to the next iteration of the while loop at which
					 * point we'll reattempt to push the new item onto the top
					 * of the sto/fwd stack. */
					continue;
				}
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
				log_warn("Moved item from bottom of S&F stack to the front of the S&F FIFO");
#endif
			} else {
				errorCount_incrementCount(ERROR_STOFWD_PUSH_ERROR_RESULTING_IN_LOST_ITEM);
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
				log_error("Unable to push item removed from bottom of S&F stack onto the front of the S&F FIFO because the Unix time is unknown!");
#endif
			}
		}

		if (popAttempts >= initialStackCount + 10) {
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
			log_error("Error: attempted to pop 10 more items from the S&F stack than the number initially counted!");
#endif
			return false;
		} else {
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
			log_warnf("Pushed %ld-sec-old item onto the top of the S&F stack", age_sec);
#endif
			/* Having successfully pushed an item onto the stack, increment the
			 * counter used to track the number of items on the stack. */
			if (m_noinitStackData.stackCount < UINT16_MAX) {
				m_noinitStackData.stackCount++;
			}

			return true;
		}

	} else {
		errorCount_incrementCount(ERROR_STOFWD_PUSH_ERROR_RESULTING_IN_LOST_ITEM);
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
		log_error("New item too big for the S&F stack!");
#endif
		return false;
	}
}


bool stoFwd_pushFIFO(const sf_data_t *dataOriginal) {
	sf_data_t data;
	uint8_t dataLen;
	int32_t age_sec;
	int32_t unixTime;

	if (!stoFwdInitialized) {
		return false;
	}

	/* Make a copy of the passed structure because we'll be modifying its
	 * time stamp if we insert it into the flash-based store and forward FIFO.
	 * All time stamps in the FIFO are expected to use absolute Unix times, not
	 * local times. */
	memcpy(&data, dataOriginal, sizeof(data));

	dataLen = sizeof(data.timeStamp) + sizeof(data.uploadFailures) +
			sizeof(data.type);

	if (data.type == SF_LOCAL_MEAS) {
		dataLen += sizeof(data.sourceData.localMeas);
//		dataLen += sizeof(data.sourceData.localMeas.sensorTypeCount);
//		dataLen += sizeof(data.sourceData.localMeas.sensorValueDataLen);
//		dataLen += sizeof(data.sourceData.localMeas.sensorSNDataLen);
//		dataLen += data.sourceData.localMeas.sensorValueDataLen;
//		dataLen += data.sourceData.localMeas.sensorSNDataLen;
		age_sec = 0;
	} else if (data.type == SF_LOCAL_PACKET) {
#if defined(HARDWARE_FAMILY) && (HARDWARE_FAMILY==3)
		dataLen += sizeof(data.sourceData.nodePacket.packetLen);
		dataLen += data.sourceData.nodePacket.packetLen;

		/* Extract the packet's age when it arrived at the gateway */
		if (!packet_getAge(data.sourceData.nodePacket.packet,
				&(data.sourceData.nodePacket.packetLen), PACKET_DIR_INBOUND, &age_sec)) {
			/* Node packets only contain an age if store and forward was enabled
			 * on the node. */
			age_sec = 0;
		}
#else
	errorCount_incrementCount(ERROR_STOFWD_PUSH_ERROR_RESULTING_IN_LOST_ITEM);
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
		log_error("S&F data type of SF_NODE_PACKET detected in stoFwd_pushFIFO()--packet will not be pushed!");
#endif
		return false;
#endif
	} else {
		errorCount_incrementCount(ERROR_STOFWD_PUSH_ERROR_RESULTING_IN_LOST_ITEM);
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
		log_errorf("Unknown S&F data type (%u) detected in stoFwd_pushFIFO()--packet will not be pushed!", data.type);
#endif
		return false;
	}

	/* Compute the packet's total age */
	age_sec += (get_time_100ms() - data.timeStamp) / 10;

	if (!unixTime_getSeconds(&unixTime)) {
		errorCount_incrementCount(ERROR_STOFWD_PUSH_ERROR_RESULTING_IN_LOST_ITEM);
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
		log_error("Unable to push item to front of S&F FIFO because current Unix time is unknown!");
#endif
		return false;
	}

	/* Update the packet's time stamp with the absolute Unix time at which the
	 * measurement was taken or the node packet received. */
	data.timeStamp = unixTime - age_sec;

	if (!sfFlashBuf_push((uint8_t *)&data, dataLen)) {
		errorCount_incrementCount(ERROR_STOFWD_PUSH_ERROR_RESULTING_IN_LOST_ITEM);
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
		log_error("Unable to push item onto front of S&F FIFO!");
#endif
		return false;
	}
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
	log_warnf("Pushed %ld-sec-old item onto the front of the S&F FIFO", age_sec);
#endif
	return true;
}


bool stoFwd_pop(sf_data_t *data) {
	sfBufItemSize_t dataLength;
	int32_t age_sec;
	int32_t unixTime;
	int32_t currentAge; //current age in units of 100ms of the data
	static uint8_t discardFailureCount = 0; //number of successive times sfBuf_discardTF fails
	bool packetDiscardedDueToAgeFlag = false; //no packets have been discarded due to age>180days
	//bool result = false; //result so far

	if (!stoFwdInitialized) {
		return false;
	}

  while(1){
		taskYIELD(); //Give other tasks a chance to run
    /**print debug statement if packet was discarded due to age in previous iteration
		 * In the terminal this will appear after 'packet discarded from top of stack'
		 * Also send error to server and reset the discard flag to false.*/

		if (packetDiscardedDueToAgeFlag){
			errorCount_incrementCount(ERROR_STOFWD_TOO_OLD_PROMPTING_DISCARD_OF_PACKET);
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
			log_error("Packet was discarded due to age > 180 days.");
#endif
		}
		packetDiscardedDueToAgeFlag = false;

    /* First we try to pop the newest data off of the top of the RAM-based
	 * stack. If that fails, we then try to pop the oldest data from the back of
	 * the FIFO.*/
	dataLength = (uint8_t)sizeof(*data);
	if (sfBuf_peekTF(stoFwdStack, data, &dataLength)) {
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
		log_warn("Read item from the top of the S&F stack");
#endif

		/* Attempt to discard the item we just read */
		if (sfBuf_discardTF(stoFwdStack)) {
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
			log_warn("Discarded item from the top of the S&F stack");
#endif
			discardFailureCount = 0; //reset the successive failure counter

			/* Having successfully removed an item from the stack, decrement
			 * the counter used to track the number of items on the stack. */
			if (m_noinitStackData.stackCount > 0) {
				m_noinitStackData.stackCount--;
			}

      currentAge = get_time_100ms()-data->timeStamp;
      if (currentAge>MAX_DATA_AGE_100MS){ //If the age of the packet is too old
        packetDiscardedDueToAgeFlag = true;
        continue; //go next iteration of the loop to look for a younger packet
      }else{//otherwise, the packet is less than 6.4 months old
        return true; //return true only if data valid
        break;
      }


		} else {
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
			log_error("Failed to discard item from the top of the S&F stack");
#endif
      discardFailureCount++; //increment the successive failure counter

      /**If we ever fail to discard an item 3 times, return false instead and
       * break from the while loop so we don't get trapped forever with an old
       * item at the top. Discarding could fail if buffer is corrupt so we
       * will flush it before returning. */
      if (discardFailureCount>=3){
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
      log_error("Failed to discard from S&F stack 3 times. Flushing buffer.");
#endif
        sfBuf_flush(stoFwdStack);//flush the buffer since it's probably corrupted
        errorCount_incrementCount(ERROR_STOFWD_BUFFER_CORRUPTED_PROMPTING_FLUSH);
        discardFailureCount = 0; //reset counter
        return false; //will return false instead
        break;
      }

			/* Even if we fail to discard the item, we still return true
			 * because we did manage to peek at the item and we're passing a
			 * pointer to the item back to the caller. */
      currentAge = get_time_100ms()-data->timeStamp;
      if (currentAge>MAX_DATA_AGE_100MS){ //If the age of the packet is too old
        packetDiscardedDueToAgeFlag = true;
        continue; //go next iteration of the loop to look for a younger packet
      }else{//otherwise, the packet is less than 6.4 months old
        return true;//return true only if data valid
        break;
      }

		}
	} else if (unixTime_getSeconds(&unixTime)) {
		if (sfFlashBuf_pop((uint8_t *)data, (sfBufItemSize_t *)&dataLength)) {
			/* We are obligated to return a packet whose time stamp represents the
			 * number of tenths of a second since reset.  Packets stored in the
			 * flash-based FIFO have time stamps that represent Unix time, so here
			 * we convert back to local time. */
			age_sec = unixTime - data->timeStamp;
			data->timeStamp = get_time_100ms() - (10 * age_sec);

#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
			log_warn("Popped item from back of S&F FIFO");
#endif
			currentAge = get_time_100ms()-data->timeStamp;
				if (currentAge>MAX_DATA_AGE_100MS){ //If the age of the packet is too old
					packetDiscardedDueToAgeFlag = true;
					continue; //go next iteration of the loop to look for a younger packet
				}else{//otherwise, the packet is less than 6.4 months old
					return true;//return true only  if data valid
					break;
				}
		} else {
			/* If we know the Unix time but popping from the back of the S&F
			 * FIFO failed, we print an error message and return false to
			 * indicate that nothing was popped. */
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
			log_error("Failed to pop item from back of S&F FIFO");
#endif
			return false;
		}
	} else {
		/* If the Unix time is unknown, print an error message and return
		 * failure to indicate that nothing was popped. */
#if defined(PRINT_FROM_SF) && (PRINT_FROM_SF==1)
		log_error("Unknown Unix time prevents popping of items from the back of S&F FIFO");
#endif
		return false;
	}

	return false;
}
}

bool stoFwd_moveStackToFIFO() {
	int32_t unixTime;
	sf_data_t stackToFIFOData;
	sfBufItemSize_t stackToFIFODataLen;

	uint16_t popAttempts = 0;
	uint16_t initialStackCount = 0;
	uint16_t movedItemCount = 0;

	if (!stoFwdInitialized) {
		return false;
	}

	initialStackCount = stoFwd_countItemsStack();

	stackToFIFODataLen = (uint8_t)sizeof(stackToFIFOData);
	while (sfBuf_popBB(stoFwdStack, &stackToFIFOData, &stackToFIFODataLen) &&
			(++popAttempts < initialStackCount + 10)) {
		taskYIELD(); //Give other tasks a chance to run

		/* Having successfully popped an item from the stack, decrement the
		 * counter used to track the number of items on the stack. */
		if (m_noinitStackData.stackCount > 0) {
			m_noinitStackData.stackCount--;
		}

		if (stackToFIFODataLen == 0) {
			log_error("Popped 0-byte item from bottom of S&F stack!");

			/* Reset the data length variable to its maximum before the
			 * next call to sfBuf_popBB(). */
			stackToFIFODataLen = (uint8_t)sizeof(stackToFIFOData);
			continue;
		}

		/* Now we want to push the item we removed from the stack onto the
		 * FIFO.  To do this, we need to update the packet's time stamp so
		 * that it represents the absolute Unix time at which the
		 * measurement was taken or received from the node. */
		if (!unixTime_getSeconds(&unixTime)) {
			/* Because Unix time is unknown, we cannot move the item to the
			 * FIFO, so we increment the discarded stack item counter.  This
			 * counter's only purpose is to inform the server, to the best
			 * degree possible, how many S&F items have been accidently lost. */
			m_noinitStackData.discardedStackCount++;

			/* Reset the data length variable to its maximum before the
			 * next call to sfBuf_popBB(). */
			stackToFIFODataLen = (uint8_t)sizeof(stackToFIFOData);
			continue;
		}

		/* Because we are placing the packet in the flash-based sto/fwd
		 * buffer, we update the time stamp to reflect the Unix time at
		 * which the local measurement was taken or the node packet was
		 * received.  We do this by subtracting the packet's age from the
		 * current Unix time. */
		stackToFIFOData.timeStamp = unixTime -
				((get_time_100ms() - stackToFIFOData.timeStamp) / 10);

		if (!sfFlashBuf_push((uint8_t *)&stackToFIFOData, (uint8_t)stackToFIFODataLen)) {
			/* Because were were unable to push the item removed from the
			 * stack into the FIFO, we increment the discarded stack item
			 * counter.  This counter's only purpose is to inform the
			 * server, to the best degree possible, how many S&F items
			 * have been accidently lost. */
			m_noinitStackData.discardedStackCount++;
			/* Reset the data length variable to its maximum before the
			 * next call to sfBuf_popBB(). */
			stackToFIFODataLen = (uint8_t)sizeof(stackToFIFOData);
			continue;
		}

		movedItemCount++;

		/* Reset the data length variable to its maximum before the
		 * next call to sfBuf_popBB(). */
		stackToFIFODataLen = (uint8_t)sizeof(stackToFIFOData);
	}

	if (popAttempts >= initialStackCount + 10) {
		log_error("Error: attempted to pop 10 more items from the S&F stack than the number initially counted!");
	}

	log_infof("Moved %u of %u items from S&F stack to FIFO, all other items discarded", movedItemCount, initialStackCount);

	return true;

  }


bool stoFwd_flush() {
	if (!stoFwdInitialized) {
		return false;
	}


	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		sfBuf_flush(stoFwdStack);
		sfFlashBuf_flush();
	}

	m_noinitStackData.stackCount = 0;

	return true;
}
