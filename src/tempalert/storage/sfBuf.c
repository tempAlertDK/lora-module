#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "utils.h"
#include "globals.h"
#include "defines.h"
#include "sfBuf.h"
#include "log.h"

static void sfBuf_circBufPeek(const void *circBuf, sfBufSize_t circBufSize, 
		sfBufSize_t startIndex, void* outData, sfBufItemSize_t dataSize);

static void sfBuf_circBufPush(void *circBuf, sfBufSize_t cirBufSize, 
		sfBufSize_t startIndex, const void *inData, 
		sfBufItemSize_t dataSize);

bool sfBuf_recoverPostReset(const sfBuf_t *buf, resetReason_t resetReason) {
	if (((resetReason == RESET_REASON_EXTERNAL) ||
		(resetReason == RESET_REASON_EXITING_SIGNAL_FINDER_MODE ) ||
		(resetReason == RESET_REASON_WATCHDOG_EMBER_NONRESPONSIVE) ||
		(resetReason == RESET_REASON_WATCHDOG_EMBER_FAILED_TO_SET_NETWORK_KEY) ||
		(resetReason == RESET_REASON_WATCHDOG_EMBER_FAILED_TO_SET_BROADCAST_KEY)) &&
		sfBuf_validate(buf)) {
		return true;
	}
	
	return false;
}

bool sfBuf_init(void *memory, sfBufSize_t memSize) {
	sfBuf_t *buf;
	
	/* If the size of the provided memory region is not larger than the size of
	 * the book keeping data structure (so that we can store at least one byte
	 * in the buffer), we return failure. */
	if (memSize <= (sfBufSize_t)sizeof(sfBuf_t)) {
		return false;
	}
	
	/* For easy of access through a struct, we cast the provided memory block 
	 * as a sfBuf_t structure. */
	buf = (sfBuf_t *)memory;
	
	/* We use the first several bytes of memory to hold the book-keeping info.
	 * The remainder of memory is used to store the data. */
		
	/* The size of the data section is the total amount of memory allocated to
	 * the buffer minus the size of the book-keeping data. */
	buf->dataSize = memSize - (sfBufSize_t)sizeof(sfBuf_t);
	
	buf->empty = true;
	buf->full = false;
	
	/* When inserting or removing data from the buffer, we'll insert/remove 
	 * bytes from the buf->data[] array. The bbIndex represents the index in 
	 * the data array the back or bottom element resides.  The tfIndex is the 
	 * index at which the top or front element is stored.  Both the bb and tf
	 * indices point to the start of their elements.  */
	buf->bbIndex = 0;
	buf->tfIndex = 0;
	
	return true;
}


uint32_t sfBuf_getMaxItemSize() {
	return SFBUF_ITEM_SIZE_MAX;
}


bool sfBuf_isEmpty(sfBuf_t *buf) {
	return buf->empty;
}


uint32_t sfBuf_countItems(sfBuf_t *buf) {
	sfBufItemSize_t itemSize;
	sfBufSize_t nextItemIndex;
	uint32_t count = 0;
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if (buf->empty) {
			return 0;
		}

		/* We start counting items at the bottom/back index */
		nextItemIndex = buf->bbIndex;
		
		while(true) {
			/* If the index for the next item is the index of the item at the
			 * top/front of the buffer, we have iterated over all items in the
			 * buffer.  The only thing left to do is count the last item.*/
			if (nextItemIndex == buf->tfIndex) {
				count++;
				break;
			}
			
			/* Each item in the buffer is preceeded (and followed) by its 
			 * length.  This length may span several bytes, and it may be split
			 * across the "end" of the buffer, so we use the circBufPeek 
			 * function to read it. */
			sfBuf_circBufPeek((void *)buf->data, buf->dataSize, 
					nextItemIndex, &itemSize, sizeof(itemSize));

			if (itemSize > 0) {
				count++;
			} else {
				break;
			}
			
			/* Now that we have the size of the item, we compute the starting
			 * index of the next item.  We add size of two sfBufItemSize_t
			 * variables because there is one on each side of the item's payload
			 * in the buffer. */
			if (buf->dataSize - (2*(sfBufSize_t)sizeof(sfBufItemSize_t) + itemSize) > nextItemIndex) {
				nextItemIndex = nextItemIndex + 2*(sfBufSize_t)sizeof(sfBufItemSize_t) + itemSize;
			} else {
				nextItemIndex = 2*(sfBufSize_t)sizeof(sfBufItemSize_t) + itemSize - (buf->dataSize - nextItemIndex);
			}
		}
	}
	
	return count;
}


sfBufSize_t sfBuf_getAvailSpace(sfBuf_t *buf) {
	sfBufSize_t usedSpace, availSpace;
	sfBufItemSize_t tfItemSize;
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		/* Handle the easy cases in which inIndex = outIndex */
		if (buf->empty) {
			return buf->dataSize;
		} else if (buf->full) {
			return 0;
		}

		/* Get the size of the top/front item */
		sfBuf_circBufPeek((void *)buf->data, buf->dataSize, buf->tfIndex, &tfItemSize, 
				sizeof(tfItemSize));
		
		/* The top/front index is always "ahead of" the bottom/back index, but
		 * because we're using a circular buffer, tfIndex is not always 
		 * numerically larger than bbIndex. */
		if (buf->tfIndex >= buf->bbIndex) {
			/* When the tfIndex has not looped around "behind" the bbIndex, the
			 * amount of used space is the difference between the two indices 
			 * plus the amount of space consumed by the top/front item (be sure
			 * to count the space consumed by the size fields which bookend the 
			 * element's payload). Therefore, the amount of free space is the 
			 * total space minus the used space. */
			usedSpace = buf->tfIndex - buf->bbIndex;
			usedSpace += tfItemSize + 2*(sfBufSize_t)sizeof(sfBufItemSize_t);
			availSpace = buf->dataSize - usedSpace;
		} else {
			/* When the top/front index has looped around the end of the 
			 * circular buffer and is now numerically less than the bottom/back
			 * index, the remaining free space is the number of bytes between 
			 * the bbIndex and the tfIndex minus the number of bytes consumed by
			 * the top/front item */
			availSpace = buf->bbIndex - buf->tfIndex;
			availSpace -= tfItemSize + 2*(sfBufSize_t)sizeof(sfBufItemSize_t);
		}
		
		return availSpace;
	}
	
	return 0;
}

bool sfBuf_validate(const sfBuf_t *buf) {
	bool valid = true;
	sfBufSize_t index;
	sfBufSize_t leadingSizeIndex, trailingSizeIndex;
	sfBufItemSize_t itemSizeLeading, itemSizeTrailing;
	sfBufSize_t iterations;
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		/* Verify that the full and empty flags are not both set */
		if ((buf->full) && (buf->empty)) {
			log_error("Both the full and empty flags are set in sfBuf_validate()!");
			valid &= false;
		}

		/* Verify that the data size is non-zero */
		if (buf->dataSize <= 0) {
			log_error("Data size is less than or equal to 0 in sfBuf_validate()!");
			valid &= false;
		}

		/* Verify that the raw data buffer points to a valid memory location */
		if (buf->data == NULL) {
			log_error("Data array pointer is null in sfBuf_validate()!");
			valid &= false;
		}
	
		/* Verify that the bottom/back index is less than the buffer's size */
		if (buf->bbIndex >= buf->dataSize) {
			log_errorf("Bottom/back index (%u) is greater than or equal to the buffer size (%u bytes) in sfBuf_validate()!", buf->bbIndex, buf->dataSize);
			valid &= false;
		}
	
		/* Verify that the top/front index is less than the buffer's size */
		if (buf->tfIndex >= buf->dataSize) {
			log_errorf("Top/front index (%u) is greater than or equal to the buffer size (%u bytes) in sfBuf_validate()!", buf->tfIndex, buf->dataSize);
			valid &= false;
		}
	
		/* Verify that if the buffer is marked empty that the top/front index matches the bottom/back index */
		if ((buf->empty) && (buf->bbIndex != buf->tfIndex)) {
			log_errorf("Buffer is marked empty but the bottom/back index (%u) differs from the top/front index (%u) in sfBuf_validate()!", buf->bbIndex, buf->tfIndex);
			valid &= false;
		}
	
		/* Read the leading size of the item at the top/front of the buffer */
		sfBuf_circBufPeek((void *)buf->data, buf->dataSize, buf->tfIndex, &itemSizeLeading, sizeof(itemSizeLeading));
		
		/* Compute the index at which the next item would inserted at the top/front of the buffer */
		if (buf->dataSize - (2*(sfBufSize_t)sizeof(sfBufItemSize_t) + itemSizeLeading) > buf->tfIndex) {
			index = buf->tfIndex + 2*(sfBufSize_t)sizeof(sfBufItemSize_t) + itemSizeLeading;
		} else {
			index = 2*(sfBufSize_t)sizeof(sfBufItemSize_t) + itemSizeLeading - (buf->dataSize - buf->tfIndex);
		}
	
		/* Verify that if the buffer is marked full that the top/front element abuts the bottom/back element */
		if ((buf->full) && (index != buf->bbIndex)) {
			log_errorf("Buffer is marked full but the %u byte top/front element at index %u does not abut the bottom/back element at index %u in sfBuf_validate()!", itemSizeLeading, buf->tfIndex, buf->bbIndex);
			valid &= false;		
		}
	
		iterations = 0;
		leadingSizeIndex = buf->bbIndex;
		while (!buf->empty && (iterations++ < buf->dataSize)) {
			/* Each item in the buffer is preceded (and followed) by its length.  
			 * This length may span several bytes, and it may be split across the 
			 * "end" of the buffer, so we use the circBufPeek function to read it.*/
			sfBuf_circBufPeek((void *)buf->data, buf->dataSize, leadingSizeIndex, 
					&itemSizeLeading, sizeof(itemSizeLeading));
		
			/* Compute the location of the trailing size field */
			if (buf->dataSize - ((sfBufSize_t)sizeof(sfBufItemSize_t) + itemSizeLeading) > leadingSizeIndex) {
				trailingSizeIndex = leadingSizeIndex + (sfBufSize_t)sizeof(sfBufItemSize_t) + itemSizeLeading;
			} else {
				trailingSizeIndex = (sfBufSize_t)sizeof(sfBufItemSize_t) + itemSizeLeading - (buf->dataSize - leadingSizeIndex);
			}
		
			/* Read the trailing size field */
			sfBuf_circBufPeek((void *)buf->data, buf->dataSize, trailingSizeIndex,
					&itemSizeTrailing, sizeof(itemSizeTrailing));
		
			/* Verify that the leading size field matches the trailing size field */
			if (itemSizeLeading != itemSizeTrailing) {
				log_errorf("Leading size field (%u) of element at index %u does not match trailing size field (%u) at index %u in sfBuf_validate()!", itemSizeLeading, leadingSizeIndex, itemSizeTrailing, trailingSizeIndex);
				valid &= false;
				break;
			}
		
			/* If we just iterated over the top/front element, we've successfully
			 * iterated over all elements in the buffer, so we stop. */
			if (leadingSizeIndex == buf->tfIndex) {
				break;
			}
		
			/* Otherwise, we compute the location of the next element's leading 
			 * size field */
			if (buf->dataSize - (sfBufSize_t)sizeof(sfBufItemSize_t) > trailingSizeIndex) {
				leadingSizeIndex = trailingSizeIndex + (sfBufSize_t)sizeof(sfBufItemSize_t);
			} else {
				leadingSizeIndex = (sfBufSize_t)sizeof(sfBufItemSize_t) - (buf->dataSize - trailingSizeIndex);
			}
		}
	
		if (!valid) {
			log_error("Failed to validate RAM-based S&F stack!");
		}
	}
	
	if (valid) {
		log_debug("Successfully validated RAM-based S&F stack");
	}
	
	return valid;
}

void sfBuf_flush(sfBuf_t *buf) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		buf->bbIndex = 0;
		buf->tfIndex = 0;
		buf->full = false;
		buf->empty = true;
	}
	
	sfBuf_validate(buf);
}

bool sfBuf_discardBB(sfBuf_t *buf) {
	sfBufItemSize_t bbItemSizeLeading;
	sfBufItemSize_t bbItemSizeTrailing;

#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
	sfBufSize_t index;
#endif
	
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
	log_debug("sfBuf_discardBB() is being called to remove the bottom/back item");
	log_debugf("  full: %u", buf->full);
	log_debugf("  empty: %u", buf->empty);
	log_debugf("  bbIndex: %u", buf->bbIndex);
	log_debugf("  tfIndex: %u", buf->tfIndex);
	log_debugf("  dataSize: %u", buf->dataSize);
	log_debugf("  data[]: %p", buf->data);
#endif
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if (buf->empty) {
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
			log_debug("sfBuf_discardBB() failed because buffer is empty");
			log_debugf("  full: %u", buf->full);
			log_debugf("  empty: %u", buf->empty);
			log_debugf("  bbIndex: %u", buf->bbIndex);
			log_debugf("  tfIndex: %u", buf->tfIndex);
			log_debugf("  dataSize: %u", buf->dataSize);
			log_debugf("  data[]: %p\r\n", buf->data);
#endif
			return false;
		}
		
		if (buf->bbIndex == buf->tfIndex) {
			/* If there is only one item in the buffer, we set the empty flag.
			 * When the empty flag is set, it indicates that neither index 
			 * points to a valid element.  Updating the bbIndex in this case
			 * is pointless as there is no longer a valid element in the buffer.
			 */
			buf->empty = true;
			buf->full = false;
			
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
			log_debug("sfBuf_discardBB() succeeded in removing the only item");
			log_debugf("  full: %u", buf->full);
			log_debugf("  empty: %u", buf->empty);
			log_debugf("  bbIndex: %u", buf->bbIndex);
			log_debugf("  tfIndex: %u", buf->tfIndex);
			log_debugf("  dataSize: %u", buf->dataSize);
			log_debugf("  data[]: %p\r\n", buf->data);
#endif
			
			return sfBuf_validate(buf);
		}
		
		/* Read the leading length of the old bottom/back element */
		sfBuf_circBufPeek((void *)buf->data, buf->dataSize, 
				buf->bbIndex, &bbItemSizeLeading, sizeof(bbItemSizeLeading));
				
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
		/* Compute the index of the trailing length of the old bottom/back element */
		if (buf->dataSize - ((sfBufSize_t)sizeof(sfBufItemSize_t) + bbItemSizeLeading) > buf->bbIndex) {
			index = buf->bbIndex + (sfBufSize_t)sizeof(sfBufItemSize_t) + bbItemSizeLeading;
		} else {
			index = (sfBufSize_t)sizeof(sfBufItemSize_t) + bbItemSizeLeading - (buf->dataSize - buf->bbIndex);
		}
#endif
				
		/* Read the trailing length of the old bottom/back element */
		sfBuf_circBufPeek((void *)buf->data, buf->dataSize, 
				buf->bbIndex, &bbItemSizeTrailing, sizeof(bbItemSizeTrailing));
				
		/* Compare the leading and trailing lengths of the old bottom/back element */
		if (bbItemSizeLeading != bbItemSizeTrailing) {
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
			log_errorf("Leading size field (%u at index %u) of old bottom/back item does not match trailing size field (%u at index %u) in sfBuf_discardBB()!", bbItemSizeLeading, buf->bbIndex, bbItemSizeTrailing, index);
#endif
		}
		
		/* To effectively remove the item from the bottom/back of the buffer,
		 * all we have to do is update the bottom/back index.  To do this, we 
		 * add twice the size of the length field, (because it is stored both 
		 * before and after each payload element), and the size of the payload
		 * itself to the current bbIndex.  Then, we use modulo arithmetic to 
		 * wrap the index around the end of the buffer back to the beginning if
		 * necessary. */
		if (buf->dataSize - (2*(sfBufSize_t)sizeof(sfBufItemSize_t) + bbItemSizeLeading) > buf->bbIndex) {
			buf->bbIndex = buf->bbIndex + 2*(sfBufSize_t)sizeof(sfBufItemSize_t) + bbItemSizeLeading;
		} else {
			buf->bbIndex = 2*(sfBufSize_t)sizeof(sfBufItemSize_t) + bbItemSizeLeading - (buf->dataSize - buf->bbIndex);
		}
		
		/* Read the leading length of the new bottom/back element */
		sfBuf_circBufPeek((void *)buf->data, buf->dataSize, 
				buf->bbIndex, &bbItemSizeLeading, sizeof(bbItemSizeLeading));
				
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
		/* Compute the index of the trailing length of the new bottom/back element */
		if (buf->dataSize - ((sfBufSize_t)sizeof(sfBufItemSize_t) + bbItemSizeLeading) < buf->bbIndex) {
			index = buf->bbIndex + (sfBufSize_t)sizeof(sfBufItemSize_t) + bbItemSizeLeading;
		} else {
			index = (sfBufSize_t)sizeof(sfBufItemSize_t) + bbItemSizeLeading - (buf->dataSize - buf->bbIndex);
		}
#endif
				
		/* Read the trailing length of the new bottom/back element */
		sfBuf_circBufPeek((void *)buf->data, buf->dataSize, 
				buf->bbIndex, &bbItemSizeTrailing, sizeof(bbItemSizeTrailing));
				
		/* Compare the leading and trailing lengths of the new bottom/back element */
		if (bbItemSizeLeading != bbItemSizeTrailing) {
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
			log_errorf("Leading size field (%u at index %u) of new bottom/back item does not match trailing size field (%u at index %u) in sfBuf_discardBB()!", bbItemSizeLeading, buf->bbIndex, bbItemSizeTrailing, index);
#endif
		}
		
		/* We just removed an element from the buffer, so it cannot be full any 
		 * longer. */
		buf->full = false;
	}
	
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
	log_debugf("sfBuf_discardBB() succeeded in removing a %u byte item", bbItemSizeLeading);
	log_debugf("  full: %u", buf->full);
	log_debugf("  empty: %u", buf->empty);
	log_debugf("  bbIndex: %u", buf->bbIndex);
	log_debugf("  tfIndex: %u", buf->tfIndex);
	log_debugf("  dataSize: %u", buf->dataSize);
	log_debugf("  data[]: %p\r\n", buf->data);
#endif
	
	return sfBuf_validate(buf);
}

bool sfBuf_discardTF(sfBuf_t *buf) {
	sfBufItemSize_t tfItemSizeTrailing;
	sfBufItemSize_t tfItemSizeLeading;
	sfBufSize_t trailingSizeIndex;
	
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
	log_debug("sfBuf_discardTF() is being called to remove the top/front item");
	log_debugf("  full: %u", buf->full);
	log_debugf("  empty: %u", buf->empty);
	log_debugf("  bbIndex: %u", buf->bbIndex);
	log_debugf("  tfIndex: %u", buf->tfIndex);
	log_debugf("  dataSize: %u", buf->dataSize);
	log_debugf("  data[]: %p", buf->data);
#endif
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if (buf->empty) {
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
			log_debug("sfBuf_discardTF() failed because buffer is empty");
			log_debugf("  full: %u", buf->full);
			log_debugf("  empty: %u", buf->empty);
			log_debugf("  bbIndex: %u", buf->bbIndex);
			log_debugf("  tfIndex: %u", buf->tfIndex);
			log_debugf("  dataSize: %u", buf->dataSize);
			log_debugf("  data[]: %p\r\n", buf->data);
#endif
			return false;
		}
		
		if (buf->tfIndex == buf->bbIndex) {
			/* If there is only one item in the buffer, we set the empty flag.
			 * When the empty flag is set, it indicates that neither index
			 * points to a valid element.  Updating the tfIndex in this case
			 * is pointless as there is no longer a valid element in the buffer.
			 */
			buf->empty = true;
			buf->full = false;
			
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
			log_debug("sfBuf_discardTF() succeeded in removing the only item");
			log_debugf("  full: %u", buf->full);
			log_debugf("  empty: %u", buf->empty);
			log_debugf("  bbIndex: %u", buf->bbIndex);
			log_debugf("  tfIndex: %u", buf->tfIndex);
			log_debugf("  dataSize: %u", buf->dataSize);
			log_debugf("  data[]: %p\r\n", buf->data);
#endif
					
			return sfBuf_validate(buf);
		}
		
		/* The top/front index points to the item at the top/front of the array
		 * which makes up the buffer.  Each item stored in the buffer is both
		 * preceded and followed by the item's size.  */
		
		/* We compute the index of the trailing size byte of the item that is 
		 * just behind the item at the top/front of the buffer. */
		if (buf->tfIndex >= (sfBufSize_t)sizeof(sfBufItemSize_t)) {
			trailingSizeIndex = buf->tfIndex - (sfBufSize_t)sizeof(sfBufItemSize_t);
		} else {
			trailingSizeIndex = buf->dataSize - ((sfBufSize_t)sizeof(sfBufItemSize_t) - buf->tfIndex);
		}
		
		/* Read the trailing length of the size of the item just behind the item at the
		 * top/front of the buffer and store this length in the tfItemSizeTrailing
		 * variable because that item is about to become the top/front item. */
		sfBuf_circBufPeek((void *)buf->data, buf->dataSize, 
				trailingSizeIndex, &tfItemSizeTrailing, sizeof(tfItemSizeTrailing));
				
		/* To remove the item that used to be the top/front item in the buffer,
		 * all we have to do is update the top/front index.  To do this, we 
		 * subtract twice the size of the length field, (because it is stored 
		 * both before and after each payload element), and the size of the 
		 * payload of the element that is about become the new top/front element
		 * from the current top/front index. */
		if (buf->tfIndex >= 2*(sfBufSize_t)sizeof(sfBufItemSize_t) + tfItemSizeTrailing)  {
			/* If the top/front index will still be non-negative after
			 * subtracting the size of the two item size fields and the size of
			 * the old top/front item, perform the subtraction to compute the 
			 * new top/front index. */
			buf->tfIndex = buf->tfIndex - (2*(sfBufSize_t)sizeof(sfBufItemSize_t) + tfItemSizeTrailing);
		} else {
			buf->tfIndex = buf->dataSize - ((2*(sfBufSize_t)sizeof(sfBufItemSize_t) + tfItemSizeTrailing) - buf->tfIndex);
		}
				
		/* Read the leading length of the new top/front item and store this 
		 * length in the tfItemSizeLeading variable because for comparison with
		 * the tfItemSizeTrailing variable.. */
		sfBuf_circBufPeek((void *)buf->data, buf->dataSize, 
				buf->tfIndex, &tfItemSizeLeading, sizeof(tfItemSizeLeading));		
		
		/* Verify that the leading size field matches the trailing size field */
		if (tfItemSizeLeading != tfItemSizeTrailing) {
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
			log_errorf("Leading size field (%u at index %u) of new top/front item does not match trailing size field (%u at index %u) in sfBuf_discardTF()!", tfItemSizeLeading, buf->tfIndex, tfItemSizeTrailing, trailingSizeIndex);
#endif
		}
				
		/* We just removed an element from the buffer, so it cannot be full any 
		 * longer. */
		buf->full = false;
	}
	
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
	log_debugf("sfBuf_discardTF() succeeded in removing a %u byte item", tfItemSizeTrailing);
	log_debugf("  full: %u", buf->full);
	log_debugf("  empty: %u", buf->empty);
	log_debugf("  bbIndex: %u", buf->bbIndex);
	log_debugf("  tfIndex: %u", buf->tfIndex);
	log_debugf("  dataSize: %u", buf->dataSize);
	log_debugf("  data[]: %p\r\n", buf->data);
#endif
	
	return sfBuf_validate(buf);
}

bool sfBuf_forcePushBB(sfBuf_t *buf, const void *item, sfBufItemSize_t size) {
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
	log_debug("sfBuf_forcePushBB() has been called\r\n");
#endif
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		/* Do not make any attempt to place the item in the buffer if the item's 
		 * size exceeds the total size of the buffer. */
		if (size > buf->dataSize) {
			log_errorf("Item size (%u bytes) exceeds size of the entire buffer (%u bytes), so it cannot be pushed by sfBuf_forcePushBB()!\r\n", size, buf->dataSize);
			return false;
		}
		
		/* If we can successfully push the item into the bottom/back of the 
		 * buffer, we return success. */
		if (sfBuf_tryPushBB(buf, item, size)) {
			return true;
		}
		
		/* While there is not enough room in the buffer for the new element plus
		 * the size fields which bookend it, we discard items from the top/front
		 * of the buffer. */
		while(size + 2*(sfBufSize_t)sizeof(sfBufItemSize_t) > sfBuf_getAvailSpace(buf)) {
			sfBuf_discardTF(buf);
		}
			
		/* Now that we think there is enough room in the buffer for the new 
		 * element, make one final attempt to insert it. */
		if (sfBuf_tryPushBB(buf, item, size)) {
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
			log_debug("sfBuf_forcePushBB() succeeded\r\n");
#endif
			return true;
		}
	}
	
	log_error("sfBuf_forcePushBB() failed!\r\n");
	
	return false;
}

bool sfBuf_forcePushTF(sfBuf_t *buf, const void *item, sfBufItemSize_t size) {
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
	DEBUGMSG(ALL_DEBUG,
			LOGGER(0,"sfBuf_forcePushTF() has been called\r\n");
	);
#endif
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		/* Do not make any attempt to place the item in the buffer if the item's 
		 * size exceeds the total size of the buffer. */
		if (size > buf->dataSize) {
			log_errorf("Item size (%u bytes) exceeds size of the entire buffer (%u bytes), so it cannot be pushed by sfBuf_forcePushTF()!\r\n", size, buf->dataSize);
			return false;
		}
		
		/* If we can successfully push the item into the top/front of the 
		 * buffer, we return success. */
		if (sfBuf_tryPushTF(buf, item, size)) {
			return true;
		}
		
		/* While there is not enough room in the buffer for the new element plus
		 * the size fields which bookend it, we discard items from the back/
		 * bottom of the buffer. */
		while(size + 2*(sfBufSize_t)sizeof(sfBufItemSize_t) > sfBuf_getAvailSpace(buf)) {
			sfBuf_discardBB(buf);
		}
			
		/* Now that we think there is enough room in the buffer for the new 
		 * element, make one final attempt to insert it. */
		if (sfBuf_tryPushTF(buf, item, size)) {
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
			log_debug("sfBuf_forcePushTF() succeeded\r\n");
#endif
			return true;
		}
	}
	
	log_error("sfBuf_forcePushTF() failed!\r\n");
	
	return false;
}


bool sfBuf_tryPushBB(sfBuf_t *buf, const void *item, sfBufItemSize_t size) {
	sfBufSize_t index;
	sfBufItemSize_t tfItemSize;
	
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
	log_debugf("sfBuf_tryPushbb() is being called to insert a %u byte item", size);
	log_debugf("  full: %u", buf->full);
	log_debugf("  empty: %u", buf->empty);
	log_debugf("  bbIndex: %u", buf->bbIndex);
	log_debugf("  tfIndex: %u", buf->tfIndex);
	log_debugf("  dataSize: %u", buf->dataSize);
	log_debugf("  data[]: %p\r\n", buf->data);
#endif
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		/* If the size of the item we're trying to add plus the two size fields
		 * which bookend it exceed the amount of free space available in the
		 * buffer, we return false. */
		if (size + 2*(sfBufSize_t)sizeof(size) > sfBuf_getAvailSpace(buf)) {
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
			log_debugf("sfBuf_tryPushbb() failed to insert %u byte item because the item's size plus two bookends (%u bytes each) is greater than the %u bytes available\r\n", size, sizeof(size), sfBuf_getAvailSpace(buf));
#endif
			return false;
		}
		
		/* Insert the trailing length field immediately before the current 
		 * bottom/back element. */
		if (buf->bbIndex >= (sfBufSize_t)sizeof(sfBufItemSize_t)) {
			index = buf->bbIndex - (sfBufSize_t)sizeof(sfBufItemSize_t);
		} else {
			index = buf->dataSize - ((sfBufSize_t)sizeof(sfBufItemSize_t) - buf->bbIndex);
		} 
		sfBuf_circBufPush((void *)buf->data, buf->dataSize, index, &size, 
				sizeof(size));
		
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
		log_debugf("sfBuf_tryPushBB() wrote trailing length field to buffer starting at index %u", index);
#endif
		
		/* Next insert the new payload immediately before the trailing length
		 * field. */
		if (index >= size) {
			index = index - size;
		} else {
			index = buf->dataSize - (size - index);
		}
		sfBuf_circBufPush((void *)buf->data, buf->dataSize, index, item, 
				size);
		
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
		log_debugf("sfBuf_tryPushBB() wrote payload to buffer starting at index %u", index);
#endif
		
		/* Now insert the leading length field immediately before the new
		 * payload. */
		if (index >= (sfBufSize_t)sizeof(sfBufItemSize_t)) {
			index = index - (sfBufSize_t)sizeof(sfBufItemSize_t);
		} else {
			index = buf->dataSize - ((sfBufSize_t)sizeof(sfBufItemSize_t) - index);
		}
		sfBuf_circBufPush((void *)buf->data, buf->dataSize, index, &size, 
				sizeof(size));
		
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
		log_debugf("sfBuf_tryPushBB() wrote leading length field to buffer starting at index %u", index);
#endif
		
		/* Update the buffer's bottom/back index pointer */
		buf->bbIndex = index;
		
		/* If the buffer was empty before we inserted this item, we need to move
		 * the top/front pointer to the same index as the bottom/back pointer. 
		 * We do this because the top/front pointer should always point to the
		 * index of the item at the top/front of the buffer, not the index of
		 * the next available location. */
		if (buf->empty) {
			buf->tfIndex = index;
		}
	
		/* We just added an element to the buffer, so it cannot be empty. */
		buf->empty = false;
		
		/* The tfIndex pointer points to a valid element that has previously 
		 * been inserted, not the location of the next insertion.  Here, we 
		 * calculate the index of the next top/front insertion so that we can
		 * determine whether it is the same as the newly updated bottom/back
		 * pointer.  If the two pointers match, it means that the buffer is 
		 * full.  The index of the next insertion should never surpass the 
		 * bottom/back index because we verified that the buffer had enough room
		 * for the new element before we inserted it. */
		sfBuf_circBufPeek((void *)buf->data, buf->dataSize, buf->tfIndex, &tfItemSize, 
				sizeof(tfItemSize));

		if (buf->dataSize - (2*(sfBufSize_t)sizeof(sfBufItemSize_t) + tfItemSize) > buf->tfIndex) {
			/* If the index of the next top/front item can be computed without
			 * rolling around from the back of the buffer to the front, simply
			 * add the size of the two length fields and the current item size
			 * to the top/front pointer. */
			index = buf->tfIndex + 2*(sfBufSize_t)sizeof(sfBufItemSize_t) + tfItemSize;
		} else {
			index = 2*(sfBufSize_t)sizeof(sfBufItemSize_t) + tfItemSize - (buf->dataSize - buf->tfIndex);
		}
		
		/* If the next top/front insertion index matches the bottom/back index,
		 * the buffer must be full. */
		if (index == buf->bbIndex) {
			buf->full = true;
		}
	}
	

#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
	log_debugf("sfBuf_tryPushBB() succeeded in inserting %u byte item", size);
	log_debugf("  full: %u", buf->full);
	log_debugf("  empty: %u", buf->empty);
	log_debugf("  bbIndex: %u", buf->bbIndex);
	log_debugf("  tfIndex: %u", buf->tfIndex);
	log_debugf("  dataSize: %u", buf->dataSize);
	log_debugf("  data[]: %p\r\n", buf->data);
	);	
#endif
	
	return sfBuf_validate(buf);
}

bool sfBuf_tryPushTF(sfBuf_t *buf, const void *item, sfBufItemSize_t size) {
	sfBufSize_t index;
	sfBufItemSize_t tfItemSize;
	
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
	log_debugf("sfBuf_tryPushTF() is being called to insert a %u byte item", size);
	log_debugf("  full: %u", buf->full);
	log_debugf("  empty: %u", buf->empty);
	log_debugf("  bbIndex: %u", buf->bbIndex);
	log_debugf("  tfIndex: %u", buf->tfIndex);
	log_debugf("  dataSize: %u", buf->dataSize);
	log_debugf("  data[]: %p\r\n", buf->data);
#endif
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		/* If the size of the item we're trying to add plus the two size fields
		 * which bookend it exceed the amount of free space available in the
		 * buffer, we return false. */
		if (size + 2*(sfBufSize_t)sizeof(size) > sfBuf_getAvailSpace(buf)) {
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
			log_debugf(,"sfBuf_tryPushTF() failed to insert %u byte item because the item's size plus two bookends (%u bytes each) is greater than the %u bytes available\r\n", size, sizeof(size), sfBuf_getAvailSpace(buf));
#endif
			return false;
		}
		
		/* The current tfIndex pointer points to a valid element that has 
		 * previously been inserted, not the new location of the element we are
		 * about to insert.  Here, read the current top/front element's size 
		 * from the buffer so that we can compute the location of the next top/
		 * front element.  If the buffer is empty, we won't use this value.*/
		sfBuf_circBufPeek((void *)buf->data, buf->dataSize, buf->tfIndex, &tfItemSize, 
				sizeof(tfItemSize));
		
		
		/* Insert the leading length field immediately after the current 
		 * bottom/back element.  Each element's payload is bookended by two 
		 * item size fields which is why we add two times the size of a size
		 * field. */
		if (!buf->empty) {
			if (buf->dataSize - (2*(sfBufSize_t)sizeof(sfBufItemSize_t) + tfItemSize) > buf->tfIndex) {
				index = buf->tfIndex + 2*(sfBufSize_t)sizeof(sfBufItemSize_t) + tfItemSize;
			} else {
				index = 2*(sfBufSize_t)sizeof(sfBufItemSize_t) + tfItemSize - (buf->dataSize - buf->tfIndex);
			}
		} else {
			/* If the buffer is empty, the new element should be placed at the
			 * position currently makred as the top/front of the buffer. */
			index = buf->tfIndex;
		}
		sfBuf_circBufPush((void *)buf->data, buf->dataSize, index, &size, 
				sizeof(size));
				
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
		log_debugf("sfBuf_tryPushTF() wrote leading length field to buffer starting at index %u", index);
#endif
		
		/* The index of the leading length field is the index kept in the top/
		 * front index pointer.  */
		buf->tfIndex = index;
				
		/* Next insert the new payload immediately after the leading length
		 * field. */
		if (buf->dataSize - (sfBufSize_t)sizeof(sfBufItemSize_t) > index) {
			index = index + (sfBufSize_t)sizeof(sfBufItemSize_t);
		} else {
			index = (sfBufSize_t)sizeof(sfBufItemSize_t) - (buf->dataSize - index);
		}
		sfBuf_circBufPush((void *)buf->data, buf->dataSize, index, item, 
				size);
				
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
		log_debugf("sfBuf_tryPushTF() wrote payload to buffer starting at index %u", index);
#endif
		
		/* Now insert the trailing length field immediately after the new
		 * payload. */
		if (buf->dataSize - size > index) {
			/* If the result of adding the size of the new item to the current
			 * 'index' is less than the total size of the buffer, proceed with
			 * the simple addition to compute the new index. */
			index = index + size;
		} else {
			/* Otherwise, the new index is whatever portion of the data that
			 * cannot fit at the end of the buffer. */
			index = size - (buf->dataSize - index);
		}
		sfBuf_circBufPush((void *)buf->data, buf->dataSize, index, &size, 
				sizeof(size));
		
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
		log_debugf("sfBuf_tryPushTF() wrote trailing length field to buffer starting at index %u", index);
#endif
		
		/* We just added an element to the buffer, so it cannot be empty. */
		buf->empty = false;
		
		/* The tfIndex pointer points to the element that has just been 
		 * inserted, not the location of the next insertion.  Here, we 
		 * calculate the index of the next top/front insertion so that we can
		 * determine whether it is the same as the bottom/back pointer.  If the 
		 * two pointers match, it means that the buffer is full.  The index of 
		 * the next insertion should never surpass the bottom/back index because
		 * we verified that the buffer had enough room for the new element 
		 * before we inserted it. */
		if (buf->dataSize - (2*(sfBufSize_t)sizeof(sfBufItemSize_t) + tfItemSize) > buf->tfIndex) {
			/* If the index of the next top/front item can be computed without
			 * rolling around from the back of the buffer to the front, simply
			 * add the size of the two length fields and the current item size
			 * to the top/front pointer. */
			index = buf->tfIndex + 2*(sfBufSize_t)sizeof(sfBufItemSize_t) + size;
		} else {
			index = 2*(sfBufSize_t)sizeof(sfBufItemSize_t) + size - (buf->dataSize - buf->tfIndex);
		}
		
		/* If the next top/front insertion index matches the bottom/back index,
		 * the buffer must be full. */
		if (index == buf->bbIndex) {
			buf->full = true;
		}
	}
	
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
	log_debugf("sfBuf_tryPushTF() succeeded in inserting %u byte item", size);
	log_debugf("  full: %u", buf->full);
	log_debugf("  empty: %u", buf->empty);
	log_debugf("  bbIndex: %u", buf->bbIndex);
	log_debugf("  tfIndex: %u", buf->tfIndex);
	log_debugf("  dataSize: %u", buf->dataSize);
	log_debugf("  data[]: %p\r\n", buf->data);
#endif
	
	return sfBuf_validate(buf);
}


bool sfBuf_popBB(sfBuf_t *buf, void *item, sfBufItemSize_t *sizePtr) {
	bool success = false;
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		/* To pop an element from the bottom/back of the buffer, we first 
		 * peek at the element and then we discard it. */
		success = sfBuf_peekBB(buf, item, sizePtr);
		sfBuf_discardBB(buf);
	}
	
	return success;
}

bool sfBuf_popTF(sfBuf_t *buf, void *item, sfBufItemSize_t *sizePtr) {
	bool success = false;
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		/* To pop an element from the top/front of the buffer, we first 
		 * peek at the element and the we discard it. */
		success = sfBuf_peekTF(buf, item, sizePtr);
		sfBuf_discardTF(buf);
	}
	
	return success;
}

bool sfBuf_peekBB(sfBuf_t *buf, void *item, sfBufItemSize_t *sizePtr) {
	sfBufItemSize_t itemSizeLeading;
	sfBufItemSize_t itemSizeTrailing;
	sfBufSize_t payloadIndex;
	sfBufSize_t trailingSizeIndex;
	
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
	log_debugf("sfBuf_peekBB() is being called to peek at the bottom/back item");
#endif
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if (buf->empty) { 
			return false;
		}

		/* Each item in the buffer is preceded (and followed) by its length.  
		 * This length may span several bytes, and it may be split across the 
		 * "end" of the buffer, so we use the circBufPeek function to read it.*/
		sfBuf_circBufPeek((void *)buf->data, buf->dataSize, buf->bbIndex, 
				&itemSizeLeading, sizeof(itemSizeLeading));

		/* If the user-supplied item pointer does not point to a variable that
		 * is large enough to hold the item, we return failure. */
		if (itemSizeLeading > *sizePtr) {
			log_errorf("Item size (%u bytes) is larger than provided buffer (%u bytes) in sfBuf_peekBB()!\r\n", itemSizeLeading, *sizePtr);
			return false;
		}

		/* Compute the locating of the trailing size field */
		if (buf->dataSize - ((sfBufSize_t)sizeof(sfBufItemSize_t) + itemSizeLeading) > buf->bbIndex) {
			trailingSizeIndex = buf->bbIndex + (sfBufSize_t)sizeof(sfBufItemSize_t) + itemSizeLeading;
		} else {
			trailingSizeIndex = (sfBufSize_t)sizeof(sfBufItemSize_t) + itemSizeLeading - (buf->dataSize - buf->bbIndex);
		}

		/* Read the trailing size */
		sfBuf_circBufPeek((void *)buf->data, buf->dataSize, trailingSizeIndex,
				&itemSizeTrailing, sizeof(itemSizeTrailing));

		/* Verify that the leading size field matches the trailing size field */
		if (itemSizeLeading != itemSizeTrailing) {
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
			log_errorf("Leading size field (%u at index %u) of bottom/back item does not match trailing size field (%u at index %u) in sfBuf_peekBB()!\r\n", itemSizeLeading, buf->bbIndex, itemSizeTrailing, trailingSizeIndex);
#endif
			return false;
		}		

		/* The payloadIndex is the index in the circular buffer where the data 
		 * payload for the item we are peeking at actually begins; it is just 
		 * after the length information. */
		if (buf->dataSize - (sfBufSize_t)sizeof(sfBufItemSize_t) > buf->bbIndex) {
			payloadIndex = buf->bbIndex + (sfBufSize_t)sizeof(sfBufItemSize_t);
		} else {
			payloadIndex = (sfBufSize_t)sizeof(sfBufItemSize_t) - (buf->dataSize - buf->bbIndex);
		}

		/* It is possible that the item we're peeking at may be wrapped around 
		 * the upper end of the buffer and continue at index 0 of the data 
		 * array. Because this is a possibility, we use the circBufPeek function
		 * to retrieve it. */
		sfBuf_circBufPeek((void *)buf->data, buf->dataSize, payloadIndex, 
				item, itemSizeLeading);

		/* Update the sizePtr argument with the actual size of the item 
		 * retrieved from the buffer. */
		*sizePtr = itemSizeLeading;
	}
	
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
	log_debugf("sfBuf_peekBB() used to peek at %u byte bottom/back item at index %u\r\n", *sizePtr, buf->bbIndex);
#endif
	
	return true;
}

bool sfBuf_peekTF(sfBuf_t *buf, void *item, sfBufItemSize_t *sizePtr) {
	sfBufItemSize_t itemSizeLeading;
	sfBufItemSize_t itemSizeTrailing;
	sfBufSize_t payloadIndex;
	sfBufSize_t trailingSizeIndex;
	
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
	log_debugf("sfBuf_peekTF() is being called to peek at the top/front item");
#endif
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if (buf->empty) { 
			return false;
		}

		/* Each item in the buffer is preceded (and followed) by its length.  
		 * This length may span several bytes, and it may be split across the 
		 * "end" of the buffer, so we use the circBufPeek function to read it.*/
		sfBuf_circBufPeek((void *)buf->data, buf->dataSize, buf->tfIndex, 
				&itemSizeLeading, sizeof(itemSizeLeading));

		/* If the user-supplied item pointer does not point to a variable that
		 * is large enough to hold the item, we return failure. */
		if (itemSizeLeading > *sizePtr) {
			log_errorf("Item size (%u bytes) is larger than provided buffer (%u bytes) in sfBuf_peekTF()!\r\n", itemSizeLeading, *sizePtr);
			return false;
		}
		
		/* Compute the location of the trailing size field */
		if (buf->dataSize - ((sfBufSize_t)sizeof(sfBufItemSize_t) + itemSizeLeading) > buf->tfIndex) {
			trailingSizeIndex = buf->tfIndex + (sfBufSize_t)sizeof(sfBufItemSize_t) + itemSizeLeading;
		} else {
			trailingSizeIndex = (sfBufSize_t)sizeof(sfBufItemSize_t) + itemSizeLeading - (buf->dataSize - buf->tfIndex);
		}
		
		/* Read the trailing size field */
		sfBuf_circBufPeek((void *)buf->data, buf->dataSize, trailingSizeIndex,
				&itemSizeTrailing, sizeof(itemSizeTrailing));
		
		/* Verify that the leading size field matches the trailing size field */
		if (itemSizeLeading != itemSizeTrailing) {
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
			log_errorf("Leading size field (%u at index %u) of bottom/back item does not match trailing size field (%u at index %u) in sfBuf_peekTF()!\r\n", itemSizeLeading, buf->tfIndex, itemSizeTrailing, trailingSizeIndex);
#endif
			return false;
		}			

		/* The payloadIndex is the index in the circular buffer where the data 
		 * payload for the item we are peeking at actually begins; it is just 
		 * after the length information. */
		if (buf->dataSize - (sfBufItemSize_t)sizeof(sfBufItemSize_t) > buf->tfIndex) {
			payloadIndex = buf->tfIndex + (sfBufSize_t)sizeof(sfBufItemSize_t);
		} else {
			payloadIndex = (sfBufSize_t)sizeof(sfBufItemSize_t) - (buf->dataSize - buf->tfIndex);
		}

		/* It is possible that the item we're peeking at may be wrapped around 
		 * the upper end of the buffer and continue at index 0 of the data 
		 * array. Because this is a possibility, we use the circBufPeek function
		 * to retrieve it. */
		sfBuf_circBufPeek((void *)buf->data, buf->dataSize, payloadIndex, 
				item, itemSizeLeading);

		/* Update the sizePtr argument with the actual size of the item 
		 * retrieved from the buffer. */
		*sizePtr = itemSizeLeading;
	}
	
#if defined(DEBUG_SFBUF) && (DEBUG_SFBUF == 1)
	log_debugf("sfBuf_peekTF() used to peek at %u byte top/front item at index %u\r\n", *sizePtr, buf->tfIndex);
#endif
	
	return true;
}


/* The sfBuf_circBufPeek function uses memcpy to very quickly read dataSize 
 * bytes out of a circular buffer starting at startIndex into a provided null
 * pointer, outData. Note: this function is not interrupt-safe and if the 
 * circular buffer is also modified by an interrupt routine, the caller must
 * ensure that interrupts are disabled before calling this function. */
void sfBuf_circBufPeek(const void *circBuf, sfBufSize_t circBufSize, 
		sfBufSize_t startIndex, void* outData, sfBufItemSize_t dataSize) {
	sfBufItemSize_t partialLength[2];
	
	/* If the caller got lazy and provided us with a index to the buffer that
	 * exceeds its dimensions, we assume that it should be wrapped around to the
	 * front of the buffer. */
	if (startIndex >= circBufSize) {
		startIndex = startIndex % circBufSize;
	}
	
	/* If the data we've been asked to retrieve from the circular buffer is 
	 * large enough that it wraps around the end of the buffer back to the 
	 * beginning, we'll need to read it from the buffer in two chunks.   */
	if (startIndex + dataSize > circBufSize) {
		/* The first chunk of data that we copy to the provided outData 
		 * pointer is the data from the starting index to the end of the 
		 * buffer. */
		partialLength[0] = circBufSize - startIndex;
		/* The second chunk of data is whatever amount remains after the 
		 * first chunk has been copied. */
		partialLength[1] = dataSize - partialLength[0];

		memcpy(outData, circBuf + startIndex, partialLength[0]);
		memcpy(outData + partialLength[0], circBuf, partialLength[1]);
	} else {
		memcpy(outData, circBuf + startIndex, dataSize);
	}
}


/* The sfBuf_circBufPush function uses memcpy to very quickly write dataSize 
 * bytes into a circular buffer starting at startIndex from a provided null
 * pointer, inData. Note: this function is not interrupt-safe and if the 
 * circular buffer is also modified by an interrupt routine, the caller must
 * ensure that interrupts are disabled before calling this function. */
void sfBuf_circBufPush(void *circBuf, sfBufSize_t circBufSize, 
		sfBufSize_t startIndex, const void *inData, 
		sfBufItemSize_t dataSize) {
	sfBufItemSize_t partialLength[2];
	
	/* If the caller got lazy and provided us with a index to the buffer that
	 * exceeds its dimensions, we assume that it should be wrapped around to the
	 * front of the buffer. */
	if (startIndex >= circBufSize) {
		startIndex = startIndex % circBufSize;
	}
	
	if (startIndex + dataSize > circBufSize) {
		partialLength[0] = circBufSize - startIndex;
		partialLength[1] = dataSize - partialLength[0];

		memcpy(circBuf + startIndex, inData, partialLength[0]);
		memcpy(circBuf, inData + partialLength[0], partialLength[1]);
	} else {
		memcpy(circBuf + startIndex, inData, dataSize);
	}
}
