/*
 * stoFwd.h
 *
 *  Created on: Aug 4, 2014
 *      Author: kwgilpin
 */

#ifndef STOFWDFIFO_H_
#define STOFWDFIFO_H_

#include <stdint.h>
#include <stdbool.h>


#define STOFWDFIFO_STATUS_TEMP_PRESENT_POSITION		0
#define STOFWDFIFO_STATUS_HUMIDITY_PRESENT_POSITION 1
#define STOFWDFIFO_STATUS_LIGHT_PRESENT_POSITION	2

/* The status byte is used to indicate both whether the record contains valid
 * data and what type of data it contains.  The MSb is cleared when data is
 * first stored in the record.  The other 7 bits are left set or cleared to
 * indicate what type of data the record contains.  For example, the LSb is
 * left set to indicate that the record contains a temperature measurement.
 * Once the record has been popped from the FIFO, all bits are cleared to
 * indicate that the record is no longer valid. */
typedef struct __attribute__ ((__packed__)) {
	uint8_t status;
	uint8_t timestamp_min[3];
	uint8_t data[4];
} stoFwdFifo_record_t;

bool stoFwdFifo_init(void (*stoFwdFifo_initCompleteCallback)(bool success));

uint32_t stoFwdFifo_getFreeRecordCount(void);
uint32_t stoFwdFifo_getActiveRecordCount(void);
uint32_t stoFwdFifo_getLostRecordCount(void);
void stoFwdFifo_resetLostRecordCount(void);
uint32_t stoFwdFifo_getOccupiedSize(void);
uint32_t stoFwdFifo_getTotalSize(void);
uint32_t stoFwdFifo_getInRecNumber(void);
uint32_t stoFwdFifo_getOutRecNumber(void);

bool stoFwdFifo_push(const stoFwdFifo_record_t *record, void (*stoFwdFifo_pushCompleteCallback)(bool success));
bool stoFwdFifo_peek(stoFwdFifo_record_t *record);
bool stoFwdFifo_pop(stoFwdFifo_record_t *record, void (*stoFwdFifo_popCompleteCallback)(bool success));
bool stoFwdFifo_flush(void (*stoFwdFifo_flushCompleteCallback)(bool success));

#endif /* STOFWDFIFO_H_ */
