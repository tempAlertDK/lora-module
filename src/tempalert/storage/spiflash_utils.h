#ifndef SRC_TEMPALERT_PERIPHERALS_FLASH_H_
#define SRC_TEMPALERT_PERIPHERALS_FLASH_H_

#include <stdint.h>
#include <stdbool.h>

#define PAGE_SIZE			256
#define SECTOR_SIZE			0x1000
#define BLOCK_SIZE			0x8000
#define MAX_ADDRESS			0x7FFFFF

#define IMAGE_HEADER_START_ADDR 	0
#define IMAGE_BLOCK_START_ADDR 		SECTOR_SIZE
#define IMAGE_BLOCK_END_ADDR 		0x81000 // 524kB + image header sector
#define APP_DATA_START_ADDR         IMAGE_BLOCK_END_ADDR

bool spiflash_init(void);
bool spiflash_getInit(void);
uint32_t spiflash_getSize(void);
uint32_t spiflash_getSectorSize(void);
uint8_t spiflash_readStatus(void);
void spiflash_writeStatus(uint8_t stat);
uint16_t spiflash_readID(void);
bool spiflash_test(void);

bool spiflash_disableBlockProtect(uint8_t blocks);
bool spiflash_enableBlockProtect(uint8_t blocks);

bool spiflash_standby(void);
bool spiflash_deepSleep(void);

bool spiflash_programByte(uint32_t addr, uint8_t data);
bool spiflash_program(uint32_t addr, uint8_t *data, uint16_t length);
bool spiflash_read(uint32_t addr, uint8_t *data, uint16_t length);
bool spiflash_eraseSector(uint32_t addr);
bool spiflash_eraseBlock(uint32_t addr);
bool spiflash_test(void);

#endif
