/*
 * genericQueue.h
 *
 *  Created on: Feb 2, 2017
 *      Author: kwgilpin
 */

#ifndef SRC_TEMPALERT_GENERICQUEUE_H_
#define SRC_TEMPALERT_GENERICQUEUE_H_

#include <stdint.h>
#include <stdbool.h>

#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"

typedef enum {
	GENERIC_QUEUE_MODEM_RESPONSE_TYPE,
	GENERIC_QUEUE_MODEM_ON_COMMAND_TYPE,
	GENERIC_QUEUE_MODEM_OFF_COMMAND_TYPE,
	GENERIC_QUEUE_MODEM_PACKET_READY_TYPE,
	GENERIC_QUEUE_GENERAL_PURPOSE_TIMER_EXPIRED_TYPE,
	GENERIC_QUEUE_AT_COMMAND_TIMER_EXPIRED_TYPE
} genericQueueElementType_t;

typedef struct {
	bool free;
	size_t dataSize;
	void *p_data;
} genericQueueBufferHeader_t;

typedef struct {
	genericQueueElementType_t type;
	SemaphoreHandle_t mutex;
	genericQueueBufferHeader_t *p_record;
} genericQueueElement_t;

typedef struct {
	SemaphoreHandle_t mutex;
	StaticSemaphore_t mutex_buffer;
	genericQueueBufferHeader_t *p_poolHeaders;
	size_t poolBufferCount;
	size_t poolBufferSize;
} genericQueuePool_t;

#define GENERIC_QUEUE_BUFFER_POOL_CREATE(poolName, size, dataType) \
	genericQueueBufferHeader_t poolName##_headers_[size]; \
	dataType poolName##_data_[size]; \
	genericQueuePool_t poolName = {NULL, poolName##_headers_, size, sizeof(dataType)};

#define GENERIC_QUEUE_BUFFER_POOL_INITIALIZE(poolName) \
	do { \
		if ((poolName.mutex = xSemaphoreCreateMutexStatic(&poolName.mutex_buffer)) == NULL) { \
			APP_ERROR_HANDLER(NRF_ERROR_NO_MEM); \
		} \
		\
		for (int i = 0; i < poolName.poolBufferCount; i++) { \
				poolName.p_poolHeaders[i].free = true; \
				poolName.p_poolHeaders[i].dataSize = 0; \
				poolName.p_poolHeaders[i].p_data = &poolName##_data_[i]; \
		}; \
		xSemaphoreGive(poolName.mutex); \
	} while(0);

bool genericQueueSend(QueueHandle_t genQ, genericQueueElementType_t type, const genericQueuePool_t *pool, const void *data, TickType_t ticksToWait);
bool genericQueueReceive(QueueHandle_t genQ,  genericQueueElementType_t *type, void *data, size_t *dataSize, TickType_t ticksToWait);

#endif /* SRC_TEMPALERT_GENERICQUEUE_H_ */
