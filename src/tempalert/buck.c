/*
 * buck.c
 *
 *  Created on: Jun 29, 2017
 *      Author: kwgilpin
 */


#include <stdint.h>
#include <stdbool.h>

#include "boards.h"
#include "mcp23008.h"
#include "buck.h"
#include "twi_mtx.h"

bool buck_setBuckmode(buckmode_t mode) {
	bool success;

	WITH_TWI_MTX() {
		if (mode == BUCKMODE_BURST) {
			success = mcp23008_clearPins(MCP23008_CELL_I2C_ADDR, BUCKMODE_EXPANDER_PIN);
		} else if (mode == BUCKMODE_PULSE_SKIPPING) {
			success = mcp23008_tristatePins(MCP23008_CELL_I2C_ADDR, BUCKMODE_EXPANDER_PIN);
		} else if (mode == BUCKMODE_PULSE_SKIPPING_SPREAD_SPECTRUM) {
			success = mcp23008_setPins(MCP23008_CELL_I2C_ADDR, BUCKMODE_EXPANDER_PIN);
		} else {
			success = false;
		}
	}

	return success;
}


