/*
 * packet.c
 *
 *  Created on: Feb 9, 2017
 *      Author: Nate-Tempalert
 */
#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "SEGGER_RTT.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "twi_mtx.h"
#include "semphr.h"
#include "portmacro_cmsis.h"
#include "modem.h"
#include "bsp.h"
#include "boards.h"
#include "nrf_gpio.h"
#include "sensors.h"
#include "packet.h"
#include "defines.h"
#include "deviceID.h"
#include "crc.h"
#include "stoFwd.h"
#include "power.h"
#include "unixTime.h"
#include "gitversion.h"

#define ID_SIZE
#define CCID_SIZE

#define DEBUG_PACKET 1

uint8_t packet_countMsgs(uint8_t *p_packet, const uint16_t *p_packetLen);
void packet_updateCRC(uint8_t *p_packet, uint16_t packetLen);
uint8_t packet_computeCRC(uint8_t *bytes, uint16_t byteCount);


bool g_firstPacket = false;

static uint8_t m_seqNum = 0;

bool packet_init(packet_t * packet) {

	bool success = true;
	char id[11];
	uint16_t ptr = 0;
	uint16_t messages = 0;

	/* Initialize basic packet status and initial variables */
	memset(packet, 0, sizeof(packet_t));
	packet->created = xTaskGetTickCount();
	packet->full = false;
	packet->sf_records = 0;
	packet->crc = false;

	/* Insert device ID into the packet */
	success &= deviceID_getDeviceIDencString(id,sizeof(id));
	memcpy(&(packet->data[1]),id,sizeof(id)-1);
	ptr += 11;

	/* The sequence number must be the first message in the packet. If it is
	 * not, the server will not echo the sequence number back to us.*/
	packet->data[ptr++] = SEQUENCE_NUMBER_MESSAGE_ID;
	packet->data[ptr++] = m_seqNum++;
	messages++;

    if (g_firstPacket) {
		/* The first packet that we send to the server after a reboot contains
		 * some additional information about the protocol, hardware, and
		 * firmware versions.  */
    	packet->data[ptr++] = INITIALIZATION_MESSAGE_ID;
    	packet->data[ptr++] = 0;
        messages++;

        packet->data[ptr++] = PROTOCOL_VERSION_MESSAGE_ID;
        packet->data[ptr++] = PROTOCOL_MAJOR;
        packet->data[ptr++] = PROTOCOL_MINOR;
        messages++;

        packet->data[ptr++] = HARDWARE_FIRMWARE_VERSION_MESSAGE_ID;
        packet->data[ptr++] = HARDWARE_FAMILY;
        packet->data[ptr++] = HARDWARE_VERSION;
        packet->data[ptr++] = MAJOR_VERSION;
        packet->data[ptr++] = MINOR_VERSION;
        messages++;

        /* Set flag to indicate that this is the first packet */
        packet->firstPacket = true;
    }

    if (!unixTime_isValid() || g_updateUnixTime) {
    		if (!unixTime_isValid()) {
    			SEGGER_RTT_printf(0, "Unix time invalid, sending time request. !\r\n");
    		} else {
    			SEGGER_RTT_printf(0, "Unix time update interval expired, sending time request\r\n");
    		}
		packet->data[ptr++] = UNIX_TIME_REQUEST_MESSAGE_ID;
		messages++;
    }

	/* Update packet length message count */
	packet->dataLength = ptr;
	packet->data[0] = messages;

	return success;
}


bool packet_appendSfRecord (packet_t * packet, sf_record_t * sfrec) {
	bool success = true;
	uint16_t ptr = packet->dataLength;
	uint16_t messages = packet->data[0];
	uint16_t sfrec_len = 0;
	/* Age the record if necessary */
	uint32_t age_sec = (get_time_100ms() - sfrec->header.timeStamp) / 10;

	/* Verify that the record can fir into the current packet */
	sfrec_len = sfrec->header.dataLength;
	if (age_sec > 0) {
		sfrec_len += 4;
	}
	if (ptr + sfrec_len <= PACKET_MAX_LENGTH-CRC_LENGTH) {

		if (age_sec > 0) {
			/* Otherwise, if the sensor values have been sitting in the store and
			 * forward buffer, the first message in the packet must be an age
			 * message specifying how old the data is.  If the data is old, we do
			 * not send battery information because the server doesn't care what
			 * the state of our battery was at any point in the past. */
			packet->data[ptr++] = AGE_MESSAGE_ID;
			packet->data[ptr++] = (age_sec >> 16) & 0xFF;
			packet->data[ptr++] = (age_sec >> 8) & 0xFF;
			packet->data[ptr++] = age_sec & 0xFF;
			messages++;
		}

		/* Save the record location in the packet, that way if we need to find
		 * it we know where to look for it later. */
		sfrec->header.p_pkt = ptr;

		/* Copy record data into packet */
		memcpy(&(packet->data[ptr]), sfrec->data, sfrec->header.dataLength);
		ptr += sfrec->header.dataLength;
		messages += sfrec->header.messages;

		/* update packet variables */
		packet->sf_records++;
		packet->data[0] = messages;
		packet->dataLength = ptr;

#if defined(DEBUG_PACKET) && (defined(DEBUG_PACKET) == 1)
		SEGGER_RTT_printf(0, "s&f message added packet, age %u, !\r\n", age_sec);
#endif

	} else {
		success = false;
		SEGGER_RTT_printf(0, "ERROR: Not enough room in packet for S&F record data!\r\n");
	}

	return success;
}



void packet_print(uint8_t * packet, uint16_t packetLen) {
	uint8_t i = 0;

	/* Short delay to allow buffering... */
	vTaskDelay(pdMS_TO_TICKS(200));
	/* Then suspend tasks to commit to packet display... */
	vTaskSuspendAll ();

	SEGGER_RTT_printf(0,"PACKET: [");
	for (i = 0; i < packetLen; i++) {
		SEGGER_RTT_printf(0,"%02x",packet[i]);
	}
	SEGGER_RTT_printf(0,"]\r\n");

	/* Resume kernel ... */
	xTaskResumeAll ();
}

uint8_t packet_countMsgs(uint8_t *p_packet, const uint16_t *p_packetLen) {
	return p_packet[0];
}

void packet_updateCRC(uint8_t *p_packet, uint16_t packetLen) {
	p_packet[packetLen-1] = packet_computeCRC(p_packet, packetLen - 1);
}


bool packet_verifyCRC(uint8_t *p_packet, uint16_t packetLen) {
	if (packet_computeCRC(p_packet, packetLen - 1) == p_packet[packetLen - 1]) {
		return true;
	}

	return false;
}

uint8_t packet_computeCRC(uint8_t *bytes, uint16_t byteCount) {
	uint16_t i;
	uint8_t runningCRC;

	runningCRC = 0;

    for (i = 0; i < byteCount; i++) {
        runningCRC = updateCRC(runningCRC, bytes[i]);
    }

	return runningCRC;
}
