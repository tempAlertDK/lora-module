/*
 * log.c
 *
 *  Created on: Sep 8, 2017
 *      Author: nreimens
 */
#include "log.h"
#include "stdint.h"
#include "stdbool.h"

#ifdef DEBUG
uint8_t log_level = LOG_LEVEL_ALL;
#else
uint8_t log_level = LOG_LEVEL_BASIC;
#endif

char * log_levelStrs[] = {
	"LOG_LEVEL_NONE",
	"LOG_LEVEL_ERROR",
	"LOG_LEVEL_BASIC",
	"LOG_LEVEL_DETAILED",
	"LOG_LEVEL_ALL"
};

bool log_setLevel (uint8_t level) {

	if (level > LOG_LEVEL_ALL) {
		level = LOG_LEVEL_ALL;
	}

	log_level = level;

	return true;
}

uint8_t log_getLevel (void) {
	return log_level;
}
