#include "version.h"
#include "log.h"
#include "gitversion.h"


#if defined (QA) && (QA == 1)
#define TARGET_SERVER 		"QA"
#elif defined (DEV) && (DEV == 1)
#define TARGET_SERVER		"DEV"
#elif (PR) //Production Devices
#define TARGET_SERVER		"PR"
#else
	#error Server enviroment not defined!
#endif



// see .h
void log_version(void) {
    log_infof("CellNode server %s, version %u.%u, Build %s", TARGET_SERVER, MAJOR_VERSION, MINOR_VERSION, gitVersionStr);
}
