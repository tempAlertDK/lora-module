/*
 * atecc508a.h
 *
 *  Created on: Jan 6, 2017
 *      Author: Nate-Tempalert
 */

#ifndef SRC_TEMPALERT_PERIPHERALS_ATECC508A_H_
#define SRC_TEMPALERT_PERIPHERALS_ATECC508A_H_


#include "stdint.h"
#include "stdbool.h"

#define ATECC508A_TWI_ADDR	0x60
#define SERIAL_LOW 			0x00
#define SERIAL_HIGH			0x00


bool atecc508a_init(void);

#endif /* SRC_TEMPALERT_PERIPHERALS_ATECC508A_H_ */
