/*
 * atecc508a.c
 *
 *  Created on: Jan 6, 2017
 *      Author: Nate-Tempalert
 */
#include <stdint.h>
#include <stdbool.h>

#include "app_twi.h"
#include "app_util_platform.h"

#include "SEGGER_RTT.h"

#include "boards.h"
#include "nrf_delay.h"
#include "twi_mtx.h"
#include "twi_register.h"
#include "semphr.h"
#include "portmacro_cmsis.h"
#include "atecc508a.h"

#define MAX_PENDING_TWI_TRANSACTIONS	6
bool atecc508a_sleep(void);

app_twi_t m_app_twi_atecc = APP_TWI_INSTANCE(0);

bool atecc508a_init(void) {
	bool success = true;

	uint8_t twiRead[4]; // Invert INT pin polarity

	WITH_TWI_MTX() {

		if (success) {
			success = twiRegister_readData(&config, ATECC508A_TWI_ADDR, SERIAL_LOW, twiRead, sizeof(twiRead));
		}

		if (!success) {
			SEGGER_RTT_printf(0, "I2C communication failed to communicate with LIS2DH12!\r\n");
		} else {
			SEGGER_RTT_printf(0, "atecc508a low addr value: %u", (uint32_t) * twiRead);
		}

		atecc508a_sleep();

	}

	return success;
}

bool atecc508a_sleep(void) {
	bool success = true;

	/* Write a single 1 to tell the device to go to sleep */
	uint8_t data = 0x01;
	if (success) {
		success = twiRegister_writeData(&config, ATECC508A_TWI_ADDR, &data,1);
	}

	if (!success) {
		SEGGER_RTT_printf(0, "I2C communication failed to communicate with LIS2DH12!\r\n");
	}

	return success;
}
