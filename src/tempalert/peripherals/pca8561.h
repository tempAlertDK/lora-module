/*
 * pca8561.h
 *
 *  Created on: Jan 2, 2017
 *      Author: Nate-Tempalert
 *
 *      Driver for NXP I2C LCD Display Driver
 *
 */

#ifndef SRC_TEMPALERT_PERIPHERALS_PCA8561_H_
#define SRC_TEMPALERT_PERIPHERALS_PCA8561_H_

#include <stdint.h>
#include <stdbool.h>

bool pca8561_swreset(void);
bool pca8561_configure(void);
bool pca8561_enableDisplay(void);
bool pca8561_disableDisplay(void);
bool pca8561_updateDisplay(char * str);

#endif /* SRC_TEMPALERT_PERIPHERALS_MCP23008_H_ */
