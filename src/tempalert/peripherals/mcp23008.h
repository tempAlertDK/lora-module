/*
 * mcp23008.h
 *
 *  Created on: Dec 30, 2016
 *      Author: Nate-Tempalert
 *
 *      Driver for Microchip I2C GPIO
 *
 */

#ifndef SRC_TEMPALERT_PERIPHERALS_MCP23008_H_
#define SRC_TEMPALERT_PERIPHERALS_MCP23008_H_

#include <stdint.h>
#include <stdbool.h>

bool mcp23008_openDrainInterruptPin(uint8_t addr);
bool mcp23008_setPins(uint8_t addr, uint8_t pins);
bool mcp23008_clearPins(uint8_t addr, uint8_t pins);
bool mcp23008_tristatePins(uint8_t addr, uint8_t pins);
bool mcp23008_queryTristatePins(uint8_t addr, uint8_t *pins);
bool mcp23008_readPins(uint8_t addr, uint8_t *pins);

bool mcp23008_enablePinChangeInterrupts(uint8_t addr, uint8_t pins);
bool mcp23008_enableLevelInterrupts(uint8_t addr, uint8_t pins, uint8_t defaultLevels);
bool mcp23008_disableInterrupts(uint8_t addr, uint8_t pins);
bool mcp23008_readInterruptFlags(uint8_t addr, uint8_t *flags);
bool mcp23008_readInterruptCaptureLevels(uint8_t addr, uint8_t *levels);

#endif /* SRC_TEMPALERT_PERIPHERALS_MCP23008_H_ */
