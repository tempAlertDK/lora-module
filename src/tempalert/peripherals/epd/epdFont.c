/*
 * epdFont.c
 *
 * Created: 9/10/2014 10:32:32 PM
 *  Author: kwgilpin
 */ 

#include <stdint.h>
#include <stdbool.h>

#include "bfclatin_font.h"
#include "Verdana16h.h"
#include "epdFont.h"

bool epdFont_getCharLine(char c, uint8_t lineIndex, uint16_t *data, uint8_t *width) {
	uint8_t index;
	uint16_t width16;
	uint32_t offset;
	uint8_t bytesPerLine;
	
	index = fontVerdana16h.index_table[(unsigned char)c];
	width16 = fontVerdana16h.width_table[index];
	offset = fontVerdana16h.offset_table[index];
	
	if (width16 <= 8) {
		bytesPerLine = 1;
	} else {
		bytesPerLine = 2;
	}
	
	offset += (lineIndex * bytesPerLine);
	
	*data = 0;
	*data |= (((uint16_t)fontVerdana16h.data_table[offset + 0]) << 8) & 0xFF00;
	
	if (bytesPerLine == 2) {
		*data |= (((uint16_t)fontVerdana16h.data_table[offset + 1]) << 0) & 0x00FF;
	}		
	
	*width = width16;
	
	return true;
}
