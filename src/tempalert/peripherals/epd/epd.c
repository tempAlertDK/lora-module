/*
 * epd.c
 *
 * Created: 9/3/2014 10:53:02 PM
 *  Author: kwgilpin
 */ 

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "nrf_delay.h"
#include "nrf_drv_spi.h"
#include "app_util_platform.h"

#include "spi.h"
#include "spi_mtx.h"

#include "boards.h"
#include "epd.h"

/* Number of scan bytes in a single line of interlaced data.  Each byte holds scan
 * information for four lines. */
#define EPD_SCAN_BYTE_COUNT					(EPD_LINE_COUNT / 4)

/* Size of the buffer used to send a single interlaced line to the display.  
 * When transmitting pixel data, each byte holds four pixel's worth of data.
 * Additionally, we have to send the scan bytes, and we have to send a border
 * byte (in case of the 2.0in LCD) or a dummy 0x00 byte (in case of the 2.7in
 * LCD). */
#define EPD_INTERLACED_LINE_DATA_LENGTH		((EPD_COLUMN_COUNT / 4) + EPD_SCAN_BYTE_COUNT + 1)

nrf_drv_spi_config_t epd_spi_config = {
    .sck_pin = SCLK_PIN,
    .mosi_pin = MOSI_PIN,
    .miso_pin = MISO_PIN,
    .ss_pin = NRF_DRV_SPI_PIN_NOT_USED,
    .irq_priority = APP_IRQ_PRIORITY_LOW,
    .orc = 0x00,
    .frequency = NRF_DRV_SPI_FREQ_8M,
    .mode = NRF_DRV_SPI_MODE_0,
	.bit_order = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST
};

static bool m_initialized = false;
static bool m_flipHorizontal = false;
static bool m_flipVertical = false;

static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(1);
static volatile bool m_spi_xfer_done = false;

void epd_spi_event_handler(nrf_drv_spi_evt_t const * p_event);

static bool epd_spi(uint8_t registerIndex, const uint8_t *data, uint16_t dataLength);
static bool epd_spi_r(uint8_t registerIndex, uint8_t *data, uint16_t dataLength);
static bool epd_spi_rid(uint8_t *driverID);

static bool epd_power_on(void);
static bool epd_power_off(void);

static bool epd_initialize_driver(void);

static bool epd_write_line(uint8_t lineNumber, uint8_t borderByte, const uint8_t *lineData, bool negativeImage, bool nothingLine);
static bool epd_write_image_block(bool (*lineMemoryRead_fcn)(uint8_t, uint8_t *), bool negativeImage, uint8_t frameCount, uint8_t blockWidth, uint8_t blockStep);
static bool epd_write_image_frame(bool (*lineMemoryRead_fcn)(uint8_t, uint8_t *), bool negativeImage);
static bool epd_write_blank_image_frame(void);

static bool lineMemoryReadAllWhite(uint8_t lineNumber, uint8_t *lineData);
static bool lineMemoryReadAllBlack(uint8_t lineNumber, uint8_t *lineData);


/* Public functions */

void epd_init(bool flipHorizontal, bool flipVertical) {
	epd_init_power();
	epd_init_reset();
	epd_init_discharge();
	epd_init_busy();
	epd_init_border();

	m_flipHorizontal = flipHorizontal;
	m_flipVertical = flipVertical;
	
	m_initialized = true;
}

bool epd_update(bool (*lineMemoryRead_fcn)(uint8_t lineNumber, uint8_t *lineData), int8_t tempC) {
	uint8_t frameCount;
	uint8_t blockHeight;
	uint8_t blockStep;
	uint16_t blockTime_usec;
	
	uint16_t blackTime_ms;
	uint16_t whiteTime_ms;
	uint8_t blackWhiteRepeatCount;
	
	if (!m_initialized) {
		return false;
	}
	
	epd_power_on();
	epd_initialize_driver();
	
	/* Stage 1 -- Write the negative of the new image to the display using the
	 * block-based method as described in the datasheet.  The block parameters
	 * depend on ambient temperature. */
#if (EPD_SIZE_2P0 == 1)
	if (tempC > 40) {
		frameCount = 4;
		blockTime_usec = 22000;
		blockStep = 2;
	} else if (tempC > 10) {
		frameCount = 2;
		blockTime_usec = 22000;
		blockStep = 6;		
	} else {
		frameCount = 2;
		blockTime_usec = 22000;
		blockStep = 6;		
	}
#elif (EPD_SIZE_2P7 == 1)
	if (tempC > 40) {
		frameCount = 4;
		blockTime_usec = 7400;
		blockStep = 2;		
	} else if (tempC > 10) {
		frameCount = 2;
		blockTime_usec = 7400;
		blockStep = 6;		
	} else {
		frameCount = 2;
		blockTime_usec = 7400;
		blockStep = 6;		
	}
#endif

#ifdef TTDEBUG
	blockHeight = 20;
	blockStep = 2;
#else 
	blockHeight = blockTime_usec / EPD_LINE_TIME_USEC;	
#endif
	
	epd_write_image_block(lineMemoryRead_fcn, true, frameCount, blockHeight, blockStep);
	
	/* Stage 2 -- Write alternating black and white pages to the display using
	 * the frame method as described in the datasheet. */
#if (EPD_SIZE_2P0 == 1)
	if (tempC > 40) {
		blackTime_ms = whiteTime_ms = 196;
		blackWhiteRepeatCount = 4;
	} else if (tempC > 10) {
		blackTime_ms = whiteTime_ms = 196;
		blackWhiteRepeatCount = 4;		
	} else {
		blackTime_ms = whiteTime_ms = 392;
		blackWhiteRepeatCount = 4;		
	}
#elif (EPD_SIZE_2P7 == 1)
	if (tempC > 40) {
		blackTime_ms = whiteTime_ms = 196;
		blackWhiteRepeatCount = 4;		
	} else if (tempC > 10) {
		blackTime_ms = whiteTime_ms = 196;
		blackWhiteRepeatCount = 4;		
	} else {
		blackTime_ms = whiteTime_ms = 392;
		blackWhiteRepeatCount = 4;		
	}
#endif

	while (blackWhiteRepeatCount-- > 0) {
		epd_write_image_frame(lineMemoryReadAllBlack, false);
#ifdef TTDEBUG
		nrf_delay_ms(196);
#else
		nrf_delay_ms(blackTime_ms);
#endif

		epd_write_image_frame(lineMemoryReadAllWhite, false);
#ifdef TTDEBUG
		nrf_delay_ms(196);
#else
		nrf_delay_ms(whiteTime_ms);
#endif
	}
	
	/* Stage 3 -- Write the true new image to the display using the block-based
	 * method as described in the datasheet.  */
#if (EPD_SIZE_2P0 == 1)
	if (tempC > 40) {
		frameCount = 4;
		blockTime_usec = 22000;
		blockStep = 2;
	} else if (tempC > 10) {
		frameCount = 2;
		blockTime_usec = 22000;
		blockStep = 6;
	} else {
		frameCount = 2;
		blockTime_usec = 22000;
		blockStep = 6;
	}
#elif (EPD_SIZE_2P7 == 1)
	if (tempC > 40) {
		frameCount = 4;
		blockTime_usec = 7400;
		blockStep = 2;
	} else if (tempC > 10) {
		frameCount = 2;
		blockTime_usec = 7400;
		blockStep = 6;
	} else {
		frameCount = 2;
		blockTime_usec = 7400;
		blockStep = 6;
	}
#endif
	
	
#ifdef TTDEBUG
	blockHeight = 20;
	blockStep = 2;
#else
	blockHeight = blockTime_usec / EPD_LINE_TIME_USEC;
#endif	
	
	epd_write_image_block(lineMemoryRead_fcn, false, frameCount, blockHeight, blockStep);
	
	epd_power_off();
	
	return true;
}


/* Static functions */

void epd_spi_event_handler(nrf_drv_spi_evt_t const * p_event)
{
    m_spi_xfer_done = true;
}

bool epd_spi(uint8_t registerIndex, const uint8_t *data, uint16_t dataLength) {
	uint8_t txBytes[2];
	bool success = true;
	
	WITH_SPI_MTX() {
		spi_init(&epd_spi_config, epd_spi_event_handler);

		/* Ensure that there is a 10us delay before we drive the CS* line low */
		epd_deassert_cs();
		nrf_delay_us(10);
		epd_assert_cs();
	
		/* Register indices are always preceded by a 0x70 byte to identify the
		 * byte that follows as a register index. */
		//spi_send_recv(0x70);
		m_spi_xfer_done = false;
		txBytes[0] = 0x70;
		txBytes[1] = registerIndex;
		nrf_drv_spi_transfer(&spi, txBytes, 2, NULL, 0);
		while(!m_spi_xfer_done);

		/* Bring the CS* line high for at least 10us */
		epd_deassert_cs();
		nrf_delay_us(10);
		epd_assert_cs();
	
		/* Data to be written to a register is always preceded by a 0x72 byte to
		 * identify the data as data to be written */
		m_spi_xfer_done = false;
		txBytes[0] = 0x72;
		nrf_drv_spi_transfer(&spi, txBytes, 1, NULL, 0);
		while(!m_spi_xfer_done);
	
		/* Send all data bytes, 25ms timeout */
		m_spi_xfer_done = false;
		nrf_drv_spi_transfer(&spi, data, dataLength, NULL, 0);
		while(!m_spi_xfer_done);
	
		/* Bring the CS* line high */
		epd_deassert_cs();

		spi_deinit();
	}
	
	return success;
}

bool epd_spi_r(uint8_t registerIndex, uint8_t *data, uint16_t dataLength) {
	uint8_t txBytes[2];

	WITH_SPI_MTX() {
		spi_init(&epd_spi_config, epd_spi_event_handler);

		/* Ensure that there is a 10us delay before we drive the CS* line low */
		epd_deassert_cs();
		nrf_delay_us(10);
		epd_assert_cs();

		/* Register indices are always preceded by a 0x70 byte to identify the
		 * byte that follows as a register index. */
		m_spi_xfer_done = false;
		txBytes[0] = 0x70;
		txBytes[1] = registerIndex;
		nrf_drv_spi_transfer(&spi, txBytes, 2, NULL, 0);
		while(!m_spi_xfer_done);
	
		/* Bring the CS* line high for at least 10us */
		epd_deassert_cs();
		nrf_delay_us(10);
		epd_assert_cs();

		/* Data to be read from a register is always preceded by a 0x73 byte to
		 * identify the data as data to be read */
		m_spi_xfer_done = false;
		txBytes[0] = 0x73;
		nrf_drv_spi_transfer(&spi, txBytes, 1, data, dataLength);
		while(!m_spi_xfer_done);
	
		/* Bring the CS* line high */
		epd_deassert_cs();

		spi_deinit();
	}
	
	return true;
}

bool epd_spi_rid(uint8_t *driverID) {
	uint8_t rxData[2];

	WITH_SPI_MTX() {
		spi_init(&epd_spi_config, epd_spi_event_handler);
	
		/* Ensure that there is a 10us delay before we drive the CS* line low */
		epd_deassert_cs();
		nrf_delay_us(10);
		epd_assert_cs();

		/* The command read the COG ID is identified by the fact that the first byte
		 * is a 0x71.  The driver responds with its ID in the subsequent byte
		 * returned. */
		m_spi_xfer_done = false;
		uint8_t txByte = 0x71;
		nrf_drv_spi_transfer(&spi, &txByte, 1, rxData, 2);
		while(!m_spi_xfer_done);

		/* Bring the CS* line high */
		epd_deassert_cs();

		spi_deinit();
	}
	
	*driverID = rxData[1];

	return true;
}

bool epd_power_on() {
	if (!m_initialized) {
		return false;
	}
	
	/* G2 COG datasheet states that RESET*, CS*, BORDER, MOSI, and SCLK should 
	 * all be low before applying power. */
	epd_deassert_reset();
	epd_deassert_cs();
	epd_assert_border();
	
	/* Should check that SCLK and MOSI are low */
	
	/* Should check whether boost converter is needed here. The LCD needs a 
	 * supply voltage between 2.3 and 3.6V. */
	
	epd_assert_power();
	
	epd_deassert_cs();
#if (EPD_SIZE_2P7)
	epd_deassert_border();
#endif
	epd_deassert_reset();
	nrf_delay_ms(5+1);
	epd_assert_reset();
	nrf_delay_ms(5+1);
	epd_deassert_reset();
	nrf_delay_ms(5+1);
	
	return true;
}

bool epd_power_off() {
	uint8_t spiData[2];
	uint8_t lineData[EPD_COLUMN_COUNT];
	
	epd_write_blank_image_frame();

#if (EPD_SIZE_2P0 == 1)
	/* Write a dummy line (all 0's) with a black (0xFF) border byte */
	memset(lineData, 0x00, sizeof(lineData));
	if (!epd_write_line(0, 0xFF, lineData, false, true)) {
		return false;
	}
	
	/* Delay at least 40ms */
	nrf_delay_ms(40+1);
	
	/* Write a dummy line (all 0's) with a white (0xAA) border byte */ 
	if (!epd_write_line(0, 0xAA, lineData, false, true)) {
		return false;
	}
	
	/* Delay at least 200ms */
	nrf_delay_ms(200+1);
	
	/* Write a dummy line with the border byte set to 0x00 */
	if (!epd_write_line(0, 0x00, lineData, false, true)) {
		return false;
	}
	
	/* Delay at least 25ms */
	nrf_delay_ms(25+1);
#elif (EPD_SIZE_2P7 == 1) 
	/*  Write a dummy line (all 0's) with all scan bytes = 0 */
	memset(lineData, 0x00, sizeof(lineData));
	if (!epd_write_line(0, 0x00, lineData, false, true)) {
		return false;
	}

	/* Delay at least 25ms after writing image data to the display */
	nrf_delay_ms(25+1);
	
	/* Drive BORDER low */
	epd_assert_border();
	
	/* Delay between 100 and 200ms */
	nrf_delay_ms(201);
	
	/* Drive border high */
	epd_deassert_border();
#endif
	

	/* Unknown what this does */
	spiData[0] = 0x00;
	if (!epd_spi(0x0B, spiData, 1)) {
		return false;
	}

	/* Turn on latch reset */
	spiData[0] = 0x01;
	if (!epd_spi(0x03, spiData, 1)) {
		return false;
	}
	
	/* Power-off chargepump / Vcom off*/
	spiData[0] = 0x03;
	if (!epd_spi(0x05, spiData, 1)) {
		return false;
	}
	
	/* Power-off chargepump negative voltage / VGL & VDL off */
	spiData[0] = 0x01;
	if (!epd_spi(0x05, spiData, 1)) {
		return false;
	}

	/* Delay > 300ms */
	nrf_delay_ms(300 + 1);
	
	/* Discharge internal */
	spiData[0] = 0x80;
	if (!epd_spi(0x04, spiData, 1)) {
		return false;
	}
	
	/* Power off positive charge pump / VHG & VDH off*/
	spiData[0] = 0x00;
	if (!epd_spi(0x05, spiData, 1)) {
		return false;
	}

	/* Turn off OSC */
	spiData[0] = 0x01;
	if (!epd_spi(0x07, spiData, 1)) {
		return false;
	}
	
	/* Delay at least 50ms */
	nrf_delay_ms(50+1);
	
	/* Ground MOSI, SCLK, and /BORDER_CONTRL signals */
	epd_assert_border();

	/* Remove power */
	epd_deassert_power();

	/* Delay > 10ms */
	nrf_delay_ms(10+1);

	/* Ground /RESET and /CS signals */
	epd_assert_reset();
	epd_assert_cs();

	/* Assert discharge for 150ms */
	epd_assert_discharge();
	nrf_delay_ms(150+1);
	epd_deassert_discharge();

	return true;	
}

bool epd_initialize_driver() {
	bool success;
	uint8_t driverID;
	uint8_t spiData[9];
	uint8_t cpCounter;
	
	/* Wait for the BUSY signal to go low */
	while (epd_is_busy());
	
	/* Return failure if BUSY signal does not go low after 1 second */
	if (epd_is_busy()) {
		return false;
	}
	
	/* Read and verify the COG driver device ID */
	if (!(success = epd_spi_rid(&driverID))) {
		return false;
	}

	if (!((driverID == 0x11) || (driverID == 0x12))) {
		return false;
	}
	
	/* Disable OE */
	spiData[0] = 0x40;
	if (!epd_spi(0x02, spiData, 1)) {
		return false;
	}
	
	/* Check for breakage */
	success = epd_spi_r(0x0F, spiData, 2);
	if (!success || ((spiData[1] & 0x80) != 0x80)) {
		return false;
	}
	
	/* Enter power-saving mode */
	spiData[0] = 0x02;
	if (!epd_spi(0x0B, spiData, 1)) {
		return false;
	}
	
	/* Channel select--data depends on display size.  The datasheet does not
	 * explain exactly what this data does. */
#if (EPD_SIZE_2P0 == 1)
	spiData[0] = 0x00;
	spiData[1] = 0x00;
	spiData[2] = 0x00;
	spiData[3] = 0x00;
	spiData[4] = 0x01;
	spiData[5] = 0xFF;
	spiData[6] = 0xE0;
	spiData[7] = 0x00;
#elif (EPD_SIZE_2P7 == 1)
	spiData[0] = 0x00;
	spiData[1] = 0x00;
	spiData[2] = 0x00;
	spiData[3] = 0x7F;
	spiData[4] = 0xFF;
	spiData[5] = 0xFE;
	spiData[6] = 0x00;
	spiData[7] = 0x00;
#endif
	if (!epd_spi(0x01, spiData, 8)) {
		return false;
	}

	/* High power mode oscillator setting */
	spiData[0] = 0xD1;
	if(!epd_spi(0x07, spiData, 1)) {
		return false;
	}

	/* Power setting */
	spiData[0] = 0x02;
	if (!epd_spi(0x08, spiData, 1)) {
		return false;
	}
	
	/* Set VCOM level */
	spiData[0] = 0xC2;
	if (!epd_spi(0x09, spiData, 1)) {
		return false;
	}
	
	/* Power setting */
	spiData[0] = 0x03;
	if (!epd_spi(0x04, spiData, 1)) {
		return false;
	}
	
	/* Driver latch on */
	spiData[0] = 0x01;
	if (!epd_spi(0x03, spiData, 1)) {
		return false;
	}
	
	/* Driver latch off */
	spiData[0] = 0x00;
	if (!epd_spi(0x03, spiData, 1)) {
		return false;
	}
	
	/* Delay at least 5ms */
	nrf_delay_ms(5+1);
	
	/* cpCounter is used to track how many attempts we have made to start the
	 * DC/DC converter. */
	cpCounter = 1;
	do {
		/* Start charge pump positive voltage: VGH and VDH on */
		spiData[0] = 0x01;
		if (!epd_spi(0x05, spiData, 1)) {
			return false;
		}
		
		/* Delay at least 240ms */
		nrf_delay_ms(240+1);
		
		/* Start charge pump negative voltage: VGL and VDL on */
		spiData[0] = 0x03;
		if (!epd_spi(0x05, spiData, 1)) {
			return false;
		}
		
		/* Delay at least 40ms */
		nrf_delay_ms(40+1);
		
		/* Set charge pump VCOM_DRIVER to on */
		spiData[0] = 0x05;
		if (!epd_spi(0x05, spiData, 1)) {
			return false;
		}
		
		/* Delay at least 40ms */
		nrf_delay_ms(40+1);
		
		/* Check the DC/DC status */
		if (!epd_spi_r(0x0F, spiData, 2)) {
			return false;
		}
		
		/* Break out of the do/while loop if the DC/DC converter was started 
		 * successfully. */
		if (spiData[1] & 0x40) {
			break;
		}
	
		/* Increment the start attempts counter */
		cpCounter++;
	} while (cpCounter < 4);
	
	/* If the DC/DC converter is still not operating correctly after three
	 * attempts to start it, return failure. */
	if (cpCounter >= 4) {
		return false;
	}
	
	/* Disable OE */
	spiData[0] = 0x06;
	if (!epd_spi(0x02, spiData, 1)) {
		return false;
	}
	
	return true;
}

bool epd_write_line(uint8_t lineNumber, uint8_t borderByte, const uint8_t *lineData, bool negativeImage, bool nothingLine) {
	uint8_t interlacedData[EPD_INTERLACED_LINE_DATA_LENGTH];
	uint8_t interlacedDataOffset;
	int16_t col;
	int8_t j;
	uint8_t activeScanByte;
	uint8_t activeScanBitsOffset;
	uint8_t blackCode, whiteCode;
	

	/* Each byte in lineData represents the value of a single pixel.  We care 
	 * about the two least significant bits, which indicate whether the pixel
	 * should be set to black, white, or left unchanged (i.e. "nothing" in the
	 * datasheet). */

	/* To be consistent with the datasheet, lines are indexed from 1 to 96 (for
	 * 2.0" displays) or 1 to 176 (for 2.7" displays).  We allow the caller to 
	 * pass this lineNumber = 0 so that a dummy line (all 0's) can be written. 
	 */
	if  (lineNumber > EPD_LINE_COUNT) {
		return false;
	}
	
	/* For a normal image, 0x3 indicates a black pixel and 0x02 indicates a
	 * white pixel. */
	if (negativeImage) {
		blackCode = 0x02;
		whiteCode = 0x03;
	} else {
		blackCode = 0x03;
		whiteCode = 0x02;
	}	
	
	/* Fill the interlaced data line with 0x00.  This will pay off when writing
	 * the scan data to the line below. */
	memset(interlacedData, 0x00, sizeof(interlacedData));
	
	/* The interlacedDataOffset is used to keep track of our position in the
	 * interlaced data array. It represents the array index where the next byte
	 * should be written. */ 
	interlacedDataOffset = 0;
	
	/* In the case of a 2.0in display, the first byte of the line is the border
	 * byte.  In the case of a 2.7in display, this should always be 0x00. */
#if (EPD_SIZE_2P7)
	borderByte = 0x00;
#endif
	interlacedData[interlacedDataOffset++] = borderByte;
	
	/* We add the odd half (using base 1 indexing) of the data bytes to the 
	 * interlaced line data starting with the highest-numbered (right-most)
	 * pixel.  In the case of a 2.0in LCD, the first data byte will contain
	 * {D(199,y), D(197,y), D(195,y), D(193,y)}.  */
	for (col = EPD_COLUMN_COUNT-1; col >= 1; ) {	
		/* Each pixel state when sent to the LCD is represented by 2 bits, 
		 * but the lineData provided by the caller is just 1-bit (1 for
		 * black and 0 for white.  We must convert here: 
		 *  0x3 - black
		 *  0x2 - white
		 *  0x0 - nothing */
		
		if (nothingLine) {
			interlacedData[interlacedDataOffset++] = 0x00;
			col -= 8;
			continue;
		}
		
		for (j=3; j>=0; j--) {
			/* Perform horizontal flip here if necessary */		
			int16_t adjCol;
			if (m_flipHorizontal) {
				/* When flipping horizontally, D(199,y) should be replaced by 
				 * D(2,y); D(197,y) should be replaced by D(4,y); ...; and
				 * D(1,y) should be replaced by D(200,y). */
				adjCol = EPD_COLUMN_COUNT - col + 1;
			} else {
				adjCol = col;
			}
			
			if (lineData[adjCol-1] & 0x01) {
				interlacedData[interlacedDataOffset] |= blackCode << (j*2);
			} else {
				interlacedData[interlacedDataOffset] |= whiteCode << (j*2);
			}

			col -= 2;
		}
		
		interlacedDataOffset++;
	}
	
	/* After the odd half of the pixel data is added to the interlaced line
	 * data, we insert the scan information.  This simply informs the displays
	 * which line we are providing data for.  */
	
	/* Active scan bytes are indexed from 1 to 24 (or 44 for 2.7in displays), 
	 * to be consistent with the EPD datasheet. Each byte holds scan 
	 * information (2 bits) for four lines.  We simply need to set the two bits
	 * corresponding to the active line high.  All other scan bits remain off. 
	 * Since we filled the interlaced line data with 0x00 above, we just need 
	 * identify the byte containing the bits for the current line and then find
	 * the offset within that byte corresponding to the particular line. */
	if (lineNumber != 0) {
		/* Perform vertical flip here if necessary */
		uint8_t adjLineNumber;
		if (m_flipVertical) {
			/* When flipping vertically, we just change which scan bits are 
			 * set.  In particular, if the scan bits for line 1 were being set,
			 * we now set the scan bits for line 96 (in the case of 2in 
			 * display). */
			adjLineNumber = EPD_LINE_COUNT - lineNumber + 1;
		} else {
			adjLineNumber = lineNumber;
		}
		
		activeScanByte = EPD_SCAN_BYTE_COUNT - ((adjLineNumber - 1) / 4);
	
		activeScanBitsOffset = 2 * (adjLineNumber - (((EPD_SCAN_BYTE_COUNT - activeScanByte) * 4) + 1));
	
		interlacedData[interlacedDataOffset + (activeScanByte - 1)] |= (0x3 << activeScanBitsOffset);
	}
	
	interlacedDataOffset += EPD_SCAN_BYTE_COUNT;
	
	/* After the scan bytes, we add the even half of the pixel data, starting 
	 * with the lowest even-numbered pixel (using base 1 indexing).  The first
	 * data byte in this section will contain {D(2,y), D(4,y), D(6,y), D(8,y)}.
	 */
	for (col = 2; col <= EPD_COLUMN_COUNT; ) {		
		if (nothingLine) {
			interlacedData[interlacedDataOffset++] = 0x00;
			col += 8;		
			continue;
		}
		
		for (j=3; j>=0; j--) {
			/* Perform horizontal flip here if necessary */		
			int16_t adjCol;
			if (m_flipHorizontal) {
				/* When flipping horizontally, D(2,y) should be replaced by 
				 * D(199,y); D(4,y) should be replaced by D(197,y); ...; and
				 * D(200,y) should be replaced by D(1,y). */
				adjCol = EPD_COLUMN_COUNT - col + 1;
			} else {
				adjCol = col;
			}			
			
			if (lineData[adjCol-1] & 0x01) {
				interlacedData[interlacedDataOffset] |= blackCode << (j*2);
			} else {
				interlacedData[interlacedDataOffset] |= whiteCode << (j*2);
			}

			col += 2;
		}
		
		interlacedDataOffset++;		
	}
	
	/* Having constructed the complete interlaced line, attempt to send it to the
	 * LCD. */
	return epd_spi(0x0A, interlacedData, interlacedDataOffset);
}

bool epd_write_image_block(bool (*lineMemoryRead_fcn)(uint8_t, uint8_t *), 
		bool negativeImage, uint8_t frameCount, uint8_t blockHeight, uint8_t blockStep) {
	uint8_t lineNumber;
	uint8_t lineData[EPD_COLUMN_COUNT];
	int16_t blockStartLine;	
	uint8_t spiData;
	
	if ((blockStep == 0) || (blockHeight == 0)) {
		return false;
	}
	
	/* The frameCount specifies how many times the same image should be 
	 * repeatedly written to the display. */ 
	while (frameCount > 0) {
		/* We write the image in blocks, of constant height, with a constant
		 * number of rows stepped between each block. */
		for (blockStartLine = 1 - blockHeight + 1; blockStartLine <= EPD_LINE_COUNT; blockStartLine += blockStep) {
			/* The first line to scan is the first line of the block unless the
			 * block starts above the top of the display, in which case the
			 * first line to scan is line 1. */
			if (blockStartLine >= 1) {
				lineNumber = blockStartLine;
			} else {
				lineNumber = 1;
			}
			
			/* Scan lines until we reach the last line of the block or the last
			 * line of the display. */
			while ((lineNumber < blockStartLine + blockHeight) && (lineNumber <= EPD_LINE_COUNT)) {
				
				if (!lineMemoryRead_fcn(lineNumber, lineData)) {
					return false;
				}
				
				/* If this is the last time that the line will be scanned, (as
				 * indicated by the fact that the line number will be less
				 * than the starting line of the next block), the datasheet
				 * specifies that we must set all pixels in the line to 
				 * 'nothing' which is represented by both pixel bits being set
				 * to 0. */
				if ((frameCount == 1) && (lineNumber < blockStartLine + blockStep)) {
					if (!epd_write_line(lineNumber, 0x00, lineData, false, true)) {
						return false;
					}
				} else {
					if (!epd_write_line(lineNumber, 0x00, lineData, negativeImage, false)) {
						return false;
					}
				}	
					
				/* Turn on OE */
				spiData = 0x07;
				if (!epd_spi(0x02, &spiData, 1)) {
					return false;
				}
				
				lineNumber++;
			}
		}
		
		frameCount--;
	}
	
	
	return true;
}

bool epd_write_image_frame(bool (*lineMemoryRead_fcn)(uint8_t, uint8_t *), bool negativeImage) {
	uint8_t lineNumber;
	uint8_t lineData[EPD_COLUMN_COUNT];
	uint8_t spiData;
	
	/* This function simply scans through every line of the LCD once */
	
	for (lineNumber = 1; lineNumber <= EPD_LINE_COUNT; lineNumber++) {
		/* Read the line's pixel data */
		if (!lineMemoryRead_fcn(lineNumber, lineData)) {
			return false;
		}
		
		/* Write the line to the LCD's memory */
		if (!epd_write_line(lineNumber, 0x00, lineData, negativeImage, false)) {
			return false;
		}
		
		/* Turn on OE */
		spiData = 0x07;
		if (!epd_spi(0x02, &spiData, 1)) {
			return false;
		}
	}
	
	return true;
}

bool epd_write_blank_image_frame() {
	uint8_t lineNumber;
	uint8_t lineData[EPD_COLUMN_COUNT];
	uint8_t spiData;

	/* This function simply scans through every line of the LCD once */

	memset(lineData, 0x00, EPD_COLUMN_COUNT);

	for (lineNumber = 1; lineNumber <= EPD_LINE_COUNT; lineNumber++) {
		/* Write the line to the LCD's memory */
		if (!epd_write_line(lineNumber, 0x00, lineData, false, true)) {
			return false;
		}

		/* Turn on OE */
		spiData = 0x07;
		if (!epd_spi(0x02, &spiData, 1)) {
			return false;
		}
	}

	return true;
}

bool lineMemoryReadAllWhite(uint8_t lineNumber, uint8_t *lineData) {
	memset(lineData, 0x00, EPD_COLUMN_COUNT);
	return true;
}

bool lineMemoryReadAllBlack(uint8_t lineNumber, uint8_t *lineData) {
	memset(lineData, 0x01, EPD_COLUMN_COUNT);
	return true;
}
