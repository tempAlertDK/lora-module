/*
 * epdScreen.c
 *
 * Created: 9/5/2014 3:16:19 PM
 *  Author: kwgilpin
 */ 

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "epd.h"
#include "epdFont.h"
#include "bfclatin_font.h"
#include "Verdana16h.h"
#include "epdScreen.h"

#define TOP_MARGIN_WIDTH_PIXELS		0
#define BOTTOM_MARGIN_WIDTH_PIXELS	0
#define LEFT_MARGIN_WIDTH_PIXELS	0
#define RIGHT_MARGIN_WIDTH_PIXELS	0
#define LINE_SPACING_PIXELS			0

#define TEXT_LINE_COUNT_LINES		6
#define TEXT_LINE_WIDTH_MAX_CHARS	32

static char m_textLines[TEXT_LINE_COUNT_LINES][TEXT_LINE_WIDTH_MAX_CHARS];

static const BFCLATIN_FONT *m_font = &fontVerdana16h;

static bool epdScreen_lineMemoryReadFcn(uint8_t lineNumber, uint8_t *lineData);

/* Public functions */

bool epdScreen_update(const char *text) {
	strcpy(m_textLines[0], "Line 1");
	strcpy(m_textLines[1], "Line 2");
	strcpy(m_textLines[2], "Line 3");
	strcpy(m_textLines[3], "Line 4");
	strcpy(m_textLines[4], "Line 5");
	strcpy(m_textLines[5], "Line 6");	
		
	return epd_update(epdScreen_lineMemoryReadFcn, 25);
}

/* Static functions */

bool epdScreen_lineMemoryReadFcn(uint8_t lcdLineNumber, uint8_t *lcdLineData) {
	uint8_t lcdLineIndex;
	uint8_t textLineIndex;
	uint8_t charIndex;
	unsigned char c;
	uint8_t charLineIndex;
	uint16_t charLineData;
	uint8_t charLineDataWidth;
	uint16_t colIndex;
	
	/* The lcdLineNumber provided by the caller should fall into the range
	 * [1, EPD_LINE_COUNT].  That is, it is referenced from 1.  If it falls
	 * outside of this range, return failure. */
	if ((lcdLineNumber < 1) || (lcdLineNumber > EPD_LINE_COUNT)) {
		return false;
	}
	
	/* For convenience, define lcdLineIndex, a 0-indexed version of the LCD
	 * line number. */
	lcdLineIndex = lcdLineNumber - 1;
	
	/* If the LCD line being requested is part of the top margin, return a 
	 * blank line to the caller. */
	if (lcdLineIndex < TOP_MARGIN_WIDTH_PIXELS) {
		memset(lcdLineData, 0x00, EPD_COLUMN_COUNT);
		return true;
	}
	
	/* If the LCD line being requested is part of the bottom margin, return a
	 * blank line to the caller. */
	if (lcdLineIndex >= (EPD_LINE_COUNT - BOTTOM_MARGIN_WIDTH_PIXELS)) {
		memset(lcdLineData, 0x00, EPD_COLUMN_COUNT);
		return true;		
	}
	
	/* Determine which line within the character bitmaps of the current font
	 * we are being asked to access. */
	charLineIndex = (lcdLineIndex - TOP_MARGIN_WIDTH_PIXELS) % (m_font->Height + LINE_SPACING_PIXELS);
	
	/* If the line index into the character row bitmap array exceeds the 
	 * character height, it indicates that the caller has requested a blank
	 * line that separates the lines of text.  As such, we return a blank line.
	 */
	if (charLineIndex >= m_font->Height) {
		memset(lcdLineData, 0x00, EPD_COLUMN_COUNT);
		return true;		
	}

	/* Determine which of the text lines on the display we are being asked to
	 * access. */
	textLineIndex = (lcdLineIndex - TOP_MARGIN_WIDTH_PIXELS) / (m_font->Height + LINE_SPACING_PIXELS);
	
	/* Fill the left margin with 0x00 bytes to overwrite any garbage which may
	 * be present. */
	memset(lcdLineData, 0x00, LEFT_MARGIN_WIDTH_PIXELS);
	colIndex = LEFT_MARGIN_WIDTH_PIXELS;
	
	/* Iterate over all characters in the line */
	charIndex = 0;
	while ((charIndex < TEXT_LINE_WIDTH_MAX_CHARS) && (colIndex < EPD_COLUMN_COUNT - RIGHT_MARGIN_WIDTH_PIXELS)) {
		c = m_textLines[textLineIndex][charIndex++];
		
		/* If we find a null terminator in the current text line before 
		 * reaching the maximum number of characters or the last column of the
		 * LCD, there is nothing else to print to the line.  We break out of 
		 * the enclosing while loop. */
		if (c == '\0') {
			break;
		}
		
		if (!epdFont_getCharLine(c, charLineIndex, &charLineData, &charLineDataWidth)) {
			return false;
		}
		
		while ((charLineDataWidth-- > 0) && (colIndex < EPD_COLUMN_COUNT - RIGHT_MARGIN_WIDTH_PIXELS)) {
			if (charLineData & 0x8000) {
				lcdLineData[colIndex++] = 0x01;
			} else {
				lcdLineData[colIndex++] = 0x00;
			}
			
			charLineData <<= 1;
		}
	}
	
	/* Fill whatever remains of the line with 0x00 bytes to clear any garbage
	 * that may be there. */
	memset(&lcdLineData[colIndex], 0x00, EPD_COLUMN_COUNT - colIndex);
	
	return true;
}	
