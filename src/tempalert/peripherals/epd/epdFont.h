/*
 * epdFont.h
 *
 * Created: 9/10/2014 11:00:42 PM
 *  Author: kwgilpin
 */ 


#ifndef EPDFONT_H_
#define EPDFONT_H_

bool epdFont_getCharLine(char c, uint8_t lineIndex, uint16_t *data, uint8_t *width);

#endif /* EPDFONT_H_ */