/*
 * Verdana16h.h
 *
 * Created: 9/10/2014 10:17:47 PM
 *  Author: kwgilpin
 */ 


#ifndef VERDANA16H_H_
#define VERDANA16H_H_

#include "bfclatin_font.h"

extern const BFCLATIN_FONT fontVerdana16h;


#endif /* VERDANA16H_H_ */