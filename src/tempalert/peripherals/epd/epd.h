/*
 * epd.h
 *
 * Created: 9/3/2014 10:53:16 PM
 *  Author: kwgilpin
 */ 


#ifndef EPD_H_
#define EPD_H_

#include "boards.h"
#include "mcp23008.h"
#include "spi.h"

#define EPD_SIZE_2P0	0
#define EPD_SIZE_2P7	1

/* This is hardware dependent */
#define EPD_LINE_TIME_USEC	1000

#if (EPD_SIZE_2P0 == 1)
#define EPD_COLUMN_COUNT	(200)
#define EPD_LINE_COUNT		(96)
#elif (EPD_SIZE_2P7 == 1)
#define EPD_COLUMN_COUNT	(264)
#define EPD_LINE_COUNT		(176)
#endif


static inline void epd_init_power(void) {
	mcp23008_setPins(MCP23008_EPD_I2C_ADDR, EPD_ONN_EXPANDER_PIN);
}

static inline void epd_assert_power(void) {
	mcp23008_clearPins(MCP23008_EPD_I2C_ADDR, EPD_ONN_EXPANDER_PIN);
}

static inline void epd_deassert_power(void) {
	mcp23008_setPins(MCP23008_EPD_I2C_ADDR, EPD_ONN_EXPANDER_PIN);
}


static inline void epd_init_reset(void) {
	mcp23008_clearPins(MCP23008_EPD_I2C_ADDR, EPD_RESETN_EXPANDER_PIN);
}

static inline void epd_assert_reset(void) {
	mcp23008_clearPins(MCP23008_EPD_I2C_ADDR, EPD_RESETN_EXPANDER_PIN);
}

static inline void epd_deassert_reset(void) {
	mcp23008_setPins(MCP23008_EPD_I2C_ADDR, EPD_RESETN_EXPANDER_PIN);
}


static inline void epd_init_discharge(void) {
	mcp23008_clearPins(MCP23008_EPD_I2C_ADDR, EPD_DISCHARGE_EXPANDER_PIN);
}

static inline void epd_assert_discharge(void) {
	mcp23008_setPins(MCP23008_EPD_I2C_ADDR, EPD_DISCHARGE_EXPANDER_PIN);
}

static inline void epd_deassert_discharge() {
	mcp23008_clearPins(MCP23008_EPD_I2C_ADDR, EPD_DISCHARGE_EXPANDER_PIN);
}


static inline void epd_init_border(void) {
	mcp23008_clearPins(MCP23008_EPD_I2C_ADDR, EPD_BORDER_CTRLN_EXPANDER_PIN);
}

static inline void epd_assert_border(void) {
	mcp23008_clearPins(MCP23008_EPD_I2C_ADDR, EPD_BORDER_CTRLN_EXPANDER_PIN);
}

static inline void epd_deassert_border(void) {
	mcp23008_setPins(MCP23008_EPD_I2C_ADDR, EPD_BORDER_CTRLN_EXPANDER_PIN);
}


static inline void epd_assert_cs(void) {
	spi_csmux_select(EPD_CSN);
	nrf_gpio_pin_clear(CSN_ENN_PIN);
}

static inline void epd_deassert_cs(void) {
	nrf_gpio_pin_set(CSN_ENN_PIN);
}

static inline void epd_init_busy(void) {
	mcp23008_tristatePins(MCP23008_EPD_I2C_ADDR, EPD_BUSY_EXPANDER_PIN);
}

static inline bool epd_is_busy(void) {
	uint8_t pins;

	/* Read the state of all pins on the port expanded dedicated to the EPD */
	if (!mcp23008_readPins(MCP23008_EPD_I2C_ADDR, &pins)) {
		/* If we fail to communicate with the port expander, assume that the
		 * busy signal in inactive. */
		return false;
	}

	/* Return true of the BUSY pin is high */
	if (pins & EPD_BUSY_EXPANDER_PIN) {
		return true;
	}

	return false;
}


void epd_init(bool flipHorizontal, bool flipVertical);
bool epd_update(bool (*lineMemoryRead_fcn)(uint8_t lineNumber, uint8_t *lineData), int8_t tempC);

#endif /* EPD_H_ */
