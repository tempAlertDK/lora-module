/*
 * epdScreen.h
 *
 * Created: 9/5/2014 3:16:10 PM
 *  Author: kwgilpin
 */ 


#ifndef EPDSCREEN_H_
#define EPDSCREEN_H_

#include <stdint.h>
#include <stdbool.h>

bool epdScreen_update(const char *text);

#endif /* EPDSCREEN_H_ */