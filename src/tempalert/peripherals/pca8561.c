/*
 * pca8561.c
 *
 *  Created on: Jan 2, 2017
 *      Author: Nate-Tempalert
 */

#include <stdint.h>
#include <stdbool.h>

#include "app_twi.h"

#include "app_util_platform.h"

#include "SEGGER_RTT.h"

#include "boards.h"
#include "pca8561.h"
#include "FreeRTOS.h"
#include "task.h"
#include "twi_register.h"
#include "twi_mtx.h"
#include "semphr.h"
#include "string.h"

/* I2C REGS */
#define PCA_TWI_ADDR		0x38

#define  SWRESET			0x00	//  Writing value 0b00101100 [0x2C] executes sw reset of device
#define  DEVICECTL			0x01	//  Defines CLK freq and occilator control
#define  DISPLAYCTL1		0x02	//	Basic Control, mode and bias selection, enable
#define  DISPLAYCTL2		0x03	//  Blinking and line inversion

#define  COM0_1				0x04	//	Segments 7  -> 0
#define  COM0_2				0x05	//	Segments 15 -> 8
#define  COM0_3				0x05	//	Segments 17,16

#define  COM1_1				0x04	//	Segments 7  -> 0
#define  COM1_2				0x05	//	Segments 15 -> 8
#define  COM1_3				0x05	//	Segments 17,16

#define  COM2_1				0x04	//	Segments 7  -> 0
#define  COM2_2				0x05	//	Segments 15 -> 8
#define  COM2_3				0x05	//	Segments 17,16

#define  COM3_1				0x04	//	Segments 7  -> 0
#define  COM3_2				0x05	//	Segments 15 -> 8
#define  COM3_3				0x05	//	Segments 17,16

#define  SWRESET_CMD		0x2C	// Applying this value to the SWRESET register will trigger a SW reset of the device.

// Device Control register bits
#define  FRAMEFREQ_MASK  	0b111
#define  FRAMEFREQ			2
#define  INTOSCEN_MASK		0b1
#define  INTOSCEN			1
#define  CLKOUTEN_MASK		0b1
#define  CLKOUTEN			0

// Display Control 0 register bits
#define  BOOSTEN			4
#define  BOOSTEN_MASK		0b1
#define  MUXMODE			2
#define  MUXMODE_MASK		0b11
#define  BIASSEL			1
#define  BIASSEL_MASK		0b1
#define  DISPLAYEN			0
#define  DISPLAYEN_MASK		0b1

// Display Control 1 register bits
#define  BLINKEN			1
#define  BLINKEN_MASK		0b11
#define  INVMODEEN			0
#define  INVMODEEN_MASK		0b1

typedef struct {
	uint8_t device_ctl;
	uint8_t display_ctl0;
	uint8_t display_ctl1;
} pca8561Config_t;

#define  BASIC			0

/* Lookup table for character conversion */
static const uint8_t seg2_map[] = {
		0b11110110, // 0
		0b11000000, // 1
		0b01101110, // 2
		0b11101010, // 3
		0b11011000, // 4
		0b10111010, // 5
		0b10111110, // 6
		0b11110000, // 7
		0b11111110, // 8
		0b11111010, // 9
		0b00001000, // a == '-'
		0b00000000, // b == ' '
};

static const uint8_t seg3_map[] = {
		0b01011111, // 0
		0b00000110, // 1
		0b00111011, // 2
		0b00101111, // 3
		0b01100110, // 4
		0b01101101, // 5
		0b01111101, // 6
		0b01000111, // 7
		0b01111111, // 8
		0b01101111, // 9
		0b00100000, // a == '-'
		0b00000000, // b == ' '
};



bool pca8561_swreset(void) {

	return true;
}

bool pca8561_configure(void) {
	bool success = true;

	/* Fill configuration buffer for write */
	uint8_t config_data[4];
	config_data[0] = DEVICECTL;
	config_data[1] = 0b00010001; // device control register
	config_data[2] = 0b00001101; // diplay control register 1
	config_data[3] = 0b00000000; // diplay control register 2
	//   /\   #added 10/26/17 to disable occasional blinking issue

	/* Wrtie configuration */
	if (success) {
		success = twiRegister_writeData(&config, PCA_TWI_ADDR, config_data, sizeof(config_data));
	}

	if (!success) {
		SEGGER_RTT_printf(0, "I2C communication failed to communicate with PCA8561!\r\n");
		return false;
	}

	return true;
}

bool pca8561_enableDisplay(void) {
	pca8561_configure();
	pca8561_updateDisplay("   ");
	return true;
}

bool pca8561_disableDisplay(void) {
	bool success = true;
	/* To disable the display, we simply trigger a software reset of the device */
	/* Fill data write buffer starting at COM0_1 reg. */
	uint8_t cmd_data[2];
	cmd_data[0] = SWRESET;
	cmd_data[1] = SWRESET_CMD; // device control register

	/* execute TWI write to update the software reset register */
	if (success) {
		success = twiRegister_writeData(&config, PCA_TWI_ADDR, cmd_data, sizeof(cmd_data));
	}

	if (!success) {
		SEGGER_RTT_printf(0, "I2C communication failed to communicate with PCA8561!\r\n");
		return success;
	}

	return success;
}

bool pca8561_setDisplay(uint8_t * digits) {
	bool success = true;

	/* Fill data write buffer starting at COM0_1 reg. */
	uint8_t display_data[3];
	display_data[0] = COM0_1;
	display_data[1] = digits[1]; // device control register
	display_data[2] = digits[0]; // display control register 1

	/* execute TWI write to update the segment memory locations */
	if (success) {
		success = twiRegister_writeData(&config, PCA_TWI_ADDR, display_data, sizeof(display_data));
	}

	if (!success) {
		SEGGER_RTT_printf(0, "I2C communication failed to communicate with PCA8561!\r\n");
		return success;
	}

	return success;

}


static char * validSegChars[3] = {
		" 1",
		"0123456789- ",
		"0123456789- "};

bool pca8561_parseStr(char * str, uint8_t * digits) {
	char * strptr;
	uint8_t i,len,offset;
	/* calculate string stuff */
	len = strnlen(str,3);
	offset = 3 - len;

	/* Validate chars to be displayed and convert to valid segment digits */
	for (i = 0; i < len; i++) {
		strptr = strchr(validSegChars[i+offset],str[i]);
		if (strptr) {
			digits[i+offset] = (uint8_t) (strptr - validSegChars[i+offset]);
		} else {
			return false;
		}
	}
	return true;
}


bool pca8561_updateDisplay(char * str) {

	uint8_t digits[3] = { 0, 0, 0 };

	if (!pca8561_parseStr(str, digits)) {
		SEGGER_RTT_printf(0, "ERROR: Passed string is invalid!\r\n");
		return false;
	}

	/* Fill data write buffer starting at COM0_1 reg. */
	uint8_t data[2];
	data[1] = seg2_map[digits[1]]; // device control register
	data[0] = seg3_map[digits[2]]; // diplay control register 1

	/* If the value is >= 100, set the bit located in segment 3's memory loc */
	if (digits[0]) {
		data[0] |= (1 << 7);
	}

	return pca8561_setDisplay(data);
}
