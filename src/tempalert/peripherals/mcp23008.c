/*
 * mcp23008.c
 *
 *  Created on: Dec 30, 2016
 *      Author: Nate-Tempalert
 *
 *      Driver for Microchip I2C GPIO
 */

#include <stdint.h>
#include <stdbool.h>

#include "app_twi.h"
#include "app_util_platform.h"

#include "SEGGER_RTT.h"

#include "boards.h"
#include "twi_register.h"
#include "mcp23008.h"

/* Internal register addresses */
#define  IODIR			0x00
#define  IPOL			0x01
#define  GPINTEN		0x02
#define  DEFVAL			0x03
#define  INTCON			0x04
#define  IOCON			0x05
#define  GPPU			0x06
#define  INTF			0x07
#define  INTCAP_RO		0x08
#define  GPIO			0x09
#define  OLAT			0x0A

static nrf_drv_twi_config_t const mcp23008_twi_config = {
		.scl = SCL_PIN,
		.sda = SDA_PIN,
		.frequency = NRF_TWI_FREQ_100K,
		.interrupt_priority = APP_IRQ_PRIORITY_HIGH
};

bool mcp23008_openDrainInterruptPin(uint8_t addr) {
	bool success = true;

	if (success) {
		success = twiRegister_setBits(&mcp23008_twi_config, addr, IOCON, (1<<2));
	}

	if (!success) {
		SEGGER_RTT_printf(0, "ERROR: Failed to configure INT pin of MCP23008 as open-drain (I2C addr: 0x%02X)\r\n", addr);
	}

	return success;
}


bool mcp23008_setPins(uint8_t addr, uint8_t pins) {
	bool success = true;

	/*
	 * Clear bits in the IODIR register to the make the corresponding pins
	 * outputs.
	 */
	if (success) {
		success = twiRegister_clearBits(&mcp23008_twi_config, addr, IODIR, pins);
	}

	/*
	 * Set bits in the OLAT register to drive output pins high.
	 */
	if (success) {
		success = twiRegister_setBits(&mcp23008_twi_config, addr, OLAT, pins);
	}

	if (!success) {
		SEGGER_RTT_printf(0, "ERROR: Failed to set outputs of MCP23008 (I2C addr: 0x%02X, pins: 0x%02X)\r\n", addr, pins);
	}

	return success;
}

bool mcp23008_clearPins(uint8_t addr, uint8_t pins) {
	bool success = true;

	/*
	 * Clear bits in the IODIR register to the make the corresponding pins
	 * outputs.
	 */
	if (success) {
		success = twiRegister_clearBits(&mcp23008_twi_config, addr, IODIR, pins);
	}

	/*
	 * Clear bits in the OLAT register to drive output pins low.
	 */
	if (success) {
		success = twiRegister_clearBits(&mcp23008_twi_config, addr, OLAT, pins);
	}

	if (!success) {
		SEGGER_RTT_printf(0, "ERROR: Failed to clear outputs of MCP23008 (I2C addr: 0x%02X, pins: 0x%02X)\r\n", addr, pins);
	}

	return success;
}

bool mcp23008_tristatePins(uint8_t addr, uint8_t pins) {
	bool success = true;

	/*
	 * Set bits in the IODIR register to the make the corresponding pins
	 * inputs.
	 */
	if (success) {
		success = twiRegister_setBits(&mcp23008_twi_config, addr, IODIR, pins);
	}

	/*
	 * Clear bits in the GPPU register to disable internal pull-ups
	 */
	if (success) {
		success = twiRegister_clearBits(&mcp23008_twi_config, addr, GPPU, pins);
	}

	if (!success) {
		SEGGER_RTT_printf(0, "ERROR: Failed to tri-state pins of MCP23008 (I2C addr: 0x%02X, pins: 0x%02X)\r\n", addr, pins);
	}

	return success;
}

bool mcp23008_queryTristatePins(uint8_t addr, uint8_t *pins) {
	bool success = true;

	uint8_t inputs;
	uint8_t pullups;

	/*
	 * Query which pins are configured as inputs
	 */
	if (success) {
		success = twiRegister_read(&mcp23008_twi_config, addr, IODIR, &inputs);
	}

	/*
	 * Query which pins have their pull-ups enabled
	 */
	if (success) {
		success = twiRegister_read(&mcp23008_twi_config, addr, GPPU, &pullups);
	}

	/*
	 * Tri-stated pins are those which are inputs and do not have their pull-ups
	 * enabled.
	 */
	*pins = inputs & ~pullups;

	if (!success) {
		SEGGER_RTT_printf(0, "ERROR: Failed to query tri-state pins of MCP23008 (I2C addr: 0x%02X)\r\n", addr);
	}

	return success;
}

bool mcp23008_readPins(uint8_t addr, uint8_t *pins) {
	if (!twiRegister_read(&mcp23008_twi_config, addr, GPIO, pins)) {
		SEGGER_RTT_printf(0, "ERROR: Failed to read MCP23008 pins (I2C addr: 0x%02X)\r\n", addr);
		return false;
	}

	return true;
}


bool mcp23008_enablePinChangeInterrupts(uint8_t addr, uint8_t pins) {
	bool success = true;
	uint8_t dummy;

	/*
	 * Pins must be configured as inputs to be interrupt sources.
	 */
	if (success) {
		success = mcp23008_tristatePins(addr, pins);
	}

	/*
	 * For 'change' interrupts, compare pins against their previous values, not
	 * against a default value.  A potential problem with comparing against a
	 * default value is that the interrupt will not clear until the pin value
	 * changes back to its default.
	 */
	if (success) {
		success = twiRegister_clearBits(&mcp23008_twi_config, addr, INTCON, pins);
	}

	/*
	 * Reading the GPIO register clears pending interrupts
	 */
	if (success) {
		success = twiRegister_read(&mcp23008_twi_config, addr, GPIO, &dummy);
	}

	/*
	 * Enable interrupts for the specified pins
	 */
	if (success) {
		success = twiRegister_setBits(&mcp23008_twi_config, addr, GPINTEN, pins);
	}

	if (!success) {
		SEGGER_RTT_printf(0, "ERROR: Failed to enable pin change interrupts on MCP23008 (I2C addr: 0x%02X, pins: 0x%02X)\r\n", addr, pins);
	}

	return success;
}

bool mcp23008_enableLevelInterrupts(uint8_t addr, uint8_t pins, uint8_t defaultLevels) {
	bool success = true;
	uint8_t dummy;

	/*
	 * Pins must be configured as inputs to be interrupt sources.
	 */
	if (success) {
		success = mcp23008_tristatePins(addr, pins);
	}

	/*
	 * For 'level' interrupts, compare pins against default values. A potential
	 * downside to this approach is that the interrupt will not clear until the
	 * pin value changes back to its default value.
	 */
	if (success) {
		success = twiRegister_setBits(&mcp23008_twi_config, addr, INTCON, pins);
	}

	/*
	 * Set the default pin levels.  We use the set and clear operations to
	 * avoid modifying the default levels for pins that are already configured.
	 */
	if (success) {
		success = twiRegister_setBits(&mcp23008_twi_config, addr, DEFVAL, pins & defaultLevels);
	}

	if (success) {
		success = twiRegister_clearBits(&mcp23008_twi_config, addr, DEFVAL, pins & ~defaultLevels);
	}

	/*
	 * Reading the GPIO register clears pending interrupts
	 */
	if (success) {
		success = twiRegister_read(&mcp23008_twi_config, addr, GPIO, &dummy);
	}

	/*
	 * Enable interrupts for the specified pins
	 */
	if (success) {
		success = twiRegister_setBits(&mcp23008_twi_config, addr, GPINTEN, pins);
	}

	if (!success) {
		SEGGER_RTT_printf(0, "ERROR: Failed to enable level interrupts on MCP23008 (I2C addr: 0x%02X, pins: 0x%02X)\r\n", addr, pins);
	}

	return success;
}

bool mcp23008_disableInterrupts(uint8_t addr, uint8_t pins) {
	bool success = true;
	uint8_t dummy;

	/*
	 * Disable interrupts for the specified pins
	 */
	if (success) {
		success = twiRegister_clearBits(&mcp23008_twi_config, addr, GPINTEN, pins);
	}

	/*
	 * Reading the GPIO register clears pending interrupts
	 */
	if (success) {
		success = twiRegister_read(&mcp23008_twi_config, addr, GPIO, &dummy);
	}

	if (!success) {
		SEGGER_RTT_printf(0, "ERROR: Failed to disable interrupts on MCP23008 (I2C addr: 0x%02X, pins: 0x%02X)\r\n", addr, pins);
	}

	return success;
}

bool mcp23008_readInterruptFlags(uint8_t addr, uint8_t *flags) {
	/*
	 * Note: reading the interrupt flags register does not clear any pending
	 * interrupt.  Instead, one must read the GPIO register or the INTCAP
	 * register to actually clear the pending interrupts.
	 */

	if (!twiRegister_read(&mcp23008_twi_config, addr, INTF, flags)) {
		SEGGER_RTT_printf(0, "ERROR: Failed to read MCP23008 interrupt flags (I2C addr: 0x%02X)\r\n", addr);
		return false;
	}

	return true;
}

bool mcp23008_readInterruptCaptureLevels(uint8_t addr, uint8_t *levels) {
	if (!twiRegister_read(&mcp23008_twi_config, addr, INTCAP_RO, levels)) {
		SEGGER_RTT_printf(0, "ERROR: Failed to read MCP23008 interrupt capture levels (I2C addr: 0x%02X)\r\n", addr);
		return false;
	}

	return true;
}

