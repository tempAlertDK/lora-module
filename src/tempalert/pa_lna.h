#ifndef PA_LNA_H__
#define PA_LNA_H__

#include <stdint.h>
#include <stdbool.h>

#include "ble.h"
#include "app_error.h"
#include "nrf_drv_gpiote.h"
#include "nrf_drv_ppi.h"

bool pa_lna_init(uint32_t gpio_pa_pin, uint32_t gpio_lna_pin);
bool pa_lna_enable(void);
bool pa_lna_disable(void);

#endif
