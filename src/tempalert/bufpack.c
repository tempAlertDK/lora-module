/*
 * Created on: May 6, 2017
 *     Author: shawnlewis
 *
 */

#include <string.h>
#include "FreeRTOS.h"

#include "bufpack.h"

void bufpack_init(bufpack_t *bp, uint8_t *buf, uint32_t max_size) {
	bp->buf = buf;
	bp->idx = 0;
	bp->max_size = max_size;
	bp->error = false;
}

static bool bufpack_check(bufpack_t *bp, int add_bytes) {
	if (bp->error) {
		return bp->error;
	}
	if (bp->idx + add_bytes > bp->max_size) {
		bp->error = true;
	}
	return bp->error;
}

void bufpack_assert_ok(bufpack_t *bp) {
	if (bp->error) {
		APP_ERROR_HANDLER(NRF_ERROR_DATA_SIZE);
	}
}

void bufpack_write1(bufpack_t *bp, uint8_t v) {
	if (bufpack_check(bp, 1)) {
		return;
	}
	bp->buf[bp->idx] = v;
	bp->idx += 1;
}

void bufpack_write2(bufpack_t *bp, uint16_t v) {
	if (bufpack_check(bp, 2)) {
		return;
	}
	bp->buf[bp->idx] = v;
	bp->buf[bp->idx + 1] = v >> 8;
	bp->idx += 2;
}

void bufpack_write3(bufpack_t *bp, uint32_t v) {
	if (bufpack_check(bp, 3)) {
		return;
	}
	bp->buf[bp->idx] = (uint8_t) v;
	bp->buf[bp->idx + 1] = v >> 8;
	bp->buf[bp->idx + 2] = v >> 16;
	bp->idx += 3;
}

void bufpack_write4(bufpack_t *bp, uint32_t v) {
	if (bufpack_check(bp, 4)) {
		return;
	}
	bp->buf[bp->idx] = v;
	bp->buf[bp->idx + 1] = v >> 8;
	bp->buf[bp->idx + 2] = v >> 16;
	bp->buf[bp->idx + 3] = v >> 24;
	bp->idx += 4;
}

void bufpack_write_buf(bufpack_t *bp, uint8_t *buf, int buf_len) {
	if (bufpack_check(bp, buf_len)) {
		return;
	}
	memcpy(&bp->buf[bp->idx], buf, buf_len);
	bp->idx += buf_len;
}

uint8_t bufpack_read1(bufpack_t *bp) {
	if (bufpack_check(bp, 1)) {
		return 0;
	}
	uint8_t result = bp->buf[bp->idx];
	bp->idx += 1;
	return result;
}

uint16_t bufpack_read2(bufpack_t *bp) {
	if (bufpack_check(bp, 2))  {
		return 0;
	}
	uint16_t result = (uint16_t) bp->buf[bp->idx]
		| ((uint16_t) bp->buf[bp->idx + 1]) << 8;
	bp->idx += 2;
	return result;
}

uint32_t bufpack_read3(bufpack_t *bp) {
	if (bufpack_check(bp, 3)) {
		return 0;
	}
	uint32_t result = (uint32_t) bp->buf[bp->idx]
		| ((uint32_t) bp->buf[bp->idx + 1]) << 8
		| ((uint32_t) bp->buf[bp->idx + 2]) << 16;
	bp->idx += 3;
	return result;
}

uint32_t bufpack_read4(bufpack_t *bp) {
	if (bufpack_check(bp, 4)) {
		return 0;
	}
	uint32_t result = (uint32_t) bp->buf[bp->idx]
		| ((uint32_t) bp->buf[bp->idx + 1]) << 8
		| ((uint32_t) bp->buf[bp->idx + 2]) << 16
		| ((uint32_t) bp->buf[bp->idx + 3]) << 24;
	bp->idx += 4;
	return result;
}

uint8_t *bufpack_read_buf(bufpack_t *bp, int buf_len) {
	if (buf_len < 0) {
		bp->error = true;
		return NULL;
	}
	if (bufpack_check(bp, buf_len)) {
		return NULL;
	}
	uint8_t *result = &bp->buf[bp->idx];
	bp->idx += buf_len;
	return result;
}
