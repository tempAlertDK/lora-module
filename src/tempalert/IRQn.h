/*
 * IRQn.h
 *
 *  Created on: Jan 12, 2017
 *      Author: kwgilpin
 */

#ifndef SRC_TEMPALERT_IRQN_H_
#define SRC_TEMPALERT_IRQN_H_

#include <stdint.h>
#include <stdbool.h>

#include "queue.h"
#include "task.h"


typedef enum {
	IRQ_SRC_CELL,
	IRQ_SRC_EPD,
	IRQ_SRC_LORA,
	IRQ_SRC_ACCEL,
	IRQ_SRC_EXT_SENSOR
} irq_src_t;

typedef enum {
	IRQ_TYPE_INVALID,
	/* MCP23008 port expander interrupt types */
	IRQ_TYPE_MCP23008_FIRST,
	IRQ_TYPE_MCP23008_PIN0_CHANGE = IRQ_TYPE_MCP23008_FIRST,
	IRQ_TYPE_MCP23008_PIN1_CHANGE,
	IRQ_TYPE_MCP23008_PIN2_CHANGE,
	IRQ_TYPE_MCP23008_PIN3_CHANGE,
	IRQ_TYPE_MCP23008_PIN4_CHANGE,
	IRQ_TYPE_MCP23008_PIN5_CHANGE,
	IRQ_TYPE_MCP23008_PIN6_CHANGE,
	IRQ_TYPE_MCP23008_PIN7_CHANGE,
	IRQ_TYPE_MCP23008_LAST = IRQ_TYPE_MCP23008_PIN7_CHANGE,
	/* LIS2DH12 accelerometer interrupt types */
	IRQ_TYPE_LIS2DH12_FIRST,
	IRQ_TYPE_LIS2DH12_I1_CLICK = IRQ_TYPE_LIS2DH12_FIRST,
	IRQ_TYPE_LIS2DH12_I1_IA1,
	IRQ_TYPE_LIS2DH12_I1_IA2,
	IRQ_TYPE_LIS2DH12_I1_ZYXDA,
	IRQ_TYPE_LIS2DH12_I1_WTM,
	IRQ_TYPE_LIS2DH12_I1_OVERRUN,
	IRQ_TYPE_LIS2DH12_I2_CLICK,
	IRQ_TYPE_LIS2DH12_I2_IA1,
	IRQ_TYPE_LIS2DH12_I2_IA2,
	IRQ_TYPE_LIS2DH12_I2_BOOT,
	IRQ_TYPE_LIS2DH12_I2_ACT,
	IRQ_TYPE_LIS2DH12_LAST = IRQ_TYPE_LIS2DH12_I2_ACT,
	/* */
	IRQ_TYPE_EXT_SENSOR_NEGATIVE_SPIKE
} irq_type_t;

typedef enum {
	IRQ_SUBTYPE_NONE,
	IRQ_SUBTYPE_RISING_EDGE,
	IRQ_SUBTYPE_FALLING_EDGE
} irq_subtype_t;


typedef struct {
	irq_src_t source;
	irq_type_t type;
	irq_subtype_t subtype;
} irq_params_t;


int irqn_registerCallback(const irq_params_t *p_params, void (*cb)(const irq_params_t *));
bool irqn_deregisterCallback(int handle);

void irqn_init(void);

#endif /* SRC_TEMPALERT_IRQN_H_ */
