/*
 * uart.c
 *
 *  Created on: Jan 3, 2017
 *      Author: Nate-Tempalert
 */

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "SEGGER_RTT.h"
#include "app_uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "portmacro_cmsis.h"
#include "bsp.h"
#include "boards.h"
#include "nrf_gpio.h"

static SemaphoreHandle_t m_uart_event_ready; /**< Semaphore raised if there is a new event to be processed in the BLE thread. */
static StaticSemaphore_t m_uart_event_ready_buffer;

static TaskHandle_t m_uart_thread; /**< Definition of BLE stack thread. */
static StaticTask_t m_uart_thread_static;
#define UART_STACK_SIZE		128
StackType_t m_uart_thread_buffer[ UART_STACK_SIZE ];

app_uart_event_handler_t uart_callback(app_uart_evt_t * p_event) {
	if (p_event->evt_type == APP_UART_DATA_READY) {
		LEDS_INVERT(BSP_LED_1_MASK);

	    BaseType_t yield_req = pdFALSE;
	    // The returned value may be safely ignored, if error is returned it only means that
	    // the semaphore is already given (raised).
	    UNUSED_VARIABLE(xSemaphoreGiveFromISR(m_uart_event_ready, &yield_req));
	    portYIELD_FROM_ISR(yield_req);
	} else if (p_event->evt_type == APP_UART_TX_EMPTY) {

	} else {
		SEGGER_RTT_printf(0,"uarterr: %u\r\n",p_event->evt_type);
	}
	return NRF_SUCCESS;
}


#define MAX_TEST_DATA_BYTES     (15U)                /**< max number of test bytes to be used for tx and rx. */
#define UART_TX_BUF_SIZE 256                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE 1                           /**< UART RX buffer size. */

void uart_thread(void *arg) {

	uint32_t err_code;
	const app_uart_comm_params_t comm_params =
	  {
		  CELL_RXD_PIN,
		  CELL_TXD_PIN,
		  CELL_RTS_PIN,
		  CELL_CTS_PIN,
		  APP_UART_FLOW_CONTROL_DISABLED,
		  false,
		  UART_BAUDRATE_BAUDRATE_Baud115200
	  };

	APP_UART_FIFO_INIT(&comm_params,
						 UART_RX_BUF_SIZE,
						 UART_TX_BUF_SIZE,
						 (app_uart_event_handler_t)uart_callback,
						 APP_IRQ_PRIORITY_LOW,
						 err_code);

	APP_ERROR_CHECK(err_code);

	uint8_t in_byte;
	SEGGER_RTT_printf(0,"UART THREAD STARTED\r\n");
    while (1)
       {
    	   LEDS_ON(BSP_LED_1_MASK);
           /* Wait for event from SoftDevice */
           while(pdFALSE == xSemaphoreTake(m_uart_event_ready, portMAX_DELAY))
           {
               // Just wait again in the case when INCLUDE_vTaskSuspend is not enabled
           }

           while (app_uart_get(&in_byte) == NRF_SUCCESS) {
        	   SEGGER_RTT_printf(0,"%c",in_byte);
           }

       }

}

bool uart_sendStr(char * txStr) {
	char * ptr = txStr;
	uint32_t err_code = NRF_SUCCESS;
	while ((*ptr != '\0') && (err_code == NRF_SUCCESS)) {
		app_uart_put(*ptr);
		ptr++;
	}
	return true;
}

bool uart_init(void) {

	// Init a semaphore for the UART thread.
	m_uart_event_ready = xSemaphoreCreateBinaryStatic(&m_uart_event_ready_buffer);
	if(NULL == m_uart_event_ready)
	{
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	// Start execution.
	if ((m_uart_thread = xTaskCreateStatic(uart_thread, "UART", UART_STACK_SIZE, NULL, 1,
			m_uart_thread_buffer, &m_uart_thread_static)) == NULL)
	{
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	return true;
}

