/*
 * extsensor.h
 *
 *  Created on: Jun 30, 2017
 *      Author: kwgilpin
 */

#ifndef SRC_TEMPALERT_EXTSENSOR_H_
#define SRC_TEMPALERT_EXTSENSOR_H_

#include "boards.h"
#include "FreeRTOS.h"
#include "portmacro_cmsis.h"

bool extsensor_getState(void);
bool extsensor_read(void);

#if defined (BOARD_CELL_NODE_REV1P0) 
    #define extsensor_init() extsensor_off()
    #define extsensor_on() nrf_gpio_cfg_input(EXTSENSOREN_PIN,NRF_GPIO_PIN_PULLUP)
    #define extsensor_off() nrf_gpio_cfg_input(EXTSENSOREN_PIN,NRF_GPIO_PIN_PULLDOWN)


#elif defined(BOARD_CELL_NODE_REV1P1) | defined (BOARD_LORA_MODULE_REV1P0)
    void extsensor_init(void);
    
    void extsensor_off(void);
    void extsensor_on(TickType_t powerUpTicks);
    static inline void extsensor_off_(uint8_t * x) {extsensor_off();}
    static inline uint8_t extsensor_on_(TickType_t powerUpTicks) {extsensor_on(powerUpTicks); return 0;}

    #define EXT_SENSOR_DEFAULT_DELAY_TICKS		pdMS_TO_TICKS(10)
    #define EXT_SENSOR_CUSTOM_DELAY(ticks) for (uint8_t taken __attribute__((__cleanup__(extsensor_off_))) = extsensor_on_(ticks), __ToDo = 1; __ToDo; __ToDo = 0)
    #define EXT_SENSOR_DEFAULT_DELAY() for (uint8_t taken __attribute__((__cleanup__(extsensor_off_))) = extsensor_on_(EXT_SENSOR_DEFAULT_DELAY_TICKS), __ToDo = 1; __ToDo; __ToDo = 0)

    #define GET_MACRO(_0, _1, NAME, ...) NAME
    #define EXT_SENSOR(...) GET_MACRO(_0, ##__VA_ARGS__, EXT_SENSOR_CUSTOM_DELAY, EXT_SENSOR_DEFAULT_DELAY)(__VA_ARGS__)

#endif


#endif /* SRC_TEMPALERT_EXTSENSOR_H_ */
