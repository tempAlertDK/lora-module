/*
 * deviceID.c
 *
 *  Created on: Jul 11, 2016
 *      Author: kwgilpin
 */

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>

#include "SEGGER_RTT.h"

#include "si705x.h"
#include "deviceID.h"

static char m_deviceIDString[21] = "11777000000000000000";
static uint64_t m_serialNumber = 0;
static uint8_t m_serialNumberArray[8];
static void deviceID_convertUInt64ToString(const uint64_t *uint64, char *string, size_t maxSize);
static bool m_init = false;
void deviceID_init() {
	uint8_t serialNumberArray[8];

	if (si705x_getSerial(serialNumberArray)) {
		/* The Si705x provides an 8-byte serial number which we read left-to-
		 * right, so the most-significant byte has the lowest array index and
		 * the least-significant byte has the highest. */
		m_serialNumber  = (uint64_t)serialNumberArray[7] << 0;
		m_serialNumber |= (uint64_t)serialNumberArray[6] << 8;
		m_serialNumber |= (uint64_t)serialNumberArray[5] << 16;
		m_serialNumber |= (uint64_t)serialNumberArray[4] << 24;
		m_serialNumber |= (uint64_t)serialNumberArray[3] << 32;
		m_serialNumber |= (uint64_t)serialNumberArray[2] << 40;
		m_serialNumber |= (uint64_t)serialNumberArray[1] << 48;
		m_serialNumber |= (uint64_t)serialNumberArray[0] << 56;

	    memcpy (m_serialNumberArray, serialNumberArray, sizeof(serialNumberArray));

	    /* Convert the serial number to a 20 decimal digit string */
	    deviceID_convertUInt64ToString(&m_serialNumber, m_deviceIDString, sizeof(m_deviceIDString));

	    /* When taken from the Si705x, the serial number is a full 8 bytes.
	     * We know that 2^64-1 is 20 decimal digits (18,...).  The most
	     * significant digit is 1.  To avoid confusion with other devices who
	     * have a 11777 (or similar) prefix, we add 40,000,000,000,000,000,000)
	     * to the result so that any device ID derived from a Si705x always
	     * begins with '4' or '5'. */
	    if (m_deviceIDString[0] == '0') {
	    	m_deviceIDString[0] = '4';
	    } else {
	    	m_deviceIDString[0] = '5';
	    }
	} else {
		m_serialNumber = 0;
		strcpy(m_deviceIDString, "11777000000000000000");
	}

	m_init = true;

}

bool deviceID_getSerialNumber(uint64_t *serialNumber) {
	if (m_init) {
		*serialNumber = m_serialNumber;
		return true;
	} else {
		return false;
	}
}

bool deviceID_getSerialNumberArray(uint8_t * p_serialArray) {
	if (m_init) {
		memcpy (p_serialArray, m_serialNumberArray, sizeof(m_serialNumberArray));
		return true;
	} else {
		return false;
	}
}

bool deviceID_getDeviceIDString(char *deviceIDString, size_t stringSize) {
	if (stringSize < sizeof(m_deviceIDString)) {
		return false;
	}

	strncpy(deviceIDString, m_deviceIDString, stringSize);
	deviceIDString[stringSize-1] = '\0';

	return true;
}

bool deviceID_getDeviceIDencString(char * deviceIDencString, size_t encStringSize) {
	if (encStringSize < sizeof(m_deviceIDString)>>1) {
		return false;
	}

	unsigned int i = 0;

	for (i = 0; i < encStringSize-1; i++) {
		deviceIDencString[i] = (char)(0xFF & (m_deviceIDString[i<<1]-'0')<<4) | (char)(m_deviceIDString[1 + (i<<1)]-'0');
	}
	deviceIDencString[encStringSize-1] = '\0';

	return true;
}

void deviceID_convertUInt64ToString(const uint64_t *uint64, char *string, size_t maxSize) {
	uint64_t v;

	char* p = string + maxSize;
	*(--p) = '\0';

	v = *uint64;

	for (uint8_t i = 1; i < maxSize; i++) {
		const uint32_t digit = v % 10;
		const char c = '0' + digit;
		*(--p) = c;
		v = v / 10;
	}
}
