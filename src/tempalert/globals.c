/*
 * globals.c
 *
 *  Created on: Mar 3, 2017
 *      Author: Nate-Tempalert
 */
#include "globals.h"

bool g_alarmState = false;
uint8_t g_alarmCount = 0;
uint8_t g_alarmPorts[MAX_ALARM_PORTS];

bool g_updateUnixTime = false;
bool g_bleIsAdvertising = false;
bool g_bleIsConnected = false;
bool g_modemIsBooting = false;
bool g_uploadInprogress = false;
bool g_lastUploadFailure = false;

/* global transmit flag -- will be set to true in scController_init */
bool g_sc_transmit = false;

/* sensor simulation */
bool g_simTemp = false;
int16_t g_simTemp10DegC = 0;
bool g_simBatt = false;
uint16_t g_simBattVoltage;
bool g_simLevel = false;
int8_t g_simLevelPercent = 0;
uint32_t g_simCapacitance = 0;

/* General Device Information */
resetReason_t g_resetReason = RESET_REASON_UNKNOWN;
