/*
 * IRQn.c
 *
 *  Created on: Jan 12, 2017
 *      Author: kwgilpin
 */


#include <stdint.h>
#include <stdbool.h>
#include <string.h>


#include "nrf_drv_gpiote.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "portmacro_cmsis.h"


#include "SEGGER_RTT.h"

#include "boards.h"

#include "twi_mtx.h"
#include "mcp23008.h"
#include "lis2dh12.h"
#include "IRQn.h"


static SemaphoreHandle_t m_irqn_active_low = NULL;
static StaticSemaphore_t irqn_active_low_buffer;

static SemaphoreHandle_t m_irqn_mutex = NULL;
static StaticSemaphore_t irqn_mutex_buffer;

static TaskHandle_t irqn_thread_handle;
static StaticTask_t irqn_thread_tcb;
#define IRQN_THREAD_STACK_SIZE 256
static StackType_t irqn_thread_stack[IRQN_THREAD_STACK_SIZE];


typedef struct {
	irq_params_t params;
	void (*fcn)(const irq_params_t *);
} irq_callback_t;

#define MAX_CALLBACKS 10

irq_callback_t callbacks[MAX_CALLBACKS];

static unsigned int active_callback_count;


void irqn_thread(void *arg);

bool irqn_validateParams(const irq_params_t *p_params);

static uint8_t irqn_portExpanderIrqTypeToPinMask(irq_type_t type);
static irq_type_t irqn_portExpanderPinMaskToIrqType(uint8_t pin_mask);
static bool irqn_enablePortExpanderInterrupts(uint8_t addr, uint8_t pin_mask);
static bool irqn_disablePortExpanderInterrupts(uint8_t addr, uint8_t pin_mask);
static void irqn_processPortExpanderInterrupts(uint8_t addr);


static void irqn_dispatch(const irq_params_t *irq_params);
static void irqn_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);


int irqn_registerCallback(const irq_params_t *p_params, void (*cb)(const irq_params_t *)) {
	int handle = -1;
	int i;

	if (!irqn_validateParams(p_params)) {
		return -1;
	}

	/*
	 * Take control of the mutex which controls access to the callbacks[] array
	 * and ensures that the enabling and disabling of interrupts is atomic.
	 */
	if (m_irqn_mutex == NULL) {
		return -1;
	} else if (xSemaphoreTake(m_irqn_mutex, pdMS_TO_TICKS(10)) != pdTRUE) {
		return -1;
	}

	/*
	 * Search for an available element in the array of parameter / callback
	 * pairs.
	 */
	for (i = 0; i < MAX_CALLBACKS; i++) {
		if (callbacks[i].fcn == NULL) {
			callbacks[i].fcn = cb;
			memcpy(&callbacks[i].params, p_params, sizeof(callbacks[i].params));
			handle = i;
			break;
		}
	}

	if (handle >= 0) {
		/*
		 * If we found an available element in the array of callbacks,
		 * enable the interrupt.
		 */
		switch (p_params->source) {
		case IRQ_SRC_CELL:
			irqn_enablePortExpanderInterrupts(MCP23008_CELL_I2C_ADDR, irqn_portExpanderIrqTypeToPinMask(p_params->type));
			break;
		case IRQ_SRC_EPD:
			irqn_enablePortExpanderInterrupts(MCP23008_EPD_I2C_ADDR, irqn_portExpanderIrqTypeToPinMask(p_params->type));
			break;
		case IRQ_SRC_LORA:
			irqn_enablePortExpanderInterrupts(MCP23008_LORA_I2C_ADDR, irqn_portExpanderIrqTypeToPinMask(p_params->type));
			break;
		case IRQ_SRC_ACCEL:
			// TODO: Call function in LIS2DH12 driver file to enable the specified interrupt
			break;
		case IRQ_SRC_EXT_SENSOR:
			/*
			 * Nothing to do here--the external sensor interrupt is a simple
			 * capacitive between the IRQn net (on the processor side) and the
			 * EXTINTn net on the external sensor connector.
			 */
			break;
		default:
			break;
		}

		active_callback_count++;

		/*
		 * Now that we have at least one callback active, enable the interrupt
		 * on the IRQn line
		 */
		nrf_drv_gpiote_in_event_enable(IRQN_PIN, true);
	}


	xSemaphoreGive(m_irqn_mutex);

	return handle;
}

bool irqn_deregisterCallback(int handle) {
	irq_params_t deregistered_cb_params;
	bool handle_was_active = false;
	bool interrupt_still_in_use;


	/* Validate the handle */
	if ((handle < 0) || (handle >= MAX_CALLBACKS)) {
		return false;
	}

	/*
	 * Take control of the mutex which controls access to the callbacks[] array
	 * and ensures that the enabling and disabling of interrupts is atomic.
	 */
	if (m_irqn_mutex == NULL) {
		return false;
	} else if (xSemaphoreTake(m_irqn_mutex, pdMS_TO_TICKS(10)) != pdTRUE) {
		return false;
	}


	/*
	 * Check whether the callback associated with the handle is active.
	 */
	if (callbacks[handle].fcn != NULL) {
		handle_was_active = true;
		/*
		 * If the provided handled referenced an active callback, we'll need to
		 * disable the interrupt, so we copy the parameters to a local variable
		 * for use later.
		 */
		memcpy(&deregistered_cb_params, &callbacks[handle].params, sizeof(deregistered_cb_params));
		/*
		 * Setting the callback function to NULL effectively marks the element
		 * in the callbacks array as available for reuse.
		 */
		callbacks[handle].fcn = NULL;
	}


	if (handle_was_active) {
		/*
		 * Check whether other callbacks are triggered by the same interrupt
		 * conditions.  If there are other callbacks associated with the same
		 * interrupt conditions, we will leave the interrupt enabled.
		 */
		interrupt_still_in_use = false;
		for (int i = 0; i < MAX_CALLBACKS; i++) {
			if (callbacks[i].fcn == NULL) {
				continue;
			} else if ((callbacks[i].params.source == deregistered_cb_params.source) && (callbacks[i].params.type == deregistered_cb_params.type)) {
				/* If we find that another callback is associated with the same
				 * interrupt source and the same conditions, we do not want to
				 * disable the interrupt.
				 */
				interrupt_still_in_use = true;
				break;
			}
		}

		if (!interrupt_still_in_use) {
			switch (deregistered_cb_params.source) {
			case IRQ_SRC_CELL:
				irqn_disablePortExpanderInterrupts(MCP23008_CELL_I2C_ADDR, irqn_portExpanderIrqTypeToPinMask(deregistered_cb_params.type));
				break;
			case IRQ_SRC_EPD:
				irqn_disablePortExpanderInterrupts(MCP23008_EPD_I2C_ADDR, irqn_portExpanderIrqTypeToPinMask(deregistered_cb_params.type));
				break;
			case IRQ_SRC_LORA:
				irqn_disablePortExpanderInterrupts(MCP23008_LORA_I2C_ADDR, irqn_portExpanderIrqTypeToPinMask(deregistered_cb_params.type));
				break;
			case IRQ_SRC_ACCEL:
				// TODO: Call function in LIS2DH12 driver file to disable the specified interrupt
				break;
			case IRQ_SRC_EXT_SENSOR:
				break;
			default:
				break;
			}

			/*
			 * Decrement the counter which tracks the number of active
			 * callbacks.  If it reaches zero, disable the external interrupt
			 * on the IRQn pin.
			 */
			if (active_callback_count > 0) {
				active_callback_count--;
			}

			if (active_callback_count == 0) {
				nrf_drv_gpiote_in_event_disable(IRQN_PIN);
			}

		}
	}

	xSemaphoreGive(m_irqn_mutex);

	return true;
}


void irqn_init() {
	uint32_t err_code;

	/*
	 * Initialize the semaphore that is released by the ISR monitoring the IRQn
	 * line at the nRF's input pin.  When the IRQn goes low, the ISR will give
	 * this semaphore.  Meanwhile, the thread is waiting to take the semaphore.
	 * Once the thread is able to take the semaphore, it queries all possible
	 * interrupt sources to determine the particular cause of the interrupt.
	 */

	/*
	 * Create a mutex which protects the callbacks[] array and ensures that
	 * the enabling and disabling of interrupts is atomic.
	 */
	if ((m_irqn_mutex = xSemaphoreCreateMutexStatic(&irqn_mutex_buffer)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}


	/*
	 * Create mutex which is used by the IRQn line ISR to signal to the IRQ
	 * task that that IRQn has gone active-low
	 */
	if ((m_irqn_active_low = xSemaphoreCreateMutexStatic(&irqn_active_low_buffer)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	WITH_TWI_MTX() {
		/*
		 * By default, the interrupt pins on the MCP23008 port expanders are
		 * push-pull.  They need to be open-drain to avoid contention with each
		 * other.
		 */
		mcp23008_openDrainInterruptPin(MCP23008_CELL_I2C_ADDR);
		mcp23008_openDrainInterruptPin(MCP23008_EPD_I2C_ADDR);
		mcp23008_openDrainInterruptPin(MCP23008_LORA_I2C_ADDR);

		/*
		 * By default, the accelerometer's interrupt pins are active high (low by
		 * default).  They drive the OE* inputs to two tri-state buffers.  So, we
		 * need to change the interrupt pins to be active low (default high) so
		 * that the tri-state buffers' outputs are high-z until an interrupt
		 * occurs.
		 */
		lis2dh12_activeLowInterruptPins();
	}

	/*
	 * If the queue field of an 'instruction' is NULL, it indicates that the
	 * instruction is currently unused.  Here initialize the list of registered
	 * queues.
	 */
	for (uint8_t i = 0; i < MAX_CALLBACKS; i++) {
		callbacks[i].fcn = NULL;
	}

	active_callback_count = 0;


	/*
	 * Setup the pin change interrupt which monitors the IRQn pin
	 */
	nrf_drv_gpiote_init();

    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(true);

    err_code = nrf_drv_gpiote_in_init(IRQN_PIN, &in_config, irqn_pin_handler);
    APP_ERROR_CHECK(err_code);

    /* Start the IRQN thread */
    if ((irqn_thread_handle = xTaskCreateStatic(irqn_thread, "IRQN", IRQN_THREAD_STACK_SIZE, NULL, 1, irqn_thread_stack, &irqn_thread_tcb)) == NULL) {
    	APP_ERROR_CHECK(NRF_ERROR_INVALID_PARAM);
    }
}

/*
 * Task which waits for semaphore to be released by the interrupt handler and
 * then checks the state of the pins on each of the MCP23008 port expanders,
 * the accelerometer, and the external sensor.
 */
void irqn_thread(void *arg) {
	UNUSED_PARAMETER(arg);

	while (1) {
		/* Wait for the IRQn line to go low */
		while (pdFALSE == xSemaphoreTake(m_irqn_active_low, portMAX_DELAY));

		/* Check each of the 3 port expanders for active interrupts */
		irqn_processPortExpanderInterrupts(MCP23008_CELL_I2C_ADDR);
		irqn_processPortExpanderInterrupts(MCP23008_LORA_I2C_ADDR);
		irqn_processPortExpanderInterrupts(MCP23008_EPD_I2C_ADDR);
	}
}


bool irqn_validateParams(const irq_params_t *p_params) {
	bool valid = false;

	if ((p_params->source == IRQ_SRC_CELL) || (p_params->source == IRQ_SRC_EPD)
			|| (p_params->source == IRQ_SRC_LORA)) {

		if ((IRQ_TYPE_MCP23008_FIRST <= p_params->type)
				&& (p_params->type <= IRQ_TYPE_MCP23008_LAST)
				&& ((p_params->subtype == IRQ_SUBTYPE_RISING_EDGE)
						|| (p_params->subtype == IRQ_SUBTYPE_FALLING_EDGE))) {
			valid = true;
		}
	} else if (p_params->source == IRQ_SRC_ACCEL) {
		if ((IRQ_TYPE_LIS2DH12_FIRST <= p_params->type)
				&& (p_params->type <= IRQ_TYPE_LIS2DH12_LAST)
				&& (p_params->subtype == IRQ_SUBTYPE_NONE)) {
			valid = true;
		}
	} else if ((p_params->source == IRQ_SRC_EXT_SENSOR)
			&& (p_params->type == IRQ_TYPE_EXT_SENSOR_NEGATIVE_SPIKE)
			&& (p_params->subtype == IRQ_SUBTYPE_NONE)) {
		valid = true;
	}

	return valid;
}


uint8_t irqn_portExpanderIrqTypeToPinMask(irq_type_t type) {
	switch (type) {
	case IRQ_TYPE_MCP23008_PIN0_CHANGE:
		return 0x01;
	case IRQ_TYPE_MCP23008_PIN1_CHANGE:
		return 0x02;
	case IRQ_TYPE_MCP23008_PIN2_CHANGE:
		return 0x04;
	case IRQ_TYPE_MCP23008_PIN3_CHANGE:
		return 0x08;
	case IRQ_TYPE_MCP23008_PIN4_CHANGE:
		return 0x10;
	case IRQ_TYPE_MCP23008_PIN5_CHANGE:
		return 0x20;
	case IRQ_TYPE_MCP23008_PIN6_CHANGE:
		return 0x40;
	case IRQ_TYPE_MCP23008_PIN7_CHANGE:
		return 0x80;
	default:
		return 0x00;
	}
}


irq_type_t irqn_portExpanderPinMaskToIrqType(uint8_t pin_mask) {
	switch (pin_mask) {
	case 0x01:
		return IRQ_TYPE_MCP23008_PIN0_CHANGE;
	case 0x02:
		return IRQ_TYPE_MCP23008_PIN1_CHANGE;
	case 0x04:
		return IRQ_TYPE_MCP23008_PIN2_CHANGE;
	case 0x08:
		return IRQ_TYPE_MCP23008_PIN3_CHANGE;
	case 0x10:
		return IRQ_TYPE_MCP23008_PIN4_CHANGE;
	case 0x20:
		return IRQ_TYPE_MCP23008_PIN5_CHANGE;
	case 0x40:
		return IRQ_TYPE_MCP23008_PIN6_CHANGE;
	case 0x80:
		return IRQ_TYPE_MCP23008_PIN7_CHANGE;
	default:
		return IRQ_TYPE_INVALID;
	}
}

bool irqn_enablePortExpanderInterrupts(uint8_t addr, uint8_t pin_mask) {
	bool success = false;

	WITH_TWI_MTX() {
		success = mcp23008_enablePinChangeInterrupts(addr, pin_mask);
	}

    return success;
}

bool irqn_disablePortExpanderInterrupts(uint8_t addr, uint8_t pin_mask) {
	bool success = false;

	WITH_TWI_MTX() {
		success = mcp23008_disableInterrupts(addr, pin_mask);
	}

    return success;
}

void irqn_processPortExpanderInterrupts(uint8_t addr) {
	uint8_t flags, levels;
	irq_params_t irq_params;

	/*
	 * Ensure that the provided address is valid and then set the source
	 * field of the irq_params struct appropriately.
	 */

	if (addr == MCP23008_CELL_I2C_ADDR) {
		irq_params.source = IRQ_SRC_CELL;
	} else if (addr == MCP23008_EPD_I2C_ADDR) {
		irq_params.source = IRQ_SRC_EPD;
	} else if (addr == MCP23008_LORA_I2C_ADDR) {
		irq_params.source = IRQ_SRC_LORA;
	} else {
		return;
	}

	WITH_TWI_MTX() {
		/*
		 * Read which interrupts are active then clear the interrupts by
		 * reading the state of the pins when the interrupt occurred.
		 */
		mcp23008_readInterruptFlags(addr, &flags);
		mcp23008_readInterruptCaptureLevels(addr, &levels);
	}

	for (int pin_mask = 0x01; pin_mask <= 0x80; pin_mask <<= 1) {
		if (flags & pin_mask) {
			irq_params.type = irqn_portExpanderPinMaskToIrqType(pin_mask);
			irq_params.subtype = (levels & pin_mask) ? IRQ_SUBTYPE_RISING_EDGE : IRQ_SUBTYPE_FALLING_EDGE;
			irqn_dispatch(&irq_params);
		}
	}
}

void irqn_dispatch(const irq_params_t *irq_params) {
	int i;

	for (i = 0; i < MAX_CALLBACKS; i++) {
		if (callbacks[i].fcn == NULL) {
			break;
		} else if ((irq_params->source == callbacks[i].params.source)
				&& (irq_params->type == callbacks[i].params.type)
				&& (irq_params->subtype == callbacks[i].params.subtype)) {
			callbacks[i].fcn(irq_params);
		}
	}
}

void irqn_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action) {
	BaseType_t yield_req = pdFALSE;

	xSemaphoreGiveFromISR(m_irqn_active_low, &yield_req);
	portYIELD_FROM_ISR(yield_req);
}
