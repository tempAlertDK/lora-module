/*
 * scController.c
 *
 *  Created on: Feb 15, 2017
 *      Author: Nate-Tempalert
 */
#include <stdint.h>
#include <stdbool.h>
#include "bsp.h"
#include "bsp_btn_ble.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "stoFwd.h"
#include "scController.h"
#include "packet.h"
#include "SEGGER_RTT.h"
#include "event_groups.h"
#include "modem.h"
#include "globals.h"
#include "unixTime.h"
#include "ui.h"
#include "dfu_types.h"
#include "bootloader_types.h"
#include "bootloader_settings.h"
#include "bootloader_util.h"
#include "pstorage.h"
#include "nrf_nvic.h"
#include "defines.h"
#include "gitversion.h"
#include "notify.h"
#include "log.h"
#include "errors.h"
#include "errorCount.h"

#if defined (QA) && (QA == 1)
#define CORE_IP 		"52.21.161.189"
#define CORE_PORT		4344
#elif defined (DEV) && (DEV == 1)
#define CORE_IP 		"52.20.157.191"
#define CORE_PORT		4344
#elif (PR) //Production Devices
#define CORE_IP 		"54.175.87.89"
#define CORE_PORT		4344
#else
	#error Server enviroment not defined!
#endif

#define CORE_CONTROLLER 				1

TaskHandle_t m_scController_thread;
static StaticTask_t m_scController_thread_static;
#define SCCONTROLLER_STACK_SIZE		512
static StackType_t m_scController_thread_buffer[ SCCONTROLLER_STACK_SIZE ];

EventBits_t sc_eventBitsMask;
EventBits_t sc_activeEventBit;
EventBits_t sc_localEventBits;
EventGroupHandle_t sc_eventGroup = NULL;
StaticEventGroup_t sc_eventGroupStatic;

TimerHandle_t m_uploadTimer;
StaticTimer_t m_uploadTimerStatic;
TimerHandle_t m_genericTimer;
StaticTimer_t m_genericTimerStatic;

NotifyHandle_t uploadFinishedNotifyHandle;
static Notify_t uploadFinishedNotify;

uint8_t m_ota_maj = 0;
uint8_t m_ota_min = 0;

#define UPLOAD_INTERVAL_TIMER 				2;
#define SC_GENERIC_TIMER 					3;

/* Timing defines */
#define SECOND								1000UL
#define MS_PER_MINUTE						60		* SECOND
#define SC_WAIT_FOR_MODEM_GET_STATE			10 		* SECOND
#define SC_WAIT_INTERVAL_FOR_MODEM_BUSY		10 		* SECOND
#define SC_WAIT_FOR_SENSORS_READ				50 		* SECOND
#define SC_WAIT_FOR_STATUS_READ				10 		* SECOND

#define SC_WAIT_FOR_SOCKET					pdMS_TO_TICKS(10		* SECOND)

#define SC_WAIT_FOR_MODEM_TO_BOOT			3	 	* MS_PER_MINUTE
#define SC_WAIT_FOR_MODEM_TO_SLEEP			1 		* MS_PER_MINUTE
#define SC_MIN_RECOVERY_INTERVAL_MINS		15
#define SC_MIN_UPLOAD_INTERVAL_MINS			5
#define SC_DEFAULT_UPLOAD_INTERVAL_MINS		4 * 60					// 4 hour default for device that is unregistered or failing on first try
#define SC_MAX_UPLOAD_INTERVAL_MINS			48 * 60

#if (defined (MODEM_ON_BY_DEFAULT) && (MODEM_ON_BY_DEFAULT == 1))
	#define FIRST_BOOT_DELAY 90
#else
	#define FIRST_BOOT_DELAY 30
#endif

#define MODEM_SLEEP_MIN_UPLOAD_INTERVAL		15

#define SC_PACKET_MAX_RETRIES 				2
#define SC_UPLOAD_MAX_RETRIES 				2
#define SC_SOCKET_MAX_RETRIES 				3
#define SC_MODEM_MAX_RETRIES 				3

TickType_t m_uploadInterval_min = SC_DEFAULT_UPLOAD_INTERVAL_MINS;  // Start with 4 hrs
TickType_t m_recoveryInterval_min = SC_MIN_RECOVERY_INTERVAL_MINS;  // Start with 15 mins

QueueHandle_t scController_controlQueue;

/* Controller state machine definitions */
typedef enum {
	SCC_STATE_NULL = 0,	
	SCC_STATE_INIT,					//1
	SCC_STATE_INIT_MODEM,			//2
	SCC_STATE_FIRST_UPLOAD,			//3
	SCC_STATE_UPLOAD_EVENT,			//4
	SCC_STATE_WAKE_MODEM,			//5
	SCC_STATE_READ_STATUS,			//6
	SCC_STATE_OPEN_SOCKET,			//7
	SCC_STATE_COLLECT_SF_RECORDS,	//8
	SCC_STATE_SEND_PACKET,			//9
	SCC_STATE_RECEIVE_PACKET,		//10
	SCC_STATE_PROCESS_RECEIVED,		//11
	SCC_STATE_CLOSE_SOCKET,			//12
	SCC_STATE_SLEEP_MODEM,			//13
	SCC_STATE_IDLE,					//14
	SCC_STATE_MODEM_ERESET			//15
} sc_state_t;


void scController_thread(void *arg);
bool scController_processReceivedPacket(const uint8_t *p_packet, const uint16_t *p_packetLen);
uploadResult_t scController_verifyPacket(uint8_t *p_packet, const uint16_t packetLen);
sc_state_t scController_getState(void);
bool scController_processState (sc_state_t new_state);

bool scController_checkRecovery(bool uploadSuccess);

void scController_enterSleep(void);
void scController_deviceOta(uint8_t fw_maj, uint8_t fw_min);
void scController_updateTransmitionEvent(scUploadEventType_t eventType);


bool scController_init(void) {
#if defined(CORE_CONTROLLER) && (CORE_CONTROLLER == 1)
	// Create scController event group
	if ((sc_eventGroup = xEventGroupCreateStatic(&sc_eventGroupStatic)) == pdFALSE){
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	/* initialize global alarms */
	g_alarmState = false;
	g_alarmCount = 0;
	uint8_t i = 0;
	for (i = 0; i < MAX_ALARM_PORTS; i++) {
		g_alarmPorts[i] = 0;
	}

	/* Initialize upload completed notification */
	if (notifyInit(&uploadFinishedNotify)) {
		uploadFinishedNotifyHandle = &uploadFinishedNotify;
	}

	/* set the global transmit flag */
#if defined(MODEM_TESTING) && (MODEM_TESTING == 1)
	g_sc_transmit = false;
#else
	g_sc_transmit = true;
#endif

	return true;
#else
	return false;
#endif
}

void scController_start(void) {
	// Start execution.
	if ((m_scController_thread = xTaskCreateStatic(scController_thread, "SCCTL",
			SCCONTROLLER_STACK_SIZE, NULL, 1, m_scController_thread_buffer, &m_scController_thread_static)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
}

void scControllerUploadIntervalExpired(TimerHandle_t xTimer) {
	/* The sensor measurement interval timer has expired, set event bit */
	if (g_sc_transmit) {
		xEventGroupSetBits(sc_eventGroup,SC_EVENTGROUP_UPLOAD_EVENT_BIT);
	}

}

bool scController_setUploadInterval(uint32_t interval) {
	/* interval is in minutes, only update the interval if needed. */
	if (interval < SC_MIN_UPLOAD_INTERVAL_MINS){
		log_infof("Requested measurement interval %u less than SC_MIN_UPLOAD_INTERVAL_MINS", interval);
		return false;
	}
	if (interval != m_uploadInterval_min) {
		if (xTimerChangePeriod(m_uploadTimer, (pdMS_TO_TICKS(MS_PER_MINUTE)) * interval, portMAX_DELAY)) {
			m_uploadInterval_min = interval;
			SEGGER_RTT_printf(0, "New Upload interval: %u min, %u ticks\r\n", m_uploadInterval_min,xTimerGetPeriod( m_uploadTimer ));
		} else {
			SEGGER_RTT_printf(0,"Failed to update measurement interval to %u mins!", interval);
		}
	} else {
		SEGGER_RTT_printf(0, "Upload interval of %u min unchanged. Next upload in %u ticks...\r\n", m_uploadInterval_min, xTimerGetPeriod( m_uploadTimer ) );
	}

	return true;
}

void scController_thread(void *arg) {

	/* Set the first packet flag true here */
	g_firstPacket = true;

	SEGGER_RTT_printf(0, "SENSORCLOUD CONTROLLER THREAD STARTED\r\n");

	/* Start Measurement Interval Timer (MI) */
	uint8_t tid = UPLOAD_INTERVAL_TIMER;
	m_uploadTimer = xTimerCreateStatic("TSNS",( pdMS_TO_TICKS(MS_PER_MINUTE) ) * m_uploadInterval_min,
			pdTRUE, (void *) &tid, scControllerUploadIntervalExpired, &m_uploadTimerStatic);
	xTimerStart(m_uploadTimer, portMAX_DELAY);

	/* Start Generic Timer to trigger first upload interval in 30 seconds... */
	tid = SC_GENERIC_TIMER;
	m_genericTimer = xTimerCreateStatic("SCGT", pdMS_TO_TICKS(FIRST_BOOT_DELAY  * SECOND),
			pdFALSE, (void *) &tid, scControllerUploadIntervalExpired, &m_genericTimerStatic);
	xTimerStart(m_genericTimer, portMAX_DELAY);

	/* Set the event mask */
	sc_eventBitsMask = SC_EVENTGROUP_UPLOAD_EVENT_BIT | SC_EVENTGROUP_SLEEP_EVENT_BIT | SC_EVENTGROUP_OTA_EVENT_BIT;
	sc_localEventBits = 0;

	sc_state_t currentState = SCC_STATE_FIRST_UPLOAD;

	// lets wait for S&F task to finish initialization process
	stoFwd_waitUntilInit();

	for (;;) {

		if (scController_processState(currentState)) {
			;
		} else {
			currentState = scController_getState();
			switch(currentState) {
				case SCC_STATE_WAKE_MODEM:
				case SCC_STATE_SLEEP_MODEM:
					break;
				default:
					/* TODO: Handle errors and trigger actions. */
					SEGGER_RTT_printf(0,"Unexpected failure in state %u, need proper error handling![%s:%u]",currentState,__FUNCTION__,__LINE__);
					break;
			}

		}
		currentState = scController_getState();
	
		/* Between state transisions, check the global transmission flag,
		 * if false, pause the state machine */
		if (!g_sc_transmit) {
			while (!g_sc_transmit) {
				vTaskDelay(pdMS_TO_TICKS(500));
			}
		}
	}
}

static sc_state_t m_current_state = SCC_STATE_INIT;
static packet_t m_localPacket;
static socket_t m_localSocket;
static sf_record_t m_localSfRecord;
static bool m_modemIsOff = false;
static bool m_modemAdmin = false;
static bool m_scInit = false;

// upload event type counters
static scUploadEventType_t m_nextUploadEvent = UPLOAD_EVENT_INITIAL_TRANSMITTION;
static scUploadEventType_t m_lastUploadEvent = UPLOAD_EVENT_INITIAL_TRANSMITTION;
static uint8_t m_uploadEventCount = 0;


static uint8_t packetRetries = SC_PACKET_MAX_RETRIES;
static uint8_t unixTimeRetries = SC_PACKET_MAX_RETRIES;
static uint8_t uploadRetries = SC_UPLOAD_MAX_RETRIES;
static uint8_t socketRetries = SC_SOCKET_MAX_RETRIES;
static uint8_t modemRetries = SC_MODEM_MAX_RETRIES;
static bool modemFailure = false;
static bool uploadSuccess = false;

#define SC_MAX_SFRECORDS_PER_PACKET			12

static sf_record_header_t packetSfHeaderBuffer[SC_MAX_SFRECORDS_PER_PACKET];

sc_state_t scController_getState(void) {
	return m_current_state;
}

bool scController_recordTransmitionEvent(void);

bool scController_processState (sc_state_t new_state) {

	bool success = true;
	sc_state_t localState = m_current_state;
	int32_t sec;

	uint8_t i;
	sf_record_t tempRecord;


	/* set current state */
	if (new_state && (new_state != localState)) {
		localState = new_state;
	}
	SEGGER_RTT_printf(0, "STATE: %u [%s]!\r\n",(unsigned int)localState,__FUNCTION__);

	switch (localState) {


		case SCC_STATE_FIRST_UPLOAD:
			/* SCC_STATE_FIRST_UPLOAD should handle the setting of first boot parameters and trigger
			 * an initial sensor read. */

			g_firstPacket = true;

			/* init local socket */
			strcpy(m_localSocket.addr, CORE_IP);
			m_localSocket.port = CORE_PORT;
			m_localSocket.open = false;
			m_localSocket.sd = -1;

			/* Init error flags in good state for first idle */
			modemFailure = false;
			uploadSuccess = true;
			m_scInit = false;

			// Set initial upload event cause
			scController_updateTransmitionEvent(UPLOAD_EVENT_INITIAL_TRANSMITTION);

			/* Block while the sensors are read. This will insure
			 * that a initial sensor measurement is sent to Core. */
			if (sensors_read(SC_WAIT_FOR_SENSORS_READ, SENSORS_READ)) {
				SEGGER_RTT_printf(0, "Sensors read for first packet transmission.\r\n");
			} else {
				SEGGER_RTT_printf(0, "WARN: No response from sensors_read, continuing.  [%s:%u]\r\n",__FUNCTION__,__LINE__);
			}

			m_current_state = SCC_STATE_IDLE;
			break;



		case SCC_STATE_UPLOAD_EVENT:
			/* SCC_STATE_UPLOAD_EVENT:
			 * -- Decide to wake modem.
			 * -- Initialize the first packet. etc */
			if (unixTime_getSeconds(&sec)) {
				SEGGER_RTT_printf(0, "Attempting local data upload to core at %u seconds (UNIXTIME)...\r\n",sec);
			} else {
				SEGGER_RTT_printf(0, "Attempting local data upload to core at %u sysTicks...\r\n", xTaskGetTickCount());
			}

			/* Prepare for the upload */
			uploadRetries = SC_UPLOAD_MAX_RETRIES;
			unixTimeRetries = SC_PACKET_MAX_RETRIES;
			uploadSuccess = false;
			socketRetries = SC_SOCKET_MAX_RETRIES;
			modemRetries = SC_MODEM_MAX_RETRIES;
			modemFailure = false;
			g_lastUploadFailure = false;

			/* Collect the event that triggered the upload and store
			 * it in S&F to be sent (hopefully) in the next transmission
			 */
			scController_recordTransmitionEvent();

			packet_init(&m_localPacket);

			if (xEventGroupGetBits(modem_stateEventGroup) & MODEM_STATE_ONLINE_IDLE_BIT) {
				m_current_state = SCC_STATE_READ_STATUS;
			} else if (xEventGroupGetBits(modem_stateEventGroup) & MODEM_STATE_ADMIN_ACTIVE_BIT) {
				SEGGER_RTT_printf(0, "WARN: Modem Admin context active, upload event canceled.\r\n");
				m_current_state = SCC_STATE_IDLE;
			} else {
				m_current_state = SCC_STATE_WAKE_MODEM;
			}

			break;



		case SCC_STATE_WAKE_MODEM:
			/* SCC_STATE_WAKE_MODEM: Send a command to the modem task to turn the modem on. */

			/* Confimm that the modem is in its off state. If not, we are not in a defined
			 * state and we should power cycle the modem for the next interval.  */
			if (xEventGroupGetBits(modem_stateEventGroup) & (MODEM_STATE_OFF_BIT)) {
				/* Send the boot command */
				if (modem_queuePhyCommand(MODEM_PHY_TURN_ON)) {
					SEGGER_RTT_printf(0,"Modem on command sent.\r\n");
					g_modemIsBooting = true;
					/* Now wait for the modem state event group to indicate that the modem is ready (ONLINE_IDLE) */
					if (pdFAIL == xEventGroupWaitBits(modem_stateEventGroup,MODEM_STATE_ONLINE_IDLE_BIT | MODEM_STATE_LOW_BATTERY_BIT,
							pdFALSE,pdFALSE,pdMS_TO_TICKS(SC_WAIT_FOR_MODEM_TO_BOOT))) {
						/* No point in retrying the modem on command, once is enough. */

						// Increment to the current modem state and save it to the ERROR_MODEM_ON_FAILED_AT_STATE count
						EventBits_t statebits = xEventGroupGetBits(modem_stateEventGroup), errState = 0;
						while (statebits >>= 1) ++errState;
						errorCount_setCount(ERROR_MODEM_ON_FAILED_AT_STATE,errState);
						modemRetries = 0;
					} else {
						if (xEventGroupGetBits(modem_stateEventGroup) & MODEM_STATE_LOW_BATTERY_BIT) {

							if (!modem_acknowledgeLowBattery()) {
								log_error("Failed to acknowledge the low battery warning!");
							}

							log_warnf("Battery to low to turn modem on! [%s:%u]\r\n",__FUNCTION__,__LINE__);
							modemRetries = 0;
						} else {
							/* Modem is now online, continue to read current status */
							modemFailure = false;
							g_modemIsBooting = false;
							/* Pause here to allow the connection to stabilize. TODO: Necessary? */
							vTaskDelay(pdMS_TO_TICKS(1000));
							m_current_state = SCC_STATE_READ_STATUS;
							break;
						}
					}
				} else {
					SEGGER_RTT_printf(0,"ERROR: Modem on command failed!  [%s:%u]\r\n",__FUNCTION__,__LINE__);
				}
			} else {
				/* The modem is in an unknown state, attempt to turn it off. */
				modemRetries = 0;
			}

			/* If the modem has experienced a failure condition, either from a failure to queue a phy command
			 * or from entering an invalid state, */
			if (modemRetries) {
				modemRetries--;
				SEGGER_RTT_printf(0, "WARN: Failed turn on modem! %u attempts remaining... [%s:%u]\r\n",socketRetries,__FUNCTION__,__LINE__);
			} else {
				SEGGER_RTT_printf(0, "ERROR: Failed to bring modem online! [%s:%u]\r\n",__FUNCTION__,__LINE__);
				modemRetries = SC_MODEM_MAX_RETRIES;
				modemFailure = true;
				g_modemIsBooting = false;
				m_current_state = SCC_STATE_SLEEP_MODEM;
				success = false;
			}
			break;

		case SCC_STATE_READ_STATUS:
			/* We are now online, take a status reading to get an accurate RSSI reading */
			if (sensors_read(SC_WAIT_FOR_STATUS_READ, STATUS_READ)) {
				SEGGER_RTT_printf(0, "Status read.\r\n");
			} else {
				SEGGER_RTT_printf(0, "WARN: No response from Status Read, continuing.  [%s:%u]\r\n",__FUNCTION__,__LINE__);
			}
			m_current_state = SCC_STATE_OPEN_SOCKET;
			break;


		case SCC_STATE_OPEN_SOCKET:
			/* SCC_STATE_OPEN_SOCKET: Open socket if necessary. */
			if (m_localSocket.open) {
				SEGGER_RTT_printf(0,"Socket already open.\r\n");
				m_current_state = SCC_STATE_COLLECT_SF_RECORDS;
			} else {
				success = modem_openSocket(&m_localSocket, SC_WAIT_FOR_SOCKET);
				if (success) {
					SEGGER_RTT_printf(0, "Service identifier %d opened as a TCP Socket.\r\n", m_localSocket.sd);
					m_current_state = SCC_STATE_COLLECT_SF_RECORDS;

					/* Pause here to allow the connection to stabilize. TODO: Necessary? */
					vTaskDelay(pdMS_TO_TICKS(1000));

					socketRetries = SC_SOCKET_MAX_RETRIES;
				} else if (socketRetries) {
					socketRetries--;
					SEGGER_RTT_printf(0, "WARN: Failed to open socket! %u attempts remaining... [%s:%u]\r\n",socketRetries,__FUNCTION__,__LINE__);
				} else {
					SEGGER_RTT_printf(0, "ERROR: Failed to open socket! [%s:%u]\r\n",__FUNCTION__,__LINE__);
					socketRetries = SC_SOCKET_MAX_RETRIES;
					errorCount_incrementCount(ERROR_FAILED_TO_OPEN_UDP_SOCKET);
					m_current_state = SCC_STATE_CLOSE_SOCKET;
				}
			}
			break;



		case SCC_STATE_COLLECT_SF_RECORDS:
			/* Retrieve a record from S&F and insert it into the packet until full */
			/* TODO: handle local record that did not fit in previous packet */
			success = stoFwd_receiveRecord(&m_localSfRecord, pdMS_TO_TICKS(100));
			/* is it a valid record? */
			if (m_localSfRecord.header.type != SF_NULL_RECORD) {
				if (!packet_appendSfRecord(&m_localPacket,&m_localSfRecord)) {
					/* if the local record was not append to the current packet successfully,
					 * we place it back into the FIFO for now. */
					m_localPacket.full = true;
					stoFwd_sendRecord(&m_localSfRecord, pdMS_TO_TICKS(1000));
					m_current_state = SCC_STATE_SEND_PACKET;
				} else {
					/* record was added to the packet */
					memcpy(&packetSfHeaderBuffer[m_localPacket.sf_records-1],&m_localSfRecord.header,sizeof(sf_record_header_t));
				}
				/* Check that the maximum number of headers has not been reached... */
				if (m_localPacket.sf_records >= SC_MAX_SFRECORDS_PER_PACKET) {
					m_localPacket.full = true;
					m_current_state = SCC_STATE_SEND_PACKET;
				}
			} else {
				log_info("Finished sending available S&F items.");
				if (m_localPacket.sf_records > 0 || !unixTime_isValid() || g_updateUnixTime) {
					/* If the packet is not quite full or its the first packet ever,
					 * we mark it full and transmission to send the packet. */
					m_localPacket.full = true;
					m_current_state = SCC_STATE_SEND_PACKET;
				} else if (!sensors_hasValidDataUploaded()) {
					/* If no valid data (data taken with unix time) has been uploaded,
					 * but unix time has been received by core, allow the sensors task
					 * to take a readings and place them in S&F for uploading. */
					m_localPacket.full = true;
					m_current_state = SCC_STATE_SEND_PACKET;
				} else {
					log_info("Upload Event Successful.");
					uploadSuccess = true;
					/* Nothing left in the S&F buffer to send at the moment. */
					m_current_state = SCC_STATE_CLOSE_SOCKET;
				}
			}

			/* If next state is send packet, compute the checksum now,
			 * this way the SCC_STATE_SEND_PACKET can repeat X retries without issue */
			if (m_current_state == SCC_STATE_SEND_PACKET) {
				m_localPacket.dataLength++;
				packet_updateCRC(m_localPacket.data,m_localPacket.dataLength);

				SEGGER_RTT_printf(0, "Sending Packet: ");
				packet_print(m_localPacket.data,m_localPacket.dataLength);
				/* Reset tx retries before entering tx state */
				packetRetries = SC_PACKET_MAX_RETRIES;
			}
			break;



		case SCC_STATE_SEND_PACKET:
			/* Handle the constructed packet and attempt to send it to core. */
			success = modem_sendPacket(m_localSocket.sd,&m_localPacket,pdMS_TO_TICKS(3000));
			if (success) {
				SEGGER_RTT_printf(0, "Packet sent successfully.\r\n");
				/* Transition to receive state. */
				m_current_state = SCC_STATE_RECEIVE_PACKET;
				/* Reset rx retries before entering rx state */
				packetRetries = SC_PACKET_MAX_RETRIES;
			} else 	if (packetRetries) {
				packetRetries--;
				SEGGER_RTT_printf(0, "WARN: Failed to send packet! %u attempts remaining... [%s:%u]\r\n",packetRetries,__FUNCTION__,__LINE__);
			} else {
				SEGGER_RTT_printf(0, "ERROR: Packet failed to send! [%s:%u]\r\n",__FUNCTION__,__LINE__);
				errorCount_incrementCount(ERROR_PACKET_SEND_RETRIES_EXHAUSTED);
				m_current_state = SCC_STATE_CLOSE_SOCKET;
			}
			break;



		case SCC_STATE_RECEIVE_PACKET:
			/* Last packet was sent successfully, we will now attempt to receive the
			 * expected response packet from core. */
			/* TODO: check sequence number to verify packet received, will require a separate rx packet container */
			success = modem_receivePacket(m_localSocket.sd,&m_localPacket,pdMS_TO_TICKS(10000));
			if (success) {
				SEGGER_RTT_printf(0, "Packet received: ");
				packet_print(m_localPacket.data,m_localPacket.dataLength);
			}

			/* Check that the packet is valid, ie not an error packet and has a valid CRC */
			if (success &&
					(scController_verifyPacket(m_localPacket.data,m_localPacket.dataLength) == UPLOAD_RESULT_SUCCESS)) {

				/* set the g_firstPacket to false, just in case */
				g_firstPacket = false;

				// if we receive a packet, reset our upload retry count since we
				// are actually sending data to Core.  this way, if Core decides
				// to close the connection on us after processing a packet
				// (which it appears to be doing as of 9/13/17) then we'll open
				// a new connection and send another packet.
				uploadRetries = SC_UPLOAD_MAX_RETRIES;

				m_current_state = SCC_STATE_PROCESS_RECEIVED;
			} else {
				if (packetRetries) {
					packetRetries--;
					SEGGER_RTT_printf(0, "WARN: Failed to receive a packet! %u attempts remaining... [%s:%u]\r\n",packetRetries,__FUNCTION__,__LINE__);
				} else {
					errorCount_incrementCount(ERROR_NO_RESPONSE_FROM_SERVER);
					SEGGER_RTT_printf(0, "ERROR: Failed to receive a packet! [%s:%u]\r\n",__FUNCTION__,__LINE__);
					m_current_state = SCC_STATE_CLOSE_SOCKET;
				}
			}
			break;



		case SCC_STATE_PROCESS_RECEIVED:
			/* Handle received packet. */
			scController_processReceivedPacket(m_localPacket.data,&m_localPacket.dataLength);

			/* Initialize packet for next transmission*/
			packet_init(&m_localPacket);

			/* if we have yet to get the unix time from the server, breifly delay the
			 * next packet transmission allow server to process time request. */
			if (!unixTime_isValid() && unixTimeRetries) {
				vTaskDelay(pdMS_TO_TICKS(3000));
			}

			m_current_state = SCC_STATE_COLLECT_SF_RECORDS;
			break;



		case SCC_STATE_CLOSE_SOCKET:
			/* Either an error occurred while attempting to open a socket
			 * or send/receive a packet, or the device was done sending S&F data.
			 * We check uploadSuccess to determine the if the upload was successful,
			 * if not and uploadRetries > 0, try again.  */

			success = modem_closeSocket(&m_localSocket, SC_WAIT_FOR_SOCKET);

			if (!success) {
				if (socketRetries) { // <<-- STATE REPEAT
					socketRetries--;
					SEGGER_RTT_printf(0, "WARN: Failed to close socket! %u attempts remaining... [%s:%u]\r\n",socketRetries,__FUNCTION__,__LINE__);
					break;
				} else {
					SEGGER_RTT_printf(0, "ERROR: Failed to close socket! [%s:%u]\r\n",__FUNCTION__,__LINE__);
				}
			}
			if (!uploadSuccess) {

				/* On failure, for now push the last packet back into S&F. */
				for (i = 0; i < m_localPacket.sf_records; i++) {
					memcpy(&tempRecord.header,&packetSfHeaderBuffer[i],sizeof(sf_record_header_t));
					memcpy(&tempRecord.data,(m_localPacket.data+tempRecord.header.p_pkt),tempRecord.header.dataLength);
					if (!stoFwd_sendRecord(&tempRecord, pdMS_TO_TICKS(1000))) {
						SEGGER_RTT_printf(0, "ERROR: Failed to push S&F record back into S&F Stack queue! [%s:%u]\r\n",__FUNCTION__,__LINE__);
					}
				}
				/* As we have just saved the packet data, initialize it to prevent duplicate data */
				packet_init(&m_localPacket);

				/* We failed to handle the previous packet, check upload retries */
				if (uploadRetries) { // <<-- STATE EXIT
					uploadRetries--;
					SEGGER_RTT_printf(0,"WARN Upload of packet failed. %u attempts remaining... [%s:%u]\r\n",uploadRetries,__FUNCTION__,__LINE__);
					socketRetries = SC_SOCKET_MAX_RETRIES;
					m_current_state = SCC_STATE_OPEN_SOCKET;
					break;
				} else {
					SEGGER_RTT_printf(0, "ERROR: Upload attempts exhausted! [%s:%u]\r\n",__FUNCTION__,__LINE__);
				}
			}
			/* Uploads finished (with/without failure) and socket is 'closed'.
			 * Determine if the */
			if (m_uploadInterval_min >= MODEM_SLEEP_MIN_UPLOAD_INTERVAL) {
				/* Interval is long enough to make it worth shutting down the modem. */
				SEGGER_RTT_printf(0,"Upload interval > %u mins, turning it off...\r\n", MODEM_SLEEP_MIN_UPLOAD_INTERVAL);
				m_current_state = SCC_STATE_SLEEP_MODEM;
				modemRetries = SC_MODEM_MAX_RETRIES;
			} else {
				SEGGER_RTT_printf(0,"Upload interval =< %u mins, leaving it on.\r\n", MODEM_SLEEP_MIN_UPLOAD_INTERVAL);
				m_current_state = SCC_STATE_IDLE;
			}

			break;



		case SCC_STATE_SLEEP_MODEM:
			/* Send a command to the modem task to turn the modem off.
			 * first we get the current state of the modem. */
			m_modemIsOff = false;
			m_modemAdmin = false;

			vTaskDelay(200);

			if (!(xEventGroupGetBits(modem_stateEventGroup) & (MODEM_STATE_OFF_BIT))) {
				/* First verify that the modem's admin PDP is not active */
				if (xEventGroupGetBits(modem_stateEventGroup) & (MODEM_STATE_ADMIN_ACTIVE_BIT)) {
					/* If the reboot timer has not been set, set it. Either way
					 * record this error and return to the idle state. */
					m_modemAdmin = true;
				} else {
					if (modem_queuePhyCommand(MODEM_PHY_TURN_OFF)) {
						SEGGER_RTT_printf(0,"Modem off command sent.\r\n");
						/* The off command was sent, */
						if (pdFAIL == xEventGroupWaitBits(modem_stateEventGroup,MODEM_STATE_OFF_BIT,
								pdFALSE,pdFALSE,pdMS_TO_TICKS(SC_WAIT_FOR_MODEM_TO_SLEEP))) {
							SEGGER_RTT_printf(0,"ERROR: Timed out waiting for modem to turn off!  [%s:%u]\r\n",__FUNCTION__,__LINE__);
							modemRetries = 0;
						} else {
							m_modemIsOff = true;
						}
					} else {
						SEGGER_RTT_printf(0,"ERROR: Failed to send modem off command!  [%s:%u]\r\n",__FUNCTION__,__LINE__);
					}
				}
			} else {
				m_modemIsOff = true;
			}

			if (m_modemIsOff) {
				modemFailure = false;
				SEGGER_RTT_printf(0,"Modem is now off.\r\n");
				m_current_state = SCC_STATE_IDLE;
			} else if (m_modemAdmin) {
				modemFailure = false;
				SEGGER_RTT_printf(0,"Modem admin context active, leaving modem on.\r\n");
				/* TODO: set failure condition timer to reset modem if it expires */
				m_current_state = SCC_STATE_IDLE;
			} else if (modemRetries) {
				modemRetries--;
				SEGGER_RTT_printf(0, "WARN: Failed to turn off modem! %u attempts remaining... [%s:%u]\r\n",modemRetries,__FUNCTION__,__LINE__);
			} else {
				errorCount_incrementCount(ERROR_FAILED_TO_TURNOFF_MODEM);
				SEGGER_RTT_printf(0, "ERROR: Failed to turn off modem! [%s:%u]\r\n",__FUNCTION__,__LINE__);
				m_current_state = SCC_STATE_IDLE;
				modemFailure = true;
			}
			break;



		case SCC_STATE_IDLE:
			/* In the SCC_STATE_IDLE state, we wait for an event to occur. */

			/* TODO: Handle modem errors. if modemRetries == 0, we should
			 * Initiate a emergency reset or possibly reset the entire device to
			 * a known state. */

			sc_activeEventBit = 0;
			g_uploadInprogress = false;

			/* If we've experienced a failure, reset the event mask and clear the local bits.
			 * this is combat a sleep event failure. */
			if (modemFailure) {
				sc_localEventBits = 0;
				sc_eventBitsMask = SC_EVENTGROUP_SLEEP_EVENT_BIT | SC_EVENTGROUP_UPLOAD_EVENT_BIT | SC_EVENTGROUP_OTA_EVENT_BIT;
			}

			g_lastUploadFailure = modemFailure | (!uploadSuccess);

			/* Things to do if at lease a single upload has been attempted */
			if (!m_scInit) {
				m_scInit = true;
				errors_collectStartupErrors();
			} else {
				// reset upload event cause to upload interval
				scController_updateTransmitionEvent(UPLOAD_EVENT_UPLOAD_INTEVAL_EXPIRED);

				/* If the recent upload was a success, do stuff */
				if (uploadSuccess) {
					/* if this upload was a success, notify interested parties  */
					notifySubscribers(uploadFinishedNotifyHandle,NULL);
					/* stop the recovery timer */
					xTimerStop(m_genericTimer, pdMS_TO_TICKS(5000));
					/* reset the current recovery interval back to the minimum */
					m_recoveryInterval_min = SC_MIN_RECOVERY_INTERVAL_MINS;
				}

				/* updated error counters */
				errors_collectUploadErrors();

				/* Check to see if a recovery interval will be triggered. */
				scController_checkRecovery(uploadSuccess);
			}

			/* Wait for event to be placed into the tasks event generic queue */
			if (!sc_localEventBits) {
				sc_localEventBits = xEventGroupWaitBits(sc_eventGroup, sc_eventBitsMask, pdFALSE, pdFALSE, portMAX_DELAY);
			}
			/* Handle incoming sources (Queues) */
			if (sc_localEventBits & SC_EVENTGROUP_UPLOAD_EVENT_BIT) {
				sc_activeEventBit = SC_EVENTGROUP_UPLOAD_EVENT_BIT;
				sc_localEventBits &= ~SC_EVENTGROUP_UPLOAD_EVENT_BIT;
				xEventGroupClearBits(sc_eventGroup, SC_EVENTGROUP_UPLOAD_EVENT_BIT);
			} else if (sc_localEventBits & SC_EVENTGROUP_OTA_EVENT_BIT ) {
				sc_activeEventBit = SC_EVENTGROUP_OTA_EVENT_BIT;
				sc_localEventBits &= ~SC_EVENTGROUP_OTA_EVENT_BIT;
				xEventGroupClearBits(sc_eventGroup, SC_EVENTGROUP_OTA_EVENT_BIT);
			} else if (sc_localEventBits & SC_EVENTGROUP_SLEEP_EVENT_BIT) {
				sc_activeEventBit = SC_EVENTGROUP_SLEEP_EVENT_BIT;
				/* mask out all other bits to prevent other tasks from interrupting the sleep transition */
				sc_eventBitsMask = SC_EVENTGROUP_SLEEP_EVENT_BIT;
				modemRetries = SC_MODEM_MAX_RETRIES;
				/* Don't clear the local bit, thus allowing this process to continue through multiple events */
				xEventGroupClearBits(sc_eventGroup, SC_EVENTGROUP_SLEEP_EVENT_BIT);
			} else {
				/* Unknown event bit triggered */
				SEGGER_RTT_printf(0, "ERROR: Unknown event bits [%08x] in scLocalEventBits [%s]\r\n", sc_localEventBits, __FUNCTION__);
				/* bits should be cleared to avoid re-triggering this event */
				break;
			}

			switch (sc_activeEventBit) {
				case SC_EVENTGROUP_UPLOAD_EVENT_BIT:
					/* Upload timer expired, we now attempt to upload local data until
					 * MAX_PACKETS_PER_UPLOAD has been met. */
					/* Stop the generic timer incase the user pressed the button, preempting an early upload. */
					xTimerStop(m_genericTimer,pdMS_TO_TICKS(100));

					m_current_state = SCC_STATE_UPLOAD_EVENT;
					g_uploadInprogress = true;
					break;

				case SC_EVENTGROUP_OTA_EVENT_BIT:
					/* Either a local ota was triggered or a packet was received via the
					 * last transmission */
					scController_deviceOta(m_ota_maj, m_ota_min);
					/* will not return */
					break;


				case SC_EVENTGROUP_SLEEP_EVENT_BIT:
					if (xEventGroupGetBits(modem_stateEventGroup) & MODEM_STATE_OFF_BIT) {
						/* Modem is off. We now can Suspend tasks and prepare for sleep. */
						scController_enterSleep();
					} else {
						m_current_state = SCC_STATE_SLEEP_MODEM;
					}
					break;
			}
			break;



		case SCC_STATE_MODEM_ERESET:
			break;



		default:
			success = false;
			SEGGER_RTT_printf(0, "ERROR: Modem in unknown state! Setting state to SCC_STATE_INIT.[%s]\r\n",__FUNCTION__);
			break;

	}

	return success;
}


uint32_t scController_getSecondsToNextUpload(void) {
	uint32_t nextUploadSeconds = 0;

	if (xTimerIsTimerActive(m_genericTimer)) {
		nextUploadSeconds = ((xTimerGetExpiryTime( m_genericTimer ) - xTaskGetTickCount())) / pdMS_TO_TICKS(1000);
	} else {
		nextUploadSeconds = ((xTimerGetExpiryTime( m_uploadTimer ) - xTaskGetTickCount())) / pdMS_TO_TICKS(1000);
	}

	return nextUploadSeconds;
}

/*
 * methods for accessing server intervals (upload and recovery)
 * */

bool scController_getRecoveryIntervalSecounds(uint32_t * interval) {
	if ((interval == NULL) || (pdFALSE == xTimerIsTimerActive(m_genericTimer))) {
		return false;
	}
	*interval = (xTimerGetPeriod( m_genericTimer ) / pdMS_TO_TICKS(1000));
	return true;
}

bool scController_getUploadIntervalSecounds(uint32_t * interval) {
	if ((interval == NULL) || (pdFALSE == xTimerIsTimerActive(m_uploadTimer))) {
		return false;
	}
	*interval = (xTimerGetPeriod( m_uploadTimer ) / pdMS_TO_TICKS(1000));
	return true;
}

bool scController_scheduleRecoveryIntervalMins(uint32_t mins) {
	if (mins < SC_MIN_RECOVERY_INTERVAL_MINS) {
		log_error("Requested interval less then minimum, skipping!");
		return false;
	}
	m_recoveryInterval_min = mins;
	if (xTimerChangePeriod(m_genericTimer, (pdMS_TO_TICKS(MS_PER_MINUTE)) * m_recoveryInterval_min, portMAX_DELAY)) {
		xTimerStart(m_genericTimer, portMAX_DELAY);
		scController_updateTransmitionEvent(UPLOAD_EVENT_RECOVERY_INTERVAL_EXPIRED);
		log_infof("Recovery interval started: %u min", m_recoveryInterval_min);
		return true;
	} else {
		log_error("Failed to start Recovery Upload Timer!");
	}
	return false;
}

bool scController_checkRecovery(bool uploadSuccess) {
	/* If we've failed to upload during this period or Unix Time is invalid
	 * and the upload interval is > 15mins, set a short interval timer to trigger
	 * an early upload. That way the user won't have to wait the full interval
	 * in cases where the interval is large. */
	if ((!uploadSuccess || !unixTime_isValid()) && (m_uploadInterval_min > SC_MIN_RECOVERY_INTERVAL_MINS)) {
	/* If the next recovery interval is larger than the current upload interval,
	 * we don't set the recovery interval, the normal upload interval will serve the same purpose. */
		if (((m_recoveryInterval_min) < m_uploadInterval_min)) {
			scController_scheduleRecoveryIntervalMins(m_recoveryInterval_min);
			/* We now double the recovery interval to exponentially increase the period between
			 * retries to combat network outages. The check previously will prevent from
			 * rolling over the max value. */
			m_recoveryInterval_min = (m_recoveryInterval_min << 1);
		} else {
			log_info("Recovery interval not started as it will exceed current upload interval");
		}
	}
	return false;
}

void scController_updateTransmitionEvent(scUploadEventType_t eventType) {
	m_nextUploadEvent = eventType;
}

void scController_startUpload(scUploadEventType_t eventType) {
	if (sc_eventGroup == NULL) {
		APP_ERROR_CHECK(NRF_ERROR_INVALID_STATE);
	}

	if (xEventGroupGetBits(sc_eventGroup) & SC_EVENTGROUP_UPLOAD_EVENT_BIT) {
		SEGGER_RTT_printf(0, "Upload event already pending.\r\n");
	} else {
		// save upload trigger type
		scController_updateTransmitionEvent(eventType);

		SEGGER_RTT_printf(0, "Signaling the scController to begin upload.\r\n");
		xEventGroupSetBits(sc_eventGroup, SC_EVENTGROUP_UPLOAD_EVENT_BIT);
	}
}


bool scController_recordTransmitionEvent(void) {

	sf_record_t uploadEventRec;
	uint16_t ptr = 0;
	/* Initialize record header */
	uploadEventRec.header.type = SF_STATUS_RECORD;
	uploadEventRec.header.utc_s = 0;
	uploadEventRec.header.timeStamp = get_time_100ms();


	if (m_nextUploadEvent == m_lastUploadEvent) {
		if (m_uploadEventCount < 255) {
			m_uploadEventCount++;
		}
	} else {
		m_uploadEventCount = 1;
	}
	m_lastUploadEvent = m_nextUploadEvent;

	uploadEventRec.data[ptr++] = ERROR_REPORT_MESSAGE_ID;
	uploadEventRec.data[ptr++] = ERROR_REPORT_UPLOAD_TRIGGER_EVENT_SUBMESSAGE_ID;
	uploadEventRec.data[ptr++] = m_nextUploadEvent;
	uploadEventRec.data[ptr++] = m_uploadEventCount;
	uploadEventRec.data[ptr++] = 0; // unused

	uploadEventRec.header.messages = 1;
	uploadEventRec.header.dataLength = ptr;

	if (stoFwd_sendRecord(&uploadEventRec, portMAX_DELAY) == false) {
		errorCount_incrementCount(ERROR_STOFWD_PUSH_ERROR_RESULTING_IN_LOST_ITEM);
		log_error("Failed to queue upload upload trigger message!");
		return false;
	}
	return true;

}


void scController_startOTA(uint8_t fw_maj, uint8_t fw_min) {
	if (sc_eventGroup == NULL) {
		APP_ERROR_CHECK(NRF_ERROR_INVALID_STATE);
	}

	m_ota_maj = fw_maj;
	m_ota_min = fw_min;
	xEventGroupSetBits(sc_eventGroup, SC_EVENTGROUP_OTA_EVENT_BIT);
}

void scController_startSleep() {
	if (sc_eventGroup == NULL) {
		APP_ERROR_CHECK(NRF_ERROR_INVALID_STATE);
	}

#if defined(CORE_CONTROLLER) && (CORE_CONTROLLER == 1)
	if (xEventGroupGetBits(sc_eventGroup) & SC_EVENTGROUP_SLEEP_EVENT_BIT) {
		SEGGER_RTT_printf(0,"Sleep signal already sent.\r\n");
	} else {
		SEGGER_RTT_printf(0,"Signaling the scController to sleep the device.\r\n");
		xEventGroupSetBits(sc_eventGroup,SC_EVENTGROUP_SLEEP_EVENT_BIT);
	}
#else
	scController_enterSleep();
#endif
}

/**@brief Function for putting the chip into sleep mode.
	 *
	 * @note This function will not return.
	 */
void scController_enterSleep(void) {

	uint32_t err_code; // = bsp_indication_set(BSP_INDICATE_IDLE);
	//APP_ERROR_CHECK(err_code);

	/* TODO: Put S&F into FLASH */

	vTaskDelay(500);

	/* Disable UI timer */
	ui_deinit();

	LEDS_OFF(LEDS_MASK);
	LEDS_ON(BSP_LED_0_MASK);

	/* Slowly dim red led to indicate power down. */
	TickType_t x = 0;
	for (x = 0; x < 255; x+=2) {
		LEDS_ON(BSP_LED_0_MASK);
		vTaskDelay(pdMS_TO_TICKS(0x0000000F & (0xF-(x>>4))));
		LEDS_OFF(LEDS_MASK);
		vTaskDelay(pdMS_TO_TICKS(0x0000000F & ((x>>4))));
	}
	vTaskDelay(pdMS_TO_TICKS(4000));
#ifndef BOARD_LORA_MODULE_REV1P0
	/* TODO: Place all GPIOs and peripherals into lowest power mode */
	nrf_gpio_pin_set(CSN_ENN_PIN); // Chip select enable line connected to a 10k pullup
#endif
	// Prepare wakeup buttons.
	err_code = bsp_btn_ble_sleep_mode_prepare();
	APP_ERROR_CHECK(err_code);

	// Go to system-off mode (this function will not return; wakeup will cause a reset).
	err_code = sd_power_system_off();
	APP_ERROR_CHECK(err_code);

}

uploadResult_t scController_verifyPacket(uint8_t *p_packet, const uint16_t packetLen) {
	uploadResult_t txResult;
	if ((packetLen <= 2) && (p_packet[0] == 0x6E)) {
		/* If the server sent us an error packet... */
		if (packetLen == 1) {
			/* If the error packet does not contain a payload
			 * identifying the type of error and we just performed our
			 * last retry, it is just a general NACK from the server. */
			txResult = UPLOAD_RESULT_SERVER_GENERAL_NACK;
			log_error("Server sent NACK!");
		} else if (p_packet[1] == 0x05) {
			/* If the server rejected the packet's CRC on our
			 * last retry, we return an error code.  Otherwise, if
			 * we still have retries left to use, we do so. */
			txResult = UPLOAD_RESULT_SERVER_REJECTED_CRC;
			log_error("Server rejected CRC!");
		} else if (p_packet[1] == 0x0A) {
			/* If the server did not recognize the device ID, we do not
			 * bother retrying to send the packet because it is so
			 * unlikely that both the ID was wrong and the CRC was
			 * wrong. */
			txResult = UPLOAD_RESULT_SERVER_REJECTED_ID_UNKNOWN;
			log_error("Server rejected device ID, device is likely not registered!")
		} else if (p_packet[1] == 0x0B) {
			/* If the server recognized the device ID but the account
			 * is not in good standing, we do not bother resending the
			 * packet because it is so unlikely that both the ID and
			 * CRC were wrong simultaneously. */
			txResult = UPLOAD_RESULT_SERVER_REJECTED_ID_BAD_ACCOUNT;
			log_error("Server rejected device, bad account!");
		} else if (p_packet[1] == 0x30) {
			/* If the server was busy, we return without attempting to
			 * retry the transmission.  Immediately retrying the
			 * transmission will not help alleviate server load. */
			txResult = UPLOAD_RESULT_SERVER_BUSY;
			log_error("Server busy!");
		} else {
			/* If we received an unknown type of error and this is our
			 * last retry, we return.  Otherwise, we re-send the packet.
			 * It is unlikely, but possible, that the error type code was
			 * corrupted. */
			txResult = UPLOAD_RESULT_SERVER_UNKNOWN_ERROR;
			log_error("Server sent unknown ERROR!");
		}
	} else {
		if (packet_verifyCRC(p_packet,packetLen)) {
			txResult = UPLOAD_RESULT_SUCCESS;
		} else {
			txResult = UPLOAD_RESULT_INVALID_CRC_FROM_SERVER;
			log_error("Server packet CRC failure!");
		}
	}
	return txResult;
}


bool scController_processReceivedPacket(const uint8_t *p_packet, const uint16_t *p_packetLen) {
	uint16_t i, j;
	int32_t newUnixTime;
	uint8_t pkt_hardware_family = 0;
	uint8_t pkt_hardware_id = 0;
	uint8_t pkt_maj = 0;
	uint8_t pkt_min = 0;
	uint16_t pkt_new_interval = 0;
	bool unknownMsgType = false;

	/* i starts counting at 1 because we skip the message count field. */
    for (i = 1; i < (*p_packetLen - 1); i++) {
        switch (p_packet[i]) {
            case MEASUREMENT_INTERVAL_MINS_MESSAGE_ID:
				pkt_new_interval += p_packet[++i];
				scController_setUploadInterval((uint32_t) pkt_new_interval);
                break;
            case MEASUREMENT_INTERVAL_HRS_MESSAGE_ID:
				pkt_new_interval += ((uint16_t)p_packet[++i])*60;
				scController_setUploadInterval((uint32_t) pkt_new_interval);
                break;
            case FORCE_DNS_LOOKUP_MESSAGE_ID:
                //g_dnsLookupRqst = true;
                break;
            case SET_STOFWD_STATE_MESSAGE_ID:
				// Always S&F
				i++;
                break;
            case ALARM_STATE_MESSAGE_ID:
                g_alarmState = p_packet[++i];
                break;
            case ALARM_PORTS_MESSAGE_ID:
            	/* The ALARM_PORTS message contains a variable length payload.
            	 * The first payload byte is always present and specifies how
            	 * many ports are in an alarm state. */
            	g_alarmCount = p_packet[++i];

            	/* If at least one port is alarming, set the global alarmState
            	 * flag so that the LEDs (and LCD, if present) indicate that
            	 * the device is in an alarm state. */
            	if (g_alarmCount > 0) {
            		g_alarmState = 0x01;
            		SEGGER_RTT_printf(0, "Device is in Alarm!!!\r\n");
            	} else {
            		SEGGER_RTT_printf(0, "Device is not in Alarm.\r\n");
            		g_alarmState = 0x00;
            	}

            	/* Save the port numbers that are currently alarming to the
            	 * alarmPorts array.  If the device has an LCD, we'll be
            	 * able to use it to indicate which sensors are generating
            	 * the alarms. */
            	for (j=0; j<MAX_ALARM_PORTS; j++) {
            		if (j == g_alarmCount) {
            			break;
            		}
            		g_alarmPorts[j] = p_packet[++i];
            	}
            	break;
            case RUN_GATEWAY_BOOTLOADER_MESSAGE_ID:
                i+=2;
                break;
            case DOWNLOAD_FIRMWARE_MESSAGE_ID:
            	pkt_hardware_family = p_packet[++i];
            	pkt_hardware_id = p_packet[++i];
            	pkt_maj = p_packet[++i];
            	pkt_min = p_packet[++i];

            	SEGGER_RTT_printf(0,"OTA Requested by Server for HF%u HID%u to v%u.%u.\r\n",
            			pkt_hardware_family,pkt_hardware_id, pkt_maj, pkt_min);

            	if (pkt_hardware_family == HARDWARE_FAMILY) {
            		if (pkt_hardware_id == HARDWARE_VERSION) {
            			if ((pkt_maj != MAJOR_VERSION) || (pkt_min != MINOR_VERSION)) {
            				m_ota_maj = (uint8_t)pkt_maj;
            				m_ota_min = (uint8_t)pkt_min;
            				/* set the ota event */
            				xEventGroupSetBits(sc_eventGroup,SC_EVENTGROUP_OTA_EVENT_BIT);
							SEGGER_RTT_printf(0,"Updating to requested version.\r\n");
            			} else {
            				SEGGER_RTT_printf(0,"Version is current. Skipping request.\r\n");
            			}
            		} else {
        				SEGGER_RTT_printf(0,"Wrong hardware version. Skipping request.\r\n");
            		}
            	} else {
    				SEGGER_RTT_printf(0,"Wrong hardware family. Skipping request.\r\n");
            	}
            	break;
			case SEQUENCE_NUMBER_MESSAGE_ID: // Sequence number
				i++;
				break;
			case UNIX_TIME_RESPONSE_MESSAGE_ID:
				newUnixTime  = (int32_t)p_packet[++i] << 24;
				newUnixTime |= (int32_t)p_packet[++i] << 16;
				newUnixTime |= (int32_t)p_packet[++i] << 8;
				newUnixTime |= (int32_t)p_packet[++i];
				if ((unixTimeMutex) && (xSemaphoreTake(unixTimeMutex,pdMS_TO_TICKS(100)) == pdTRUE)) {
					unixTime_setSeconds(newUnixTime);
					xSemaphoreGive(unixTimeMutex);
					// clear the unixtime update flag
					g_updateUnixTime = false;
				} else {
            		SEGGER_RTT_printf(0, "ERROR: Couldn't take unixTime mutex, update failed!\r\n");
				}
				break;
			case SET_SENSORSN_STATE_MESSAGE_ID:
				i++;
				break;
			case ENABLE_VBV_MESSAGE_ID:
				i+=3;
				break;
			case ZPOINT_DIAGNOSTIC_MESSAGE_ID:
				i += 5;
				break;
            case DISPLAY_MODE_MESSAGE_ID:
				i++;
                break;
            case SUBMEASUREMENT_INTERVAL_MESSAGE_ID:
            	i++;
            	break;

            default:
            	SEGGER_RTT_printf(0, "Unknown message type (ID: 0x%02x) found by packet_processReceived() at index %d!\r\n", p_packet[i], i);
				/* If we encounter an unknown message ID, we must stop
				 * processing the packet because we do not know the payload
				 * length of the unknown message type. */
				unknownMsgType = true;
        }

        /* Stop iterating over the messages in the packet if we do not
         * recognize a particular message. */
        if (unknownMsgType) {
        	break;
        }
    }

    /* Always return true */
    return true;
}


/**@brief   Function for handling callbacks from pstorage module.
 *
 * @details Handles pstorage results for clear and storage operation. For detailed description of
 *          the parameters provided with the callback, please refer to \ref pstorage_ntf_cb_t.
 */
static bool btl_settings_updating = false;

static void sc_pstorage_callback_handler(pstorage_handle_t * p_handle,
                                      uint8_t             op_code,
                                      uint32_t            result,
                                      uint8_t           * p_data,
                                      uint32_t            data_len)
{
    // If we are in BOOTLOADER_SETTINGS_SAVING state and we receive an PSTORAGE_STORE_OP_CODE
    // response then settings has been saved and update has completed.
    if ((btl_settings_updating) && ((op_code == PSTORAGE_STORE_OP_CODE) || (op_code == PSTORAGE_CLEAR_OP_CODE)))
    {
    	btl_settings_updating = false;
    	 SEGGER_RTT_printf(0,"Done. [%u]\r\n",result);
    }

    APP_ERROR_CHECK(result);
}

/**@brief Function for disabling all interrupts before jumping from bootloader to application.
 */

#define IRQ_ENABLED            0x01                                     /**< Field that identifies if an interrupt is enabled. */
#define MAX_NUMBER_INTERRUPTS  32                                       /**< Maximum number of interrupts available. */

//static void interrupts_disable(void)
//{
//    uint32_t interrupt_setting_mask;
//    uint32_t irq;
//
//    // Fetch the current interrupt settings.
//    interrupt_setting_mask = NVIC->ISER[0];
//
//    // Loop from interrupt 0 for disabling of all interrupts.
//    for (irq = 0; irq < MAX_NUMBER_INTERRUPTS; irq++)
//    {
//        if (interrupt_setting_mask & (IRQ_ENABLED << irq))
//        {
//            // The interrupt was enabled, hence disable it.
//            NVIC_DisableIRQ((IRQn_Type)irq);
//        }
//    }
//}

void scController_deviceOta(uint8_t fw_maj, uint8_t fw_min) {
	    uint32_t                err_code;
	    uint16_t 				major,minor;
	    pstorage_module_param_t storage_params = {.cb = sc_pstorage_callback_handler};
	    pstorage_handle_t bootsettings_handle;
	    bootloader_settings_t btl_settings;
	    const bootloader_settings_t * p_btl_settings;

		err_code = pstorage_init();
	    APP_ERROR_CHECK(err_code);

	    /* read the current bootloader settings */
	    bootloader_util_settings_get(&p_btl_settings);

	    bootsettings_handle.block_id = (uint32_t)p_btl_settings;
	    err_code = pstorage_raw_register(&storage_params, &bootsettings_handle);
	    APP_ERROR_CHECK(err_code);

	    memcpy(&btl_settings,p_btl_settings,sizeof(bootloader_settings_t));

	    major = fw_maj;
	    minor = fw_min;

	    /* update requested version */
	    btl_settings.requested_version = (major << 8) | (minor & 0x00FF);

	    err_code = pstorage_raw_clear(&bootsettings_handle, sizeof(bootloader_settings_t));
	    APP_ERROR_CHECK(err_code);

	    btl_settings_updating = true;

	    while (btl_settings_updating) {
	    	 vTaskDelay(100);
	    }

	    /* enqueue the store command to save the data */
	    err_code = pstorage_raw_store(&bootsettings_handle,
	                              (uint8_t *)&btl_settings,
	                              sizeof(bootloader_settings_t),
	                              0);
	    APP_ERROR_CHECK(err_code);

	    btl_settings_updating = true;

	    while (btl_settings_updating) {
	    	 vTaskDelay(100);
	    }

	    SEGGER_RTT_printf(0,"Bootloader settings updated.\r\n");

	    /* Update the GPREGRET to inticate to the bootloader that an
	     * update via modem is requested. */
		err_code = sd_power_gpregret_set(BOOTLOADER_MODEM_REQ);
		APP_ERROR_CHECK(err_code);

		err_code = sd_softdevice_disable();
		APP_ERROR_CHECK(err_code);


	    NVIC_SystemReset();


//		err_code = sd_softdevice_vector_table_base_set(BOOTLOADER_REGION_START);
//		APP_ERROR_CHECK(err_code);
//
//		NVIC_ClearPendingIRQ(SWI2_IRQn);
//		interrupts_disable();
//
//		bootloader_util_app_start(BOOTLOADER_REGION_START);
}





