/* 
 * File:   defines.h
 * Author: kwgilpin
 *
 * Created on January 6, 2013, 7:43 PM
 */

#ifndef DEFINES_H
#define	DEFINES_H

#define TEMPALERT_SERVER_PORT 8002

#define PROTOCOL_MAJOR 0x04
#define PROTOCOL_MINOR 0x02

#define DATA_STACK_BUFFER_SIZE 512 // 1KB
#define DATA_FIFO_BUFFER_SIZE 68314308 // 6MBytes

#define PACKET_MAX_LENGTH	220
#define BOOTLOADER_VERIFICATION_TIME_100MS	20 // 2 seconds

#define ATOMIC_BLOCK(state) {}

#define PRINT_FROM_SF 1
#define CRLF "\r\n"

#endif	/* DEFINES_H */

