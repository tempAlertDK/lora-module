/*
 * sensors.h
 *
 *  Created on: Jul 2, 2014
 *      Author: kwgilpin
 */

#ifndef SENSORS_H_
#define SENSORS_H_

#include <stdint.h>
#include <stdbool.h>
#include "nrf_gpio.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "portmacro_cmsis.h"
#include "app_twi.h"
#include "event_groups.h"
#include "capwand.h"
#include "notify.h"

#define SENSOR_MEASUREMENT_INTERVAL_TIMER		0x01

#define MODEM_MAXIMUM_RSRP_LEVEL		97
#define MODEM_UNKNOWN_RSRP_LEVEL		255
#define SC_MAXIMUM_SIGNAL_LEVEL			10

typedef struct {
	uint8_t validMeasurementFlags;
	TickType_t timestamp;
	int32_t unixtime;
	int16_t temperature_tenthDegC;
	int8_t level_percentage;
	bool level_valid;
	uint32_t capacitance;
	uint16_t batteryVoltage_mV;
	uint8_t batteryPercentage;
	uint8_t calibrationData0[3];
	uint8_t calibrationData1[3];
} sensorData_t;

typedef struct {
	TickType_t timestamp;
	uint8_t rsrp;
	uint8_t rsrq;
	uint16_t batteryVoltage_mV;
} statusData_t;


/* Control Message struct used to alert the sensors task
 * of a requested operation.
 */
extern QueueHandle_t sensors_controlQueue;
extern NotifyHandle_t lowBatteryNotifyHandle;

typedef union {
	uint32_t _ui32;
	int8_t _i8;
	uint8_t a_ui8[4];
} sensors_control_message_data_t;

typedef enum {
    SENSORS_EVENT_CALIBRATE_LEVEL,
    SENSORS_EVENT_MI_UPDATE,
	SENSORS_EVENT_READ_NOW,
	SENSORS_EVENT_READ_STATUS,
	SENSORS_EVENT_SET_CALIBRATION_POINT
} sensors_control_message_type_t;

typedef struct {
	sensors_control_message_type_t type;
	sensors_control_message_data_t data;
	QueueHandle_t responseQueue;
} sensors_control_message_t;

typedef enum {
	SENSORS_UNKNOWN_ERROR,
	SENSORS_READ,
	STATUS_READ
} sensors_responce_t ;

extern TaskHandle_t m_sensors_thread;

bool sensors_init(void);
bool sensors_requestRead();
bool sensors_setMeasurementInterval(uint32_t newMeasurementInterval_sec);
bool sensors_hasValidDataUploaded();
int16_t sensors_getRecentTemp(void);
int8_t sensors_getRecentLevel(void);
uint32_t sensors_getRecentCapacitance(void);
bool sensors_getCalibrationDataPoint(uint8_t point, uint8_t * data);  //  [POINT, CAP_MSB, CAP_LSB, LVL]
bool sensors_getRecentBattery(uint16_t *batt);
bool sensors_getRecentBatteryPercentage(uint8_t * percentage);
bool sensors_getRecentSensorData(sensorData_t * p_sensorData);
bool sensors_getMeasurementIntervalSeconds(uint32_t * interval);
bool sensors_read(TickType_t blockUntil, sensors_responce_t type);

/* Calls to access shared memory */
extern QueueHandle_t sensors_sfQueue;
extern EventGroupHandle_t xSensorsEventGroup;

/* Bit mask and queue/event identifiers */
#define SENSORS_EVENT_MI_TIMER_EXPIRED_BIT				(1 << 0) // Highest priority
#define SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT			(1 << 1) //
#define SENSORS_EVENT_REQUEST_READING_BIT				(1 << 2)
#define SENSORS_EVENT_UPLOAD_FINISHED_BIT				(1 << 3)	 // Upload of data is completed


extern SemaphoreHandle_t g_recentSensorData_smphr;

/* Sensor type definitions */
#define SENSOR_ID_TEMP     				0x74
#define SENSOR_ID_STARWATCH				0x7D
#define SENSOR_ID_LEVEL					0x79
#define SENSOR_ID_CAPACITANCE			0x59
#define SENSOR_ID_VOLTAGE				0x6C

#define SENSOR_SIZE_TEMP     			2
#define SENSOR_SIZE_STARWATCH			4


#endif /* SENSORS_H_ */
