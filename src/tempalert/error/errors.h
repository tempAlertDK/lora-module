/*
 * errors.h
 *
 *  Created on: Oct 10, 2017
 *      Author: Nate-Tempalert
 */

#ifndef ERRORS_H_
#define ERRORS_H_

/**
 * Collect all error counters and populate S&F records accordingly
 * Format expressed: https://tempalert.atlassian.net/wiki/spaces/EN/pages/116424705/Beta+NG+Error+Message+Subtypes+0x99+0x96 
 * 
 * returns true if no failure occured (such as failed to push to S&F)
*/
void errors_init(void);
bool errors_collectStartupErrors(void);
bool errors_collectUploadErrors(void);
#endif
