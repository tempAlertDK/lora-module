#include <stdint.h>
#include <stdbool.h>

#include "errorCount.h"

#define ERROR_COUNTS_VALID_KEY		0x6AAAEFEBUL

static struct {
	uint32_t key;
	uint8_t errorCounts[ERRORCOUNT_TYPE_COUNT];
} m_noinitErrorLogData __attribute__ ((section(".noinit")));

static bool m_countsValid = false;

static const char * errorCountTypeNames[ERRORCOUNT_TYPE_COUNT];


void errorCount_init() {
	if (m_noinitErrorLogData.key != ERROR_COUNTS_VALID_KEY) {
		errorCount_resetCounts();
		m_noinitErrorLogData.key = ERROR_COUNTS_VALID_KEY;
	}

	m_countsValid = true;

	errorCountTypeNames[0] = "Failed to register";
	errorCountTypeNames[1] = "Failed to activate PDP context";
	errorCountTypeNames[2] = "Failed to open UDP socket";
	errorCountTypeNames[3] = "No response from server";
	errorCountTypeNames[4] = "VBAT too low to operate modem";
	errorCountTypeNames[5] = "Modem hardware failure";
	errorCountTypeNames[6] = "Too many upload failures prompting discard of packet";
	errorCountTypeNames[7] = "Server did not recognize device ID prompting discard of packet";
	errorCountTypeNames[8] = "Server rejected device ID due to bad account status prompting discard of packet";
	errorCountTypeNames[9] = "S&F disabled prompting discard of old local packet";
	errorCountTypeNames[10] = "S&F buffer corrupted prompting flush";
	errorCountTypeNames[11] = "S&F packet corrupted prompting discard of packet";
	errorCountTypeNames[12] = "S&F packet too old prompting discard of packet";
	errorCountTypeNames[13] = "S&F item lost due to stack/FIFO push error";
	errorCountTypeNames[14] = "Cell modem responded to data before all bytes were sent to it.";
	errorCountTypeNames[15] = "Cell modem did not respond after all bytes were sent to it.";
	errorCountTypeNames[16] = "Cell modem experienced an error requiring ON_OFF* command.";
	errorCountTypeNames[17] = "Cell modem off command failed at AT SHDN attempt";
	errorCountTypeNames[18] = "Cell modem off command failed at On_OFF pulse attempt.";
	errorCountTypeNames[19] = "Cell modem off command failed at unconditional shutdown attempt.";
	errorCountTypeNames[20] = "Cell modem experienced an error requiring reset command.";
	errorCountTypeNames[21] = "Cell modem reset command failed.";
	errorCountTypeNames[22] = "Cell modem unconditional shutdown command failed in modemtype_on() ";
}

void errorCount_resetCount(errorCount_error_t errorType) {
	if ((errorType < 0) || (errorType >= ERRORCOUNT_TYPE_COUNT)) {
		return;
	}

	m_noinitErrorLogData.errorCounts[errorType] = 0;
}

void errorCount_resetCounts() {
	int i;

	for (i=0; i<ERRORCOUNT_TYPE_COUNT; i++) {
		m_noinitErrorLogData.errorCounts[i] = 0;
	}
}

void errorCount_resetCountsArray(errorCountMask_t * array, uint8_t arrayLength) {
	int i;

	for (i = 0; i < arrayLength; i++) {
		if ((array[i] < 0) || (array[i] >= ERRORCOUNT_TYPE_COUNT)) {
			return;
		}
		if (m_noinitErrorLogData.errorCounts[array[i]] > 0) {
			m_noinitErrorLogData.errorCounts[array[i]] = 0;
		}
	}
}
uint8_t errorCount_getTypeCount() {
	return ERRORCOUNT_TYPE_COUNT;
}

bool errorCount_getErrorsExist() {
	int i;

	if (!m_countsValid) {
		return false;
	}

	for (i = 0; i < ERRORCOUNT_TYPE_COUNT; i++) {
		if (m_noinitErrorLogData.errorCounts[i] > 0) {
			return true;
		}
	}

	return false;
}

bool errorCount_getErrorsExistingArray(errorCountMask_t * array, uint8_t arrayLength) {
	int i;

	if (!m_countsValid) {
		return false;
	}

	for (i = 0; i < arrayLength; i++) {
		if ((array[i] < 0) || (array[i] >= ERRORCOUNT_TYPE_COUNT)) {
			return false;
		}
		if (m_noinitErrorLogData.errorCounts[array[i]] > 0) {
			return true;
		}
	}

	return false;
}


void errorCount_incrementCount(errorCount_error_t errorType) {
	if (!m_countsValid) {
		return;
	}

	if ((errorType < 0) || (errorType >= ERRORCOUNT_TYPE_COUNT)) {
		return;
	}

	if (m_noinitErrorLogData.errorCounts[errorType] < UINT8_MAX) {
		m_noinitErrorLogData.errorCounts[errorType]++;
	}
}

bool errorCount_setCount(errorCount_error_t errorType,uint8_t value) {
	if (!m_countsValid) {
		return false;
	}

	if ((errorType < 0) || (errorType >= ERRORCOUNT_TYPE_COUNT)) {
		return false;
	}

	m_noinitErrorLogData.errorCounts[errorType] = value;
	return true;
}

uint8_t errorCount_getCount(errorCount_error_t errorType) {
	if (!m_countsValid) {
		return 0;
	}

	if ((errorType < 0) || (errorType >= ERRORCOUNT_TYPE_COUNT)) {
		return 0;
	}

	return m_noinitErrorLogData.errorCounts[errorType];
}

uint8_t errorCount_getCount4Bits(errorCount_error_t errorType) {
	uint8_t count;

	count = errorCount_getCount(errorType);

	if (count > 0x0F) {
		return 0x0F;
	} else {
		return count;
	}
}

uint8_t errorCount_getCount2Bits(errorCount_error_t errorType) {
	uint8_t count;

	count = errorCount_getCount(errorType);

	if (count > 0x03) {
		return 0x03;
	} else {
		return count;
	}
}

uint8_t errorCount_getCount1Bit(errorCount_error_t errorType) {
	uint8_t count;

	count = errorCount_getCount(errorType);

	if (count > 0x01) {
		return 0x01;
	} else {
		return count;
	}
}
