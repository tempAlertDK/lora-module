/*
 * errors.c
 *
 *  Created on: Oct 10, 2017
 *      Author: Nate-Tempalert
 */
#include <stdint.h>
#include <stdbool.h>

#include "errors.h"
#include "errorCount.h"
#include "stoFwd.h"
#include "packet.h"
#include "unixTime.h"
#include "stoFwd.h"
#include "log.h"
#include "FreeRTOS.h"
#include "task.h"
#include "unixTime.h"

errorCountMask_t longError_counts2Mask[] = {
		ERROR_MODEM_HARDWARE_FAILURE,
		ERROR_VBAT_TOO_LOW_FOR_MODEM,
		ERROR_FAILED_TO_REGISTER,
		ERROR_FAILED_TO_ACTIVATE_PDP_CONTEXT,
		ERROR_FAILED_TO_OPEN_UDP_SOCKET,
		ERROR_NO_RESPONSE_FROM_SERVER,
		ERROR_MODEM_RESPONDED_PREMATURELY_TO_SENT_DATA,
		ERROR_MODEM_NO_RESPONSE_TO_SENT_DATA,
		ERROR_MODEM_REQUIRING_OFF_CMD,
		ERROR_MODEM_REQUIRING_OFF_CMD_FAILED_AT_ATSHDN,
		ERROR_MODEM_REQUIRING_OFF_CMD_FAILED_AT_ON_OFF_PULSE,
		ERROR_MODEM_REQUIRING_OFF_CMD_FAILED_AT_UNCONDITIONAL_SHUTDOWN,
		ERROR_MODEM_REQUIRING_RESET_CMD,
		ERROR_MODEM_REQUIRING_RESET_CMD_FAILED,
		ERROR_MODEM_REQUIRING_ON_CMD_FAILED_AT_UNCONDITIONAL_SHUTDOWN};

errorCountMask_t longError_stofwdStatus[] = {
		ERROR_STOFWD_BUFFER_CORRUPTED_PROMPTING_FLUSH,
		ERROR_STOFWD_DISABLED_PROMPTING_DISCARD_OF_OLD_LOCAL_PACKET,
		ERROR_TOO_MANY_UPLOAD_FAILURES_PROMPTING_DISCARD_OF_PACKET,
		ERROR_STOFWD_PACKET_CORRUPTED_PROMPTING_DISCARD_OF_PACKET,
		ERROR_SERVER_DID_NOT_RECOGNIZE_DEVICE_ID_PROMPTING_DISCARD_OF_PACKET,
		ERROR_SERVER_REJECTED_DEVICE_ID_DUE_TO_BAD_ACCOUNT_STATUS_PROMPTING_DISCARD_OF_PACKET,
		ERROR_STOFWD_TOO_OLD_PROMPTING_DISCARD_OF_PACKET,
		ERROR_STOFWD_PUSH_ERROR_RESULTING_IN_LOST_ITEM};

errorCountMask_t longError_counts3Mask[] = {
		ERROR_MODEM_FAILED_TO_BOOT,
		ERROR_MODEM_UNRESPONSIVE,
		ERROR_SERVER_CLOSED_TCP_CONNECTION,
		ERROR_MODEM_AT_COMMAND_FAILED_TO_QUEUE,
		ERROR_MODEM_UNHANDLED_RESPONSE,
		ERROR_FAILED_TO_TURNOFF_MODEM,
		ERROR_PACKET_SEND_RETRIES_EXHAUSTED,
		ERROR_MODEM_ON_FAILED_AT_STATE};

void errors_init(void) {
	errorCount_init();
}

bool errors_collectStartupErrors(void) {
    sf_record_t sfrec;
    uint16_t ptr = 0, messages = 0;

	/* Initialize record header */
    sfrec.header.type = SF_STATUS_RECORD;
    sfrec.header.utc_s = 0;
    sfrec.header.timeStamp = get_time_100ms();

    // Report reset reason and seconds since reset ERROR_REPORT_SECONDS_SINCE_RESET_SUBMESSAGE_ID
    sfrec.data[ptr++] = ERROR_REPORT_MESSAGE_ID;
    sfrec.data[ptr++] = ERROR_REPORT_SECONDS_SINCE_RESET_SUBMESSAGE_ID;
    sfrec.data[ptr++] = g_resetReason;
    uint32_t boottime = get_time_100ms();
    sfrec.data[ptr++] = (uint8_t)(boottime >> 8);
    sfrec.data[ptr++] = (uint8_t)(boottime);
    messages++;

    // TODO: Report ERROR_REPORT_BOOTLOADER_STATUS_SUBMESSAGE_ID

    // TODO: Report ERROR_REPORT_DISCARDED_STACK_COUNT_SUBMESSAGE_ID

    sfrec.header.messages = messages;
    sfrec.header.dataLength = ptr;

    // Send data to the S&F buffer
    if (stoFwd_sendRecord(&sfrec, portMAX_DELAY) == false) {
    		log_error("Failed to queue startup error messages!");
    		return false;
    }
    return true;
}

bool errors_collectUploadErrors(void) {
	sf_record_t sfrec;
	uint16_t ptr = 0, messages = 0;

	/* Initialize record header */
    sfrec.header.type = SF_STATUS_RECORD;
    sfrec.header.utc_s = 0;
    sfrec.header.timeStamp = get_time_100ms();

	// Report reset reason and seconds since reset ERROR_REPORT_ERROR_COUNTS_2_SUBMESSAGE_ID
	if (errorCount_getErrorsExistingArray(longError_counts2Mask,sizeof(longError_counts2Mask))) {
		sfrec.data[ptr++] = LONG_ERROR_REPORT_MESSAGE_ID;
		sfrec.data[ptr++] = LONG_ERROR_REPORT_ERROR_COUNTS_2_SUBMESSAGE_ID;
		sfrec.data[ptr++] = (errorCount_getCount4Bits(ERROR_MODEM_HARDWARE_FAILURE) << 4)
								| errorCount_getCount4Bits(ERROR_VBAT_TOO_LOW_FOR_MODEM);
		sfrec.data[ptr++] = (errorCount_getCount4Bits(ERROR_FAILED_TO_REGISTER) << 4)
								| errorCount_getCount4Bits(ERROR_FAILED_TO_ACTIVATE_PDP_CONTEXT);
		sfrec.data[ptr++] = (errorCount_getCount4Bits(ERROR_FAILED_TO_OPEN_UDP_SOCKET) << 4)
								| errorCount_getCount4Bits(ERROR_NO_RESPONSE_FROM_SERVER);
		sfrec.data[ptr++] = (errorCount_getCount4Bits(ERROR_MODEM_RESPONDED_PREMATURELY_TO_SENT_DATA) << 4)
								| errorCount_getCount4Bits(ERROR_MODEM_NO_RESPONSE_TO_SENT_DATA);
		sfrec.data[ptr++] = (errorCount_getCount4Bits(ERROR_MODEM_REQUIRING_OFF_CMD) << 4)
								| errorCount_getCount4Bits(ERROR_MODEM_REQUIRING_OFF_CMD_FAILED_AT_ATSHDN);
		sfrec.data[ptr++] = (errorCount_getCount4Bits(ERROR_MODEM_REQUIRING_OFF_CMD_FAILED_AT_ON_OFF_PULSE) << 4)
								| errorCount_getCount4Bits(ERROR_MODEM_REQUIRING_OFF_CMD_FAILED_AT_UNCONDITIONAL_SHUTDOWN);
		sfrec.data[ptr++] = (errorCount_getCount2Bits(ERROR_MODEM_REQUIRING_RESET_CMD) << 6)
								| (errorCount_getCount2Bits(ERROR_MODEM_REQUIRING_RESET_CMD_FAILED) << 4)
								| (errorCount_getCount2Bits(ERROR_MODEM_REQUIRING_ON_CMD_FAILED_AT_UNCONDITIONAL_SHUTDOWN) << 2);
		messages++;
		errorCount_resetCountsArray(longError_counts2Mask,sizeof(longError_counts2Mask));
	}

	// Report the current S&F status including dropped items or item count remaining
	if (errorCount_getErrorsExistingArray(longError_stofwdStatus,sizeof(longError_stofwdStatus)) || !stoFwd_isEmpty()) {
		sfrec.data[ptr++] = LONG_ERROR_REPORT_MESSAGE_ID;
		sfrec.data[ptr++] = LONG_ERROR_REPORT_STOFWD_STATUS_SUBMESSAGE_ID;
		sfrec.data[ptr++] = (errorCount_getCount1Bit(ERROR_STOFWD_BUFFER_CORRUPTED_PROMPTING_FLUSH) << 1) | 1;
		sfrec.data[ptr++] = (uint8_t) stoFwd_countItemsStack();
		sfrec.data[ptr++] = (uint8_t) (stoFwd_countItemsFIFO());
		sfrec.data[ptr++] = (uint8_t) (stoFwd_countItemsFIFO() >> 8);
		sfrec.data[ptr++] = errorCount_getCount(ERROR_STOFWD_DISABLED_PROMPTING_DISCARD_OF_OLD_LOCAL_PACKET);
		sfrec.data[ptr++] = (errorCount_getCount2Bits(ERROR_TOO_MANY_UPLOAD_FAILURES_PROMPTING_DISCARD_OF_PACKET) << 6) |
				(errorCount_getCount2Bits(ERROR_STOFWD_PACKET_CORRUPTED_PROMPTING_DISCARD_OF_PACKET) << 4) |
				(errorCount_getCount2Bits(ERROR_SERVER_DID_NOT_RECOGNIZE_DEVICE_ID_PROMPTING_DISCARD_OF_PACKET) << 2) |
				errorCount_getCount2Bits(ERROR_SERVER_REJECTED_DEVICE_ID_DUE_TO_BAD_ACCOUNT_STATUS_PROMPTING_DISCARD_OF_PACKET);
		sfrec.data[ptr++] = (errorCount_getCount2Bits(ERROR_STOFWD_TOO_OLD_PROMPTING_DISCARD_OF_PACKET) << 6) |
				(errorCount_getCount2Bits(ERROR_STOFWD_PUSH_ERROR_RESULTING_IN_LOST_ITEM) << 2);
		messages++;
		errorCount_resetCountsArray(longError_stofwdStatus,sizeof(longError_stofwdStatus));
	}

	if(errorCount_getErrorsExistingArray(longError_counts3Mask,sizeof(longError_counts3Mask))) {
		sfrec.data[ptr++] = LONG_ERROR_REPORT_MESSAGE_ID;
		sfrec.data[ptr++] = ERROR_REPORT_ERROR_COUNTS_3_SUBMESSAGE_ID;
		sfrec.data[ptr++] = (errorCount_getCount(ERROR_MODEM_ON_FAILED_AT_STATE));
		sfrec.data[ptr++] = (errorCount_getCount2Bits(ERROR_MODEM_FAILED_TO_BOOT) << 6) |
				(errorCount_getCount2Bits(ERROR_FAILED_TO_TURNOFF_MODEM) << 4) |
				(errorCount_getCount4Bits(ERROR_PACKET_SEND_RETRIES_EXHAUSTED));
		sfrec.data[ptr++] = (errorCount_getCount(ERROR_MODEM_UNRESPONSIVE));
		sfrec.data[ptr++] = (errorCount_getCount4Bits(ERROR_MODEM_UNHANDLED_RESPONSE) << 4) |
				(errorCount_getCount4Bits(ERROR_MODEM_AT_COMMAND_FAILED_TO_QUEUE));
		sfrec.data[ptr++] = (errorCount_getCount(ERROR_SERVER_CLOSED_TCP_CONNECTION));
		sfrec.data[ptr++] = 0; // unused as of now
		sfrec.data[ptr++] = 0; // unused as of now
		messages++;
		errorCount_resetCountsArray(longError_counts3Mask,sizeof(longError_counts3Mask));
	}

    sfrec.header.messages = messages;
    sfrec.header.dataLength = ptr;

    if (messages) {
		// Send data to the S&F buffer
		if (stoFwd_sendRecord(&sfrec, portMAX_DELAY) == false) {
				errorCount_incrementCount(ERROR_STOFWD_PUSH_ERROR_RESULTING_IN_LOST_ITEM);
				log_error("Failed to queue upload error messages!");
				return false;
		}
    }
    return true;

}
