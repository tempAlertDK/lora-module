/*
 * battTemp.c
 *
 *  Created on: Sep 20, 2017
 *      Author: nreimens
 */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "power.h"
#include "FreeRTOS.h"
#include "battTemp.h"
#include "log.h"

#if defined (HARDWARE_VERSION) && ((HARDWARE_VERSION == 1) || (HARDWARE_VERSION == 3)) // Primary Cells
#define BATTERY_TEMP_HISTORY_DEPTH				24 // 24 HOURS
#define MAX_BATTERY_TEMPERATURE 					600
#define MIN_BATTERY_TEMPERAUTRE					-400
#define MAX_TEMPERATURE_BATTERY_HIGH_VOLTAGE 	7100
#define MAX_TEMPERATURE_BATTERY_LOW_VOLTAGE 		6400
#define MIN_TEMPERATURE_BATTERY_HIGH_VOLTAGE 	5600
#define MIN_TEMPERATURE_BATTERY_LOW_VOLTAGE 		4800

#elif defined (HARDWARE_VERSION) && (HARDWARE_VERSION == 2) // Secondary Cells
#define BATTERY_TEMP_HISTORY_DEPTH				4 // 24 HOURS
#define MAX_BATTERY_TEMPERATURE 					250
#define MIN_BATTERY_TEMPERAUTRE					-100
#define MAX_TEMPERATURE_BATTERY_HIGH_VOLTAGE 	4200
#define MAX_TEMPERATURE_BATTERY_LOW_VOLTAGE 		3400
#define MIN_TEMPERATURE_BATTERY_HIGH_VOLTAGE 	4000
#define MIN_TEMPERATURE_BATTERY_LOW_VOLTAGE 		3000
#else
#error Hardware Version not defined!!!
#endif


#define SLOPE_COMPENSATION
#define DEBUG_BATTTEMP 1

typedef struct {
	uint16_t vbatt_mv;
	int16_t temp_dC;
	TickType_t timestamp_ms;
} BattTempPoint_t;


BattTempPoint_t m_battTempHistory[24];
BattTempPoint_t m_recentBattTempPoint;

/* recent calculated values */
uint16_t m_lastLowBatteryThreshold_mV;
uint16_t m_lastHighBatteryThreshold_mV;
uint8_t m_lastBatteryPercentage_pct;

uint16_t battTemp_calculateLowThreshold(int16_t temp);
uint16_t battTemp_calculateHighThreshold(int16_t temp);

uint16_t min(uint16_t a, uint16_t b) { if (a < b) { return a; } else { return b;}}
uint16_t max(uint16_t a, uint16_t b) { if (a > b) { return a; } else { return b;}}

void battTemp_init(void) {
	uint16_t i;

	// Clear history for now
	for (i=0; i< BATTERY_TEMP_HISTORY_DEPTH; i++) {
		memset(&m_battTempHistory[i],0,sizeof(BattTempPoint_t));
	}


	return;
}

/* Module iterator: Pass in current conditions to generate a new battery percentage approximation */
uint8_t battTemp_getBatteryPercentage(TickType_t readingTime_ms,  uint16_t batteryVoltage_mV, int16_t temperature_dC) {

	// TODO: Implement a more sophisticated scheme for determining the battery state
	uint16_t vBatt = batteryVoltage_mV;
	int16_t temp = temperature_dC;
	uint16_t vLow = battTemp_calculateLowThreshold(temperature_dC);
	uint16_t vHigh = battTemp_calculateHighThreshold(temperature_dC);
#if defined (DEBUG_BATTTEMP) && (DEBUG_BATTTEMP == 1)
	log_debugf("BATTTEMP: VHIGH[%umV] VLOW[%umV] VBATT[%umV] TEMP[%d°dC]",vHigh,vLow,vBatt,temp);
#endif

	uint16_t percentFull = (((max(min(vBatt,vHigh),vLow) - vLow) * 100)) / (vHigh - vLow);

	m_lastLowBatteryThreshold_mV = vLow;
	m_lastHighBatteryThreshold_mV = vHigh;
	m_lastBatteryPercentage_pct = (uint8_t) percentFull;

	return m_lastBatteryPercentage_pct;
}

/* Module internal: Returns low threshold used to calculate recent battery percentage */
uint16_t battTemp_calculateLowThreshold(int16_t temp) {

	int32_t lowThresholdVoltage = MIN_TEMPERATURE_BATTERY_LOW_VOLTAGE + (((((int32_t)temp) - MIN_BATTERY_TEMPERAUTRE) *
			(MAX_TEMPERATURE_BATTERY_LOW_VOLTAGE - MIN_TEMPERATURE_BATTERY_LOW_VOLTAGE)) /
			(MAX_BATTERY_TEMPERATURE - MIN_BATTERY_TEMPERAUTRE));
	return (uint16_t) lowThresholdVoltage;
}

/* Module external: Returns last percentage calculated from recent battery percentage */
uint8_t battTemp_getLastPercentage(void) {
	return m_lastBatteryPercentage_pct;
}

/* Module internal: Returns low threshold used to calculate recent battery percentage */
uint16_t battTemp_calculateHighThreshold(int16_t temp) {

	int32_t highThresholdVoltage = MIN_TEMPERATURE_BATTERY_HIGH_VOLTAGE + (((((int32_t)temp) - MIN_BATTERY_TEMPERAUTRE) *
			(MAX_TEMPERATURE_BATTERY_HIGH_VOLTAGE - MIN_TEMPERATURE_BATTERY_HIGH_VOLTAGE)) /
			(MAX_BATTERY_TEMPERATURE - MIN_BATTERY_TEMPERAUTRE));
	return (uint16_t) highThresholdVoltage;
}



/* Module external: Returns last high threshold used to calculate recent battery percentage */
uint16_t battTemp_getLastHighThreshold(void) {
	return m_lastHighBatteryThreshold_mV;
}

/* Module external: Returns last low threshold used to calculate recent battery percentage */
uint16_t battTemp_getLastLowThreshold(void) {
	return m_lastLowBatteryThreshold_mV;
}

/* Module external: Returns last battery percentage used to calculate recent battery percentage */
uint16_t battTemp_getLastBatteryPercentage(void) {
	return m_lastBatteryPercentage_pct;
}
