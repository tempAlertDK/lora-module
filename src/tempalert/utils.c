/*
 * util.c
 *
 *  Created on: Nov 13, 2013
 *      Author: kwgilpin
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "nrf_error.h"
#include "utils.h"
#include "FreeRTOS.h"
#include "task.h"

#ifdef TTDEBUG
uint8_t debug_level = ALL_DEBUG;
#else
uint8_t debug_level = BASIC_DEBUG;
#endif
