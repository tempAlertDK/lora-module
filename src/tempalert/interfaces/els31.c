/*
 * els31.c
 *
 *  Created on: Dec 30, 2016
 *      Author: Nate-Tempalert
 */

#include <stdint.h>
#include <stdbool.h>

#include "SEGGER_RTT.h"

#include "FreeRTOS.h"
#include "task.h"

#include "boards.h"
#include "nrf_delay.h"
#include "mcp23008.h"
#include "els31.h"
#include "twi_mtx.h"



bool els31_init() {
	bool status = false;
	uint8_t tristates;

	WITH_TWI_MTX() {
		/*
		 * If the CELL_POWER pin driving either high or low, we leave it
		 * alone so that we do not improperly remove or apply power to the cell
		 * modem.  Otherwise, if it appears to not have been configured, we set it
		 * to drive low, in order to withhold power from the cell modem and to keep
		 * the output of the buck converter at its lower voltage setting.
		 */
		status = mcp23008_queryTristatePins(MCP23008_CELL_I2C_ADDR, &tristates);
		if (status && (tristates & CELL_POWER_EXPANDER_PIN)) {
			status &= mcp23008_clearPins(MCP23008_CELL_I2C_ADDR, CELL_POWER_EXPANDER_PIN);
		}

		/*
		 * Likewise, if the LDO-EN pin is driving either high or low, we leave it
		 * alone.  Otherwise, if it appears to not have been configured, we set it
		 * to drive low by default, in order to keep the LDO disabled and instead
		 * short VBUCK directly to VSYS.
		 */
		if (status && (tristates & LDOEN_EXPANDER_PIN)) {
			status &= mcp23008_clearPins(MCP23008_CELL_I2C_ADDR, LDOEN_EXPANDER_PIN);
		}

	#ifdef BOARD_CELL_NODE_REV1P0
		// CELL_FAST_SHDN_PIN should be a default-low output
		nrf_gpio_pin_clear(CELL_FAST_SHDN_PIN);
		nrf_gpio_pin_dir_set(CELL_FAST_SHDN_PIN,  NRF_GPIO_PIN_DIR_OUTPUT);
	#endif

		// CELLSTATUSn should be an input
		status = mcp23008_tristatePins(MCP23008_CELL_I2C_ADDR, CELL_STATUSN_EXPANDER_PIN);

		// CELL_EMERG_RST should should be an output driving low by default
		status &= mcp23008_clearPins(MCP23008_CELL_I2C_ADDR, CELL_EMRG_RST_EXPANDER_PIN);

		// CELL_IGNITION should be an output driving low by default
		status &= mcp23008_clearPins(MCP23008_CELL_I2C_ADDR, CELL_IGNITION_EXPANDER_PIN);

		if (!status) {
			SEGGER_RTT_printf(0, "ERROR: Failed to initialize cell modem hardware interface.\r\n");
		}
	}

	return status;
}

bool els31_enablePower() {
	bool status = false;

	WITH_TWI_MTX() {
		status = mcp23008_setPins(MCP23008_CELL_I2C_ADDR, LDOEN_EXPANDER_PIN);
		vTaskDelay(pdMS_TO_TICKS(50));
		status &= mcp23008_setPins(MCP23008_CELL_I2C_ADDR, CELL_POWER_EXPANDER_PIN);
	}

	if (!status) {
		SEGGER_RTT_printf(0, "ERROR: Failed to enable Cell modem power.\r\n");
	}

	return status;
}

bool els31_disablePower() {
	bool status = false;

	WITH_TWI_MTX() {
		status = mcp23008_clearPins(MCP23008_CELL_I2C_ADDR, CELL_POWER_EXPANDER_PIN);
	}

	vTaskDelay(pdMS_TO_TICKS(50));

	WITH_TWI_MTX() {
		status &= mcp23008_clearPins(MCP23008_CELL_I2C_ADDR, LDOEN_EXPANDER_PIN);
	}

	if (!status) {
		SEGGER_RTT_printf(0, "ERROR: Failed to disable cell modem power.\r\n");
	}

	return status;
}

bool els31_queryPower(bool *powerIsOn) {
	bool status = false;
	uint8_t pins;

	WITH_TWI_MTX() {
		status = mcp23008_readPins(MCP23008_CELL_I2C_ADDR, &pins);
	}

	if (status) {
		*powerIsOn = (pins & CELL_POWER_EXPANDER_PIN);
	} else {
		SEGGER_RTT_printf(0, "ERROR: Failed to query state of cell modem power.\r\n");
	}

	return status;
}


bool els31_modemIgnition() {
	bool status = false;

	WITH_TWI_MTX() {
		status = mcp23008_setPins(MCP23008_CELL_I2C_ADDR, CELL_IGNITION_EXPANDER_PIN);
	}

	vTaskDelay(pdMS_TO_TICKS(75));

	WITH_TWI_MTX() {
		status &= mcp23008_clearPins(MCP23008_CELL_I2C_ADDR, CELL_IGNITION_EXPANDER_PIN);
	}

	if (!status) {
		SEGGER_RTT_printf(0, "ERROR: Failed to send ignition signal.\r\n");
	}

	return status;
}


bool els31_queryOn(bool *modemIsOn) {
	bool status = false;
	uint8_t pins;

	WITH_TWI_MTX() {
		status = mcp23008_readPins(MCP23008_CELL_I2C_ADDR, &pins);
	}

	if (status) {
		*modemIsOn = !(pins & CELL_STATUSN_EXPANDER_PIN);
	} else {
		printf("ERROR: Failed to read cell modem status\r\n");
	}

	return status;
}

#ifdef BOARD_CELL_NODE_REV1P0
bool els31_fastShutdown() {
	nrf_gpio_pin_set(CELL_FAST_SHDN_PIN);

	vTaskDelay(pdMS_TO_TICKS(20));

	nrf_gpio_pin_clear(CELL_FAST_SHDN_PIN);

	return true;
}
#endif

bool els31_emergencyReset() {
	bool status = false;

	WITH_TWI_MTX() {
		status = mcp23008_setPins(MCP23008_CELL_I2C_ADDR, CELL_EMRG_RST_EXPANDER_PIN);
	}

	vTaskDelay(pdMS_TO_TICKS(50));

	WITH_TWI_MTX() {
		status &= mcp23008_clearPins(MCP23008_CELL_I2C_ADDR, CELL_EMRG_RST_EXPANDER_PIN);
	}

	if (!status) {
		SEGGER_RTT_printf(0, "ERROR: Failed to send emergency reset signal to modem.\r\n");
	}

	return status;
}

bool els31_readCTS(bool * on) {
	bool status = true;
	nrf_gpio_pin_dir_set(CELL_CTS_PIN, 0); //it's an input
	status = (bool)nrf_gpio_pin_read(CELL_CTS_PIN);
	*on = status;
	return true;
}
