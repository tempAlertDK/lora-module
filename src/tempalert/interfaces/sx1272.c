/*
 * sx1272.c
 *
 *  Created on: Jan 10, 2017
 *      Author: Nate-Tempalert
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "SEGGER_RTT.h"
#include "app_util_platform.h"
#include "nordic_common.h"
#include "nrf.h"
#include "boards.h"
#include "nrf_delay.h"
#include "sx1272.h"
#include "nrf_drv_spi.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "app_error.h"
#include "mcp23008.h"
#include "twi_mtx.h"
#include "spi.h"
#include "FreeRTOS.h"
#include "task.h"

static const nrf_drv_spi_config_t sx1272_spi_config = {
		.sck_pin = SCLK_PIN,
		.mosi_pin = MOSI_PIN,
		.miso_pin = MISO_PIN,
		.ss_pin = NRF_DRV_SPI_PIN_NOT_USED,
		.irq_priority = APP_IRQ_PRIORITY_HIGH,
		.orc = 0xFF,
		.frequency = NRF_DRV_SPI_FREQ_1M,
		.mode = NRF_DRV_SPI_MODE_0,
		.bit_order = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST
};

static inline void sx1272_assert_cs(void);
static inline void sx1272_deassert_cs(void);

bool sx1272_clearDIO(void);

bool sx1272_init(void) {

	sx1272_reset();
	vTaskDelay(100);

	sx1272_disableRF();
	//dk_todo: these pin assignments(not expanded)
#ifndef BOARD_LORA_MODULE_REV1P0
	mcp23008_tristatePins(MCP23008_LORA_I2C_ADDR,
			LORA_DIO0_EXPANDER_PIN |
			LORA_DIO1_EXPANDER_PIN |
			LORA_DIO2_EXPANDER_PIN |
			LORA_DIO3_EXPANDER_PIN |
			LORA_DIO4_EXPANDER_PIN);
#endif

	WITH_TWI_MTX() {
		sx1272_assert_cs();
		sx1272_clearDIO();
		sx1272_deassert_cs();

		vTaskDelay(100);

		sx1272_assert_cs();
		sx1272_setModeSleep();
		sx1272_deassert_cs();
	}



	return true;
}

bool sx1272_setModeRX(void) {

	nrf_delay_ms(150);

	uint8_t p_tx_data[] = {0x01 | (1 << 7),0b00000101};  // Set to RX Mode
	uint8_t * p_rx_data = NULL;

	if (!spi_transfer(p_tx_data,sizeof(p_tx_data),p_rx_data,0)){
		return false;
	}
	return true;
}

bool sx1272_clearDIO(void) {

	nrf_delay_ms(10);

	uint8_t p_tx_data[] = {0x40 | (1 << 7),0b00000001};  // Set to RX Mode
	uint8_t * p_rx_data = NULL;

	if (!spi_transfer(p_tx_data,sizeof(p_tx_data),p_rx_data,0)){
		return false;
	}
	return true;
}

bool sx1272_setModeTX(void) {

	nrf_delay_ms(150);

	uint8_t p_tx_data[] = {0x01 | (1 << 7),0b00000011};  // Set to TX Mode
	uint8_t * p_rx_data = NULL;

	if (!spi_transfer(p_tx_data,sizeof(p_tx_data),p_rx_data,0)){
		return false;
	}
	return true;
}

bool sx1272_disableRF(void) {
#ifdef BOARD_CELL_NODE_REV1P0
	if (!mcp23008_setPins(MCP23008_LORA_I2C_ADDR, LORA_RFENN_EXPANDER_PIN))
#elif defined (BOARD_CELL_NODE_REV1P1)
	if (!mcp23008_clearPins(MCP23008_LORA_I2C_ADDR, LORA_FEM_CSD_EXPANDER_PIN | LORA_FEM_CPS_EXPANDER_PIN))
#elif defined (BOARD_LORA_MODULE_REV1P0)
	nrf_gpio_pin_clear(LORA_FEM_CPS);
	nrf_gpio_pin_clear(LORA_FEM_CSD);
#endif
	{
		SEGGER_RTT_printf(0,"Failed to disable SX1272 RF Path!!\r\n");
		return false;
	}
	return true;
}

bool sx1272_enableRF(void) {
#ifdef BOARD_CELL_NODE_REV1P0
	if (!mcp23008_clearPins(MCP23008_LORA_I2C_ADDR, LORA_RFENN_EXPANDER_PIN))
#elif defined (BOARD_CELL_NODE_REV1P1)
	if (!mcp23008_setPins(MCP23008_LORA_I2C_ADDR, LORA_FEM_CSD_EXPANDER_PIN))
#elif defined (BOARD_LORA_MODULE_REV1P0)
	nrf_gpio_pin_set(LORA_FEM_CPS);
	nrf_gpio_pin_set(LORA_FEM_CSD);
#endif
	{
		SEGGER_RTT_printf(0,"Failed to enable SX1272 RF Path!!\r\n");
		return false;
	}
	return true;
}

bool sx1272_reset() {
#ifndef BOARD_LORA_MODULE_REV1P0
	if (!mcp23008_setPins(MCP23008_LORA_I2C_ADDR,LORA_RESETN_EXPANDER_PIN)) {
		SEGGER_RTT_printf(0,"Failed to disable SX1272 reset line!!\r\n");
		return false;
	}
#else
	nrf_gpio_pin_set(LORA_RESETN);
#endif

	nrf_delay_ms(10);
#ifndef BOARD_LORA_MODULE_REV1P0
	if (!mcp23008_clearPins(MCP23008_LORA_I2C_ADDR,LORA_RESETN_EXPANDER_PIN)) {
		SEGGER_RTT_printf(0,"Failed to assert SX1272 reset line!!\r\n");
		return false;
	}
#else
		nrf_gpio_pin_clear(LORA_RESETN);
#endif

	vTaskDelay(100);

#ifndef BOARD_LORA_MODULE_REV1P0
	if (!mcp23008_tristatePins(MCP23008_LORA_I2C_ADDR, LORA_RESETN_EXPANDER_PIN)) {
		SEGGER_RTT_printf(0,"Failed to change SX1272 reset line to a Tristate pin!!\r\n");
		return false;
	}
#else
	nrf_gpio_cfg_input(LORA_RESETN, NRF_GPIO_PIN_NOPULL);
#endif

	return true;
}


bool sx1272_setModeSleep(void) {
	bool success = true;
	nrf_delay_ms(150);

	uint8_t p_tx_data[] = {0x01 | (1 << 7),0b00000000};  // Set Sleep Mode
	uint8_t * p_rx_data = NULL;

	if (!spi_transfer(p_tx_data,sizeof(p_tx_data),p_rx_data,0)){
		success = false;
	}

	return success;
}

const char *byte_to_binary(uint8_t x)
{
    static char b[9];
    b[0] = '\0';

    int z;
    for (z = 128; z > 0; z >>= 1)
    {
        strcat(b, ((x & z) == z) ? "1" : "0");
    }

    return b;
}

bool sx1272_testDIO(void) {

	nrf_delay_ms(50);

	uint8_t p_tx_data[] = {0x40 | (1 << 7),0b01010100,0b01110000};  // Set GPIO to (hopefully) Known States
	uint8_t * p_rx_data = NULL;

	if (!spi_transfer(p_tx_data,sizeof(p_tx_data),p_rx_data,0)){
		return false;
	}

	nrf_delay_ms(50);

	uint8_t pins = 0;
#ifndef BOARD_LORA_MODULE_REV1P0
	if (!mcp23008_readPins(MCP23008_LORA_I2C_ADDR,&pins)){
		return false;
	}
	SEGGER_RTT_printf(0,"SX1272 DIO Pin States: %s\r\n", byte_to_binary(pins));
#else
	SEGGER_RTT_printf(0,"Not implemented for BOARD_LORA_MODULE_REV1P0");
	//todo: lora module functionality absent
#endif

	return true;
}

bool sx1272_queryMode(void) {

	nrf_delay_ms(150);

	uint8_t p_tx_data[] = {0x01};  // Set Sleep Mode
	uint8_t p_rx_data[] = {0,0};

	if (!spi_transfer(p_tx_data,sizeof(p_tx_data),p_rx_data,sizeof(p_rx_data))){
		return false;
	}
	SEGGER_RTT_printf(0,"SX1272 Mode: %02x\r\n",p_rx_data[1]);
	return true;
}


bool sx1272_checkManID(void) {

	nrf_delay_ms(150);

	uint8_t p_tx_data[] = {0x42,0};  // read manufacture/device ID
	uint8_t p_rx_data[] = {0,0};

	if (!spi_transfer(p_tx_data,sizeof(p_tx_data),p_rx_data,sizeof(p_rx_data))){
		return false;
	}
	if (p_rx_data[1] != SX1272_MANID) {
		SEGGER_RTT_printf(0,"SX1272 MANID Check failed!\r\n");
		return false;
	}
	SEGGER_RTT_printf(0,"SX1272 MANID Check passed.\r\n");

	return true;
}


void sx1272_assert_cs() {
	spi_init(&sx1272_spi_config, NULL);
#ifndef BOARD_LORA_MODULE_REV1P0
	spi_csmux_select(LORA_CSN);
	nrf_gpio_pin_clear(CSN_ENN_PIN);
#else
	nrf_gpio_pin_set(LORA_CSN_PIN);
#endif
}

void sx1272_deassert_cs() {
#ifndef BOARD_LORA_MODULE_REV1P0
	nrf_gpio_pin_set(CSN_ENN_PIN);
#else
	nrf_gpio_pin_clear(LORA_CSN_PIN);
#endif

	spi_deinit();


}

