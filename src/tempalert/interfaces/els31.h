/*
 * els31.h
 *
 *  Created on: Dec 30, 2016
 *      Author: Nate-Tempalert
 */

#ifndef SRC_TEMPALERT_INTERFACES_ELS31_H_
#define SRC_TEMPALERT_INTERFACES_ELS31_H_

#include <stdint.h>
#include <stdbool.h>



bool els31_init(void);
bool els31_enablePower(void);
bool els31_disablePower(void);
bool els31_queryPower(bool *powerIsOn);
bool els31_modemIgnition(void);
bool els31_queryOn(bool *modemIsOn);
bool els31_emergencyReset(void);
#ifdef BOARD_CELL_NODE_REV1P0
bool els31_fastShutdown(void);
#endif
bool els31_readCTS(bool * on);

#endif /* SRC_TEMPALERT_INTERFACES_ELS31_H_ */
