/*
 * sx1272.h
 *
 *  Created on: Jan 10, 2017
 *      Author: Nate-Tempalert
 */

#ifndef SRC_TEMPALERT_INTERFACES_SX1272_H_
#define SRC_TEMPALERT_INTERFACES_SX1272_H_

#define SX1272_MANID			0x22

bool sx1272_init(void);
bool sx1272_setModeRX(void);
bool sx1272_setModeSleep(void);
bool sx1272_setModeTX(void);
bool sx1272_checkManID(void);
bool sx1272_queryMode(void);
bool sx1272_testDIO(void);
bool sx1272_disableRF(void);
bool sx1272_enableRF(void);
bool sx1272_reset(void);

#endif /* SRC_TEMPALERT_INTERFACES_SX1272_H_ */
