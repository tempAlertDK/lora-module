
#ifndef SRC_TEMPALERT_VERSION_H_
#define SRC_TEMPALERT_VERSION_H_


/**
 * Dump our version info to the log.
 */
extern void log_version(void);


#endif  // SRC_TEMPALERT_VERSION_H_