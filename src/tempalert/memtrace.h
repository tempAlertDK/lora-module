// Memory buffer for storing timed events.
//
// This is not thread-safe.

#pragma once

#include <stdint.h>

typedef enum {
	MEMTRACE_EMPTY = 0,
	MEMTRACE_EV_BEACON_TX_START,
	MEMTRACE_EV_BEACON_TX_END,
	MEMTRACE_EV_BEACON_RX_START,
	MEMTRACE_EV_BEACON_RX_END,
	MEMTRACE_EV_SENSOR_READ_START,
	MEMTRACE_EV_SENSOR_READ_DONE
} memtrace_event_t;

void memtrace_event0(memtrace_event_t ev);
void memtrace_event1(memtrace_event_t ev, uint8_t arg1);
void memtrace_event2(memtrace_event_t ev, uint8_t arg1, uint8_t arg2);

#define MEMTRACE_ENABLE 1

#ifdef MEMTRACE_ENABLE

#define MEMTRACE_EVENT0(event_name) memtrace_event2(event_name, 0, 0);
#define MEMTRACE_EVENT1(event_name, arg1) memtrace_event2(event_name, arg1, 0);
#define MEMTRACE_EVENT2(event_name, arg1, arg2) memtrace_event2(event_name, arg2);

#else

#define MEMTRACE_EVENT0(event_name)
#define MEMTRACE_EVENT1(event_name, arg1)
#define MEMTRACE_EVENT2(event_name, arg1, arg2)

#endif
