#ifndef SRC_TEMPALERT_CMDSFFILL_H_
#define SRC_TEMPALERT_CMDSFFILL_H_


/**
 * Handy debuggin/testing command.  Will fill the 
 * store/fwd buffer with some number of dummy readings.
 */
extern void cmdSFFill(const char * args);


#endif // SRC_TEMPALERT_CMDSFFILL_H_