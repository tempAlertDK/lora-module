/*
 * cmdTime.h
 *
 *  Created on: Oct 18, 2017
 *      Author: nreimens
 */

#ifndef SRC_TEMPALERT_COMMAND_CMDTIME_H_
#define SRC_TEMPALERT_COMMAND_CMDTIME_H_

/*
 * Command to manipulate unixtime
 * */
extern void cmdTime(const char * args);


#endif /* SRC_TEMPALERT_COMMAND_CMDTIME_H_ */
