/*
 * cmdInterval.h
 *
 *  Created on: Sep 18, 2017
 *      Author: nreimens
 */

#ifndef SRC_TEMPALERT_COMMAND_CMDINTERVALS_H_
#define SRC_TEMPALERT_COMMAND_CMDINTERVALS_H_

/*
 * Command to display the current intervals
 * 1. Measurement Interval
 * 2. Upload Interval
 * 3. Recovery Interval
 * */
extern void cmdIntervals(const char * args);

/*
 * Command to schedule a recovery interval
 * */
extern void cmdRecovery(const char * args);

/*
 * Command to set the measurement interval
 * */
extern void cmdMeasurementInterval (const char * args);

#endif /* SRC_TEMPALERT_COMMAND_CMDINTERVALS_H_ */
