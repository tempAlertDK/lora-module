/*
 * cmdSim.h
 *
 *  Created on: Sep 26, 2017
 *      Author: nreimens
 */

#ifndef SRC_TEMPALERT_COMMAND_CMDSIM_H_
#define SRC_TEMPALERT_COMMAND_CMDSIM_H_

extern void cmdSimLevel(const char * args);
extern void cmdSimBatt(const char * args);
extern void cmdSimTemp(const char * args);

#endif /* SRC_TEMPALERT_COMMAND_CMDSIM_H_ */
