/*
 * cmdInterval.c
 *
 *  Created on: Oct 12, 2017
 *      Author: nreimens
 */
#include <stdbool.h>
#include <stdint.h>
#include "log.h"
#include "scController.h"
#include "errors.h"
#include "errorCount.h"


void cmdErrorSet(unsigned int errCounter, unsigned int errCount) {
	if (errCounter >= ERRORCOUNT_TYPE_COUNT) {
		log_errorf("Error counter (%u) invalid!",errCounter);
	} else if (errCount > UINT8_MAX) {
		log_info("Count must be < 265");
	} else if (errorCount_setCount((uint8_t)errCounter,(uint8_t)errCount)) {
		log_infof("Error counter (%u) set to %u", errCounter, errCount);
	} else {
		log_error("Failed to set error counter!");
	}
}

void cmdErrorGet(unsigned int errCounter) {
	if (errCounter >= ERRORCOUNT_TYPE_COUNT) {
		log_errorf("Error counter (%u) invalid!",(unsigned int)errCounter);
	} else {
		log_infof("Error counter (%u) currently %u", errCounter,
				(unsigned int)errorCount_getCount((uint8_t)errCounter));
	}
}

void cmdErrorReset(unsigned int errCounter) {
	if (errCounter >= ERRORCOUNT_TYPE_COUNT) {
		log_errorf("Error counter (%u) invalid!",(unsigned int)errCounter);
	} else {
		errorCount_resetCount((uint8_t)errCounter);
		log_infof("Error counter (%u) reset", errCounter);
	}
}

void cmdErrorResetAll(void) {
	errorCount_resetCounts();
	log_info("All error counters reset");
}

/*
 * Command to manipulate error counters
 * */
static void log_cmdErrorCounts_usage()
{
    log_info("errors - set/reset/get error counters");
    log_info("");
    log_info("  usage: errors <set|reset|get|resetall> [errorCounter] [counts]");
    log_info("");
}
extern void cmdErrorCounts(const char * args) {

	unsigned int errorCounter, errorCount, numArgs;
	char command[10];

	numArgs = sscanf(args, "%s %u %u", command, &errorCounter, &errorCount);

	if ((numArgs == 0) || (numArgs > 3)) {
		log_cmdErrorCounts_usage();
		return;
	}

	if ((strcmp(command,"set") == 0) && (numArgs == 3)) {
		cmdErrorSet(errorCounter,errorCount);
	} else if ((strcmp(command,"get") == 0) && (numArgs == 2)) {
		cmdErrorGet(errorCounter);
	} else if ((strcmp(command,"reset") == 0) && (numArgs == 2)) {
		cmdErrorReset(errorCounter);
	} else if ((strcmp(command,"resetall") == 0) && (numArgs == 1)) {
		cmdErrorResetAll();
	} else {
		log_cmdErrorCounts_usage();
	}
}
