/*
 * cmdSim.c
 *
 *  Created on: Sep 26, 2017
 *      Author: nreimens
 */

#include <stdbool.h>
#include <stdint.h>
#include "log.h"
#include "scController.h"
#include "sensors.h"
#include "globals.h"
#include <math.h>

void cmdSimLevel(const char *args) {
	int lvl;
	unsigned int cap_fF;

	uint8_t numArgs = sscanf(args, "%i %u", &lvl, &cap_fF);

	if ((numArgs == 0) || (strlen(args) == 0)) {
		if (g_simLevel) {
			log_printf("Currently simulating a Level percentage of: %d%%", g_simLevelPercent);
			log_printf("Currently simulating a Capwand capacitance of: %ufF", g_simCapacitance);
		}
		return;
	} else if (numArgs == 1) {
		g_simCapacitance = 0;
	} else if (numArgs > 2) {
		return;
	}

	if (lvl < 0 || lvl > 100 || cap_fF > (1<<24)) {
		g_simLevel = false;
		g_simLevelPercent = 0;
		g_simCapacitance = 0;
		log_print("Halting Level simulation.");
		return;
	}


	log_printf("Simulating a Level percentage of: %d%%", lvl);
	log_printf("Simulating a Capwand capacitance of: %ufF", cap_fF);


	g_simLevel = true;
	g_simLevelPercent = (int8_t) lvl;
	g_simCapacitance = (uint32_t) cap_fF;

	if (sensors_read(0, SENSORS_READ)) {
		log_print("Reading Sensors...");
	}
}

void cmdSimBatt(const char *args) {
	unsigned int vbatt;

	if (sscanf(args, "%u", &vbatt) != 1) {
		return;
	}

	if ((vbatt > 10000) || (vbatt == 0)) {
		g_simBatt = false;
		log_print("Stopping battery simulation.");
		return;
	}

	log_printf("Simulating a battery voltage: %umV", vbatt);

	g_simBatt = true;
	g_simBattVoltage = (uint16_t) vbatt;

	if (sensors_read(0, SENSORS_READ)) {
		log_print("Reading Sensors...");
	}

}

void cmdSimTemp(const char *args) {
	int temp, numargs;
	char strcmd[10];

	/* test for string args */
	if (strlen(args) > 0 && strlen(args) <= sizeof(strcmd)) {
		numargs = sscanf(args, "%s", strcmd);
		if (numargs == 1) {
			if (strcmp(strcmd,"on") == 0) {
				g_simTemp = true;
			} else if (strcmp(strcmd,"off") == 0){
				g_simTemp = false;
				log_print("Disabled internal temperature simulation.");
				return;
			}
		} else {
			return;
		}
	}

	/* Parse for integer value */
	numargs = sscanf(args, "%i", &temp);

	if (numargs == 1) {
		g_simTemp10DegC = (int16_t) temp;
		g_simTemp = true;
	}

	int mult = 1;
	if (g_simTemp10DegC < 0) {
		mult = -1;
	}

	log_printf("Simulating a internal temperature of %d.%02d",
			(int) g_simTemp10DegC/10, (int)(mult)*(((g_simTemp10DegC-((g_simTemp10DegC/10)*10)))));
}

