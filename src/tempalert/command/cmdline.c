#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#include "SEGGER_RTT.h"

#include "commands.h"
#include "cmdline.h"

#define MAX_CMDSTR_LEN 128 // to handle longer modem at commands

static const cmdFcnPair_t *cmdTable;
static char cmdStr[MAX_CMDSTR_LEN] = "";

static void cmdline_thread(void *arg);
static void cmdline_loadCmds(const cmdFcnPair_t cmds[]);
static void cmdline_newChar(char c);
static bool cmdline_execCmd(const char *cmd);

static TaskHandle_t m_cmdline_thread_handle;
static StaticTask_t m_cmdline_thread_tcb;
#define CMDLINE_STACK_SIZE	512
StackType_t m_cmdline_thread_stack[CMDLINE_STACK_SIZE];

static bool m_debugging = true;

void cmdline_init() {
	cmdline_loadCmds(commands_getCommandTable());

	/* Start the command line thread */
	if ((m_cmdline_thread_handle = xTaskCreateStatic(cmdline_thread, "CMD", CMDLINE_STACK_SIZE, NULL, 1, m_cmdline_thread_stack, &m_cmdline_thread_tcb)) == NULL) {
			APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
}


void cmdline_thread(void *arg) {
	char c;

	printf("Command line parse started\r\n");

	while (1) {
		if (NRF_LOG_HAS_INPUT()) {
			NRF_LOG_READ_INPUT(&c);
			cmdline_newChar(c);
			if (!m_debugging) {
				printf("Input Enabled!\r\n");
				m_debugging = true;
			}
			taskYIELD();
			continue;
		}

		/* Wait before checking for another character */
		if (!m_debugging) {
			vTaskDelay(pdMS_TO_TICKS(10000));
		} else {
			vTaskDelay(pdMS_TO_TICKS(50));
		}
	}
}


void cmdline_loadCmds(const cmdFcnPair_t cmds[]) {
	cmdTable = cmds;
}


void cmdline_newChar(char c) {
	size_t cmdStrLen;
	
	cmdStrLen = strlen(cmdStr);
	
	if ((c == '\n') || (c == '\r')) {
		// If we received a CR or LF, attempt to execute whatever command
		// is in the buffer.
		cmdline_execCmd(cmdStr);
		// Effectively empty the command buffer after executing the command
		cmdStr[0] = '\0';
	} else if ((c < 32) || (c > 126)) {
		// If the character is outside of the printable range, return without
		// doing anything with it.
		return;
	} else if (cmdStrLen + 1 < MAX_CMDSTR_LEN) {
		// If there is still space in the buffer, add the new character to 
		// the end of it being sure to always maintain null termination.
		cmdStr[cmdStrLen+1] = '\0';
		cmdStr[cmdStrLen] = c;
	}
}

bool cmdline_execCmd(const char *cmd) {
	uint8_t i;
	const char *args;
	
	for (i=0; ; i++) {
		// Scan through all lines of the command table, comparing the received
		// command with the current line of the table.
		
		
		if (strlen(cmdTable[i].cmd) == 0) {
			// If we reach the last line in the command table without a match,
			// break out of the loop and return to the caller.
			return false;
		}
		
		// If the command string does not match the string at the current index
		// of the table, continue to the next index.
		if (strncmp(cmd, cmdTable[i].cmd, strlen(cmdTable[i].cmd)) != 0) {
			continue;
		}			
		
		if ((cmd[strlen(cmdTable[i].cmd)] != '\0') && (cmd[strlen(cmdTable[i].cmd)] != ' ')) {
			// If there is not a space after the command name, and if the 
			// command is not just the name, we declare the command invalid.
			// This means that "temperature" cannot be substituted for "temp".
			continue;	
		}		
		
		// If the function associated with the command is invalid, continue
		// scanning the list for the same command with a valid function pointer
		if (cmdTable[i].fcn == NULL) {
			continue;
		}		
		
		// Command arguments start just after the command itself
		args = &cmd[strlen(cmdTable[i].cmd)];
		
		// Execute the command and return true
		cmdTable[i].fcn(args);
		return true;
	}
	
	// If we did not find a match, return false.
	return false;
}

