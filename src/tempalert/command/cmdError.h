/*
 * cmdErrors.h
 *
 *  Created on: Oct 12, 2017
 *      Author: nreimens
 */

#ifndef SRC_TEMPALERT_COMMAND_CMDERROR_H_
#define SRC_TEMPALERT_COMMAND_CMDERROR_H_


/*
 * Command to manipulate error counters
 * */
extern void cmdErrorCounts(const char * args);


#endif /* SRC_TEMPALERT_COMMAND_CMDERROR_H_ */
