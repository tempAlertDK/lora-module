/* 
 * File:   cmdline.h
 * Author: iuliuv / kwgilpin
 *
 * Created on October 10, 2009, 6:13 PM
 */

#ifndef _CMDLINE_H
#define	_CMDLINE_H


typedef struct {
    char *cmd;
    void (*fcn)(const char *);
} cmdFcnPair_t;
     

void cmdline_init(void);

#endif	/* _CMDLINE_H */

