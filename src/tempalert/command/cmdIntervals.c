/*
 * cmdInterval.c
 *
 *  Created on: Sep 18, 2017
 *      Author: nreimens
 */
#include <stdbool.h>
#include <stdint.h>
#include "log.h"
#include "scController.h"
#include "sensors.h"

void cmdIntervals(const char * args) {

	uint32_t interval = 0;

	log_write("Sensor Measurement Interval: ");
	if (sensors_getMeasurementIntervalSeconds(&interval)) {
		log_write("%us\r\n",interval);
	} else {
		log_write("Inactive\r\n");
	}
	log_write("Core Upload Interval: ");
	if (scController_getUploadIntervalSecounds(&interval)) {
		log_write("%us\r\n",interval);
	} else {
		log_write("Inactive\r\n");
	}
	log_write("Recovery Upload Interval: ");
	if (scController_getRecoveryIntervalSecounds(&interval)) {
		log_write("%us\r\n",interval);
	} else {
		log_write("Inactive\r\n");
	}
	log_write("Next upload attempt in %u seconds\r\n", scController_getSecondsToNextUpload());

}

void cmdRecovery(const char * args)
{
	unsigned int mins;
	if (sscanf(args, "%u", &mins) == 1) {
		scController_scheduleRecoveryIntervalMins((uint32_t)mins);
	}
}

void cmdMeasurementInterval (const char * args) {
	unsigned int sec,currMi;
	if (sscanf(args, "%u", &sec) == 1) {
		if (sensors_setMeasurementInterval((uint32_t)sec)) {
			vTaskDelay(200);
			sensors_getMeasurementIntervalSeconds((uint32_t *)&currMi);
			log_infof("Measurement Interval set to %us",currMi);
		} else {
			log_error("Failed to update Measurement Interval!");
		}
	} else {
		sensors_getMeasurementIntervalSeconds((uint32_t *)&currMi);
		log_infof("Current Measurement Interval is %us",currMi);
	}
}
