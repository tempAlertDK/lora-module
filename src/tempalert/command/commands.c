/*
 * commands.c
 *
 *  Created on: Jul 14, 2017
 *      Author: kwgilpin
 */


#include <cmdError.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "SEGGER_RTT.h"

#include "freeRTOS.h"
#include "queue.h"
#include "globals.h"
#include "sensors.h"
#include "pca8561.h"

#include "modem.h"
#include "els31.h"
#include "els31uart.h"
#include "deviceID.h"

#include "scController.h"
#include "stoFwd.h"
#include "unixTime.h"

#include "loratest.h"
#include "fcc_bluetooth.h"

#include "ui.h"
#include "cmdline.h"
#include "commands.h"
#include "log.h"
#include "gitversion.h"
#include "cmdSFFill.h"
#include "cmdIntervals.h"
#include "cmdSim.h"
#include "cmdError.h"
#include "cmdTime.h"
#include "fcc_control.h"

void cmdHelp(const char* args);

void cmdSensors(const char *args) {
	if (sensors_read(0, SENSORS_READ)) {
		log_print("Reading Sensors...");
	}
}


void cmdTransmitMode(const char *args) {
	unsigned int mode;

	if (sscanf(args, "%u", &mode) != 1) {
		return;
	}

	switch (mode) {
	case 0:
		g_sc_transmit = false;
		log_print("Disabling transmissions to Core.");
		break;
	case 1:
		g_sc_transmit = true;
		log_print("Enabling transmissions to Core");
		break;
	case 2:
		g_sc_transmit = true;
		sensors_requestRead();
		scController_startUpload(UPLOAD_EVENT_TR_COMMAND);
		log_print("Requesting sensor read and upload");
		break;
	default:
		log_printf("ERROR: Invalid transmit mode (%u)", mode);
	}
}

void cmdOTAUpdate(const char *args) {
	unsigned int fw_maj, fw_min;

	if (sscanf(args, "%u.%u", &fw_maj, &fw_min) != 2) {
		return;
	}

	log_printf("User requested OTA to version %u.%u.", (uint8_t)fw_maj, (uint8_t)fw_min);
	scController_startOTA(fw_maj, fw_min);
}
#ifndef BOARD_LORA_MODULE_REV1P0
void cmdSFStatus(const char *args) {
	log_printf("S&F contains %u items in the Stack and %u items in the FIFO.",
			(unsigned int)stoFwd_countItemsStack(),(unsigned int)stoFwd_countItemsFIFO());
}
#endif

#if MODEM_POPULATED == 1
void cmdModemAT(const char *args) {
	char cmd[100];

	if (sscanf(args, "%98s", cmd) != 1) {
		return;
	}

	cmd[strlen(cmd)+1] = '\0';
	cmd[strlen(cmd)] = '\r';

	els31uart_sendATCmd((const uint8_t *)cmd, strlen(cmd), 1000);
}

void cmdModemOn(const char *args) {
	modem_queuePhyCommand(MODEM_PHY_TURN_ON);
}

void cmdModemOff(const char *args) {
	modem_queuePhyCommand(MODEM_PHY_TURN_OFF);
}

void cmdManualModemInit(const char *args) {
	if (els31_init()) {
		log_print("Initialized modem hardware successfully.");
	}
}

void cmdManualModemPower(const char *args) {
	if (els31_enablePower()) {
		if (!els31uart_wakeUart(portMAX_DELAY)) {
			log_printf("ERROR: Failed to wake els_uart![%s:%u]", __FUNCTION__, __LINE__);
		} else {
			log_print("Enabled modem power supply successfully.");
		}
	}
}

void cmdManualModemOn(const char *args) {
	bool modemIsOn;

	els31_enablePower();
	vTaskDelay(pdMS_TO_TICKS(100));

	els31_modemIgnition();
	vTaskDelay(pdMS_TO_TICKS(500));

	els31_queryOn(&modemIsOn);

	if (modemIsOn) {
		if (!els31uart_wakeUart(pdMS_TO_TICKS(1000))) {
			log_printf("ERROR: Failed to wake UART module![%s:%u]", __FUNCTION__, __LINE__);
		} else {
			log_print("Modem is booting...");
		}
	} else {
		log_print("ERROR: Failed to turn modem on");
		log_print("Removing power from modem");
		els31_disablePower();
	}
}

void cmdManualModemOff(const char *args) {
	if (els31_disablePower()) {
		if (!els31uart_sleepUart(portMAX_DELAY)) {
			log_printf("ERROR: Failed to sleep els_uart![%s:%u]", __FUNCTION__, __LINE__);
		} else {
			log_print("Disabled modem power supply successfully.");
		}
	}
}

void cmdManualModemIgnit(const char *args) {
	if (!els31_modemIgnition()) {
		log_printf("ERROR: Failed to send modem ignition signal![%s:%u]", __FUNCTION__, __LINE__);
	} else {
		log_print("Sent modem ignition signal successfully.");
	}
}

void cmdManualModemShutdown(const char *args) {
	if (!els31uart_sendATCmd((uint8_t *)"AT^SMSO\r", 8, 1000)) {
		log_printf("ERROR: Failed to send AT^SMSO shutdown command to modem![%s:%u]", __FUNCTION__, __LINE__);
	} else {
		log_print("Queued modem shutdown command successfully.");
	}
}


#ifdef BOARD_CELL_NODE_REV1P0
void cmdManualModemFastShutdown(const char *args) {
	if (els31_fastShutdown()) {
		log_print("Sent fast shutdown signal to modem successfully.");
	} else {
		log_printf("ERROR: Failed to send fast shutdown signal to modem![%s,%u]", __FUNCTION__, __LINE__);
	}
}
#endif

void cmdManualModemEmergencyReset(const char *args) {
	if (!els31_emergencyReset()) {
		log_printf("ERROR: Failed to send emergency reset signal to modem![%s:%u]", __FUNCTION__, __LINE__);
	} else {
		log_print("Sent emergency reset signal to modem successfully.");
	}
}

void cmdManualModemStatus(const char *args) {
	bool statusPin;
	if (els31_queryOn(&statusPin)) {
		if (statusPin) {
			log_print("Modem Status: ON");
		} else {
			log_print("Modem Status: OFF");
		}
	} else {
		log_printf("ERROR: Failed to query modem status![%s,%u]", __FUNCTION__, __LINE__);
	}
}
#endif
void cmdCCID(const char *args) {
	char id[DEVICE_ID_STRING_SIZE];
	if (deviceID_getDeviceIDString(id,sizeof(id))) {
		log_printf("Device ID: %s",id);
	} else {
		log_print("ERROR: Failed to obtain device ID!");
	}
}

void cmdRadioSetChannel(const char *args) {
	unsigned int u;
	radio_control_message_t rad_control_message;

	if (sscanf(args, "%u", &u) == 1){
		if(u >= RADIO_MIN_CHANNEL && u <= RADIO_MAX_CHANNEL){
			rad_control_message.updated_field = RADIO_CHANNEL;
			rad_control_message.updated_value = u;
			//radio_tx_carrier(RADIO_TXPOWER_TXPOWER_0dBm, RADIO_MODE_MODE_Nrf_2Mbit, u);
			xQueueSend(radio_controlQueue, &rad_control_message, pdMS_TO_TICKS(10));
		}
	}
}

void cmdRadioSetTest(const char *args) {
	radio_control_message_t rad_control_message;
	char testStr[10];

	rad_control_message.updated_field = RADIO_TEST;

	if (sscanf(args, "%s", testStr) == 1) {
		if (strncmp(testStr,"txc",3) == 0) {
			rad_control_message.updated_value = TX_CARRIER;
		} 
		else if (strncmp(testStr,"rxc",3) == 0) {
			rad_control_message.updated_value = RX_CARRIER;
		}
		else if (strncmp(testStr,"txm",3) == 0) {
			rad_control_message.updated_value = TX_MODULATED;
		} else { 
			log_printf("Argument %s not recognized", testStr);
			return;
		}

		xQueueSend(radio_controlQueue, &rad_control_message, pdMS_TO_TICKS(10));
	}
}

void cmdRadioSetBand(const char *args) {
	radio_control_message_t rad_control_message;
	char testStr[10];

	rad_control_message.updated_field = RADIO_BAND;

	if (sscanf(args, "%s", testStr) == 1) {
		if (strncmp(testStr,"low",3) == 0) {
			rad_control_message.updated_value = LOW_BAND;
		}
		else if (strncmp(testStr,"med",3) == 0) {
			rad_control_message.updated_value = MED_BAND;
		}
		else if (strncmp(testStr,"high",4) == 0) {
			rad_control_message.updated_value = HIGH_BAND;
		} else {
			log_printf("Argument %s not recognized", testStr);
			return;
		}

		xQueueSend(radio_controlQueue, &rad_control_message, pdMS_TO_TICKS(10));
	}
}

void cmdRadioSelect(const char *args) {
	radio_control_message_t rad_control_message;
	char testStr[10];

	rad_control_message.updated_field = RADIO_SELECT;

	if (sscanf(args, "%s", testStr) == 1) {
		if (strncmp(testStr,"lora",4) == 0) {
			rad_control_message.updated_value = SELECT_LORA;
		} 
		else if (strncmp(testStr,"bt",2) == 0) {
			rad_control_message.updated_value = SELECT_BT;
		}
		else if (strncmp(testStr,"both",4) == 0) {
			rad_control_message.updated_value = SELECT_BOTH;
		}
		else if ((strncmp(testStr,"none",4) == 0)) {
			rad_control_message.updated_value = SELECT_NONE;
		} else { 
			log_printf("Argument %s not recognized", testStr);
			return;
		}

		xQueueSend(radio_controlQueue, &rad_control_message, pdMS_TO_TICKS(10));
	}
}

void cmdLoRaTxTone(const char *args) {
	unsigned int u;

	if (sscanf(args, "%u", &u) == 1) {
		if ((u == 0) && loratest_stop_tone()) {
			log_print("Stopping LoRa tone");
		} else if ((u == 1) && loratest_start_tone()) {
			log_print("Starting LoRa tone");
		}
	}
}

void cmdLoRaTxToneFreq(const char *args) {
	unsigned int freq;

	if (sscanf(args, "%u", &freq) == 1) {
		if (loratest_set_freq(freq)) {
			log_printf("Setting LoRa tone frequency to %u Hz", freq);
		}
	}
}

void cmdLoRaTxTonePow(const char *args) {
	int tx_pow;

	if (sscanf(args, "%d", &tx_pow) == 1) {
		if (loratest_set_tx_pow(tx_pow)) {
			log_printf("Setting LoRa tone power to %d dBm", tx_pow);
		}
	}
}

void cmdLoRaTxTonePAEnable(const char *args) {
	unsigned int u;

	if (sscanf(args, "%u", &u) == 1) {
		if ((u == 0) && loratest_disable_pa()) {
			log_print("Disabling LoRa PA");
		} else if ((u == 1) && loratest_enable_pa()) {
			log_print("Enabling LoRa PA");
		}
	}
}

char * testStrings[27] = {""," ","0","1","2","3","4","5","6","7","8","9","-",
	"10","20","30","40","50","60","70","80","90","100",
"1--","  1"," - "};

void cmdDisplayTest(const char* args) {
	char testStr[10]; /* allow larger strings to test ui checks */
	uint8_t i;

	if (sscanf(args, "%s", testStr) == 1) {
		if (strncmp(testStr,"test",5) != 0) {
			ui_lcdUpdate(testStr);
		} else {
			for (i=0;i<26;i++) {
				ui_lcdUpdate(testStrings[i]);
				vTaskDelay(500);
			}
		}
	}
}

void cmdDebugLevel (const char* args) {
	unsigned int level;
	int nargs = sscanf(args, "%u", &level);
	if (nargs == 1) {
		if (log_setLevel((uint8_t)level)) {
			log_printf("LOG: Set log level to: %s",log_levelStrs[level]);
		}
	} else if (nargs < 1) {
		level = log_getLevel();
		log_printf("LOG: Log level is currently: %s",log_levelStrs[level]);
	}
}
#ifndef BOARD_LORA_MODULE_REV1P0
void cmdHist(const char* args) {
	char modeStr[2];
	unsigned int tempUInt = 1;
	uint32_t measurements, i, j, hr, min, sec, tsec;
	bool save = true;
	sf_record_t sfRecord;
	uint16_t dataLength;
	uint8_t *data;
	int32_t unixTime;

	if (sscanf(args, "%1s %u", modeStr, &tempUInt) >= 1) {
		if (modeStr[0] == 's') {
			save = true;
		} else if (modeStr[0] == 'd') {
			save = false;
		} else {
			log_print("Invalid mode character; expecting (s)ave or (d)iscard");
			return;
		}

		if (tempUInt == 0) {
			measurements = stoFwd_countItems();
		} else {
			measurements = (uint32_t)tempUInt;
			if (measurements > stoFwd_countItems()) {
				measurements = stoFwd_countItems();
			}
		}
	} else {
		log_print("You must specify whether to (s)ave or (d)iscard the measurement(s)");
		return;
	}

	if (measurements == 0) {
		log_print("Sto/fwd buffer is empty");
		return;
	}

	if (!unixTime_getSeconds(&unixTime)) {
		log_print("");
		log_print("Because the current Unix time is unknown, packets stored in the flash-based FIFO will not be accessible");
		log_print("You may set the Unix time with 'time s {seconds}' or request it from the server with 'time u'");
		log_print("");
	}

	for (i = 1; i <= measurements; i++) {
		taskYIELD();

		xEventGroupSetBits(sf_eventGroup,SF_EVENT_GROUP_OUTGOING_BIT);
		if (stoFwd_receiveRecord(&sfRecord, pdMS_TO_TICKS(500))) {
			if (sfRecord.header.type != SF_NULL_RECORD) {
				tsec = (get_time_100ms() - sfRecord.header.timeStamp) / 10UL;
				hr = tsec/3600;
				sec = tsec%3600;
				min = sec/60;
				sec = sec%60;
				log_print("");
				log_printf("Item %u of %u:", i, measurements);
				log_printf("  Age [hh:mm:ss]: %u:%02u:%02u",hr,min,sec);

				dataLength = sfRecord.header.dataLength;
				data = sfRecord.data;


				log_print("  Data: ");
				for (j = 0; j < dataLength; j++) {
					log_write("%02x",data[j]);
				}
				log_print("");

				if (save) {
					if (!stoFwd_sendRecord(&sfRecord, pdMS_TO_TICKS(1000))) {
						log_printf("ERROR: Failed to push sensor data back into S&F Stack queue [%s]!",__FUNCTION__);
					}
				}
			}
		}
	}
}
#endif

void cmdVersion(const char* args) {
	log_version();
}

cmdFcnPair_t cmdTable[] = {
	{"sensors", cmdSensors},

	{"simbatt", cmdSimBatt},
	{"simlevel", cmdSimLevel},
	{"simtemp", cmdSimTemp},

	{"tr", cmdTransmitMode},

	{"ota", cmdOTAUpdate},

	{"display", cmdDisplayTest},
#ifndef BOARD_LORA_MODULE_REV1P0
	{"sfstatus", cmdSFStatus},
	{"sffill", cmdSFFill },
	{"hist",cmdHist},
#endif
	{"time", cmdTime},
	{"debug",cmdDebugLevel},
	{"intervals",cmdIntervals},
	{"recovery",cmdRecovery},
	{"mi",cmdMeasurementInterval},
#if (MODEM_POPULATED == 1)
	{"at", cmdModemAT},
	{"mon", cmdModemOn},
	{"moff", cmdModemOff},

	{"mminit", cmdManualModemInit},
	{"mmpwr", cmdManualModemPower},
	{"mmon", cmdManualModemOn},
	{"mmoff", cmdManualModemOff},
	{"mmignit", cmdManualModemIgnit},
	{"mmshutdown", cmdManualModemShutdown},
#ifdef BOARD_CELL_NODE_REV1P0
	{"mmfastsd", cmdManualModemFastShutdown},
#endif
	{"mmereset", cmdManualModemEmergencyReset},
	{"mmstatus", cmdManualModemStatus},
#endif
	{"ccid", cmdCCID},
	{"v", cmdVersion},

	{"errors",cmdErrorCounts},

	{"loratxtone", cmdLoRaTxTone},
	{"loratxtonefreq", cmdLoRaTxToneFreq},
	{"loratxtonepow", cmdLoRaTxTonePow},
	{"loratxtonepaen", cmdLoRaTxTonePAEnable},
#ifdef FCC_TESTING
	{"radiosetchannel", cmdRadioSetChannel},
	{"radiosettest", cmdRadioSetTest},
	{"radioselect", cmdRadioSelect},
	{"radiosetband", cmdRadioSetBand},
#endif

	{"help",cmdHelp},
	{"?",cmdHelp},

	{"", NULL}
};

void cmdHelp(const char* args) {
	unsigned int i=0;

	log_info("help - list currently supported commands")

	while (strlen(cmdTable[i].cmd) != 0) {
		log_infof("\t%s",cmdTable[i].cmd);
		i++;
	}
	log_info("");

}

cmdFcnPair_t *commands_getCommandTable() {
	return cmdTable;
}
