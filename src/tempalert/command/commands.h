/*
 * commands.h
 *
 *  Created on: Jul 14, 2017
 *      Author: kwgilpin
 */

#ifndef SRC_TEMPALERT_COMMAND_COMMANDS_H_
#define SRC_TEMPALERT_COMMAND_COMMANDS_H_

#include "cmdline.h"

cmdFcnPair_t *commands_getCommandTable(void);

#endif /* SRC_TEMPALERT_COMMAND_COMMANDS_H_ */
