#include "log.h"
#include "sensors.h"

#define READING_DELAY_MS 500

static void log_sfstatus_usage()
{
    log_info("sffill - fill the store & forward buffer with dummy readings.");
    log_info("");
    log_info("  usage: sffill [reading count]");
    log_info("");
}

void cmdSFFill(const char *args)
{
    unsigned int count = 0;

    if (sscanf(args, "%u", &count) != 1)
    {
        log_sfstatus_usage();
        return;
    }

    log_infof("Taking %u readings, spaced %ums apart.", count, READING_DELAY_MS);

    for (int i = 0; i < count; i++)
    {
        sensors_read(pdMS_TO_TICKS(2000), SENSORS_READ);
        vTaskDelay(pdMS_TO_TICKS(READING_DELAY_MS));
    }
}