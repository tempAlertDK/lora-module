/*
 * cmdTime.c
 *
 *  Created on: Oct 18, 2017
 *      Author: nreimens
 */

#include <stdbool.h>
#include <stdint.h>
#include "log.h"
#include "scController.h"
#include "unixTime.h"


void cmdSetTime(int sec) {
	if (pdFALSE == xSemaphoreTake(unixTimeMutex, pdMS_TO_TICKS(500))) {
		log_error("Failed to take mutex to set Unix time!");
		return;
	}
	if (!unixTime_setSeconds(sec)) {
		log_error("Failed to set Unix time!");
	}
	xSemaphoreGive(unixTimeMutex);
}

void cmdTriggerUnixtimeUpdate() {
	if (unixTime_triggerTimeUpdate()) {
		log_info("Unix time update timer triggered, requesting update on with next upload");
	} else {
		log_error("Failed to trigger Unix time update timer");
	}
}

void cmdPrintTime(void) {
	int32_t sec;

	log_printf("Local time is: %u seconds",(xTaskGetTickCount() / pdMS_TO_TICKS(1000)));
	if (unixTime_getSeconds(&sec)) {
		log_printf("Unix time is: %d seconds.", sec);
		if (unixTime_getSecondsUntillUpdate(&sec)) {
			log_printf("   update request in: %d seconds.", sec);
		}
	} else {
		log_print("Unix time is invalid!");
	}
}

static void log_cmdTime_usage()
{
    log_info("time - set/reset/update current [Unix] time");
    log_info("");
    log_info("  usage: time [<set|clear|update>] [seconds]");
    log_info("");
}
extern void cmdTime(const char * args) {

	int value, numArgs;
	char command[10];

	numArgs = sscanf(args, "%s %u", command, &value);

	if ((numArgs > 2)) {
		log_cmdTime_usage();
		return;
	}

	if ((strcmp(command,"set") == 0) && (numArgs == 2)) {
		cmdSetTime(value);
	} else if ((strcmp(command,"clear") == 0) && (numArgs == 1)) {
		if (!unixTime_isValid()) {
			log_error("Unix time already invalid");
		} else if (pdTRUE == xSemaphoreTake(unixTimeMutex, pdMS_TO_TICKS(500))) {
			if (unixTime_invalidate()) {
				log_info("Unix time cleared");
			} else {
				log_error("Failed to clear Unix time!");
			}
			xSemaphoreGive(unixTimeMutex);
		} else {
			log_error("Failed to take mutex to reset Unix time!");
		}
	} else if ((strcmp(command,"update") == 0) && (numArgs == 1)) {
		if (!unixTime_isValid()) {
			log_error("Unix time invalid");
		} else {
			cmdTriggerUnixtimeUpdate();
		}
	} else if (numArgs == -1) {
		cmdPrintTime();
	} else {
		log_cmdTime_usage();
	}
}
