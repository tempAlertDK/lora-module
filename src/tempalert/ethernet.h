/*
 * ethernet.h
 *
 *  Created on: Jul 2, 2017
 *      Author: kwgilpin
 */

#ifndef SRC_TEMPALERT_ETHERNET_H_
#define SRC_TEMPALERT_ETHERNET_H_

#include <stdbool.h>

void ethernet_init(void);
void ethernet_off(void);
void ethernet_on(void);
bool ethernet_getState(void);

#endif /* SRC_TEMPALERT_ETHERNET_H_ */
