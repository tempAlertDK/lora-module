/*
 * spi.c
 *
 *  Created on: Feb 9, 2017
 *      Author: Nate-Tempalert
 */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "nordic_common.h"
#include "nrf.h"
#include "nrf_delay.h"
#include "nrf_drv_spi.h"

#include "app_util_platform.h"
#include "app_error.h"


#include "FreeRTOS.h"
#include "task.h"

#include "SEGGER_RTT.h"

#include "boards.h"
#include "spi.h"

#include "log.h"

static nrf_drv_spi_t m_spi_instance = NRF_DRV_SPI_INSTANCE(1);


bool spi_csmux_init(void) {
	/* Initialize select lines */
#ifndef BOARD_LORA_MODULE_REV1P0
	nrf_gpio_pin_dir_set(CSN_SEL0_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
	nrf_gpio_pin_set(CSN_SEL0_PIN);

	nrf_gpio_pin_dir_set(CSN_SEL1_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
	nrf_gpio_pin_set(CSN_SEL1_PIN);

	nrf_gpio_pin_dir_set(CSN_SEL2_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
	nrf_gpio_pin_set(CSN_SEL2_PIN);

	/* Initialize the enable line */
	nrf_gpio_pin_set(CSN_ENN_PIN);
	nrf_gpio_cfg_output(CSN_ENN_PIN);
#endif
	return true;
}


bool spi_csmux_select(spi_cs_t chipselect) {
#ifndef BOARD_LORA_MODULE_REV1P0
	switch (chipselect) {

#if defined(BOARD_CELL_NODE_REV1P0)
	case EXPANSION_CSN:
		nrf_gpio_pin_clear(CSN_SEL0_PIN);
		nrf_gpio_pin_clear(CSN_SEL1_PIN);
		nrf_gpio_pin_clear(CSN_SEL2_PIN);
		break;
#elif defined(BOARD_CELL_NODE_REV1P1)
	case FLASH_RESETN_CSN:
		nrf_gpio_pin_clear(CSN_SEL0_PIN);
		nrf_gpio_pin_clear(CSN_SEL1_PIN);
		nrf_gpio_pin_clear(CSN_SEL2_PIN);
		break;
#endif

	case EPD_CSN:
		nrf_gpio_pin_set(CSN_SEL0_PIN);
		nrf_gpio_pin_clear(CSN_SEL1_PIN);
		nrf_gpio_pin_clear(CSN_SEL2_PIN);
		break;

	case LORA_CSN:
		nrf_gpio_pin_clear(CSN_SEL0_PIN);
		nrf_gpio_pin_set(CSN_SEL1_PIN);
		nrf_gpio_pin_clear(CSN_SEL2_PIN);
		break;

#if defined(BOARD_CELL_NODE_REV1P1)
	case EXPANSION_CSN:
		nrf_gpio_pin_set(CSN_SEL0_PIN);
		nrf_gpio_pin_set(CSN_SEL1_PIN);
		nrf_gpio_pin_clear(CSN_SEL2_PIN);
		break;
#endif

#if defined(BOARD_CELL_NODE_REV1P0)
	case ADC_CSN:
		nrf_gpio_pin_clear(CSN_SEL0_PIN);
		nrf_gpio_pin_clear(CSN_SEL1_PIN);
		nrf_gpio_pin_set(CSN_SEL2_PIN);
		break;
#elif defined(BOARD_CELL_NODE_REV1P1)
	case GPIO_EXPANDER_RESETN_CSN:
		nrf_gpio_pin_set(CSN_SEL0_PIN);
		nrf_gpio_pin_set(CSN_SEL1_PIN);
		nrf_gpio_pin_clear(CSN_SEL2_PIN);
		break;
#endif

	case ETHERNET_CSN:
		nrf_gpio_pin_set(CSN_SEL0_PIN);
		nrf_gpio_pin_clear(CSN_SEL1_PIN);
		nrf_gpio_pin_set(CSN_SEL2_PIN);
		break;

	case FLASH_CSN:
		nrf_gpio_pin_clear(CSN_SEL0_PIN);
		nrf_gpio_pin_set(CSN_SEL1_PIN);
		nrf_gpio_pin_set(CSN_SEL2_PIN);
		break;

	case EXPANSION_RESETN_CSN:
		nrf_gpio_pin_set(CSN_SEL0_PIN);
		nrf_gpio_pin_set(CSN_SEL1_PIN);
		nrf_gpio_pin_set(CSN_SEL2_PIN);
		break;

	default:
		printf("SPI ERROR: %s task - unhandled spi_cs_t value (%d)\r\n", pcTaskGetName(NULL), chipselect);
		return false;
	}
#else
	nrf_gpio_pin_set(LORA_CSN_PIN);
#endif
	return true;
}


bool spi_init(const nrf_drv_spi_config_t *spi_config, nrf_drv_spi_handler_t handler) {
	ret_code_t err_code = nrf_drv_spi_init(&m_spi_instance, spi_config, handler);

	if (err_code == NRF_ERROR_INVALID_STATE) {
#if defined(SPI_DEBUG) && (SPI_DEBUG >= 1)
		log_errorf("SPI: peripheral initialization failed (already initialized) in %s task\r\n", pcTaskGetName(NULL));
#endif
		return false;
	} else if (err_code != NRF_SUCCESS) {
#if defined(SPI_DEBUG) && (SPI_DEBUG >= 1)
		log_errorf("SPI: peripheral initialization failed (code: %d) in %s task\r\n", err_code, pcTaskGetName(NULL));
#endif
		return false;
	} else {
		return true;
	}
}


bool spi_deinit(void) {
	nrf_drv_spi_uninit(&m_spi_instance);

	/* Drive the SCLK and MOSI pins to ground so that they do not float nor
	 * source current to sleeping devices on the bus. */
	nrf_gpio_pin_clear(SCLK_PIN);
	nrf_gpio_cfg_output(SCLK_PIN);

	nrf_gpio_pin_clear(MOSI_PIN);
	nrf_gpio_cfg_output(MOSI_PIN);

	/* To be safe, we do not want to drive MISO to ground, but we do add a
	 * pull-down so that it does not float. */
	nrf_gpio_cfg_input(MISO_PIN, NRF_GPIO_PIN_PULLDOWN);

#ifdef BOARD_LORA_MODULE_REV1P0
	nrf_gpio_pin_dir_set(LORA_CSN_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
	nrf_gpio_pin_set(LORA_CSN_PIN);
#endif
	/* Note: the nrf_drv_spi_init() function called by spi_init() will always
	 * configure the SPI pins correctly for a transaction. */

	return true;
}

bool spi_transfer(const uint8_t * p_tx_data, uint8_t tx_buffer_len, uint8_t * p_rx_data, uint8_t rx_buffer_len) {
	if (nrf_drv_spi_getinit(&m_spi_instance) == false) {
#if defined(SPI_DEBUG) && (SPI_DEBUG >= 1)
		log_errorf("SPI: transfer aborted because peripheral not initialized in %s task\r\n", pcTaskGetName(NULL));
#endif
		return false;
	}

	ret_code_t ret = nrf_drv_spi_transfer(&m_spi_instance, p_tx_data, tx_buffer_len, p_rx_data, rx_buffer_len);

	if (ret != NRF_SUCCESS) {
#if defined(SPI_DEBUG) && (SPI_DEBUG >= 1)
		log_errorf("SPI: transfer failed in %s task\r\n", pcTaskGetName(NULL));
#endif
		return false;
	}

	return true;

}
