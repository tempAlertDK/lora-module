/*
 * ble_levelSensor.h
 *
 *  Created on: Jul 3, 2014
 *      Author: kwgilpin
 */

#ifndef BLE_LEVELSENSOR_H_
#define BLE_LEVELSENSOR_H_

#include <stdint.h>
#include <stdbool.h>

#include "ble.h"
#include "ble_srv_common.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "capwand.h"

#define LEVELSENSOR_UUID_SERVUCE_BASE					{{0xEE, 0xA1, 0xB9, 0xE3, 0x55, 0xEA, 0xB6, 0x20, 0xEE, 0xEF, 0xD8, 0x82, 0x00, 0x00, 0x60, 0x6D}}
#define LEVELSENSOR_UUID_SERVICE						0x1A00

#define LEVELSENSOR_UUID_CALIBRATION_CHAR				0x1A01

#define LEVELSENSOR_UUID_LEVEL_CHAR						0x1A02

#define LEVELSENSOR_UUID_TEMPERATURE_CHAR				0x1A03

#define LEVELSENSOR_UUID_MEASUREMENT_INTERVAL_CHAR		0x1A04

#define LEVELSENSOR_UUID_SENSOR_SERIAL_CHAR				0x1A05

#define LEVELSENSOR_UUID_BATTERY_VOLTAGE_CHAR			0x1A06

#define LEVELSENSOR_UUID_CAPACITANCE_CHAR               0x1A07

#define LEVELSENSOR_UUID_CALIBRATION_DATA0_CHAR         0x1A08
#define LEVELSENSOR_UUID_CALIBRATION_DATA1_CHAR         0x1A09

/* Forward declaration of the ble_levelSensor_t type. */
typedef struct ble_levelSensorService_s ble_levelSensorService_t;

extern ble_levelSensorService_t m_levelSensorService;

/* Level monitoring service initialization structure. This contains all options and
 * data needed to initialize the service. */
typedef struct {
	uint8_t *p_data;
	uint8_t measurementInterval_min;
	uint8_t sensorSerial[8];
	int8_t calibration;
    uint32_t capacitance;
    uint8_t calibrationData0[3];
    uint8_t calibrationData1[3];
} ble_levelSensorService_init_t;

extern QueueHandle_t xBleLevelSensorQueue;

/* Level monitoring service structure. This contains various status information for
 * the service. */
typedef struct ble_levelSensorService_s
{
    uint16_t					service_handle;
    uint8_t						service_uuid_type;

    ble_gatts_char_handles_t 	batteryVoltage_char_handles;

    ble_gatts_char_handles_t	calibration_char_handles;

    ble_gatts_char_handles_t	calibrationData0_char_handles;
    ble_gatts_char_handles_t	calibrationData1_char_handles;

    ble_gatts_char_handles_t	temperature_char_handles;

    ble_gatts_char_handles_t    level_char_handles;
    ble_gatts_char_handles_t    capacitance_char_handles;

    ble_gatts_char_handles_t	sensorSerial_char_handles;

    ble_gatts_char_handles_t	measurementInterval_char_handles;

    uint16_t					conn_handle;
    int8_t						lastRSSI;
    uint8_t						sensorSerial[8];
    int8_t						calibration;
    uint8_t                     calibrationData0[3];  // N points [1 byte int level, 2 bytes uint capacitance]
    uint8_t                     calibrationData1[3];  // N points [1 byte int level, 2 bytes uint capacitance]
    int8_t						level;
    uint32_t                    capacitance;
    int16_t						temperature;
} ble_levelSensorService_t;


uint32_t ble_levelSensorService_init(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init);
void ble_levelSensorService_on_ble_evt(ble_levelSensorService_t *p_levelSensorService, ble_evt_t *p_ble_evt);

bool ble_levelSensorService_indicateLevel(const ble_levelSensorService_t *p_levelSensorService, int8_t p_level);
bool ble_levelSensorService_indicateCapacitance(const ble_levelSensorService_t *p_levelSensorService, uint32_t p_cap);

#endif /* BLE_LEVELSENSOR_H_ */
