/*
 * ble_levelSensor.c
 *
 *  Created on: Jul 3, 2014
 *      Author: kwgilpin
 */

#include "ble_levelSensor.h"

#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "nrf52.h"

#include "nordic_common.h"
#include "ble_srv_common.h"
#include "ble_advertising.h"

#include "app_util.h"
#include "app_error.h"
#include "app_timer.h"

#include "SEGGER_RTT.h"

#include "log.h"
#include "sensors.h"
#include "stoFwdFifo.h"
#include "si705x.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "portmacro_cmsis.h"
#include "event_groups.h"
#include "deviceID.h"

#define DEBUG_BLE	1

ble_levelSensorService_t m_levelSensorService;

static void on_connect(ble_levelSensorService_t *p_levelSensorService, ble_evt_t *p_ble_evt);
static void on_disconnect(ble_levelSensorService_t *p_levelSensorService, ble_evt_t *p_ble_evt);
static void on_rw_authorize(ble_levelSensorService_t *p_levelSensorService, ble_evt_t *p_ble_evt);
static void on_write(ble_levelSensorService_t *p_levelSensorService, ble_evt_t *p_ble_evt);

//static uint32_t measurementInterval_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init);
//static uint32_t batteryVoltage_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init);
//static uint32_t sensorSerial_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init);
//static uint32_t temperature_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init);
//static uint32_t calibration_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init);
static uint32_t level_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init);
static uint32_t capacitance_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init);
static uint32_t calibrationData0_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init);
static uint32_t calibrationData1_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init);


/* Container for storing current sensor data */
sensorData_t ble_mostRecentMeasurement;
statusData_t ble_mostRecentStatus;

QueueHandle_t xBleLevelSensorQueue;

/**@brief Connect event handler.
 *
 * @param[in]   p_lbs       Serial Port Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
void on_connect(ble_levelSensorService_t *p_levelSensorService, ble_evt_t *p_ble_evt) {
    uint32_t err_code;

#if (DEBUG_BLE == 1)
    log_info("ON CONNECT");
#endif

	p_levelSensorService->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
	p_levelSensorService->lastRSSI = 0;

	//1.0 - err_code = sd_ble_gap_rssi_start(p_levelSensorService->conn_handle);
	err_code = sd_ble_gap_rssi_start(p_levelSensorService->conn_handle, 0, 0);
	APP_ERROR_CHECK(err_code);
}


/**@brief Disconnect event handler.
 *
 * @param[in]   p_lbs       Serial Port Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
void on_disconnect(ble_levelSensorService_t *p_levelSensorService, ble_evt_t *p_ble_evt) {
    UNUSED_PARAMETER(p_ble_evt);

#if (DEBUG_BLE == 1)
    log_info("ON DISCONNECT");
#endif

    p_levelSensorService->conn_handle = BLE_CONN_HANDLE_INVALID;
}

void on_rssi_changed(ble_levelSensorService_t *p_levelSensorService, ble_evt_t *p_ble_evt) {
#if (DEBUG_BLE == 1)
	log_info("ON RSSI CHANGED");
#endif

	p_levelSensorService->lastRSSI = p_ble_evt->evt.gap_evt.params.rssi_changed.rssi;

	sd_ble_gap_rssi_stop(p_levelSensorService->conn_handle);
}

uint16_t m_rw_authorize_conn_handle;


void on_rw_authorize(ble_levelSensorService_t *p_levelSensorService, ble_evt_t *p_ble_evt) {
	uint32_t err_code;
	bool success = true;

	ble_gatts_evt_rw_authorize_request_t *authorize_request;
	ble_gatts_rw_authorize_reply_params_t authorize_reply_params;

#if (DEBUG_BLE == 1)
	log_info("ON RW AUTHORIZE");
#endif

	authorize_request = &p_ble_evt->evt.gatts_evt.params.authorize_request;

	memset(&authorize_reply_params, 0, sizeof(authorize_reply_params));

    /* Measurement Interval */

	if ((authorize_request->type == BLE_GATTS_AUTHORIZE_TYPE_WRITE) &&
			(authorize_request->request.write.handle == p_levelSensorService->measurementInterval_char_handles.value_handle)) {
		/* If the client is attempting to write to the measurement interval
		 * characteristic, we must verify that the new interval is valid before
		 * giving permission for the write. */

        sensors_control_message_t ble_lvl_evt;

        ble_lvl_evt.type = SENSORS_EVENT_MI_UPDATE;
        ble_lvl_evt.data._ui32 = authorize_request->request.write.data[0];;

        if (success) {
            success = xQueueSend(sensors_controlQueue, &ble_lvl_evt, pdMS_TO_TICKS(10));
            if (success) {
            	success = (xEventGroupSetBits(xSensorsEventGroup,
            			SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT) & SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT);
            }
        }

		if (success) {
			//xTaskAbortDelay(m_sensors_thread); /* unblock to allow update */
			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
			authorize_reply_params.params.write.gatt_status = BLE_GATT_STATUS_SUCCESS;
			authorize_reply_params.params.write.update = 1;
			authorize_reply_params.params.write.len = authorize_request->request.write.len;
			authorize_reply_params.params.write.p_data = authorize_request->request.write.data;
		} else {
			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
			authorize_reply_params.params.write.gatt_status = BLE_GATT_STATUS_ATTERR_WRITE_NOT_PERMITTED;
		}

		err_code = sd_ble_gatts_rw_authorize_reply(p_levelSensorService->conn_handle, &authorize_reply_params);
		APP_ERROR_CHECK(err_code);
	}

	/* Sensor Serial Number */

	if ((authorize_request->type == BLE_GATTS_AUTHORIZE_TYPE_READ) &&
			(authorize_request->request.read.handle == p_levelSensorService->sensorSerial_char_handles.value_handle)) {

		/* Save the authorization connection handle so that we can provide
		 * authorization in the callback after the battery reading is
		 * complete. */
		m_rw_authorize_conn_handle = p_levelSensorService->conn_handle;

        uint8_t serial[8];
    
        deviceID_getSerialNumberArray(serial);
		
        SEGGER_RTT_printf(0, "SERIAL NUMBER:");
        uint8_t i;
        for (i=0;i<8;i++)
             SEGGER_RTT_printf(0, "%02x",serial[i]);
        SEGGER_RTT_printf(0, "\r\n");

        authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
        authorize_reply_params.params.read.gatt_status = BLE_GATT_STATUS_SUCCESS;
        authorize_reply_params.params.read.update = 1;
        authorize_reply_params.params.read.len = 8;
        authorize_reply_params.params.read.offset = 0;
        authorize_reply_params.params.read.p_data = (uint8_t *)serial;

		err_code = sd_ble_gatts_rw_authorize_reply(p_levelSensorService->conn_handle, &authorize_reply_params);
		APP_ERROR_CHECK(err_code);

	}


	/* Battery voltage */

	if ((authorize_request->type == BLE_GATTS_AUTHORIZE_TYPE_READ) &&
			(authorize_request->request.read.handle == p_levelSensorService->batteryVoltage_char_handles.value_handle)) {

		/* Save the authorization connection handle so that we can provide
		 * authorization in the callback after the battery reading is
		 * complete. */
		m_rw_authorize_conn_handle = p_levelSensorService->conn_handle;

		if (	sensors_getRecentBattery(&ble_mostRecentStatus.batteryVoltage_mV) &&
				(xSemaphoreTake(g_recentSensorData_smphr,10) == pdPASS)) {
			/* Give back semaphore now */
			xSemaphoreGive(g_recentSensorData_smphr);

			authorize_reply_params.params.read.gatt_status = BLE_GATT_STATUS_SUCCESS;
			authorize_reply_params.params.read.update = 1;
			authorize_reply_params.params.read.len = 2;
			authorize_reply_params.params.read.offset = 0;
			authorize_reply_params.params.read.p_data = (uint8_t *)&ble_mostRecentStatus.batteryVoltage_mV;

		} else {
			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
			authorize_reply_params.params.read.gatt_status = BLE_GATT_STATUS_ATTERR_READ_NOT_PERMITTED;
		}
		err_code = sd_ble_gatts_rw_authorize_reply(p_levelSensorService->conn_handle, &authorize_reply_params);
		APP_ERROR_CHECK(err_code);

	}

	/* Calibration Char */

	if ((authorize_request->type == BLE_GATTS_AUTHORIZE_TYPE_WRITE) &&
			(authorize_request->request.write.handle == p_levelSensorService->calibration_char_handles.value_handle)) {
        uint8_t cal_idx = 255;
        int8_t cal_lvl = -2;
		/* User write data */
        bool success = ((authorize_request->request.write.len == 1) ||
                            (authorize_request->request.write.len == 2));

        if (success) {
            cal_lvl = (int8_t)(authorize_request->request.write.data[0]);
            /* if read length is 2 then the second byte is the cal index */
            if (authorize_request->request.write.len == 2) {
                cal_idx = (int8_t)(authorize_request->request.write.data[1]);
            } else {
                /* Otherwise assume this is an empty calibration */
                cal_idx = 0;
            }
        }

		success = 	((cal_lvl >= -1) && (cal_lvl <= 100)) && (sensors_controlQueue !=0);

        sensors_control_message_t ble_lvl_evt;

        if (success) {
            ble_lvl_evt.type = SENSORS_EVENT_CALIBRATE_LEVEL;
            ble_lvl_evt.data.a_ui8[0] = cal_idx;
            ble_lvl_evt.data.a_ui8[1] = cal_lvl;
        }

        if (success) {
			success = xQueueSend(sensors_controlQueue, &ble_lvl_evt, pdMS_TO_TICKS(10));
			if (success) {
				success = (xEventGroupSetBits(xSensorsEventGroup,
						SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT) & SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT);
			}
		}
        
		if (success) {
			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
			authorize_reply_params.params.write.gatt_status = BLE_GATT_STATUS_SUCCESS;
			authorize_reply_params.params.write.update = 1;
			authorize_reply_params.params.write.len = authorize_request->request.write.len;
			authorize_reply_params.params.write.p_data = authorize_request->request.write.data;
		} else {
			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
			authorize_reply_params.params.write.gatt_status = BLE_GATT_STATUS_ATTERR_WRITE_NOT_PERMITTED;
		}

		err_code = sd_ble_gatts_rw_authorize_reply(p_levelSensorService->conn_handle, &authorize_reply_params);
		APP_ERROR_CHECK(err_code);

	}

	/* Temperature */

	if ((authorize_request->type == BLE_GATTS_AUTHORIZE_TYPE_READ) &&
			(authorize_request->request.read.handle == p_levelSensorService->temperature_char_handles.value_handle)) {

		/* Save the authorization connection handle so that we can provide
		 * authorization in the callback after the battery reading is
		 * complete. */
		m_rw_authorize_conn_handle = p_levelSensorService->conn_handle;

		authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_READ;

		if (xSemaphoreTake(g_recentSensorData_smphr,10) == pdPASS) {
			/* Copy into local struct */
			ble_mostRecentMeasurement.temperature_tenthDegC = sensors_getRecentTemp();
			/* Give back semaphore now */
			xSemaphoreGive(g_recentSensorData_smphr);

			authorize_reply_params.params.read.gatt_status = BLE_GATT_STATUS_SUCCESS;
			authorize_reply_params.params.read.update = 1;
			authorize_reply_params.params.read.len = 2;
			authorize_reply_params.params.read.offset = 0;
			authorize_reply_params.params.read.p_data = (uint8_t *)&ble_mostRecentMeasurement.temperature_tenthDegC;

		} else {
			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
			authorize_reply_params.params.read.gatt_status = BLE_GATT_STATUS_ATTERR_READ_NOT_PERMITTED;
		}
		err_code = sd_ble_gatts_rw_authorize_reply(p_levelSensorService->conn_handle, &authorize_reply_params);
		APP_ERROR_CHECK(err_code);

	}

	/* Current level */

	if ((authorize_request->type == BLE_GATTS_AUTHORIZE_TYPE_READ) &&
			(authorize_request->request.read.handle == p_levelSensorService->level_char_handles.value_handle)) {

		/* Save the authorization connection handle so that we can provide
		 * authorization in the callback after the battery reading is
		 * complete. */
		m_rw_authorize_conn_handle = p_levelSensorService->conn_handle;

		authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_READ;

        sensors_control_message_t ble_lvl_evt;
        ble_lvl_evt.responseQueue = NULL;
        ble_lvl_evt.type = SENSORS_EVENT_READ_NOW;
        /* Send a read request to be handled ASAP. The device will notify on completion */
        if (success) {
			success = xQueueSend(sensors_controlQueue, &ble_lvl_evt, pdMS_TO_TICKS(10));
			if (success) {
				success = (xEventGroupSetBits(xSensorsEventGroup,
						SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT) & SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT);
			}
		}


		if (xSemaphoreTake(g_recentSensorData_smphr, 10) == pdPASS) {
			/* Copy into local struct */
			ble_mostRecentMeasurement.level_percentage = sensors_getRecentLevel();
			/* Give back semaphore now */
			xSemaphoreGive(g_recentSensorData_smphr);

			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
			authorize_reply_params.params.read.gatt_status = BLE_GATT_STATUS_SUCCESS;
			authorize_reply_params.params.read.update = 1;
			authorize_reply_params.params.read.len = 1;
			authorize_reply_params.params.read.offset = 0;
			authorize_reply_params.params.read.p_data = (uint8_t *)&(ble_mostRecentMeasurement.level_percentage);
		} else {
			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
			authorize_reply_params.params.read.gatt_status = BLE_GATT_STATUS_ATTERR_READ_NOT_PERMITTED;
		}
		err_code = sd_ble_gatts_rw_authorize_reply(p_levelSensorService->conn_handle, &authorize_reply_params);
		APP_ERROR_CHECK(err_code);
	}

	/* Current Capacitance */

	if ((authorize_request->type == BLE_GATTS_AUTHORIZE_TYPE_READ) &&
			(authorize_request->request.read.handle == p_levelSensorService->capacitance_char_handles.value_handle)) {

		/* Save the authorization connection handle so that we can provide
		 * authorization in the callback after the battery reading is
		 * complete. */
		m_rw_authorize_conn_handle = p_levelSensorService->conn_handle;

		authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_READ;

		if (xSemaphoreTake(g_recentSensorData_smphr, 10) == pdPASS) {
			/* Copy into local struct */
			ble_mostRecentMeasurement.capacitance = sensors_getRecentCapacitance()/10;
			/* Give back semaphore now */
			xSemaphoreGive(g_recentSensorData_smphr);

			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
			authorize_reply_params.params.read.gatt_status = BLE_GATT_STATUS_SUCCESS;
			authorize_reply_params.params.read.update = 1;
			authorize_reply_params.params.read.len = 4;
			authorize_reply_params.params.read.offset = 0;
			authorize_reply_params.params.read.p_data = (uint8_t *)(&ble_mostRecentMeasurement.capacitance);
		} else {
			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
			authorize_reply_params.params.read.gatt_status = BLE_GATT_STATUS_ATTERR_READ_NOT_PERMITTED;
		}
		err_code = sd_ble_gatts_rw_authorize_reply(p_levelSensorService->conn_handle, &authorize_reply_params);
		APP_ERROR_CHECK(err_code);
	}

    /* Calibration Data Point 0 */

	if ((authorize_request->type == BLE_GATTS_AUTHORIZE_TYPE_READ) &&
			(authorize_request->request.read.handle == p_levelSensorService->calibrationData0_char_handles.value_handle)) {

		/* Save the authorization connection handle so that we can provide
		 * authorization in the callback after the battery reading is
		 * complete. */
		m_rw_authorize_conn_handle = p_levelSensorService->conn_handle;

		authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_READ;

		if (xSemaphoreTake(g_recentSensorData_smphr, 10) == pdPASS) {
			/* Copy into local struct */
			sensors_getCalibrationDataPoint(0,ble_mostRecentMeasurement.calibrationData0);
			/* Give back semaphore now */
			xSemaphoreGive(g_recentSensorData_smphr);

			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
			authorize_reply_params.params.read.gatt_status = BLE_GATT_STATUS_SUCCESS;
			authorize_reply_params.params.read.update = 1;
			authorize_reply_params.params.read.len = 3;
			authorize_reply_params.params.read.offset = 0;
			authorize_reply_params.params.read.p_data = (uint8_t *)(ble_mostRecentMeasurement.calibrationData0);
		} else {
			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
			authorize_reply_params.params.read.gatt_status = BLE_GATT_STATUS_ATTERR_READ_NOT_PERMITTED;
		}
		err_code = sd_ble_gatts_rw_authorize_reply(p_levelSensorService->conn_handle, &authorize_reply_params);
		APP_ERROR_CHECK(err_code);
	}

    if ((authorize_request->type == BLE_GATTS_AUTHORIZE_TYPE_WRITE) &&
			(authorize_request->request.write.handle == p_levelSensorService->calibrationData0_char_handles.value_handle)) {

		bool success = (authorize_request->request.write.len == 3) && (sensors_controlQueue !=0);

        sensors_control_message_t ble_lvl_evt;

        if (success) {
    		/* User write data */
            ble_lvl_evt.type = SENSORS_EVENT_SET_CALIBRATION_POINT;
            ble_lvl_evt.data.a_ui8[0] = 0;
            memcpy(&(ble_lvl_evt.data.a_ui8[1]),authorize_request->request.write.data,3);

			success = xQueueSend(sensors_controlQueue, &ble_lvl_evt, pdMS_TO_TICKS(10));
			if (success) {
				success = (xEventGroupSetBits(xSensorsEventGroup,
						SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT) & SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT);
			}
		}
        
		if (success) {
			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
			authorize_reply_params.params.write.gatt_status = BLE_GATT_STATUS_SUCCESS;
			authorize_reply_params.params.write.update = 1;
			authorize_reply_params.params.write.len = authorize_request->request.write.len;
			authorize_reply_params.params.write.p_data = authorize_request->request.write.data;
		} else {
			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
			authorize_reply_params.params.write.gatt_status = BLE_GATT_STATUS_ATTERR_WRITE_NOT_PERMITTED;
		}

		err_code = sd_ble_gatts_rw_authorize_reply(p_levelSensorService->conn_handle, &authorize_reply_params);
		APP_ERROR_CHECK(err_code);

	}


    /* Calibration Data Point 1 */

	if ((authorize_request->type == BLE_GATTS_AUTHORIZE_TYPE_READ) &&
			(authorize_request->request.read.handle == p_levelSensorService->calibrationData1_char_handles.value_handle)) {

		/* Save the authorization connection handle so that we can provide
		 * authorization in the callback after the battery reading is
		 * complete. */
		m_rw_authorize_conn_handle = p_levelSensorService->conn_handle;

		authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_READ;

		if (xSemaphoreTake(g_recentSensorData_smphr, 10) == pdPASS) {
			/* Copy into local struct */
			sensors_getCalibrationDataPoint(1,ble_mostRecentMeasurement.calibrationData1);
			/* Give back semaphore now */
			xSemaphoreGive(g_recentSensorData_smphr);

			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
			authorize_reply_params.params.read.gatt_status = BLE_GATT_STATUS_SUCCESS;
			authorize_reply_params.params.read.update = 1;
			authorize_reply_params.params.read.len = 3;
			authorize_reply_params.params.read.offset = 0;
			authorize_reply_params.params.read.p_data = (uint8_t *)(ble_mostRecentMeasurement.calibrationData1);
		} else {
			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
			authorize_reply_params.params.read.gatt_status = BLE_GATT_STATUS_ATTERR_READ_NOT_PERMITTED;
		}
		err_code = sd_ble_gatts_rw_authorize_reply(p_levelSensorService->conn_handle, &authorize_reply_params);
		APP_ERROR_CHECK(err_code);
	}

    if ((authorize_request->type == BLE_GATTS_AUTHORIZE_TYPE_WRITE) &&
			(authorize_request->request.write.handle == p_levelSensorService->calibrationData1_char_handles.value_handle)) {

		bool success = (authorize_request->request.write.len == 3) && (sensors_controlQueue !=0);

        sensors_control_message_t ble_lvl_evt;

        if (success) {
    		/* User write data */
            ble_lvl_evt.type = SENSORS_EVENT_SET_CALIBRATION_POINT;
            ble_lvl_evt.data.a_ui8[0] = 1;
            memcpy(&(ble_lvl_evt.data.a_ui8[1]),authorize_request->request.write.data,3);

			success = xQueueSend(sensors_controlQueue, &ble_lvl_evt, pdMS_TO_TICKS(10));
			if (success) {
				success = (xEventGroupSetBits(xSensorsEventGroup,
						SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT) & SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT);
			}
		}
        
		if (success) {
			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
			authorize_reply_params.params.write.gatt_status = BLE_GATT_STATUS_SUCCESS;
			authorize_reply_params.params.write.update = 1;
			authorize_reply_params.params.write.len = authorize_request->request.write.len;
			authorize_reply_params.params.write.p_data = authorize_request->request.write.data;
		} else {
			authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
			authorize_reply_params.params.write.gatt_status = BLE_GATT_STATUS_ATTERR_WRITE_NOT_PERMITTED;
		}

		err_code = sd_ble_gatts_rw_authorize_reply(p_levelSensorService->conn_handle, &authorize_reply_params);
		APP_ERROR_CHECK(err_code);

	}

}

void on_write(ble_levelSensorService_t *p_levelSensorService, ble_evt_t *p_ble_evt) {
#if (DEBUG_BLE == 1)
	log_info("ON WRITE");
#endif
}


void ble_levelSensorService_on_ble_evt(ble_levelSensorService_t *p_levelSensorService, ble_evt_t *p_ble_evt)
{
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_levelSensorService, p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_levelSensorService, p_ble_evt);
            break;

        case BLE_GAP_EVT_RSSI_CHANGED:
        	on_rssi_changed(p_levelSensorService, p_ble_evt);
        	break;

        case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:
        	on_rw_authorize(p_levelSensorService, p_ble_evt);
        	break;

        case BLE_GATTS_EVT_WRITE:
        	on_write(p_levelSensorService, p_ble_evt);
        	break;

        default:
            break;
    }
}


// uint32_t calibration_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init) {
// 	ble_gatts_char_md_t char_md;
// 	ble_gatts_attr_md_t attr_md;
// 	ble_uuid_t calibration_char_uuid;
// 	ble_gatts_attr_t attr;

// 	/* Characteristic meta-data.  In particular, here is were we make the
// 	 * characteristic read/write-able as well as where we set its name. */
//     memset(&char_md, 0, sizeof(char_md));
//     char_md.char_props.read = 1;
//     char_md.char_props.write = 1;
//     char_md.p_char_user_desc = (uint8_t *)"Calibration";
//     char_md.char_user_desc_max_size = 11;
//     char_md.char_user_desc_size = 11;
//     char_md.p_user_desc_md = NULL;
//     char_md.p_char_pf = NULL;
//     char_md.p_cccd_md = NULL;
//     char_md.p_sccd_md = NULL;

//     /* The attribute is the part of the characteristic that holds data. Here
//      * we configure the attribute's meta-data to allow open reads and writes
//      * but we require authorization on write so that the application can verify
//      * that the measurement interval is valid. */
//     memset(&attr_md, 0, sizeof(attr_md));
//     BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
//     BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
//     attr_md.wr_auth		= 1;
//     attr_md.vloc		= BLE_GATTS_VLOC_STACK;
//     attr_md.vlen		= 0;

//     /* Add a vendor specific 128-bit UUID to the BLE stack's table of UUIDs.
//      * In particular, this UUID will be used to identify the acknowledged data
//      * characteristic. . */

//     /* The data characteristic uses the same 128-bit base UUID than the
//      * PicoSensor service, so we do not need to add another UUID to the BLE
//      * stack's table of vendor-specific UUIDs.  */

//     /* Create a more easily used 24-bit UUID from the new vendor-specific
//      * UUID base and the two TimeLow bytes that are specific to the data
//      * characteristic. */
//     calibration_char_uuid.type = p_levelSensorService->service_uuid_type;
//     calibration_char_uuid.uuid = LEVELSENSOR_UUID_CALIBRATION_CHAR;

//     /* Finally, we initialize the attribute */
//     memset(&attr, 0, sizeof(attr));
//     attr.p_uuid = &calibration_char_uuid;
//     attr.p_attr_md = &attr_md;
//     attr.init_len = 1;
//     attr.init_offs = 0;
//     attr.max_len = 2;
//     attr.p_value = (uint8_t *)&p_levelSensorService_init->calibration;

//     return sd_ble_gatts_characteristic_add(p_levelSensorService->service_handle,
//     		&char_md, &attr,
//     		&p_levelSensorService->calibration_char_handles);
// }

uint32_t calibrationData0_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init) {
	ble_gatts_char_md_t char_md;
	ble_gatts_attr_md_t attr_md;
	ble_uuid_t calibrationData0_char_uuid;
	ble_gatts_attr_t attr;

	/* Characteristic meta-data.  In particular, here is were we make the
	 * characteristic read/write-able as well as where we set its name. */
    memset(&char_md, 0, sizeof(char_md));
    char_md.char_props.read = 1;
    char_md.char_props.write = 1;
    char_md.p_char_user_desc = (uint8_t *)"Calibration Data 0";
    char_md.char_user_desc_max_size = 18;
    char_md.char_user_desc_size = 18;
    char_md.p_user_desc_md = NULL;
    char_md.p_char_pf = NULL;
    char_md.p_cccd_md = NULL;
    char_md.p_sccd_md = NULL;

    /* The attribute is the part of the characteristic that holds data. Here
     * we configure the attribute's meta-data to allow open reads and writes
     * but we require authorization on write so that the application can verify
     * that the measurement interval is valid. */
    memset(&attr_md, 0, sizeof(attr_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    attr_md.rd_auth		= 1;
    attr_md.wr_auth		= 1;
    attr_md.vloc		= BLE_GATTS_VLOC_STACK;
    attr_md.vlen		= 0;

    /* Add a vendor specific 128-bit UUID to the BLE stack's table of UUIDs.
     * In particular, this UUID will be used to identify the acknowledged data
     * characteristic. . */

    /* The data characteristic uses the same 128-bit base UUID than the
     * PicoSensor service, so we do not need to add another UUID to the BLE
     * stack's table of vendor-specific UUIDs.  */

    /* Create a more easily used 24-bit UUID from the new vendor-specific
     * UUID base and the two TimeLow bytes that are specific to the data
     * characteristic. */
    calibrationData0_char_uuid.type = p_levelSensorService->service_uuid_type;
    calibrationData0_char_uuid.uuid = LEVELSENSOR_UUID_CALIBRATION_DATA0_CHAR;

    /* Finally, we initialize the attribute */
    memset(&attr, 0, sizeof(attr));
    attr.p_uuid = &calibrationData0_char_uuid;
    attr.p_attr_md = &attr_md;
    attr.init_len = 3;
    attr.init_offs = 0;
    attr.max_len = 3;
    attr.p_value = (uint8_t *)p_levelSensorService_init->calibrationData0;

    return sd_ble_gatts_characteristic_add(p_levelSensorService->service_handle,
    		&char_md, &attr,
    		&p_levelSensorService->calibrationData0_char_handles);
}

uint32_t calibrationData1_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init) {
	ble_gatts_char_md_t char_md;
	ble_gatts_attr_md_t attr_md;
	ble_uuid_t calibrationData1_char_uuid;
	ble_gatts_attr_t attr;

	/* Characteristic meta-data.  In particular, here is were we make the
	 * characteristic read/write-able as well as where we set its name. */
    memset(&char_md, 0, sizeof(char_md));
    char_md.char_props.read = 1;
    char_md.char_props.write = 1;
    char_md.p_char_user_desc = (uint8_t *)"Calibration Data 1";
    char_md.char_user_desc_max_size = 18;
    char_md.char_user_desc_size = 18;
    char_md.p_user_desc_md = NULL;
    char_md.p_char_pf = NULL;
    char_md.p_cccd_md = NULL;
    char_md.p_sccd_md = NULL;

    /* The attribute is the part of the characteristic that holds data. Here
     * we configure the attribute's meta-data to allow open reads and writes
     * but we require authorization on write so that the application can verify
     * that the measurement interval is valid. */
    memset(&attr_md, 0, sizeof(attr_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    attr_md.rd_auth     = 1;
    attr_md.wr_auth		= 1;
    attr_md.vloc		= BLE_GATTS_VLOC_STACK;
    attr_md.vlen		= 0;

    /* Add a vendor specific 128-bit UUID to the BLE stack's table of UUIDs.
     * In particular, this UUID will be used to identify the acknowledged data
     * characteristic. . */

    /* The data characteristic uses the same 128-bit base UUID than the
     * PicoSensor service, so we do not need to add another UUID to the BLE
     * stack's table of vendor-specific UUIDs.  */

    /* Create a more easily used 24-bit UUID from the new vendor-specific
     * UUID base and the two TimeLow bytes that are specific to the data
     * characteristic. */
    calibrationData1_char_uuid.type = p_levelSensorService->service_uuid_type;
    calibrationData1_char_uuid.uuid = LEVELSENSOR_UUID_CALIBRATION_DATA1_CHAR;

    /* Finally, we initialize the attribute */
    memset(&attr, 0, sizeof(attr));
    attr.p_uuid = &calibrationData1_char_uuid;
    attr.p_attr_md = &attr_md;
    attr.init_len = 3;
    attr.init_offs = 0;
    attr.max_len = 3;
    attr.p_value = (uint8_t *)p_levelSensorService_init->calibrationData1;

    return sd_ble_gatts_characteristic_add(p_levelSensorService->service_handle,
    		&char_md, &attr,
    		&p_levelSensorService->calibrationData1_char_handles);
}


// uint32_t measurementInterval_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init) {
// 	ble_gatts_char_md_t char_md;
// 	ble_gatts_attr_md_t attr_md;
// 	ble_uuid_t measurementInterval_char_uuid;
// 	ble_gatts_attr_t attr;

// 	/* Characteristic meta-data.  In particular, here is were we make the
// 	 * characteristic read/write-able as well as where we set its name. */
//     memset(&char_md, 0, sizeof(char_md));
//     char_md.char_props.read = 1;
//     char_md.char_props.write = 1;
//     char_md.p_char_user_desc = (uint8_t *)"Measurement Interval";
//     char_md.char_user_desc_max_size = 20;
//     char_md.char_user_desc_size = 20;
//     char_md.p_user_desc_md = NULL;
//     char_md.p_char_pf = NULL;
//     char_md.p_cccd_md = NULL;
//     char_md.p_sccd_md = NULL;

//     /* The attribute is the part of the characteristic that holds data. Here
//      * we configure the attribute's meta-data to allow open reads and writes
//      * but we require authorization on write so that the application can verify
//      * that the measurement interval is valid. */
//     memset(&attr_md, 0, sizeof(attr_md));
//     BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
//     BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
//     attr_md.wr_auth		= 1;
//     attr_md.vloc		= BLE_GATTS_VLOC_STACK;
//     attr_md.vlen		= 0;

//     /* Add a vendor specific 128-bit UUID to the BLE stack's table of UUIDs.
//      * In particular, this UUID will be used to identify the acknowledged data
//      * characteristic. . */

//     /* The data characteristic uses the same 128-bit base UUID than the
//      * PicoSensor service, so we do not need to add another UUID to the BLE
//      * stack's table of vendor-specific UUIDs.  */

//     /* Create a more easily used 24-bit UUID from the new vendor-specific
//      * UUID base and the two TimeLow bytes that are specific to the data
//      * characteristic. */
//     measurementInterval_char_uuid.type = p_levelSensorService->service_uuid_type;
//     measurementInterval_char_uuid.uuid = LEVELSENSOR_UUID_MEASUREMENT_INTERVAL_CHAR;

//     /* Finally, we initialize the attribute */
//     memset(&attr, 0, sizeof(attr));
//     attr.p_uuid = &measurementInterval_char_uuid;
//     attr.p_attr_md = &attr_md;
//     attr.init_len = 1;
//     attr.init_offs = 0;
//     attr.max_len = 1;
//     attr.p_value = (uint8_t *)&p_levelSensorService_init->measurementInterval_min;

//     return sd_ble_gatts_characteristic_add(p_levelSensorService->service_handle,
//     		&char_md, &attr,
//     		&p_levelSensorService->measurementInterval_char_handles);
// }

/*uint32_t batteryVoltage_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init) {
	ble_gatts_char_md_t char_md;
    ble_gatts_attr_t attr;
    ble_gatts_attr_md_t attr_md;

    ble_uuid_t batteryVoltage_char_uuid;

	 The characteristic should be read- and write-able. This block of code
	 * configures the characteristic's metadata.
    memset(&char_md, 0, sizeof(char_md));
    char_md.char_props.read = 1;
    char_md.p_char_user_desc = (uint8_t *)"Battery Voltage";
    char_md.char_user_desc_max_size = 15;
    char_md.char_user_desc_size = 15;
    char_md.p_user_desc_md = NULL;
    char_md.p_char_pf = NULL;
    char_md.p_cccd_md = NULL;
    char_md.p_sccd_md = NULL;

     The attribute is the part of the characteristic that holds data. Here
     * we configure the attribute's metadata to allow open reads and writes,
     * but we require write authorization, so that we get a
     * callback which we can use to increment the trip counter.
     * We'll initialize the attribute below after initializating the metadata
     * and creating a new UUID for it.
    memset(&attr_md, 0, sizeof(attr_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&attr_md.write_perm);
    attr_md.rd_auth		= 1;
    attr_md.wr_auth		= 0;
    attr_md.vloc		= BLE_GATTS_VLOC_STACK;
    attr_md.vlen		= 0;


     Add a vendor specific 128-bit UUID to the BLE stack's table of UUIDs.
     * In particular, this UUID will be used to identify the acknowledged data
     * characteristic. .

     The data characteristic uses the same 128-bit base UUID than the
     * PicoSensor service, so we do not need to add another UUID to the BLE
     * stack's table of vendor-specific UUIDs.

     Create a more easily used 24-bit UUID from the new vendor-specific
     * UUID base and the two TimeLow bytes that are specific to the data
     * characteristic.
    batteryVoltage_char_uuid.type = p_levelSensorService->service_uuid_type;
    batteryVoltage_char_uuid.uuid = LEVELSENSOR_UUID_BATTERY_VOLTAGE_CHAR;

     Finally, we initialize the attribute
    memset(&attr, 0, sizeof(attr));
    attr.p_uuid = &batteryVoltage_char_uuid;
    attr.p_attr_md = &attr_md;
    attr.init_len = 2;
    attr.init_offs = 0;
    attr.max_len = 2;
    attr.p_value = NULL;

    return sd_ble_gatts_characteristic_add(p_levelSensorService->service_handle,
    		&char_md, &attr,
    		&p_levelSensorService->batteryVoltage_char_handles);
}*/


// uint32_t sensorSerial_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init) {
// 	ble_gatts_char_md_t char_md;
//     ble_gatts_attr_t attr;
//     ble_gatts_attr_md_t attr_md;

//     ble_uuid_t sensorSerial_char_uuid;

//     memcpy(p_levelSensorService->sensorSerial, p_levelSensorService_init->sensorSerial, sizeof(p_levelSensorService->sensorSerial));

// 	/* The characteristic should be read- and write-able. This block of code
// 	 * configures the characteristic's metadata. */
//     memset(&char_md, 0, sizeof(char_md));
//     uint8_t user_desc[] = "ACCEL";

//     char_md.char_props.read = 1;
//     char_md.p_char_user_desc = (uint8_t *)user_desc;
//     char_md.char_user_desc_max_size = 5;
//     char_md.char_user_desc_size = 5;
//     char_md.p_user_desc_md = NULL;
//     char_md.p_char_pf = NULL;
//     char_md.p_cccd_md = NULL;
//     char_md.p_sccd_md = NULL;

//     /* The attribute is the part of the characteristic that holds data. Here
//      * we configure the attribute's metadata to allow open reads and writes,
//      * but we require write authorization, so that we get a
//      * callback which we can use to load the demo data into the FIFO.
//      * We'll initialize the attribute below after initializing the metadata
//      * and creating a new UUID for it.  */
//     memset(&attr_md, 0, sizeof(attr_md));
//     BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
//     attr_md.rd_auth		= 1;
//     attr_md.wr_auth		= 0;
//     attr_md.vloc		= BLE_GATTS_VLOC_STACK;
//     attr_md.vlen		= 0;


//     /* Add a vendor specific 128-bit UUID to the BLE stack's table of UUIDs.
//      * In particular, this UUID will be used to identify the acknowledged data
//      * characteristic. . */

//     /* The data characteristic uses the same 128-bit base UUID than the
//      * PicoSensor service, so we do not need to add another UUID to the BLE
//      * stack's table of vendor-specific UUIDs.  */

//     /* Create a more easily used 24-bit UUID from the new vendor-specific
//      * UUID base and the two TimeLow bytes that are specific to the data
//      * characteristic. */
//     sensorSerial_char_uuid.type = p_levelSensorService->service_uuid_type;
//     sensorSerial_char_uuid.uuid = LEVELSENSOR_UUID_SENSOR_SERIAL_CHAR;

//     /* Finally, we initialize the attribute */
//     memset(&attr, 0, sizeof(attr));
//     attr.p_uuid = &sensorSerial_char_uuid;
//     attr.p_attr_md = &attr_md;
//     attr.init_len = 8;
//     attr.init_offs = 0;
//     attr.max_len = 8;
//     attr.p_value = (uint8_t *)&p_levelSensorService->sensorSerial;

//     return sd_ble_gatts_characteristic_add(p_levelSensorService->service_handle,
//     		&char_md, &attr,
//     		&p_levelSensorService->sensorSerial_char_handles);
// }

// uint32_t temperature_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init) {
// 	ble_gatts_char_md_t char_md;
//     ble_gatts_attr_t attr;
//     ble_gatts_attr_md_t attr_md;

//     ble_uuid_t temperature_char_uuid;

//     /* We use read authorization, so the initial temperature value does not
//      * matter. */
//     p_levelSensorService->temperature = INT16_MIN;

// 	/* The characteristic should be read- and write-able. This block of code
// 	 * configures the characteristic's metadata. */
//     memset(&char_md, 0, sizeof(char_md));
//     char_md.char_props.read = 1;
//     char_md.p_char_user_desc = (uint8_t *)"Temperature";
//     char_md.char_user_desc_max_size = 11;
//     char_md.char_user_desc_size = 11;
//     char_md.p_user_desc_md = NULL;
//     char_md.p_char_pf = NULL;
//     char_md.p_cccd_md = NULL;
//     char_md.p_sccd_md = NULL;

//     /* The attribute is the part of the characteristic that holds data. Here
//      * we configure the attribute's metadata to allow open reads and writes,
//      * but we require write authorization, so that we get a
//      * callback which we can use to load the demo data into the FIFO.
//      * We'll initialize the attribute below after initializing the metadata
//      * and creating a new UUID for it.  */
//     memset(&attr_md, 0, sizeof(attr_md));
//     BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
//     attr_md.rd_auth		= 1;
//     attr_md.wr_auth		= 0;
//     attr_md.vloc		= BLE_GATTS_VLOC_STACK;
//     attr_md.vlen		= 0;


//     /* Add a vendor specific 128-bit UUID to the BLE stack's table of UUIDs.
//      * In particular, this UUID will be used to identify the acknowledged data
//      * characteristic. . */

//     /* The data characteristic uses the same 128-bit base UUID than the
//      * PicoSensor service, so we do not need to add another UUID to the BLE
//      * stack's table of vendor-specific UUIDs.  */

//     /* Create a more easily used 24-bit UUID from the new vendor-specific
//      * UUID base and the two TimeLow bytes that are specific to the data
//      * characteristic. */
//     temperature_char_uuid.type = p_levelSensorService->service_uuid_type;
//     temperature_char_uuid.uuid = LEVELSENSOR_UUID_TEMPERATURE_CHAR;

//     /* Finally, we initialize the attribute */
//     memset(&attr, 0, sizeof(attr));
//     attr.p_uuid = &temperature_char_uuid;
//     attr.p_attr_md = &attr_md;
//     attr.init_len = 2;
//     attr.init_offs = 0;
//     attr.max_len = 2;
//     attr.p_value = (uint8_t *)&p_levelSensorService->temperature;

//     return sd_ble_gatts_characteristic_add(p_levelSensorService->service_handle,
//     		&char_md, &attr,
//     		&p_levelSensorService->temperature_char_handles);
// }

uint32_t level_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init) {
	ble_gatts_attr_md_t cccd_md;
	ble_gatts_char_md_t char_md;
    ble_gatts_attr_t attr;
    ble_gatts_attr_md_t attr_md;

    ble_uuid_t level_char_uuid;

    /* The client characteristic configuration descriptor is used to control
     * whether the server (the nRF5x) can indicate or notify to client.  We
     * allow the client to both read and write the CCCD. */
	memset(&cccd_md, 0, sizeof(cccd_md));
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
	/* The CCCD should be located on the stack */
	cccd_md.vloc = BLE_GATTS_VLOC_STACK;


	/* The characteristic should be read-able and support notification. This
	 * block of code configures the characteristic's metadata. */
	memset(&char_md, 0, sizeof(char_md));
	char_md.char_props.notify = 1;
	char_md.char_props.read = 1;
	char_md.p_char_user_desc = (uint8_t *)"Level";
	char_md.char_user_desc_max_size = 5;
	char_md.char_user_desc_size = 5;
	char_md.p_user_desc_md = NULL;
	char_md.p_char_pf = NULL;
	char_md.p_cccd_md = &cccd_md;
	char_md.p_sccd_md = NULL;

    /* The attribute is the part of the characteristic that holds data. Here
     * we configure the attribute's metadata to allow open reads and writes,
     * but we require write authorization, so that we get a
     * callback which we can use to load the demo data into the FIFO.
     * We'll initialize the attribute below after initializing the metadata
     * and creating a new UUID for it.  */
	 memset(&attr_md, 0, sizeof(attr_md));
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&attr_md.write_perm);
	attr_md.rd_auth		= 1;
	attr_md.vloc		= BLE_GATTS_VLOC_STACK;
	attr_md.vlen		= 0;

    /* Add a vendor specific 128-bit UUID to the BLE stack's table of UUIDs.
     * In particular, this UUID will be used to identify the acknowledged data
     * characteristic. . */

    /* The data characteristic uses the same 128-bit base UUID than the
     * PicoSensor service, so we do not need to add another UUID to the BLE
     * stack's table of vendor-specific UUIDs.  */

    /* Create a more easily used 24-bit UUID from the new vendor-specific
     * UUID base and the two TimeLow bytes that are specific to the data
     * characteristic. */
    level_char_uuid.type = p_levelSensorService->service_uuid_type;
    level_char_uuid.uuid = LEVELSENSOR_UUID_LEVEL_CHAR;

    /* Finally, we initialize the attribute */
    memset(&attr, 0, sizeof(attr));
    attr.p_uuid = &level_char_uuid;
    attr.p_attr_md = &attr_md;
    attr.init_len = 1;
    attr.init_offs = 0;
    attr.max_len = 1;
    attr.p_value = (uint8_t *)&p_levelSensorService->level;

    return sd_ble_gatts_characteristic_add(p_levelSensorService->service_handle,
    		&char_md, &attr,
    		&p_levelSensorService->level_char_handles);
}

uint32_t capacitance_char_add(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init) {
	ble_gatts_attr_md_t cccd_md;
	ble_gatts_char_md_t char_md;
    ble_gatts_attr_t attr;
    ble_gatts_attr_md_t attr_md;

    ble_uuid_t capacitance_char_uuid;

    /* The client characteristic configuration descriptor is used to control
     * whether the server (the nRF5x) can indicate or notify to client.  We
     * allow the client to both read and write the CCCD. */
	memset(&cccd_md, 0, sizeof(cccd_md));
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
	/* The CCCD should be located on the stack */
	cccd_md.vloc = BLE_GATTS_VLOC_STACK;


	/* The characteristic should be read-able and support notification. This
	 * block of code configures the characteristic's metadata. */
     static char user_desc[] = "Capacitance";
	memset(&char_md, 0, sizeof(char_md));
    char_md.char_props.notify = 1;
	char_md.char_props.read = 1;
	char_md.p_char_user_desc = (uint8_t *)user_desc;
	char_md.char_user_desc_max_size = strlen(user_desc);
	char_md.char_user_desc_size = strlen(user_desc);
	char_md.p_user_desc_md = NULL;
	char_md.p_char_pf = NULL;
	char_md.p_cccd_md = &cccd_md;
	char_md.p_sccd_md = NULL;

    /* The attribute is the part of the characteristic that holds data. Here
     * we configure the attribute's metadata to allow open reads and writes,
     * but we require write authorization, so that we get a
     * callback which we can use to load the demo data into the FIFO.
     * We'll initialize the attribute below after initializing the metadata
     * and creating a new UUID for it.  */
	 memset(&attr_md, 0, sizeof(attr_md));
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&attr_md.write_perm);
	attr_md.rd_auth		= 1;
	attr_md.vloc		= BLE_GATTS_VLOC_STACK;
	attr_md.vlen		= 0;

    /* Add a vendor specific 128-bit UUID to the BLE stack's table of UUIDs.
     * In particular, this UUID will be used to identify the acknowledged data
     * characteristic. . */

    /* The data characteristic uses the same 128-bit base UUID than the
     * PicoSensor service, so we do not need to add another UUID to the BLE
     * stack's table of vendor-specific UUIDs.  */

    /* Create a more easily used 24-bit UUID from the new vendor-specific
     * UUID base and the two TimeLow bytes that are specific to the data
     * characteristic. */
    capacitance_char_uuid.type = p_levelSensorService->service_uuid_type;
    capacitance_char_uuid.uuid = LEVELSENSOR_UUID_CAPACITANCE_CHAR;

    p_levelSensorService->capacitance = 0;

    /* Finally, we initialize the attribute */
    memset(&attr, 0, sizeof(attr));
    attr.p_uuid = &capacitance_char_uuid;
    attr.p_attr_md = &attr_md;
    attr.init_len = 4;
    attr.init_offs = 0;
    attr.max_len = 4;
    attr.p_value = (uint8_t *)&(p_levelSensorService->capacitance);

    return sd_ble_gatts_characteristic_add(p_levelSensorService->service_handle,
    		&char_md, &attr,
    		&p_levelSensorService->capacitance_char_handles);
}


bool ble_levelSensorService_indicateLevel(const ble_levelSensorService_t *p_levelSensorService, int8_t p_level) {
	ble_gatts_hvx_params_t hvx_params;
    ble_gatts_value_t gatts_value;

	uint32_t err_code;

    // Initialize value struct.
    memset(&gatts_value, 0, sizeof(gatts_value));

    gatts_value.len     = sizeof(uint8_t);
    gatts_value.offset  = 0;
    gatts_value.p_value = (uint8_t *)&p_level;

    /* We should consider checking whether notification is enabled. */

	/* Prepare to notify this data to the client */
	memset(&hvx_params, 0, sizeof(hvx_params));

    hvx_params.handle = p_levelSensorService->level_char_handles.value_handle;
    hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
    hvx_params.offset = gatts_value.offset;
    hvx_params.p_len  = &gatts_value.len;
    hvx_params.p_data = gatts_value.p_value;

    err_code = sd_ble_gatts_hvx(p_levelSensorService->conn_handle, &hvx_params);

    /* We should consider checking whether notification is enabled. If a peer is not connected
     * or notification has not been enable an error is returned*/

	if (err_code != NRF_SUCCESS) {
		log_warnf("Notification of level characteristic failed! ERROR %04X",err_code);
		return false;
	} else {
		log_infof("Notified client that the level %d was reported", p_level);
		return true;
	}
}

bool ble_levelSensorService_indicateCapacitance(const ble_levelSensorService_t *p_levelSensorService, uint32_t p_cap) {
	ble_gatts_hvx_params_t hvx_params;
    ble_gatts_value_t gatts_value;

	uint32_t err_code;

    // Initialize value struct.
    memset(&gatts_value, 0, sizeof(gatts_value));

    // TODO: Standardize units between sensor and app
    p_cap = p_cap / 10;

    gatts_value.len     = sizeof(uint32_t);
    gatts_value.offset  = 0;
    gatts_value.p_value = (uint8_t *)&p_cap;

    /* We should consider checking whether notification is enabled. */

	/* Prepare to notify this data to the client */
	memset(&hvx_params, 0, sizeof(hvx_params));

    hvx_params.handle = p_levelSensorService->capacitance_char_handles.value_handle;
    hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
    hvx_params.offset = gatts_value.offset;
    hvx_params.p_len  = &gatts_value.len;
    hvx_params.p_data = gatts_value.p_value;

    err_code = sd_ble_gatts_hvx(p_levelSensorService->conn_handle, &hvx_params);

    /* We should consider checking whether notification is enabled. If a peer is not connected
     * or notification has not been enable an error is returned*/

	if (err_code != NRF_SUCCESS) {
		log_warnf("Notification of capacitance characteristic failed! ERROR %04X",err_code);
		return false;
	} else {
		log_infof("Notified client that the capacitance %u was reported", p_cap);
		return true;
	}
}


uint32_t ble_levelSensorService_init(ble_levelSensorService_t *p_levelSensorService, const ble_levelSensorService_init_t *p_levelSensorService_init) {
    uint32_t   err_code;
    ble_uuid_t ble_service_uuid;

    /* Initialize service structure */
    p_levelSensorService->conn_handle = BLE_CONN_HANDLE_INVALID;

    /* Add a vendor specific 128-bit UUID to the BLE stack's table of UUIDs. In
     * particular, this UUID will be used to identify the PicoSensor service.
     * An index identifying this new UUID will be stored in
     * the p_levelSensorService->service_uuid_type variable. */
    ble_uuid128_t base_uuid = LEVELSENSOR_UUID_SERVUCE_BASE;
    err_code = sd_ble_uuid_vs_add(&base_uuid,
    		&p_levelSensorService->service_uuid_type);
    if (err_code != NRF_SUCCESS) {
    	return err_code;
    }

    /* Create a more easily used 24-bit UUID from the new vendor-specific
     * service UUID base and the two TimeLow bytes. */
    ble_service_uuid.type = p_levelSensorService->service_uuid_type;
    ble_service_uuid.uuid = LEVELSENSOR_UUID_SERVICE;

    /* Add the PicoSensor service to the BLE stack using the abbreviated UUID
     * which we just created. */
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
    		&ble_service_uuid, &p_levelSensorService->service_handle);
    if (err_code != NRF_SUCCESS) {
    	return err_code;
    }

    /* At this point, we have created the LevelSensor service, but it does not
     * yet have any characteristics, so we add them now: */

    // /* Measurement interval */
    // err_code = measurementInterval_char_add(p_levelSensorService, p_levelSensorService_init);
    // if (err_code != NRF_SUCCESS) {
    // 	return err_code;
    // }

    // /* Calibration point */
    // err_code = calibration_char_add(p_levelSensorService, p_levelSensorService_init);
    // if (err_code != NRF_SUCCESS) {
    // 	return err_code;
    // }

    // /* Calibration Data Access */
    err_code = calibrationData0_char_add(p_levelSensorService, p_levelSensorService_init);
    if (err_code != NRF_SUCCESS) {
    	return err_code;
    }
    err_code = calibrationData1_char_add(p_levelSensorService, p_levelSensorService_init);
    if (err_code != NRF_SUCCESS) {
    	return err_code;
    }

    /* Current Level */
    err_code = level_char_add(p_levelSensorService, p_levelSensorService_init);
    if (err_code != NRF_SUCCESS) {
    	return err_code;
    }

    // /* Battery voltage */
    // err_code = batteryVoltage_char_add(p_levelSensorService, p_levelSensorService_init);
    // if (err_code != NRF_SUCCESS) {
    //     return err_code;
    // }

    /* Sensor Serial Number */
    // err_code = sensorSerial_char_add(p_levelSensorService, p_levelSensorService_init);
	// if (err_code != NRF_SUCCESS) {
	// 	return err_code;
	// }

	// /* Current temperature */
    // err_code = temperature_char_add(p_levelSensorService, p_levelSensorService_init);
	// if (err_code != NRF_SUCCESS) {
	// 	return err_code;
	// }

        /* Current Capacitance */
    err_code = capacitance_char_add(p_levelSensorService, p_levelSensorService_init);
    if (err_code != NRF_SUCCESS) {
    	return err_code;
    }


    return NRF_SUCCESS;
}

