/*
 * extsensor.c
 *
 *  Created on: Jun 30, 2017
 *      Author: kwgilpin
 */


#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"

#include "app_util_platform.h"
#include "app_twi.h"
#include "app_error.h"

#include "SEGGER_RTT.h"

#include "boards.h"

#include "twi_register.h"
#include "extsensor.h"
#include "FreeRTOS.h"
#include "portmacro_cmsis.h"
#include "task.h"

void extsensor_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
void extsensor_toggleState(bool t_on);

void extsensor_init() {
	uint32_t err_code;

    /* Configure the EXTSENSOREN_PIN line as a input with HI-LO sensing */
    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(false);
    err_code = nrf_drv_gpiote_in_init(EXTSENSOREN_PIN, &in_config, extsensor_pin_handler);
    APP_ERROR_CHECK(err_code);

	extsensor_off();

    //nrf_drv_gpiote_in_event_enable(EXTSENSOREN_PIN, true);
}

void extsensor_off() {
	if (nrf_gpio_pin_read(EXTSENSOREN_PIN)) {
		nrf_drv_gpiote_in_event_disable(EXTSENSOREN_PIN);
		extsensor_toggleState(false);
	}
}

void extsensor_on(TickType_t powerUpTicks) {
	if (!nrf_gpio_pin_read(EXTSENSOREN_PIN)) {
		nrf_drv_gpiote_in_event_disable(EXTSENSOREN_PIN);
		extsensor_toggleState(true);
	    nrf_drv_gpiote_in_event_enable(EXTSENSOREN_PIN, true);
	}
	vTaskDelay(powerUpTicks);
}

bool extsensor_getState() {
	return nrf_gpio_pin_read(EXTSENSOREN_PIN);
}

void extsensor_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    printf("Fault on external sensor connector\r\n");
}

void extsensor_toggleState(bool t_on) {
	nrf_gpio_pin_clear(EXTSENSOREN_PIN);
	nrf_gpio_cfg_output(EXTSENSOREN_PIN);
	nrf_delay_us(100);

	nrf_gpio_pin_set(EXTSENSOREN_PIN);
	nrf_delay_us(50);

	if (!t_on) {
		nrf_gpio_pin_clear(EXTSENSOREN_PIN);
		nrf_delay_us(50);
	}

	nrf_gpio_cfg_input(EXTSENSOREN_PIN, NRF_GPIO_PIN_NOPULL);
}
