/*
 * notify.h
 *
 *  Created on: Aug 30, 2017
 *      Author: nreimens
 */

#ifndef SRC_TEMPALERT_NOTIFY_H_
#define SRC_TEMPALERT_NOTIFY_H_

#include <stdint.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "event_groups.h"

#define MAX_SUBSCRIPTIONS 5

/* ERROR TYPES */
#define NOTIFY_ERROR_NULL_HANDLE 			-1
#define NOTIFY_ERROR_SUBSCRIPTIONS_FULL 		-2
#define NOTIFY_ERROR_ARGUMNET_NULL			-3
#define NOTIFY_ERROR_BAD_RECIPT				-4


/* NotifyReceipt_t # returned to caller of Subscribe for unsubscribing */
typedef int32_t NotifyReceipt_t;

typedef struct {
	EventGroupHandle_t eventGroup;
	const EventBits_t eventBit;
	QueueHandle_t respQueue;
} NotifyInfo_t;

typedef struct {
	uint8_t numSubscribers;
	NotifyInfo_t subscriptions[MAX_SUBSCRIPTIONS];
	NotifyReceipt_t receipts[MAX_SUBSCRIPTIONS];
	NotifyReceipt_t lastReceipt;
	uint32_t notifyItemSize;
} Notify_t;

typedef Notify_t * NotifyHandle_t;


bool notifyInit(Notify_t * notifyingElement);
bool notifySubscribe(NotifyHandle_t notifyHandle, NotifyReceipt_t * p_subscriberReceipt, NotifyInfo_t subscriberInfo);
bool notifyUnsubscribe(NotifyHandle_t notifyHandle, NotifyReceipt_t subscriberReceipt);
bool notifySubscribers(NotifyHandle_t notifyingElement, void * item);


#endif /* SRC_TEMPALERT_NOTIFY_H_ */
