/*
 * els31uart.c
 *
 *  Created on: Jan 16, 2017
 *      Author: kwgilpin
 */


#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "nordic_common.h"
#include "nrf.h"
#include "SEGGER_RTT.h"
#include "app_uart.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "portmacro_cmsis.h"

#include "bsp.h"
#include "boards.h"

#include "simplehsm.h"
#include "modem.h"
#include "els31uart.h"

#include "errorCount.h"


#define DEBUG_ELS31UART_HSM 	0
#define DEBUG_PRINT				1

#define UART_TX_BUF_SIZE		256
#define UART_RX_BUF_SIZE		256

#define CMD_LENGTH_MAX			256

static const app_uart_comm_params_t m_comm_params = {
		CELL_RXD_PIN,
		CELL_TXD_PIN,
		CELL_RTS_PIN,
		CELL_CTS_PIN,
		APP_UART_FLOW_CONTROL_ENABLED,
		false,
		UART_BAUDRATE_BAUDRATE_Baud115200
};

typedef enum {
	UARTRxEvent,
	TimerExpiredEvent,
	ATCmdEvent,
	UartSleepEvent,
	UartWakeEvent
} els31uart_eventType_t;

typedef struct {
	els31uart_eventType_t eventType;
	uint8_t *data;
} els31uart_event_t;

typedef enum {
	SIG_RX_CHAR = SIG_USER,
	SIG_AT_CMD,
	SIG_TIMEOUT,
	SIG_SLEEP_UART,
	SIG_WAKE_UART
} els31uart_hsm_signals_t;

static const char *m_signalNames[10];


static simplehsm_t els31uart_hsm = {NULL};

static stnext state_top(int signal, void *param);
  static stnext state_tx(int signal, void *param);
    static stnext state_tx_idle(int signal, void *param);
    static stnext state_tx_first(int signal, void *param);
    static stnext state_tx_remainder(int signal, void *param);
  static stnext state_rxline(int signal, void *param);
  static stnext state_enable_uart(int signal, void *param);
  static stnext state_disable_uart(int signal, void *param);


static SemaphoreHandle_t m_at_cmd_smphr = NULL;
static StaticSemaphore_t m_at_cmd_smphr_buffer;

static QueueHandle_t m_event_queue = NULL;
static StaticQueue_t m_event_queue_static;
#define M_EVENT_QUEUE_LENGTH		4
#define M_EVENT_QUEUE_ITEM_SIZE		sizeof(els31uart_event_t)
uint8_t m_event_queue_static_buffer[M_EVENT_QUEUE_LENGTH*M_EVENT_QUEUE_ITEM_SIZE];

static TimerHandle_t lineTimer;
static StaticTimer_t lineTimerStatic;
static TimerHandle_t atCmdTimer;
static StaticTimer_t atCmdTimerStatic;

static TaskHandle_t m_els31uart_thread;
static StaticTask_t m_els31uart_threadStatic;
#define ELSUART_STACK_SIZE		512
static StackType_t m_els31uart_threadBuffer[ ELSUART_STACK_SIZE ];

static bool m_isATCmdPending;
static uint8_t m_pendingATCmd[CMD_LENGTH_MAX];
static uint16_t m_pendingATCmdLength;
static uint16_t m_pendingATCmdIndex;

static bool m_isInit = false;

static uint8_t m_response[RSP_LENGTH_MAX];
static uint16_t m_responseLength;

static app_uart_event_handler_t els31uart_callback(app_uart_evt_t * p_event);

static void els31uart_lineTimerCallback(TimerHandle_t timer);
static void els31uart_atCmdTimerCallback(TimerHandle_t timer);

bool els31uart_init(void) {
	m_pendingATCmdLength = 0;
	m_responseLength = 0;

	m_signalNames[0] = "SIG_NULL";
	m_signalNames[1] = "SIG_INIT";
	m_signalNames[2] = "SIG_ENTRY";
	m_signalNames[3] = "SIG_DEEPHIST";
	m_signalNames[4] = "SIG_EXIT";
	m_signalNames[5] = "SIG_RX_CHAR";
	m_signalNames[6] = "SIG_AT_CMD";
	m_signalNames[7] = "SIG_TIMEOUT";
	m_signalNames[8] = "SIG_SLEEP_UART";
	m_signalNames[9] = "SIG_WAKE_UART";

	m_at_cmd_smphr = xSemaphoreCreateBinaryStatic(&m_at_cmd_smphr_buffer);
	if (m_at_cmd_smphr == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	m_event_queue = xQueueCreateStatic(M_EVENT_QUEUE_LENGTH, M_EVENT_QUEUE_ITEM_SIZE, m_event_queue_static_buffer, &m_event_queue_static);
	if (NULL == m_event_queue) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}


	if ((lineTimer = xTimerCreateStatic("Modem UART Line Timer", pdMS_TO_TICKS(100), pdFALSE, (void *)0, els31uart_lineTimerCallback, &lineTimerStatic)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	if ((atCmdTimer = xTimerCreateStatic("Modem UART AT Command Timer", pdMS_TO_TICKS(200), pdFALSE, (void *)0, els31uart_atCmdTimerCallback, &atCmdTimerStatic)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	// Start execution.
	if((m_els31uart_thread = xTaskCreateStatic(els31uart_thread, "ELS31 UART", ELSUART_STACK_SIZE, NULL, 1, m_els31uart_threadBuffer, &m_els31uart_threadStatic)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	return true;
}

void els31uart_initUART(void) {
	uint32_t err_code;
	// Initialize the UART
	if (!m_isInit) {
		APP_UART_FIFO_INIT(
				&m_comm_params,
				UART_RX_BUF_SIZE,
				UART_TX_BUF_SIZE,
				(app_uart_event_handler_t )els31uart_callback,
				APP_IRQ_PRIORITY_LOW,
				err_code);
		APP_ERROR_CHECK(err_code);
		
		//nrf_gpio_pin_clear(CELL_RTS_PIN);
		
		m_isInit = true;

		nrf_gpio_pin_clear(CELL_RTS_PIN);

		SEGGER_RTT_printf(0, "UART enabled.\r\n");
	} else {
		SEGGER_RTT_printf(0, "WARN: UART already enabled.\r\n");
	}
}

bool els31uart_deinitUART(void) {
	if (m_isInit) {
		app_uart_close();

		nrf_gpio_pin_clear(CELL_TXD_PIN);
		nrf_gpio_cfg_output(CELL_TXD_PIN);

		nrf_gpio_pin_clear(CELL_RTS_PIN);
		nrf_gpio_cfg_output(CELL_RTS_PIN);

		nrf_gpio_cfg_input(CELL_RXD_PIN, NRF_GPIO_PIN_PULLDOWN);
		nrf_gpio_cfg_input(CELL_CTS_PIN, NRF_GPIO_PIN_PULLDOWN);

		m_isInit = false;
		SEGGER_RTT_printf(0, "UART disabled.\r\n");
	} else {
		SEGGER_RTT_printf(0, "WARN: UART already disabled.\r\n");
	}
	return true;
}

void els31uart_thread(void *arg) {

	uint8_t rxByte;
	els31uart_event_t event;

	// Initialize the UART
	els31uart_initUART();

	/* Clear RTS to put it in a known state */
	nrf_gpio_pin_clear(CELL_RTS_PIN);
	nrf_gpio_cfg_output(CELL_RTS_PIN);


	simplehsm_initialize(&els31uart_hsm, state_top);

	SEGGER_RTT_printf(0, "ELS31 UART thread started\r\n");
	while (1) {

		while ((!m_event_queue) || (xQueueReceive(m_event_queue, (void *)&event, portMAX_DELAY) != pdTRUE));

		switch (event.eventType) {
		case UARTRxEvent:
			/*
			 * Grab as many characters as are available and pass them to the
			 * state machine.
			 */
			while (app_uart_get(&rxByte) == NRF_SUCCESS) {
				simplehsm_signal_current_state(&els31uart_hsm, SIG_RX_CHAR, &rxByte);
			}
			break;
		case TimerExpiredEvent:
			simplehsm_signal_current_state(&els31uart_hsm, SIG_TIMEOUT, NULL);
			break;
		case ATCmdEvent:
			simplehsm_signal_current_state(&els31uart_hsm, SIG_AT_CMD, NULL);
			break;
		case UartSleepEvent:
			simplehsm_signal_current_state(&els31uart_hsm, SIG_SLEEP_UART, NULL);
			break;
		case UartWakeEvent:
			simplehsm_signal_current_state(&els31uart_hsm, SIG_WAKE_UART, NULL);
			break;
		default:
			SEGGER_RTT_printf(0, "ERROR: Unknown event type (%d) in ELS31 UART event queue\r\n", event.eventType);
			break;
		}
	}
}

bool els31uart_sendATCmd(const uint8_t *cmd, uint16_t cmdLength, TickType_t xTicksToWait) {
	els31uart_event_t event;

	// Ensure that there's room available in our local buffer
	if (cmdLength > sizeof(m_pendingATCmd)) {
		return false;
	}

	/*
	 *  This semaphore prevents multiple AT commands from being queued simultaneously.
	 */
	if ((!m_at_cmd_smphr) || (xSemaphoreTake(m_at_cmd_smphr, xTicksToWait) != pdTRUE)) {
		return false;
	}

	memcpy(m_pendingATCmd, cmd, cmdLength);
	m_pendingATCmdLength = cmdLength;
	m_pendingATCmdIndex = 0;

	event.eventType = ATCmdEvent;
	event.data = 0;

	if ((!m_event_queue) || (xQueueSend(m_event_queue, &event, xTicksToWait) != pdTRUE)) {
		return false;
	}

	return true;;
}

bool els31uart_sleepUart(TickType_t xTicksToWait) {
	els31uart_event_t event;
	event.eventType = UartSleepEvent;
	event.data = 0;
	if ((!m_event_queue) || (xQueueSend(m_event_queue, &event, xTicksToWait) != pdTRUE)) {
		return false;
	}
	return true;
}

bool els31uart_wakeUart(TickType_t xTicksToWait) {
	els31uart_event_t event;
	event.eventType = UartWakeEvent;
	event.data = 0;
	if ((!m_event_queue) || (xQueueSend(m_event_queue, &event, xTicksToWait) != pdTRUE)) {
		return false;
	}
	return true;
}

stnext state_top(int signal, void *param) {

#if (defined(DEBUG_ELS31UART_HSM) && (DEBUG_ELS31UART_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s\r\n", __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_INIT:
    	// When this timer expires, the task will be able to accept a AT command
    	xTimerStart(atCmdTimer, portMAX_DELAY);
    	simplehsm_init_transition_state(&els31uart_hsm, state_tx);
    	return stnone;
    case SIG_WAKE_UART:
    	simplehsm_transition_state(&els31uart_hsm, state_enable_uart);
		return stnone;
    case SIG_SLEEP_UART:
    	simplehsm_transition_state(&els31uart_hsm, state_disable_uart);
    	return stnone;
    case SIG_EXIT:
    	return stnone;
    case SIG_AT_CMD:
    	m_isATCmdPending = true;
    	return stnone;
	}

	return stnone;
}

stnext state_tx(int signal, void *param) {
#if (defined(DEBUG_ELS31UART_HSM) && (DEBUG_ELS31UART_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s\r\n", __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_DEEPHIST:
        return stdeephist;
    case SIG_INIT:
    	simplehsm_init_transition_state(&els31uart_hsm, state_tx_idle);
    	return stnone;
    case SIG_EXIT:
    	return stnone;
	}

	return state_top;
}

stnext state_tx_idle(int signal, void *param) {
#if (defined(DEBUG_ELS31UART_HSM) && (DEBUG_ELS31UART_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s\r\n", __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_INIT:
    	m_responseLength = 0;
    	if (m_isATCmdPending) {
    		m_isATCmdPending = false;
    		simplehsm_transition_state(&els31uart_hsm, state_tx_first);
    	}
    	return stnone;
    case SIG_EXIT:
    	return stnone;
    case SIG_RX_CHAR:
    	m_response[m_responseLength++] = *(uint8_t *)param;
		simplehsm_transition_state(&els31uart_hsm, state_rxline);
    	return stnone;
    case SIG_AT_CMD:
    	simplehsm_transition_state(&els31uart_hsm, state_tx_first);
    	return stnone;
    }

	return state_tx;
}


stnext state_tx_first(int signal, void *param) {
#if (defined(DEBUG_ELS31UART_HSM) && (DEBUG_ELS31UART_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s\r\n", __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_INIT:
    	m_pendingATCmdIndex = 0;
    	app_uart_put(m_pendingATCmd[m_pendingATCmdIndex++]);

    	/* Allow 500ms for the modem to echo each character */
    	xTimerChangePeriod(lineTimer, pdMS_TO_TICKS(500), portMAX_DELAY);
    	xTimerStart(lineTimer, portMAX_DELAY);

    	simplehsm_transition_state(&els31uart_hsm, state_tx_remainder);
    	return stnone;
    case SIG_EXIT:
    	return stnone;
	}

	return state_tx;
}


stnext state_tx_remainder(int signal, void *param) {
	uint8_t c;

#if (defined(DEBUG_ELS31UART_HSM) && (DEBUG_ELS31UART_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s\r\n", __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_INIT:
    	return stnone;
    case SIG_EXIT:
    	return stnone;
    case SIG_RX_CHAR:
    	c = *(uint8_t *)param;

    	// Stop timer every time character is received
    	xTimerStop(lineTimer, portMAX_DELAY);

    	// Record the received character
    	if (m_responseLength < sizeof(m_response)) {
    		m_response[m_responseLength++] = c;
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: No room in m_modemResponse for echoed command\r\n");
    	}

    	if ((m_pendingATCmdIndex == 1) && (c != m_pendingATCmd[m_pendingATCmdIndex - 1])) {
    		/*
    		 * The first received character did not match what we transmitted,
    		 * so we assume that we are receiving a URC.  We'll return to this
    		 * state after we have completed receiving the URC.
    		 */
    		simplehsm_transition_state(&els31uart_hsm, state_rxline);
    		return stnone;
    	}

    	if (c != m_pendingATCmd[m_pendingATCmdIndex - 1]) {
    		SEGGER_RTT_printf(0, "ERROR: Character echoed from modem (%c) does not match character transmitted (%d)\r\n", c, m_pendingATCmd[m_pendingATCmdIndex]);
    	}

		if (m_pendingATCmdIndex >= m_pendingATCmdLength) {
#if (defined(DEBUG_PRINT) && (DEBUG_PRINT == 1))
			// Entire AT command has be transmitted and echoed
			els31uart_prettyPrint((char *)m_response, m_responseLength, "MODEM COMMAND");
#endif

			modem_uart_response_t response;
			if (m_responseLength + 1 <= sizeof(response.data)) {
				response.type = 'C';
				response.length = m_responseLength;
				memcpy(response.data, m_response, m_responseLength);
				// Null terminate the response to allow for processing as a string
				response.data[m_responseLength] = '\0';

				modem_queueUartResponse(&response);
			}

			xTimerStart(atCmdTimer, portMAX_DELAY);
			simplehsm_transition_state(&els31uart_hsm, state_tx_idle);
		} else {
			// Send next character
			app_uart_put(m_pendingATCmd[m_pendingATCmdIndex++]);

			// Give modem another 500ms to respond
			xTimerChangePeriod(lineTimer, pdMS_TO_TICKS(500), portMAX_DELAY);
			xTimerStart(lineTimer, portMAX_DELAY);
		}

    	return stnone;
    case SIG_TIMEOUT:
    	SEGGER_RTT_printf(0, "ERROR: Timeout while waiting for modem to echo input\r\n");
    	errorCount_incrementCount(ERROR_MODEM_NO_RESPONSE_TO_SENT_DATA);
    	xTimerStart(atCmdTimer, portMAX_DELAY);
		simplehsm_transition_state(&els31uart_hsm, state_tx_idle);

    	return stnone;
	}

	return state_tx;
}


stnext state_rxline(int signal, void *param) {
	uint8_t c;

#if (defined(DEBUG_ELS31UART_HSM) && (DEBUG_ELS31UART_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s\r\n", __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_INIT:
		/*
		 * Once the modem starts sending characters, give it 1s to finish
		 * the line (i.e. send a \r\n sequence).
		 */
		xTimerChangePeriod(lineTimer, pdMS_TO_TICKS(1000), portMAX_DELAY);
		xTimerStart(lineTimer, portMAX_DELAY);

    	return stnone;
    case SIG_EXIT:
    	m_responseLength = 0;
    	return stnone;
    case SIG_RX_CHAR:
    	c = *(uint8_t *)param;

    	if (m_responseLength < sizeof(m_response)) {
    		m_response[m_responseLength++] = c;
    	} else {
    		SEGGER_RTT_printf(0, "ERROR: No room in m_modemResponse for modem's response\r\n");
    		/*
    		 * Always keep the last two characters received so that we can
    		 * check for the <CR><LF> that should terminate every line.
    		 */
    		m_response[sizeof(m_response) - 2] = m_response[sizeof(m_response) - 1];
    		m_response[sizeof(m_response) - 1] = c;
    	}

    	if ((m_response[m_responseLength - 2] == '\r') && (m_response[m_responseLength - 1] == '\n')) {
    		/* Stop the timer so that we do not get an unexpected signal */
    		xTimerStop(lineTimer, portMAX_DELAY);
#if (defined(DEBUG_PRINT) && (DEBUG_PRINT == 1))
    		/* Print the modem's response */
    		els31uart_prettyPrint((char *)m_response, m_responseLength, "MODEM RESPONSE");
#endif

    		/* Queue the modem's response */
			modem_uart_response_t response;
			if (m_responseLength + 1 <= sizeof(response.data)) {
				response.type = 'R';
				response.length = m_responseLength;
				memcpy(response.data, m_response, m_responseLength);
				// Null terminate the response to allow for processing as a string
				response.data[m_responseLength] = '\0';

				if (!modem_queueUartResponse(&response)) {
					SEGGER_RTT_printf(0, "ERROR: Failed to queue UART response from %s\r\n", __FUNCTION__);
				}
			}

    		/* Return to the idle state using deephistory */
    		simplehsm_transition_state_ex(&els31uart_hsm, state_tx, true);
    	}

    	return stnone;
    case SIG_TIMEOUT:
    	SEGGER_RTT_printf(0, "ERROR: Timeout while receiving line from modem\r\n");

    	simplehsm_transition_state_ex(&els31uart_hsm, state_tx, true);
    	return stnone;
	}

	return state_top;
}




static stnext state_disable_uart(int signal, void *param) {
#if (defined(DEBUG_ELS31UART_HSM) && (DEBUG_ELS31UART_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s\r\n", __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_DEEPHIST:
        return stdeephist;
    case SIG_INIT:
    	els31uart_deinitUART();
    	simplehsm_init_transition_state(&els31uart_hsm, state_top);
	 	return stnone;
    case SIG_EXIT:
    	return stnone;
	}

	return state_top;
}

static stnext state_enable_uart(int signal, void *param) {
#if (defined(DEBUG_ELS31UART_HSM) && (DEBUG_ELS31UART_HSM == 1))
	if (signal != SIG_NULL) {
		printf("SHSM: %s - %s\r\n", __FUNCTION__, m_signalNames[signal]);
	}
#endif

	switch (signal) {
	case SIG_ENTRY:
		return stnone;
    case SIG_DEEPHIST:
        return stdeephist;
    case SIG_INIT:
    	els31uart_initUART();
    	simplehsm_init_transition_state(&els31uart_hsm, state_top);
    	return stnone;
    case SIG_EXIT:
    	return stnone;
	}

	return state_top;
}




app_uart_event_handler_t els31uart_callback(app_uart_evt_t * p_event) {
	BaseType_t yield_req = pdFALSE;
	els31uart_event_t event;

	switch (p_event->evt_type) {
	case APP_UART_DATA_READY:
		// The data is available in the FIFO and can be fetched using app_uart_get().
		event.eventType = UARTRxEvent;
		xQueueSendFromISR(m_event_queue, &event, &yield_req);
		portYIELD_FROM_ISR(yield_req);
		break;
	case APP_UART_FIFO_ERROR:
		// The FIFO error code is stored in app_uart_evt_t.data.error_code field.
		SEGGER_RTT_printf(0, "ERROR: FIFO error (%d) received by els31uart_callback()\r\n", p_event->data.error_code);
		break;
	case APP_UART_COMMUNICATION_ERROR:
		//The error is stored in app_uart_evt_t.data.error_communication field.
		SEGGER_RTT_printf(0, "ERROR: Communication error (%d) received by els31uart_callback()\r\n", p_event->data.error_communication);
		break;
	case APP_UART_TX_EMPTY:
		break;
	case APP_UART_DATA:
		SEGGER_RTT_printf(0, "ERROR: Unexpected APP_UART_DATA event received by els31uart_callback()\r\n");
		break;
	default:
		SEGGER_RTT_printf(0, "ERROR: Unknown evt_type (%d) received by els31uart_callback()\r\n", p_event->evt_type);
		break;
	}

	return NRF_SUCCESS;
}

void els31uart_lineTimerCallback(TimerHandle_t timer) {
	els31uart_event_t event;

	event.eventType = TimerExpiredEvent;
	event.data = 0;

	if ((!m_event_queue) || (xQueueSend(m_event_queue, &event, 0) != pdTRUE)) {
		xTimerChangePeriod(lineTimer, pdMS_TO_TICKS(100), portMAX_DELAY);
		xTimerStart(lineTimer, portMAX_DELAY);
	}
}

static void els31uart_atCmdTimerCallback(TimerHandle_t timer) {
	/*
	 * Give the semaphore that allows another AT command to be queued.
	 */
	if (m_at_cmd_smphr) {
		xSemaphoreGive(m_at_cmd_smphr);
	}
}

void els31uart_prettyPrint(const char *str, uint16_t strLength, const char *header) {
	uint16_t i = 0;
	bool newline = true;

	if (strlen(header) > 0) {
		SEGGER_RTT_printf(0, "<%s>\r\n", header);
	}

	while (i < strLength) {
		if (newline) {
			SEGGER_RTT_Write(0, "\t", 1);
			newline = false;
			continue;
		} else if ((' ' <= str[i]) && (str[i] <= '~')) {
			SEGGER_RTT_Write(0, &str[i], 1);
			i++;
		} else if ((str[i] == '\r')) {
			SEGGER_RTT_Write(0, "\\r", 2);
			if (str[i+1] == '\n') {
				SEGGER_RTT_Write(0, "\\n", 2);
				i++;
			}
			SEGGER_RTT_Write(0, "\r\n", 2);
			newline = true;
			i++;
		} else if ((str[i] == '\n')) {
			SEGGER_RTT_Write(0, "\\n", 2);
			if (str[i+1] == '\r') {
				SEGGER_RTT_Write(0, "\\r", 2);
				i++;
			}
			SEGGER_RTT_Write(0, "\r\n", 2);
			newline = true;
			i++;
		} else {
			SEGGER_RTT_printf(0, "\\x%02X", str[i]);
			i++;
		}
	}

	if (strlen(header) > 0) {
		SEGGER_RTT_printf(0, "</%s>\r\n", header);
	}
}
