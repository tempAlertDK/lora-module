/*
 * util.h
 *
 *  Created on: Nov 13, 2013
 *      Author: kwgilpin
 */

#ifndef UTIL_H_
#define UTIL_H_

#include <stdint.h>
#include <stdbool.h>
#include "SEGGER_RTT.h"

extern uint8_t debug_level;

#define NO_DEBUG       0
#define ERROR_DEBUG    1
#define BASIC_DEBUG    2
#define DETAILED_DEBUG 3
#define ALL_DEBUG      4

#define DEBUGMSG(level, action) if(level<=debug_level) { action };

#define LOGGER(...) SEGGER_RTT_printf(__VA_ARGS__)
#if defined(DEBUG) && DEBUG == 1
#define DBLOGGER(...) LOGGER(__VA_ARGS__)
#else
#define DBLOGGER(...) ;
#endif

#define LOGGER(...) SEGGER_RTT_printf(__VA_ARGS__)


#define LOGGER_FLUSH()

uint32_t get_time_100ms(void);

#endif /* UTIL_H_ */
