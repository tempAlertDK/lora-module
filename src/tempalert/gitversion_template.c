#include "gitversion.h"
#include "log.h"

const char gitVersionStr[] = "$GIT_VERSION_STR$";

void log_version(void) {
    log_infof("CellNode server %s, version %u.%u, Build %s", TARGET_SERVER, MAJOR_VERSION, MINOR_VERSION, gitVersionStr);
}

