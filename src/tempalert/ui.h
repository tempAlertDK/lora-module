/*
 * ui.h
 *
 *  Created on: Jan 13, 2017
 *      Author: Nate-Tempalert
 */

#ifndef SRC_TEMPALERT_UI_H_
#define SRC_TEMPALERT_UI_H_

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "SEGGER_RTT.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "portmacro_cmsis.h"
#include "event_groups.h"


typedef enum {
	LCD_ON = 0,
	LCD_BLINK_2000MS,
	LCD_BLINK_1000MS,
	LCD_BLINK_500MS,
	LCD_OFF
} lcd_display_type_t;

typedef struct {
	lcd_display_type_t blink;
	char data[3];
} LcdData_t;

bool ui_init(void);
bool ui_deinit(void);
void ui_buttonPress(void);
bool ui_lcdUpdate(char * str);

#endif /* SRC_TEMPALERT_UI_H_ */
