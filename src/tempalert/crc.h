/* 
 * File:   crc.h
 * Author: kwgilpin
 *
 * Created on January 13, 2013, 10:48 AM
 */

#ifndef CRC_H
#define	CRC_H

#include <stdint.h>

uint8_t updateCRC(uint8_t runningCRC, uint8_t nextByte);
uint8_t computeCRC(uint8_t *bytes, uint8_t byteCount);

#endif	/* CRC_H */

