/*
 * twi_register.h
 *
 *  Created on: Jan 15, 2017
 *      Author: kwgilpin
 */

#ifndef SRC_TEMPALERT_TWI_REGISTER_H_
#define SRC_TEMPALERT_TWI_REGISTER_H_

#include <stdint.h>
#include <stdbool.h>

#ifdef BOARD_CELL_NODE_REV1P0
#define TWI_CONFIG config
#elif defined (BOARD_CELL_NODE_REV1P1) | defined (BOARD_LORA_MODULE_REV1P0)
#define TWI_CONFIG config_ext
#else
#error board file is undefined!
#endif

bool twiRegister_read(const nrf_drv_twi_config_t *config, uint8_t addr, uint8_t reg, uint8_t *data);
bool twiRegister_readData(const nrf_drv_twi_config_t *config, uint8_t addr, uint8_t reg, uint8_t *data, uint8_t dataLen);
bool twiRegister_write(const nrf_drv_twi_config_t *config, uint8_t addr, uint8_t reg, uint8_t data);
bool twiRegister_writeData(const nrf_drv_twi_config_t *config, uint8_t addr, uint8_t *data, uint8_t dataLen);
bool twiRegister_setBits(const nrf_drv_twi_config_t *config, uint8_t addr, uint8_t reg, uint8_t bits);
bool twiRegister_clearBits(const nrf_drv_twi_config_t *config, uint8_t addr, uint8_t reg, uint8_t bits);

extern nrf_drv_twi_config_t const config;
#if defined (BOARD_CELL_NODE_REV1P1) |  defined (BOARD_LORA_MODULE_REV1P0)
extern nrf_drv_twi_config_t const config_ext;
#endif

#endif /* SRC_TEMPALERT_TWI_REGISTER_H_ */
