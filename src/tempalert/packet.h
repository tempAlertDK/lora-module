/*
 * packet.h
 *
 *  Created on: Feb 9, 2017
 *      Author: Nate-Tempalert
 */

#ifndef SRC_TEMPALERT_PACKET_H_
#define SRC_TEMPALERT_PACKET_H_

#include <stdint.h>
#include <string.h>
#include "genericQueue.h"
#include "stoFwd.h"
#include "defines.h"

#define AGE_MESSAGE_ID							0x40

#define BATTERY_STATE_MESSAGE_ID				0x50
#define RSSI_MESSAGE_ID							0x52
#define SEQUENCE_NUMBER_MESSAGE_ID				0x55

#define SENSORSN_MESSAGE_ID						0x56
#define SET_SENSORSN_STATE_MESSAGE_ID			0x57

#define ENABLE_VBV_MESSAGE_ID					0x62
#define FORCE_DNS_LOOKUP_MESSAGE_ID				0x64

#define PROTOCOL_VERSION_MESSAGE_ID				0x66
#define HARDWARE_FIRMWARE_VERSION_MESSAGE_ID	0x67

#define MEASUREMENT_INTERVAL_HRS_MESSAGE_ID		0x68
#define MEASUREMENT_INTERVAL_MINS_MESSAGE_ID	0x69

#define RUN_GATEWAY_BOOTLOADER_MESSAGE_ID		0x70
#define SET_STOFWD_STATE_MESSAGE_ID				0x71
#define INITIALIZATION_MESSAGE_ID				0x75
#define ALARM_STATE_MESSAGE_ID					0x79

#define ALARM_PORTS_MESSAGE_ID					0x80
#define ALARM_ACKNOWLEDGEMENT_MESSAGE_ID		0x81
#define ACKNOWLEDGE_MESSAGE_ID					0x83
#define DOWNLOAD_FIRMWARE_MESSAGE_ID			0x8C
#define NODE_FIRMWARE_REQUEST_MESSAGE_ID		0x8D

#define LONG_ERROR_REPORT_MESSAGE_ID			0x96
#define ERROR_REPORT_MESSAGE_ID					0x99
#define UNIX_TIME_REQUEST_MESSAGE_ID			0x9A
#define UNIX_TIME_RESPONSE_MESSAGE_ID			0x9B

#define ZPOINT_DIAGNOSTIC_MESSAGE_ID			0x9D
#define ROUTE_MESSAGE_ID						0x9E
#define CELL_ID_MESSAGE_ID						0x9F

#define DISPLAY_MODE_MESSAGE_ID					0xB0

#define SUBMEASUREMENT_INTERVAL_MESSAGE_ID		0xB1


#define SENSORSN_LENGTH							7

#define CRC_LENGTH								1

/* Message subtypes for the error report message (0x99) */
#define ERROR_REPORT_ERROR_COUNTS_1_SUBMESSAGE_ID				0x64 // 100
#define ERROR_REPORT_ERROR_COUNTS_2_SUBMESSAGE_ID				0x65 // 101
#define ERROR_REPORT_ERROR_COUNTS_3_SUBMESSAGE_ID				0x66 // 102
#define ERROR_REPORT_RAW_RSSI_LQI_SUBMESSAGE_ID				0x78 // 120
#define ERROR_REPORT_CSQ_SUBMESSAGE_ID						0x79 // 121
#define	ERROR_REPORT_GATEWAY_CHANNEL_RSSI_LQI_SUBMESSAGE_ID	0x7A // 122
#define ERROR_REPORT_DISCARDED_STACK_COUNT_SUBMESSAGE_ID		0x82 // 130
#define ERROR_REPORT_SECONDS_SINCE_RESET_SUBMESSAGE_ID		0x8C // 140
#define ERROR_REPORT_BOOTLOADER_STATUS_SUBMESSAGE_ID			0xC8 // 200
#define ERROR_REPORT_CAPWAND_SENDOR_ERROR_SUBMESSAGE_ID		0xC9	 // 201
#define ERROR_REPORT_UPLOAD_TRIGGER_EVENT_SUBMESSAGE_ID		0xF0 // 240

/* Message subtypes for the long error report message (0x96) */
#define LONG_ERROR_REPORT_STOFWD_STATUS_SUBMESSAGE_ID			0xE0 	// 224
#define LONG_ERROR_REPORT_ZPOINT_CHANNEL_SCAN3_SUBMESSAGE_ID 	0xD5 	//
#define LONG_ERROR_REPORT_ERROR_COUNTS_2_SUBMESSAGE_ID		0x65 	// 101
#define ERROR_REPORT_ERROR_COUNTS_3_SUBMESSAGE_ID				0x66		// 102
#define LONG_ERROR_REPORT_CELLNODE_SUBMESSAGE_ID				0xE0 	// 224
#define LONG_ERROR_REPORT_DYNAMIC_INTERVAL_STATUS				0xF1		// 241
#define LONG_ERROR_REPORT_BATTERY_TEMPERATURE_STATUS			0xF3		// 243

extern bool g_firstPacket;

typedef struct {
	TickType_t created;
	bool firstPacket;
	bool full;
	bool crc;
	uint8_t sf_records;			// Number of S&F records in packet (Used for deconstruction)
	uint16_t dataLength;
	uint8_t data[PACKET_MAX_LENGTH+CRC_LENGTH];
} packet_t;

//bool packet_build(uint8_t * p_packet, uint16_t * p_packetLen, packetStruct_t * packet_t);
bool packet_init(packet_t * packet);
bool packet_appendMessage(packet_t * packet, sf_record_t * gQueueData);
void packet_print(uint8_t * packet, uint16_t packetLen);
void packet_updateCRC(uint8_t *p_packet, uint16_t packetLen);
bool packet_verifyCRC(uint8_t *p_packet, uint16_t packetLen);
bool packet_appendSfRecord (packet_t * packet, sf_record_t * sfrec);
//bool packet_copySfRecord(sf_record_t * sfrec, packet_t * packet);

#endif /* SRC_TEMPALERT_PACKET_H_ */
