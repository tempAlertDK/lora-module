/*
 * power.h
 *
 *  Created on: Feb 10, 2017
 *      Author: Nate-Tempalert
 */

#ifndef SRC_TEMPALERT_POWER_H_
#define SRC_TEMPALERT_POWER_H_

bool power_init(void);
bool power_readBattery(uint16_t * batteryVoltage_mV);
bool power_acPowerGood(void);
bool power_batteryState(int8_t * batteryState, uint16_t batteryVoltage_mv);
bool power_getBatteryLow(bool *batteryLow, bool verbose);
bool power_checkBatteryVoltageLow(uint16_t vbat_mV);

#endif /* SRC_TEMPALERT_POWER_H_ */
