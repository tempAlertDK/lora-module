/*
 * spi_mtx.h
 *
 *  Created on: Jan 12, 2017
 *      Author: kwgilpin
 */

#ifndef SRC_TEMPALERT_SPI_SMPHR_H_
#define SRC_TEMPALERT_SPI_SMPHR_H_

#include "FreeRTOS.h"
#include "portmacro_cmsis.h"

#define SPI_MTX_DEFAULT_DELAY		pdMS_TO_TICKS(1000)

void spi_mtx_init(void);
uint8_t spi_mtx_take(TickType_t ticksToWait);
void spi_mtx_give(void);
void spi_mtx_restore(const uint8_t *taken);

#define WITH_SPI_MTX_CUSTOM_DELAY(ticks) for (uint8_t taken __attribute__((__cleanup__(spi_mtx_restore))) = spi_mtx_take(ticks), __ToDo = 1; __ToDo; __ToDo = 0)
#define WITH_SPI_MTX_DEFAULT_DELAY() for (uint8_t taken __attribute__((__cleanup__(spi_mtx_restore))) = spi_mtx_take(SPI_MTX_DEFAULT_DELAY), __ToDo = 1; __ToDo; __ToDo = 0)

#define GET_MACRO(_0, _1, NAME, ...) NAME
#define WITH_SPI_MTX(...) GET_MACRO(_0, ##__VA_ARGS__, WITH_SPI_MTX_CUSTOM_DELAY, WITH_SPI_MTX_DEFAULT_DELAY)(__VA_ARGS__)


#endif /* SRC_TEMPALERT_SPI_SMPHR_H_ */
