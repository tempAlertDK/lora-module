/*
 * ui.c
 *
 *  Created on: Jan 13, 2017
 *      Author: Nate-Tempalert
 */

#include "ui.h"

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "SEGGER_RTT.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "twi_mtx.h"
#include "semphr.h"
#include "portmacro_cmsis.h"
#include "bsp.h"
#include "boards.h"
#include "capwand.h"
#include "pca8561.h"
#include "globals.h"
#if defined(MODEM_TESTING) && (MODEM_TESTING == 1)
#include "modem.h"
#endif

#define LED_BLINK_ON_TIME				pdMS_TO_TICKS(20)		// ms to keep led(s) on

#define LED_SLOW_BLINK_OFF_TIME pdMS_TO_TICKS(15980);
#define LED_FAST_BLINK_OFF_TIME pdMS_TO_TICKS(480);

void uiLedTimerExpired(TimerHandle_t xTimer);
static xTimerHandle m_led_timer;
static StaticTimer_t m_led_timerStatic;
static uint8_t m_led_timer_id = 1;

typedef enum {
	LED_RED_SLOW_BLINK,
	LED_GREEN_SLOW_BLINK,
	LED_GREEN_FAST_BLINK,
	LED_OFF
} LedStates_t;

TickType_t m_currentDelay = LED_BLINK_ON_TIME;
LedStates_t m_currentState, m_previousState;

bool m_buttonPressed = false;

bool ui_init(void) {

	WITH_TWI_MTX() {
		/* configure display and set to CAPWAND_INVALID_READING */
		pca8561_configure();
		pca8561_updateDisplay("--");
	}
	vTaskDelay(500);

	m_led_timer = (xTimerCreateStatic("LEDU",100,pdFALSE,&m_led_timer_id,uiLedTimerExpired, &m_led_timerStatic ));
	if (m_led_timer == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	xTimerStart(m_led_timer, pdMS_TO_TICKS(100));
	SEGGER_RTT_printf(0, "UI TIMER STARTED\r\n");

	return true;
}

bool ui_deinit (void) {
	/* Attempt to stop the timer, clear the LCD screen and disable it. */
	if (pdFAIL == xTimerStop(m_led_timer, pdMS_TO_TICKS(1000))) {
		return false;
	}

	WITH_TWI_MTX() {
		pca8561_updateDisplay("   ");
		vTaskDelay(10);
		pca8561_disableDisplay();
	}
	/* Turn the LEDs off */
	LEDS_OFF(LEDS_MASK);
	m_currentState = LED_OFF;
	return true;
}

void ui_updateTimer(void) {
	xTimerChangePeriod(m_led_timer, m_currentDelay, pdMS_TO_TICKS(100));
}

void ui_updateLEDs() {

	LEDS_OFF(LEDS_MASK);
	m_previousState = m_currentState;
	if (m_previousState == LED_OFF) {
		/* Update new state if led was off previously */
		if (m_buttonPressed){
			m_buttonPressed = false;
			m_currentState = LED_GREEN_FAST_BLINK;
		} else if (g_uploadInprogress && g_sc_transmit) {
			m_currentState = LED_GREEN_FAST_BLINK;
		} else if (g_lastUploadFailure) {
			m_currentState = LED_RED_SLOW_BLINK;
		} else {
			m_currentState = LED_GREEN_SLOW_BLINK;  // NR @ 10/24/17 to appease Praxair request
		}

#if defined(MODEM_TESTING) && (MODEM_TESTING == 1)
		 if (g_modemTestState == MODEM_TESTMODE_ON) {
			 m_currentState = LED_GREEN_SLOW_BLINK;
		 } else if(g_modemTestState == MODEM_TESTMODE_BOOTING) {
			 m_currentState = LED_GREEN_FAST_BLINK;
		 } else if (g_modemTestState == MODEM_TESTMODE_OFF) {
			 m_currentState = LED_RED_SLOW_BLINK;
		 }
#endif

	} else {
		/* Otherwise turn the LEDs off. */
		m_currentState = LED_OFF;
	}

	/* based on next state, update LED outputs and
	 * set timer timeout */
	m_currentDelay = LED_BLINK_ON_TIME;
	switch (m_currentState) {
	case LED_RED_SLOW_BLINK:
		LEDS_ON(BSP_LED_0_MASK);
		break;
	case LED_GREEN_SLOW_BLINK:
	case LED_GREEN_FAST_BLINK:
		LEDS_ON(BSP_LED_1_MASK);
		break;
	case LED_OFF:
		if (m_previousState == LED_GREEN_FAST_BLINK) {
			m_currentDelay = LED_FAST_BLINK_OFF_TIME;
		} else {
			m_currentDelay = LED_SLOW_BLINK_OFF_TIME;
		}
		break;
	default:
		break;
	}
}


void ui_buttonPress(void) {
	/* Set button pressed flag to indicate a quick flash
	 * to the user to provide visual feedback of button
	 * interaction. */
	m_buttonPressed = true;
	m_currentState = LED_OFF;
	ui_updateLEDs();
	ui_updateTimer();
}


bool ui_lcdUpdate(char * str) {
	bool success = false;
	WITH_TWI_MTX() {
		success = pca8561_updateDisplay(str);
	}
	return success;
}

void uiLedTimerExpired(TimerHandle_t xTimer) {
	/* On interval, update LED status and reset the timer with new interval */
	ui_updateLEDs();
	ui_updateTimer();
}


