/*
 * sensors.c
 *
 *  Created on: Jan 13, 2017
 *      Author: Nate-Tempalert
 */

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "SEGGER_RTT.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "twi_mtx.h"
#include "semphr.h"
#include "portmacro_cmsis.h"
#include "ble_levelSensor.h"
#include "bsp.h"
#include "boards.h"
#include "nrf_gpio.h"
#include "sensors.h"
#include "packet.h"
#include "si705x.h"
#include "ads7866.h"
#include "capwand.h"
#include "power.h"
#include "deviceID.h"
#include "stoFwd.h"
#include "event_groups.h"
#include "scController.h"
#include "modem.h"
#include "ui.h"
#include "extsensor.h"
#include "unixTime.h"
#include "notify.h"
#include "log.h"
#include "battTemp.h"

/* Sensors task gQ variables */

#define SENSORSCTL_QUEUE_LENGTH	3
#define SENSORSCTL_QUEUE_ITEMSIZE sizeof(sensors_control_message_t)
uint8_t sensors_controlQueueBuffer[SENSORSCTL_QUEUE_LENGTH*SENSORSCTL_QUEUE_ITEMSIZE];
static StaticQueue_t sensors_controlQueueStatic;
QueueHandle_t sensors_controlQueue;

QueueHandle_t sensors_sfQueue;

EventGroupHandle_t xSensorsEventGroup = NULL;
StaticEventGroup_t xSensorsEventGroupStatic;
EventBits_t xSensorsGroupBitMask = 0xFF;
EventBits_t xSensorsGroupBits = 0x00;

TaskHandle_t m_sensors_thread; /**< Definition of BLE stack thread. */

TickType_t m_sensorsMeasurementInterval_ms = 15 * 60 * 1000;  // once every 15 mins DEFAULT

TimerHandle_t m_sensorsMiTimer;
StaticTimer_t m_sensorsMiTimerStatic;
bool m_measurementIntervalRequested = false;

/* Attached sensor flags
 *  1 == present
 *  0 == not detected since reset
 * -1 == was detected since reset but not in last mi
 */

static bool m_validBatteryReading = false;
SemaphoreHandle_t g_recentSensorData_smphr;
StaticSemaphore_t g_recentSensorData_smphr_buffer;
sensorData_t m_sensorTask_lastMeasurement;
sensorData_t m_sensorTask_lastValidMeasurement;
sensorData_t m_sensorTask_lastUploadedMeasurement;
bool m_sensorTask_uploadHasOccured = false;
statusData_t m_sensorTask_lastStatus;

// dynamic level sensor metrics

typedef enum {
	// Non Triggers
	DYNAMIC_INTERVAL_SKIPPED_LEVEL_BELOW_LOW_THRESHOLD = -8,
	DYNAMIC_INTERVAL_SKIPPED_LOW_LEVEL_WAIT_PERIOD = -7,
	DYNAMIC_INTERVAL_SKIPPED_RATE_CHANGE_NOT_EXCEEDED = -6,
	DYNAMIC_INTERVAL_SKIPPED_LAST_LEVEL_ABOVE_WARNING_THREASHOLD = -5,
	DYNAMIC_INTERVAL_SKIPPED_UPLOAD_INPROGRESS = -4,
	DYNAMIC_INTERVAL_SKIPPED_VALID_DATA_NOT_TRANSMISSION = -3,
	DYNAMIC_INTERVAL_SKIPPED_UNIXTIME_NOT_VALID = -2,
	DYNAMIC_INTERVAL_SKIPPED_LEVEL_NOT_VALID = -1,
	// Triggers
	DYNAMIC_INTERVAL_UNKNOWN_RESULT = 0,
	DYNAMIC_INTERVAL_RATE_CHANGE_EXCEEDED = 1,
	DYNAMIC_INTERVAL_LAST_LEVEL_TRIGGER_LOW_THRESHOLD = 2,
	DYNAMIC_INTERVAL_LEVEL_LOW = 3
} dynamicIntervalResult_t;

int32_t m_lastTenthLevelChangePerHr = 0;
int8_t m_lastTransmittedLevel = 0;
int32_t m_lastTransmittedLevelUnixtime = 0;
dynamicIntervalResult_t m_lastDynamicIntervalResult = DYNAMIC_INTERVAL_UNKNOWN_RESULT;


void sensors_thread(void *arg);
bool sensors_updateDisplay(char * str);
bool sensors_readStatus(void);
bool sensors_readSensors(void);
bool sensors_readInternalTemp(int16_t * temp);
bool sensors_readBattery(uint16_t * batt);
bool sensors_signalStrength(uint8_t * sig, uint8_t rssi);

bool sensors_readCapacitanceWand(int8_t * lvl, bool * isvalid);

bool sensors_packData(sf_record_t * sfRec, sensorData_t * sensor_data);
bool sensors_packStatus(sf_record_t * sfRec, statusData_t * status_data);

bool sensors_setCalibrationDataPoint(uint8_t * cal_data);
bool sensors_setMeasurementIntervalSeconds(uint32_t interval);
void sensorIntervalTimerCallback(TimerHandle_t xTimer);
bool sensors_updateLastUploadedMeasurement(void);
bool sensors_capwandTriggerUpload (int8_t newLevel, uint32_t newLevelUnixtime);
bool sensors_readCapwandCalibration(sensorData_t * snsrData);

NotifyHandle_t lowBatteryNotifyHandle;
static Notify_t lowBatteryNotify;

static StaticQueue_t sensorsStaticReadQueue;
/* Sensors Read: Allows other tasks to trigger sensors readings.
 * Function blocks until timeout or sensors task responds via queue. */

bool sensors_read(TickType_t blockUntil, sensors_responce_t type) {
	bool success = true;
	sensors_control_message_t sensorsCtlMsg;
	sensors_responce_t sensorsReadRsp;

	QueueHandle_t rspQueue = NULL;
	uint8_t rspQueueBuffer[1 * sizeof(sensors_responce_t)];

	if (blockUntil) {
		/* Trigger a sensor reading with a responseQueue: TODO: protect static queue handle with mutex. */
		rspQueue = xQueueCreateStatic(1, sizeof(sensors_responce_t), rspQueueBuffer, &sensorsStaticReadQueue);
		sensorsCtlMsg.responseQueue = rspQueue;
	} else {
		sensorsCtlMsg.responseQueue = NULL;
	}
	/* Trigger a sensors reading now so that there will be readings available in S&F */
	if (type == SENSORS_READ) {
		sensorsCtlMsg.type = SENSORS_EVENT_READ_NOW;
	} else {
		sensorsCtlMsg.type = SENSORS_EVENT_READ_STATUS;
	}

	if (xQueueSend(sensors_controlQueue,&sensorsCtlMsg,pdMS_TO_TICKS(100)) == pdTRUE) {
		xEventGroupSetBits(xSensorsEventGroup, SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT);
		if (blockUntil) {
			if (xQueueReceive(rspQueue, &sensorsReadRsp, pdMS_TO_TICKS(blockUntil)) == pdTRUE) {
				if (sensorsReadRsp == type) {
					success = true;
					/* Sensors task reported that the sensors were read successfully. */
				} else {
					log_errorf("Sensors/Status read failed![%s:%u]", __FUNCTION__,__LINE__);
					success = false;
				}
			} else {
				log_errorf("No response from sensors control queue![%s:%u]", __FUNCTION__,__LINE__);
				success = false;
			}
		} else {
			success = true;
		}
	} else {
		log_errorf("Failed to send Sensors/Status read command![%s:%u]", __FUNCTION__,__LINE__);
		success = false;
	}

	return success;
}

bool sensors_hasValidDataUploaded() {

	if (!m_sensorTask_lastMeasurement.level_valid) {
		log_warn("Level sensor not attached. Skipping validation.");
		return true;
	}
	if (m_sensorTask_lastValidMeasurement.level_valid == true) {
		return true;
	}

	if (sensors_read(pdMS_TO_TICKS(10000), SENSORS_READ) == false) {
		log_error("Failed to trigger sensor read to validate dynamic interval!");
	}

	/* short delay to allow S&F to receive sensor data. */
	vTaskDelay(pdMS_TO_TICKS(1000));

	return false;
}


StaticTask_t m_sensors_thread_static;
#define SENSORS_STACK_SIZE		256
StackType_t m_sensors_thread_buffer[ SENSORS_STACK_SIZE ];


bool sensors_init(void) {

	/* Create semaphore to protect the global current measurement data. */
	g_recentSensorData_smphr = xSemaphoreCreateBinaryStatic(&g_recentSensorData_smphr_buffer);
	configASSERT( g_recentSensorData_smphr );
	xSemaphoreGive(g_recentSensorData_smphr);

	/* Create sensors_controlQueue */
	if ((sensors_controlQueue = xQueueCreateStatic(SENSORSCTL_QUEUE_LENGTH,SENSORSCTL_QUEUE_ITEMSIZE,
			sensors_controlQueueBuffer, &sensors_controlQueueStatic)) == pdFAIL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}

	/* Create and event group for triggering on incoming events from various sources */
	xSensorsEventGroup = xEventGroupCreateStatic(&xSensorsEventGroupStatic);
	configASSERT( xSensorsEventGroup );

	/* Initialize low battery notification */
	if (notifyInit(&lowBatteryNotify)) {
		lowBatteryNotifyHandle = &lowBatteryNotify;
	}


	// Start execution.
	if ((m_sensors_thread = xTaskCreateStatic(sensors_thread, "SNSR", SENSORS_STACK_SIZE, NULL,
			1, m_sensors_thread_buffer, &m_sensors_thread_static)) == NULL) {
		APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
	}
	return true;

}

bool sensors_setMeasurementInterval(uint32_t newMeasurementInterval_sec) {
	bool success;
	sensors_control_message_t miMsg;
	miMsg.type = SENSORS_EVENT_MI_UPDATE;
	miMsg.responseQueue = NULL;
	miMsg.data._ui32 = newMeasurementInterval_sec;
	success = xQueueSend(sensors_controlQueue, &miMsg, pdMS_TO_TICKS(100));
	if (success) {
		success = (xEventGroupSetBits(xSensorsEventGroup,
				SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT) & SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT);
	}
	return success;
}


bool sensors_requestRead() {
	if (xSensorsEventGroup == NULL) {
		return false;
	}

	if (xEventGroupGetBits(xSensorsEventGroup) & SENSORS_EVENT_REQUEST_READING_BIT) {
		log_warn("Sensors read event already pending");
	} else {
		 xEventGroupSetBits(xSensorsEventGroup, SENSORS_EVENT_REQUEST_READING_BIT);
	}

	return true;
}

void sensors_thread(void *arg) {
	/* Sensors thread local gQ containers */
	EventBits_t activeEventBit;
	NotifyReceipt_t uploadNfRecp;
	sensors_control_message_t sensorControlMessage;
	sensors_responce_t resp;

	log_info("SENSORS THREAD STARTED\r\n");
	/* Initialize device ID, call if semaphore is claimed */
	deviceID_init();

	/* initialize external sensor line */
	extsensor_init();

	/* initialize battery temperature module */
	battTemp_init();

	/* attempt to subscribe to upload event notification */
	NotifyInfo_t uploadSub = {
			.eventGroup = xSensorsEventGroup,
			.eventBit = SENSORS_EVENT_UPLOAD_FINISHED_BIT,
			.respQueue = NULL
	};
	if (!notifySubscribe(uploadFinishedNotifyHandle, &uploadNfRecp, uploadSub) ) {
		APP_ERROR_CHECK(NRF_ERROR_INVALID_STATE);
	}

	/* Start Measurement Interval Timer (MI) */
	uint8_t tid = SENSOR_MEASUREMENT_INTERVAL_TIMER;
	m_sensorsMiTimer = xTimerCreateStatic("TSNS", pdMS_TO_TICKS(m_sensorsMeasurementInterval_ms), pdTRUE, (void *) &tid, sensorIntervalTimerCallback, &m_sensorsMiTimerStatic);
#if defined(MODEM_TESTING) && (MODEM_TESTING == 1)
#else
	xTimerStart(m_sensorsMiTimer, pdMS_TO_TICKS(100));
#endif

	/* Main Sensor Task Loop */
	for (;;) {
		/* Wait for event to be triggered in the task event group*/
		if (!xSensorsGroupBits) {
			xSensorsGroupBits = xEventGroupWaitBits(xSensorsEventGroup, xSensorsGroupBitMask, pdFALSE, pdFALSE, portMAX_DELAY);
		}

		activeEventBit = 0;
		/* Handle incoming sources (Queues) */
		if (xSensorsGroupBits & SENSORS_EVENT_MI_TIMER_EXPIRED_BIT) {
			/* Simple timer event, */
			xSensorsGroupBits &= ~(SENSORS_EVENT_MI_TIMER_EXPIRED_BIT);
			xEventGroupClearBits(xSensorsEventGroup, SENSORS_EVENT_MI_TIMER_EXPIRED_BIT);
			activeEventBit = SENSORS_EVENT_MI_TIMER_EXPIRED_BIT;
		} else if (xSensorsGroupBits & SENSORS_EVENT_REQUEST_READING_BIT){
			/* Forced measurement event, */
			xSensorsGroupBits &= ~(SENSORS_EVENT_REQUEST_READING_BIT);
			xEventGroupClearBits(xSensorsEventGroup, SENSORS_EVENT_REQUEST_READING_BIT);
			activeEventBit = SENSORS_EVENT_REQUEST_READING_BIT;
		} else if (xSensorsGroupBits & SENSORS_EVENT_UPLOAD_FINISHED_BIT){
			/* Forced measurement event, */
			xSensorsGroupBits &= ~(SENSORS_EVENT_UPLOAD_FINISHED_BIT);
			xEventGroupClearBits(xSensorsEventGroup, SENSORS_EVENT_UPLOAD_FINISHED_BIT);
			activeEventBit = SENSORS_EVENT_UPLOAD_FINISHED_BIT;
		} else if (xSensorsGroupBits & SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT) {
			if (xQueueReceive(sensors_controlQueue,&( sensorControlMessage ),pdMS_TO_TICKS(10)) == true) {
				activeEventBit = SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT;
			} else {
				xSensorsGroupBits &= ~(SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT);
				xSensorsGroupBits |= xEventGroupClearBits(xSensorsEventGroup, SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT);
			}
		} else {
			/* Unknown event bit triggered */
			log_errorf("Unknown event bits [%08x] in xSensorsGroupBits", xSensorsGroupBits);
		}

		switch (activeEventBit) {
		case SENSORS_EVENT_REQUEST_READING_BIT:
		case SENSORS_EVENT_MI_TIMER_EXPIRED_BIT:
			log_info("Measurement Interval Expired, Reading Sensors...");
			sensors_readSensors();
			break;

		case SENSORS_EVENT_CONTROL_COMMAND_REQUEST_BIT:
			switch (sensorControlMessage.type) {
			case SENSORS_EVENT_SET_CALIBRATION_POINT:
				log_info("Updating Calibration Setpoint...");
				/* Pass the calibration point info to the capwand sensor */
				sensors_setCalibrationDataPoint(sensorControlMessage.data.a_ui8);
				break;

			case SENSORS_EVENT_MI_UPDATE:
				if (sensors_setMeasurementIntervalSeconds(sensorControlMessage.data._ui32)) {
					log_infof("New Measurement Interval %u", sensorControlMessage.data._ui32);
				} else {
					log_errorf("Failed to set measurement interval to %u!", sensorControlMessage.data._ui32);
				}
				break;

			case SENSORS_EVENT_READ_STATUS:
				log_info("Reading device status...");
				sensors_readStatus();
				resp = STATUS_READ;
				/* If response queue is valid, report sensors read. */
				if (sensorControlMessage.responseQueue) {
					if(xQueueSend(sensorControlMessage.responseQueue, &resp, pdMS_TO_TICKS(1000))) {
						log_info("Sending response to scController...");
					}
				}
				break;

			case SENSORS_EVENT_READ_NOW:
				log_info("Forcing a measurement interval now. Reading sensors...");
				sensors_readSensors();
				resp = SENSORS_READ;
				/* If response queue is valid, report sensors read. */
				if (sensorControlMessage.responseQueue) {
					if(xQueueSend(sensorControlMessage.responseQueue, &resp, pdMS_TO_TICKS(1000))) {
						log_info("Sending response to scController...");
					}
				}
			case SENSORS_EVENT_CALIBRATE_LEVEL:
				break;				
			}
			break;

		case SENSORS_EVENT_UPLOAD_FINISHED_BIT:
			sensors_updateLastUploadedMeasurement();
			break;
		default:
			break;
		}

	}
}


bool sensors_setMeasurementIntervalSeconds(uint32_t interval) {
	/* interval is in seconds, convert to ms ticks */
	m_sensorsMeasurementInterval_ms = pdMS_TO_TICKS(interval * 1000);
	xTimerChangePeriod(m_sensorsMiTimer, m_sensorsMeasurementInterval_ms, portMAX_DELAY);
	log_infof("New measurement interval: %u", m_sensorsMeasurementInterval_ms);

	return true;
}

bool sensors_getMeasurementIntervalSeconds(uint32_t * interval) {
	/* interval is in seconds, convert to ms ticks */
	if ((interval == NULL) || (!xTimerIsTimerActive(m_sensorsMiTimer))) {
		return false;
	}
	*interval = (xTimerGetPeriod(m_sensorsMiTimer) / pdMS_TO_TICKS(1000));
	return true;
}



void sensorIntervalTimerCallback(TimerHandle_t xTimer) {
	/* The sensor measurement interval timer has expired, put a null timer event element into the queue */
	EventBits_t bits = xEventGroupSetBits(xSensorsEventGroup,SENSORS_EVENT_MI_TIMER_EXPIRED_BIT) & SENSORS_EVENT_MI_TIMER_EXPIRED_BIT;

	if (!(bits & SENSORS_EVENT_MI_TIMER_EXPIRED_BIT)) {
		log_error("Failed to set SENSORS_EVENT_MI_TIMER_EXPIRED_BIT in xSensorsEventGroup event group!");
	}
}



/* Temporary function that will be replaced by SV functions later */
bool sensors_packData(sf_record_t * sfRec, sensorData_t * sensor_data) {
	uint16_t ptr = 0;
	uint16_t messages = 0;
	int8_t batteryState = 0;

	/* Initialize record header */
	sfRec->header.type = SF_SENSORS_RECORD;
	sfRec->header.utc_s = 0;
	sfRec->header.timeStamp = sensor_data->timestamp;

	/* Copy data */
	uint8_t temp[2] = { (uint8_t)((sensor_data->temperature_tenthDegC * 16 / 10) & 0x00FF),
			(uint8_t)(((sensor_data->temperature_tenthDegC * 16 / 10) >> 8) & 0x00FF) };
	int8_t level = sensor_data->level_percentage;
	uint8_t temp_c[4];

	memcpy(temp_c,&(sensor_data->capacitance),4);

	/* Insert Temperature */
	sfRec->data[ptr++] = SENSOR_ID_TEMP; 		// Sensor message ID
	sfRec->data[ptr++] = 1; 						// Single sensor
	sfRec->data[ptr++] = 0; 						// Port 0
	sfRec->data[ptr++] = temp[1]; 				// Temp MSB
	sfRec->data[ptr++] = temp[0]; 				// Temp LSB
	messages++;

	sfRec->data[ptr++] = SENSOR_ID_VOLTAGE;
	sfRec->data[ptr++] = 1;
	sfRec->data[ptr++] = 20;
	sfRec->data[ptr++] = 0;
	sfRec->data[ptr++] = ((sensor_data->batteryVoltage_mV >> 8) & 0x000000FF);
	sfRec->data[ptr++] = ((sensor_data->batteryVoltage_mV >> 0) & 0x000000FF);
	messages++;

	/* Insert Improved Battery State */
	batteryState = (sensor_data->batteryPercentage / 10);
	if (power_acPowerGood()) {
		batteryState |= 0x10;
	}
	sfRec->data[ptr++] = BATTERY_STATE_MESSAGE_ID;
	sfRec->data[ptr++] = batteryState;
	messages++;

	/* TODO: If calibration is invalid, send an error message indicating this issue */

	/* If the level sensor is not detected, add the data to the sensor data item */
	if (sensor_data->level_valid) {
		/* Insert Level */
		sfRec->data[ptr++] = SENSOR_ID_LEVEL; 		// Sensor message ID
		sfRec->data[ptr++] = 1; 						// Single sensor
		sfRec->data[ptr++] = 10; 					// Port 10
		sfRec->data[ptr++] = level; 					// Level
		messages++;

		/* Insert Capacitance */
		sfRec->data[ptr++] = SENSOR_ID_CAPACITANCE; 	// Sensor message ID
		sfRec->data[ptr++] = 1; 						// Single sensor
		sfRec->data[ptr++] = 11; 					// Port 11
		sfRec->data[ptr++] = temp_c[2]; 				// Capacitance MSB
		sfRec->data[ptr++] = temp_c[1]; 				//
		sfRec->data[ptr++] = temp_c[0];				// Capacitance LSB
		messages++;
	}


	sfRec->header.dataLength = ptr;
	sfRec->header.messages = messages;

	return true;
}

bool sensors_packStatus(sf_record_t * sfRec, statusData_t * status_data) {
	uint16_t ptr = 0;
	uint16_t messages = 0;
	uint8_t sigstr = 255;

	sfRec->header.type = SF_STATUS_RECORD;
	sfRec->header.utc_s = 0;
	sfRec->header.timeStamp = status_data->timestamp;

//	if (power_batteryState(&batteryState, status_data->batteryVoltage_mV) && batteryState > 0) {
//		sfRec->data[ptr++] = BATTERY_STATE_MESSAGE_ID;
//		sfRec->data[ptr++] = batteryState;
//		messages++;
//	}

	/* Add the adjusted RSSI (0-10) information to every packet in effort to
	 * diagnose missed reports. */
	if (sensors_signalStrength(&sigstr,status_data->rsrp)) {
		sfRec->data[ptr++] = RSSI_MESSAGE_ID;
		sfRec->data[ptr++] = sigstr;
		messages++;
	}

	/* Add RAW RSRP/RSRQ and Battery (30mV steps) error message 0x99x78*/
	sfRec->data[ptr++] = ERROR_REPORT_MESSAGE_ID;
	sfRec->data[ptr++] = ERROR_REPORT_CSQ_SUBMESSAGE_ID;
	sfRec->data[ptr++] = (status_data->batteryVoltage_mV / 30);
	sfRec->data[ptr++] = status_data->rsrp;
	sfRec->data[ptr++] = status_data->rsrq;
	messages++;

	sfRec->header.dataLength = ptr;
	sfRec->header.messages = messages;

	return true;
}

bool sensors_readStatus(void) {
	bool success = false;
	/* Local Variables */
	statusData_t stats;
	sf_record_t stats_rec;


	stats.timestamp = get_time_100ms();
	sensors_readBattery(&stats.batteryVoltage_mV);

	success = true;

	if (!success) {
		xSemaphoreGive(g_recentSensorData_smphr);
	}

	if (success) {
		/* Request the current RSSI From the modem, 
		 * if this fails set to the default unknown value (255) */
		if (!modem_getSignalQuality(&(stats.rsrp), &(stats.rsrq))) {
			stats.rsrp = 255;
			stats.rsrq = 255;
		}
		sensors_packStatus(&stats_rec,&stats);
	}

	if (!success) {
		log_error("ERROR: Sensor measurement failure occurred, skipping measurement!");
		return success;
	}

	if (success) {
		/* Attempt to push the status S&F record into the queue */
		stoFwd_sendRecord(&stats_rec, portMAX_DELAY);
	}

	/* Update the locally stored recent data variables */
	if (xSemaphoreTake(g_recentSensorData_smphr, pdMS_TO_TICKS(1000)) == pdTRUE) {
		memcpy(&m_sensorTask_lastStatus, &stats, sizeof(statusData_t));
		xSemaphoreGive(g_recentSensorData_smphr);
	} else {
		log_error("Sensor measurement failure occurred!");
	}

	return success;
}


int16_t safeI32toI16(int32_t val_i32) {
	if (val_i32 > (int32_t)INT16_MAX) {
		return INT16_MAX;
	} else if (val_i32 < (int32_t)INT16_MIN) {
		return INT16_MIN;
	} else {
		return (int16_t) val_i32;
	}


}

bool sensors_packAndSendSensorStatus(void) {
	uint16_t ptr = 0,messages = 0;
	sf_record_t sensorStatus_rec;

	/* Initialize record header */
	sensorStatus_rec.header.type = SF_STATUS_RECORD;
	sensorStatus_rec.header.utc_s = 0;
	sensorStatus_rec.header.timeStamp = get_time_100ms();

	// Dynamic interval status
	sensorStatus_rec.data[ptr++] = 0x96;
	sensorStatus_rec.data[ptr++] = LONG_ERROR_REPORT_DYNAMIC_INTERVAL_STATUS;
	sensorStatus_rec.data[ptr++] = m_lastDynamicIntervalResult;
	sensorStatus_rec.data[ptr++] = ((uint8_t) ((safeI32toI16(m_lastTenthLevelChangePerHr) >> 8) & 0x00FF));
	sensorStatus_rec.data[ptr++] = ((uint8_t) (safeI32toI16(m_lastTenthLevelChangePerHr) & 0x00FF));
	sensorStatus_rec.data[ptr++] = m_lastTransmittedLevel;
	sensorStatus_rec.data[ptr++] = m_sensorTask_lastMeasurement.level_percentage;
	sensorStatus_rec.data[ptr++] = (uint8_t)((safeI32toI16((m_sensorTask_lastMeasurement.unixtime - m_lastTransmittedLevelUnixtime) / 60) >> 8) & 0x00FF);
	sensorStatus_rec.data[ptr++] = (uint8_t)(safeI32toI16((m_sensorTask_lastMeasurement.unixtime - m_lastTransmittedLevelUnixtime) / 60) & 0x00FF);
	messages++;

	//Battery Temperature Model Status
	sensorStatus_rec.data[ptr++] = 0x96;
	sensorStatus_rec.data[ptr++] = LONG_ERROR_REPORT_BATTERY_TEMPERATURE_STATUS;
	sensorStatus_rec.data[ptr++] = (uint8_t)(m_sensorTask_lastMeasurement.batteryVoltage_mV / 30);
	sensorStatus_rec.data[ptr++] = (uint8_t)((m_sensorTask_lastMeasurement.temperature_tenthDegC) >> 8);
	sensorStatus_rec.data[ptr++] = (uint8_t)((m_sensorTask_lastMeasurement.temperature_tenthDegC) & 0x00FF);
	sensorStatus_rec.data[ptr++] = (uint8_t)((battTemp_getLastLowThreshold() >> 8) & 0x00FF);
	sensorStatus_rec.data[ptr++] = (uint8_t)((battTemp_getLastLowThreshold()) & 0x00FF);
	sensorStatus_rec.data[ptr++] = (uint8_t)((battTemp_getLastHighThreshold() >> 8) & 0x00FF);
	sensorStatus_rec.data[ptr++] = (uint8_t)((battTemp_getLastHighThreshold()) & 0x00FF);
	messages++;

	sensorStatus_rec.header.messages = messages;
	sensorStatus_rec.header.dataLength = ptr;

    // Send data to the S&F buffer
    if (stoFwd_sendRecord(&sensorStatus_rec, pdMS_TO_TICKS(5000)) == false) {
    		log_error("Failed to queue startup error messages!");
    		return false;
    }
    return true;

}



bool sensors_readSensors(void) {
	/* Local variables */
	char str[4];
	sensorData_t snsrs;
	sf_record_t snsrs_rec;
	bool success = true;
	int err;

	// Clear sensors data struct
	memset(&snsrs,0,sizeof(sensorData_t));

	/* Stamp readings with current time :) */
	snsrs.timestamp = get_time_100ms();
	if (!unixTime_getSeconds(&snsrs.unixtime)) {
		log_warnf("Unixtime unknown, local timestamp only![%s]!",__FUNCTION__);
	}

	/* Read Sensors */
	sensors_readInternalTemp(&snsrs.temperature_tenthDegC);
	err = capwand_readSensor(&snsrs.level_percentage,&snsrs.capacitance);
	if (err == CAPWAND_SUCCESS) {
		sensors_readCapwandCalibration(&snsrs);
		snsrs.level_valid = true;
	} else {
		snsrs.level_valid = false;		
	}

	sensors_readBattery(&snsrs.batteryVoltage_mV);

	snsrs.batteryPercentage = battTemp_getBatteryPercentage(
			xTaskGetTickCount(),
			snsrs.batteryVoltage_mV,
			snsrs.temperature_tenthDegC);

	log_info("\r\nLocal Sensor Status:")
	log_infof("Temperature: %d°dC", (int)snsrs.temperature_tenthDegC);
	log_infof("Battery Voltage: %umV", snsrs.batteryVoltage_mV);
	log_infof("Battery Percentage: %u%%", snsrs.batteryPercentage);

	if (snsrs.level_valid) {
		log_infof("Level Percentage: %u%%", snsrs.level_percentage);
		log_infof("Level Capacitance: %ufF", snsrs.capacitance);
		snprintf(str,sizeof(str),"%u",snsrs.level_percentage);
	} else {
		strcpy(str,"--");
	}
	/* Indicate new sensor values to attached BLE devices */
	ble_levelSensorService_indicateLevel(&m_levelSensorService, snsrs.level_percentage);
	ble_levelSensorService_indicateCapacitance(&m_levelSensorService, snsrs.capacitance);

	/* Update displays */
	sensors_updateDisplay(str);

	/* Packet up data types. */
	sensors_packData(&snsrs_rec,&snsrs);

	// Send the record to S&F
	stoFwd_sendRecord(&snsrs_rec, portMAX_DELAY);

	/* Update the locally stored recent data variables */
	if (xSemaphoreTake(g_recentSensorData_smphr, pdMS_TO_TICKS(1000)) == pdTRUE) {
		memcpy(&m_sensorTask_lastMeasurement, &snsrs, sizeof(sensorData_t));
		xSemaphoreGive(g_recentSensorData_smphr);
	} else {
		log_error("Sensor measurement failure occurred!");
	}

	m_lastDynamicIntervalResult = DYNAMIC_INTERVAL_UNKNOWN_RESULT;

	if (snsrs.level_valid && unixTime_isValid()) {
		/* overwrite new last valid readings */
		memcpy(&m_sensorTask_lastValidMeasurement, &snsrs, sizeof(sensorData_t));

		/* validate the upload now conditions */
		if (m_sensorTask_uploadHasOccured) {
			if (sensors_capwandTriggerUpload(snsrs.level_percentage, snsrs.unixtime)) {
				if (!g_uploadInprogress) {
					log_info("Dynamic interval event triggered");
					scController_startUpload(UPLOAD_EVENT_DYNAMIC_INTERVAL_THREASHOLD);
				} else {
					m_lastDynamicIntervalResult = DYNAMIC_INTERVAL_SKIPPED_UPLOAD_INPROGRESS;
					log_info("Deferring dynamic interval event, upload in progress");
				}
			} else {
				log_warn("Dynamic interval event skipped");
			}
		} else {
			m_lastDynamicIntervalResult = DYNAMIC_INTERVAL_SKIPPED_VALID_DATA_NOT_TRANSMISSION;
			log_warn("Skipping dynamic interval check, valid data has yet to be transmitted");
		}
	} else {
		if (snsrs.level_valid) {
			m_lastDynamicIntervalResult = DYNAMIC_INTERVAL_SKIPPED_UNIXTIME_NOT_VALID;
		} else {
			m_lastDynamicIntervalResult = DYNAMIC_INTERVAL_SKIPPED_LEVEL_NOT_VALID;
		}
		log_warn("Skipping dynamic interval check, level or Unix time is invalid");
	}

	sensors_packAndSendSensorStatus();


	return success;
}



/* Individual Sensor Read Functions */

bool sensors_readCapwandCalibration(sensorData_t * snsrData) {
	int8_t level;
	uint32_t cap_fF;
	capwand_readCalibration(0,&level,&cap_fF);
	log_infof("Level Calibration Point 0: \tLevel\t%d%%\t\t\tCapacitance\t%ufF", level, cap_fF);
	snsrData->calibrationData0[2] = level;
	cap_fF /= 10;
	memcpy(snsrData->calibrationData0,(uint8_t *)&cap_fF,2);
	capwand_readCalibration(1,&level,&cap_fF);
	log_infof("Level Calibration Point 1: \tLevel\t%d%%\t\t\tCapacitance\t%ufF", level, cap_fF);
	snsrData->calibrationData1[2] = level;
	cap_fF /= 10;
	memcpy(snsrData->calibrationData1,(uint8_t *)&cap_fF,2);

	return true;
}

/**
 * Calibrate the capwand with associated data
 * Use of >= allows non failure codes to continue with success
 * 
 * param[in] data	pointer to data containing calibration data
 * 
 * return true on calibration saved and false otherwise
*/
bool sensors_setCalibrationDataPoint(uint8_t * data) {
	uint8_t point = data[0];
	int8_t level = data[3];
	uint16_t cap_data;
	uint32_t cap_fF;
	memcpy((uint8_t *)&cap_data,&data[1],2);
	cap_fF = ((uint32_t)cap_data) * 10;
	if (capwand_writeCalibration(point,level,cap_fF) >= CAPWAND_SUCCESS) {
		return true;
	} else {
		return false;
	}
}


bool sensors_readInternalTemp(int16_t * temp) {
	bool status;
	int16_t internalTemp;

	if (!g_simTemp) {
		status = si705x_getTemp_tenthDegC(&internalTemp);
		if (status) {
			*temp = internalTemp;
		}
	} else {
		*temp = g_simTemp10DegC;
	}
	return status;
}

bool sensors_readBattery(uint16_t * batt) {
	bool success = true;
	uint16_t vbatt;
	success = power_readBattery(&vbatt);
	if (success) {
		*batt = vbatt;
		m_validBatteryReading = true;
	} else {
		m_validBatteryReading = false;
	}

	if (m_validBatteryReading) {
		log_debugf("Battery Voltage: %umV",vbatt);
//		if (power_checkBatteryVoltageLow(vbatt)) {
//			notifySubscribers(lowBatteryNotifyHandle,&vbatt);
//		}
	}

	return success;
}

bool sensors_signalStrength(uint8_t * sig, uint8_t rssi) {
	uint8_t sig_level = 0;
	if (rssi != 255) {
		sig_level = (rssi * 10) / 97;
	} else {
		return false;
	}
	*sig = sig_level;
	return true;
}

/* Display functions */
bool sensors_updateDisplay(char * str) {

	if (!ui_lcdUpdate(str)) {
		/* Failed to post the message, even after 10 ticks. */
		log_debugf("Failed to updated LCD![%s]!",__FUNCTION__);
		return false;
	}
	return true;
}

bool sensors_updateLastUploadedMeasurement() {
	/* Update the locally stored recent data variables */

	if (m_sensorTask_lastValidMeasurement.level_valid) {
		memcpy(&m_sensorTask_lastUploadedMeasurement, &m_sensorTask_lastValidMeasurement, sizeof(sensorData_t));
		m_sensorTask_uploadHasOccured = true;
		log_debugf("Updated m_sensorTask_lastValidMeasurement: %d",m_sensorTask_lastUploadedMeasurement.level_percentage);
		return true;
	} else {
		log_error("Last level measurement was invalid, not updating m_sensorTask_lastUploadedMeasurement.");
		return false;
	}

}


/* Trigger upload based on current and previous level data */
// Transmission Critirea:
// 	1.) New Level is > 30% and <= 50%
// 			AND time since last upload >= 6HRs
// 			AND useage is > 1.5%/HR
// 	2.) New level is > 10% and <= 30%
// 			AND last uploaded level > 35%
// 			OR time since last upload >= 4HRs
bool sensors_capwandTriggerUpload (int8_t newLevel, uint32_t newLevelUnixtime) {

	m_lastTransmittedLevel = m_sensorTask_lastUploadedMeasurement.level_percentage;
	m_lastTransmittedLevelUnixtime = m_sensorTask_lastUploadedMeasurement.unixtime;


	float levelChangePerHr =
		(float) (m_lastTransmittedLevel-newLevel) /  // reverse of what was requested by PXA
		(((float)(newLevelUnixtime - m_lastTransmittedLevelUnixtime))/3600);

	m_lastTenthLevelChangePerHr = levelChangePerHr * 10;


	// Validation
#if (1)
	log_debug("Checking for dynamic interval condition: ");
	log_debugf("m_lastTransmittedLevel: %d%%",(int)m_lastTransmittedLevel);
	log_debugf("m_lastTransmittedLevelUnixtime: %d seconds",(int)m_lastTransmittedLevelUnixtime);
	log_debugf("newLevel: %d%%",(int)newLevel);
	log_debugf("newLevelUnixtime: %d seconds",(int)newLevelUnixtime);
	log_debugf("tenthLevelChangePerHr: %d\\10 %%\\hr",(int)m_lastTenthLevelChangePerHr);
#endif

	if (newLevel > 50) {
		m_lastDynamicIntervalResult = DYNAMIC_INTERVAL_SKIPPED_LAST_LEVEL_ABOVE_WARNING_THREASHOLD;
		return false;  // all good
	} else if ((newLevel <= 50) && (newLevel > 30)) {
		if (((newLevelUnixtime - m_lastTransmittedLevelUnixtime)/(3600) >= 6) &&
			(levelChangePerHr > 1.5)) {
			m_lastDynamicIntervalResult = DYNAMIC_INTERVAL_RATE_CHANGE_EXCEEDED;
			log_infof("Dynamic interval triggered! (RATE>1.5) [%s]!",__FUNCTION__);
			return true;
		} else {
			m_lastDynamicIntervalResult = DYNAMIC_INTERVAL_SKIPPED_RATE_CHANGE_NOT_EXCEEDED;
			return false;
		}
	} else if ((newLevel <= 30) && (newLevel > 10)) {
		if (m_lastTransmittedLevel > 35) {
			m_lastDynamicIntervalResult = DYNAMIC_INTERVAL_LAST_LEVEL_TRIGGER_LOW_THRESHOLD;
			log_infof("Dynamic interval triggered! (LASTTX>35) [%s]!",__FUNCTION__);
			return true;
		} else if ((newLevelUnixtime - m_lastTransmittedLevelUnixtime)/(3600) >= 4) {
			m_lastDynamicIntervalResult = DYNAMIC_INTERVAL_LEVEL_LOW;
			log_infof("Dynamic interval triggered! (LOWLEVEL < 4HR) [%s]!",__FUNCTION__);
			return true;
		} else {
			m_lastDynamicIntervalResult = DYNAMIC_INTERVAL_SKIPPED_LOW_LEVEL_WAIT_PERIOD;
			return false;
		}
	} else {
		m_lastDynamicIntervalResult = DYNAMIC_INTERVAL_SKIPPED_LEVEL_BELOW_LOW_THRESHOLD;
		return false;
	}
	m_lastDynamicIntervalResult = DYNAMIC_INTERVAL_UNKNOWN_RESULT;
	return false;

}

/* Following functions must be called ONLY if g_recentSensorData_smphr is taken */

bool sensors_getRecentSensorData(sensorData_t * p_sensorData) {
	bool success = false;
	if (p_sensorData != NULL) {
		memcpy(p_sensorData, &m_sensorTask_lastMeasurement, sizeof(sensorData_t));
		success = true;
	}
	return success;
}

int16_t sensors_getRecentTemp(void) {
	return m_sensorTask_lastMeasurement.temperature_tenthDegC;
}

bool sensors_getRecentBattery(uint16_t * vbatt) {
	if (m_validBatteryReading) {
		*vbatt = m_sensorTask_lastMeasurement.batteryVoltage_mV;
		return true;
	} else {
		return false;
	}
}

bool sensors_getRecentBatteryPercentage(uint8_t * percentage) {
	if (m_validBatteryReading) {
		*percentage = m_sensorTask_lastMeasurement.batteryPercentage;
		return true;
	} else {
		return false;
	}
}

int8_t sensors_getRecentLevel(void) {
	return m_sensorTask_lastMeasurement.level_percentage;
}

uint32_t sensors_getRecentCapacitance(void) {
	return (uint32_t) m_sensorTask_lastMeasurement.capacitance;
}

bool sensors_getCalibrationDataPoint(uint8_t point, uint8_t * data) {
	if (point == 0) {
		memcpy(data,m_sensorTask_lastMeasurement.calibrationData0,3);
	} else if (point == 1) {
		memcpy(data,m_sensorTask_lastMeasurement.calibrationData1,3);
	} else {
		return false;
	}
	return true;
}
