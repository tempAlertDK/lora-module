/*
 * globals.h
 *
 *  Created on: Mar 3, 2017
 *      Author: Nate-Tempalert
 */

#ifndef SRC_TEMPALERT_GLOBALS_H_
#define SRC_TEMPALERT_GLOBALS_H_

#include <stdint.h>
#include <stdbool.h>

#define MAX_ALARM_PORTS	3

typedef enum __attribute__ ((__packed__)) {
	RESET_REASON_UNKNOWN = 0x00,
	RESET_REASON_POWERON = 0x01,
	RESET_REASON_EXTERNAL = 0x02,
	RESET_REASON_BROWNOUT = 0x03,
	RESET_REASON_JTAG = 0x04,
	RESET_REASON_WATCHDOG_UNEXPECTED = 0x05,
	RESET_REASON_WATCHDOG_BOOTLOADER_TRIGGER_UNKNOWN = 0x06,
	RESET_REASON_WATCHDOG_BOOTLOADER_TRIGGER_RESET_BUTTON = 0x07,
	RESET_REASON_WATCHDOG_BOOTLOADER_TRIGGER_SERIAL_DEBUG_COMMAND = 0x08,
	RESET_REASON_WATCHDOG_BOOTLOADER_TRIGER_SERVER_PACKET = 0x09,
	RESET_REASON_WATCHDOG_REBOOT_SERIAL_DEBUG_COMMAND = 0x0a,
	RESET_REASON_WATCHDOG_EMBER_NONRESPONSIVE = 0x0b,
	RESET_REASON_WATCHDOG_POWERDOWN = 0x0c,
	RESET_REASON_WATCHDOG_EMBER_FAILED_TO_SET_NETWORK_KEY = 0x0d,
	RESET_REASON_WATCHDOG_EMBER_FAILED_TO_SET_BROADCAST_KEY = 0x0e,
	RESET_REASON_WATCHDOG_BOOTLOADER_UPDATE_SUCCESSFUL = 0x0f,
	RESET_REASON_EXTERNAL_FROM_POWERED_DOWN = 0x10,
	RESET_REASON_EXITING_SIGNAL_FINDER_MODE = 0x11,
	RESET_REASON_SOFTWARE_RESET = 0x12,
	RESET_REASON_CPU_LOCKUP = 0x13,
	RESET_REASON_GPIO_DETECT = 0x14,
	RESET_REASON_ANADETECT_LPCOMP = 0x15
} resetReason_t;

typedef enum __attribute__ ((__packed__)) {
	BOOTLOADER_TRIGGER_UNKNOWN = 0x00,
	BOOTLOADER_TRIGGER_RESET_BUTTON = 0x01,
	BOOTLOADER_TRIGGER_SERIAL_DEBUG_COMMAND = 0x02,
	BOOTLOADER_TRIGGER_SERVER_PACKET = 0x03
} bootloaderTrigger_t;


/* sensor simulation */
extern bool g_simBatt;
extern uint16_t g_simBattVoltage;
extern bool g_simTemp;
extern int16_t g_simTemp10DegC;
extern bool g_simLevel;
extern int8_t g_simLevelPercent;
extern uint32_t g_simCapacitance;


extern bool g_alarmState;
extern uint8_t g_alarmCount;
extern uint8_t g_alarmPorts[MAX_ALARM_PORTS];
extern bool g_updateUnixTime;
extern bool g_bleIsAdvertising;
extern bool g_bleIsConnected;
extern bool g_modemIsBooting;
extern bool g_uploadInprogress;
extern bool g_lastUploadFailure;

extern bool g_sc_transmit;
/* General Device Information */
extern resetReason_t g_resetReason;


#endif /* SRC_TEMPALERT_GLOBALS_H_ */


