/*
 * deviceID.h
 *
 *  Created on: Jul 11, 2016
 *      Author: kwgilpin
 */

#ifndef SRC_TEMPALERT_INC_DEVICEID_H_
#define SRC_TEMPALERT_INC_DEVICEID_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#define DEVICE_ID_STRING_SIZE		21

void deviceID_init();
bool deviceID_getSerialNumber(uint64_t *serialNumber);
bool deviceID_getDeviceIDString(char *deviceIDString, size_t stringSize);
bool deviceID_getDeviceIDencString(char * deviceIDencString, size_t encStringSize);
bool deviceID_getSerialNumberArray(uint8_t * p_serialArray);
#endif /* SRC_TEMPALERT_INC_DEVICEID_H_ */
