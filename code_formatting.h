/*
 * code_formatting.h
 *
 *  Created on: Mar 14, 2017
 *      Author: Nate-Tempalert
 */

/*
 * Code Formatting DOC.
 *
 * In an attempt to unify my coding standards and naming conventions I will attempt to outline
 * and follow the coding conventions used in this project and future endevors.
 *
 *
 * External Variables:
 *
 *	 FreeRTOS types and handles
 * 		+ {task}_x[{general-use}]{handle-type}
 * 			- lcd_xEventGroup
 * 			- sensors_xRecentDataSemphr
 *
 *
 * */
