// schedule object
// has slots
// get_slot returns object, which can be:
//   downlink
//   uplink (default)
//   beacon
//   broadcast

typedef struct {
	loragw_slot_t *slots[LORAGW_NUM_SLOTS];
	int millis_per_slot;
} loragw_schedule_t;

void loragw_schedule_init(loragw_schedule_t *sched, int millis_per_slot) {
	memset(sched, 0, sched);
	sched->millis_per_slot = millis_per_slot;
}

// Returns slot for the given timestamp
// millis must be > 0 and less than len schedule
loragw_slot_t *loragw_schedule_get_slot(loragw_schedule_t *sched, int millis) {
	if (millis < 0) {
		APP_ERROR_HANDLER(NRF_ERROR_INVALID_PARAM);
	}
	int slot_idx = millis / sched->millis_per_slot;
	if (slot_idx > LORAGW_NUM_SLOTS) {
		APP_ERROR_HANDLER(NRF_ERROR_INVALID_PARAM);
	}
	return sched->slots[slot_idx];
}

void loragw_schedule_set_slot(int slot_index, lora_slot_t *slot) {
	if (slot_index < 0 || slot_index > LORAGW_NUM_SLOTS) {
		APP_ERROR_HANDLER(NRF_ERROR_INVALID_PARAM);
	}
	sched->slots[slot_idx] = slot;
}
