set $HOST_JLINK1 = 0
set $HOST_JLINK2 = 1

define jlink1
    set $HOST = $HOST_JLINK1
end

define jlink2
    set $HOST = $HOST_JLINK2
end

jlink1

define connect
    if $HOST == $HOST_JLINK1
        target remote localhost:2331
    end
    if $HOST == $HOST_JLINK2
        target remote localhost:2341
    end
end

define reset
    connect
    monitor reset
    monitor halt
end

define flash
    reset
    load
    reset
end

define mtrace
    set print address off
    set print array-indexes on
    set print array on
    p MEMTRACE
    p MEMTRACE->buf[0]@MEMTRACE.len
end

define mtrace_log
    if $argc != 1
        echo Error: First argument must be log file name\n
    end
    shell rm $arg0
    set logging file $arg0
    set pagination off
    set logging on
    # skip one element, otherwise we might get unlucky and be stopped
    # in the middle of the memtrace_event2 call, when it hasn't yet
    # updated write_index, so we'd end up printing the most recent item
    # first, erroneously.
    set $i = 0
    set $read = MEMTRACE->write_index + 1
    while $i < MEMTRACE->len - 1
        #printf "%u %u\n", MEMTRACE->buf[$i].ticks, MEMTRACE->buf[$i].ev
        output $read
        echo \ \ 
        output MEMTRACE->buf[$read].ticks
        echo \ \ 
        output MEMTRACE->buf[$read].ev
        echo \ \ 
        output/u MEMTRACE->buf[$read].arg1
        echo \ \ 
        output/u MEMTRACE->buf[$read].arg2
        echo \n
        set $read = $read + 1
        if $read >= MEMTRACE->len
            set $read = 0
        end
        set $i = $i + 1
    end
    set logging off
    set pagination on
end
