#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$SCRIPT_DIR/.."

if [ "$#" -ne 1 ]; then
    echo "APP to be debugged must be first argument. Ex: gateway or node"
    exit 1
fi

/usr/local/gcc-arm-none-eabi-4_9-2015q1/bin/arm-none-eabi-gdb _build/$1_nrf52832_xxaa_s132.out
