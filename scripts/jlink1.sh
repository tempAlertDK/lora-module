#!/bin/bash
#
# This is Shawn's j-Trace device.

JLinkGDBServer \
    -device NRF52832_XXAA \
    -if SWD \
    -select usb=751000179 \
    -port 2331 \
    -swoport 2332 \
    -telnetport 2333 \
    -RTTTelnetPort 2334
