#!/bin/bash
#
# This is Shawn's j-Trace device.

JLinkGDBServer \
    -device NRF52832_XXAA \
    -if SWD \
    -select usb=600105232 \
    -port 2341 \
    -swoport 2342 \
    -telnetport 2343 \
    -RTTTelnetPort 2344
